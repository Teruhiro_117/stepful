﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OpeningButton : MonoBehaviour
{
    public int stageNum;
    //GameObject obj;
    //GameObject prefab;
    // Use this for initialization
    void Start()
    {
        //obj = GameObject.Find("CanvasStart(Clone)");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClick()
    {
        switch (stageNum)
        {
            case 0:
                SceneManager.LoadScene("Lobby");
                break;
            case 1:
                SceneManager.LoadScene("arrangedKoma");
                break;
            case 2:
                //SceneManager.LoadScene("KomaBako");
                break;
            case 3:
                //Everyplay.StopRecording();
                SceneManager.LoadScene("opening");
                break;
            case 4:
                SceneManager.LoadScene("playing");
                break;
            case 5:
                Everyplay.PlayLastRecording();
                break;
            case 6:
                SceneManager.LoadScene("FirePlaying");
                break;
                /*
                Destroy(obj);
                prefab = (GameObject)Resources.Load("Prefab/CanvasData");
                Instantiate(prefab);
                */
                //SceneManager.LoadScene("demo");
                break;
        }
    }
}
