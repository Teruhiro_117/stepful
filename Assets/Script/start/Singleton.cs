﻿using UnityEngine;
using System.Collections;

public class Singleton : MonoBehaviour {

    static bool existsInstance = false;

	// Use this for initialization
	void Awake () {
        Debug.Log(System.DateTime.Now.ToString() + Random.Range(100, 1000));
        if (existsInstance)
        {
            Destroy(gameObject);
            return;
        }
        Init();
        existsInstance = true;
        DontDestroyOnLoad(gameObject);
	}

    public void soundOff()
    {
        AudioListener.volume = 0f;
    }

    public void soundOn()
    {
        AudioListener.volume = 1f;
    }

    public void setAccountName(string name)
    {
        PlayerPrefs.SetString("AccountName", name);
    }

    public void setInitBool(int num)
    {
        PlayerPrefs.SetInt("InitBool", num);
    }

    public void setNumOfWin(int num)
    {
        PlayerPrefs.SetInt("NumOfWin", num);
    }

    public void setNumOfLose(int num)
    {
        PlayerPrefs.SetInt("NumOfLose", num);
    }

    public void setRensyoNum(int num)
    {
        PlayerPrefs.SetInt("RensyoNum", num);
    }

    public void setMaxRensyoNum(int num)
    {
        PlayerPrefs.SetInt("maxRensyoNum", num);
    }

    public void setUniqueId(string id){
        PlayerPrefs.SetString("UniqueId", id);
    }

    public string getAccountName()
    {
        return PlayerPrefs.GetString("AccountName");
    }

    public int getInitBool()
    {
        return PlayerPrefs.GetInt("InitBool");
    }

    public int getNumOfWin()
    {
        return PlayerPrefs.GetInt("NumOfWin");
    }

    public int getNumOfLose()
    {
        return PlayerPrefs.GetInt("NumOfLose");
    }

    public int getRensyoNum()
    {
        return PlayerPrefs.GetInt("RensyoNum");
    }

    public int getMaxRensyoNum()
    {
        return PlayerPrefs.GetInt("maxRensyoNum");
    }

    public string getUniqueId()
    {
        return PlayerPrefs.GetString("UniqueId");
    }

    public void Init()
    {
        if(getInitBool() == 0)
        {
            setNumOfWin(0);
            setNumOfLose(0);
            setRensyoNum(0);
            setMaxRensyoNum(0);
            setAccountName("");
            setInitBool(1);
            setUniqueId(System.DateTime.Now.ToString()+Random.Range(100,1000));
        }
    }

    public void CountWinLose(bool win)
    {
        if (win)
        {
            int NumOfWin = getNumOfWin();
            NumOfWin++;
            setNumOfWin(NumOfWin);
            int RensyoNum = getRensyoNum();
            RensyoNum++;
            setRensyoNum(RensyoNum);
            if(RensyoNum > getMaxRensyoNum())
            {
                setMaxRensyoNum(RensyoNum);
            }
        }
        else
        {
            int NumOfLose = getNumOfLose();
            NumOfLose++;
            setNumOfLose(NumOfLose);
            setRensyoNum(0);
        }
    }

    public void Save()
    {
        PlayerPrefs.Save();
    }
}

