﻿using UnityEngine;
using System.Collections;

public class createCanvas : MonoBehaviour {

    public int canvasNum;
    GameObject prefab;
    // Use this for initialization
    void Start()
    {
        switch (canvasNum)
        {
            case 0:
                prefab = (GameObject)Resources.Load("Prefab/CanvasStart");
                Instantiate(prefab);
                break;
            case 1:
                prefab = (GameObject)Resources.Load("Prefab/CanvasPlaying");
                Instantiate(prefab);
                break;
            case 2:
                prefab = (GameObject)Resources.Load("Prefab/CanvasNetPlay");
                Instantiate(prefab);
                break;
            case 3:
                prefab = (GameObject)Resources.Load("Prefab/CanvasKomabako");
                Instantiate(prefab);
                break;
            case 4:
                prefab = (GameObject)Resources.Load("Prefab/CanvasArrangedKoma");
                Instantiate(prefab);
                break;
            case 5:
                prefab = (GameObject)Resources.Load("Prefab/CanvasLobby");
                Instantiate(prefab);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
