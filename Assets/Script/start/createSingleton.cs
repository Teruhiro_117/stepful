﻿using UnityEngine;
using System.Collections;

public class createSingleton : MonoBehaviour {

	// Use this for initialization
	void Start () {
        // プレハブを取得
        GameObject prefab = (GameObject)Resources.Load("Prefab/singleton");
        Instantiate(prefab);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
