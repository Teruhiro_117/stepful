﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class showData : MonoBehaviour {

    Text text;
    GameObject refObj;
    Singleton singleton;
    double average;

	// Use this for initialization
	void Start () {
        refObj = GameObject.Find("singleton");
        singleton = refObj.GetComponent<Singleton>();
        text = GameObject.Find("Canvas/Windows/Panel2/Name").GetComponent<Text>();
        text.text = "" + singleton.getAccountName();
        text = GameObject.Find("Canvas/Windows/Panel2/winNum").GetComponent<Text>();
        text.text = ""+singleton.getNumOfWin()+"勝利";
        text = GameObject.Find("Canvas/Windows/Panel2/loseNum").GetComponent<Text>();
        text.text = "" + singleton.getNumOfLose()+"敗北";
        text = GameObject.Find("Canvas/Windows/Panel2/averageNum").GetComponent<Text>();
        if(singleton.getNumOfWin() + singleton.getNumOfLose() == 0)
        {
            text.text = "";
        }else
        {
            average = 1.0*singleton.getNumOfWin() / (singleton.getNumOfWin() + singleton.getNumOfLose());
            average = average*100;
            text.text = "" + average + "%";
        }
        text = GameObject.Find("Canvas/Windows/Panel2/nowRensyoNum").GetComponent<Text>();
        text.text = "" + singleton.getRensyoNum()+"連勝";
        text = GameObject.Find("Canvas/Windows/Panel2/maxRensyoNum").GetComponent<Text>();
        text.text = "" + singleton.getMaxRensyoNum()+"連勝";
    }

    public void reShow(){
        refObj = GameObject.Find("singleton");
        singleton = refObj.GetComponent<Singleton>();
        text = GameObject.Find("Canvas/Windows/Panel2/Name").GetComponent<Text>();
        text.text = "" + singleton.getAccountName();
        text = GameObject.Find("Canvas/Windows/Panel2/winNum").GetComponent<Text>();
        text.text = ""+singleton.getNumOfWin()+"勝利";
        text = GameObject.Find("Canvas/Windows/Panel2/loseNum").GetComponent<Text>();
        text.text = "" + singleton.getNumOfLose()+"敗北";
        text = GameObject.Find("Canvas/Windows/Panel2/averageNum").GetComponent<Text>();
        if(singleton.getNumOfWin() + singleton.getNumOfLose() == 0)
        {
            text.text = "";
        }else
        {
            average = 1.0*singleton.getNumOfWin() / (singleton.getNumOfWin() + singleton.getNumOfLose());
            average = average*100;
            text.text = "" + average + "%";
        }
        text = GameObject.Find("Canvas/Windows/Panel2/nowRensyoNum").GetComponent<Text>();
        text.text = "" + singleton.getRensyoNum()+"連勝";
        text = GameObject.Find("Canvas/Windows/Panel2/maxRensyoNum").GetComponent<Text>();
        text.text = "" + singleton.getMaxRensyoNum()+"連勝";
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
