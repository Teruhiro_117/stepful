﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class arrangeKoma : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Init()
    {
        setKoma();
        setColorWhite();
    }

    //駒をバイトで割り当てる
    enum KOMAINF : byte
    {
        EMPTY,//0
        HU,//1
        GR,//2
        SR,//3
        MI,//4
        FA,//5
        DR,//6
        ST,//7
        EHU,//8
        EGR,//9
        ESR,//10
        EMI,//11
        EFA,//12
        EDR,//13
        EST,//14
        FUTI,//15
    }

    public static byte[,] getStartBoard()
    {
        return startBoard;
    }

    public bool getHuman()
    {
        return human;
    }

    public static int[,] getFactoryLocation()
    {
        return factoryLocation;
    }

    public static int[,] getBarrier()
    {
        return barrier;
    }

    public static int getMyResources()
    {
        return myResources;
    }

    //盤面の表示・プリント
    public static byte[,] startBoard;
    bool[,] canTouchPlace;
    bool human;
    bool resources;
    Sprite koma;
    int komaNum;
    Image img;
    Text text;
    int line;
    int row;
    float changeRed;
    float changeGreen;
    float changeBlue;
    float changeAlpha;
    public static int[,] barrier;
    public static int[,] factoryLocation;
    int[,] humanPosition;
    public static int myResources;

    //盤面の駒の初期化
    void setKoma()
    {
        startBoard = new byte[8, 8];
        canTouchPlace = new bool[2, 6];
        barrier = new int[2,2];
        barrier[0, 0] = -1;
        barrier[0, 1] = -1;
        factoryLocation = new int[2,2];
        factoryLocation[0, 0] = -1;
        factoryLocation[0, 1] = -1;
        //humanPosition = new int[2, 2];
        myResources = 40;
        resources = true;
        human = false;
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                startBoard[i, j] = (byte)KOMAINF.EMPTY;
            }
        }
        for (int i = 0; i < 8; i++)
        {
            startBoard[i, 0] = (byte)KOMAINF.FUTI;
            startBoard[i, 7] = (byte)KOMAINF.FUTI;
            startBoard[0, i] = (byte)KOMAINF.FUTI;
            startBoard[7, i] = (byte)KOMAINF.FUTI;
        }
        canNotTouchAll();
        barrier[1, 0] = 2;
        barrier[1, 1] = 1;
        factoryLocation[1, 0] = 4;
        factoryLocation[1, 1] = 5;
    }

    //駒を動かすプロセス
    public void printKoma()
    {
        //そのクリックしたマスがクリック可能かどうか
        if (canTouchPlace[line, row])
        {
            if (!human || komaNum != 1)
            {
                canNotTouchAll();
                countResources(komaNum);
                if (resources)
                {
                    changeKomaResources(startBoard[line, row]);
                    paint(line, row);                 
                    startBoard[row+1, line+5] = (byte)komaNum;                  
                }
                if (komaNum == 7)
                {
                    barrier[0, 0] = row + 1;
                    barrier[0, 1] = line + 5;
                }
                else if (komaNum == 5)
                {
                    factoryLocation[0, 0] = row + 1;
                    factoryLocation[0, 1] = line + 5;
                }
            }
        }
    }

    //上のマスに駒をプリントする
    void paint(int l, int r)
    {
        setColorWhite();
        GameObject.Find("CanvasArrangedKoma/pixel " + (r+1) + "-" + (l+1)).GetComponent<Image>().color
            = new Color(changeRed, changeGreen, changeBlue, changeAlpha);
        img = GameObject.Find("CanvasArrangedKoma/pixel " + (r+1) + "-" + (l+1)).GetComponent<Image>();
        img.sprite = koma;
    }

    //下のマスのすべての色をプリントする
    void allPrintColorBottom()
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                printColorBottom(i + 1, j + 1);
            }
        }
    }

    //下のマスのカラーをプリント
    void printColorBottom(int l, int r)
    {
        GameObject.Find("CanvasArrangedKoma/pixel " + l + "-" + r + " (1)").GetComponent<Image>().color
            = new Color(changeRed, changeGreen, changeBlue, changeAlpha);
    }

    //上のマスのカラーをプリント
    void printColorAbove(int l, int r)
    {
        GameObject.Find("CanvasArrangedKoma/pixel " + l + "-" + r).GetComponent<Image>().color
            = new Color(changeRed, changeGreen, changeBlue, changeAlpha);
    }

    //カラーの色を白にする
    void setColorWhite()
    {
        changeRed = 1.0f;
        changeGreen = 1.0f;
        changeBlue = 1.0f;
        changeAlpha = 1.0f;
    }

    //カラーの色を赤色にする
    void setColorRed()
    {
        changeRed = 1.0f;
        changeGreen = 0.0f;
        changeBlue = 0.0f;
        changeAlpha = 1.0f;
    }

    //カラーの色をブルーにする
    void setColorBlue()
    {
        changeRed = 0.0f;
        changeGreen = 0.0f;
        changeBlue = 1.0f;
        changeAlpha = 1.0f;
    }

    //カラーの色をクリアにする
    void setColorClear()
    {
        changeAlpha = 0.0f;
    }

    //すべての下のマスを白くする
    void setAllWhite()
    {
        setColorWhite();
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                printColorBottom(i + 1, j + 1);
            }
        }
    }

    //駒にそれぞれのスプライトを代入
    public void inputKoma(int num)
    {
        switch (num)
        {
            case 7:
                koma = Resources.Load<Sprite>("Sprite/friend/skytree");
                canTouchAll();
                komaNum = num;
                break;
            case 6:
                koma = Resources.Load<Sprite>("Sprite/friend/drone");
                komaNum = num;
                canTouchAll();
                break;
            case 5:
                koma = Resources.Load<Sprite>("Sprite/friend/factory");
                komaNum = num;
                canTouchAll();
                break;
            case 4:
                koma = Resources.Load<Sprite>("Sprite/friend/missile");
                komaNum = num;
                canTouchAll();
                break;
            case 3:
                koma = Resources.Load<Sprite>("Sprite/friend/silverRobot");
                komaNum = num;
                canTouchAll();
                break;
            case 2:
                koma = Resources.Load<Sprite>("Sprite/friend/goldenRobot");
                komaNum = num;
                canTouchAll();
                break;
            case 1:
                koma = Resources.Load<Sprite>("Sprite/friend/human");
                komaNum = num;
                canTouchAll();
                break;
            case 0:
                koma = Resources.Load<Sprite>("Sprite/white");
                komaNum = num;
                canTouchAll();
                break;
        }
    }

    //それぞれの資源のコストを取得
    void changeKomaResources(int num)
    {
        switch (num)
        {
            case 7:
                getResources(9);
                break;
            case 6:
                getResources(3);
                break;
            case 5:
                getResources(9);
                break;
            case 4:
                getResources(8);
                break;
            case 3:
                getResources(4);
                break;
            case 2:
                getResources(5);
                break;
            case 1:
                human = false;
                break;
            case 0:
                break;
        }
    }

    //それぞれの資源のコストを取得
    void countResources(int num)
    {
        switch (num)
        {
            case 7:
                if (myResources >8){
                    getResources(-9);
                    resources = true;
                }
                else
                {
                    resources = false;
                }
                
                break;
            case 6:
                if (myResources > 2)
                {
                    getResources(-3);
                    resources = true;
                }
                else
                {
                    resources = false;
                }
                break;
            case 5:
                if (myResources > 8)
                {
                    getResources(-9);
                    resources = true;
                }
                else
                {
                    resources = false;
                }
                break;
            case 4:
                if (myResources > 7)
                {
                    getResources(-8);
                    resources = true;
                }else
                {
                    resources = false;
                }
                break;
            case 3:
                if (myResources > 3)
                {
                    getResources(-4);
                    resources = true;
                }else
                {
                    resources = false;
                }
                break;
            case 2:
                if (myResources > 4)
                {
                    getResources(-5);
                    resources = true;
                }else
                {
                    resources = false;
                }
                break;
            case 1:
                human = true;
                resources = true;
                break;
            case 0:
                resources = true;
                break;
        }
    }

    //行と列の値を代入
    public void lineRow(int l, int r)
    {
        line = l;
        row = r;
    }

    //すべてのマスをタッチできなくする
    void canNotTouchAll()
    {
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                canTouchPlace[i, j] = false;
            }
        }
    }

    //すべてのマスをタッチできるようにする
    void canTouchAll()
    {
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                canTouchPlace[i, j] = true;
            }
        }
    }

    //味方の資源の増減
    void getResources(int resource)
    {
        text = GameObject.Find("CanvasArrangedKoma/Resources").GetComponent<Text>();
        myResources = myResources + resource;
        text.text = " " + myResources;
    }
}