﻿using UnityEngine;
using System.Collections;

public class selectPlace : MonoBehaviour
{

    GameObject refObj;
    public int line;
    public int row;

    // Use this for initialization
    void Start()
    {
        refObj = GameObject.Find("banmenScript");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClick()
    {
        arrangeKoma a = refObj.GetComponent<arrangeKoma>();
        a.lineRow(line, row);
        a.printKoma();
    }
}
