﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class selectKoma : MonoBehaviour
{ 
    GameObject refObj;
    public int selectNum;

    // Use this for initialization
    void Start()
    {
        refObj = GameObject.Find("banmenScript");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClick()
    {
        arrangeKoma a = refObj.GetComponent<arrangeKoma>();
        a.inputKoma(selectNum);
    }
}
