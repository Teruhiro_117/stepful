﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Distribution : MonoBehaviour {

    GameObject[,] prefab;
    GameObject prefabResource;
    GameObject prefabEResource;
    GameObject prefabBackGround;
    GameObject prefabRefresh;
	// Use this for initialization
	void Start () {
        var canvasObject = new GameObject("Canvas");
        var canvas = canvasObject.AddComponent<Canvas>();
        canvasObject.AddComponent<GraphicRaycaster>();
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        GameObject objPixel = (GameObject)Resources.Load("Prefab/PartOfCanvas/Pixel");
        GameObject objResource = (GameObject)Resources.Load("Prefab/PartOfCanvas/Resource");
        GameObject objBackGround = (GameObject)Resources.Load("Prefab/PartOfCanvas/BackGround");
        GameObject objRefresh = (GameObject)Resources.Load("Prefab/PartOfCanvas/Refresh");
        float width = Screen.width;
        float oneWidth = width / 6;
        float height = Screen.height;
        prefabBackGround = (GameObject)Instantiate(objBackGround);
        prefabBackGround.transform.SetParent(canvas.transform, false);
        prefabResource = (GameObject)Instantiate(objResource);
        prefabResource.transform.SetParent(canvas.transform, false);
        prefabResource.transform.position = new Vector2(oneWidth, height/3 - oneWidth);
        prefabEResource = (GameObject)Instantiate(objResource);
        prefabEResource.transform.SetParent(canvas.transform, false);
        prefabEResource.transform.position = new Vector2(oneWidth, height / 3 + oneWidth * 6);
        prefabRefresh = (GameObject)Instantiate(objRefresh);
        prefabRefresh.transform.SetParent(canvas.transform, false);
        prefabRefresh.transform.position = new Vector2(oneWidth*6, height / 3-10);
        prefab = new GameObject[6,6];
        for (int i = 0; i < 6; i++)
        {
            for(int j = 0; j < 6; j++)
            {
                prefab[i, j] = (GameObject)Instantiate(objPixel);
                prefab[i, j].transform.SetParent(canvas.transform, false);
                prefab[i, j].transform.position = new Vector2(oneWidth * i + oneWidth / 2, oneWidth * j + height / 3);
                prefab[i, j].name = "pixel " + (i + 1) + "-" + (j + 1);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
