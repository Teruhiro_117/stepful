﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class inputName : MonoBehaviour {

	GameObject obj;
	GameObject canvasObject;
	GameObject prefabResource;
	Text text;

	string str;
	public InputField inputField;
	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnClick()
    {
		canvasObject = GameObject.Find("Canvas");
		GameObject objResource = (GameObject)Resources.Load("Prefab/name");
		Canvas canvas = canvasObject.GetComponent<Canvas>();
		prefabResource = (GameObject)Instantiate(objResource);
        prefabResource.transform.SetParent(canvas.transform, false);
    }// Use this for initialization

	public void SaveText(){
		obj = GameObject.Find("singleton");
		Singleton b = obj.GetComponent<Singleton>();
		str = inputField.text;
		b.setAccountName(str);
		inputField.text = "";
		text = GameObject.Find("Canvas/Windows/Panel2/Name").GetComponent<Text>();
        text.text = "" + b.getAccountName();
		Destroy(GameObject.Find("Canvas/name(Clone)"));
	}
}
