﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class banmen : MonoBehaviour
{
    //盤面の表示・プリント
    public baseMethods baseM;
    public Koma[,] Board;
    public bool[,] canTouchPlace;
    bool[,] broken;
    int[,] barrier;
    int[,] factoryLocation;
    int[,] humanPosition;
    int[,] komaTime;
    Slider[,] komaSlider;
    int[,] EkomaTime;
    Slider[,] EkomaSlider;

    bool arrangeMode;
    bool arrange1;
    bool arrange2;
    bool humanExist;
    bool winLose;

    Image img;
    public Text text;

    public int line;
    public int row;

    public int halfOfKoma;

    public int bl;
    public int br;
    public int al;
    public int ar;

    public float changeRed;
    public float changeGreen;
    public float changeBlue;
    public float changeAlpha;

    public bool enemy;
    public bool wall;

    public bool satelite;
    public bool Esatelite;

    Koma kariKoma;
    int kariKomaNum;
    int myFactoryTime;
    int enemyFactoryTime;

    public int myResources;
    public int enemyResources;

    public Empty empty;
    Human human;
    GRobot gRobot;
    SRobot sRobot;
    Missile missile;
    Factory factory;
    Drone drone;
    Skytree skytree;
    EHuman Ehuman;
    EGRobot EgRobot;
    ESRobot EsRobot;
    EMissile Emissile;
    EFactory Efactory;
    EDrone Edrone;
    ESkytree Eskytree;
    Futi futi;

    //駒をバイトで割り当てる
    enum KOMAINF : byte
    {
        EMPTY,//0
        HU,//1
        GR,//2
        SR,//3
        MI,//4
        FA,//5
        DR,//6
        ST,//7
        EHU,//8
        EGR,//9
        ESR,//10
        EMI,//11
        EFA,//12
        EDR,//13
        EST,//14
        FUTI,//15
    }

    // Use this for initialization
    void Start()
    {
        Init();
    }

    void Init()
    {
        halfOfKoma = 8;
        arrangeMode = true;
        baseM = new baseMethods();
        baseM.Init("GameManeger");
        reset();
        Board = new Koma[8, 8];
        canTouchPlace = new bool[8, 8];
        humanExist = false;

        broken = new bool[6, 6];
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                broken[i, j] = true;
            }
        }

        barrier = new int[2, 2];
        factoryLocation = new int[2, 2];
        humanPosition = new int[2,2];

        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                barrier[i, j] = -1;
                factoryLocation[i, j] = -1;
                humanPosition[i,j]=-1;
            }
        }

        komaTime = new int[6, 6];
        komaSlider = new Slider[6, 6];
        EkomaTime = new int[6, 6];
        EkomaSlider = new Slider[6, 6];

        empty = new Empty();
        human = new Human();
        gRobot = new GRobot();
        sRobot = new SRobot();
        missile = new Missile();
        factory = new Factory();
        drone = new Drone();
        skytree = new Skytree();
        Ehuman = new EHuman();
        EgRobot = new EGRobot();
        EsRobot = new ESRobot();
        Emissile = new EMissile();
        Efactory = new EFactory();
        Edrone = new EDrone();
        Eskytree = new ESkytree();
        futi = new Futi();

        empty.Init("GameManeger");
        human.Init("GameManeger");
        gRobot.Init("GameManeger");
        sRobot.Init("GameManeger");
        missile.Init("GameManeger");
        factory.Init("GameManeger");
        drone.Init("GameManeger");
        skytree.Init("GameManeger");
        Ehuman.Init("GameManeger");
        EgRobot.Init("GameManeger");
        EsRobot.Init("GameManeger");
        Emissile.Init("GameManeger");
        Efactory.Init("GameManeger");
        Edrone.Init("GameManeger");
        Eskytree.Init("GameManeger");

        baseM.canTouchAll(true);
        baseM.canTouchArrangeMode();
        wall = true;
        enemy = false;
        satelite = true;
        Esatelite = true;
        winLose = false;
        myResources = 20;
        enemyResources = 20;
        baseM.getMyResources(0);
        baseM.getEResources(0);
        myFactoryTime = 101;
        enemyFactoryTime = 101;
        kariKomaNum = -1;
        kariKoma = empty;

        for (int i = 0; i < 8; i++)
        {
            Board[i, 0] = futi;
            Board[i, 7] = futi;
            Board[0, i] = futi;
            Board[7, i] = futi;
        }
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                Board[i + 1, j + 1] = empty;
            }
        }
        Board[2, 2] = Ehuman;
        Board[1,1] = Edrone;
        Board[6,1] = EgRobot;
        Board[5,1]= EsRobot;
        Board[4,1]=Emissile;
        Board[3,1]=Eskytree;
        Board[2,1]=Efactory;
        Board[1,2]=EgRobot;
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                komaTime[i, j] = 0;
                komaSlider[i, j] = GameObject.Find("CanvasPlaying/Slider " + (i + 1) + "-" + (j + 1)).GetComponent<Slider>();
                komaSlider[i, j].maxValue = 60;
                EkomaTime[i, j] = 0;
                EkomaSlider[i, j] = GameObject.Find("CanvasPlaying/Slider " + (i + 1) + "-" + (j + 1)+" (1)").GetComponent<Slider>();
                EkomaSlider[i, j].maxValue = 60;
                baseM.OffTimeBar(i + 1, j + 1);
                baseM.OffTimeBar2(i + 1, j + 1);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (komaTime[i, j] < komaSlider[i, j].maxValue)
                {
                    komaTime[i, j]++;
                    komaSlider[i, j].value = komaTime[i, j];
                }
                if (EkomaTime[i, j] < EkomaSlider[i, j].maxValue)
                {
                    EkomaTime[i, j]++;
                    EkomaSlider[i, j].value = EkomaTime[i, j];
                }
            }
        }
        if(myFactoryTime < 100)
        {
            myFactoryTime++;
        }else if(myFactoryTime == 100)
        {
            Board[factoryLocation[0, 0], factoryLocation[0, 1] - 1] = kariKoma;
            paint(factoryLocation[0, 0], factoryLocation[0, 1] - 1);
            baseM.OnTimeBar(factoryLocation[0, 0], factoryLocation[0, 1] - 1);
            myFactoryTime++;
        }
        if(enemyFactoryTime < 100)
        {
            enemyFactoryTime++;
        }
        if (enemyFactoryTime == 100)
        {
            Board[factoryLocation[1, 0], factoryLocation[1, 1] + 1] = kariKoma;
            paint(factoryLocation[1, 0], factoryLocation[1, 1] + 1);
            baseM.OnTimeBar(factoryLocation[1, 0], factoryLocation[1, 1] + 1);
            enemyFactoryTime++;
        }
        if (arrangeMode)
        {
            if (bl == -1) return;
            if (inputKoma(kariKomaNum) == skytree)
            {
                barrier[1, 0] = bl;
                barrier[1, 1] = br;
            }
            else if (inputKoma(kariKomaNum) == factory)
            {
                factoryLocation[1, 0] = bl;
                factoryLocation[1, 1] = br;
            }
            kariKoma = inputKoma(kariKomaNum + 7);
            Board[bl, br] = kariKoma;
            paint(bl, br);
            reset();
        }
    }

    //駒の配置終了、戦闘へ
    public void arrangeOffDo()
    {
        if (!humanExist) return;
        Everyplay.StartRecording();
        arrangeMode = false;
        baseM.setColorClear();
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                komaTime[i, j] = 0;
                komaSlider[i, j].maxValue = 60;
                EkomaTime[i, j] = 0;
                EkomaSlider[i, j].maxValue = 60;
                if (Board[i + 1, j + 1] != empty)
                {
                    if(Board[i + 1, j + 1].komaNumber < halfOfKoma)
                    {
                        baseM.OnTimeBar(i + 1, j + 1);
                    }
                    else
                    {
                        baseM.OnTimeBar2(i + 1, j + 1);
                    }
                }
            }
        }
        baseM.setColorWhite();
        for(int i = 0; i < 6; i++)
            {
                baseM.printColorBottom(i + 1, 5);
                baseM.printColorBottom(i + 1, 6);
            }
        paintEnemyArea();
        kariKomaNum = -1;
        baseM.canTouchAll(true);
    }

    public void paintEnemyArea(){
        for(int i = 0;i<2;i++){
            for(int j=0;j<6;j++){
                paint(j+1,i+1);
            }
        }
    }

    public void win()
    {
        GameObject winLoseObj = (GameObject)Resources.Load("Prefab/PartOfCanvas/winLoseShow");
        GameObject canvas = GameObject.Find("CanvasPlaying");
        GameObject prefab = (GameObject)Instantiate(winLoseObj);
        prefab.transform.SetParent(canvas.transform, false);
        Text winLoseText = GameObject.Find("CanvasPlaying/winLoseShow(Clone)/Text").GetComponent<Text>();
        winLoseText.text = "Win!";
    }

    public void lose()
    {
        GameObject winLoseObj = (GameObject)Resources.Load("Prefab/PartOfCanvas/winLoseShow");
        GameObject canvas = GameObject.Find("CanvasPlaying");
        GameObject prefab = (GameObject)Instantiate(winLoseObj);
        prefab.transform.SetParent(canvas.transform, false);
        Text winLoseText = GameObject.Find("CanvasPlaying/winLoseShow(Clone)/Text").GetComponent<Text>();
        winLoseText.text = "Lose!";
    }

    //盤面の駒の初期化
    void setKoma()
    {
        bl = line;
        br = row;
        if(Board[line,row]!=empty){
            baseM.getResources(Board[line,row].resources);
        }
        Board[line, row] = kariKoma;
        paint(line, row);
        baseM.getResources(-kariKoma.resources);
        if (kariKoma == human)
        {
            if(humanPosition[0,0] != -1)
            {
                Board[humanPosition[0,0],humanPosition[0,1]]=empty;
                paint(humanPosition[0,0],humanPosition[0,1]);
            }
            humanExist = true;
            humanPosition[0,0]=line;
            humanPosition[0,1]=row;
        }
        if (Board[line, row] == skytree)
        {
            barrier[0, 0] = line;
            barrier[0, 1] = row;
        }
        else if (Board[line, row] == factory)
        {
            factoryLocation[0, 0] = line;
            factoryLocation[0, 1] = row;
        }
    }

    public void setKomaNumber(int komaNumber)
    {
        if (arrangeMode)
        {
            if (komaNumber == 20)
            {
                arrangeOffDo();
                return;
            }
            kariKoma = inputKoma(komaNumber);
            if (kariKoma.resources > myResources) return;
            baseM.setColorRed();
            for(int i = 0; i < 6; i++)
            {
                baseM.printColorBottom(i + 1, 5);
                baseM.printColorBottom(i + 1, 6);
            }
            baseM.canTouchAll(true);
        }
        else
        {
            Produce(komaNumber);
        }
    }

    //ホストとクライアントどちらなのかで分岐
    public void whichPlayer()
    {
        if (arrangeMode)
        {
            if (!canTouchPlace[line, row]) return;
            if (kariKoma.resources > myResources) return;
            setKoma();
            bl = -1;
            br = -1;
            baseM.setColorWhite();
            baseM.canTouchAll(false);
            for (int i = 0; i < 6; i++)
            {
                baseM.printColorBottom(i + 1, 5);
                baseM.printColorBottom(i + 1, 6);
            }
            return;
        }
        processKoma();
    }

    //駒を動かすプロセス
    public void processKoma()
    {
        //そのクリックしたマスがクリック可能かどうか
        if (!canTouchPlace[line, row])
        {
            reset();
            baseM.canTouchAll(true);
            baseM.setRedWhite();
            return;
        }
        //駒の選択か移動先の指定かどうか判定
        if (bl == -1)
        {
            if (komaTime[line - 1, row - 1] != komaSlider[line - 1, row - 1].maxValue) return;
            if (!broken[line - 1, row - 1]) return;
            bl = line;
            br = row;
            baseM.canTouchAll(false);
            baseM.setColorRed();
            Board[bl, br].Move(bl, br);
        }
        else
        {
            al = line;
            ar = row;
            winLose = baseM.judge();
            Board[bl, br].Action(Board[al, ar].resources);
            //Board[al, ar].Draw(al, ar);
            if(winLose)Everyplay.StopRecording();
            baseM.setRedWhite();
            reset();
            baseM.canTouchAll(true);
        }
    }
   
    //その駒の時間にセットし０にする
    public void reTime()
    {
        if (Board[al, ar].komaNumber < halfOfKoma)
        {
            baseM.setColorClear();
            baseM.OffTimeBar(bl, br);
            baseM.OffTimeBar2(al,ar);
            baseM.OnTimeBar(al, ar);
            komaTime[al - 1, ar - 1] = 0;
            komaSlider[al - 1, ar - 1].maxValue = Board[al, ar].komaMaxTime;
        }else
        {
            baseM.setColorClear();
            baseM.OffTimeBar2(bl, br);
            baseM.OffTimeBar(al,ar);
            baseM.OnTimeBar2(al, ar);
            EkomaTime[al - 1, ar - 1] = 0;
            EkomaSlider[al - 1, ar - 1].maxValue = Board[al, ar].komaMaxTime;
            EkomaSlider[al - 1, ar - 1].value = 0;
        }
    }

    //ミサイルの場合の時間設定
    void timeChangeMissile()
    {
        komaTime[bl - 1, br - 1] = 0;
        komaSlider[bl - 1, br - 1].maxValue = 80;
    }

    //工場の場合の時間設定
    void timeChangeFactory()
    {
        komaTime[factoryLocation[0, 0] - 1, factoryLocation[0, 1] - 1] = 0;
        komaSlider[factoryLocation[0, 0] - 1, factoryLocation[0, 1] - 1].maxValue = 100;
        myFactoryTime = 0;
    }

    public void drawBarrier()
    {
        if (Board[bl, br].komaNumber > 7)
        {
            if (barrier[0, 0] == -1) return;
            barrierBlueToWhite(barrier[0, 0], barrier[0, 1]);
        }
        else
        {
            if (barrier[1, 0] == -1) return;
            barrierBlueToWhite(barrier[1, 0], barrier[1, 1]);
        }
    }

    //上のマスに駒をプリントする
    public void paint(int l, int r)
    {
        if (Board[l, r] == empty)
        {
            baseM.setColorClear();
        }
        else
        {
            baseM.setColorWhite();
        }
        GameObject.Find("CanvasPlaying/pixel " + l + "-" + r).GetComponent<Image>().color
            = new Color(changeRed, changeGreen, changeBlue, changeAlpha);
        img = GameObject.Find("CanvasPlaying/pixel " + l + "-" + r).GetComponent<Image>();
        if (broken[l-1,r-1])
        {
            img.sprite = Board[l, r].komaView;
        }else
        {
            img.sprite = Board[l, r].brokeKomaView;
        }
    }
    
    //駒の移動後の盤面を描きなおす
    public void rePaint()
    {
        paint(bl, br);
        paint(al, ar);
    }

    //クリック前後のスプライトの変更
    public void changeSprite()
    {
        Board[al, ar] = Board[bl, br];
        Board[bl, br] = empty;
        broken[al - 1, ar - 1] = true;
    }
    
    //駒にそれぞれのスプライトを代入
    Koma inputKoma(int komaNum)
    {
        switch (komaNum)
        {
            case 14:
                return Eskytree;
            case 13:
                return Edrone;
            case 12:
                return Efactory;
            case 11:
                return Emissile;
            case 10:
                return EsRobot;
            case 9:
                return EgRobot;
            case 8:
                return Ehuman;
            case 7:
                return skytree;
            case 6:
                return drone;
            case 5:
                return factory;
            case 4:
                return missile;
            case 3:
                return sRobot;
            case 2:
                return gRobot;
            case 1:
                return human;
            case 0:
                return empty;
        }
        return empty;
    }
    
    //味方のミサイル攻撃
    public void atackMissile()
    {
        if (satelite)
        {
            baseM.canTouchAll(true);
            baseM.allPrintColorBottom();
            if (barrier[1, 0] == -1) return;
            barrierPoint(barrier[1, 0], barrier[1, 1]);
        }
        else
        {
            baseM.canTouchHalf();
            baseM.halfPrintColorBottom(false);
        }
    }

    //敵のミサイル攻撃
    public void atackEMissile()
    {
        if (Esatelite)
        {
            baseM.canTouchAll(true);
            baseM.allPrintColorBottom();
            if (barrier[0, 0] == -1) return;
            barrierPoint(barrier[0, 0], barrier[0, 1]);
        }
        else
        {
            baseM.canTouchHalf();
            baseM.halfPrintColorBottom(true);
        }
    }

    //ミサイル攻撃によって駒を破壊する
    public void destroyByMissile()
    {
        broken[al - 1, ar - 1] = false;
        paint(al, ar);
        timeChangeMissile();
    }

    //ミサイル攻撃に対するバリアを張る
    void barrierPoint(int l, int r)
    {
        baseM.setColorBlue();
        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                if (0 != l + i && l + i != 7 && 0 != r + j && r + j != 7)
                {
                    canTouchPlace[l + i, r + j] = false;
                    baseM.printColorBottom(l + i, r + j);
                }
            }
        }
    }

    //ミサイル攻撃に対するバリアを張る
    void barrierBlueToWhite(int l, int r)
    {
        baseM.setColorWhite();
        for (int i = -1; i < 2; i++)
        {
            for (int j = -1; j < 2; j++)
            {
                if (0 != l + i && l + i != 7 && 0 != r + j && r + j != 7)
                {
                    baseM.printColorBottom(l + i, r + j);
                }
            }
        }
    }

    //移動先に駒があるかどうか返す
    public void isThereKoma(int l, int r)
    {
        if (Board[l, r] == empty)
        {
            canTouchPlace[l, r] = true;
            baseM.printColorBottom(l, r);
            return;
        }
        if(Board[l,r] == futi)
        {
            wall = false;
            return;
        }
        if (Board[l, r].komaNumber > 7 && !enemy)
        {
            canTouchPlace[l, r] = true;
            baseM.printColorBottom(l, r);
            wall = false;
            return;
        }
        if (Board[l, r].komaNumber < halfOfKoma && enemy)
        {
            canTouchPlace[l, r] = true;
            baseM.printColorBottom(l, r);
            wall = false;
            return;
        }
        if (broken[l - 1, r - 1] == false)
        {
            canTouchPlace[l, r] = true;
            baseM.printColorBottom(l, r);
            wall = false;
            return;
        }
        wall = false;
    }

    //移動先に駒があるかどうか返す
    public void isThereFuti(int l, int r)
    {
        if (Board[l, r] == futi)
        {
            wall = false;
        }
        else
        {
            canTouchPlace[l, r] = true;
            baseM.printColorBottom(l, r);
        }
    }

    //移動先に駒があるかどうか返す
    public void isThereDraw(int l, int r)
    {
        if (Board[l, r] == futi)
        {
            wall = false;
        }
        else
        {
            paint(l, r);
            baseM.setColorWhite();
            baseM.printColorBottom(l, r);
        }
    }

    //移動先に駒があるかどうか返す
    public void isThereErase(int l, int r)
    {
        if (Board[l, r] == futi)
        {
            wall = false;
        }
        else
        {
            baseM.setColorClear();
            baseM.printColorAbove(l, r);
            baseM.printColorBottom(l, r);
        }
    }
    
    //クリック前と後の行列をー１にリセットする
    public void reset()
    {
        bl = -1;
        br = -1;
        al = -1;
        ar = -1;
    }

    //ゴールデンロボットの生産
    public void ProduceGRobot()
    {
        if (factoryLocation[0, 0] == -1) return;
        if (Board[factoryLocation[0, 0], factoryLocation[0, 1] - 1] == empty && myResources >= 5)
        {
            baseM.getMyResources(-5);
            timeChangeFactory();
            kariKoma = gRobot;
        }
    }

    //シルバーロボットの生産
    public void ProduceSRobot()
    {
        if (factoryLocation[0, 0] == -1) return;
        if (Board[factoryLocation[0, 0], factoryLocation[0, 1] - 1] == empty && myResources >= 4)
        {
            baseM.getMyResources(-4);
            timeChangeFactory();
            kariKoma = sRobot;
        }
    }

    //ドローンの生産
    public void ProduceDrone()
    {
        if (factoryLocation[0, 0] == -1) return;
        if (Board[factoryLocation[0, 0], factoryLocation[0, 1] - 1] == empty && myResources >= 3)
        {
            baseM.getMyResources(-3);
            timeChangeFactory();
            kariKoma = drone;
        }
    }

    public void Produce(int komaNum)
    {
        if (factoryLocation[0, 0] == -1) return;
        kariKoma = inputKoma(komaNum);
        if (Board[factoryLocation[0, 0], factoryLocation[0, 1] - 1] == empty && myResources >= kariKoma.resources)
        {
            baseM.getMyResources(-kariKoma.resources);
            timeChangeFactory();
        }
    }
}