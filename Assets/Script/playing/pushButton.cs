﻿using UnityEngine;
using System.Collections;

public class pushButton : MonoBehaviour {

    GameObject refObj;
    public int line;
    public int row;

	// Use this for initialization
	void Start () {
        refObj = GameObject.Find("GameManeger");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnClick()
    {
        banmen b = refObj.GetComponent<banmen>();
        b.baseM.lineRow(line, row);
        b.whichPlayer();
    }

    public void OnPointerEnter()
    {
        banmen b = refObj.GetComponent<banmen>();
        b.baseM.setColorBlack();
        b.baseM.printColorAbove(line,row);
    }

    public void OnPointerExit()
    {
        
    }
}
