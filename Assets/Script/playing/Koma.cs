﻿using UnityEngine;
using System.Collections;

public class Koma {
    public banmen ban;
    public Sprite komaView;
    public int komaNumber;
    public int resources;
    public Sprite brokeKomaView;
    public int komaMaxTime;

    public virtual void Init(string str)
    {
        ban = GameObject.Find(str).GetComponent<banmen>();
    }
    public virtual void Move(int l, int r)
    {
    }
    public virtual void Draw(int l, int r)
    {
    }
    public virtual void Erase(int l, int r)
    {
    }
    public virtual void Action(int resources)
    {
        ban.baseM.getResources(resources);
        ban.changeSprite();
        ban.reTime();
        ban.rePaint();
    }
}
public class Empty : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/white");
        base.komaNumber = 0;
        base.resources = 0;
    }
    public override void Move(int l, int r)
    {
        base.ban.reset();
        base.ban.baseM.canTouchAll(true);
    }
}

public class Human : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/friend/human");
        base.komaNumber = 1;
        base.komaMaxTime = 100;
    }
    public override void Move(int l, int r)
    {
        base.ban.isThereKoma(l, r - 1);
        base.ban.isThereKoma(l - 1, r - 1);
        base.ban.isThereKoma(l + 1, r - 1);
        base.ban.isThereKoma(l + 1, r);
        base.ban.isThereKoma(l - 1, r);
        base.ban.isThereKoma(l, r + 1);
        base.ban.isThereKoma(l - 1, r + 1);
        base.ban.isThereKoma(l + 1, r + 1);
    }
    public override void Draw(int l, int r)
    {
        base.ban.isThereDraw(l, r - 1);
        base.ban.isThereDraw(l - 1, r - 1);
        base.ban.isThereDraw(l + 1, r - 1);
        base.ban.isThereDraw(l + 1, r);
        base.ban.isThereDraw(l - 1, r);
        base.ban.isThereDraw(l, r + 1);
        base.ban.isThereDraw(l - 1, r + 1);
        base.ban.isThereDraw(l + 1, r + 1);
        base.ban.wall = true;
    }
    public override void Erase(int l, int r)
    {
        base.ban.isThereErase(l, r - 1);
        base.ban.isThereErase(l - 1, r - 1);
        base.ban.isThereErase(l + 1, r - 1);
        base.ban.isThereErase(l + 1, r);
        base.ban.isThereErase(l - 1, r);
        base.ban.isThereErase(l, r + 1);
        base.ban.isThereErase(l - 1, r + 1);
        base.ban.isThereErase(l + 1, r + 1);
        base.ban.wall = true;
    }
}

public class GRobot : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/friend/goldenRobot");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/friend/goldenRobotBroken");
        base.komaNumber = 2;
        base.resources = 5;
        base.komaMaxTime = 50;
    }
    public override void Move(int l, int r)
    {
        base.ban.isThereKoma(l, r - 1);
        base.ban.isThereKoma(l - 1, r - 1);
        base.ban.isThereKoma(l + 1, r - 1);
        base.ban.isThereKoma(l + 1, r);
        base.ban.isThereKoma(l - 1, r);
        base.ban.isThereKoma(l, r + 1);
    }
    public override void Draw(int l, int r)
    {
        base.ban.isThereDraw(l, r - 1);
        base.ban.isThereDraw(l - 1, r - 1);
        base.ban.isThereDraw(l + 1, r - 1);
        base.ban.isThereDraw(l + 1, r);
        base.ban.isThereDraw(l - 1, r);
        base.ban.isThereDraw(l, r + 1);
        base.ban.wall = true;
    }
    public override void Erase(int l, int r)
    {
        base.ban.isThereErase(l, r - 1);
        base.ban.isThereErase(l - 1, r - 1);
        base.ban.isThereErase(l + 1, r - 1);
        base.ban.isThereErase(l + 1, r);
        base.ban.isThereErase(l - 1, r);
        base.ban.isThereErase(l, r + 1);
        base.ban.wall = true;
    }
}

public class SRobot : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/friend/silverRobot");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/friend/silverRobotBroken");
        base.komaNumber = 3;
        base.resources = 4;
        base.komaMaxTime = 40;
    }
    public override void Move(int l, int r)
    {
        base.ban.isThereKoma(l, r - 1);
        base.ban.isThereKoma(l - 1, r - 1);
        base.ban.isThereKoma(l + 1, r - 1);
        base.ban.isThereKoma(l - 1, r + 1);
        base.ban.isThereKoma(l + 1, r + 1);
    }
    public override void Draw(int l, int r)
    {
        base.ban.isThereDraw(l, r - 1);
        base.ban.isThereDraw(l - 1, r - 1);
        base.ban.isThereDraw(l + 1, r - 1);
        base.ban.isThereDraw(l - 1, r + 1);
        base.ban.isThereDraw(l + 1, r + 1);
        base.ban.wall = true;
    }
    public override void Erase(int l, int r)
    {
        base.ban.isThereErase(l, r - 1);
        base.ban.isThereErase(l - 1, r - 1);
        base.ban.isThereErase(l + 1, r - 1);
        base.ban.isThereErase(l - 1, r + 1);
        base.ban.isThereErase(l + 1, r + 1);
        base.ban.wall = true;
    }
}

public class Missile : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/friend/missile");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/friend/missileBroken");
        base.komaNumber = 4;
        base.resources = 8;
        base.komaMaxTime = 80;
    }
    public override void Move(int l, int r)
    {
        base.ban.atackMissile();
    }
    public override void Action(int resource)
    {
        base.ban.destroyByMissile();
        base.ban.drawBarrier();
    }
}

public class Factory : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/friend/factory");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/friend/factoryBroken");
        base.komaNumber = 5;
        base.resources = 10;
        base.komaMaxTime = 100;
    }
}

public class Drone : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/friend/drone");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/friend/droneBroken");
        base.komaNumber = 6;
        base.resources = 3;
        base.komaMaxTime = 30;
    }
    public override void Move(int l, int r)
    {
        base.ban.wall = true;
        int i = 1;
        while (base.ban.wall)
        {
            base.ban.isThereFuti(l, r - i);
            i++;
        }
        i = 1;
        base.ban.wall = true;
        while (base.ban.wall)
        {
            base.ban.isThereFuti(l, r + i);
            i++;
        }
        i = 1;
        base.ban.wall = true;
        while (base.ban.wall)
        {
            base.ban.isThereFuti(l + i, r);
            i++;
        }
        i = 1;
        base.ban.wall = true;
        while (base.ban.wall)
        {
            base.ban.isThereFuti(l - i, r);
            i++;
        }
        base.ban.wall = true;
    }
}

public class Skytree : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/friend/skytree");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/friend/skytreeBroken");
        base.komaNumber = 7;
        base.resources = 10;
    }
}

public class EHuman : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/Enemy/Ehuman");
        base.komaNumber = 8;
        base.resources = 5;
        base.komaMaxTime = 100;
    }
    public override void Move(int l, int r)
    {
        base.ban.enemy = true;
        base.ban.isThereKoma(l, r - 1);
        base.ban.isThereKoma(l - 1, r - 1);
        base.ban.isThereKoma(l + 1, r - 1);
        base.ban.isThereKoma(l + 1, r);
        base.ban.isThereKoma(l - 1, r);
        base.ban.isThereKoma(l, r + 1);
        base.ban.isThereKoma(l - 1, r + 1);
        base.ban.isThereKoma(l + 1, r + 1);
        base.ban.enemy = false;
    }
    public override void Draw(int l, int r)
    {
        base.ban.enemy = true;
        base.ban.isThereDraw(l, r - 1);
        base.ban.isThereDraw(l - 1, r - 1);
        base.ban.isThereDraw(l + 1, r - 1);
        base.ban.isThereDraw(l + 1, r);
        base.ban.isThereDraw(l - 1, r);
        base.ban.isThereDraw(l, r + 1);
        base.ban.isThereDraw(l - 1, r + 1);
        base.ban.isThereDraw(l + 1, r + 1);
        base.ban.enemy = false;
    }
    public override void Erase(int l, int r)
    {
        base.ban.enemy = true;
        base.ban.isThereErase(l, r - 1);
        base.ban.isThereErase(l - 1, r - 1);
        base.ban.isThereErase(l + 1, r - 1);
        base.ban.isThereErase(l + 1, r);
        base.ban.isThereErase(l - 1, r);
        base.ban.isThereErase(l, r + 1);
        base.ban.isThereErase(l - 1, r + 1);
        base.ban.isThereErase(l + 1, r + 1);
        base.ban.wall = true;
        base.ban.enemy = false;
    }
}

public class EGRobot : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/Enemy/EgoldenRobot");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/Enemy/EgoldenRobotBroken");
        base.komaNumber = 9;
        base.resources = 5;
        base.komaMaxTime = 50;
    }
    public override void Move(int l, int r)
    {
        base.ban.enemy = true;
        base.ban.isThereKoma(l, r - 1);
        base.ban.isThereKoma(l - 1, r + 1);
        base.ban.isThereKoma(l + 1, r + 1);
        base.ban.isThereKoma(l + 1, r);
        base.ban.isThereKoma(l - 1, r);
        base.ban.isThereKoma(l, r + 1);
        base.ban.enemy = false;
    }
    public override void Draw(int l, int r)
    {
        base.ban.enemy = true;
        base.ban.isThereDraw(l, r - 1);
        base.ban.isThereDraw(l - 1, r + 1);
        base.ban.isThereDraw(l + 1, r + 1);
        base.ban.isThereDraw(l + 1, r);
        base.ban.isThereDraw(l - 1, r);
        base.ban.isThereDraw(l, r + 1);
        base.ban.enemy = false;
    }
    public override void Erase(int l, int r)
    {
        base.ban.enemy = true;
        base.ban.isThereErase(l, r - 1);
        base.ban.isThereErase(l - 1, r + 1);
        base.ban.isThereErase(l + 1, r + 1);
        base.ban.isThereErase(l + 1, r);
        base.ban.isThereErase(l - 1, r);
        base.ban.isThereErase(l, r + 1);
        base.ban.enemy = false;
    }
}

public class ESRobot : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/Enemy/EsilverRobot");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/Enemy/EsilverRobotBroken");
        base.komaNumber = 10;
        base.resources = 4;
        base.komaMaxTime = 40;
    }

    public override void Move(int l, int r)
    {
        base.ban.enemy = true;
        base.ban.isThereKoma(l, r + 1);
        base.ban.isThereKoma(l - 1, r - 1);
        base.ban.isThereKoma(l + 1, r - 1);
        base.ban.isThereKoma(l - 1, r + 1);
        base.ban.isThereKoma(l + 1, r + 1);
        base.ban.enemy = false;
    }
    public override void Draw(int l, int r)
    {
        base.ban.enemy = true;
        base.ban.isThereDraw(l, r + 1);
        base.ban.isThereDraw(l - 1, r - 1);
        base.ban.isThereDraw(l + 1, r - 1);
        base.ban.isThereDraw(l - 1, r + 1);
        base.ban.isThereDraw(l + 1, r + 1);
        base.ban.enemy = false;
    }
    public override void Erase(int l, int r)
    {
        base.ban.enemy = true;
        base.ban.isThereErase(l, r + 1);
        base.ban.isThereErase(l - 1, r - 1);
        base.ban.isThereErase(l + 1, r - 1);
        base.ban.isThereErase(l - 1, r + 1);
        base.ban.isThereErase(l + 1, r + 1);
        base.ban.enemy = false;
    }
}

public class EMissile : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/Enemy/Emissile");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/Enemy/EmissileBroken");
        base.komaNumber = 11;
        base.resources = 8;
        base.komaMaxTime = 80;
    }
    public override void Move(int l, int r)
    {
        base.ban.atackEMissile();
    }
    public override void Action(int resource)
    {
        base.ban.destroyByMissile();
        base.ban.drawBarrier();
    }
}

public class EFactory : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/Enemy/Efactory");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/Enemy/EfactoryBroken");
        base.komaNumber = 12;
        base.resources = 10;
        base.komaMaxTime = 100;
    }
}

public class EDrone : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/Enemy/Edrone");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/friend/droneBroken");
        base.komaNumber = 13;
        base.resources = 3;
        base.komaMaxTime = 30;
    }
    public override void Move(int l, int r)
    {
        base.ban.enemy = true;
        base.ban.wall = true;
        int i = 1;
        while (base.ban.wall)
        {
            base.ban.isThereFuti(l, r - i);
            i++;
        }
        i = 1;
        base.ban.wall = true;
        while (base.ban.wall)
        {
            base.ban.isThereFuti(l, r + i);
            i++;
        }
        i = 1;
        base.ban.wall = true;
        while (base.ban.wall)
        {
            base.ban.isThereFuti(l + i, r);
            i++;
        }
        i = 1;
        base.ban.wall = true;
        while (base.ban.wall)
        {
            base.ban.isThereFuti(l - i, r);
            i++;
        }
        base.ban.wall = true;
        base.ban.enemy = false;
    }
}

public class ESkytree : Koma
{
    public override void Init(string str)
    {
        base.Init(str);
        base.komaView = Resources.Load<Sprite>("Sprite/Enemy/Eskytree");
        base.brokeKomaView = Resources.Load<Sprite>("Sprite/Enemy/EskytreeBroken");
        base.komaNumber = 14;
        base.resources = 10;
    }
}

public class Futi : Koma
{
    void Start()
    {

    }
}