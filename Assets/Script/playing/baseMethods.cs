﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class baseMethods
{
    banmen ban; //= GameObject.Find("banmenScript").GetComponent<banmen>();
    GameObject refObj;
    Singleton singleton;
    GameObject prefab;
    GameObject winLoseObj;

    public void Init(string str)
    {
        ban = GameObject.Find(str).GetComponent<banmen>();
    }

    //すべての上のマスに駒をペイントする
    public void allPaint()
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (ban.Board[i + 1, j + 1] != ban.empty)
                {
                    ban.paint(i + 1, j + 1);
                }
            }
        }
    }

    //勝敗の判定
    public bool judge()
    {
        if (ban.Board[ban.al, ban.ar].komaNumber == 1)
        {
            ban.lose();
            refObj = GameObject.Find("singleton");
            singleton = refObj.GetComponent<Singleton>();
            singleton.CountWinLose(false);
            singleton.Save();
            return true;
        }
        else if (ban.Board[ban.al, ban.ar].komaNumber == 8)
        {
            ban.win();
            refObj = GameObject.Find("singleton");
            singleton = refObj.GetComponent<Singleton>();
            singleton.CountWinLose(true);
            singleton.Save();
            return true;
        }
        return false;
    }

    public void getResources(int resource)
    {
        if (ban.Board[ban.bl, ban.br].komaNumber < ban.halfOfKoma)
        {
            getMyResources(resource);
        }
        else
        {
            getEResources(resource);
        }
    }

    //味方の資源の増減
    public void getMyResources(int resource)
    {
        ban.text = GameObject.Find("CanvasPlaying/Resources").GetComponent<Text>();
        ban.myResources = ban.myResources + resource;
        ban.text.text = " " + ban.myResources;
    }

    //敵の資源の増減
    public void getEResources(int resource)
    {
        ban.text = GameObject.Find("CanvasPlaying/EResources").GetComponent<Text>();
        ban.enemyResources = ban.enemyResources + resource;
        ban.text.text = " " + ban.enemyResources;
    }

    //待機時間のバーを消す
    public void OffTimeBar(int l, int r)
    {
        GameObject.Find("CanvasPlaying/Slider " + l + "-" + r + "/Background").GetComponent<Image>().color = new Color(ban.changeRed, ban.changeGreen, ban.changeBlue, ban.changeAlpha);
        GameObject.Find("CanvasPlaying/Slider " + l + "-" + r + "/Fill Area/Fill").GetComponent<Image>().color = new Color(ban.changeRed, ban.changeGreen, ban.changeBlue, ban.changeAlpha);
    }

    //待機時間のバーを消す
    public void OffTimeBar2(int l, int r)
    {
        GameObject.Find("CanvasPlaying/Slider " + l + "-" + r + " (1)/Background").GetComponent<Image>().color = new Color(ban.changeRed, ban.changeGreen, ban.changeBlue, ban.changeAlpha);
        GameObject.Find("CanvasPlaying/Slider " + l + "-" + r + " (1)/Fill Area/Fill").GetComponent<Image>().color = new Color(ban.changeRed, ban.changeGreen, ban.changeBlue, ban.changeAlpha);
    }

    //待機時間のバーを表示する
    public void OnTimeBar(int l, int r)
    {
        setColorBlack();
        GameObject.Find("CanvasPlaying/Slider " + l + "-" + r + "/Background").GetComponent<Image>().color = new Color(ban.changeRed, ban.changeGreen, ban.changeBlue, ban.changeAlpha);
        setColorGreen();
        GameObject.Find("CanvasPlaying/Slider " + l + "-" + r + "/Fill Area/Fill").GetComponent<Image>().color = new Color(ban.changeRed, ban.changeGreen, ban.changeBlue, ban.changeAlpha);
    }

    //待機時間のバーを表示する
    public void OnTimeBar2(int l, int r)
    {
        setColorBlack();
        GameObject.Find("CanvasPlaying/Slider " + l + "-" + r + " (1)/Background").GetComponent<Image>().color = new Color(ban.changeRed, ban.changeGreen, ban.changeBlue, ban.changeAlpha);
        setColorGreen();
        GameObject.Find("CanvasPlaying/Slider " + l + "-" + r + " (1)/Fill Area/Fill").GetComponent<Image>().color = new Color(ban.changeRed, ban.changeGreen, ban.changeBlue, ban.changeAlpha);
    }

    //下のマスのカラーをプリント
    public void printColorBottom(int l, int r)
    {
        GameObject.Find("CanvasPlaying/pixel " + l + "-" + r + " (1)").GetComponent<Image>().color
            = new Color(ban.changeRed, ban.changeGreen, ban.changeBlue, ban.changeAlpha);
    }

    //上のマスのカラーをプリント
    public void printColorAbove(int l, int r)
    {
        GameObject.Find("CanvasPlaying/pixel " + l + "-" + r).GetComponent<Image>().color
            = new Color(ban.changeRed, ban.changeGreen, ban.changeBlue, ban.changeAlpha);
    }

    //下のマスのすべての色をプリントする
    public void allPrintColorBottom()
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                printColorBottom(i + 1, j + 1);
            }
        }
    }

    //盤面半分の下のマスの色をプリントする
    public void halfPrintColorBottom(bool e)
    {
        if (e)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    printColorBottom(i + 1, j + 1);
                }
            }
        }
        else
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    printColorBottom(i + 1, j + 4);
                }
            }
        }
    }

    //カラーの色を白にする
    public void setColorWhite()
    {
        ban.changeRed = 1.0f;
        ban.changeGreen = 1.0f;
        ban.changeBlue = 1.0f;
        ban.changeAlpha = 1.0f;
    }

    //カラーの色を黒にする
    public void setColorBlack()
    {
        ban.changeRed = 0.0f;
        ban.changeGreen = 0.0f;
        ban.changeBlue = 0.0f;
        ban.changeAlpha = 1.0f;
    }

    //カラーの色を赤色にする
    public void setColorRed()
    {
        ban.changeRed = 1.0f;
        ban.changeGreen = 0.0f;
        ban.changeBlue = 0.0f;
        ban.changeAlpha = 1.0f;
    }

    //カラーの色をブルーにする
    public void setColorBlue()
    {
        ban.changeRed = 0.0f;
        ban.changeGreen = 0.0f;
        ban.changeBlue = 1.0f;
        ban.changeAlpha = 1.0f;
    }

    //カラーの色を緑にする
    public void setColorGreen()
    {
        ban.changeRed = 0.0f;
        ban.changeGreen = 1.0f;
        ban.changeBlue = 0.0f;
        ban.changeAlpha = 1.0f;
    }

    //カラーの色をクリアにする
    public void setColorClear()
    {
        ban.changeAlpha = 0.0f;
    }

    //盤面の赤マスだけを白く塗りつぶす
    public void setRedWhite()
    {
        setColorWhite();
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (ban.canTouchPlace[i + 1, j + 1])
                {
                    printColorBottom(i + 1, j + 1);
                }

            }
        }
    }
    //行と列の値を代入
    public void lineRow(int l, int r)
    {
        ban.line = l;
        ban.row = r;
    }

    //すべてのマスをタッチできるようにする
    public void canTouchAll(bool TF)
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                ban.canTouchPlace[i + 1, j + 1] = TF;
            }
        }
    }

    //すべてのマスをタッチできるようにする
    public void canTouchArrangeMode()
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                ban.canTouchPlace[i + 1, j + 1] = false;
            }
            for (int k = 0; k < 2; k++)
            {
                ban.canTouchPlace[i + 1, k + 5] = true;
            }
        }
    }

    //すべてのマスをタッチできるようにする
    public void canTouchHalf()
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                ban.canTouchPlace[i + 1, j + 4] = true;
            }
        }
    }
    public void cloudy()
    {
        ClearEPlace();
        GameObject.Find("CanvasPlaying/banmenUp").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprite/cloudy");
    }

    //衛星がない状態
    public void nonSatelite()
    {
        ClearEPlace();
        setColorClear();
        GameObject.Find("CanvasPlaying/banmenUp").GetComponent<Image>().color
            = new Color(ban.changeRed, ban.changeGreen, ban.changeBlue, ban.changeAlpha);
    }

    //敵エリアを見えなくする
    public void ClearEPlace()
    {
        setColorClear();
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                printColorBottom(j + 1, i + 1);
                printColorAbove(j + 1, i + 1);
                OffTimeBar(j + 1, i + 1);
            }
        }
    }

    //衛星を使える状態にする
    public void OnSatelite()
    {
        ban.satelite = true;
    }

    //衛星を壊す
    public void DestroySatelite()
    {
        ban.Esatelite = false;
    }
}
