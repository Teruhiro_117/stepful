﻿using UnityEngine;
using System.Collections;

public class soundOnOff : MonoBehaviour {

    GameObject obj;
    bool onOff;
	// Use this for initialization
	void Start () {
        obj = GameObject.Find("singleton");
        onOff = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnClick()
    {
        Singleton b = obj.GetComponent<Singleton>();
        if (onOff)
        {
            b.soundOn();
            onOff = false;
        }else
        {
            b.soundOff();
            onOff = true;
        }
    }
}
