﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Factory
struct Factory_t931158954;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Factory::.ctor()
extern "C"  void Factory__ctor_m2197654087 (Factory_t931158954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Factory::Init(System.String)
extern "C"  void Factory_Init_m1507447763 (Factory_t931158954 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
