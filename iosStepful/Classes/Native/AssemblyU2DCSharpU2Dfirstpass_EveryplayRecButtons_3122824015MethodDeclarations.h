﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayRecButtons/ButtonTapped
struct ButtonTapped_t3122824015;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void EveryplayRecButtons/ButtonTapped::.ctor(System.Object,System.IntPtr)
extern "C"  void ButtonTapped__ctor_m207266678 (ButtonTapped_t3122824015 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons/ButtonTapped::Invoke()
extern "C"  void ButtonTapped_Invoke_m1052892676 (ButtonTapped_t3122824015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult EveryplayRecButtons/ButtonTapped::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ButtonTapped_BeginInvoke_m331452055 (ButtonTapped_t3122824015 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons/ButtonTapped::EndInvoke(System.IAsyncResult)
extern "C"  void ButtonTapped_EndInvoke_m1076891736 (ButtonTapped_t3122824015 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
