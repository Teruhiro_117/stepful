﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// banmen
struct banmen_t2510752313;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Singleton
struct Singleton_t1770047133;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// baseMethods
struct  baseMethods_t2705876163  : public Il2CppObject
{
public:
	// banmen baseMethods::ban
	banmen_t2510752313 * ___ban_0;
	// UnityEngine.GameObject baseMethods::refObj
	GameObject_t1756533147 * ___refObj_1;
	// Singleton baseMethods::singleton
	Singleton_t1770047133 * ___singleton_2;
	// UnityEngine.GameObject baseMethods::prefab
	GameObject_t1756533147 * ___prefab_3;
	// UnityEngine.GameObject baseMethods::winLoseObj
	GameObject_t1756533147 * ___winLoseObj_4;

public:
	inline static int32_t get_offset_of_ban_0() { return static_cast<int32_t>(offsetof(baseMethods_t2705876163, ___ban_0)); }
	inline banmen_t2510752313 * get_ban_0() const { return ___ban_0; }
	inline banmen_t2510752313 ** get_address_of_ban_0() { return &___ban_0; }
	inline void set_ban_0(banmen_t2510752313 * value)
	{
		___ban_0 = value;
		Il2CppCodeGenWriteBarrier(&___ban_0, value);
	}

	inline static int32_t get_offset_of_refObj_1() { return static_cast<int32_t>(offsetof(baseMethods_t2705876163, ___refObj_1)); }
	inline GameObject_t1756533147 * get_refObj_1() const { return ___refObj_1; }
	inline GameObject_t1756533147 ** get_address_of_refObj_1() { return &___refObj_1; }
	inline void set_refObj_1(GameObject_t1756533147 * value)
	{
		___refObj_1 = value;
		Il2CppCodeGenWriteBarrier(&___refObj_1, value);
	}

	inline static int32_t get_offset_of_singleton_2() { return static_cast<int32_t>(offsetof(baseMethods_t2705876163, ___singleton_2)); }
	inline Singleton_t1770047133 * get_singleton_2() const { return ___singleton_2; }
	inline Singleton_t1770047133 ** get_address_of_singleton_2() { return &___singleton_2; }
	inline void set_singleton_2(Singleton_t1770047133 * value)
	{
		___singleton_2 = value;
		Il2CppCodeGenWriteBarrier(&___singleton_2, value);
	}

	inline static int32_t get_offset_of_prefab_3() { return static_cast<int32_t>(offsetof(baseMethods_t2705876163, ___prefab_3)); }
	inline GameObject_t1756533147 * get_prefab_3() const { return ___prefab_3; }
	inline GameObject_t1756533147 ** get_address_of_prefab_3() { return &___prefab_3; }
	inline void set_prefab_3(GameObject_t1756533147 * value)
	{
		___prefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_3, value);
	}

	inline static int32_t get_offset_of_winLoseObj_4() { return static_cast<int32_t>(offsetof(baseMethods_t2705876163, ___winLoseObj_4)); }
	inline GameObject_t1756533147 * get_winLoseObj_4() const { return ___winLoseObj_4; }
	inline GameObject_t1756533147 ** get_address_of_winLoseObj_4() { return &___winLoseObj_4; }
	inline void set_winLoseObj_4(GameObject_t1756533147 * value)
	{
		___winLoseObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___winLoseObj_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
