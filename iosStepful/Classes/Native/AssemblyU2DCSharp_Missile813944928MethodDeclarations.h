﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Missile
struct Missile_t813944928;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Missile::.ctor()
extern "C"  void Missile__ctor_m2545921555 (Missile_t813944928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Missile::Init(System.String)
extern "C"  void Missile_Init_m2886599655 (Missile_t813944928 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Missile::Move(System.Int32,System.Int32)
extern "C"  void Missile_Move_m4106143890 (Missile_t813944928 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Missile::Action(System.Int32)
extern "C"  void Missile_Action_m1343903756 (Missile_t813944928 * __this, int32_t ___resource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
