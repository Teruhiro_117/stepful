﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct DefaultComparer_t3139025178;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1517095017.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.ctor()
extern "C"  void DefaultComparer__ctor_m4172767863_gshared (DefaultComparer_t3139025178 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4172767863(__this, method) ((  void (*) (DefaultComparer_t3139025178 *, const MethodInfo*))DefaultComparer__ctor_m4172767863_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3712991160_gshared (DefaultComparer_t3139025178 * __this, PendingPlayer_t1517095017  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3712991160(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3139025178 *, PendingPlayer_t1517095017 , const MethodInfo*))DefaultComparer_GetHashCode_m3712991160_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1476340500_gshared (DefaultComparer_t3139025178 * __this, PendingPlayer_t1517095017  ___x0, PendingPlayer_t1517095017  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1476340500(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3139025178 *, PendingPlayer_t1517095017 , PendingPlayer_t1517095017 , const MethodInfo*))DefaultComparer_Equals_m1476340500_gshared)(__this, ___x0, ___y1, method)
