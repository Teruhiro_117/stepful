﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// inputName
struct inputName_t1068137479;

#include "codegen/il2cpp-codegen.h"

// System.Void inputName::.ctor()
extern "C"  void inputName__ctor_m2866011510 (inputName_t1068137479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void inputName::Start()
extern "C"  void inputName_Start_m3658301090 (inputName_t1068137479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void inputName::Update()
extern "C"  void inputName_Update_m3174464491 (inputName_t1068137479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void inputName::OnClick()
extern "C"  void inputName_OnClick_m3622305209 (inputName_t1068137479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void inputName::SaveText()
extern "C"  void inputName_SaveText_m2989475918 (inputName_t1068137479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
