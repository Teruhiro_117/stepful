﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct List_1_t4188176625;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct IEnumerable_1_t816215242;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct IEnumerator_1_t2294579320;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct ICollection_1_t1476163502;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct ReadOnlyCollection_1_t709873889;
// UnityEngine.Networking.NetworkSystem.CRCMessageEntry[]
struct CRCMessageEntryU5BU5D_t2803924168;
// System.Predicate`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct Predicate_1_t3262025608;
// System.Collections.Generic.IComparer`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct IComparer_1_t2773518615;
// System.Comparison`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct Comparison_1_t1785827048;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo524088197.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3722906299.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.ctor()
extern "C"  void List_1__ctor_m1844386887_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1__ctor_m1844386887(__this, method) ((  void (*) (List_1_t4188176625 *, const MethodInfo*))List_1__ctor_m1844386887_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m578010036_gshared (List_1_t4188176625 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m578010036(__this, ___collection0, method) ((  void (*) (List_1_t4188176625 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m578010036_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2530551890_gshared (List_1_t4188176625 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2530551890(__this, ___capacity0, method) ((  void (*) (List_1_t4188176625 *, int32_t, const MethodInfo*))List_1__ctor_m2530551890_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.cctor()
extern "C"  void List_1__cctor_m1918721858_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1918721858(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1918721858_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m41290737_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m41290737(__this, method) ((  Il2CppObject* (*) (List_1_t4188176625 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m41290737_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m4091309353_gshared (List_1_t4188176625 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m4091309353(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4188176625 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4091309353_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2579088984_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2579088984(__this, method) ((  Il2CppObject * (*) (List_1_t4188176625 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m2579088984_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2076092121_gshared (List_1_t4188176625 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2076092121(__this, ___item0, method) ((  int32_t (*) (List_1_t4188176625 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2076092121_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m997846457_gshared (List_1_t4188176625 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m997846457(__this, ___item0, method) ((  bool (*) (List_1_t4188176625 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m997846457_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m232855595_gshared (List_1_t4188176625 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m232855595(__this, ___item0, method) ((  int32_t (*) (List_1_t4188176625 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m232855595_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2821470210_gshared (List_1_t4188176625 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2821470210(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4188176625 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2821470210_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3154169632_gshared (List_1_t4188176625 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3154169632(__this, ___item0, method) ((  void (*) (List_1_t4188176625 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3154169632_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m723625256_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m723625256(__this, method) ((  bool (*) (List_1_t4188176625 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m723625256_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m4281230893_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m4281230893(__this, method) ((  bool (*) (List_1_t4188176625 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m4281230893_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m970235869_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m970235869(__this, method) ((  Il2CppObject * (*) (List_1_t4188176625 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m970235869_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3013200146_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3013200146(__this, method) ((  bool (*) (List_1_t4188176625 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3013200146_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1580330201_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1580330201(__this, method) ((  bool (*) (List_1_t4188176625 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1580330201_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m426623904_gshared (List_1_t4188176625 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m426623904(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4188176625 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m426623904_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3830463667_gshared (List_1_t4188176625 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3830463667(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4188176625 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3830463667_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Add(T)
extern "C"  void List_1_Add_m1867474859_gshared (List_1_t4188176625 * __this, CRCMessageEntry_t524088197  ___item0, const MethodInfo* method);
#define List_1_Add_m1867474859(__this, ___item0, method) ((  void (*) (List_1_t4188176625 *, CRCMessageEntry_t524088197 , const MethodInfo*))List_1_Add_m1867474859_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m238852873_gshared (List_1_t4188176625 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m238852873(__this, ___newCount0, method) ((  void (*) (List_1_t4188176625 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m238852873_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2000162985_gshared (List_1_t4188176625 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2000162985(__this, ___collection0, method) ((  void (*) (List_1_t4188176625 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2000162985_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m622904601_gshared (List_1_t4188176625 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m622904601(__this, ___enumerable0, method) ((  void (*) (List_1_t4188176625 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m622904601_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m844778448_gshared (List_1_t4188176625 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m844778448(__this, ___collection0, method) ((  void (*) (List_1_t4188176625 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m844778448_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t709873889 * List_1_AsReadOnly_m694144145_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m694144145(__this, method) ((  ReadOnlyCollection_1_t709873889 * (*) (List_1_t4188176625 *, const MethodInfo*))List_1_AsReadOnly_m694144145_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Clear()
extern "C"  void List_1_Clear_m678622098_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_Clear_m678622098(__this, method) ((  void (*) (List_1_t4188176625 *, const MethodInfo*))List_1_Clear_m678622098_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Contains(T)
extern "C"  bool List_1_Contains_m196225272_gshared (List_1_t4188176625 * __this, CRCMessageEntry_t524088197  ___item0, const MethodInfo* method);
#define List_1_Contains_m196225272(__this, ___item0, method) ((  bool (*) (List_1_t4188176625 *, CRCMessageEntry_t524088197 , const MethodInfo*))List_1_Contains_m196225272_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1267496254_gshared (List_1_t4188176625 * __this, CRCMessageEntryU5BU5D_t2803924168* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1267496254(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4188176625 *, CRCMessageEntryU5BU5D_t2803924168*, int32_t, const MethodInfo*))List_1_CopyTo_m1267496254_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Find(System.Predicate`1<T>)
extern "C"  CRCMessageEntry_t524088197  List_1_Find_m717089444_gshared (List_1_t4188176625 * __this, Predicate_1_t3262025608 * ___match0, const MethodInfo* method);
#define List_1_Find_m717089444(__this, ___match0, method) ((  CRCMessageEntry_t524088197  (*) (List_1_t4188176625 *, Predicate_1_t3262025608 *, const MethodInfo*))List_1_Find_m717089444_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1403250097_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3262025608 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1403250097(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3262025608 *, const MethodInfo*))List_1_CheckMatch_m1403250097_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2712986614_gshared (List_1_t4188176625 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3262025608 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2712986614(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4188176625 *, int32_t, int32_t, Predicate_1_t3262025608 *, const MethodInfo*))List_1_GetIndex_m2712986614_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::GetEnumerator()
extern "C"  Enumerator_t3722906299  List_1_GetEnumerator_m544364021_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m544364021(__this, method) ((  Enumerator_t3722906299  (*) (List_1_t4188176625 *, const MethodInfo*))List_1_GetEnumerator_m544364021_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1602481272_gshared (List_1_t4188176625 * __this, CRCMessageEntry_t524088197  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1602481272(__this, ___item0, method) ((  int32_t (*) (List_1_t4188176625 *, CRCMessageEntry_t524088197 , const MethodInfo*))List_1_IndexOf_m1602481272_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3037505861_gshared (List_1_t4188176625 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3037505861(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4188176625 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3037505861_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3571166026_gshared (List_1_t4188176625 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3571166026(__this, ___index0, method) ((  void (*) (List_1_t4188176625 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3571166026_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m4238026227_gshared (List_1_t4188176625 * __this, int32_t ___index0, CRCMessageEntry_t524088197  ___item1, const MethodInfo* method);
#define List_1_Insert_m4238026227(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4188176625 *, int32_t, CRCMessageEntry_t524088197 , const MethodInfo*))List_1_Insert_m4238026227_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1480824928_gshared (List_1_t4188176625 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1480824928(__this, ___collection0, method) ((  void (*) (List_1_t4188176625 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1480824928_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Remove(T)
extern "C"  bool List_1_Remove_m1193088055_gshared (List_1_t4188176625 * __this, CRCMessageEntry_t524088197  ___item0, const MethodInfo* method);
#define List_1_Remove_m1193088055(__this, ___item0, method) ((  bool (*) (List_1_t4188176625 *, CRCMessageEntry_t524088197 , const MethodInfo*))List_1_Remove_m1193088055_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2183854217_gshared (List_1_t4188176625 * __this, Predicate_1_t3262025608 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2183854217(__this, ___match0, method) ((  int32_t (*) (List_1_t4188176625 *, Predicate_1_t3262025608 *, const MethodInfo*))List_1_RemoveAll_m2183854217_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m826711871_gshared (List_1_t4188176625 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m826711871(__this, ___index0, method) ((  void (*) (List_1_t4188176625 *, int32_t, const MethodInfo*))List_1_RemoveAt_m826711871_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Reverse()
extern "C"  void List_1_Reverse_m296656741_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_Reverse_m296656741(__this, method) ((  void (*) (List_1_t4188176625 *, const MethodInfo*))List_1_Reverse_m296656741_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Sort()
extern "C"  void List_1_Sort_m3411547847_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_Sort_m3411547847(__this, method) ((  void (*) (List_1_t4188176625 *, const MethodInfo*))List_1_Sort_m3411547847_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m221862771_gshared (List_1_t4188176625 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m221862771(__this, ___comparer0, method) ((  void (*) (List_1_t4188176625 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m221862771_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3913117946_gshared (List_1_t4188176625 * __this, Comparison_1_t1785827048 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3913117946(__this, ___comparison0, method) ((  void (*) (List_1_t4188176625 *, Comparison_1_t1785827048 *, const MethodInfo*))List_1_Sort_m3913117946_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::ToArray()
extern "C"  CRCMessageEntryU5BU5D_t2803924168* List_1_ToArray_m895406225_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_ToArray_m895406225(__this, method) ((  CRCMessageEntryU5BU5D_t2803924168* (*) (List_1_t4188176625 *, const MethodInfo*))List_1_ToArray_m895406225_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3906130780_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3906130780(__this, method) ((  void (*) (List_1_t4188176625 *, const MethodInfo*))List_1_TrimExcess_m3906130780_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1883152926_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1883152926(__this, method) ((  int32_t (*) (List_1_t4188176625 *, const MethodInfo*))List_1_get_Capacity_m1883152926_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1888988169_gshared (List_1_t4188176625 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1888988169(__this, ___value0, method) ((  void (*) (List_1_t4188176625 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1888988169_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::get_Count()
extern "C"  int32_t List_1_get_Count_m3074374601_gshared (List_1_t4188176625 * __this, const MethodInfo* method);
#define List_1_get_Count_m3074374601(__this, method) ((  int32_t (*) (List_1_t4188176625 *, const MethodInfo*))List_1_get_Count_m3074374601_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::get_Item(System.Int32)
extern "C"  CRCMessageEntry_t524088197  List_1_get_Item_m4162554163_gshared (List_1_t4188176625 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4162554163(__this, ___index0, method) ((  CRCMessageEntry_t524088197  (*) (List_1_t4188176625 *, int32_t, const MethodInfo*))List_1_get_Item_m4162554163_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3620839500_gshared (List_1_t4188176625 * __this, int32_t ___index0, CRCMessageEntry_t524088197  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3620839500(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4188176625 *, int32_t, CRCMessageEntry_t524088197 , const MethodInfo*))List_1_set_Item_m3620839500_gshared)(__this, ___index0, ___value1, method)
