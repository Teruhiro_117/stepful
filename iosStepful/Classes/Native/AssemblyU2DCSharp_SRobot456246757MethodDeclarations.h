﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SRobot
struct SRobot_t456246757;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SRobot::.ctor()
extern "C"  void SRobot__ctor_m1465433094 (SRobot_t456246757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SRobot::Init(System.String)
extern "C"  void SRobot_Init_m2528330196 (SRobot_t456246757 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SRobot::Move(System.Int32,System.Int32)
extern "C"  void SRobot_Move_m2909660533 (SRobot_t456246757 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SRobot::Draw(System.Int32,System.Int32)
extern "C"  void SRobot_Draw_m1781548374 (SRobot_t456246757 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SRobot::Erase(System.Int32,System.Int32)
extern "C"  void SRobot_Erase_m4091973232 (SRobot_t456246757 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
