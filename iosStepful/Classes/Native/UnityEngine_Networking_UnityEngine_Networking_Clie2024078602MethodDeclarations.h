﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.ClientCallbackAttribute
struct ClientCallbackAttribute_t2024078602;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.ClientCallbackAttribute::.ctor()
extern "C"  void ClientCallbackAttribute__ctor_m3364227933 (ClientCallbackAttribute_t2024078602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
