﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkSystem.NotReadyMessage
struct NotReadyMessage_t745889821;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.NetworkSystem.NotReadyMessage::.ctor()
extern "C"  void NotReadyMessage__ctor_m3157481669 (NotReadyMessage_t745889821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
