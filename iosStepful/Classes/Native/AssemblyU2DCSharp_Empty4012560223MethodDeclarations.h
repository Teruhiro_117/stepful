﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Empty
struct Empty_t4012560223;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Empty::.ctor()
extern "C"  void Empty__ctor_m4179857912 (Empty_t4012560223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Empty::Init(System.String)
extern "C"  void Empty_Init_m1995398434 (Empty_t4012560223 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Empty::Move(System.Int32,System.Int32)
extern "C"  void Empty_Move_m501850743 (Empty_t4012560223 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
