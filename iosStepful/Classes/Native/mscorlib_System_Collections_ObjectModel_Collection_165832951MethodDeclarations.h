﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct Collection_1_t65832951;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.NetworkSystem.CRCMessageEntry[]
struct CRCMessageEntryU5BU5D_t2803924168;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct IEnumerator_1_t2294579320;
// System.Collections.Generic.IList`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct IList_1_t1065028798;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo524088197.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.ctor()
extern "C"  void Collection_1__ctor_m3068023286_gshared (Collection_1_t65832951 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3068023286(__this, method) ((  void (*) (Collection_1_t65832951 *, const MethodInfo*))Collection_1__ctor_m3068023286_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1496822091_gshared (Collection_1_t65832951 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1496822091(__this, method) ((  bool (*) (Collection_1_t65832951 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1496822091_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1070111250_gshared (Collection_1_t65832951 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1070111250(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t65832951 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1070111250_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m196035215_gshared (Collection_1_t65832951 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m196035215(__this, method) ((  Il2CppObject * (*) (Collection_1_t65832951 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m196035215_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m953655358_gshared (Collection_1_t65832951 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m953655358(__this, ___value0, method) ((  int32_t (*) (Collection_1_t65832951 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m953655358_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1910889280_gshared (Collection_1_t65832951 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1910889280(__this, ___value0, method) ((  bool (*) (Collection_1_t65832951 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1910889280_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1930093396_gshared (Collection_1_t65832951 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1930093396(__this, ___value0, method) ((  int32_t (*) (Collection_1_t65832951 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1930093396_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2765802695_gshared (Collection_1_t65832951 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2765802695(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t65832951 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2765802695_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2276132175_gshared (Collection_1_t65832951 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2276132175(__this, ___value0, method) ((  void (*) (Collection_1_t65832951 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2276132175_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m4287962758_gshared (Collection_1_t65832951 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m4287962758(__this, method) ((  bool (*) (Collection_1_t65832951 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m4287962758_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1643083192_gshared (Collection_1_t65832951 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1643083192(__this, method) ((  Il2CppObject * (*) (Collection_1_t65832951 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1643083192_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2585593527_gshared (Collection_1_t65832951 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2585593527(__this, method) ((  bool (*) (Collection_1_t65832951 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2585593527_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1907052514_gshared (Collection_1_t65832951 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1907052514(__this, method) ((  bool (*) (Collection_1_t65832951 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1907052514_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2123583915_gshared (Collection_1_t65832951 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2123583915(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t65832951 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2123583915_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m936957468_gshared (Collection_1_t65832951 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m936957468(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t65832951 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m936957468_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Add(T)
extern "C"  void Collection_1_Add_m116632875_gshared (Collection_1_t65832951 * __this, CRCMessageEntry_t524088197  ___item0, const MethodInfo* method);
#define Collection_1_Add_m116632875(__this, ___item0, method) ((  void (*) (Collection_1_t65832951 *, CRCMessageEntry_t524088197 , const MethodInfo*))Collection_1_Add_m116632875_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Clear()
extern "C"  void Collection_1_Clear_m316539343_gshared (Collection_1_t65832951 * __this, const MethodInfo* method);
#define Collection_1_Clear_m316539343(__this, method) ((  void (*) (Collection_1_t65832951 *, const MethodInfo*))Collection_1_Clear_m316539343_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2641671209_gshared (Collection_1_t65832951 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2641671209(__this, method) ((  void (*) (Collection_1_t65832951 *, const MethodInfo*))Collection_1_ClearItems_m2641671209_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Contains(T)
extern "C"  bool Collection_1_Contains_m40936313_gshared (Collection_1_t65832951 * __this, CRCMessageEntry_t524088197  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m40936313(__this, ___item0, method) ((  bool (*) (Collection_1_t65832951 *, CRCMessageEntry_t524088197 , const MethodInfo*))Collection_1_Contains_m40936313_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m662070743_gshared (Collection_1_t65832951 * __this, CRCMessageEntryU5BU5D_t2803924168* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m662070743(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t65832951 *, CRCMessageEntryU5BU5D_t2803924168*, int32_t, const MethodInfo*))Collection_1_CopyTo_m662070743_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2484815946_gshared (Collection_1_t65832951 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2484815946(__this, method) ((  Il2CppObject* (*) (Collection_1_t65832951 *, const MethodInfo*))Collection_1_GetEnumerator_m2484815946_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m815883251_gshared (Collection_1_t65832951 * __this, CRCMessageEntry_t524088197  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m815883251(__this, ___item0, method) ((  int32_t (*) (Collection_1_t65832951 *, CRCMessageEntry_t524088197 , const MethodInfo*))Collection_1_IndexOf_m815883251_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3009274632_gshared (Collection_1_t65832951 * __this, int32_t ___index0, CRCMessageEntry_t524088197  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3009274632(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t65832951 *, int32_t, CRCMessageEntry_t524088197 , const MethodInfo*))Collection_1_Insert_m3009274632_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3920891449_gshared (Collection_1_t65832951 * __this, int32_t ___index0, CRCMessageEntry_t524088197  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3920891449(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t65832951 *, int32_t, CRCMessageEntry_t524088197 , const MethodInfo*))Collection_1_InsertItem_m3920891449_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Remove(T)
extern "C"  bool Collection_1_Remove_m3712578062_gshared (Collection_1_t65832951 * __this, CRCMessageEntry_t524088197  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3712578062(__this, ___item0, method) ((  bool (*) (Collection_1_t65832951 *, CRCMessageEntry_t524088197 , const MethodInfo*))Collection_1_Remove_m3712578062_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m4197637052_gshared (Collection_1_t65832951 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m4197637052(__this, ___index0, method) ((  void (*) (Collection_1_t65832951 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m4197637052_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3098526858_gshared (Collection_1_t65832951 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3098526858(__this, ___index0, method) ((  void (*) (Collection_1_t65832951 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3098526858_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m551667294_gshared (Collection_1_t65832951 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m551667294(__this, method) ((  int32_t (*) (Collection_1_t65832951 *, const MethodInfo*))Collection_1_get_Count_m551667294_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::get_Item(System.Int32)
extern "C"  CRCMessageEntry_t524088197  Collection_1_get_Item_m1781266432_gshared (Collection_1_t65832951 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1781266432(__this, ___index0, method) ((  CRCMessageEntry_t524088197  (*) (Collection_1_t65832951 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1781266432_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2000008979_gshared (Collection_1_t65832951 * __this, int32_t ___index0, CRCMessageEntry_t524088197  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2000008979(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t65832951 *, int32_t, CRCMessageEntry_t524088197 , const MethodInfo*))Collection_1_set_Item_m2000008979_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m4221424172_gshared (Collection_1_t65832951 * __this, int32_t ___index0, CRCMessageEntry_t524088197  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m4221424172(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t65832951 *, int32_t, CRCMessageEntry_t524088197 , const MethodInfo*))Collection_1_SetItem_m4221424172_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1642932733_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1642932733(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1642932733_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::ConvertItem(System.Object)
extern "C"  CRCMessageEntry_t524088197  Collection_1_ConvertItem_m2499408489_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2499408489(__this /* static, unused */, ___item0, method) ((  CRCMessageEntry_t524088197  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2499408489_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3256678173_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3256678173(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3256678173_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m4088211085_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m4088211085(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m4088211085_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1239392388_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1239392388(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1239392388_gshared)(__this /* static, unused */, ___list0, method)
