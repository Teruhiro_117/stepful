﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>
struct U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CProcessMatchResponseU3Ec__Iterator0_2__ctor_m1399652640_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_2__ctor_m1399652640(__this, method) ((  void (*) (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_2__ctor_m1399652640_gshared)(__this, method)
// System.Boolean UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CProcessMatchResponseU3Ec__Iterator0_2_MoveNext_m295428290_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_2_MoveNext_m295428290(__this, method) ((  bool (*) (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_2_MoveNext_m295428290_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CProcessMatchResponseU3Ec__Iterator0_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2722760562_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2722760562(__this, method) ((  Il2CppObject * (*) (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2722760562_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CProcessMatchResponseU3Ec__Iterator0_2_System_Collections_IEnumerator_get_Current_m2214904858_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_2_System_Collections_IEnumerator_get_Current_m2214904858(__this, method) ((  Il2CppObject * (*) (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_2_System_Collections_IEnumerator_get_Current_m2214904858_gshared)(__this, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CProcessMatchResponseU3Ec__Iterator0_2_Dispose_m2975156865_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_2_Dispose_m2975156865(__this, method) ((  void (*) (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_2_Dispose_m2975156865_gshared)(__this, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::Reset()
extern "C"  void U3CProcessMatchResponseU3Ec__Iterator0_2_Reset_m307142371_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_2_Reset_m307142371(__this, method) ((  void (*) (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_2_Reset_m307142371_gshared)(__this, method)
