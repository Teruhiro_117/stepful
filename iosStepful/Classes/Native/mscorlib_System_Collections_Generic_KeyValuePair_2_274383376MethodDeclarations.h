﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21197192881MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4095647918(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t274383376 *, NetworkSceneId_t2931030083 , NetworkIdentity_t1766639790 *, const MethodInfo*))KeyValuePair_2__ctor_m3453855065_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity>::get_Key()
#define KeyValuePair_2_get_Key_m1116432956(__this, method) ((  NetworkSceneId_t2931030083  (*) (KeyValuePair_2_t274383376 *, const MethodInfo*))KeyValuePair_2_get_Key_m75033871_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4270516245(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t274383376 *, NetworkSceneId_t2931030083 , const MethodInfo*))KeyValuePair_2_set_Key_m3856356940_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity>::get_Value()
#define KeyValuePair_2_get_Value_m3842683372(__this, method) ((  NetworkIdentity_t1766639790 * (*) (KeyValuePair_2_t274383376 *, const MethodInfo*))KeyValuePair_2_get_Value_m438770735_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1393599477(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t274383376 *, NetworkIdentity_t1766639790 *, const MethodInfo*))KeyValuePair_2_set_Value_m4003642012_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkIdentity>::ToString()
#define KeyValuePair_2_ToString_m2574360767(__this, method) ((  String_t* (*) (KeyValuePair_2_t274383376 *, const MethodInfo*))KeyValuePair_2_ToString_m334347088_gshared)(__this, method)
