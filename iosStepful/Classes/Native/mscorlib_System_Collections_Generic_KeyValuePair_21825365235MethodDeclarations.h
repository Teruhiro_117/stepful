﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23823471070MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int16,UnityEngine.Networking.NetworkConnection/PacketStat>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m617381698(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1825365235 *, int16_t, PacketStat_t691343460 *, const MethodInfo*))KeyValuePair_2__ctor_m2070884156_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int16,UnityEngine.Networking.NetworkConnection/PacketStat>::get_Key()
#define KeyValuePair_2_get_Key_m3270640652(__this, method) ((  int16_t (*) (KeyValuePair_2_t1825365235 *, const MethodInfo*))KeyValuePair_2_get_Key_m2738192346_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int16,UnityEngine.Networking.NetworkConnection/PacketStat>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3668398849(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1825365235 *, int16_t, const MethodInfo*))KeyValuePair_2_set_Key_m4076108421_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int16,UnityEngine.Networking.NetworkConnection/PacketStat>::get_Value()
#define KeyValuePair_2_get_Value_m271991052(__this, method) ((  PacketStat_t691343460 * (*) (KeyValuePair_2_t1825365235 *, const MethodInfo*))KeyValuePair_2_get_Value_m902390722_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int16,UnityEngine.Networking.NetworkConnection/PacketStat>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3385633969(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1825365235 *, PacketStat_t691343460 *, const MethodInfo*))KeyValuePair_2_set_Value_m4268247485_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int16,UnityEngine.Networking.NetworkConnection/PacketStat>::ToString()
#define KeyValuePair_2_ToString_m4061345791(__this, method) ((  String_t* (*) (KeyValuePair_2_t1825365235 *, const MethodInfo*))KeyValuePair_2_ToString_m878106415_gshared)(__this, method)
