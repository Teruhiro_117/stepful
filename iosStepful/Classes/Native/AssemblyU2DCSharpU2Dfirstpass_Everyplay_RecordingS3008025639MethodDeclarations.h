﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/RecordingStoppedDelegate
struct RecordingStoppedDelegate_t3008025639;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/RecordingStoppedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void RecordingStoppedDelegate__ctor_m1537694926 (RecordingStoppedDelegate_t3008025639 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/RecordingStoppedDelegate::Invoke()
extern "C"  void RecordingStoppedDelegate_Invoke_m2827616860 (RecordingStoppedDelegate_t3008025639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/RecordingStoppedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RecordingStoppedDelegate_BeginInvoke_m1915962495 (RecordingStoppedDelegate_t3008025639 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/RecordingStoppedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void RecordingStoppedDelegate_EndInvoke_m1411260736 (RecordingStoppedDelegate_t3008025639 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
