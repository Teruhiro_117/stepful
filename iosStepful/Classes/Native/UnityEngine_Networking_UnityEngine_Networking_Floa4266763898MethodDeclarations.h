﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.FloatConversion
struct FloatConversion_t4266763898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Decimal724701077.h"

// System.Void UnityEngine.Networking.FloatConversion::.ctor()
extern "C"  void FloatConversion__ctor_m2753477417 (FloatConversion_t4266763898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Networking.FloatConversion::ToSingle(System.UInt32)
extern "C"  float FloatConversion_ToSingle_m1299974852 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.Networking.FloatConversion::ToDouble(System.UInt64)
extern "C"  double FloatConversion_ToDouble_m4170175469 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal UnityEngine.Networking.FloatConversion::ToDecimal(System.UInt64,System.UInt64)
extern "C"  Decimal_t724701077  FloatConversion_ToDecimal_m110707180 (Il2CppObject * __this /* static, unused */, uint64_t ___value10, uint64_t ___value21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
