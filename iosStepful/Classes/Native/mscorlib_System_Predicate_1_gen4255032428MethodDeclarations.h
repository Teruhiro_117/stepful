﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Predicate`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct Predicate_1_t4255032428;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1517095017.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Predicate`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m4138988622_gshared (Predicate_1_t4255032428 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Predicate_1__ctor_m4138988622(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t4255032428 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m4138988622_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3393660506_gshared (Predicate_1_t4255032428 * __this, PendingPlayer_t1517095017  ___obj0, const MethodInfo* method);
#define Predicate_1_Invoke_m3393660506(__this, ___obj0, method) ((  bool (*) (Predicate_1_t4255032428 *, PendingPlayer_t1517095017 , const MethodInfo*))Predicate_1_Invoke_m3393660506_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m101869037_gshared (Predicate_1_t4255032428 * __this, PendingPlayer_t1517095017  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m101869037(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t4255032428 *, PendingPlayer_t1517095017 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m101869037_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m685968816_gshared (Predicate_1_t4255032428 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Predicate_1_EndInvoke_m685968816(__this, ___result0, method) ((  bool (*) (Predicate_1_t4255032428 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m685968816_gshared)(__this, ___result0, method)
