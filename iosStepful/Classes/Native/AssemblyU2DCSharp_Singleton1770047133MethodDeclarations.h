﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Singleton
struct Singleton_t1770047133;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Singleton::.ctor()
extern "C"  void Singleton__ctor_m676879296 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::Awake()
extern "C"  void Singleton_Awake_m3364215385 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::soundOff()
extern "C"  void Singleton_soundOff_m842928774 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::soundOn()
extern "C"  void Singleton_soundOn_m1062527022 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::setAccountName(System.String)
extern "C"  void Singleton_setAccountName_m4172620012 (Singleton_t1770047133 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::setInitBool(System.Int32)
extern "C"  void Singleton_setInitBool_m2992378539 (Singleton_t1770047133 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::setNumOfWin(System.Int32)
extern "C"  void Singleton_setNumOfWin_m3889254428 (Singleton_t1770047133 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::setNumOfLose(System.Int32)
extern "C"  void Singleton_setNumOfLose_m162589789 (Singleton_t1770047133 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::setRensyoNum(System.Int32)
extern "C"  void Singleton_setRensyoNum_m309863713 (Singleton_t1770047133 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::setMaxRensyoNum(System.Int32)
extern "C"  void Singleton_setMaxRensyoNum_m2081612021 (Singleton_t1770047133 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::setUniqueId(System.String)
extern "C"  void Singleton_setUniqueId_m652611108 (Singleton_t1770047133 * __this, String_t* ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Singleton::getAccountName()
extern "C"  String_t* Singleton_getAccountName_m4227199261 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Singleton::getInitBool()
extern "C"  int32_t Singleton_getInitBool_m1924461332 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Singleton::getNumOfWin()
extern "C"  int32_t Singleton_getNumOfWin_m3262476063 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Singleton::getNumOfLose()
extern "C"  int32_t Singleton_getNumOfLose_m3025985308 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Singleton::getRensyoNum()
extern "C"  int32_t Singleton_getRensyoNum_m68857382 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Singleton::getMaxRensyoNum()
extern "C"  int32_t Singleton_getMaxRensyoNum_m1807931810 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Singleton::getUniqueId()
extern "C"  String_t* Singleton_getUniqueId_m206607097 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::Init()
extern "C"  void Singleton_Init_m3535994028 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::CountWinLose(System.Boolean)
extern "C"  void Singleton_CountWinLose_m4154155941 (Singleton_t1770047133 * __this, bool ___win0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::Save()
extern "C"  void Singleton_Save_m574479149 (Singleton_t1770047133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Singleton::.cctor()
extern "C"  void Singleton__cctor_m926949583 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
