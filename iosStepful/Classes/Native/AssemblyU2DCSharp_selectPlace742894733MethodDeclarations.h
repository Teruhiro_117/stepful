﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// selectPlace
struct selectPlace_t742894733;

#include "codegen/il2cpp-codegen.h"

// System.Void selectPlace::.ctor()
extern "C"  void selectPlace__ctor_m2383638438 (selectPlace_t742894733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void selectPlace::Start()
extern "C"  void selectPlace_Start_m1096605630 (selectPlace_t742894733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void selectPlace::Update()
extern "C"  void selectPlace_Update_m3720865529 (selectPlace_t742894733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void selectPlace::OnClick()
extern "C"  void selectPlace_OnClick_m2105193043 (selectPlace_t742894733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
