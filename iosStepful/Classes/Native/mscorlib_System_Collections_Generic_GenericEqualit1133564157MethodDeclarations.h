﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>
struct GenericEqualityComparer_1_t1133564157;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m2828795098_gshared (GenericEqualityComparer_1_t1133564157 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m2828795098(__this, method) ((  void (*) (GenericEqualityComparer_1_t1133564157 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m2828795098_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m734592575_gshared (GenericEqualityComparer_1_t1133564157 * __this, uint32_t ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m734592575(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t1133564157 *, uint32_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m734592575_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.UInt32>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m719101535_gshared (GenericEqualityComparer_1_t1133564157 * __this, uint32_t ___x0, uint32_t ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m719101535(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t1133564157 *, uint32_t, uint32_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m719101535_gshared)(__this, ___x0, ___y1, method)
