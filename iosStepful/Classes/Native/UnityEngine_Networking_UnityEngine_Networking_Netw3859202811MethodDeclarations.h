﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkBehaviour/Invoker
struct Invoker_t3859202811;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.NetworkBehaviour/Invoker::.ctor()
extern "C"  void Invoker__ctor_m351892759 (Invoker_t3859202811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.NetworkBehaviour/Invoker::DebugString()
extern "C"  String_t* Invoker_DebugString_m2931367312 (Invoker_t3859202811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
