﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0
struct U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::.ctor()
extern "C"  void U3CCrossfadeTransitionU3Ec__Iterator0__ctor_m2088035682 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::MoveNext()
extern "C"  bool U3CCrossfadeTransitionU3Ec__Iterator0_MoveNext_m1072767658 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCrossfadeTransitionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2847969304 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCrossfadeTransitionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2048901056 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::Dispose()
extern "C"  void U3CCrossfadeTransitionU3Ec__Iterator0_Dispose_m573461969 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::Reset()
extern "C"  void U3CCrossfadeTransitionU3Ec__Iterator0_Reset_m249625775 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
