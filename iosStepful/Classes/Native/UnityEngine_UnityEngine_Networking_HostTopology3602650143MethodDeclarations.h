﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.HostTopology
struct HostTopology_t3602650143;
// UnityEngine.Networking.ConnectionConfig
struct ConnectionConfig_t1350910390;
// System.Collections.Generic.List`1<UnityEngine.Networking.ConnectionConfig>
struct List_1_t720031522;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_ConnectionConfi1350910390.h"

// System.Void UnityEngine.Networking.HostTopology::.ctor(UnityEngine.Networking.ConnectionConfig,System.Int32)
extern "C"  void HostTopology__ctor_m97899102 (HostTopology_t3602650143 * __this, ConnectionConfig_t1350910390 * ___defaultConfig0, int32_t ___maxDefaultConnections1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.HostTopology::.ctor()
extern "C"  void HostTopology__ctor_m3952046088 (HostTopology_t3602650143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.ConnectionConfig UnityEngine.Networking.HostTopology::get_DefaultConfig()
extern "C"  ConnectionConfig_t1350910390 * HostTopology_get_DefaultConfig_m2060597096 (HostTopology_t3602650143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.HostTopology::get_MaxDefaultConnections()
extern "C"  int32_t HostTopology_get_MaxDefaultConnections_m934817189 (HostTopology_t3602650143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.HostTopology::get_SpecialConnectionConfigsCount()
extern "C"  int32_t HostTopology_get_SpecialConnectionConfigsCount_m603012342 (HostTopology_t3602650143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Networking.ConnectionConfig> UnityEngine.Networking.HostTopology::get_SpecialConnectionConfigs()
extern "C"  List_1_t720031522 * HostTopology_get_SpecialConnectionConfigs_m675591769 (HostTopology_t3602650143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.ConnectionConfig UnityEngine.Networking.HostTopology::GetSpecialConnectionConfig(System.Int32)
extern "C"  ConnectionConfig_t1350910390 * HostTopology_GetSpecialConnectionConfig_m278222194 (HostTopology_t3602650143 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 UnityEngine.Networking.HostTopology::get_ReceivedMessagePoolSize()
extern "C"  uint16_t HostTopology_get_ReceivedMessagePoolSize_m751764457 (HostTopology_t3602650143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 UnityEngine.Networking.HostTopology::get_SentMessagePoolSize()
extern "C"  uint16_t HostTopology_get_SentMessagePoolSize_m1543341606 (HostTopology_t3602650143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Networking.HostTopology::get_MessagePoolSizeGrowthFactor()
extern "C"  float HostTopology_get_MessagePoolSizeGrowthFactor_m3918119345 (HostTopology_t3602650143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
