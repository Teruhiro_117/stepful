﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.Match.Request
struct Request_t1834273339;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_Types_SourceID1840552406.h"

// System.Void UnityEngine.Networking.Match.Request::.ctor()
extern "C"  void Request__ctor_m3707673737 (Request_t1834273339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.Types.SourceID UnityEngine.Networking.Match.Request::get_sourceId()
extern "C"  uint64_t Request_get_sourceId_m142896181 (Request_t1834273339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.Request::get_projectId()
extern "C"  String_t* Request_get_projectId_m1225017645 (Request_t1834273339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.Request::get_accessTokenString()
extern "C"  String_t* Request_get_accessTokenString_m3354282547 (Request_t1834273339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.Match.Request::get_domain()
extern "C"  int32_t Request_get_domain_m461316590 (Request_t1834273339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.Request::set_domain(System.Int32)
extern "C"  void Request_set_domain_m2899460365 (Request_t1834273339 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.Request::ToString()
extern "C"  String_t* Request_ToString_m440636222 (Request_t1834273339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.Request::.cctor()
extern "C"  void Request__cctor_m2271248550 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
