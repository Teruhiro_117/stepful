﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayRecButtons/ToggleButton
struct ToggleButton_t2881484729;
// EveryplayRecButtons/TextureAtlasSrc
struct TextureAtlasSrc_t2048635151;
// EveryplayRecButtons/ButtonTapped
struct ButtonTapped_t3122824015;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_2048635151.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_3122824015.h"

// System.Void EveryplayRecButtons/ToggleButton::.ctor(EveryplayRecButtons/TextureAtlasSrc,EveryplayRecButtons/TextureAtlasSrc,EveryplayRecButtons/ButtonTapped,EveryplayRecButtons/TextureAtlasSrc,EveryplayRecButtons/TextureAtlasSrc)
extern "C"  void ToggleButton__ctor_m801500299 (ToggleButton_t2881484729 * __this, TextureAtlasSrc_t2048635151 * ___bg0, TextureAtlasSrc_t2048635151 * ___title1, ButtonTapped_t3122824015 * ___buttonTapped2, TextureAtlasSrc_t2048635151 * ___toggleOn3, TextureAtlasSrc_t2048635151 * ___toggleOff4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
