﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// showData
struct showData_t3631251573;

#include "codegen/il2cpp-codegen.h"

// System.Void showData::.ctor()
extern "C"  void showData__ctor_m3344537568 (showData_t3631251573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void showData::Start()
extern "C"  void showData_Start_m2293015712 (showData_t3631251573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void showData::reShow()
extern "C"  void showData_reShow_m3436802746 (showData_t3631251573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void showData::Update()
extern "C"  void showData_Update_m263602305 (showData_t3631251573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
