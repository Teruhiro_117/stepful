﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.ClientAttribute
struct ClientAttribute_t2502255907;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.ClientAttribute::.ctor()
extern "C"  void ClientAttribute__ctor_m363409236 (ClientAttribute_t2502255907 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
