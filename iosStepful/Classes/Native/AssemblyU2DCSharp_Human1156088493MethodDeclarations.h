﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Human
struct Human_t1156088493;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Human::.ctor()
extern "C"  void Human__ctor_m4119456528 (Human_t1156088493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Human::Init(System.String)
extern "C"  void Human_Init_m20722698 (Human_t1156088493 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Human::Move(System.Int32,System.Int32)
extern "C"  void Human_Move_m2550404821 (Human_t1156088493 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Human::Draw(System.Int32,System.Int32)
extern "C"  void Human_Draw_m66723152 (Human_t1156088493 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Human::Erase(System.Int32,System.Int32)
extern "C"  void Human_Erase_m446363430 (Human_t1156088493 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
