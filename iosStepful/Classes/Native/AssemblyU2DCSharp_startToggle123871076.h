﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Toggle
struct Toggle_t3976754468;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// startToggle
struct  startToggle_t123871076  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Toggle startToggle::toggle
	Toggle_t3976754468 * ___toggle_2;

public:
	inline static int32_t get_offset_of_toggle_2() { return static_cast<int32_t>(offsetof(startToggle_t123871076, ___toggle_2)); }
	inline Toggle_t3976754468 * get_toggle_2() const { return ___toggle_2; }
	inline Toggle_t3976754468 ** get_address_of_toggle_2() { return &___toggle_2; }
	inline void set_toggle_2(Toggle_t3976754468 * value)
	{
		___toggle_2 = value;
		Il2CppCodeGenWriteBarrier(&___toggle_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
