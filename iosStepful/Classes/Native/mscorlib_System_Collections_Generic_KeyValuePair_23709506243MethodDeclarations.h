﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23709506243.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID348058649.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1665737714_gshared (KeyValuePair_2_t3709506243 * __this, uint64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m1665737714(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3709506243 *, uint64_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1665737714_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Key()
extern "C"  uint64_t KeyValuePair_2_get_Key_m3078650232_gshared (KeyValuePair_2_t3709506243 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3078650232(__this, method) ((  uint64_t (*) (KeyValuePair_2_t3709506243 *, const MethodInfo*))KeyValuePair_2_get_Key_m3078650232_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m62205575_gshared (KeyValuePair_2_t3709506243 * __this, uint64_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m62205575(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3709506243 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Key_m62205575_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m549329768_gshared (KeyValuePair_2_t3709506243 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m549329768(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3709506243 *, const MethodInfo*))KeyValuePair_2_get_Value_m549329768_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m223828967_gshared (KeyValuePair_2_t3709506243 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m223828967(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3709506243 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m223828967_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1791447377_gshared (KeyValuePair_2_t3709506243 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1791447377(__this, method) ((  String_t* (*) (KeyValuePair_2_t3709506243 *, const MethodInfo*))KeyValuePair_2_ToString_m1791447377_gshared)(__this, method)
