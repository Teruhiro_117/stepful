﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// arrangeKoma
struct arrangeKoma_t3423810832;
// System.Byte[0...,0...]
struct ByteU5BU2CU5D_t3397334014;
// System.Int32[0...,0...]
struct Int32U5BU2CU5D_t3030399642;

#include "codegen/il2cpp-codegen.h"

// System.Void arrangeKoma::.ctor()
extern "C"  void arrangeKoma__ctor_m1481271067 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::Start()
extern "C"  void arrangeKoma_Start_m1732940823 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::Update()
extern "C"  void arrangeKoma_Update_m1760216046 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::Init()
extern "C"  void arrangeKoma_Init_m3905406573 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[0...,0...] arrangeKoma::getStartBoard()
extern "C"  ByteU5BU2CU5D_t3397334014* arrangeKoma_getStartBoard_m4029400191 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean arrangeKoma::getHuman()
extern "C"  bool arrangeKoma_getHuman_m3006929540 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[0...,0...] arrangeKoma::getFactoryLocation()
extern "C"  Int32U5BU2CU5D_t3030399642* arrangeKoma_getFactoryLocation_m3685241018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[0...,0...] arrangeKoma::getBarrier()
extern "C"  Int32U5BU2CU5D_t3030399642* arrangeKoma_getBarrier_m3597185560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 arrangeKoma::getMyResources()
extern "C"  int32_t arrangeKoma_getMyResources_m4065310466 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::setKoma()
extern "C"  void arrangeKoma_setKoma_m1256667485 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::printKoma()
extern "C"  void arrangeKoma_printKoma_m261318148 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::paint(System.Int32,System.Int32)
extern "C"  void arrangeKoma_paint_m769033625 (arrangeKoma_t3423810832 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::allPrintColorBottom()
extern "C"  void arrangeKoma_allPrintColorBottom_m969512469 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::printColorBottom(System.Int32,System.Int32)
extern "C"  void arrangeKoma_printColorBottom_m1192818386 (arrangeKoma_t3423810832 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::printColorAbove(System.Int32,System.Int32)
extern "C"  void arrangeKoma_printColorAbove_m2660003704 (arrangeKoma_t3423810832 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::setColorWhite()
extern "C"  void arrangeKoma_setColorWhite_m729711447 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::setColorRed()
extern "C"  void arrangeKoma_setColorRed_m3294458039 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::setColorBlue()
extern "C"  void arrangeKoma_setColorBlue_m4212109396 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::setColorClear()
extern "C"  void arrangeKoma_setColorClear_m3548991013 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::setAllWhite()
extern "C"  void arrangeKoma_setAllWhite_m832677259 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::inputKoma(System.Int32)
extern "C"  void arrangeKoma_inputKoma_m2346436234 (arrangeKoma_t3423810832 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::changeKomaResources(System.Int32)
extern "C"  void arrangeKoma_changeKomaResources_m4200569561 (arrangeKoma_t3423810832 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::countResources(System.Int32)
extern "C"  void arrangeKoma_countResources_m2933092726 (arrangeKoma_t3423810832 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::lineRow(System.Int32,System.Int32)
extern "C"  void arrangeKoma_lineRow_m1340912179 (arrangeKoma_t3423810832 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::canNotTouchAll()
extern "C"  void arrangeKoma_canNotTouchAll_m2596981714 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::canTouchAll()
extern "C"  void arrangeKoma_canTouchAll_m931637095 (arrangeKoma_t3423810832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void arrangeKoma::getResources(System.Int32)
extern "C"  void arrangeKoma_getResources_m1274042461 (arrangeKoma_t3423810832 * __this, int32_t ___resource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
