﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.Types.NetworkAccessToken
struct NetworkAccessToken_t2931214851;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Networking.Types.NetworkAccessToken::.ctor()
extern "C"  void NetworkAccessToken__ctor_m1686528113 (NetworkAccessToken_t2931214851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Types.NetworkAccessToken::.ctor(System.String)
extern "C"  void NetworkAccessToken__ctor_m2798343923 (NetworkAccessToken_t2931214851 * __this, String_t* ___strArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Types.NetworkAccessToken::GetByteString()
extern "C"  String_t* NetworkAccessToken_GetByteString_m1325597153 (NetworkAccessToken_t2931214851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
