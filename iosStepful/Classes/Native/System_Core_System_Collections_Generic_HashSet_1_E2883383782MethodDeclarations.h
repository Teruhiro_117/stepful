﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E3806193287MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkIdentity>::.ctor(System.Collections.Generic.HashSet`1<T>)
#define Enumerator__ctor_m3760059395(__this, ___hashset0, method) ((  void (*) (Enumerator_t2883383782 *, HashSet_1_t100100644 *, const MethodInfo*))Enumerator__ctor_m1279102766_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkIdentity>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2370617081(__this, method) ((  Il2CppObject * (*) (Enumerator_t2883383782 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2899861010_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkIdentity>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2525883137(__this, method) ((  void (*) (Enumerator_t2883383782 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2573763156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkIdentity>::MoveNext()
#define Enumerator_MoveNext_m1634686918(__this, method) ((  bool (*) (Enumerator_t2883383782 *, const MethodInfo*))Enumerator_MoveNext_m2097560514_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkIdentity>::get_Current()
#define Enumerator_get_Current_m2006045916(__this, method) ((  NetworkIdentity_t1766639790 * (*) (Enumerator_t2883383782 *, const MethodInfo*))Enumerator_get_Current_m3016104593_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkIdentity>::Dispose()
#define Enumerator_Dispose_m1720066096(__this, method) ((  void (*) (Enumerator_t2883383782 *, const MethodInfo*))Enumerator_Dispose_m2585752265_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkIdentity>::CheckState()
#define Enumerator_CheckState_m1042711696(__this, method) ((  void (*) (Enumerator_t2883383782 *, const MethodInfo*))Enumerator_CheckState_m1761755727_gshared)(__this, method)
