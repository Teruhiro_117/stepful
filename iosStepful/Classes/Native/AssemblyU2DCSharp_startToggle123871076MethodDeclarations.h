﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// startToggle
struct startToggle_t123871076;

#include "codegen/il2cpp-codegen.h"

// System.Void startToggle::.ctor()
extern "C"  void startToggle__ctor_m1931058105 (startToggle_t123871076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void startToggle::Start()
extern "C"  void startToggle_Start_m1519248745 (startToggle_t123871076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void startToggle::Update()
extern "C"  void startToggle_Update_m2149680814 (startToggle_t123871076 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
