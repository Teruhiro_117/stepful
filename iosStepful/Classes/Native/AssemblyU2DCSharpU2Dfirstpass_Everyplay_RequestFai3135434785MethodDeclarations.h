﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/RequestFailedDelegate
struct RequestFailedDelegate_t3135434785;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/RequestFailedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestFailedDelegate__ctor_m844497070 (RequestFailedDelegate_t3135434785 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/RequestFailedDelegate::Invoke(System.String)
extern "C"  void RequestFailedDelegate_Invoke_m3522845546 (RequestFailedDelegate_t3135434785 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/RequestFailedDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RequestFailedDelegate_BeginInvoke_m3384874647 (RequestFailedDelegate_t3135434785 * __this, String_t* ___error0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/RequestFailedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void RequestFailedDelegate_EndInvoke_m3568189980 (RequestFailedDelegate_t3135434785 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
