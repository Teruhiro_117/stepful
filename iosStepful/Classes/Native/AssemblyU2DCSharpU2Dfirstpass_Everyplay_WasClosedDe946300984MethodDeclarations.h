﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/WasClosedDelegate
struct WasClosedDelegate_t946300984;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/WasClosedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void WasClosedDelegate__ctor_m1406105615 (WasClosedDelegate_t946300984 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/WasClosedDelegate::Invoke()
extern "C"  void WasClosedDelegate_Invoke_m2969330763 (WasClosedDelegate_t946300984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/WasClosedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WasClosedDelegate_BeginInvoke_m3121665708 (WasClosedDelegate_t946300984 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/WasClosedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void WasClosedDelegate_EndInvoke_m1646449925 (WasClosedDelegate_t946300984 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
