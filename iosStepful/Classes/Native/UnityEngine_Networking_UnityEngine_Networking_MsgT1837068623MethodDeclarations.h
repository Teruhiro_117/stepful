﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.MsgType
struct MsgType_t1837068623;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.MsgType::.ctor()
extern "C"  void MsgType__ctor_m3939957780 (MsgType_t1837068623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.MsgType::MsgTypeToString(System.Int16)
extern "C"  String_t* MsgType_MsgTypeToString_m3370549557 (Il2CppObject * __this /* static, unused */, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.MsgType::.cctor()
extern "C"  void MsgType__cctor_m4187474751 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
