﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Data
struct Data_t3569509720;

#include "codegen/il2cpp-codegen.h"

// System.Void Data::.ctor()
extern "C"  void Data__ctor_m613971867 (Data_t3569509720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Data::Start()
extern "C"  void Data_Start_m260840599 (Data_t3569509720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Data::Update()
extern "C"  void Data_Update_m3009678490 (Data_t3569509720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
