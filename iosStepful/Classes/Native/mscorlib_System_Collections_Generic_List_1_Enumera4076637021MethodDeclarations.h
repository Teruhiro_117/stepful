﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct List_1_t246940051;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4076637021.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clien877818919.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2511073500_gshared (Enumerator_t4076637021 * __this, List_1_t246940051 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2511073500(__this, ___l0, method) ((  void (*) (Enumerator_t4076637021 *, List_1_t246940051 *, const MethodInfo*))Enumerator__ctor_m2511073500_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m597985246_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m597985246(__this, method) ((  void (*) (Enumerator_t4076637021 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m597985246_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2336358082_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2336358082(__this, method) ((  Il2CppObject * (*) (Enumerator_t4076637021 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2336358082_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::Dispose()
extern "C"  void Enumerator_Dispose_m100793535_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m100793535(__this, method) ((  void (*) (Enumerator_t4076637021 *, const MethodInfo*))Enumerator_Dispose_m100793535_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::VerifyState()
extern "C"  void Enumerator_VerifyState_m566577098_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m566577098(__this, method) ((  void (*) (Enumerator_t4076637021 *, const MethodInfo*))Enumerator_VerifyState_m566577098_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2874416406_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2874416406(__this, method) ((  bool (*) (Enumerator_t4076637021 *, const MethodInfo*))Enumerator_MoveNext_m2874416406_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::get_Current()
extern "C"  PendingOwner_t877818919  Enumerator_get_Current_m162665063_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m162665063(__this, method) ((  PendingOwner_t877818919  (*) (Enumerator_t4076637021 *, const MethodInfo*))Enumerator_get_Current_m162665063_gshared)(__this, method)
