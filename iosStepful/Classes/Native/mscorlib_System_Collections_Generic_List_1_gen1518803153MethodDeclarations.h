﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1518803153;
// System.Collections.Generic.IEnumerable`1<System.UInt32>
struct IEnumerable_1_t2441809066;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t3920173144;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.UInt32>
struct ICollection_1_t3101757326;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>
struct ReadOnlyCollection_1_t2335467713;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Predicate`1<System.UInt32>
struct Predicate_1_t592652136;
// System.Collections.Generic.IComparer`1<System.UInt32>
struct IComparer_1_t104145143;
// System.Comparison`1<System.UInt32>
struct Comparison_1_t3411420872;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1053532827.h"

// System.Void System.Collections.Generic.List`1<System.UInt32>::.ctor()
extern "C"  void List_1__ctor_m3892229232_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1__ctor_m3892229232(__this, method) ((  void (*) (List_1_t1518803153 *, const MethodInfo*))List_1__ctor_m3892229232_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3449993445_gshared (List_1_t1518803153 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3449993445(__this, ___collection0, method) ((  void (*) (List_1_t1518803153 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3449993445_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m787949159_gshared (List_1_t1518803153 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m787949159(__this, ___capacity0, method) ((  void (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))List_1__ctor_m787949159_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::.cctor()
extern "C"  void List_1__cctor_m3327571413_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3327571413(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3327571413_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.UInt32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3982060974_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3982060974(__this, method) ((  Il2CppObject* (*) (List_1_t1518803153 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3982060974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2285669092_gshared (List_1_t1518803153 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2285669092(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1518803153 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2285669092_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m565117939_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m565117939(__this, method) ((  Il2CppObject * (*) (List_1_t1518803153 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m565117939_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m869716888_gshared (List_1_t1518803153 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m869716888(__this, ___item0, method) ((  int32_t (*) (List_1_t1518803153 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m869716888_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m32644614_gshared (List_1_t1518803153 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m32644614(__this, ___item0, method) ((  bool (*) (List_1_t1518803153 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m32644614_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3786710506_gshared (List_1_t1518803153 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3786710506(__this, ___item0, method) ((  int32_t (*) (List_1_t1518803153 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3786710506_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m441332475_gshared (List_1_t1518803153 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m441332475(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1518803153 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m441332475_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m541115227_gshared (List_1_t1518803153 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m541115227(__this, ___item0, method) ((  void (*) (List_1_t1518803153 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m541115227_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2029590095_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2029590095(__this, method) ((  bool (*) (List_1_t1518803153 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2029590095_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3183969424_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3183969424(__this, method) ((  bool (*) (List_1_t1518803153 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3183969424_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.UInt32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2565453732_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2565453732(__this, method) ((  Il2CppObject * (*) (List_1_t1518803153 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2565453732_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m480512827_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m480512827(__this, method) ((  bool (*) (List_1_t1518803153 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m480512827_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m4054385804_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m4054385804(__this, method) ((  bool (*) (List_1_t1518803153 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m4054385804_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m4221970155_gshared (List_1_t1518803153 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m4221970155(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m4221970155_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2624338790_gshared (List_1_t1518803153 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2624338790(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1518803153 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2624338790_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Add(T)
extern "C"  void List_1_Add_m3442797503_gshared (List_1_t1518803153 * __this, uint32_t ___item0, const MethodInfo* method);
#define List_1_Add_m3442797503(__this, ___item0, method) ((  void (*) (List_1_t1518803153 *, uint32_t, const MethodInfo*))List_1_Add_m3442797503_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3302502622_gshared (List_1_t1518803153 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3302502622(__this, ___newCount0, method) ((  void (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3302502622_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2775755918_gshared (List_1_t1518803153 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2775755918(__this, ___collection0, method) ((  void (*) (List_1_t1518803153 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2775755918_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m4173078734_gshared (List_1_t1518803153 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m4173078734(__this, ___enumerable0, method) ((  void (*) (List_1_t1518803153 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m4173078734_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1875433751_gshared (List_1_t1518803153 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1875433751(__this, ___collection0, method) ((  void (*) (List_1_t1518803153 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1875433751_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.UInt32>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2335467713 * List_1_AsReadOnly_m3260238084_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3260238084(__this, method) ((  ReadOnlyCollection_1_t2335467713 * (*) (List_1_t1518803153 *, const MethodInfo*))List_1_AsReadOnly_m3260238084_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Clear()
extern "C"  void List_1_Clear_m3039200491_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_Clear_m3039200491(__this, method) ((  void (*) (List_1_t1518803153 *, const MethodInfo*))List_1_Clear_m3039200491_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::Contains(T)
extern "C"  bool List_1_Contains_m546813581_gshared (List_1_t1518803153 * __this, uint32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m546813581(__this, ___item0, method) ((  bool (*) (List_1_t1518803153 *, uint32_t, const MethodInfo*))List_1_Contains_m546813581_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2655047491_gshared (List_1_t1518803153 * __this, UInt32U5BU5D_t59386216* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2655047491(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1518803153 *, UInt32U5BU5D_t59386216*, int32_t, const MethodInfo*))List_1_CopyTo_m2655047491_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.UInt32>::Find(System.Predicate`1<T>)
extern "C"  uint32_t List_1_Find_m3258427525_gshared (List_1_t1518803153 * __this, Predicate_1_t592652136 * ___match0, const MethodInfo* method);
#define List_1_Find_m3258427525(__this, ___match0, method) ((  uint32_t (*) (List_1_t1518803153 *, Predicate_1_t592652136 *, const MethodInfo*))List_1_Find_m3258427525_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3238292412_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t592652136 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3238292412(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t592652136 *, const MethodInfo*))List_1_CheckMatch_m3238292412_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2092869081_gshared (List_1_t1518803153 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t592652136 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2092869081(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1518803153 *, int32_t, int32_t, Predicate_1_t592652136 *, const MethodInfo*))List_1_GetIndex_m2092869081_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.UInt32>::GetEnumerator()
extern "C"  Enumerator_t1053532827  List_1_GetEnumerator_m1891487472_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1891487472(__this, method) ((  Enumerator_t1053532827  (*) (List_1_t1518803153 *, const MethodInfo*))List_1_GetEnumerator_m1891487472_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3589044623_gshared (List_1_t1518803153 * __this, uint32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3589044623(__this, ___item0, method) ((  int32_t (*) (List_1_t1518803153 *, uint32_t, const MethodInfo*))List_1_IndexOf_m3589044623_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m131028080_gshared (List_1_t1518803153 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m131028080(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1518803153 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m131028080_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1850885907_gshared (List_1_t1518803153 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1850885907(__this, ___index0, method) ((  void (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1850885907_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1561732170_gshared (List_1_t1518803153 * __this, int32_t ___index0, uint32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m1561732170(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1518803153 *, int32_t, uint32_t, const MethodInfo*))List_1_Insert_m1561732170_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m394760813_gshared (List_1_t1518803153 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m394760813(__this, ___collection0, method) ((  void (*) (List_1_t1518803153 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m394760813_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt32>::Remove(T)
extern "C"  bool List_1_Remove_m2573554156_gshared (List_1_t1518803153 * __this, uint32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m2573554156(__this, ___item0, method) ((  bool (*) (List_1_t1518803153 *, uint32_t, const MethodInfo*))List_1_Remove_m2573554156_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m4206403336_gshared (List_1_t1518803153 * __this, Predicate_1_t592652136 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m4206403336(__this, ___match0, method) ((  int32_t (*) (List_1_t1518803153 *, Predicate_1_t592652136 *, const MethodInfo*))List_1_RemoveAll_m4206403336_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m4232884678_gshared (List_1_t1518803153 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m4232884678(__this, ___index0, method) ((  void (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4232884678_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Reverse()
extern "C"  void List_1_Reverse_m1241861270_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_Reverse_m1241861270(__this, method) ((  void (*) (List_1_t1518803153 *, const MethodInfo*))List_1_Reverse_m1241861270_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Sort()
extern "C"  void List_1_Sort_m2195199738_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_Sort_m2195199738(__this, method) ((  void (*) (List_1_t1518803153 *, const MethodInfo*))List_1_Sort_m2195199738_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2617727094_gshared (List_1_t1518803153 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2617727094(__this, ___comparer0, method) ((  void (*) (List_1_t1518803153 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2617727094_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1226837117_gshared (List_1_t1518803153 * __this, Comparison_1_t3411420872 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1226837117(__this, ___comparison0, method) ((  void (*) (List_1_t1518803153 *, Comparison_1_t3411420872 *, const MethodInfo*))List_1_Sort_m1226837117_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.UInt32>::ToArray()
extern "C"  UInt32U5BU5D_t59386216* List_1_ToArray_m1748209125_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_ToArray_m1748209125(__this, method) ((  UInt32U5BU5D_t59386216* (*) (List_1_t1518803153 *, const MethodInfo*))List_1_ToArray_m1748209125_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::TrimExcess()
extern "C"  void List_1_TrimExcess_m126153639_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m126153639(__this, method) ((  void (*) (List_1_t1518803153 *, const MethodInfo*))List_1_TrimExcess_m126153639_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1180334853_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1180334853(__this, method) ((  int32_t (*) (List_1_t1518803153 *, const MethodInfo*))List_1_get_Capacity_m1180334853_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1969285390_gshared (List_1_t1518803153 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1969285390(__this, ___value0, method) ((  void (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1969285390_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt32>::get_Count()
extern "C"  int32_t List_1_get_Count_m3164482488_gshared (List_1_t1518803153 * __this, const MethodInfo* method);
#define List_1_get_Count_m3164482488(__this, method) ((  int32_t (*) (List_1_t1518803153 *, const MethodInfo*))List_1_get_Count_m3164482488_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.UInt32>::get_Item(System.Int32)
extern "C"  uint32_t List_1_get_Item_m629355960_gshared (List_1_t1518803153 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m629355960(__this, ___index0, method) ((  uint32_t (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))List_1_get_Item_m629355960_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.UInt32>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1004155399_gshared (List_1_t1518803153 * __this, int32_t ___index0, uint32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m1004155399(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1518803153 *, int32_t, uint32_t, const MethodInfo*))List_1_set_Item_m1004155399_gshared)(__this, ___index0, ___value1, method)
