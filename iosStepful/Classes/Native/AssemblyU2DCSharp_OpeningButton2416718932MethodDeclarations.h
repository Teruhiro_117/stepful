﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OpeningButton
struct OpeningButton_t2416718932;

#include "codegen/il2cpp-codegen.h"

// System.Void OpeningButton::.ctor()
extern "C"  void OpeningButton__ctor_m663800617 (OpeningButton_t2416718932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpeningButton::Start()
extern "C"  void OpeningButton_Start_m4151042585 (OpeningButton_t2416718932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpeningButton::Update()
extern "C"  void OpeningButton_Update_m758039582 (OpeningButton_t2416718932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OpeningButton::OnClick()
extern "C"  void OpeningButton_OnClick_m4075053516 (OpeningButton_t2416718932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
