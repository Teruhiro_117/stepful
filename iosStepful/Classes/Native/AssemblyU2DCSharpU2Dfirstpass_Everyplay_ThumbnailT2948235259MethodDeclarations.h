﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/ThumbnailTextureReadyDelegate
struct ThumbnailTextureReadyDelegate_t2948235259;
// System.Object
struct Il2CppObject;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/ThumbnailTextureReadyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ThumbnailTextureReadyDelegate__ctor_m3605697724 (ThumbnailTextureReadyDelegate_t2948235259 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/ThumbnailTextureReadyDelegate::Invoke(UnityEngine.Texture2D,System.Boolean)
extern "C"  void ThumbnailTextureReadyDelegate_Invoke_m3492481785 (ThumbnailTextureReadyDelegate_t2948235259 * __this, Texture2D_t3542995729 * ___texture0, bool ___portrait1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/ThumbnailTextureReadyDelegate::BeginInvoke(UnityEngine.Texture2D,System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ThumbnailTextureReadyDelegate_BeginInvoke_m3479227342 (ThumbnailTextureReadyDelegate_t2948235259 * __this, Texture2D_t3542995729 * ___texture0, bool ___portrait1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/ThumbnailTextureReadyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ThumbnailTextureReadyDelegate_EndInvoke_m125693966 (ThumbnailTextureReadyDelegate_t2948235259 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
