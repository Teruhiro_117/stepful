﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct Collection_1_t419563673;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.ClientScene/PendingOwner[]
struct PendingOwnerU5BU5D_t2720009054;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct IEnumerator_1_t2648310042;
// System.Collections.Generic.IList`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct IList_1_t1418759520;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clien877818919.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::.ctor()
extern "C"  void Collection_1__ctor_m3273619022_gshared (Collection_1_t419563673 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3273619022(__this, method) ((  void (*) (Collection_1_t419563673 *, const MethodInfo*))Collection_1__ctor_m3273619022_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3741783509_gshared (Collection_1_t419563673 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3741783509(__this, method) ((  bool (*) (Collection_1_t419563673 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3741783509_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3161942750_gshared (Collection_1_t419563673 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3161942750(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t419563673 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3161942750_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2454374177_gshared (Collection_1_t419563673 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2454374177(__this, method) ((  Il2CppObject * (*) (Collection_1_t419563673 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2454374177_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m801803582_gshared (Collection_1_t419563673 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m801803582(__this, ___value0, method) ((  int32_t (*) (Collection_1_t419563673 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m801803582_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1390153480_gshared (Collection_1_t419563673 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1390153480(__this, ___value0, method) ((  bool (*) (Collection_1_t419563673 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1390153480_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2655333228_gshared (Collection_1_t419563673 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2655333228(__this, ___value0, method) ((  int32_t (*) (Collection_1_t419563673 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2655333228_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3276838641_gshared (Collection_1_t419563673 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3276838641(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t419563673 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3276838641_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3693573209_gshared (Collection_1_t419563673 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3693573209(__this, ___value0, method) ((  void (*) (Collection_1_t419563673 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3693573209_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m4079280086_gshared (Collection_1_t419563673 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m4079280086(__this, method) ((  bool (*) (Collection_1_t419563673 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m4079280086_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2006138286_gshared (Collection_1_t419563673 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2006138286(__this, method) ((  Il2CppObject * (*) (Collection_1_t419563673 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2006138286_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3658828133_gshared (Collection_1_t419563673 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3658828133(__this, method) ((  bool (*) (Collection_1_t419563673 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3658828133_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1596598554_gshared (Collection_1_t419563673 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1596598554(__this, method) ((  bool (*) (Collection_1_t419563673 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1596598554_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m171221989_gshared (Collection_1_t419563673 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m171221989(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t419563673 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m171221989_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1577507800_gshared (Collection_1_t419563673 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1577507800(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t419563673 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1577507800_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::Add(T)
extern "C"  void Collection_1_Add_m3098302973_gshared (Collection_1_t419563673 * __this, PendingOwner_t877818919  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3098302973(__this, ___item0, method) ((  void (*) (Collection_1_t419563673 *, PendingOwner_t877818919 , const MethodInfo*))Collection_1_Add_m3098302973_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::Clear()
extern "C"  void Collection_1_Clear_m647571841_gshared (Collection_1_t419563673 * __this, const MethodInfo* method);
#define Collection_1_Clear_m647571841(__this, method) ((  void (*) (Collection_1_t419563673 *, const MethodInfo*))Collection_1_Clear_m647571841_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3001720831_gshared (Collection_1_t419563673 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3001720831(__this, method) ((  void (*) (Collection_1_t419563673 *, const MethodInfo*))Collection_1_ClearItems_m3001720831_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::Contains(T)
extern "C"  bool Collection_1_Contains_m2444893587_gshared (Collection_1_t419563673 * __this, PendingOwner_t877818919  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2444893587(__this, ___item0, method) ((  bool (*) (Collection_1_t419563673 *, PendingOwner_t877818919 , const MethodInfo*))Collection_1_Contains_m2444893587_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1601376909_gshared (Collection_1_t419563673 * __this, PendingOwnerU5BU5D_t2720009054* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m1601376909(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t419563673 *, PendingOwnerU5BU5D_t2720009054*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1601376909_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1562448626_gshared (Collection_1_t419563673 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1562448626(__this, method) ((  Il2CppObject* (*) (Collection_1_t419563673 *, const MethodInfo*))Collection_1_GetEnumerator_m1562448626_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m837129033_gshared (Collection_1_t419563673 * __this, PendingOwner_t877818919  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m837129033(__this, ___item0, method) ((  int32_t (*) (Collection_1_t419563673 *, PendingOwner_t877818919 , const MethodInfo*))Collection_1_IndexOf_m837129033_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m765079352_gshared (Collection_1_t419563673 * __this, int32_t ___index0, PendingOwner_t877818919  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m765079352(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t419563673 *, int32_t, PendingOwner_t877818919 , const MethodInfo*))Collection_1_Insert_m765079352_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m16044995_gshared (Collection_1_t419563673 * __this, int32_t ___index0, PendingOwner_t877818919  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m16044995(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t419563673 *, int32_t, PendingOwner_t877818919 , const MethodInfo*))Collection_1_InsertItem_m16044995_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::Remove(T)
extern "C"  bool Collection_1_Remove_m1182325982_gshared (Collection_1_t419563673 * __this, PendingOwner_t877818919  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1182325982(__this, ___item0, method) ((  bool (*) (Collection_1_t419563673 *, PendingOwner_t877818919 , const MethodInfo*))Collection_1_Remove_m1182325982_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1363603132_gshared (Collection_1_t419563673 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1363603132(__this, ___index0, method) ((  void (*) (Collection_1_t419563673 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1363603132_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2562030214_gshared (Collection_1_t419563673 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2562030214(__this, ___index0, method) ((  void (*) (Collection_1_t419563673 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2562030214_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1908219246_gshared (Collection_1_t419563673 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1908219246(__this, method) ((  int32_t (*) (Collection_1_t419563673 *, const MethodInfo*))Collection_1_get_Count_m1908219246_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::get_Item(System.Int32)
extern "C"  PendingOwner_t877818919  Collection_1_get_Item_m617562986_gshared (Collection_1_t419563673 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m617562986(__this, ___index0, method) ((  PendingOwner_t877818919  (*) (Collection_1_t419563673 *, int32_t, const MethodInfo*))Collection_1_get_Item_m617562986_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3839543069_gshared (Collection_1_t419563673 * __this, int32_t ___index0, PendingOwner_t877818919  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3839543069(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t419563673 *, int32_t, PendingOwner_t877818919 , const MethodInfo*))Collection_1_set_Item_m3839543069_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2400101976_gshared (Collection_1_t419563673 * __this, int32_t ___index0, PendingOwner_t877818919  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2400101976(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t419563673 *, int32_t, PendingOwner_t877818919 , const MethodInfo*))Collection_1_SetItem_m2400101976_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2860619423_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2860619423(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2860619423_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::ConvertItem(System.Object)
extern "C"  PendingOwner_t877818919  Collection_1_ConvertItem_m1742175727_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1742175727(__this /* static, unused */, ___item0, method) ((  PendingOwner_t877818919  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1742175727_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m4240240275_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m4240240275(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m4240240275_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3288355427_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3288355427(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3288355427_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ClientScene/PendingOwner>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1169345524_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1169345524(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1169345524_gshared)(__this /* static, unused */, ___list0, method)
