﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_g1022910149MethodDeclarations.h"

// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::.ctor()
#define HashSet_1__ctor_m2536992517(__this, method) ((  void (*) (HashSet_1_t2735696056 *, const MethodInfo*))HashSet_1__ctor_m2858247305_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define HashSet_1__ctor_m3327122835(__this, ___collection0, method) ((  void (*) (HashSet_1_t2735696056 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m3869181306_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1__ctor_m3158333247(__this, ___collection0, ___comparer1, method) ((  void (*) (HashSet_1_t2735696056 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m1859650088_gshared)(__this, ___collection0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1__ctor_m2582370131(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t2735696056 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1__ctor_m3582855242_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3204309304(__this, method) ((  Il2CppObject* (*) (HashSet_1_t2735696056 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m788997721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3433772417(__this, method) ((  bool (*) (HashSet_1_t2735696056 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2633171492_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m3968432085(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t2735696056 *, NetworkConnectionU5BU5D_t214055287*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m1933244740_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::System.Collections.Generic.ICollection<T>.Add(T)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3327181757(__this, ___item0, method) ((  void (*) (HashSet_1_t2735696056 *, NetworkConnection_t107267906 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3632050820_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::System.Collections.IEnumerable.GetEnumerator()
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m902747877(__this, method) ((  Il2CppObject * (*) (HashSet_1_t2735696056 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m2498631708_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::get_Count()
#define HashSet_1_get_Count_m1348174556(__this, method) ((  int32_t (*) (HashSet_1_t2735696056 *, const MethodInfo*))HashSet_1_get_Count_m4103055329_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1_Init_m590289111(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t2735696056 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m1258286688_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::InitArrays(System.Int32)
#define HashSet_1_InitArrays_m3556980597(__this, ___size0, method) ((  void (*) (HashSet_1_t2735696056 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m1536879844_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::SlotsContainsAt(System.Int32,System.Int32,T)
#define HashSet_1_SlotsContainsAt_m997797313(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t2735696056 *, int32_t, int32_t, NetworkConnection_t107267906 *, const MethodInfo*))HashSet_1_SlotsContainsAt_m219342270_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::CopyTo(T[],System.Int32)
#define HashSet_1_CopyTo_m1522604441(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t2735696056 *, NetworkConnectionU5BU5D_t214055287*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m1750586488_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::CopyTo(T[],System.Int32,System.Int32)
#define HashSet_1_CopyTo_m2857878968(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t2735696056 *, NetworkConnectionU5BU5D_t214055287*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m4175866709_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::Resize()
#define HashSet_1_Resize_m1234211432(__this, method) ((  void (*) (HashSet_1_t2735696056 *, const MethodInfo*))HashSet_1_Resize_m1435308491_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::GetLinkHashCode(System.Int32)
#define HashSet_1_GetLinkHashCode_m2576679084(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t2735696056 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m3972670595_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::GetItemHashCode(T)
#define HashSet_1_GetItemHashCode_m919635604(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t2735696056 *, NetworkConnection_t107267906 *, const MethodInfo*))HashSet_1_GetItemHashCode_m433445195_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::Add(T)
#define HashSet_1_Add_m3764407609(__this, ___item0, method) ((  bool (*) (HashSet_1_t2735696056 *, NetworkConnection_t107267906 *, const MethodInfo*))HashSet_1_Add_m2918921714_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::Clear()
#define HashSet_1_Clear_m1749383013(__this, method) ((  void (*) (HashSet_1_t2735696056 *, const MethodInfo*))HashSet_1_Clear_m350367572_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::Contains(T)
#define HashSet_1_Contains_m2542781915(__this, ___item0, method) ((  bool (*) (HashSet_1_t2735696056 *, NetworkConnection_t107267906 *, const MethodInfo*))HashSet_1_Contains_m1075264948_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::Remove(T)
#define HashSet_1_Remove_m189450304(__this, ___item0, method) ((  bool (*) (HashSet_1_t2735696056 *, NetworkConnection_t107267906 *, const MethodInfo*))HashSet_1_Remove_m4157587527_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1_GetObjectData_m1666460562(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t2735696056 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1_GetObjectData_m2935317189_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::OnDeserialization(System.Object)
#define HashSet_1_OnDeserialization_m733346108(__this, ___sender0, method) ((  void (*) (HashSet_1_t2735696056 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m1222146673_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkConnection>::GetEnumerator()
#define HashSet_1_GetEnumerator_m472556770(__this, method) ((  Enumerator_t1224011898  (*) (HashSet_1_t2735696056 *, const MethodInfo*))HashSet_1_GetEnumerator_m623886159_gshared)(__this, method)
