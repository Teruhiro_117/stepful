﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23479205885MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.SpawnDelegate>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m976066382(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4117622576 *, NetworkHash128_t835211239 , SpawnDelegate_t3327865986 *, const MethodInfo*))KeyValuePair_2__ctor_m4239967155_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.SpawnDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m2078563936(__this, method) ((  NetworkHash128_t835211239  (*) (KeyValuePair_2_t4117622576 *, const MethodInfo*))KeyValuePair_2_get_Key_m857379697_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.SpawnDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3218803571(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4117622576 *, NetworkHash128_t835211239 , const MethodInfo*))KeyValuePair_2_set_Key_m833328204_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.SpawnDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m3798865704(__this, method) ((  SpawnDelegate_t3327865986 * (*) (KeyValuePair_2_t4117622576 *, const MethodInfo*))KeyValuePair_2_get_Value_m3149317201_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.SpawnDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m790622027(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4117622576 *, SpawnDelegate_t3327865986 *, const MethodInfo*))KeyValuePair_2_set_Value_m443154412_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.SpawnDelegate>::ToString()
#define KeyValuePair_2_ToString_m812018849(__this, method) ((  String_t* (*) (KeyValuePair_2_t4117622576 *, const MethodInfo*))KeyValuePair_2_ToString_m1004546060_gshared)(__this, method)
