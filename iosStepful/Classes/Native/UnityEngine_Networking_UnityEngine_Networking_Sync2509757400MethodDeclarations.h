﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.SyncVarAttribute
struct SyncVarAttribute_t2509757400;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.SyncVarAttribute::.ctor()
extern "C"  void SyncVarAttribute__ctor_m4216095403 (SyncVarAttribute_t2509757400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
