﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.ClientRpcAttribute
struct ClientRpcAttribute_t2297656918;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.ClientRpcAttribute::.ctor()
extern "C"  void ClientRpcAttribute__ctor_m3115780649 (ClientRpcAttribute_t2297656918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
