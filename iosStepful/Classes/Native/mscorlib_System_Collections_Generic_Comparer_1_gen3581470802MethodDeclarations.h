﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct Comparer_1_t3581470802;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.ctor()
extern "C"  void Comparer_1__ctor_m3008791092_gshared (Comparer_1_t3581470802 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m3008791092(__this, method) ((  void (*) (Comparer_1_t3581470802 *, const MethodInfo*))Comparer_1__ctor_m3008791092_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.cctor()
extern "C"  void Comparer_1__cctor_m3995779827_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m3995779827(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m3995779827_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m4058351987_gshared (Comparer_1_t3581470802 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m4058351987(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t3581470802 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m4058351987_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::get_Default()
extern "C"  Comparer_1_t3581470802 * Comparer_1_get_Default_m900255896_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m900255896(__this /* static, unused */, method) ((  Comparer_1_t3581470802 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m900255896_gshared)(__this /* static, unused */, method)
