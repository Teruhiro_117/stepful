﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct List_1_t4060582815;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3595312489.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo396494387.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m271251706_gshared (Enumerator_t3595312489 * __this, List_1_t4060582815 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m271251706(__this, ___l0, method) ((  void (*) (Enumerator_t3595312489 *, List_1_t4060582815 *, const MethodInfo*))Enumerator__ctor_m271251706_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2908971816_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2908971816(__this, method) ((  void (*) (Enumerator_t3595312489 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2908971816_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m714227684_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m714227684(__this, method) ((  Il2CppObject * (*) (Enumerator_t3595312489 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m714227684_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Dispose()
extern "C"  void Enumerator_Dispose_m3825679319_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3825679319(__this, method) ((  void (*) (Enumerator_t3595312489 *, const MethodInfo*))Enumerator_Dispose_m3825679319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3601227088_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3601227088(__this, method) ((  void (*) (Enumerator_t3595312489 *, const MethodInfo*))Enumerator_VerifyState_m3601227088_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3774002024_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3774002024(__this, method) ((  bool (*) (Enumerator_t3595312489 *, const MethodInfo*))Enumerator_MoveNext_m3774002024_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::get_Current()
extern "C"  PeerInfoPlayer_t396494387  Enumerator_get_Current_m2503784191_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2503784191(__this, method) ((  PeerInfoPlayer_t396494387  (*) (Enumerator_t3595312489 *, const MethodInfo*))Enumerator_get_Current_m2503784191_gshared)(__this, method)
