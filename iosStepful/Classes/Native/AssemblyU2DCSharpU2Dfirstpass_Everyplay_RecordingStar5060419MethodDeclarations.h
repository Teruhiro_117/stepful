﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/RecordingStartedDelegate
struct RecordingStartedDelegate_t5060419;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/RecordingStartedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void RecordingStartedDelegate__ctor_m1828386050 (RecordingStartedDelegate_t5060419 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/RecordingStartedDelegate::Invoke()
extern "C"  void RecordingStartedDelegate_Invoke_m3318412280 (RecordingStartedDelegate_t5060419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/RecordingStartedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RecordingStartedDelegate_BeginInvoke_m1131019807 (RecordingStartedDelegate_t5060419 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/RecordingStartedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void RecordingStartedDelegate_EndInvoke_m2512124704 (RecordingStartedDelegate_t5060419 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
