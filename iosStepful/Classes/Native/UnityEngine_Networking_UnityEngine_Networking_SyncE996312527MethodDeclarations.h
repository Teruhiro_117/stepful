﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.SyncEventAttribute
struct SyncEventAttribute_t996312527;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.SyncEventAttribute::.ctor()
extern "C"  void SyncEventAttribute__ctor_m96705918 (SyncEventAttribute_t996312527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
