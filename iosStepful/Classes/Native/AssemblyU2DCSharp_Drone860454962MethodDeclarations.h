﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Drone
struct Drone_t860454962;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Drone::.ctor()
extern "C"  void Drone__ctor_m960290629 (Drone_t860454962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Drone::Init(System.String)
extern "C"  void Drone_Init_m760530005 (Drone_t860454962 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Drone::Move(System.Int32,System.Int32)
extern "C"  void Drone_Move_m1067477320 (Drone_t860454962 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
