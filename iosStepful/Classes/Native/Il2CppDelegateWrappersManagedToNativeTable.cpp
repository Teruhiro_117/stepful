﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t3898244613 ();
extern "C" void DelegatePInvokeWrapper_Swapper_t2637371637 ();
extern "C" void DelegatePInvokeWrapper_InternalCancelHandler_t2895223116 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t3184826381 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t489908132 ();
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t754146990 ();
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t362827733 ();
extern "C" void DelegatePInvokeWrapper_ThreadStart_t3437517264 ();
extern "C" void DelegatePInvokeWrapper_CharGetter_t1955031820 ();
extern "C" void DelegatePInvokeWrapper_ReadMethod_t3362229488 ();
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745 ();
extern "C" void DelegatePInvokeWrapper_WriteMethod_t1894833619 ();
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t1559754630 ();
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t888270799 ();
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t3737776727 ();
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1824458113 ();
extern "C" void DelegatePInvokeWrapper_LogCallback_t1867914413 ();
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t3007145346 ();
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554 ();
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033 ();
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3522132132 ();
extern "C" void DelegatePInvokeWrapper_StateChanged_t2480912210 ();
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815 ();
extern "C" void DelegatePInvokeWrapper_UnityAction_t4025899511 ();
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033 ();
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3486805455 ();
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336 ();
extern "C" void DelegatePInvokeWrapper_BasicResponseDelegate_t1347328110 ();
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180 ();
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t1946318473 ();
extern "C" void DelegatePInvokeWrapper_ClientMoveCallback2D_t1738312059 ();
extern "C" void DelegatePInvokeWrapper_ClientMoveCallback3D_t1738312058 ();
extern "C" void DelegatePInvokeWrapper_FaceCamRecordingPermissionDelegate_t1670731619 ();
extern "C" void DelegatePInvokeWrapper_FaceCamSessionStartedDelegate_t1733547424 ();
extern "C" void DelegatePInvokeWrapper_FaceCamSessionStoppedDelegate_t1894731428 ();
extern "C" void DelegatePInvokeWrapper_ReadyForRecordingDelegate_t1593758596 ();
extern "C" void DelegatePInvokeWrapper_RecordingStartedDelegate_t5060419 ();
extern "C" void DelegatePInvokeWrapper_RecordingStoppedDelegate_t3008025639 ();
extern "C" void DelegatePInvokeWrapper_RequestFailedDelegate_t3135434785 ();
extern "C" void DelegatePInvokeWrapper_RequestReadyDelegate_t4013578405 ();
extern "C" void DelegatePInvokeWrapper_ThumbnailReadyAtTextureIdDelegate_t3853410271 ();
extern "C" void DelegatePInvokeWrapper_UploadDidCompleteDelegate_t1564565876 ();
extern "C" void DelegatePInvokeWrapper_UploadDidProgressDelegate_t2069570344 ();
extern "C" void DelegatePInvokeWrapper_UploadDidStartDelegate_t1871027361 ();
extern "C" void DelegatePInvokeWrapper_WasClosedDelegate_t946300984 ();
extern "C" void DelegatePInvokeWrapper_ButtonTapped_t3122824015 ();
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[46] = 
{
	DelegatePInvokeWrapper_AppDomainInitializer_t3898244613,
	DelegatePInvokeWrapper_Swapper_t2637371637,
	DelegatePInvokeWrapper_InternalCancelHandler_t2895223116,
	DelegatePInvokeWrapper_ReadDelegate_t3184826381,
	DelegatePInvokeWrapper_WriteDelegate_t489908132,
	DelegatePInvokeWrapper_CrossContextDelegate_t754146990,
	DelegatePInvokeWrapper_CallbackHandler_t362827733,
	DelegatePInvokeWrapper_ThreadStart_t3437517264,
	DelegatePInvokeWrapper_CharGetter_t1955031820,
	DelegatePInvokeWrapper_ReadMethod_t3362229488,
	DelegatePInvokeWrapper_UnmanagedReadOrWrite_t1990215745,
	DelegatePInvokeWrapper_WriteMethod_t1894833619,
	DelegatePInvokeWrapper_ReadDelegate_t1559754630,
	DelegatePInvokeWrapper_WriteDelegate_t888270799,
	DelegatePInvokeWrapper_SocketAsyncCall_t3737776727,
	DelegatePInvokeWrapper_CostDelegate_t1824458113,
	DelegatePInvokeWrapper_LogCallback_t1867914413,
	DelegatePInvokeWrapper_PCMReaderCallback_t3007145346,
	DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554,
	DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033,
	DelegatePInvokeWrapper_WillRenderCanvases_t3522132132,
	DelegatePInvokeWrapper_StateChanged_t2480912210,
	DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815,
	DelegatePInvokeWrapper_UnityAction_t4025899511,
	DelegatePInvokeWrapper_FontTextureRebuildCallback_t1272078033,
	DelegatePInvokeWrapper_WindowFunction_t3486805455,
	DelegatePInvokeWrapper_SkinChangedDelegate_t3594822336,
	DelegatePInvokeWrapper_BasicResponseDelegate_t1347328110,
	DelegatePInvokeWrapper_UpdatedEventHandler_t3033456180,
	DelegatePInvokeWrapper_OnValidateInput_t1946318473,
	DelegatePInvokeWrapper_ClientMoveCallback2D_t1738312059,
	DelegatePInvokeWrapper_ClientMoveCallback3D_t1738312058,
	DelegatePInvokeWrapper_FaceCamRecordingPermissionDelegate_t1670731619,
	DelegatePInvokeWrapper_FaceCamSessionStartedDelegate_t1733547424,
	DelegatePInvokeWrapper_FaceCamSessionStoppedDelegate_t1894731428,
	DelegatePInvokeWrapper_ReadyForRecordingDelegate_t1593758596,
	DelegatePInvokeWrapper_RecordingStartedDelegate_t5060419,
	DelegatePInvokeWrapper_RecordingStoppedDelegate_t3008025639,
	DelegatePInvokeWrapper_RequestFailedDelegate_t3135434785,
	DelegatePInvokeWrapper_RequestReadyDelegate_t4013578405,
	DelegatePInvokeWrapper_ThumbnailReadyAtTextureIdDelegate_t3853410271,
	DelegatePInvokeWrapper_UploadDidCompleteDelegate_t1564565876,
	DelegatePInvokeWrapper_UploadDidProgressDelegate_t2069570344,
	DelegatePInvokeWrapper_UploadDidStartDelegate_t1871027361,
	DelegatePInvokeWrapper_WasClosedDelegate_t946300984,
	DelegatePInvokeWrapper_ButtonTapped_t3122824015,
};
