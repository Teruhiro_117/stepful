﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22979060144.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Networ33998832.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m294348854_gshared (KeyValuePair_2_t2979060144 * __this, NetworkInstanceId_t33998832  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m294348854(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2979060144 *, NetworkInstanceId_t33998832 , Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m294348854_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::get_Key()
extern "C"  NetworkInstanceId_t33998832  KeyValuePair_2_get_Key_m2311441456_gshared (KeyValuePair_2_t2979060144 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2311441456(__this, method) ((  NetworkInstanceId_t33998832  (*) (KeyValuePair_2_t2979060144 *, const MethodInfo*))KeyValuePair_2_get_Key_m2311441456_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3463227267_gshared (KeyValuePair_2_t2979060144 * __this, NetworkInstanceId_t33998832  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3463227267(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2979060144 *, NetworkInstanceId_t33998832 , const MethodInfo*))KeyValuePair_2_set_Key_m3463227267_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1496119512_gshared (KeyValuePair_2_t2979060144 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1496119512(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2979060144 *, const MethodInfo*))KeyValuePair_2_get_Value_m1496119512_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1789552059_gshared (KeyValuePair_2_t2979060144 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1789552059(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2979060144 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1789552059_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m147270409_gshared (KeyValuePair_2_t2979060144 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m147270409(__this, method) ((  String_t* (*) (KeyValuePair_2_t2979060144 *, const MethodInfo*))KeyValuePair_2_ToString_m147270409_gshared)(__this, method)
