﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23479205885MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.UnSpawnDelegate>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3145386465(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t119248413 *, NetworkHash128_t835211239 , UnSpawnDelegate_t3624459119 *, const MethodInfo*))KeyValuePair_2__ctor_m4239967155_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.UnSpawnDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m1383217143(__this, method) ((  NetworkHash128_t835211239  (*) (KeyValuePair_2_t119248413 *, const MethodInfo*))KeyValuePair_2_get_Key_m857379697_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.UnSpawnDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m308960536(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t119248413 *, NetworkHash128_t835211239 , const MethodInfo*))KeyValuePair_2_set_Key_m833328204_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.UnSpawnDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m2923587583(__this, method) ((  UnSpawnDelegate_t3624459119 * (*) (KeyValuePair_2_t119248413 *, const MethodInfo*))KeyValuePair_2_get_Value_m3149317201_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.UnSpawnDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3984693072(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t119248413 *, UnSpawnDelegate_t3624459119 *, const MethodInfo*))KeyValuePair_2_set_Value_m443154412_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,UnityEngine.Networking.UnSpawnDelegate>::ToString()
#define KeyValuePair_2_ToString_m4030321982(__this, method) ((  String_t* (*) (KeyValuePair_2_t119248413 *, const MethodInfo*))KeyValuePair_2_ToString_m1004546060_gshared)(__this, method)
