﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EFactory
struct EFactory_t2050856767;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void EFactory::.ctor()
extern "C"  void EFactory__ctor_m2230763454 (EFactory_t2050856767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EFactory::Init(System.String)
extern "C"  void EFactory_Init_m3144594972 (EFactory_t2050856767 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
