﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.QosType>
struct DefaultComparer_t493162716;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_QosType2003892483.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.QosType>::.ctor()
extern "C"  void DefaultComparer__ctor_m2066156002_gshared (DefaultComparer_t493162716 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2066156002(__this, method) ((  void (*) (DefaultComparer_t493162716 *, const MethodInfo*))DefaultComparer__ctor_m2066156002_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.QosType>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1986879123_gshared (DefaultComparer_t493162716 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1986879123(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t493162716 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m1986879123_gshared)(__this, ___x0, ___y1, method)
