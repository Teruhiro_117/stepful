﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayTest
struct EveryplayTest_t137848885;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void EveryplayTest::.ctor()
extern "C"  void EveryplayTest__ctor_m127066598 (EveryplayTest_t137848885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayTest::Awake()
extern "C"  void EveryplayTest_Awake_m2134326271 (EveryplayTest_t137848885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayTest::Start()
extern "C"  void EveryplayTest_Start_m849491018 (EveryplayTest_t137848885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayTest::Destroy()
extern "C"  void EveryplayTest_Destroy_m3770768148 (EveryplayTest_t137848885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayTest::RecordingStarted()
extern "C"  void EveryplayTest_RecordingStarted_m1058971452 (EveryplayTest_t137848885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayTest::RecordingStopped()
extern "C"  void EveryplayTest_RecordingStopped_m897046588 (EveryplayTest_t137848885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayTest::CreateUploadStatusLabel()
extern "C"  void EveryplayTest_CreateUploadStatusLabel_m41225027 (EveryplayTest_t137848885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayTest::UploadDidStart(System.Int32)
extern "C"  void EveryplayTest_UploadDidStart_m1379149981 (EveryplayTest_t137848885 * __this, int32_t ___videoId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayTest::UploadDidProgress(System.Int32,System.Single)
extern "C"  void EveryplayTest_UploadDidProgress_m2604112157 (EveryplayTest_t137848885 * __this, int32_t ___videoId0, float ___progress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayTest::UploadDidComplete(System.Int32)
extern "C"  void EveryplayTest_UploadDidComplete_m399990778 (EveryplayTest_t137848885 * __this, int32_t ___videoId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EveryplayTest::ResetUploadStatusAfterDelay(System.Single)
extern "C"  Il2CppObject * EveryplayTest_ResetUploadStatusAfterDelay_m1239864648 (EveryplayTest_t137848885 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayTest::OnGUI()
extern "C"  void EveryplayTest_OnGUI_m1024825530 (EveryplayTest_t137848885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
