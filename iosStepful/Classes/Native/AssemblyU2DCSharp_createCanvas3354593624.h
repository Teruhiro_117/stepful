﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// createCanvas
struct  createCanvas_t3354593624  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 createCanvas::canvasNum
	int32_t ___canvasNum_2;
	// UnityEngine.GameObject createCanvas::prefab
	GameObject_t1756533147 * ___prefab_3;

public:
	inline static int32_t get_offset_of_canvasNum_2() { return static_cast<int32_t>(offsetof(createCanvas_t3354593624, ___canvasNum_2)); }
	inline int32_t get_canvasNum_2() const { return ___canvasNum_2; }
	inline int32_t* get_address_of_canvasNum_2() { return &___canvasNum_2; }
	inline void set_canvasNum_2(int32_t value)
	{
		___canvasNum_2 = value;
	}

	inline static int32_t get_offset_of_prefab_3() { return static_cast<int32_t>(offsetof(createCanvas_t3354593624, ___prefab_3)); }
	inline GameObject_t1756533147 * get_prefab_3() const { return ___prefab_3; }
	inline GameObject_t1756533147 ** get_address_of_prefab_3() { return &___prefab_3; }
	inline void set_prefab_3(GameObject_t1756533147 * value)
	{
		___prefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
