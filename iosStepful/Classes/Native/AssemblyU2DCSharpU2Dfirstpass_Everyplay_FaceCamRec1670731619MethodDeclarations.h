﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/FaceCamRecordingPermissionDelegate
struct FaceCamRecordingPermissionDelegate_t1670731619;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/FaceCamRecordingPermissionDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void FaceCamRecordingPermissionDelegate__ctor_m539527746 (FaceCamRecordingPermissionDelegate_t1670731619 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/FaceCamRecordingPermissionDelegate::Invoke(System.Boolean)
extern "C"  void FaceCamRecordingPermissionDelegate_Invoke_m1744694023 (FaceCamRecordingPermissionDelegate_t1670731619 * __this, bool ___granted0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/FaceCamRecordingPermissionDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FaceCamRecordingPermissionDelegate_BeginInvoke_m3142418516 (FaceCamRecordingPermissionDelegate_t1670731619 * __this, bool ___granted0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/FaceCamRecordingPermissionDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void FaceCamRecordingPermissionDelegate_EndInvoke_m3467057676 (FaceCamRecordingPermissionDelegate_t1670731619 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
