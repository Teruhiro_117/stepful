﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Networ33998832.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityEngine.Networking.NetworkInstanceId::.ctor(System.UInt32)
extern "C"  void NetworkInstanceId__ctor_m1168671435 (NetworkInstanceId_t33998832 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkInstanceId::IsEmpty()
extern "C"  bool NetworkInstanceId_IsEmpty_m1232131530 (NetworkInstanceId_t33998832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.NetworkInstanceId::GetHashCode()
extern "C"  int32_t NetworkInstanceId_GetHashCode_m2403110898 (NetworkInstanceId_t33998832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkInstanceId::Equals(System.Object)
extern "C"  bool NetworkInstanceId_Equals_m357628998 (NetworkInstanceId_t33998832 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkInstanceId::op_Equality(UnityEngine.Networking.NetworkInstanceId,UnityEngine.Networking.NetworkInstanceId)
extern "C"  bool NetworkInstanceId_op_Equality_m286200783 (Il2CppObject * __this /* static, unused */, NetworkInstanceId_t33998832  ___c10, NetworkInstanceId_t33998832  ___c21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkInstanceId::op_Inequality(UnityEngine.Networking.NetworkInstanceId,UnityEngine.Networking.NetworkInstanceId)
extern "C"  bool NetworkInstanceId_op_Inequality_m2238395340 (Il2CppObject * __this /* static, unused */, NetworkInstanceId_t33998832  ___c10, NetworkInstanceId_t33998832  ___c21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.NetworkInstanceId::ToString()
extern "C"  String_t* NetworkInstanceId_ToString_m3480450226 (NetworkInstanceId_t33998832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.Networking.NetworkInstanceId::get_Value()
extern "C"  uint32_t NetworkInstanceId_get_Value_m2334640398 (NetworkInstanceId_t33998832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkInstanceId::.cctor()
extern "C"  void NetworkInstanceId__cctor_m2477939946 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
