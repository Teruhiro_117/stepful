﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.Channels
struct Channels_t3484471830;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.Channels::.ctor()
extern "C"  void Channels__ctor_m1449956729 (Channels_t3484471830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
