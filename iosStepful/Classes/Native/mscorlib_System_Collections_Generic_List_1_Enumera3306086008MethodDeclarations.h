﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkConnection>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3344402692(__this, ___l0, method) ((  void (*) (Enumerator_t3306086008 *, List_1_t3771356334 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkConnection>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m713425566(__this, method) ((  void (*) (Enumerator_t3306086008 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkConnection>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m393791672(__this, method) ((  Il2CppObject * (*) (Enumerator_t3306086008 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkConnection>::Dispose()
#define Enumerator_Dispose_m324377885(__this, method) ((  void (*) (Enumerator_t3306086008 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkConnection>::VerifyState()
#define Enumerator_VerifyState_m1242471934(__this, method) ((  void (*) (Enumerator_t3306086008 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkConnection>::MoveNext()
#define Enumerator_MoveNext_m1359654794(__this, method) ((  bool (*) (Enumerator_t3306086008 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkConnection>::get_Current()
#define Enumerator_get_Current_m505911533(__this, method) ((  NetworkConnection_t107267906 * (*) (Enumerator_t3306086008 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
