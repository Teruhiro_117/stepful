﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct List_1_t1422497253;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct IEnumerable_1_t2345503166;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct IEnumerator_1_t3823867244;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct ICollection_1_t3005451426;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct ReadOnlyCollection_1_t2239161813;
// UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo[]
struct PendingPlayerInfoU5BU5D_t2199507332;
// System.Predicate`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct Predicate_1_t496346236;
// System.Collections.Generic.IComparer`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct IComparer_1_t7839243;
// System.Comparison`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct Comparison_1_t3315114972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2053376121.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat957226927.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::.ctor()
extern "C"  void List_1__ctor_m1095432695_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1__ctor_m1095432695(__this, method) ((  void (*) (List_1_t1422497253 *, const MethodInfo*))List_1__ctor_m1095432695_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2085936946_gshared (List_1_t1422497253 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m2085936946(__this, ___collection0, method) ((  void (*) (List_1_t1422497253 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2085936946_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3464037896_gshared (List_1_t1422497253 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3464037896(__this, ___capacity0, method) ((  void (*) (List_1_t1422497253 *, int32_t, const MethodInfo*))List_1__ctor_m3464037896_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::.cctor()
extern "C"  void List_1__cctor_m1064581148_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1064581148(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1064581148_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2887339889_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2887339889(__this, method) ((  Il2CppObject* (*) (List_1_t1422497253 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2887339889_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1373643121_gshared (List_1_t1422497253 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1373643121(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1422497253 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1373643121_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m4009250258_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m4009250258(__this, method) ((  Il2CppObject * (*) (List_1_t1422497253 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m4009250258_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1473965321_gshared (List_1_t1422497253 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1473965321(__this, ___item0, method) ((  int32_t (*) (List_1_t1422497253 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1473965321_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1528136145_gshared (List_1_t1422497253 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1528136145(__this, ___item0, method) ((  bool (*) (List_1_t1422497253 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1528136145_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m111220627_gshared (List_1_t1422497253 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m111220627(__this, ___item0, method) ((  int32_t (*) (List_1_t1422497253 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m111220627_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3167316024_gshared (List_1_t1422497253 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3167316024(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1422497253 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3167316024_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2069714338_gshared (List_1_t1422497253 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2069714338(__this, ___item0, method) ((  void (*) (List_1_t1422497253 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2069714338_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1023948898_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1023948898(__this, method) ((  bool (*) (List_1_t1422497253 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1023948898_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3930248285_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3930248285(__this, method) ((  bool (*) (List_1_t1422497253 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3930248285_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2039345277_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2039345277(__this, method) ((  Il2CppObject * (*) (List_1_t1422497253 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2039345277_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3677442832_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3677442832(__this, method) ((  bool (*) (List_1_t1422497253 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3677442832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3148147409_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3148147409(__this, method) ((  bool (*) (List_1_t1422497253 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3148147409_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m733837082_gshared (List_1_t1422497253 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m733837082(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1422497253 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m733837082_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2480518179_gshared (List_1_t1422497253 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2480518179(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1422497253 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2480518179_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Add(T)
extern "C"  void List_1_Add_m3386070779_gshared (List_1_t1422497253 * __this, PendingPlayerInfo_t2053376121  ___item0, const MethodInfo* method);
#define List_1_Add_m3386070779(__this, ___item0, method) ((  void (*) (List_1_t1422497253 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))List_1_Add_m3386070779_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m790653617_gshared (List_1_t1422497253 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m790653617(__this, ___newCount0, method) ((  void (*) (List_1_t1422497253 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m790653617_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1418623609_gshared (List_1_t1422497253 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1418623609(__this, ___collection0, method) ((  void (*) (List_1_t1422497253 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1418623609_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m4236244969_gshared (List_1_t1422497253 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m4236244969(__this, ___enumerable0, method) ((  void (*) (List_1_t1422497253 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m4236244969_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2879542978_gshared (List_1_t1422497253 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2879542978(__this, ___collection0, method) ((  void (*) (List_1_t1422497253 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2879542978_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2239161813 * List_1_AsReadOnly_m587305921_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m587305921(__this, method) ((  ReadOnlyCollection_1_t2239161813 * (*) (List_1_t1422497253 *, const MethodInfo*))List_1_AsReadOnly_m587305921_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Clear()
extern "C"  void List_1_Clear_m1695622384_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_Clear_m1695622384(__this, method) ((  void (*) (List_1_t1422497253 *, const MethodInfo*))List_1_Clear_m1695622384_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Contains(T)
extern "C"  bool List_1_Contains_m1047681518_gshared (List_1_t1422497253 * __this, PendingPlayerInfo_t2053376121  ___item0, const MethodInfo* method);
#define List_1_Contains_m1047681518(__this, ___item0, method) ((  bool (*) (List_1_t1422497253 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))List_1_Contains_m1047681518_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1813664892_gshared (List_1_t1422497253 * __this, PendingPlayerInfoU5BU5D_t2199507332* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1813664892(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1422497253 *, PendingPlayerInfoU5BU5D_t2199507332*, int32_t, const MethodInfo*))List_1_CopyTo_m1813664892_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Find(System.Predicate`1<T>)
extern "C"  PendingPlayerInfo_t2053376121  List_1_Find_m2605030586_gshared (List_1_t1422497253 * __this, Predicate_1_t496346236 * ___match0, const MethodInfo* method);
#define List_1_Find_m2605030586(__this, ___match0, method) ((  PendingPlayerInfo_t2053376121  (*) (List_1_t1422497253 *, Predicate_1_t496346236 *, const MethodInfo*))List_1_Find_m2605030586_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3313077121_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t496346236 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3313077121(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t496346236 *, const MethodInfo*))List_1_CheckMatch_m3313077121_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2527514280_gshared (List_1_t1422497253 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t496346236 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2527514280(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1422497253 *, int32_t, int32_t, Predicate_1_t496346236 *, const MethodInfo*))List_1_GetIndex_m2527514280_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::GetEnumerator()
extern "C"  Enumerator_t957226927  List_1_GetEnumerator_m1645452637_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1645452637(__this, method) ((  Enumerator_t957226927  (*) (List_1_t1422497253 *, const MethodInfo*))List_1_GetEnumerator_m1645452637_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1767410970_gshared (List_1_t1422497253 * __this, PendingPlayerInfo_t2053376121  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1767410970(__this, ___item0, method) ((  int32_t (*) (List_1_t1422497253 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))List_1_IndexOf_m1767410970_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3064604237_gshared (List_1_t1422497253 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3064604237(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1422497253 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3064604237_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2389358608_gshared (List_1_t1422497253 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2389358608(__this, ___index0, method) ((  void (*) (List_1_t1422497253 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2389358608_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3709442219_gshared (List_1_t1422497253 * __this, int32_t ___index0, PendingPlayerInfo_t2053376121  ___item1, const MethodInfo* method);
#define List_1_Insert_m3709442219(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1422497253 *, int32_t, PendingPlayerInfo_t2053376121 , const MethodInfo*))List_1_Insert_m3709442219_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3611129398_gshared (List_1_t1422497253 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3611129398(__this, ___collection0, method) ((  void (*) (List_1_t1422497253 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3611129398_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Remove(T)
extern "C"  bool List_1_Remove_m282160479_gshared (List_1_t1422497253 * __this, PendingPlayerInfo_t2053376121  ___item0, const MethodInfo* method);
#define List_1_Remove_m282160479(__this, ___item0, method) ((  bool (*) (List_1_t1422497253 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))List_1_Remove_m282160479_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m4255030145_gshared (List_1_t1422497253 * __this, Predicate_1_t496346236 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m4255030145(__this, ___match0, method) ((  int32_t (*) (List_1_t1422497253 *, Predicate_1_t496346236 *, const MethodInfo*))List_1_RemoveAll_m4255030145_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m609856935_gshared (List_1_t1422497253 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m609856935(__this, ___index0, method) ((  void (*) (List_1_t1422497253 *, int32_t, const MethodInfo*))List_1_RemoveAt_m609856935_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Reverse()
extern "C"  void List_1_Reverse_m2485224717_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_Reverse_m2485224717(__this, method) ((  void (*) (List_1_t1422497253 *, const MethodInfo*))List_1_Reverse_m2485224717_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Sort()
extern "C"  void List_1_Sort_m3803710023_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_Sort_m3803710023(__this, method) ((  void (*) (List_1_t1422497253 *, const MethodInfo*))List_1_Sort_m3803710023_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2404812283_gshared (List_1_t1422497253 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2404812283(__this, ___comparer0, method) ((  void (*) (List_1_t1422497253 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2404812283_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m357907940_gshared (List_1_t1422497253 * __this, Comparison_1_t3315114972 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m357907940(__this, ___comparison0, method) ((  void (*) (List_1_t1422497253 *, Comparison_1_t3315114972 *, const MethodInfo*))List_1_Sort_m357907940_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::ToArray()
extern "C"  PendingPlayerInfoU5BU5D_t2199507332* List_1_ToArray_m227812798_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_ToArray_m227812798(__this, method) ((  PendingPlayerInfoU5BU5D_t2199507332* (*) (List_1_t1422497253 *, const MethodInfo*))List_1_ToArray_m227812798_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3518896478_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3518896478(__this, method) ((  void (*) (List_1_t1422497253 *, const MethodInfo*))List_1_TrimExcess_m3518896478_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3138464440_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3138464440(__this, method) ((  int32_t (*) (List_1_t1422497253 *, const MethodInfo*))List_1_get_Capacity_m3138464440_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m1838320569_gshared (List_1_t1422497253 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m1838320569(__this, ___value0, method) ((  void (*) (List_1_t1422497253 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1838320569_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::get_Count()
extern "C"  int32_t List_1_get_Count_m1094108843_gshared (List_1_t1422497253 * __this, const MethodInfo* method);
#define List_1_get_Count_m1094108843(__this, method) ((  int32_t (*) (List_1_t1422497253 *, const MethodInfo*))List_1_get_Count_m1094108843_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::get_Item(System.Int32)
extern "C"  PendingPlayerInfo_t2053376121  List_1_get_Item_m1996569842_gshared (List_1_t1422497253 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1996569842(__this, ___index0, method) ((  PendingPlayerInfo_t2053376121  (*) (List_1_t1422497253 *, int32_t, const MethodInfo*))List_1_get_Item_m1996569842_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m289326702_gshared (List_1_t1422497253 * __this, int32_t ___index0, PendingPlayerInfo_t2053376121  ___value1, const MethodInfo* method);
#define List_1_set_Item_m289326702(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1422497253 *, int32_t, PendingPlayerInfo_t2053376121 , const MethodInfo*))List_1_set_Item_m289326702_gshared)(__this, ___index0, ___value1, method)
