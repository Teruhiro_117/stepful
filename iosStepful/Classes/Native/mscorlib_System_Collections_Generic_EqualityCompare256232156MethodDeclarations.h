﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Networking.ChannelPacket>
struct EqualityComparer_1_t256232156;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Networking.ChannelPacket>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2849099357_gshared (EqualityComparer_1_t256232156 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m2849099357(__this, method) ((  void (*) (EqualityComparer_1_t256232156 *, const MethodInfo*))EqualityComparer_1__ctor_m2849099357_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Networking.ChannelPacket>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m89952510_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m89952510(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m89952510_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2842579304_gshared (EqualityComparer_1_t256232156 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2842579304(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t256232156 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m2842579304_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2607750222_gshared (EqualityComparer_1_t256232156 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2607750222(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t256232156 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2607750222_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Networking.ChannelPacket>::get_Default()
extern "C"  EqualityComparer_1_t256232156 * EqualityComparer_1_get_Default_m759719289_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m759719289(__this /* static, unused */, method) ((  EqualityComparer_1_t256232156 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m759719289_gshared)(__this /* static, unused */, method)
