﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.Match.JoinMatchRequest
struct JoinMatchRequest_t2675294872;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID348058649.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Networking.Match.JoinMatchRequest::.ctor()
extern "C"  void JoinMatchRequest__ctor_m1575542428 (JoinMatchRequest_t2675294872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.JoinMatchRequest::get_networkId()
extern "C"  uint64_t JoinMatchRequest_get_networkId_m1419222228 (JoinMatchRequest_t2675294872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C"  void JoinMatchRequest_set_networkId_m1567162539 (JoinMatchRequest_t2675294872 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.JoinMatchRequest::get_publicAddress()
extern "C"  String_t* JoinMatchRequest_get_publicAddress_m3108727683 (JoinMatchRequest_t2675294872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::set_publicAddress(System.String)
extern "C"  void JoinMatchRequest_set_publicAddress_m116537710 (JoinMatchRequest_t2675294872 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.JoinMatchRequest::get_privateAddress()
extern "C"  String_t* JoinMatchRequest_get_privateAddress_m3628464923 (JoinMatchRequest_t2675294872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::set_privateAddress(System.String)
extern "C"  void JoinMatchRequest_set_privateAddress_m61154936 (JoinMatchRequest_t2675294872 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.Match.JoinMatchRequest::get_eloScore()
extern "C"  int32_t JoinMatchRequest_get_eloScore_m398142605 (JoinMatchRequest_t2675294872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::set_eloScore(System.Int32)
extern "C"  void JoinMatchRequest_set_eloScore_m161913154 (JoinMatchRequest_t2675294872 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.JoinMatchRequest::get_password()
extern "C"  String_t* JoinMatchRequest_get_password_m2253508543 (JoinMatchRequest_t2675294872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::set_password(System.String)
extern "C"  void JoinMatchRequest_set_password_m2599961280 (JoinMatchRequest_t2675294872 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.JoinMatchRequest::ToString()
extern "C"  String_t* JoinMatchRequest_ToString_m762501327 (JoinMatchRequest_t2675294872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
