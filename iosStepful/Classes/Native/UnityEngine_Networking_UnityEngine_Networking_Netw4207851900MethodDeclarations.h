﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers
struct ConnectionPendingPlayers_t4207851900;
struct ConnectionPendingPlayers_t4207851900_marshaled_pinvoke;
struct ConnectionPendingPlayers_t4207851900_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ConnectionPendingPlayers_t4207851900;
struct ConnectionPendingPlayers_t4207851900_marshaled_pinvoke;

extern "C" void ConnectionPendingPlayers_t4207851900_marshal_pinvoke(const ConnectionPendingPlayers_t4207851900& unmarshaled, ConnectionPendingPlayers_t4207851900_marshaled_pinvoke& marshaled);
extern "C" void ConnectionPendingPlayers_t4207851900_marshal_pinvoke_back(const ConnectionPendingPlayers_t4207851900_marshaled_pinvoke& marshaled, ConnectionPendingPlayers_t4207851900& unmarshaled);
extern "C" void ConnectionPendingPlayers_t4207851900_marshal_pinvoke_cleanup(ConnectionPendingPlayers_t4207851900_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ConnectionPendingPlayers_t4207851900;
struct ConnectionPendingPlayers_t4207851900_marshaled_com;

extern "C" void ConnectionPendingPlayers_t4207851900_marshal_com(const ConnectionPendingPlayers_t4207851900& unmarshaled, ConnectionPendingPlayers_t4207851900_marshaled_com& marshaled);
extern "C" void ConnectionPendingPlayers_t4207851900_marshal_com_back(const ConnectionPendingPlayers_t4207851900_marshaled_com& marshaled, ConnectionPendingPlayers_t4207851900& unmarshaled);
extern "C" void ConnectionPendingPlayers_t4207851900_marshal_com_cleanup(ConnectionPendingPlayers_t4207851900_marshaled_com& marshaled);
