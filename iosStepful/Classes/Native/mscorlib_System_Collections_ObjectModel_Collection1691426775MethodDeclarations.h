﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.UInt32>
struct Collection_1_t1691426775;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t3920173144;
// System.Collections.Generic.IList`1<System.UInt32>
struct IList_1_t2690622622;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::.ctor()
extern "C"  void Collection_1__ctor_m2369803333_gshared (Collection_1_t1691426775 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2369803333(__this, method) ((  void (*) (Collection_1_t1691426775 *, const MethodInfo*))Collection_1__ctor_m2369803333_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2669568840_gshared (Collection_1_t1691426775 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2669568840(__this, method) ((  bool (*) (Collection_1_t1691426775 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2669568840_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3340067445_gshared (Collection_1_t1691426775 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3340067445(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1691426775 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3340067445_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3123605148_gshared (Collection_1_t1691426775 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3123605148(__this, method) ((  Il2CppObject * (*) (Collection_1_t1691426775 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3123605148_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1982385145_gshared (Collection_1_t1691426775 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1982385145(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1691426775 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1982385145_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3142736245_gshared (Collection_1_t1691426775 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m3142736245(__this, ___value0, method) ((  bool (*) (Collection_1_t1691426775 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m3142736245_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2360997715_gshared (Collection_1_t1691426775 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2360997715(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1691426775 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2360997715_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1319493978_gshared (Collection_1_t1691426775 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1319493978(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1691426775 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1319493978_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3990320392_gshared (Collection_1_t1691426775 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3990320392(__this, ___value0, method) ((  void (*) (Collection_1_t1691426775 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3990320392_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m929007081_gshared (Collection_1_t1691426775 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m929007081(__this, method) ((  bool (*) (Collection_1_t1691426775 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m929007081_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3871445029_gshared (Collection_1_t1691426775 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3871445029(__this, method) ((  Il2CppObject * (*) (Collection_1_t1691426775 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3871445029_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1692260090_gshared (Collection_1_t1691426775 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1692260090(__this, method) ((  bool (*) (Collection_1_t1691426775 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1692260090_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1361327037_gshared (Collection_1_t1691426775 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1361327037(__this, method) ((  bool (*) (Collection_1_t1691426775 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1361327037_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3050176238_gshared (Collection_1_t1691426775 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3050176238(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1691426775 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3050176238_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m214442963_gshared (Collection_1_t1691426775 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m214442963(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1691426775 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m214442963_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::Add(T)
extern "C"  void Collection_1_Add_m4145190934_gshared (Collection_1_t1691426775 * __this, uint32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m4145190934(__this, ___item0, method) ((  void (*) (Collection_1_t1691426775 *, uint32_t, const MethodInfo*))Collection_1_Add_m4145190934_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::Clear()
extern "C"  void Collection_1_Clear_m2952806802_gshared (Collection_1_t1691426775 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2952806802(__this, method) ((  void (*) (Collection_1_t1691426775 *, const MethodInfo*))Collection_1_Clear_m2952806802_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::ClearItems()
extern "C"  void Collection_1_ClearItems_m117993164_gshared (Collection_1_t1691426775 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m117993164(__this, method) ((  void (*) (Collection_1_t1691426775 *, const MethodInfo*))Collection_1_ClearItems_m117993164_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::Contains(T)
extern "C"  bool Collection_1_Contains_m1514852452_gshared (Collection_1_t1691426775 * __this, uint32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1514852452(__this, ___item0, method) ((  bool (*) (Collection_1_t1691426775 *, uint32_t, const MethodInfo*))Collection_1_Contains_m1514852452_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2455841318_gshared (Collection_1_t1691426775 * __this, UInt32U5BU5D_t59386216* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2455841318(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1691426775 *, UInt32U5BU5D_t59386216*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2455841318_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.UInt32>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m400881177_gshared (Collection_1_t1691426775 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m400881177(__this, method) ((  Il2CppObject* (*) (Collection_1_t1691426775 *, const MethodInfo*))Collection_1_GetEnumerator_m400881177_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt32>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m183303572_gshared (Collection_1_t1691426775 * __this, uint32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m183303572(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1691426775 *, uint32_t, const MethodInfo*))Collection_1_IndexOf_m183303572_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3938007579_gshared (Collection_1_t1691426775 * __this, int32_t ___index0, uint32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3938007579(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1691426775 *, int32_t, uint32_t, const MethodInfo*))Collection_1_Insert_m3938007579_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3716625136_gshared (Collection_1_t1691426775 * __this, int32_t ___index0, uint32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3716625136(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1691426775 *, int32_t, uint32_t, const MethodInfo*))Collection_1_InsertItem_m3716625136_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::Remove(T)
extern "C"  bool Collection_1_Remove_m3563583391_gshared (Collection_1_t1691426775 * __this, uint32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3563583391(__this, ___item0, method) ((  bool (*) (Collection_1_t1691426775 *, uint32_t, const MethodInfo*))Collection_1_Remove_m3563583391_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m207809239_gshared (Collection_1_t1691426775 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m207809239(__this, ___index0, method) ((  void (*) (Collection_1_t1691426775 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m207809239_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2650682139_gshared (Collection_1_t1691426775 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2650682139(__this, ___index0, method) ((  void (*) (Collection_1_t1691426775 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2650682139_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt32>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m462330153_gshared (Collection_1_t1691426775 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m462330153(__this, method) ((  int32_t (*) (Collection_1_t1691426775 *, const MethodInfo*))Collection_1_get_Count_m462330153_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.UInt32>::get_Item(System.Int32)
extern "C"  uint32_t Collection_1_get_Item_m2004342327_gshared (Collection_1_t1691426775 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2004342327(__this, ___index0, method) ((  uint32_t (*) (Collection_1_t1691426775 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2004342327_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m295541284_gshared (Collection_1_t1691426775 * __this, int32_t ___index0, uint32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m295541284(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1691426775 *, int32_t, uint32_t, const MethodInfo*))Collection_1_set_Item_m295541284_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m510404627_gshared (Collection_1_t1691426775 * __this, int32_t ___index0, uint32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m510404627(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1691426775 *, int32_t, uint32_t, const MethodInfo*))Collection_1_SetItem_m510404627_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1071296894_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1071296894(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1071296894_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.UInt32>::ConvertItem(System.Object)
extern "C"  uint32_t Collection_1_ConvertItem_m3830079140_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3830079140(__this /* static, unused */, ___item0, method) ((  uint32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3830079140_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1105878702_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1105878702(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1105878702_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3590297948_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3590297948(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3590297948_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m996141123_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m996141123(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m996141123_gshared)(__this /* static, unused */, ___list0, method)
