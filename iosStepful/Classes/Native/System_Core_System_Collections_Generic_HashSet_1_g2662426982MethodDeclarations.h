﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>
struct HashSet_1_t2662426982;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.NetworkInstanceId>
struct IEnumerable_1_t326125877;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Networking.NetworkInstanceId>
struct IEqualityComparer_1_t3541598906;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkInstanceId>
struct IEnumerator_1_t1804489955;
// UnityEngine.Networking.NetworkInstanceId[]
struct NetworkInstanceIdU5BU5D_t2259872337;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Networ33998832.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E1150742824.h"

// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::.ctor()
extern "C"  void HashSet_1__ctor_m3576640791_gshared (HashSet_1_t2662426982 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m3576640791(__this, method) ((  void (*) (HashSet_1_t2662426982 *, const MethodInfo*))HashSet_1__ctor_m3576640791_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void HashSet_1__ctor_m3959264641_gshared (HashSet_1_t2662426982 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define HashSet_1__ctor_m3959264641(__this, ___collection0, method) ((  void (*) (HashSet_1_t2662426982 *, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m3959264641_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1__ctor_m2047844613_gshared (HashSet_1_t2662426982 * __this, Il2CppObject* ___collection0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1__ctor_m2047844613(__this, ___collection0, ___comparer1, method) ((  void (*) (HashSet_1_t2662426982 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))HashSet_1__ctor_m2047844613_gshared)(__this, ___collection0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1__ctor_m683226841_gshared (HashSet_1_t2662426982 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define HashSet_1__ctor_m683226841(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t2662426982 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1__ctor_m683226841_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3271565554_gshared (HashSet_1_t2662426982 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3271565554(__this, method) ((  Il2CppObject* (*) (HashSet_1_t2662426982 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3271565554_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m643216723_gshared (HashSet_1_t2662426982 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m643216723(__this, method) ((  bool (*) (HashSet_1_t2662426982 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m643216723_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m186658939_gshared (HashSet_1_t2662426982 * __this, NetworkInstanceIdU5BU5D_t2259872337* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m186658939(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t2662426982 *, NetworkInstanceIdU5BU5D_t2259872337*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m186658939_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3104159799_gshared (HashSet_1_t2662426982 * __this, NetworkInstanceId_t33998832  ___item0, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3104159799(__this, ___item0, method) ((  void (*) (HashSet_1_t2662426982 *, NetworkInstanceId_t33998832 , const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3104159799_gshared)(__this, ___item0, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m3675498935_gshared (HashSet_1_t2662426982 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m3675498935(__this, method) ((  Il2CppObject * (*) (HashSet_1_t2662426982 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m3675498935_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::get_Count()
extern "C"  int32_t HashSet_1_get_Count_m893898755_gshared (HashSet_1_t2662426982 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m893898755(__this, method) ((  int32_t (*) (HashSet_1_t2662426982 *, const MethodInfo*))HashSet_1_get_Count_m893898755_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C"  void HashSet_1_Init_m1281713661_gshared (HashSet_1_t2662426982 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define HashSet_1_Init_m1281713661(__this, ___capacity0, ___comparer1, method) ((  void (*) (HashSet_1_t2662426982 *, int32_t, Il2CppObject*, const MethodInfo*))HashSet_1_Init_m1281713661_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::InitArrays(System.Int32)
extern "C"  void HashSet_1_InitArrays_m2798888987_gshared (HashSet_1_t2662426982 * __this, int32_t ___size0, const MethodInfo* method);
#define HashSet_1_InitArrays_m2798888987(__this, ___size0, method) ((  void (*) (HashSet_1_t2662426982 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m2798888987_gshared)(__this, ___size0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C"  bool HashSet_1_SlotsContainsAt_m3522660943_gshared (HashSet_1_t2662426982 * __this, int32_t ___index0, int32_t ___hash1, NetworkInstanceId_t33998832  ___item2, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m3522660943(__this, ___index0, ___hash1, ___item2, method) ((  bool (*) (HashSet_1_t2662426982 *, int32_t, int32_t, NetworkInstanceId_t33998832 , const MethodInfo*))HashSet_1_SlotsContainsAt_m3522660943_gshared)(__this, ___index0, ___hash1, ___item2, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::CopyTo(T[],System.Int32)
extern "C"  void HashSet_1_CopyTo_m1763324595_gshared (HashSet_1_t2662426982 * __this, NetworkInstanceIdU5BU5D_t2259872337* ___array0, int32_t ___index1, const MethodInfo* method);
#define HashSet_1_CopyTo_m1763324595(__this, ___array0, ___index1, method) ((  void (*) (HashSet_1_t2662426982 *, NetworkInstanceIdU5BU5D_t2259872337*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m1763324595_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::CopyTo(T[],System.Int32,System.Int32)
extern "C"  void HashSet_1_CopyTo_m484114206_gshared (HashSet_1_t2662426982 * __this, NetworkInstanceIdU5BU5D_t2259872337* ___array0, int32_t ___index1, int32_t ___count2, const MethodInfo* method);
#define HashSet_1_CopyTo_m484114206(__this, ___array0, ___index1, ___count2, method) ((  void (*) (HashSet_1_t2662426982 *, NetworkInstanceIdU5BU5D_t2259872337*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m484114206_gshared)(__this, ___array0, ___index1, ___count2, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::Resize()
extern "C"  void HashSet_1_Resize_m1236256814_gshared (HashSet_1_t2662426982 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m1236256814(__this, method) ((  void (*) (HashSet_1_t2662426982 *, const MethodInfo*))HashSet_1_Resize_m1236256814_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::GetLinkHashCode(System.Int32)
extern "C"  int32_t HashSet_1_GetLinkHashCode_m1609631514_gshared (HashSet_1_t2662426982 * __this, int32_t ___index0, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m1609631514(__this, ___index0, method) ((  int32_t (*) (HashSet_1_t2662426982 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m1609631514_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::GetItemHashCode(T)
extern "C"  int32_t HashSet_1_GetItemHashCode_m693687330_gshared (HashSet_1_t2662426982 * __this, NetworkInstanceId_t33998832  ___item0, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m693687330(__this, ___item0, method) ((  int32_t (*) (HashSet_1_t2662426982 *, NetworkInstanceId_t33998832 , const MethodInfo*))HashSet_1_GetItemHashCode_m693687330_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::Add(T)
extern "C"  bool HashSet_1_Add_m3262854879_gshared (HashSet_1_t2662426982 * __this, NetworkInstanceId_t33998832  ___item0, const MethodInfo* method);
#define HashSet_1_Add_m3262854879(__this, ___item0, method) ((  bool (*) (HashSet_1_t2662426982 *, NetworkInstanceId_t33998832 , const MethodInfo*))HashSet_1_Add_m3262854879_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::Clear()
extern "C"  void HashSet_1_Clear_m782272338_gshared (HashSet_1_t2662426982 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m782272338(__this, method) ((  void (*) (HashSet_1_t2662426982 *, const MethodInfo*))HashSet_1_Clear_m782272338_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::Contains(T)
extern "C"  bool HashSet_1_Contains_m2596966625_gshared (HashSet_1_t2662426982 * __this, NetworkInstanceId_t33998832  ___item0, const MethodInfo* method);
#define HashSet_1_Contains_m2596966625(__this, ___item0, method) ((  bool (*) (HashSet_1_t2662426982 *, NetworkInstanceId_t33998832 , const MethodInfo*))HashSet_1_Contains_m2596966625_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::Remove(T)
extern "C"  bool HashSet_1_Remove_m1547331952_gshared (HashSet_1_t2662426982 * __this, NetworkInstanceId_t33998832  ___item0, const MethodInfo* method);
#define HashSet_1_Remove_m1547331952(__this, ___item0, method) ((  bool (*) (HashSet_1_t2662426982 *, NetworkInstanceId_t33998832 , const MethodInfo*))HashSet_1_Remove_m1547331952_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void HashSet_1_GetObjectData_m3930939436_gshared (HashSet_1_t2662426982 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define HashSet_1_GetObjectData_m3930939436(__this, ___info0, ___context1, method) ((  void (*) (HashSet_1_t2662426982 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))HashSet_1_GetObjectData_m3930939436_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::OnDeserialization(System.Object)
extern "C"  void HashSet_1_OnDeserialization_m3400542914_gshared (HashSet_1_t2662426982 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m3400542914(__this, ___sender0, method) ((  void (*) (HashSet_1_t2662426982 *, Il2CppObject *, const MethodInfo*))HashSet_1_OnDeserialization_m3400542914_gshared)(__this, ___sender0, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>::GetEnumerator()
extern "C"  Enumerator_t1150742824  HashSet_1_GetEnumerator_m243666420_gshared (HashSet_1_t2662426982 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m243666420(__this, method) ((  Enumerator_t1150742824  (*) (HashSet_1_t2662426982 *, const MethodInfo*))HashSet_1_GetEnumerator_m243666420_gshared)(__this, method)
