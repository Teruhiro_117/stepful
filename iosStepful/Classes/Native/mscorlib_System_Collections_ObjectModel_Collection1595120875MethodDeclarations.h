﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct Collection_1_t1595120875;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo[]
struct PendingPlayerInfoU5BU5D_t2199507332;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct IEnumerator_1_t3823867244;
// System.Collections.Generic.IList`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct IList_1_t2594316722;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2053376121.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::.ctor()
extern "C"  void Collection_1__ctor_m3276278880_gshared (Collection_1_t1595120875 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3276278880(__this, method) ((  void (*) (Collection_1_t1595120875 *, const MethodInfo*))Collection_1__ctor_m3276278880_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3242465067_gshared (Collection_1_t1595120875 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3242465067(__this, method) ((  bool (*) (Collection_1_t1595120875 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3242465067_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m941439788_gshared (Collection_1_t1595120875 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m941439788(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1595120875 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m941439788_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1855904687_gshared (Collection_1_t1595120875 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1855904687(__this, method) ((  Il2CppObject * (*) (Collection_1_t1595120875 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1855904687_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m233742024_gshared (Collection_1_t1595120875 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m233742024(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1595120875 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m233742024_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2887575518_gshared (Collection_1_t1595120875 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2887575518(__this, ___value0, method) ((  bool (*) (Collection_1_t1595120875 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2887575518_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3686394774_gshared (Collection_1_t1595120875 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3686394774(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1595120875 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3686394774_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3677322063_gshared (Collection_1_t1595120875 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3677322063(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1595120875 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3677322063_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1363420479_gshared (Collection_1_t1595120875 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1363420479(__this, ___value0, method) ((  void (*) (Collection_1_t1595120875 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1363420479_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2833274992_gshared (Collection_1_t1595120875 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2833274992(__this, method) ((  bool (*) (Collection_1_t1595120875 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2833274992_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m425205574_gshared (Collection_1_t1595120875 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m425205574(__this, method) ((  Il2CppObject * (*) (Collection_1_t1595120875 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m425205574_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m885565903_gshared (Collection_1_t1595120875 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m885565903(__this, method) ((  bool (*) (Collection_1_t1595120875 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m885565903_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m4241511604_gshared (Collection_1_t1595120875 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m4241511604(__this, method) ((  bool (*) (Collection_1_t1595120875 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m4241511604_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3744356787_gshared (Collection_1_t1595120875 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3744356787(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1595120875 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3744356787_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3391070_gshared (Collection_1_t1595120875 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3391070(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1595120875 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3391070_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Add(T)
extern "C"  void Collection_1_Add_m1840608203_gshared (Collection_1_t1595120875 * __this, PendingPlayerInfo_t2053376121  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1840608203(__this, ___item0, method) ((  void (*) (Collection_1_t1595120875 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))Collection_1_Add_m1840608203_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Clear()
extern "C"  void Collection_1_Clear_m839534719_gshared (Collection_1_t1595120875 * __this, const MethodInfo* method);
#define Collection_1_Clear_m839534719(__this, method) ((  void (*) (Collection_1_t1595120875 *, const MethodInfo*))Collection_1_Clear_m839534719_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3682652369_gshared (Collection_1_t1595120875 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3682652369(__this, method) ((  void (*) (Collection_1_t1595120875 *, const MethodInfo*))Collection_1_ClearItems_m3682652369_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Contains(T)
extern "C"  bool Collection_1_Contains_m1973767049_gshared (Collection_1_t1595120875 * __this, PendingPlayerInfo_t2053376121  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1973767049(__this, ___item0, method) ((  bool (*) (Collection_1_t1595120875 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))Collection_1_Contains_m1973767049_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3740918663_gshared (Collection_1_t1595120875 * __this, PendingPlayerInfoU5BU5D_t2199507332* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3740918663(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1595120875 *, PendingPlayerInfoU5BU5D_t2199507332*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3740918663_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3915731452_gshared (Collection_1_t1595120875 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3915731452(__this, method) ((  Il2CppObject* (*) (Collection_1_t1595120875 *, const MethodInfo*))Collection_1_GetEnumerator_m3915731452_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1758976763_gshared (Collection_1_t1595120875 * __this, PendingPlayerInfo_t2053376121  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1758976763(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1595120875 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))Collection_1_IndexOf_m1758976763_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m401271154_gshared (Collection_1_t1595120875 * __this, int32_t ___index0, PendingPlayerInfo_t2053376121  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m401271154(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1595120875 *, int32_t, PendingPlayerInfo_t2053376121 , const MethodInfo*))Collection_1_Insert_m401271154_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1307418073_gshared (Collection_1_t1595120875 * __this, int32_t ___index0, PendingPlayerInfo_t2053376121  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m1307418073(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1595120875 *, int32_t, PendingPlayerInfo_t2053376121 , const MethodInfo*))Collection_1_InsertItem_m1307418073_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Remove(T)
extern "C"  bool Collection_1_Remove_m4007396252_gshared (Collection_1_t1595120875 * __this, PendingPlayerInfo_t2053376121  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m4007396252(__this, ___item0, method) ((  bool (*) (Collection_1_t1595120875 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))Collection_1_Remove_m4007396252_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m4209463030_gshared (Collection_1_t1595120875 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m4209463030(__this, ___index0, method) ((  void (*) (Collection_1_t1595120875 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m4209463030_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3378830960_gshared (Collection_1_t1595120875 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3378830960(__this, ___index0, method) ((  void (*) (Collection_1_t1595120875 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3378830960_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m443019016_gshared (Collection_1_t1595120875 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m443019016(__this, method) ((  int32_t (*) (Collection_1_t1595120875 *, const MethodInfo*))Collection_1_get_Count_m443019016_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::get_Item(System.Int32)
extern "C"  PendingPlayerInfo_t2053376121  Collection_1_get_Item_m1579386162_gshared (Collection_1_t1595120875 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1579386162(__this, ___index0, method) ((  PendingPlayerInfo_t2053376121  (*) (Collection_1_t1595120875 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1579386162_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2084357251_gshared (Collection_1_t1595120875 * __this, int32_t ___index0, PendingPlayerInfo_t2053376121  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2084357251(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1595120875 *, int32_t, PendingPlayerInfo_t2053376121 , const MethodInfo*))Collection_1_set_Item_m2084357251_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1603562094_gshared (Collection_1_t1595120875 * __this, int32_t ___index0, PendingPlayerInfo_t2053376121  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1603562094(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1595120875 *, int32_t, PendingPlayerInfo_t2053376121 , const MethodInfo*))Collection_1_SetItem_m1603562094_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m133826829_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m133826829(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m133826829_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::ConvertItem(System.Object)
extern "C"  PendingPlayerInfo_t2053376121  Collection_1_ConvertItem_m3512847729_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3512847729(__this /* static, unused */, ___item0, method) ((  PendingPlayerInfo_t2053376121  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3512847729_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3612225501_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3612225501(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3612225501_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3536792133_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3536792133(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3536792133_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m4275283142_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m4275283142(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m4275283142_gshared)(__this /* static, unused */, ___list0, method)
