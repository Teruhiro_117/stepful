﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkSystem.CRCMessageEntry
struct CRCMessageEntry_t524088197;
struct CRCMessageEntry_t524088197_marshaled_pinvoke;
struct CRCMessageEntry_t524088197_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct CRCMessageEntry_t524088197;
struct CRCMessageEntry_t524088197_marshaled_pinvoke;

extern "C" void CRCMessageEntry_t524088197_marshal_pinvoke(const CRCMessageEntry_t524088197& unmarshaled, CRCMessageEntry_t524088197_marshaled_pinvoke& marshaled);
extern "C" void CRCMessageEntry_t524088197_marshal_pinvoke_back(const CRCMessageEntry_t524088197_marshaled_pinvoke& marshaled, CRCMessageEntry_t524088197& unmarshaled);
extern "C" void CRCMessageEntry_t524088197_marshal_pinvoke_cleanup(CRCMessageEntry_t524088197_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct CRCMessageEntry_t524088197;
struct CRCMessageEntry_t524088197_marshaled_com;

extern "C" void CRCMessageEntry_t524088197_marshal_com(const CRCMessageEntry_t524088197& unmarshaled, CRCMessageEntry_t524088197_marshaled_com& marshaled);
extern "C" void CRCMessageEntry_t524088197_marshal_com_back(const CRCMessageEntry_t524088197_marshaled_com& marshaled, CRCMessageEntry_t524088197& unmarshaled);
extern "C" void CRCMessageEntry_t524088197_marshal_com_cleanup(CRCMessageEntry_t524088197_marshaled_com& marshaled);
