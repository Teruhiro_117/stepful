﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/FaceCamSessionStoppedDelegate
struct FaceCamSessionStoppedDelegate_t1894731428;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/FaceCamSessionStoppedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void FaceCamSessionStoppedDelegate__ctor_m4186510893 (FaceCamSessionStoppedDelegate_t1894731428 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/FaceCamSessionStoppedDelegate::Invoke()
extern "C"  void FaceCamSessionStoppedDelegate_Invoke_m3720472693 (FaceCamSessionStoppedDelegate_t1894731428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/FaceCamSessionStoppedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FaceCamSessionStoppedDelegate_BeginInvoke_m498308524 (FaceCamSessionStoppedDelegate_t1894731428 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/FaceCamSessionStoppedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void FaceCamSessionStoppedDelegate_EndInvoke_m3919409903 (FaceCamSessionStoppedDelegate_t1894731428 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
