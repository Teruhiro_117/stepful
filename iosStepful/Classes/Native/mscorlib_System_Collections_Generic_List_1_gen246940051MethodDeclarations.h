﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct List_1_t246940051;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct IEnumerable_1_t1169945964;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct IEnumerator_1_t2648310042;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct ICollection_1_t1829894224;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct ReadOnlyCollection_1_t1063604611;
// UnityEngine.Networking.ClientScene/PendingOwner[]
struct PendingOwnerU5BU5D_t2720009054;
// System.Predicate`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct Predicate_1_t3615756330;
// System.Collections.Generic.IComparer`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct IComparer_1_t3127249337;
// System.Comparison`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct Comparison_1_t2139557770;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clien877818919.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4076637021.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::.ctor()
extern "C"  void List_1__ctor_m3162040361_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1__ctor_m3162040361(__this, method) ((  void (*) (List_1_t246940051 *, const MethodInfo*))List_1__ctor_m3162040361_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3268076134_gshared (List_1_t246940051 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3268076134(__this, ___collection0, method) ((  void (*) (List_1_t246940051 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3268076134_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m477988332_gshared (List_1_t246940051 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m477988332(__this, ___capacity0, method) ((  void (*) (List_1_t246940051 *, int32_t, const MethodInfo*))List_1__ctor_m477988332_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::.cctor()
extern "C"  void List_1__cctor_m359821260_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m359821260(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m359821260_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2089512211_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2089512211(__this, method) ((  Il2CppObject* (*) (List_1_t246940051 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2089512211_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3526233575_gshared (List_1_t246940051 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3526233575(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t246940051 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3526233575_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m479155554_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m479155554(__this, method) ((  Il2CppObject * (*) (List_1_t246940051 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m479155554_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1575126471_gshared (List_1_t246940051 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1575126471(__this, ___item0, method) ((  int32_t (*) (List_1_t246940051 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1575126471_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m2527619407_gshared (List_1_t246940051 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m2527619407(__this, ___item0, method) ((  bool (*) (List_1_t246940051 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2527619407_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m4249013869_gshared (List_1_t246940051 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m4249013869(__this, ___item0, method) ((  int32_t (*) (List_1_t246940051 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m4249013869_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1131784728_gshared (List_1_t246940051 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1131784728(__this, ___index0, ___item1, method) ((  void (*) (List_1_t246940051 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1131784728_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m652688206_gshared (List_1_t246940051 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m652688206(__this, ___item0, method) ((  void (*) (List_1_t246940051 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m652688206_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m229985318_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m229985318(__this, method) ((  bool (*) (List_1_t246940051 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m229985318_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m908444551_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m908444551(__this, method) ((  bool (*) (List_1_t246940051 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m908444551_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3810252647_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3810252647(__this, method) ((  Il2CppObject * (*) (List_1_t246940051 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3810252647_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3542920764_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3542920764(__this, method) ((  bool (*) (List_1_t246940051 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3542920764_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m4260446643_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m4260446643(__this, method) ((  bool (*) (List_1_t246940051 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m4260446643_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m546674592_gshared (List_1_t246940051 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m546674592(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t246940051 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m546674592_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2638739533_gshared (List_1_t246940051 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2638739533(__this, ___index0, ___value1, method) ((  void (*) (List_1_t246940051 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2638739533_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::Add(T)
extern "C"  void List_1_Add_m4266312237_gshared (List_1_t246940051 * __this, PendingOwner_t877818919  ___item0, const MethodInfo* method);
#define List_1_Add_m4266312237(__this, ___item0, method) ((  void (*) (List_1_t246940051 *, PendingOwner_t877818919 , const MethodInfo*))List_1_Add_m4266312237_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3290690827_gshared (List_1_t246940051 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3290690827(__this, ___newCount0, method) ((  void (*) (List_1_t246940051 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3290690827_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2432717299_gshared (List_1_t246940051 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2432717299(__this, ___collection0, method) ((  void (*) (List_1_t246940051 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2432717299_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m298155283_gshared (List_1_t246940051 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m298155283(__this, ___enumerable0, method) ((  void (*) (List_1_t246940051 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m298155283_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m524250834_gshared (List_1_t246940051 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m524250834(__this, ___collection0, method) ((  void (*) (List_1_t246940051 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m524250834_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1063604611 * List_1_AsReadOnly_m2458250395_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2458250395(__this, method) ((  ReadOnlyCollection_1_t1063604611 * (*) (List_1_t246940051 *, const MethodInfo*))List_1_AsReadOnly_m2458250395_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::Clear()
extern "C"  void List_1_Clear_m1932533664_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_Clear_m1932533664(__this, method) ((  void (*) (List_1_t246940051 *, const MethodInfo*))List_1_Clear_m1932533664_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::Contains(T)
extern "C"  bool List_1_Contains_m795590290_gshared (List_1_t246940051 * __this, PendingOwner_t877818919  ___item0, const MethodInfo* method);
#define List_1_Contains_m795590290(__this, ___item0, method) ((  bool (*) (List_1_t246940051 *, PendingOwner_t877818919 , const MethodInfo*))List_1_Contains_m795590290_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1347790440_gshared (List_1_t246940051 * __this, PendingOwnerU5BU5D_t2720009054* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m1347790440(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t246940051 *, PendingOwnerU5BU5D_t2720009054*, int32_t, const MethodInfo*))List_1_CopyTo_m1347790440_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::Find(System.Predicate`1<T>)
extern "C"  PendingOwner_t877818919  List_1_Find_m1218105796_gshared (List_1_t246940051 * __this, Predicate_1_t3615756330 * ___match0, const MethodInfo* method);
#define List_1_Find_m1218105796(__this, ___match0, method) ((  PendingOwner_t877818919  (*) (List_1_t246940051 *, Predicate_1_t3615756330 *, const MethodInfo*))List_1_Find_m1218105796_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1276656919_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3615756330 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1276656919(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3615756330 *, const MethodInfo*))List_1_CheckMatch_m1276656919_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1198175612_gshared (List_1_t246940051 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3615756330 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1198175612(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t246940051 *, int32_t, int32_t, Predicate_1_t3615756330 *, const MethodInfo*))List_1_GetIndex_m1198175612_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::GetEnumerator()
extern "C"  Enumerator_t4076637021  List_1_GetEnumerator_m1957051363_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1957051363(__this, method) ((  Enumerator_t4076637021  (*) (List_1_t246940051 *, const MethodInfo*))List_1_GetEnumerator_m1957051363_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m342662630_gshared (List_1_t246940051 * __this, PendingOwner_t877818919  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m342662630(__this, ___item0, method) ((  int32_t (*) (List_1_t246940051 *, PendingOwner_t877818919 , const MethodInfo*))List_1_IndexOf_m342662630_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1593654427_gshared (List_1_t246940051 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1593654427(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t246940051 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1593654427_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2307557024_gshared (List_1_t246940051 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2307557024(__this, ___index0, method) ((  void (*) (List_1_t246940051 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2307557024_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m1875584705_gshared (List_1_t246940051 * __this, int32_t ___index0, PendingOwner_t877818919  ___item1, const MethodInfo* method);
#define List_1_Insert_m1875584705(__this, ___index0, ___item1, method) ((  void (*) (List_1_t246940051 *, int32_t, PendingOwner_t877818919 , const MethodInfo*))List_1_Insert_m1875584705_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3791715366_gshared (List_1_t246940051 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3791715366(__this, ___collection0, method) ((  void (*) (List_1_t246940051 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3791715366_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::Remove(T)
extern "C"  bool List_1_Remove_m959420073_gshared (List_1_t246940051 * __this, PendingOwner_t877818919  ___item0, const MethodInfo* method);
#define List_1_Remove_m959420073(__this, ___item0, method) ((  bool (*) (List_1_t246940051 *, PendingOwner_t877818919 , const MethodInfo*))List_1_Remove_m959420073_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1744012719_gshared (List_1_t246940051 * __this, Predicate_1_t3615756330 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1744012719(__this, ___match0, method) ((  int32_t (*) (List_1_t246940051 *, Predicate_1_t3615756330 *, const MethodInfo*))List_1_RemoveAll_m1744012719_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3957776699_gshared (List_1_t246940051 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3957776699(__this, ___index0, method) ((  void (*) (List_1_t246940051 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3957776699_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::Reverse()
extern "C"  void List_1_Reverse_m1646465395_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_Reverse_m1646465395(__this, method) ((  void (*) (List_1_t246940051 *, const MethodInfo*))List_1_Reverse_m1646465395_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::Sort()
extern "C"  void List_1_Sort_m1444776185_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_Sort_m1444776185(__this, method) ((  void (*) (List_1_t246940051 *, const MethodInfo*))List_1_Sort_m1444776185_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1169635957_gshared (List_1_t246940051 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1169635957(__this, ___comparer0, method) ((  void (*) (List_1_t246940051 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1169635957_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m16262244_gshared (List_1_t246940051 * __this, Comparison_1_t2139557770 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m16262244(__this, ___comparison0, method) ((  void (*) (List_1_t246940051 *, Comparison_1_t2139557770 *, const MethodInfo*))List_1_Sort_m16262244_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::ToArray()
extern "C"  PendingOwnerU5BU5D_t2720009054* List_1_ToArray_m2476745412_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_ToArray_m2476745412(__this, method) ((  PendingOwnerU5BU5D_t2720009054* (*) (List_1_t246940051 *, const MethodInfo*))List_1_ToArray_m2476745412_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1033725666_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1033725666(__this, method) ((  void (*) (List_1_t246940051 *, const MethodInfo*))List_1_TrimExcess_m1033725666_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2817485272_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2817485272(__this, method) ((  int32_t (*) (List_1_t246940051 *, const MethodInfo*))List_1_get_Capacity_m2817485272_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m4039662099_gshared (List_1_t246940051 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m4039662099(__this, ___value0, method) ((  void (*) (List_1_t246940051 *, int32_t, const MethodInfo*))List_1_set_Capacity_m4039662099_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::get_Count()
extern "C"  int32_t List_1_get_Count_m1683279441_gshared (List_1_t246940051 * __this, const MethodInfo* method);
#define List_1_get_Count_m1683279441(__this, method) ((  int32_t (*) (List_1_t246940051 *, const MethodInfo*))List_1_get_Count_m1683279441_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::get_Item(System.Int32)
extern "C"  PendingOwner_t877818919  List_1_get_Item_m3705080934_gshared (List_1_t246940051 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3705080934(__this, ___index0, method) ((  PendingOwner_t877818919  (*) (List_1_t246940051 *, int32_t, const MethodInfo*))List_1_get_Item_m3705080934_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1779219266_gshared (List_1_t246940051 * __this, int32_t ___index0, PendingOwner_t877818919  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1779219266(__this, ___index0, ___value1, method) ((  void (*) (List_1_t246940051 *, int32_t, PendingOwner_t877818919 , const MethodInfo*))List_1_set_Item_m1779219266_gshared)(__this, ___index0, ___value1, method)
