﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22290690628.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo646317982.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3420001604_gshared (KeyValuePair_2_t2290690628 * __this, Il2CppObject * ___key0, NetworkBroadcastResult_t646317982  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3420001604(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2290690628 *, Il2CppObject *, NetworkBroadcastResult_t646317982 , const MethodInfo*))KeyValuePair_2__ctor_m3420001604_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2967481714_gshared (KeyValuePair_2_t2290690628 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2967481714(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2290690628 *, const MethodInfo*))KeyValuePair_2_get_Key_m2967481714_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2740226929_gshared (KeyValuePair_2_t2290690628 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2740226929(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2290690628 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m2740226929_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::get_Value()
extern "C"  NetworkBroadcastResult_t646317982  KeyValuePair_2_get_Value_m838596530_gshared (KeyValuePair_2_t2290690628 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m838596530(__this, method) ((  NetworkBroadcastResult_t646317982  (*) (KeyValuePair_2_t2290690628 *, const MethodInfo*))KeyValuePair_2_get_Value_m838596530_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3010936665_gshared (KeyValuePair_2_t2290690628 * __this, NetworkBroadcastResult_t646317982  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3010936665(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2290690628 *, NetworkBroadcastResult_t646317982 , const MethodInfo*))KeyValuePair_2_set_Value_m3010936665_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2156236655_gshared (KeyValuePair_2_t2290690628 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2156236655(__this, method) ((  String_t* (*) (KeyValuePair_2_t2290690628 *, const MethodInfo*))KeyValuePair_2_ToString_m2156236655_gshared)(__this, method)
