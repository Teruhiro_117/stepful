﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct ReadOnlyCollection_1_t709873889;
// System.Collections.Generic.IList`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct IList_1_t1065028798;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.NetworkSystem.CRCMessageEntry[]
struct CRCMessageEntryU5BU5D_t2803924168;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct IEnumerator_1_t2294579320;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo524088197.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3278135227_gshared (ReadOnlyCollection_1_t709873889 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3278135227(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3278135227_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1832498225_gshared (ReadOnlyCollection_1_t709873889 * __this, CRCMessageEntry_t524088197  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1832498225(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, CRCMessageEntry_t524088197 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1832498225_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m592434973_gshared (ReadOnlyCollection_1_t709873889 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m592434973(__this, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m592434973_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3965111510_gshared (ReadOnlyCollection_1_t709873889 * __this, int32_t ___index0, CRCMessageEntry_t524088197  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3965111510(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, int32_t, CRCMessageEntry_t524088197 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3965111510_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m686371364_gshared (ReadOnlyCollection_1_t709873889 * __this, CRCMessageEntry_t524088197  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m686371364(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t709873889 *, CRCMessageEntry_t524088197 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m686371364_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3666516874_gshared (ReadOnlyCollection_1_t709873889 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3666516874(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3666516874_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  CRCMessageEntry_t524088197  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1792922206_gshared (ReadOnlyCollection_1_t709873889 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1792922206(__this, ___index0, method) ((  CRCMessageEntry_t524088197  (*) (ReadOnlyCollection_1_t709873889 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1792922206_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3380892257_gshared (ReadOnlyCollection_1_t709873889 * __this, int32_t ___index0, CRCMessageEntry_t524088197  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3380892257(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, int32_t, CRCMessageEntry_t524088197 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3380892257_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2854389581_gshared (ReadOnlyCollection_1_t709873889 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2854389581(__this, method) ((  bool (*) (ReadOnlyCollection_1_t709873889 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2854389581_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2004316880_gshared (ReadOnlyCollection_1_t709873889 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2004316880(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2004316880_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2327724469_gshared (ReadOnlyCollection_1_t709873889 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2327724469(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t709873889 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2327724469_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2379096132_gshared (ReadOnlyCollection_1_t709873889 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2379096132(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t709873889 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2379096132_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2662642114_gshared (ReadOnlyCollection_1_t709873889 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2662642114(__this, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2662642114_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2622230310_gshared (ReadOnlyCollection_1_t709873889 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2622230310(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t709873889 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2622230310_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2535567470_gshared (ReadOnlyCollection_1_t709873889 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2535567470(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t709873889 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2535567470_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1636642921_gshared (ReadOnlyCollection_1_t709873889 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1636642921(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1636642921_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1273662897_gshared (ReadOnlyCollection_1_t709873889 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1273662897(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1273662897_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4265406887_gshared (ReadOnlyCollection_1_t709873889 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4265406887(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m4265406887_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2352153096_gshared (ReadOnlyCollection_1_t709873889 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2352153096(__this, method) ((  bool (*) (ReadOnlyCollection_1_t709873889 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2352153096_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2731418646_gshared (ReadOnlyCollection_1_t709873889 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2731418646(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t709873889 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2731418646_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2216830141_gshared (ReadOnlyCollection_1_t709873889 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2216830141(__this, method) ((  bool (*) (ReadOnlyCollection_1_t709873889 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2216830141_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1037739580_gshared (ReadOnlyCollection_1_t709873889 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1037739580(__this, method) ((  bool (*) (ReadOnlyCollection_1_t709873889 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1037739580_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m278773801_gshared (ReadOnlyCollection_1_t709873889 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m278773801(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t709873889 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m278773801_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m649788466_gshared (ReadOnlyCollection_1_t709873889 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m649788466(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m649788466_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3079581203_gshared (ReadOnlyCollection_1_t709873889 * __this, CRCMessageEntry_t524088197  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3079581203(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t709873889 *, CRCMessageEntry_t524088197 , const MethodInfo*))ReadOnlyCollection_1_Contains_m3079581203_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2325757365_gshared (ReadOnlyCollection_1_t709873889 * __this, CRCMessageEntryU5BU5D_t2803924168* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2325757365(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t709873889 *, CRCMessageEntryU5BU5D_t2803924168*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2325757365_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1161778112_gshared (ReadOnlyCollection_1_t709873889 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1161778112(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t709873889 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1161778112_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2173852237_gshared (ReadOnlyCollection_1_t709873889 * __this, CRCMessageEntry_t524088197  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2173852237(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t709873889 *, CRCMessageEntry_t524088197 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2173852237_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1068777092_gshared (ReadOnlyCollection_1_t709873889 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1068777092(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t709873889 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1068777092_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::get_Item(System.Int32)
extern "C"  CRCMessageEntry_t524088197  ReadOnlyCollection_1_get_Item_m1629101890_gshared (ReadOnlyCollection_1_t709873889 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1629101890(__this, ___index0, method) ((  CRCMessageEntry_t524088197  (*) (ReadOnlyCollection_1_t709873889 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1629101890_gshared)(__this, ___index0, method)
