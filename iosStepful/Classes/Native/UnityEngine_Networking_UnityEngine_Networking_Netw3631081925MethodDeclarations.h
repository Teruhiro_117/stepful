﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkStartPosition
struct NetworkStartPosition_t3631081925;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.NetworkStartPosition::.ctor()
extern "C"  void NetworkStartPosition__ctor_m244456132 (NetworkStartPosition_t3631081925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkStartPosition::Awake()
extern "C"  void NetworkStartPosition_Awake_m2132696967 (NetworkStartPosition_t3631081925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkStartPosition::OnDestroy()
extern "C"  void NetworkStartPosition_OnDestroy_m3617007689 (NetworkStartPosition_t3631081925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
