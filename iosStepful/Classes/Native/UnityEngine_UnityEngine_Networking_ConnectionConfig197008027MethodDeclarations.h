﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.ConnectionConfigInternal
struct ConnectionConfigInternal_t197008027;
// UnityEngine.Networking.ConnectionConfig
struct ConnectionConfig_t1350910390;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_ConnectionConfi1350910390.h"
#include "UnityEngine_UnityEngine_Networking_QosType2003892483.h"

// System.Void UnityEngine.Networking.ConnectionConfigInternal::.ctor(UnityEngine.Networking.ConnectionConfig)
extern "C"  void ConnectionConfigInternal__ctor_m2143069373 (ConnectionConfigInternal_t197008027 * __this, ConnectionConfig_t1350910390 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitWrapper()
extern "C"  void ConnectionConfigInternal_InitWrapper_m2430841435 (ConnectionConfigInternal_t197008027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte UnityEngine.Networking.ConnectionConfigInternal::AddChannel(UnityEngine.Networking.QosType)
extern "C"  uint8_t ConnectionConfigInternal_AddChannel_m2585117210 (ConnectionConfigInternal_t197008027 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitPacketSize(System.UInt16)
extern "C"  void ConnectionConfigInternal_InitPacketSize_m1419524755 (ConnectionConfigInternal_t197008027 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitFragmentSize(System.UInt16)
extern "C"  void ConnectionConfigInternal_InitFragmentSize_m52508803 (ConnectionConfigInternal_t197008027 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitResendTimeout(System.UInt32)
extern "C"  void ConnectionConfigInternal_InitResendTimeout_m3396778470 (ConnectionConfigInternal_t197008027 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitDisconnectTimeout(System.UInt32)
extern "C"  void ConnectionConfigInternal_InitDisconnectTimeout_m4038970791 (ConnectionConfigInternal_t197008027 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitConnectTimeout(System.UInt32)
extern "C"  void ConnectionConfigInternal_InitConnectTimeout_m3744914173 (ConnectionConfigInternal_t197008027 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitMinUpdateTimeout(System.UInt32)
extern "C"  void ConnectionConfigInternal_InitMinUpdateTimeout_m3029345194 (ConnectionConfigInternal_t197008027 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitPingTimeout(System.UInt32)
extern "C"  void ConnectionConfigInternal_InitPingTimeout_m167717371 (ConnectionConfigInternal_t197008027 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitReducedPingTimeout(System.UInt32)
extern "C"  void ConnectionConfigInternal_InitReducedPingTimeout_m418520243 (ConnectionConfigInternal_t197008027 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitAllCostTimeout(System.UInt32)
extern "C"  void ConnectionConfigInternal_InitAllCostTimeout_m1419949717 (ConnectionConfigInternal_t197008027 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitNetworkDropThreshold(System.Byte)
extern "C"  void ConnectionConfigInternal_InitNetworkDropThreshold_m730024849 (ConnectionConfigInternal_t197008027 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitOverflowDropThreshold(System.Byte)
extern "C"  void ConnectionConfigInternal_InitOverflowDropThreshold_m3326588235 (ConnectionConfigInternal_t197008027 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitMaxConnectionAttempt(System.Byte)
extern "C"  void ConnectionConfigInternal_InitMaxConnectionAttempt_m2184519138 (ConnectionConfigInternal_t197008027 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitAckDelay(System.UInt32)
extern "C"  void ConnectionConfigInternal_InitAckDelay_m3164726582 (ConnectionConfigInternal_t197008027 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitMaxCombinedReliableMessageSize(System.UInt16)
extern "C"  void ConnectionConfigInternal_InitMaxCombinedReliableMessageSize_m2266994493 (ConnectionConfigInternal_t197008027 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitMaxCombinedReliableMessageCount(System.UInt16)
extern "C"  void ConnectionConfigInternal_InitMaxCombinedReliableMessageCount_m556578679 (ConnectionConfigInternal_t197008027 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitMaxSentMessageQueueSize(System.UInt16)
extern "C"  void ConnectionConfigInternal_InitMaxSentMessageQueueSize_m4272890321 (ConnectionConfigInternal_t197008027 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitIsAcksLong(System.Boolean)
extern "C"  void ConnectionConfigInternal_InitIsAcksLong_m1438140689 (ConnectionConfigInternal_t197008027 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitUsePlatformSpecificProtocols(System.Boolean)
extern "C"  void ConnectionConfigInternal_InitUsePlatformSpecificProtocols_m2106392184 (ConnectionConfigInternal_t197008027 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::InitWebSocketReceiveBufferMaxSize(System.UInt16)
extern "C"  void ConnectionConfigInternal_InitWebSocketReceiveBufferMaxSize_m4146041513 (ConnectionConfigInternal_t197008027 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::Dispose()
extern "C"  void ConnectionConfigInternal_Dispose_m2884529337 (ConnectionConfigInternal_t197008027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ConnectionConfigInternal::Finalize()
extern "C"  void ConnectionConfigInternal_Finalize_m1712428672 (ConnectionConfigInternal_t197008027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
