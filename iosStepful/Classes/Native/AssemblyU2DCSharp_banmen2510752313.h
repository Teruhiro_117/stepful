﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// baseMethods
struct baseMethods_t2705876163;
// Koma[0...,0...]
struct KomaU5BU2CU5D_t1205703982;
// System.Boolean[0...,0...]
struct BooleanU5BU2CU5D_t3568034316;
// System.Int32[0...,0...]
struct Int32U5BU2CU5D_t3030399642;
// UnityEngine.UI.Slider[0...,0...]
struct SliderU5BU2CU5D_t1144817635;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// Koma
struct Koma_t3011766596;
// Empty
struct Empty_t4012560223;
// Human
struct Human_t1156088493;
// GRobot
struct GRobot_t456225233;
// SRobot
struct SRobot_t456246757;
// Missile
struct Missile_t813944928;
// Factory
struct Factory_t931158954;
// Drone
struct Drone_t860454962;
// Skytree
struct Skytree_t850646749;
// EHuman
struct EHuman_t715499684;
// EGRobot
struct EGRobot_t1059295608;
// ESRobot
struct ESRobot_t2546702684;
// EMissile
struct EMissile_t3534618099;
// EFactory
struct EFactory_t2050856767;
// EDrone
struct EDrone_t2061607325;
// ESkytree
struct ESkytree_t2245705948;
// Futi
struct Futi_t612262014;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// banmen
struct  banmen_t2510752313  : public MonoBehaviour_t1158329972
{
public:
	// baseMethods banmen::baseM
	baseMethods_t2705876163 * ___baseM_2;
	// Koma[0...,0...] banmen::Board
	KomaU5BU2CU5D_t1205703982* ___Board_3;
	// System.Boolean[0...,0...] banmen::canTouchPlace
	BooleanU5BU2CU5D_t3568034316* ___canTouchPlace_4;
	// System.Boolean[0...,0...] banmen::broken
	BooleanU5BU2CU5D_t3568034316* ___broken_5;
	// System.Int32[0...,0...] banmen::barrier
	Int32U5BU2CU5D_t3030399642* ___barrier_6;
	// System.Int32[0...,0...] banmen::factoryLocation
	Int32U5BU2CU5D_t3030399642* ___factoryLocation_7;
	// System.Int32[0...,0...] banmen::humanPosition
	Int32U5BU2CU5D_t3030399642* ___humanPosition_8;
	// System.Int32[0...,0...] banmen::komaTime
	Int32U5BU2CU5D_t3030399642* ___komaTime_9;
	// UnityEngine.UI.Slider[0...,0...] banmen::komaSlider
	SliderU5BU2CU5D_t1144817635* ___komaSlider_10;
	// System.Int32[0...,0...] banmen::EkomaTime
	Int32U5BU2CU5D_t3030399642* ___EkomaTime_11;
	// UnityEngine.UI.Slider[0...,0...] banmen::EkomaSlider
	SliderU5BU2CU5D_t1144817635* ___EkomaSlider_12;
	// System.Boolean banmen::arrangeMode
	bool ___arrangeMode_13;
	// System.Boolean banmen::arrange1
	bool ___arrange1_14;
	// System.Boolean banmen::arrange2
	bool ___arrange2_15;
	// System.Boolean banmen::humanExist
	bool ___humanExist_16;
	// System.Boolean banmen::winLose
	bool ___winLose_17;
	// UnityEngine.UI.Image banmen::img
	Image_t2042527209 * ___img_18;
	// UnityEngine.UI.Text banmen::text
	Text_t356221433 * ___text_19;
	// System.Int32 banmen::line
	int32_t ___line_20;
	// System.Int32 banmen::row
	int32_t ___row_21;
	// System.Int32 banmen::halfOfKoma
	int32_t ___halfOfKoma_22;
	// System.Int32 banmen::bl
	int32_t ___bl_23;
	// System.Int32 banmen::br
	int32_t ___br_24;
	// System.Int32 banmen::al
	int32_t ___al_25;
	// System.Int32 banmen::ar
	int32_t ___ar_26;
	// System.Single banmen::changeRed
	float ___changeRed_27;
	// System.Single banmen::changeGreen
	float ___changeGreen_28;
	// System.Single banmen::changeBlue
	float ___changeBlue_29;
	// System.Single banmen::changeAlpha
	float ___changeAlpha_30;
	// System.Boolean banmen::enemy
	bool ___enemy_31;
	// System.Boolean banmen::wall
	bool ___wall_32;
	// System.Boolean banmen::satelite
	bool ___satelite_33;
	// System.Boolean banmen::Esatelite
	bool ___Esatelite_34;
	// Koma banmen::kariKoma
	Koma_t3011766596 * ___kariKoma_35;
	// System.Int32 banmen::kariKomaNum
	int32_t ___kariKomaNum_36;
	// System.Int32 banmen::myFactoryTime
	int32_t ___myFactoryTime_37;
	// System.Int32 banmen::enemyFactoryTime
	int32_t ___enemyFactoryTime_38;
	// System.Int32 banmen::myResources
	int32_t ___myResources_39;
	// System.Int32 banmen::enemyResources
	int32_t ___enemyResources_40;
	// Empty banmen::empty
	Empty_t4012560223 * ___empty_41;
	// Human banmen::human
	Human_t1156088493 * ___human_42;
	// GRobot banmen::gRobot
	GRobot_t456225233 * ___gRobot_43;
	// SRobot banmen::sRobot
	SRobot_t456246757 * ___sRobot_44;
	// Missile banmen::missile
	Missile_t813944928 * ___missile_45;
	// Factory banmen::factory
	Factory_t931158954 * ___factory_46;
	// Drone banmen::drone
	Drone_t860454962 * ___drone_47;
	// Skytree banmen::skytree
	Skytree_t850646749 * ___skytree_48;
	// EHuman banmen::Ehuman
	EHuman_t715499684 * ___Ehuman_49;
	// EGRobot banmen::EgRobot
	EGRobot_t1059295608 * ___EgRobot_50;
	// ESRobot banmen::EsRobot
	ESRobot_t2546702684 * ___EsRobot_51;
	// EMissile banmen::Emissile
	EMissile_t3534618099 * ___Emissile_52;
	// EFactory banmen::Efactory
	EFactory_t2050856767 * ___Efactory_53;
	// EDrone banmen::Edrone
	EDrone_t2061607325 * ___Edrone_54;
	// ESkytree banmen::Eskytree
	ESkytree_t2245705948 * ___Eskytree_55;
	// Futi banmen::futi
	Futi_t612262014 * ___futi_56;

public:
	inline static int32_t get_offset_of_baseM_2() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___baseM_2)); }
	inline baseMethods_t2705876163 * get_baseM_2() const { return ___baseM_2; }
	inline baseMethods_t2705876163 ** get_address_of_baseM_2() { return &___baseM_2; }
	inline void set_baseM_2(baseMethods_t2705876163 * value)
	{
		___baseM_2 = value;
		Il2CppCodeGenWriteBarrier(&___baseM_2, value);
	}

	inline static int32_t get_offset_of_Board_3() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___Board_3)); }
	inline KomaU5BU2CU5D_t1205703982* get_Board_3() const { return ___Board_3; }
	inline KomaU5BU2CU5D_t1205703982** get_address_of_Board_3() { return &___Board_3; }
	inline void set_Board_3(KomaU5BU2CU5D_t1205703982* value)
	{
		___Board_3 = value;
		Il2CppCodeGenWriteBarrier(&___Board_3, value);
	}

	inline static int32_t get_offset_of_canTouchPlace_4() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___canTouchPlace_4)); }
	inline BooleanU5BU2CU5D_t3568034316* get_canTouchPlace_4() const { return ___canTouchPlace_4; }
	inline BooleanU5BU2CU5D_t3568034316** get_address_of_canTouchPlace_4() { return &___canTouchPlace_4; }
	inline void set_canTouchPlace_4(BooleanU5BU2CU5D_t3568034316* value)
	{
		___canTouchPlace_4 = value;
		Il2CppCodeGenWriteBarrier(&___canTouchPlace_4, value);
	}

	inline static int32_t get_offset_of_broken_5() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___broken_5)); }
	inline BooleanU5BU2CU5D_t3568034316* get_broken_5() const { return ___broken_5; }
	inline BooleanU5BU2CU5D_t3568034316** get_address_of_broken_5() { return &___broken_5; }
	inline void set_broken_5(BooleanU5BU2CU5D_t3568034316* value)
	{
		___broken_5 = value;
		Il2CppCodeGenWriteBarrier(&___broken_5, value);
	}

	inline static int32_t get_offset_of_barrier_6() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___barrier_6)); }
	inline Int32U5BU2CU5D_t3030399642* get_barrier_6() const { return ___barrier_6; }
	inline Int32U5BU2CU5D_t3030399642** get_address_of_barrier_6() { return &___barrier_6; }
	inline void set_barrier_6(Int32U5BU2CU5D_t3030399642* value)
	{
		___barrier_6 = value;
		Il2CppCodeGenWriteBarrier(&___barrier_6, value);
	}

	inline static int32_t get_offset_of_factoryLocation_7() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___factoryLocation_7)); }
	inline Int32U5BU2CU5D_t3030399642* get_factoryLocation_7() const { return ___factoryLocation_7; }
	inline Int32U5BU2CU5D_t3030399642** get_address_of_factoryLocation_7() { return &___factoryLocation_7; }
	inline void set_factoryLocation_7(Int32U5BU2CU5D_t3030399642* value)
	{
		___factoryLocation_7 = value;
		Il2CppCodeGenWriteBarrier(&___factoryLocation_7, value);
	}

	inline static int32_t get_offset_of_humanPosition_8() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___humanPosition_8)); }
	inline Int32U5BU2CU5D_t3030399642* get_humanPosition_8() const { return ___humanPosition_8; }
	inline Int32U5BU2CU5D_t3030399642** get_address_of_humanPosition_8() { return &___humanPosition_8; }
	inline void set_humanPosition_8(Int32U5BU2CU5D_t3030399642* value)
	{
		___humanPosition_8 = value;
		Il2CppCodeGenWriteBarrier(&___humanPosition_8, value);
	}

	inline static int32_t get_offset_of_komaTime_9() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___komaTime_9)); }
	inline Int32U5BU2CU5D_t3030399642* get_komaTime_9() const { return ___komaTime_9; }
	inline Int32U5BU2CU5D_t3030399642** get_address_of_komaTime_9() { return &___komaTime_9; }
	inline void set_komaTime_9(Int32U5BU2CU5D_t3030399642* value)
	{
		___komaTime_9 = value;
		Il2CppCodeGenWriteBarrier(&___komaTime_9, value);
	}

	inline static int32_t get_offset_of_komaSlider_10() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___komaSlider_10)); }
	inline SliderU5BU2CU5D_t1144817635* get_komaSlider_10() const { return ___komaSlider_10; }
	inline SliderU5BU2CU5D_t1144817635** get_address_of_komaSlider_10() { return &___komaSlider_10; }
	inline void set_komaSlider_10(SliderU5BU2CU5D_t1144817635* value)
	{
		___komaSlider_10 = value;
		Il2CppCodeGenWriteBarrier(&___komaSlider_10, value);
	}

	inline static int32_t get_offset_of_EkomaTime_11() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___EkomaTime_11)); }
	inline Int32U5BU2CU5D_t3030399642* get_EkomaTime_11() const { return ___EkomaTime_11; }
	inline Int32U5BU2CU5D_t3030399642** get_address_of_EkomaTime_11() { return &___EkomaTime_11; }
	inline void set_EkomaTime_11(Int32U5BU2CU5D_t3030399642* value)
	{
		___EkomaTime_11 = value;
		Il2CppCodeGenWriteBarrier(&___EkomaTime_11, value);
	}

	inline static int32_t get_offset_of_EkomaSlider_12() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___EkomaSlider_12)); }
	inline SliderU5BU2CU5D_t1144817635* get_EkomaSlider_12() const { return ___EkomaSlider_12; }
	inline SliderU5BU2CU5D_t1144817635** get_address_of_EkomaSlider_12() { return &___EkomaSlider_12; }
	inline void set_EkomaSlider_12(SliderU5BU2CU5D_t1144817635* value)
	{
		___EkomaSlider_12 = value;
		Il2CppCodeGenWriteBarrier(&___EkomaSlider_12, value);
	}

	inline static int32_t get_offset_of_arrangeMode_13() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___arrangeMode_13)); }
	inline bool get_arrangeMode_13() const { return ___arrangeMode_13; }
	inline bool* get_address_of_arrangeMode_13() { return &___arrangeMode_13; }
	inline void set_arrangeMode_13(bool value)
	{
		___arrangeMode_13 = value;
	}

	inline static int32_t get_offset_of_arrange1_14() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___arrange1_14)); }
	inline bool get_arrange1_14() const { return ___arrange1_14; }
	inline bool* get_address_of_arrange1_14() { return &___arrange1_14; }
	inline void set_arrange1_14(bool value)
	{
		___arrange1_14 = value;
	}

	inline static int32_t get_offset_of_arrange2_15() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___arrange2_15)); }
	inline bool get_arrange2_15() const { return ___arrange2_15; }
	inline bool* get_address_of_arrange2_15() { return &___arrange2_15; }
	inline void set_arrange2_15(bool value)
	{
		___arrange2_15 = value;
	}

	inline static int32_t get_offset_of_humanExist_16() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___humanExist_16)); }
	inline bool get_humanExist_16() const { return ___humanExist_16; }
	inline bool* get_address_of_humanExist_16() { return &___humanExist_16; }
	inline void set_humanExist_16(bool value)
	{
		___humanExist_16 = value;
	}

	inline static int32_t get_offset_of_winLose_17() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___winLose_17)); }
	inline bool get_winLose_17() const { return ___winLose_17; }
	inline bool* get_address_of_winLose_17() { return &___winLose_17; }
	inline void set_winLose_17(bool value)
	{
		___winLose_17 = value;
	}

	inline static int32_t get_offset_of_img_18() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___img_18)); }
	inline Image_t2042527209 * get_img_18() const { return ___img_18; }
	inline Image_t2042527209 ** get_address_of_img_18() { return &___img_18; }
	inline void set_img_18(Image_t2042527209 * value)
	{
		___img_18 = value;
		Il2CppCodeGenWriteBarrier(&___img_18, value);
	}

	inline static int32_t get_offset_of_text_19() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___text_19)); }
	inline Text_t356221433 * get_text_19() const { return ___text_19; }
	inline Text_t356221433 ** get_address_of_text_19() { return &___text_19; }
	inline void set_text_19(Text_t356221433 * value)
	{
		___text_19 = value;
		Il2CppCodeGenWriteBarrier(&___text_19, value);
	}

	inline static int32_t get_offset_of_line_20() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___line_20)); }
	inline int32_t get_line_20() const { return ___line_20; }
	inline int32_t* get_address_of_line_20() { return &___line_20; }
	inline void set_line_20(int32_t value)
	{
		___line_20 = value;
	}

	inline static int32_t get_offset_of_row_21() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___row_21)); }
	inline int32_t get_row_21() const { return ___row_21; }
	inline int32_t* get_address_of_row_21() { return &___row_21; }
	inline void set_row_21(int32_t value)
	{
		___row_21 = value;
	}

	inline static int32_t get_offset_of_halfOfKoma_22() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___halfOfKoma_22)); }
	inline int32_t get_halfOfKoma_22() const { return ___halfOfKoma_22; }
	inline int32_t* get_address_of_halfOfKoma_22() { return &___halfOfKoma_22; }
	inline void set_halfOfKoma_22(int32_t value)
	{
		___halfOfKoma_22 = value;
	}

	inline static int32_t get_offset_of_bl_23() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___bl_23)); }
	inline int32_t get_bl_23() const { return ___bl_23; }
	inline int32_t* get_address_of_bl_23() { return &___bl_23; }
	inline void set_bl_23(int32_t value)
	{
		___bl_23 = value;
	}

	inline static int32_t get_offset_of_br_24() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___br_24)); }
	inline int32_t get_br_24() const { return ___br_24; }
	inline int32_t* get_address_of_br_24() { return &___br_24; }
	inline void set_br_24(int32_t value)
	{
		___br_24 = value;
	}

	inline static int32_t get_offset_of_al_25() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___al_25)); }
	inline int32_t get_al_25() const { return ___al_25; }
	inline int32_t* get_address_of_al_25() { return &___al_25; }
	inline void set_al_25(int32_t value)
	{
		___al_25 = value;
	}

	inline static int32_t get_offset_of_ar_26() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___ar_26)); }
	inline int32_t get_ar_26() const { return ___ar_26; }
	inline int32_t* get_address_of_ar_26() { return &___ar_26; }
	inline void set_ar_26(int32_t value)
	{
		___ar_26 = value;
	}

	inline static int32_t get_offset_of_changeRed_27() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___changeRed_27)); }
	inline float get_changeRed_27() const { return ___changeRed_27; }
	inline float* get_address_of_changeRed_27() { return &___changeRed_27; }
	inline void set_changeRed_27(float value)
	{
		___changeRed_27 = value;
	}

	inline static int32_t get_offset_of_changeGreen_28() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___changeGreen_28)); }
	inline float get_changeGreen_28() const { return ___changeGreen_28; }
	inline float* get_address_of_changeGreen_28() { return &___changeGreen_28; }
	inline void set_changeGreen_28(float value)
	{
		___changeGreen_28 = value;
	}

	inline static int32_t get_offset_of_changeBlue_29() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___changeBlue_29)); }
	inline float get_changeBlue_29() const { return ___changeBlue_29; }
	inline float* get_address_of_changeBlue_29() { return &___changeBlue_29; }
	inline void set_changeBlue_29(float value)
	{
		___changeBlue_29 = value;
	}

	inline static int32_t get_offset_of_changeAlpha_30() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___changeAlpha_30)); }
	inline float get_changeAlpha_30() const { return ___changeAlpha_30; }
	inline float* get_address_of_changeAlpha_30() { return &___changeAlpha_30; }
	inline void set_changeAlpha_30(float value)
	{
		___changeAlpha_30 = value;
	}

	inline static int32_t get_offset_of_enemy_31() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___enemy_31)); }
	inline bool get_enemy_31() const { return ___enemy_31; }
	inline bool* get_address_of_enemy_31() { return &___enemy_31; }
	inline void set_enemy_31(bool value)
	{
		___enemy_31 = value;
	}

	inline static int32_t get_offset_of_wall_32() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___wall_32)); }
	inline bool get_wall_32() const { return ___wall_32; }
	inline bool* get_address_of_wall_32() { return &___wall_32; }
	inline void set_wall_32(bool value)
	{
		___wall_32 = value;
	}

	inline static int32_t get_offset_of_satelite_33() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___satelite_33)); }
	inline bool get_satelite_33() const { return ___satelite_33; }
	inline bool* get_address_of_satelite_33() { return &___satelite_33; }
	inline void set_satelite_33(bool value)
	{
		___satelite_33 = value;
	}

	inline static int32_t get_offset_of_Esatelite_34() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___Esatelite_34)); }
	inline bool get_Esatelite_34() const { return ___Esatelite_34; }
	inline bool* get_address_of_Esatelite_34() { return &___Esatelite_34; }
	inline void set_Esatelite_34(bool value)
	{
		___Esatelite_34 = value;
	}

	inline static int32_t get_offset_of_kariKoma_35() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___kariKoma_35)); }
	inline Koma_t3011766596 * get_kariKoma_35() const { return ___kariKoma_35; }
	inline Koma_t3011766596 ** get_address_of_kariKoma_35() { return &___kariKoma_35; }
	inline void set_kariKoma_35(Koma_t3011766596 * value)
	{
		___kariKoma_35 = value;
		Il2CppCodeGenWriteBarrier(&___kariKoma_35, value);
	}

	inline static int32_t get_offset_of_kariKomaNum_36() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___kariKomaNum_36)); }
	inline int32_t get_kariKomaNum_36() const { return ___kariKomaNum_36; }
	inline int32_t* get_address_of_kariKomaNum_36() { return &___kariKomaNum_36; }
	inline void set_kariKomaNum_36(int32_t value)
	{
		___kariKomaNum_36 = value;
	}

	inline static int32_t get_offset_of_myFactoryTime_37() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___myFactoryTime_37)); }
	inline int32_t get_myFactoryTime_37() const { return ___myFactoryTime_37; }
	inline int32_t* get_address_of_myFactoryTime_37() { return &___myFactoryTime_37; }
	inline void set_myFactoryTime_37(int32_t value)
	{
		___myFactoryTime_37 = value;
	}

	inline static int32_t get_offset_of_enemyFactoryTime_38() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___enemyFactoryTime_38)); }
	inline int32_t get_enemyFactoryTime_38() const { return ___enemyFactoryTime_38; }
	inline int32_t* get_address_of_enemyFactoryTime_38() { return &___enemyFactoryTime_38; }
	inline void set_enemyFactoryTime_38(int32_t value)
	{
		___enemyFactoryTime_38 = value;
	}

	inline static int32_t get_offset_of_myResources_39() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___myResources_39)); }
	inline int32_t get_myResources_39() const { return ___myResources_39; }
	inline int32_t* get_address_of_myResources_39() { return &___myResources_39; }
	inline void set_myResources_39(int32_t value)
	{
		___myResources_39 = value;
	}

	inline static int32_t get_offset_of_enemyResources_40() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___enemyResources_40)); }
	inline int32_t get_enemyResources_40() const { return ___enemyResources_40; }
	inline int32_t* get_address_of_enemyResources_40() { return &___enemyResources_40; }
	inline void set_enemyResources_40(int32_t value)
	{
		___enemyResources_40 = value;
	}

	inline static int32_t get_offset_of_empty_41() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___empty_41)); }
	inline Empty_t4012560223 * get_empty_41() const { return ___empty_41; }
	inline Empty_t4012560223 ** get_address_of_empty_41() { return &___empty_41; }
	inline void set_empty_41(Empty_t4012560223 * value)
	{
		___empty_41 = value;
		Il2CppCodeGenWriteBarrier(&___empty_41, value);
	}

	inline static int32_t get_offset_of_human_42() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___human_42)); }
	inline Human_t1156088493 * get_human_42() const { return ___human_42; }
	inline Human_t1156088493 ** get_address_of_human_42() { return &___human_42; }
	inline void set_human_42(Human_t1156088493 * value)
	{
		___human_42 = value;
		Il2CppCodeGenWriteBarrier(&___human_42, value);
	}

	inline static int32_t get_offset_of_gRobot_43() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___gRobot_43)); }
	inline GRobot_t456225233 * get_gRobot_43() const { return ___gRobot_43; }
	inline GRobot_t456225233 ** get_address_of_gRobot_43() { return &___gRobot_43; }
	inline void set_gRobot_43(GRobot_t456225233 * value)
	{
		___gRobot_43 = value;
		Il2CppCodeGenWriteBarrier(&___gRobot_43, value);
	}

	inline static int32_t get_offset_of_sRobot_44() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___sRobot_44)); }
	inline SRobot_t456246757 * get_sRobot_44() const { return ___sRobot_44; }
	inline SRobot_t456246757 ** get_address_of_sRobot_44() { return &___sRobot_44; }
	inline void set_sRobot_44(SRobot_t456246757 * value)
	{
		___sRobot_44 = value;
		Il2CppCodeGenWriteBarrier(&___sRobot_44, value);
	}

	inline static int32_t get_offset_of_missile_45() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___missile_45)); }
	inline Missile_t813944928 * get_missile_45() const { return ___missile_45; }
	inline Missile_t813944928 ** get_address_of_missile_45() { return &___missile_45; }
	inline void set_missile_45(Missile_t813944928 * value)
	{
		___missile_45 = value;
		Il2CppCodeGenWriteBarrier(&___missile_45, value);
	}

	inline static int32_t get_offset_of_factory_46() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___factory_46)); }
	inline Factory_t931158954 * get_factory_46() const { return ___factory_46; }
	inline Factory_t931158954 ** get_address_of_factory_46() { return &___factory_46; }
	inline void set_factory_46(Factory_t931158954 * value)
	{
		___factory_46 = value;
		Il2CppCodeGenWriteBarrier(&___factory_46, value);
	}

	inline static int32_t get_offset_of_drone_47() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___drone_47)); }
	inline Drone_t860454962 * get_drone_47() const { return ___drone_47; }
	inline Drone_t860454962 ** get_address_of_drone_47() { return &___drone_47; }
	inline void set_drone_47(Drone_t860454962 * value)
	{
		___drone_47 = value;
		Il2CppCodeGenWriteBarrier(&___drone_47, value);
	}

	inline static int32_t get_offset_of_skytree_48() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___skytree_48)); }
	inline Skytree_t850646749 * get_skytree_48() const { return ___skytree_48; }
	inline Skytree_t850646749 ** get_address_of_skytree_48() { return &___skytree_48; }
	inline void set_skytree_48(Skytree_t850646749 * value)
	{
		___skytree_48 = value;
		Il2CppCodeGenWriteBarrier(&___skytree_48, value);
	}

	inline static int32_t get_offset_of_Ehuman_49() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___Ehuman_49)); }
	inline EHuman_t715499684 * get_Ehuman_49() const { return ___Ehuman_49; }
	inline EHuman_t715499684 ** get_address_of_Ehuman_49() { return &___Ehuman_49; }
	inline void set_Ehuman_49(EHuman_t715499684 * value)
	{
		___Ehuman_49 = value;
		Il2CppCodeGenWriteBarrier(&___Ehuman_49, value);
	}

	inline static int32_t get_offset_of_EgRobot_50() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___EgRobot_50)); }
	inline EGRobot_t1059295608 * get_EgRobot_50() const { return ___EgRobot_50; }
	inline EGRobot_t1059295608 ** get_address_of_EgRobot_50() { return &___EgRobot_50; }
	inline void set_EgRobot_50(EGRobot_t1059295608 * value)
	{
		___EgRobot_50 = value;
		Il2CppCodeGenWriteBarrier(&___EgRobot_50, value);
	}

	inline static int32_t get_offset_of_EsRobot_51() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___EsRobot_51)); }
	inline ESRobot_t2546702684 * get_EsRobot_51() const { return ___EsRobot_51; }
	inline ESRobot_t2546702684 ** get_address_of_EsRobot_51() { return &___EsRobot_51; }
	inline void set_EsRobot_51(ESRobot_t2546702684 * value)
	{
		___EsRobot_51 = value;
		Il2CppCodeGenWriteBarrier(&___EsRobot_51, value);
	}

	inline static int32_t get_offset_of_Emissile_52() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___Emissile_52)); }
	inline EMissile_t3534618099 * get_Emissile_52() const { return ___Emissile_52; }
	inline EMissile_t3534618099 ** get_address_of_Emissile_52() { return &___Emissile_52; }
	inline void set_Emissile_52(EMissile_t3534618099 * value)
	{
		___Emissile_52 = value;
		Il2CppCodeGenWriteBarrier(&___Emissile_52, value);
	}

	inline static int32_t get_offset_of_Efactory_53() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___Efactory_53)); }
	inline EFactory_t2050856767 * get_Efactory_53() const { return ___Efactory_53; }
	inline EFactory_t2050856767 ** get_address_of_Efactory_53() { return &___Efactory_53; }
	inline void set_Efactory_53(EFactory_t2050856767 * value)
	{
		___Efactory_53 = value;
		Il2CppCodeGenWriteBarrier(&___Efactory_53, value);
	}

	inline static int32_t get_offset_of_Edrone_54() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___Edrone_54)); }
	inline EDrone_t2061607325 * get_Edrone_54() const { return ___Edrone_54; }
	inline EDrone_t2061607325 ** get_address_of_Edrone_54() { return &___Edrone_54; }
	inline void set_Edrone_54(EDrone_t2061607325 * value)
	{
		___Edrone_54 = value;
		Il2CppCodeGenWriteBarrier(&___Edrone_54, value);
	}

	inline static int32_t get_offset_of_Eskytree_55() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___Eskytree_55)); }
	inline ESkytree_t2245705948 * get_Eskytree_55() const { return ___Eskytree_55; }
	inline ESkytree_t2245705948 ** get_address_of_Eskytree_55() { return &___Eskytree_55; }
	inline void set_Eskytree_55(ESkytree_t2245705948 * value)
	{
		___Eskytree_55 = value;
		Il2CppCodeGenWriteBarrier(&___Eskytree_55, value);
	}

	inline static int32_t get_offset_of_futi_56() { return static_cast<int32_t>(offsetof(banmen_t2510752313, ___futi_56)); }
	inline Futi_t612262014 * get_futi_56() const { return ___futi_56; }
	inline Futi_t612262014 ** get_address_of_futi_56() { return &___futi_56; }
	inline void set_futi_56(Futi_t612262014 * value)
	{
		___futi_56 = value;
		Il2CppCodeGenWriteBarrier(&___futi_56, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
