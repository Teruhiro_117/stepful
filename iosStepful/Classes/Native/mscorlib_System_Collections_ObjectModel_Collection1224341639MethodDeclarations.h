﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>
struct Collection_1_t1224341639;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.ChannelPacket[]
struct ChannelPacketU5BU5D_t3883591672;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.ChannelPacket>
struct IEnumerator_1_t3453088008;
// System.Collections.Generic.IList`1<UnityEngine.Networking.ChannelPacket>
struct IList_1_t2223537486;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan1682596885.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::.ctor()
extern "C"  void Collection_1__ctor_m796025725_gshared (Collection_1_t1224341639 * __this, const MethodInfo* method);
#define Collection_1__ctor_m796025725(__this, method) ((  void (*) (Collection_1_t1224341639 *, const MethodInfo*))Collection_1__ctor_m796025725_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2350400708_gshared (Collection_1_t1224341639 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2350400708(__this, method) ((  bool (*) (Collection_1_t1224341639 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2350400708_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m546453309_gshared (Collection_1_t1224341639 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m546453309(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1224341639 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m546453309_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1297882828_gshared (Collection_1_t1224341639 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1297882828(__this, method) ((  Il2CppObject * (*) (Collection_1_t1224341639 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1297882828_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3211179237_gshared (Collection_1_t1224341639 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3211179237(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1224341639 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3211179237_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2979933341_gshared (Collection_1_t1224341639 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2979933341(__this, ___value0, method) ((  bool (*) (Collection_1_t1224341639 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2979933341_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3250175031_gshared (Collection_1_t1224341639 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3250175031(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1224341639 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3250175031_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2476188318_gshared (Collection_1_t1224341639 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2476188318(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1224341639 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2476188318_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2316873796_gshared (Collection_1_t1224341639 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2316873796(__this, ___value0, method) ((  void (*) (Collection_1_t1224341639 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2316873796_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m172950921_gshared (Collection_1_t1224341639 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m172950921(__this, method) ((  bool (*) (Collection_1_t1224341639 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m172950921_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m3262591521_gshared (Collection_1_t1224341639 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m3262591521(__this, method) ((  Il2CppObject * (*) (Collection_1_t1224341639 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m3262591521_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3106783470_gshared (Collection_1_t1224341639 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3106783470(__this, method) ((  bool (*) (Collection_1_t1224341639 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3106783470_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1194009205_gshared (Collection_1_t1224341639 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1194009205(__this, method) ((  bool (*) (Collection_1_t1224341639 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1194009205_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1068969572_gshared (Collection_1_t1224341639 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1068969572(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1224341639 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1068969572_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3605426647_gshared (Collection_1_t1224341639 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3605426647(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1224341639 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3605426647_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::Add(T)
extern "C"  void Collection_1_Add_m308009074_gshared (Collection_1_t1224341639 * __this, ChannelPacket_t1682596885  ___item0, const MethodInfo* method);
#define Collection_1_Add_m308009074(__this, ___item0, method) ((  void (*) (Collection_1_t1224341639 *, ChannelPacket_t1682596885 , const MethodInfo*))Collection_1_Add_m308009074_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::Clear()
extern "C"  void Collection_1_Clear_m718846438_gshared (Collection_1_t1224341639 * __this, const MethodInfo* method);
#define Collection_1_Clear_m718846438(__this, method) ((  void (*) (Collection_1_t1224341639 *, const MethodInfo*))Collection_1_Clear_m718846438_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3087580308_gshared (Collection_1_t1224341639 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3087580308(__this, method) ((  void (*) (Collection_1_t1224341639 *, const MethodInfo*))Collection_1_ClearItems_m3087580308_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::Contains(T)
extern "C"  bool Collection_1_Contains_m21051308_gshared (Collection_1_t1224341639 * __this, ChannelPacket_t1682596885  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m21051308(__this, ___item0, method) ((  bool (*) (Collection_1_t1224341639 *, ChannelPacket_t1682596885 , const MethodInfo*))Collection_1_Contains_m21051308_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3275415522_gshared (Collection_1_t1224341639 * __this, ChannelPacketU5BU5D_t3883591672* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3275415522(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1224341639 *, ChannelPacketU5BU5D_t3883591672*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3275415522_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1084009045_gshared (Collection_1_t1224341639 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1084009045(__this, method) ((  Il2CppObject* (*) (Collection_1_t1224341639 *, const MethodInfo*))Collection_1_GetEnumerator_m1084009045_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m597128348_gshared (Collection_1_t1224341639 * __this, ChannelPacket_t1682596885  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m597128348(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1224341639 *, ChannelPacket_t1682596885 , const MethodInfo*))Collection_1_IndexOf_m597128348_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2507663887_gshared (Collection_1_t1224341639 * __this, int32_t ___index0, ChannelPacket_t1682596885  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2507663887(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1224341639 *, int32_t, ChannelPacket_t1682596885 , const MethodInfo*))Collection_1_Insert_m2507663887_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3139847616_gshared (Collection_1_t1224341639 * __this, int32_t ___index0, ChannelPacket_t1682596885  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3139847616(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1224341639 *, int32_t, ChannelPacket_t1682596885 , const MethodInfo*))Collection_1_InsertItem_m3139847616_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::Remove(T)
extern "C"  bool Collection_1_Remove_m2408115291_gshared (Collection_1_t1224341639 * __this, ChannelPacket_t1682596885  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2408115291(__this, ___item0, method) ((  bool (*) (Collection_1_t1224341639 *, ChannelPacket_t1682596885 , const MethodInfo*))Collection_1_Remove_m2408115291_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3503781587_gshared (Collection_1_t1224341639 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3503781587(__this, ___index0, method) ((  void (*) (Collection_1_t1224341639 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3503781587_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m43194655_gshared (Collection_1_t1224341639 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m43194655(__this, ___index0, method) ((  void (*) (Collection_1_t1224341639 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m43194655_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m419065573_gshared (Collection_1_t1224341639 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m419065573(__this, method) ((  int32_t (*) (Collection_1_t1224341639 *, const MethodInfo*))Collection_1_get_Count_m419065573_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::get_Item(System.Int32)
extern "C"  ChannelPacket_t1682596885  Collection_1_get_Item_m168473463_gshared (Collection_1_t1224341639 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m168473463(__this, ___index0, method) ((  ChannelPacket_t1682596885  (*) (Collection_1_t1224341639 *, int32_t, const MethodInfo*))Collection_1_get_Item_m168473463_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2699700264_gshared (Collection_1_t1224341639 * __this, int32_t ___index0, ChannelPacket_t1682596885  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2699700264(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1224341639 *, int32_t, ChannelPacket_t1682596885 , const MethodInfo*))Collection_1_set_Item_m2699700264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1342898631_gshared (Collection_1_t1224341639 * __this, int32_t ___index0, ChannelPacket_t1682596885  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1342898631(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1224341639 *, int32_t, ChannelPacket_t1682596885 , const MethodInfo*))Collection_1_SetItem_m1342898631_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1773838630_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1773838630(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1773838630_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::ConvertItem(System.Object)
extern "C"  ChannelPacket_t1682596885  Collection_1_ConvertItem_m3198441230_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3198441230(__this /* static, unused */, ___item0, method) ((  ChannelPacket_t1682596885  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3198441230_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m3801182742_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m3801182742(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m3801182742_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3234115332_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3234115332(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3234115332_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.ChannelPacket>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m725867303_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m725867303(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m725867303_gshared)(__this /* static, unused */, ___list0, method)
