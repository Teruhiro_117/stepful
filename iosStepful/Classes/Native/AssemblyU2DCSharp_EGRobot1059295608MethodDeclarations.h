﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EGRobot
struct EGRobot_t1059295608;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void EGRobot::.ctor()
extern "C"  void EGRobot__ctor_m4001745305 (EGRobot_t1059295608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EGRobot::Init(System.String)
extern "C"  void EGRobot_Init_m1503361377 (EGRobot_t1059295608 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EGRobot::Move(System.Int32,System.Int32)
extern "C"  void EGRobot_Move_m3918904246 (EGRobot_t1059295608 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EGRobot::Draw(System.Int32,System.Int32)
extern "C"  void EGRobot_Draw_m4086571065 (EGRobot_t1059295608 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EGRobot::Erase(System.Int32,System.Int32)
extern "C"  void EGRobot_Erase_m564378421 (EGRobot_t1059295608 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
