﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct ReadOnlyCollection_1_t582280079;
// System.Collections.Generic.IList`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct IList_1_t937434988;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.NetworkSystem.PeerInfoPlayer[]
struct PeerInfoPlayerU5BU5D_t1632998306;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct IEnumerator_1_t2166985510;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo396494387.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2867263257_gshared (ReadOnlyCollection_1_t582280079 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2867263257(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2867263257_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1355612203_gshared (ReadOnlyCollection_1_t582280079 * __this, PeerInfoPlayer_t396494387  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1355612203(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, PeerInfoPlayer_t396494387 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1355612203_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2025985503_gshared (ReadOnlyCollection_1_t582280079 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2025985503(__this, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2025985503_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1877423204_gshared (ReadOnlyCollection_1_t582280079 * __this, int32_t ___index0, PeerInfoPlayer_t396494387  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1877423204(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, int32_t, PeerInfoPlayer_t396494387 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1877423204_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m75047466_gshared (ReadOnlyCollection_1_t582280079 * __this, PeerInfoPlayer_t396494387  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m75047466(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t582280079 *, PeerInfoPlayer_t396494387 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m75047466_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m710954928_gshared (ReadOnlyCollection_1_t582280079 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m710954928(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m710954928_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  PeerInfoPlayer_t396494387  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2041171518_gshared (ReadOnlyCollection_1_t582280079 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2041171518(__this, ___index0, method) ((  PeerInfoPlayer_t396494387  (*) (ReadOnlyCollection_1_t582280079 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2041171518_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m710466599_gshared (ReadOnlyCollection_1_t582280079 * __this, int32_t ___index0, PeerInfoPlayer_t396494387  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m710466599(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, int32_t, PeerInfoPlayer_t396494387 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m710466599_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3620584547_gshared (ReadOnlyCollection_1_t582280079 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3620584547(__this, method) ((  bool (*) (ReadOnlyCollection_1_t582280079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3620584547_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2912950006_gshared (ReadOnlyCollection_1_t582280079 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2912950006(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2912950006_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4181544167_gshared (ReadOnlyCollection_1_t582280079 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4181544167(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t582280079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4181544167_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2349492010_gshared (ReadOnlyCollection_1_t582280079 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2349492010(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t582280079 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2349492010_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2861192064_gshared (ReadOnlyCollection_1_t582280079 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2861192064(__this, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2861192064_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1156552884_gshared (ReadOnlyCollection_1_t582280079 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1156552884(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t582280079 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1156552884_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m743252496_gshared (ReadOnlyCollection_1_t582280079 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m743252496(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t582280079 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m743252496_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2020232319_gshared (ReadOnlyCollection_1_t582280079 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2020232319(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2020232319_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2198759367_gshared (ReadOnlyCollection_1_t582280079 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2198759367(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2198759367_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m320883209_gshared (ReadOnlyCollection_1_t582280079 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m320883209(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m320883209_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m268455458_gshared (ReadOnlyCollection_1_t582280079 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m268455458(__this, method) ((  bool (*) (ReadOnlyCollection_1_t582280079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m268455458_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1087432822_gshared (ReadOnlyCollection_1_t582280079 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1087432822(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t582280079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1087432822_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m902027959_gshared (ReadOnlyCollection_1_t582280079 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m902027959(__this, method) ((  bool (*) (ReadOnlyCollection_1_t582280079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m902027959_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1591418286_gshared (ReadOnlyCollection_1_t582280079 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1591418286(__this, method) ((  bool (*) (ReadOnlyCollection_1_t582280079 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1591418286_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1809633743_gshared (ReadOnlyCollection_1_t582280079 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1809633743(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t582280079 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1809633743_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1535211964_gshared (ReadOnlyCollection_1_t582280079 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1535211964(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1535211964_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1761979125_gshared (ReadOnlyCollection_1_t582280079 * __this, PeerInfoPlayer_t396494387  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1761979125(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t582280079 *, PeerInfoPlayer_t396494387 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1761979125_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2250567287_gshared (ReadOnlyCollection_1_t582280079 * __this, PeerInfoPlayerU5BU5D_t1632998306* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2250567287(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t582280079 *, PeerInfoPlayerU5BU5D_t1632998306*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2250567287_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3582414830_gshared (ReadOnlyCollection_1_t582280079 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3582414830(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t582280079 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3582414830_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3513760739_gshared (ReadOnlyCollection_1_t582280079 * __this, PeerInfoPlayer_t396494387  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3513760739(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t582280079 *, PeerInfoPlayer_t396494387 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3513760739_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m4252196906_gshared (ReadOnlyCollection_1_t582280079 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m4252196906(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t582280079 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m4252196906_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::get_Item(System.Int32)
extern "C"  PeerInfoPlayer_t396494387  ReadOnlyCollection_1_get_Item_m3859185546_gshared (ReadOnlyCollection_1_t582280079 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3859185546(__this, ___index0, method) ((  PeerInfoPlayer_t396494387  (*) (ReadOnlyCollection_1_t582280079 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3859185546_gshared)(__this, ___index0, method)
