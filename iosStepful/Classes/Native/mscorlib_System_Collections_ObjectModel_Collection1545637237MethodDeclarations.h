﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>
struct Collection_1_t1545637237;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.QosType[]
struct QosTypeU5BU5D_t740748178;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.QosType>
struct IEnumerator_1_t3774383606;
// System.Collections.Generic.IList`1<UnityEngine.Networking.QosType>
struct IList_1_t2544833084;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Networking_QosType2003892483.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::.ctor()
extern "C"  void Collection_1__ctor_m2398973779_gshared (Collection_1_t1545637237 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2398973779(__this, method) ((  void (*) (Collection_1_t1545637237 *, const MethodInfo*))Collection_1__ctor_m2398973779_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2682281106_gshared (Collection_1_t1545637237 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2682281106(__this, method) ((  bool (*) (Collection_1_t1545637237 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2682281106_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m4218274271_gshared (Collection_1_t1545637237 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m4218274271(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1545637237 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m4218274271_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2093373110_gshared (Collection_1_t1545637237 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2093373110(__this, method) ((  Il2CppObject * (*) (Collection_1_t1545637237 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2093373110_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m108279135_gshared (Collection_1_t1545637237 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m108279135(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1545637237 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m108279135_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1002847223_gshared (Collection_1_t1545637237 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1002847223(__this, ___value0, method) ((  bool (*) (Collection_1_t1545637237 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1002847223_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3053869069_gshared (Collection_1_t1545637237 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3053869069(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1545637237 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3053869069_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3933437388_gshared (Collection_1_t1545637237 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3933437388(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1545637237 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3933437388_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m478718786_gshared (Collection_1_t1545637237 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m478718786(__this, ___value0, method) ((  void (*) (Collection_1_t1545637237 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m478718786_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3242611895_gshared (Collection_1_t1545637237 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3242611895(__this, method) ((  bool (*) (Collection_1_t1545637237 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3242611895_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2571880947_gshared (Collection_1_t1545637237 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2571880947(__this, method) ((  Il2CppObject * (*) (Collection_1_t1545637237 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2571880947_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m892180232_gshared (Collection_1_t1545637237 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m892180232(__this, method) ((  bool (*) (Collection_1_t1545637237 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m892180232_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1149006731_gshared (Collection_1_t1545637237 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1149006731(__this, method) ((  bool (*) (Collection_1_t1545637237 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1149006731_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2090964454_gshared (Collection_1_t1545637237 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2090964454(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1545637237 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2090964454_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m836665393_gshared (Collection_1_t1545637237 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m836665393(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1545637237 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m836665393_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::Add(T)
extern "C"  void Collection_1_Add_m2713896712_gshared (Collection_1_t1545637237 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m2713896712(__this, ___item0, method) ((  void (*) (Collection_1_t1545637237 *, int32_t, const MethodInfo*))Collection_1_Add_m2713896712_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::Clear()
extern "C"  void Collection_1_Clear_m1430707196_gshared (Collection_1_t1545637237 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1430707196(__this, method) ((  void (*) (Collection_1_t1545637237 *, const MethodInfo*))Collection_1_Clear_m1430707196_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2807698302_gshared (Collection_1_t1545637237 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2807698302(__this, method) ((  void (*) (Collection_1_t1545637237 *, const MethodInfo*))Collection_1_ClearItems_m2807698302_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::Contains(T)
extern "C"  bool Collection_1_Contains_m3964795586_gshared (Collection_1_t1545637237 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3964795586(__this, ___item0, method) ((  bool (*) (Collection_1_t1545637237 *, int32_t, const MethodInfo*))Collection_1_Contains_m3964795586_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2432837268_gshared (Collection_1_t1545637237 * __this, QosTypeU5BU5D_t740748178* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2432837268(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1545637237 *, QosTypeU5BU5D_t740748178*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2432837268_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2995042911_gshared (Collection_1_t1545637237 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2995042911(__this, method) ((  Il2CppObject* (*) (Collection_1_t1545637237 *, const MethodInfo*))Collection_1_GetEnumerator_m2995042911_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3122282434_gshared (Collection_1_t1545637237 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3122282434(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1545637237 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m3122282434_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1983351957_gshared (Collection_1_t1545637237 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1983351957(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1545637237 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m1983351957_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2741206262_gshared (Collection_1_t1545637237 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2741206262(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1545637237 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m2741206262_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::Remove(T)
extern "C"  bool Collection_1_Remove_m1373853285_gshared (Collection_1_t1545637237 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1373853285(__this, ___item0, method) ((  bool (*) (Collection_1_t1545637237 *, int32_t, const MethodInfo*))Collection_1_Remove_m1373853285_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3066861865_gshared (Collection_1_t1545637237 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3066861865(__this, ___index0, method) ((  void (*) (Collection_1_t1545637237 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3066861865_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1026372989_gshared (Collection_1_t1545637237 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1026372989(__this, ___index0, method) ((  void (*) (Collection_1_t1545637237 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1026372989_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m424383983_gshared (Collection_1_t1545637237 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m424383983(__this, method) ((  int32_t (*) (Collection_1_t1545637237 *, const MethodInfo*))Collection_1_get_Count_m424383983_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m2420634485_gshared (Collection_1_t1545637237 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2420634485(__this, ___index0, method) ((  int32_t (*) (Collection_1_t1545637237 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2420634485_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1876318454_gshared (Collection_1_t1545637237 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1876318454(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1545637237 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m1876318454_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2933812529_gshared (Collection_1_t1545637237 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2933812529(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1545637237 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m2933812529_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1324411652_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1324411652(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1324411652_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m3812831140_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3812831140(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3812831140_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1488338896_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1488338896(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1488338896_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3025408990_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3025408990(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3025408990_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.QosType>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3931267501_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3931267501(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3931267501_gshared)(__this /* static, unused */, ___list0, method)
