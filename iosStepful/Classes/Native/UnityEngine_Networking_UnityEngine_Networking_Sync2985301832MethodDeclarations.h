﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync3645530894MethodDeclarations.h"

// System.Void UnityEngine.Networking.SyncList`1<System.String>::.ctor()
#define SyncList_1__ctor_m3281822541(__this, method) ((  void (*) (SyncList_1_t2985301832 *, const MethodInfo*))SyncList_1__ctor_m1979817233_gshared)(__this, method)
// System.Int32 UnityEngine.Networking.SyncList`1<System.String>::get_Count()
#define SyncList_1_get_Count_m972443341(__this, method) ((  int32_t (*) (SyncList_1_t2985301832 *, const MethodInfo*))SyncList_1_get_Count_m3995922617_gshared)(__this, method)
// System.Boolean UnityEngine.Networking.SyncList`1<System.String>::get_IsReadOnly()
#define SyncList_1_get_IsReadOnly_m2994155698(__this, method) ((  bool (*) (SyncList_1_t2985301832 *, const MethodInfo*))SyncList_1_get_IsReadOnly_m1739608972_gshared)(__this, method)
// UnityEngine.Networking.SyncList`1/SyncListChanged<T> UnityEngine.Networking.SyncList`1<System.String>::get_Callback()
#define SyncList_1_get_Callback_m2657060295(__this, method) ((  SyncListChanged_t395490683 * (*) (SyncList_1_t2985301832 *, const MethodInfo*))SyncList_1_get_Callback_m3756815995_gshared)(__this, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::set_Callback(UnityEngine.Networking.SyncList`1/SyncListChanged<T>)
#define SyncList_1_set_Callback_m3054300974(__this, ___value0, method) ((  void (*) (SyncList_1_t2985301832 *, SyncListChanged_t395490683 *, const MethodInfo*))SyncList_1_set_Callback_m3071308488_gshared)(__this, ___value0, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::InitializeBehaviour(UnityEngine.Networking.NetworkBehaviour,System.Int32)
#define SyncList_1_InitializeBehaviour_m3239386853(__this, ___beh0, ___cmdHash1, method) ((  void (*) (SyncList_1_t2985301832 *, NetworkBehaviour_t3873055601 *, int32_t, const MethodInfo*))SyncList_1_InitializeBehaviour_m3472596089_gshared)(__this, ___beh0, ___cmdHash1, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,T)
#define SyncList_1_SendMsg_m2826913218(__this, ___op0, ___itemIndex1, ___item2, method) ((  void (*) (SyncList_1_t2985301832 *, int32_t, int32_t, String_t*, const MethodInfo*))SyncList_1_SendMsg_m1310025432_gshared)(__this, ___op0, ___itemIndex1, ___item2, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
#define SyncList_1_SendMsg_m860863650(__this, ___op0, ___itemIndex1, method) ((  void (*) (SyncList_1_t2985301832 *, int32_t, int32_t, const MethodInfo*))SyncList_1_SendMsg_m3027456264_gshared)(__this, ___op0, ___itemIndex1, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::HandleMsg(UnityEngine.Networking.NetworkReader)
#define SyncList_1_HandleMsg_m3889884720(__this, ___reader0, method) ((  void (*) (SyncList_1_t2985301832 *, NetworkReader_t3187690923 *, const MethodInfo*))SyncList_1_HandleMsg_m1047790890_gshared)(__this, ___reader0, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::AddInternal(T)
#define SyncList_1_AddInternal_m702154171(__this, ___item0, method) ((  void (*) (SyncList_1_t2985301832 *, String_t*, const MethodInfo*))SyncList_1_AddInternal_m1156564503_gshared)(__this, ___item0, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::Add(T)
#define SyncList_1_Add_m3647095324(__this, ___item0, method) ((  void (*) (SyncList_1_t2985301832 *, String_t*, const MethodInfo*))SyncList_1_Add_m903203842_gshared)(__this, ___item0, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::Clear()
#define SyncList_1_Clear_m2698840192(__this, method) ((  void (*) (SyncList_1_t2985301832 *, const MethodInfo*))SyncList_1_Clear_m479214166_gshared)(__this, method)
// System.Boolean UnityEngine.Networking.SyncList`1<System.String>::Contains(T)
#define SyncList_1_Contains_m460048290(__this, ___item0, method) ((  bool (*) (SyncList_1_t2985301832 *, String_t*, const MethodInfo*))SyncList_1_Contains_m2184498664_gshared)(__this, ___item0, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::CopyTo(T[],System.Int32)
#define SyncList_1_CopyTo_m1488882180(__this, ___array0, ___index1, method) ((  void (*) (SyncList_1_t2985301832 *, StringU5BU5D_t1642385972*, int32_t, const MethodInfo*))SyncList_1_CopyTo_m1104313626_gshared)(__this, ___array0, ___index1, method)
// System.Int32 UnityEngine.Networking.SyncList`1<System.String>::IndexOf(T)
#define SyncList_1_IndexOf_m2115278554(__this, ___item0, method) ((  int32_t (*) (SyncList_1_t2985301832 *, String_t*, const MethodInfo*))SyncList_1_IndexOf_m2624123316_gshared)(__this, ___item0, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::Insert(System.Int32,T)
#define SyncList_1_Insert_m1267217555(__this, ___index0, ___item1, method) ((  void (*) (SyncList_1_t2985301832 *, int32_t, String_t*, const MethodInfo*))SyncList_1_Insert_m4109060535_gshared)(__this, ___index0, ___item1, method)
// System.Boolean UnityEngine.Networking.SyncList`1<System.String>::Remove(T)
#define SyncList_1_Remove_m2198673015(__this, ___item0, method) ((  bool (*) (SyncList_1_t2985301832 *, String_t*, const MethodInfo*))SyncList_1_Remove_m338076795_gshared)(__this, ___item0, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::RemoveAt(System.Int32)
#define SyncList_1_RemoveAt_m902648711(__this, ___index0, method) ((  void (*) (SyncList_1_t2985301832 *, int32_t, const MethodInfo*))SyncList_1_RemoveAt_m20766587_gshared)(__this, ___index0, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::Dirty(System.Int32)
#define SyncList_1_Dirty_m3365965812(__this, ___index0, method) ((  void (*) (SyncList_1_t2985301832 *, int32_t, const MethodInfo*))SyncList_1_Dirty_m1369571806_gshared)(__this, ___index0, method)
// T UnityEngine.Networking.SyncList`1<System.String>::get_Item(System.Int32)
#define SyncList_1_get_Item_m3202760399(__this, ___i0, method) ((  String_t* (*) (SyncList_1_t2985301832 *, int32_t, const MethodInfo*))SyncList_1_get_Item_m172423547_gshared)(__this, ___i0, method)
// System.Void UnityEngine.Networking.SyncList`1<System.String>::set_Item(System.Int32,T)
#define SyncList_1_set_Item_m3815306278(__this, ___i0, ___value1, method) ((  void (*) (SyncList_1_t2985301832 *, int32_t, String_t*, const MethodInfo*))SyncList_1_set_Item_m4107841040_gshared)(__this, ___i0, ___value1, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.Networking.SyncList`1<System.String>::GetEnumerator()
#define SyncList_1_GetEnumerator_m2651900097(__this, method) ((  Il2CppObject* (*) (SyncList_1_t2985301832 *, const MethodInfo*))SyncList_1_GetEnumerator_m1059025845_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.Networking.SyncList`1<System.String>::System.Collections.IEnumerable.GetEnumerator()
#define SyncList_1_System_Collections_IEnumerable_GetEnumerator_m715829674(__this, method) ((  Il2CppObject * (*) (SyncList_1_t2985301832 *, const MethodInfo*))SyncList_1_System_Collections_IEnumerable_GetEnumerator_m2736782484_gshared)(__this, method)
