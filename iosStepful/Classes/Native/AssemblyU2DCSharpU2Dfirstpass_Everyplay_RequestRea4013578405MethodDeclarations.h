﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/RequestReadyDelegate
struct RequestReadyDelegate_t4013578405;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/RequestReadyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestReadyDelegate__ctor_m3227343646 (RequestReadyDelegate_t4013578405 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/RequestReadyDelegate::Invoke(System.String)
extern "C"  void RequestReadyDelegate_Invoke_m2023212226 (RequestReadyDelegate_t4013578405 * __this, String_t* ___response0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/RequestReadyDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RequestReadyDelegate_BeginInvoke_m4250386803 (RequestReadyDelegate_t4013578405 * __this, String_t* ___response0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/RequestReadyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void RequestReadyDelegate_EndInvoke_m1528410900 (RequestReadyDelegate_t4013578405 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
