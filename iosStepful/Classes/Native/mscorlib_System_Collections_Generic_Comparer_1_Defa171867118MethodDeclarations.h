﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.ChannelPacket>
struct DefaultComparer_t171867118;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan1682596885.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.ChannelPacket>::.ctor()
extern "C"  void DefaultComparer__ctor_m1577113652_gshared (DefaultComparer_t171867118 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1577113652(__this, method) ((  void (*) (DefaultComparer_t171867118 *, const MethodInfo*))DefaultComparer__ctor_m1577113652_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.ChannelPacket>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2899366125_gshared (DefaultComparer_t171867118 * __this, ChannelPacket_t1682596885  ___x0, ChannelPacket_t1682596885  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2899366125(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t171867118 *, ChannelPacket_t1682596885 , ChannelPacket_t1682596885 , const MethodInfo*))DefaultComparer_Compare_m2899366125_gshared)(__this, ___x0, ___y1, method)
