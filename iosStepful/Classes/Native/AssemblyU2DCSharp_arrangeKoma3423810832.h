﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[0...,0...]
struct ByteU5BU2CU5D_t3397334014;
// System.Boolean[0...,0...]
struct BooleanU5BU2CU5D_t3568034316;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Int32[0...,0...]
struct Int32U5BU2CU5D_t3030399642;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// arrangeKoma
struct  arrangeKoma_t3423810832  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean[0...,0...] arrangeKoma::canTouchPlace
	BooleanU5BU2CU5D_t3568034316* ___canTouchPlace_3;
	// System.Boolean arrangeKoma::human
	bool ___human_4;
	// System.Boolean arrangeKoma::resources
	bool ___resources_5;
	// UnityEngine.Sprite arrangeKoma::koma
	Sprite_t309593783 * ___koma_6;
	// System.Int32 arrangeKoma::komaNum
	int32_t ___komaNum_7;
	// UnityEngine.UI.Image arrangeKoma::img
	Image_t2042527209 * ___img_8;
	// UnityEngine.UI.Text arrangeKoma::text
	Text_t356221433 * ___text_9;
	// System.Int32 arrangeKoma::line
	int32_t ___line_10;
	// System.Int32 arrangeKoma::row
	int32_t ___row_11;
	// System.Single arrangeKoma::changeRed
	float ___changeRed_12;
	// System.Single arrangeKoma::changeGreen
	float ___changeGreen_13;
	// System.Single arrangeKoma::changeBlue
	float ___changeBlue_14;
	// System.Single arrangeKoma::changeAlpha
	float ___changeAlpha_15;
	// System.Int32[0...,0...] arrangeKoma::humanPosition
	Int32U5BU2CU5D_t3030399642* ___humanPosition_18;

public:
	inline static int32_t get_offset_of_canTouchPlace_3() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___canTouchPlace_3)); }
	inline BooleanU5BU2CU5D_t3568034316* get_canTouchPlace_3() const { return ___canTouchPlace_3; }
	inline BooleanU5BU2CU5D_t3568034316** get_address_of_canTouchPlace_3() { return &___canTouchPlace_3; }
	inline void set_canTouchPlace_3(BooleanU5BU2CU5D_t3568034316* value)
	{
		___canTouchPlace_3 = value;
		Il2CppCodeGenWriteBarrier(&___canTouchPlace_3, value);
	}

	inline static int32_t get_offset_of_human_4() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___human_4)); }
	inline bool get_human_4() const { return ___human_4; }
	inline bool* get_address_of_human_4() { return &___human_4; }
	inline void set_human_4(bool value)
	{
		___human_4 = value;
	}

	inline static int32_t get_offset_of_resources_5() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___resources_5)); }
	inline bool get_resources_5() const { return ___resources_5; }
	inline bool* get_address_of_resources_5() { return &___resources_5; }
	inline void set_resources_5(bool value)
	{
		___resources_5 = value;
	}

	inline static int32_t get_offset_of_koma_6() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___koma_6)); }
	inline Sprite_t309593783 * get_koma_6() const { return ___koma_6; }
	inline Sprite_t309593783 ** get_address_of_koma_6() { return &___koma_6; }
	inline void set_koma_6(Sprite_t309593783 * value)
	{
		___koma_6 = value;
		Il2CppCodeGenWriteBarrier(&___koma_6, value);
	}

	inline static int32_t get_offset_of_komaNum_7() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___komaNum_7)); }
	inline int32_t get_komaNum_7() const { return ___komaNum_7; }
	inline int32_t* get_address_of_komaNum_7() { return &___komaNum_7; }
	inline void set_komaNum_7(int32_t value)
	{
		___komaNum_7 = value;
	}

	inline static int32_t get_offset_of_img_8() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___img_8)); }
	inline Image_t2042527209 * get_img_8() const { return ___img_8; }
	inline Image_t2042527209 ** get_address_of_img_8() { return &___img_8; }
	inline void set_img_8(Image_t2042527209 * value)
	{
		___img_8 = value;
		Il2CppCodeGenWriteBarrier(&___img_8, value);
	}

	inline static int32_t get_offset_of_text_9() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___text_9)); }
	inline Text_t356221433 * get_text_9() const { return ___text_9; }
	inline Text_t356221433 ** get_address_of_text_9() { return &___text_9; }
	inline void set_text_9(Text_t356221433 * value)
	{
		___text_9 = value;
		Il2CppCodeGenWriteBarrier(&___text_9, value);
	}

	inline static int32_t get_offset_of_line_10() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___line_10)); }
	inline int32_t get_line_10() const { return ___line_10; }
	inline int32_t* get_address_of_line_10() { return &___line_10; }
	inline void set_line_10(int32_t value)
	{
		___line_10 = value;
	}

	inline static int32_t get_offset_of_row_11() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___row_11)); }
	inline int32_t get_row_11() const { return ___row_11; }
	inline int32_t* get_address_of_row_11() { return &___row_11; }
	inline void set_row_11(int32_t value)
	{
		___row_11 = value;
	}

	inline static int32_t get_offset_of_changeRed_12() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___changeRed_12)); }
	inline float get_changeRed_12() const { return ___changeRed_12; }
	inline float* get_address_of_changeRed_12() { return &___changeRed_12; }
	inline void set_changeRed_12(float value)
	{
		___changeRed_12 = value;
	}

	inline static int32_t get_offset_of_changeGreen_13() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___changeGreen_13)); }
	inline float get_changeGreen_13() const { return ___changeGreen_13; }
	inline float* get_address_of_changeGreen_13() { return &___changeGreen_13; }
	inline void set_changeGreen_13(float value)
	{
		___changeGreen_13 = value;
	}

	inline static int32_t get_offset_of_changeBlue_14() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___changeBlue_14)); }
	inline float get_changeBlue_14() const { return ___changeBlue_14; }
	inline float* get_address_of_changeBlue_14() { return &___changeBlue_14; }
	inline void set_changeBlue_14(float value)
	{
		___changeBlue_14 = value;
	}

	inline static int32_t get_offset_of_changeAlpha_15() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___changeAlpha_15)); }
	inline float get_changeAlpha_15() const { return ___changeAlpha_15; }
	inline float* get_address_of_changeAlpha_15() { return &___changeAlpha_15; }
	inline void set_changeAlpha_15(float value)
	{
		___changeAlpha_15 = value;
	}

	inline static int32_t get_offset_of_humanPosition_18() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832, ___humanPosition_18)); }
	inline Int32U5BU2CU5D_t3030399642* get_humanPosition_18() const { return ___humanPosition_18; }
	inline Int32U5BU2CU5D_t3030399642** get_address_of_humanPosition_18() { return &___humanPosition_18; }
	inline void set_humanPosition_18(Int32U5BU2CU5D_t3030399642* value)
	{
		___humanPosition_18 = value;
		Il2CppCodeGenWriteBarrier(&___humanPosition_18, value);
	}
};

struct arrangeKoma_t3423810832_StaticFields
{
public:
	// System.Byte[0...,0...] arrangeKoma::startBoard
	ByteU5BU2CU5D_t3397334014* ___startBoard_2;
	// System.Int32[0...,0...] arrangeKoma::barrier
	Int32U5BU2CU5D_t3030399642* ___barrier_16;
	// System.Int32[0...,0...] arrangeKoma::factoryLocation
	Int32U5BU2CU5D_t3030399642* ___factoryLocation_17;
	// System.Int32 arrangeKoma::myResources
	int32_t ___myResources_19;

public:
	inline static int32_t get_offset_of_startBoard_2() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832_StaticFields, ___startBoard_2)); }
	inline ByteU5BU2CU5D_t3397334014* get_startBoard_2() const { return ___startBoard_2; }
	inline ByteU5BU2CU5D_t3397334014** get_address_of_startBoard_2() { return &___startBoard_2; }
	inline void set_startBoard_2(ByteU5BU2CU5D_t3397334014* value)
	{
		___startBoard_2 = value;
		Il2CppCodeGenWriteBarrier(&___startBoard_2, value);
	}

	inline static int32_t get_offset_of_barrier_16() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832_StaticFields, ___barrier_16)); }
	inline Int32U5BU2CU5D_t3030399642* get_barrier_16() const { return ___barrier_16; }
	inline Int32U5BU2CU5D_t3030399642** get_address_of_barrier_16() { return &___barrier_16; }
	inline void set_barrier_16(Int32U5BU2CU5D_t3030399642* value)
	{
		___barrier_16 = value;
		Il2CppCodeGenWriteBarrier(&___barrier_16, value);
	}

	inline static int32_t get_offset_of_factoryLocation_17() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832_StaticFields, ___factoryLocation_17)); }
	inline Int32U5BU2CU5D_t3030399642* get_factoryLocation_17() const { return ___factoryLocation_17; }
	inline Int32U5BU2CU5D_t3030399642** get_address_of_factoryLocation_17() { return &___factoryLocation_17; }
	inline void set_factoryLocation_17(Int32U5BU2CU5D_t3030399642* value)
	{
		___factoryLocation_17 = value;
		Il2CppCodeGenWriteBarrier(&___factoryLocation_17, value);
	}

	inline static int32_t get_offset_of_myResources_19() { return static_cast<int32_t>(offsetof(arrangeKoma_t3423810832_StaticFields, ___myResources_19)); }
	inline int32_t get_myResources_19() const { return ___myResources_19; }
	inline int32_t* get_address_of_myResources_19() { return &___myResources_19; }
	inline void set_myResources_19(int32_t value)
	{
		___myResources_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
