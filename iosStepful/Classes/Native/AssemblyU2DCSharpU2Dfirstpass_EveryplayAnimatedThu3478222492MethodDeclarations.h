﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayAnimatedThumbnail
struct EveryplayAnimatedThumbnail_t3478222492;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void EveryplayAnimatedThumbnail::.ctor()
extern "C"  void EveryplayAnimatedThumbnail__ctor_m1963933355 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnail::Awake()
extern "C"  void EveryplayAnimatedThumbnail_Awake_m78205726 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnail::Start()
extern "C"  void EveryplayAnimatedThumbnail_Start_m2007437215 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnail::OnDestroy()
extern "C"  void EveryplayAnimatedThumbnail_OnDestroy_m1984512408 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnail::OnDisable()
extern "C"  void EveryplayAnimatedThumbnail_OnDisable_m2113261882 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnail::ResetThumbnail()
extern "C"  void EveryplayAnimatedThumbnail_ResetThumbnail_m3893133820 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EveryplayAnimatedThumbnail::CrossfadeTransition()
extern "C"  Il2CppObject * EveryplayAnimatedThumbnail_CrossfadeTransition_m3430637328 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnail::StopTransitions()
extern "C"  void EveryplayAnimatedThumbnail_StopTransitions_m473791811 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnail::Update()
extern "C"  void EveryplayAnimatedThumbnail_Update_m3294450140 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
