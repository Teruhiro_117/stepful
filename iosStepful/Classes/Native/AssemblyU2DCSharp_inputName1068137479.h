﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// inputName
struct  inputName_t1068137479  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject inputName::obj
	GameObject_t1756533147 * ___obj_2;
	// UnityEngine.GameObject inputName::canvasObject
	GameObject_t1756533147 * ___canvasObject_3;
	// UnityEngine.GameObject inputName::prefabResource
	GameObject_t1756533147 * ___prefabResource_4;
	// UnityEngine.UI.Text inputName::text
	Text_t356221433 * ___text_5;
	// System.String inputName::str
	String_t* ___str_6;
	// UnityEngine.UI.InputField inputName::inputField
	InputField_t1631627530 * ___inputField_7;

public:
	inline static int32_t get_offset_of_obj_2() { return static_cast<int32_t>(offsetof(inputName_t1068137479, ___obj_2)); }
	inline GameObject_t1756533147 * get_obj_2() const { return ___obj_2; }
	inline GameObject_t1756533147 ** get_address_of_obj_2() { return &___obj_2; }
	inline void set_obj_2(GameObject_t1756533147 * value)
	{
		___obj_2 = value;
		Il2CppCodeGenWriteBarrier(&___obj_2, value);
	}

	inline static int32_t get_offset_of_canvasObject_3() { return static_cast<int32_t>(offsetof(inputName_t1068137479, ___canvasObject_3)); }
	inline GameObject_t1756533147 * get_canvasObject_3() const { return ___canvasObject_3; }
	inline GameObject_t1756533147 ** get_address_of_canvasObject_3() { return &___canvasObject_3; }
	inline void set_canvasObject_3(GameObject_t1756533147 * value)
	{
		___canvasObject_3 = value;
		Il2CppCodeGenWriteBarrier(&___canvasObject_3, value);
	}

	inline static int32_t get_offset_of_prefabResource_4() { return static_cast<int32_t>(offsetof(inputName_t1068137479, ___prefabResource_4)); }
	inline GameObject_t1756533147 * get_prefabResource_4() const { return ___prefabResource_4; }
	inline GameObject_t1756533147 ** get_address_of_prefabResource_4() { return &___prefabResource_4; }
	inline void set_prefabResource_4(GameObject_t1756533147 * value)
	{
		___prefabResource_4 = value;
		Il2CppCodeGenWriteBarrier(&___prefabResource_4, value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(inputName_t1068137479, ___text_5)); }
	inline Text_t356221433 * get_text_5() const { return ___text_5; }
	inline Text_t356221433 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Text_t356221433 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier(&___text_5, value);
	}

	inline static int32_t get_offset_of_str_6() { return static_cast<int32_t>(offsetof(inputName_t1068137479, ___str_6)); }
	inline String_t* get_str_6() const { return ___str_6; }
	inline String_t** get_address_of_str_6() { return &___str_6; }
	inline void set_str_6(String_t* value)
	{
		___str_6 = value;
		Il2CppCodeGenWriteBarrier(&___str_6, value);
	}

	inline static int32_t get_offset_of_inputField_7() { return static_cast<int32_t>(offsetof(inputName_t1068137479, ___inputField_7)); }
	inline InputField_t1631627530 * get_inputField_7() const { return ___inputField_7; }
	inline InputField_t1631627530 ** get_address_of_inputField_7() { return &___inputField_7; }
	inline void set_inputField_7(InputField_t1631627530 * value)
	{
		___inputField_7 = value;
		Il2CppCodeGenWriteBarrier(&___inputField_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
