﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct Stack_1_t2065349876;
// System.Object
struct Il2CppObject;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct IEnumerator_1_t2748112845;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat2715348236.h"

// System.Void System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor()
extern "C"  void Stack_1__ctor_m3920966894_gshared (Stack_1_t2065349876 * __this, const MethodInfo* method);
#define Stack_1__ctor_m3920966894(__this, method) ((  void (*) (Stack_1_t2065349876 *, const MethodInfo*))Stack_1__ctor_m3920966894_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Stack_1_System_Collections_ICollection_get_IsSynchronized_m2920687314_gshared (Stack_1_t2065349876 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m2920687314(__this, method) ((  bool (*) (Stack_1_t2065349876 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m2920687314_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Stack_1_System_Collections_ICollection_get_SyncRoot_m3520337990_gshared (Stack_1_t2065349876 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3520337990(__this, method) ((  Il2CppObject * (*) (Stack_1_t2065349876 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3520337990_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Stack_1_System_Collections_ICollection_CopyTo_m415183926_gshared (Stack_1_t2065349876 * __this, Il2CppArray * ___dest0, int32_t ___idx1, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_CopyTo_m415183926(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t2065349876 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m415183926_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m310896012_gshared (Stack_1_t2065349876 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m310896012(__this, method) ((  Il2CppObject* (*) (Stack_1_t2065349876 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m310896012_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Stack_1_System_Collections_IEnumerable_GetEnumerator_m4233342271_gshared (Stack_1_t2065349876 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m4233342271(__this, method) ((  Il2CppObject * (*) (Stack_1_t2065349876 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m4233342271_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::Contains(T)
extern "C"  bool Stack_1_Contains_m3438341317_gshared (Stack_1_t2065349876 * __this, InternalMsg_t977621722  ___t0, const MethodInfo* method);
#define Stack_1_Contains_m3438341317(__this, ___t0, method) ((  bool (*) (Stack_1_t2065349876 *, InternalMsg_t977621722 , const MethodInfo*))Stack_1_Contains_m3438341317_gshared)(__this, ___t0, method)
// T System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::Peek()
extern "C"  InternalMsg_t977621722  Stack_1_Peek_m3710251926_gshared (Stack_1_t2065349876 * __this, const MethodInfo* method);
#define Stack_1_Peek_m3710251926(__this, method) ((  InternalMsg_t977621722  (*) (Stack_1_t2065349876 *, const MethodInfo*))Stack_1_Peek_m3710251926_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::Pop()
extern "C"  InternalMsg_t977621722  Stack_1_Pop_m1546183791_gshared (Stack_1_t2065349876 * __this, const MethodInfo* method);
#define Stack_1_Pop_m1546183791(__this, method) ((  InternalMsg_t977621722  (*) (Stack_1_t2065349876 *, const MethodInfo*))Stack_1_Pop_m1546183791_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::Push(T)
extern "C"  void Stack_1_Push_m4134237261_gshared (Stack_1_t2065349876 * __this, InternalMsg_t977621722  ___t0, const MethodInfo* method);
#define Stack_1_Push_m4134237261(__this, ___t0, method) ((  void (*) (Stack_1_t2065349876 *, InternalMsg_t977621722 , const MethodInfo*))Stack_1_Push_m4134237261_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m3516550662_gshared (Stack_1_t2065349876 * __this, const MethodInfo* method);
#define Stack_1_get_Count_m3516550662(__this, method) ((  int32_t (*) (Stack_1_t2065349876 *, const MethodInfo*))Stack_1_get_Count_m3516550662_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>::GetEnumerator()
extern "C"  Enumerator_t2715348236  Stack_1_GetEnumerator_m3253519884_gshared (Stack_1_t2065349876 * __this, const MethodInfo* method);
#define Stack_1_GetEnumerator_m3253519884(__this, method) ((  Enumerator_t2715348236  (*) (Stack_1_t2065349876 *, const MethodInfo*))Stack_1_GetEnumerator_m3253519884_gshared)(__this, method)
