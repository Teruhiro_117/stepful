﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUIText
struct GUIText_t2411476300;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_TextAlignment1418134952.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"

// System.Void UnityEngine.GUIText::set_text(System.String)
extern "C"  void GUIText_set_text_m3758377277 (GUIText_t2411476300 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_alignment(UnityEngine.TextAlignment)
extern "C"  void GUIText_set_alignment_m2615389432 (GUIText_t2411476300 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_anchor(UnityEngine.TextAnchor)
extern "C"  void GUIText_set_anchor_m2234105042 (GUIText_t2411476300 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
