﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct Collection_1_t519366476;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.LocalClient/InternalMsg[]
struct InternalMsgU5BU5D_t2023740543;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct IEnumerator_1_t2748112845;
// System.Collections.Generic.IList`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct IList_1_t1518562323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor()
extern "C"  void Collection_1__ctor_m2108454507_gshared (Collection_1_t519366476 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2108454507(__this, method) ((  void (*) (Collection_1_t519366476 *, const MethodInfo*))Collection_1__ctor_m2108454507_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2518487658_gshared (Collection_1_t519366476 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2518487658(__this, method) ((  bool (*) (Collection_1_t519366476 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2518487658_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3558468575_gshared (Collection_1_t519366476 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3558468575(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t519366476 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3558468575_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1067578134_gshared (Collection_1_t519366476 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1067578134(__this, method) ((  Il2CppObject * (*) (Collection_1_t519366476 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1067578134_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3645263703_gshared (Collection_1_t519366476 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3645263703(__this, ___value0, method) ((  int32_t (*) (Collection_1_t519366476 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3645263703_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m888444215_gshared (Collection_1_t519366476 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m888444215(__this, ___value0, method) ((  bool (*) (Collection_1_t519366476 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m888444215_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1218313197_gshared (Collection_1_t519366476 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1218313197(__this, ___value0, method) ((  int32_t (*) (Collection_1_t519366476 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1218313197_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m746960396_gshared (Collection_1_t519366476 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m746960396(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t519366476 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m746960396_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m415793002_gshared (Collection_1_t519366476 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m415793002(__this, ___value0, method) ((  void (*) (Collection_1_t519366476 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m415793002_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3694453295_gshared (Collection_1_t519366476 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3694453295(__this, method) ((  bool (*) (Collection_1_t519366476 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3694453295_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2826794963_gshared (Collection_1_t519366476 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2826794963(__this, method) ((  Il2CppObject * (*) (Collection_1_t519366476 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2826794963_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1266168096_gshared (Collection_1_t519366476 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1266168096(__this, method) ((  bool (*) (Collection_1_t519366476 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1266168096_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m135729899_gshared (Collection_1_t519366476 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m135729899(__this, method) ((  bool (*) (Collection_1_t519366476 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m135729899_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3357070118_gshared (Collection_1_t519366476 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3357070118(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t519366476 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3357070118_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3086876153_gshared (Collection_1_t519366476 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3086876153(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t519366476 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3086876153_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::Add(T)
extern "C"  void Collection_1_Add_m441594848_gshared (Collection_1_t519366476 * __this, InternalMsg_t977621722  ___item0, const MethodInfo* method);
#define Collection_1_Add_m441594848(__this, ___item0, method) ((  void (*) (Collection_1_t519366476 *, InternalMsg_t977621722 , const MethodInfo*))Collection_1_Add_m441594848_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::Clear()
extern "C"  void Collection_1_Clear_m3654579676_gshared (Collection_1_t519366476 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3654579676(__this, method) ((  void (*) (Collection_1_t519366476 *, const MethodInfo*))Collection_1_Clear_m3654579676_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1701418982_gshared (Collection_1_t519366476 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1701418982(__this, method) ((  void (*) (Collection_1_t519366476 *, const MethodInfo*))Collection_1_ClearItems_m1701418982_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::Contains(T)
extern "C"  bool Collection_1_Contains_m2462703882_gshared (Collection_1_t519366476 * __this, InternalMsg_t977621722  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2462703882(__this, ___item0, method) ((  bool (*) (Collection_1_t519366476 *, InternalMsg_t977621722 , const MethodInfo*))Collection_1_Contains_m2462703882_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3898638388_gshared (Collection_1_t519366476 * __this, InternalMsgU5BU5D_t2023740543* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3898638388(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t519366476 *, InternalMsgU5BU5D_t2023740543*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3898638388_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1132600799_gshared (Collection_1_t519366476 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1132600799(__this, method) ((  Il2CppObject* (*) (Collection_1_t519366476 *, const MethodInfo*))Collection_1_GetEnumerator_m1132600799_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m4161011178_gshared (Collection_1_t519366476 * __this, InternalMsg_t977621722  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m4161011178(__this, ___item0, method) ((  int32_t (*) (Collection_1_t519366476 *, InternalMsg_t977621722 , const MethodInfo*))Collection_1_IndexOf_m4161011178_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3089578797_gshared (Collection_1_t519366476 * __this, int32_t ___index0, InternalMsg_t977621722  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3089578797(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t519366476 *, int32_t, InternalMsg_t977621722 , const MethodInfo*))Collection_1_Insert_m3089578797_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3086942006_gshared (Collection_1_t519366476 * __this, int32_t ___index0, InternalMsg_t977621722  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3086942006(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t519366476 *, int32_t, InternalMsg_t977621722 , const MethodInfo*))Collection_1_InsertItem_m3086942006_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::Remove(T)
extern "C"  bool Collection_1_Remove_m2308405221_gshared (Collection_1_t519366476 * __this, InternalMsg_t977621722  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2308405221(__this, ___item0, method) ((  bool (*) (Collection_1_t519366476 *, InternalMsg_t977621722 , const MethodInfo*))Collection_1_Remove_m2308405221_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m4257078601_gshared (Collection_1_t519366476 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m4257078601(__this, ___index0, method) ((  void (*) (Collection_1_t519366476 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m4257078601_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1301380477_gshared (Collection_1_t519366476 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1301380477(__this, ___index0, method) ((  void (*) (Collection_1_t519366476 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1301380477_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3933990023_gshared (Collection_1_t519366476 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3933990023(__this, method) ((  int32_t (*) (Collection_1_t519366476 *, const MethodInfo*))Collection_1_get_Count_m3933990023_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::get_Item(System.Int32)
extern "C"  InternalMsg_t977621722  Collection_1_get_Item_m2520846141_gshared (Collection_1_t519366476 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2520846141(__this, ___index0, method) ((  InternalMsg_t977621722  (*) (Collection_1_t519366476 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2520846141_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m821160150_gshared (Collection_1_t519366476 * __this, int32_t ___index0, InternalMsg_t977621722  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m821160150(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t519366476 *, int32_t, InternalMsg_t977621722 , const MethodInfo*))Collection_1_set_Item_m821160150_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2018102105_gshared (Collection_1_t519366476 * __this, int32_t ___index0, InternalMsg_t977621722  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2018102105(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t519366476 *, int32_t, InternalMsg_t977621722 , const MethodInfo*))Collection_1_SetItem_m2018102105_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1514498012_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1514498012(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1514498012_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::ConvertItem(System.Object)
extern "C"  InternalMsg_t977621722  Collection_1_ConvertItem_m1707906972_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1707906972(__this /* static, unused */, ___item0, method) ((  InternalMsg_t977621722  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1707906972_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m550259000_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m550259000(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m550259000_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1611399030_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1611399030(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1611399030_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.LocalClient/InternalMsg>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1976007629_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1976007629(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1976007629_gshared)(__this /* static, unused */, ___list0, method)
