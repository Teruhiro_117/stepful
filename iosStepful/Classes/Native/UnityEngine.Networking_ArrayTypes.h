﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// UnityEngine.Networking.PlayerController
struct PlayerController_t612925521;
// UnityEngine.Networking.NetworkIdentity
struct NetworkIdentity_t1766639790;
// UnityEngine.Networking.NetworkSystem.PeerInfoMessage
struct PeerInfoMessage_t4070066639;
// UnityEngine.Networking.MessageBase
struct MessageBase_t2552428296;
// UnityEngine.Networking.NetworkConnection
struct NetworkConnection_t107267906;
// UnityEngine.Networking.NetworkClient
struct NetworkClient_t696867603;
// UnityEngine.Networking.NetworkBehaviour/Invoker
struct Invoker_t3859202811;
// UnityEngine.Networking.NetworkMessageDelegate
struct NetworkMessageDelegate_t1861659952;
// UnityEngine.Networking.NetworkConnection/PacketStat
struct PacketStat_t691343460;
// UnityEngine.Networking.ChannelBuffer
struct ChannelBuffer_t3774159139;
// UnityEngine.Networking.NetworkBehaviour
struct NetworkBehaviour_t3873055601;
// UnityEngine.Networking.NetworkLobbyPlayer
struct NetworkLobbyPlayer_t2123032571;
// UnityEngine.Networking.SpawnDelegate
struct SpawnDelegate_t3327865986;
// UnityEngine.Networking.UnSpawnDelegate
struct UnSpawnDelegate_t3624459119;
// UnityEngine.Networking.NetworkTransformChild
struct NetworkTransformChild_t2933852510;

#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan1682596885.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Playe612925521.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Networ33998832.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1766639790.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo835211239.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2931030083.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw4070066639.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Mess2552428296.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clien877818919.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo396494387.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo107267906.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo524088197.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo696867603.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3859202811.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1861659952.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo691343460.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan3774159139.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo646317982.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3873055601.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1517095017.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2123032571.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw4207851900.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2053376121.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Spaw3327865986.h"
#include "UnityEngine_Networking_UnityEngine_Networking_UnSp3624459119.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2933852510.h"

#pragma once
// UnityEngine.Networking.ChannelPacket[]
struct ChannelPacketU5BU5D_t3883591672  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ChannelPacket_t1682596885  m_Items[1];

public:
	inline ChannelPacket_t1682596885  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ChannelPacket_t1682596885 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ChannelPacket_t1682596885  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ChannelPacket_t1682596885  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ChannelPacket_t1682596885 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ChannelPacket_t1682596885  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.PlayerController[]
struct PlayerControllerU5BU5D_t3440576332  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PlayerController_t612925521 * m_Items[1];

public:
	inline PlayerController_t612925521 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PlayerController_t612925521 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PlayerController_t612925521 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline PlayerController_t612925521 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PlayerController_t612925521 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PlayerController_t612925521 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.NetworkInstanceId[]
struct NetworkInstanceIdU5BU5D_t2259872337  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkInstanceId_t33998832  m_Items[1];

public:
	inline NetworkInstanceId_t33998832  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NetworkInstanceId_t33998832 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NetworkInstanceId_t33998832  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline NetworkInstanceId_t33998832  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NetworkInstanceId_t33998832 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NetworkInstanceId_t33998832  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.NetworkIdentity[]
struct NetworkIdentityU5BU5D_t2912536795  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkIdentity_t1766639790 * m_Items[1];

public:
	inline NetworkIdentity_t1766639790 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NetworkIdentity_t1766639790 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NetworkIdentity_t1766639790 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline NetworkIdentity_t1766639790 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NetworkIdentity_t1766639790 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NetworkIdentity_t1766639790 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.NetworkHash128[]
struct NetworkHash128U5BU5D_t2760018078  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkHash128_t835211239  m_Items[1];

public:
	inline NetworkHash128_t835211239  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NetworkHash128_t835211239 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NetworkHash128_t835211239  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline NetworkHash128_t835211239  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NetworkHash128_t835211239 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NetworkHash128_t835211239  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.NetworkSceneId[]
struct NetworkSceneIdU5BU5D_t478005074  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkSceneId_t2931030083  m_Items[1];

public:
	inline NetworkSceneId_t2931030083  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NetworkSceneId_t2931030083 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NetworkSceneId_t2931030083  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline NetworkSceneId_t2931030083  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NetworkSceneId_t2931030083 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NetworkSceneId_t2931030083  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.NetworkSystem.PeerInfoMessage[]
struct PeerInfoMessageU5BU5D_t2669814294  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PeerInfoMessage_t4070066639 * m_Items[1];

public:
	inline PeerInfoMessage_t4070066639 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PeerInfoMessage_t4070066639 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PeerInfoMessage_t4070066639 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline PeerInfoMessage_t4070066639 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PeerInfoMessage_t4070066639 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PeerInfoMessage_t4070066639 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.MessageBase[]
struct MessageBaseU5BU5D_t1678533593  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MessageBase_t2552428296 * m_Items[1];

public:
	inline MessageBase_t2552428296 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MessageBase_t2552428296 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MessageBase_t2552428296 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline MessageBase_t2552428296 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MessageBase_t2552428296 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MessageBase_t2552428296 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.ClientScene/PendingOwner[]
struct PendingOwnerU5BU5D_t2720009054  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PendingOwner_t877818919  m_Items[1];

public:
	inline PendingOwner_t877818919  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PendingOwner_t877818919 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PendingOwner_t877818919  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PendingOwner_t877818919  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PendingOwner_t877818919 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PendingOwner_t877818919  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.NetworkSystem.PeerInfoPlayer[]
struct PeerInfoPlayerU5BU5D_t1632998306  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PeerInfoPlayer_t396494387  m_Items[1];

public:
	inline PeerInfoPlayer_t396494387  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PeerInfoPlayer_t396494387 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PeerInfoPlayer_t396494387  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PeerInfoPlayer_t396494387  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PeerInfoPlayer_t396494387 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PeerInfoPlayer_t396494387  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.NetworkConnection[]
struct NetworkConnectionU5BU5D_t214055287  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkConnection_t107267906 * m_Items[1];

public:
	inline NetworkConnection_t107267906 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NetworkConnection_t107267906 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NetworkConnection_t107267906 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline NetworkConnection_t107267906 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NetworkConnection_t107267906 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NetworkConnection_t107267906 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.LocalClient/InternalMsg[]
struct InternalMsgU5BU5D_t2023740543  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) InternalMsg_t977621722  m_Items[1];

public:
	inline InternalMsg_t977621722  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InternalMsg_t977621722 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InternalMsg_t977621722  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline InternalMsg_t977621722  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InternalMsg_t977621722 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InternalMsg_t977621722  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.NetworkSystem.CRCMessageEntry[]
struct CRCMessageEntryU5BU5D_t2803924168  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CRCMessageEntry_t524088197  m_Items[1];

public:
	inline CRCMessageEntry_t524088197  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline CRCMessageEntry_t524088197 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, CRCMessageEntry_t524088197  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline CRCMessageEntry_t524088197  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline CRCMessageEntry_t524088197 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, CRCMessageEntry_t524088197  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.NetworkClient[]
struct NetworkClientU5BU5D_t2024861506  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkClient_t696867603 * m_Items[1];

public:
	inline NetworkClient_t696867603 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NetworkClient_t696867603 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NetworkClient_t696867603 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline NetworkClient_t696867603 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NetworkClient_t696867603 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NetworkClient_t696867603 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.NetworkBehaviour/Invoker[]
struct InvokerU5BU5D_t3964248762  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Invoker_t3859202811 * m_Items[1];

public:
	inline Invoker_t3859202811 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Invoker_t3859202811 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Invoker_t3859202811 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Invoker_t3859202811 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Invoker_t3859202811 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Invoker_t3859202811 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.NetworkMessageDelegate[]
struct NetworkMessageDelegateU5BU5D_t1736345617  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkMessageDelegate_t1861659952 * m_Items[1];

public:
	inline NetworkMessageDelegate_t1861659952 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NetworkMessageDelegate_t1861659952 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NetworkMessageDelegate_t1861659952 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline NetworkMessageDelegate_t1861659952 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NetworkMessageDelegate_t1861659952 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NetworkMessageDelegate_t1861659952 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.NetworkConnection/PacketStat[]
struct PacketStatU5BU5D_t3328986509  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PacketStat_t691343460 * m_Items[1];

public:
	inline PacketStat_t691343460 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PacketStat_t691343460 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PacketStat_t691343460 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline PacketStat_t691343460 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PacketStat_t691343460 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PacketStat_t691343460 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.ChannelBuffer[]
struct ChannelBufferU5BU5D_t3955402482  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ChannelBuffer_t3774159139 * m_Items[1];

public:
	inline ChannelBuffer_t3774159139 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ChannelBuffer_t3774159139 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ChannelBuffer_t3774159139 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ChannelBuffer_t3774159139 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ChannelBuffer_t3774159139 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ChannelBuffer_t3774159139 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.NetworkBroadcastResult[]
struct NetworkBroadcastResultU5BU5D_t2110160939  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkBroadcastResult_t646317982  m_Items[1];

public:
	inline NetworkBroadcastResult_t646317982  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NetworkBroadcastResult_t646317982 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NetworkBroadcastResult_t646317982  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline NetworkBroadcastResult_t646317982  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NetworkBroadcastResult_t646317982 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NetworkBroadcastResult_t646317982  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.NetworkBehaviour[]
struct NetworkBehaviourU5BU5D_t2137248684  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkBehaviour_t3873055601 * m_Items[1];

public:
	inline NetworkBehaviour_t3873055601 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NetworkBehaviour_t3873055601 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NetworkBehaviour_t3873055601 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline NetworkBehaviour_t3873055601 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NetworkBehaviour_t3873055601 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NetworkBehaviour_t3873055601 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.NetworkLobbyManager/PendingPlayer[]
struct PendingPlayerU5BU5D_t220373972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PendingPlayer_t1517095017  m_Items[1];

public:
	inline PendingPlayer_t1517095017  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PendingPlayer_t1517095017 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PendingPlayer_t1517095017  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PendingPlayer_t1517095017  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PendingPlayer_t1517095017 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PendingPlayer_t1517095017  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.NetworkLobbyPlayer[]
struct NetworkLobbyPlayerU5BU5D_t650070458  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkLobbyPlayer_t2123032571 * m_Items[1];

public:
	inline NetworkLobbyPlayer_t2123032571 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NetworkLobbyPlayer_t2123032571 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NetworkLobbyPlayer_t2123032571 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline NetworkLobbyPlayer_t2123032571 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NetworkLobbyPlayer_t2123032571 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NetworkLobbyPlayer_t2123032571 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers[]
struct ConnectionPendingPlayersU5BU5D_t324085013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ConnectionPendingPlayers_t4207851900  m_Items[1];

public:
	inline ConnectionPendingPlayers_t4207851900  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ConnectionPendingPlayers_t4207851900 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ConnectionPendingPlayers_t4207851900  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ConnectionPendingPlayers_t4207851900  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ConnectionPendingPlayers_t4207851900 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ConnectionPendingPlayers_t4207851900  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo[]
struct PendingPlayerInfoU5BU5D_t2199507332  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PendingPlayerInfo_t2053376121  m_Items[1];

public:
	inline PendingPlayerInfo_t2053376121  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PendingPlayerInfo_t2053376121 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PendingPlayerInfo_t2053376121  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline PendingPlayerInfo_t2053376121  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PendingPlayerInfo_t2053376121 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PendingPlayerInfo_t2053376121  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Networking.SpawnDelegate[]
struct SpawnDelegateU5BU5D_t3862179639  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SpawnDelegate_t3327865986 * m_Items[1];

public:
	inline SpawnDelegate_t3327865986 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SpawnDelegate_t3327865986 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SpawnDelegate_t3327865986 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SpawnDelegate_t3327865986 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SpawnDelegate_t3327865986 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SpawnDelegate_t3327865986 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.UnSpawnDelegate[]
struct UnSpawnDelegateU5BU5D_t3637900790  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UnSpawnDelegate_t3624459119 * m_Items[1];

public:
	inline UnSpawnDelegate_t3624459119 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UnSpawnDelegate_t3624459119 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UnSpawnDelegate_t3624459119 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline UnSpawnDelegate_t3624459119 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UnSpawnDelegate_t3624459119 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UnSpawnDelegate_t3624459119 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Networking.NetworkTransformChild[]
struct NetworkTransformChildU5BU5D_t713457515  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NetworkTransformChild_t2933852510 * m_Items[1];

public:
	inline NetworkTransformChild_t2933852510 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NetworkTransformChild_t2933852510 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NetworkTransformChild_t2933852510 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline NetworkTransformChild_t2933852510 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NetworkTransformChild_t2933852510 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NetworkTransformChild_t2933852510 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
