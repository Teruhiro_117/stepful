﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// banmen
struct banmen_t2510752313;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Koma
struct  Koma_t3011766596  : public Il2CppObject
{
public:
	// banmen Koma::ban
	banmen_t2510752313 * ___ban_0;
	// UnityEngine.Sprite Koma::komaView
	Sprite_t309593783 * ___komaView_1;
	// System.Int32 Koma::komaNumber
	int32_t ___komaNumber_2;
	// System.Int32 Koma::resources
	int32_t ___resources_3;
	// UnityEngine.Sprite Koma::brokeKomaView
	Sprite_t309593783 * ___brokeKomaView_4;
	// System.Int32 Koma::komaMaxTime
	int32_t ___komaMaxTime_5;

public:
	inline static int32_t get_offset_of_ban_0() { return static_cast<int32_t>(offsetof(Koma_t3011766596, ___ban_0)); }
	inline banmen_t2510752313 * get_ban_0() const { return ___ban_0; }
	inline banmen_t2510752313 ** get_address_of_ban_0() { return &___ban_0; }
	inline void set_ban_0(banmen_t2510752313 * value)
	{
		___ban_0 = value;
		Il2CppCodeGenWriteBarrier(&___ban_0, value);
	}

	inline static int32_t get_offset_of_komaView_1() { return static_cast<int32_t>(offsetof(Koma_t3011766596, ___komaView_1)); }
	inline Sprite_t309593783 * get_komaView_1() const { return ___komaView_1; }
	inline Sprite_t309593783 ** get_address_of_komaView_1() { return &___komaView_1; }
	inline void set_komaView_1(Sprite_t309593783 * value)
	{
		___komaView_1 = value;
		Il2CppCodeGenWriteBarrier(&___komaView_1, value);
	}

	inline static int32_t get_offset_of_komaNumber_2() { return static_cast<int32_t>(offsetof(Koma_t3011766596, ___komaNumber_2)); }
	inline int32_t get_komaNumber_2() const { return ___komaNumber_2; }
	inline int32_t* get_address_of_komaNumber_2() { return &___komaNumber_2; }
	inline void set_komaNumber_2(int32_t value)
	{
		___komaNumber_2 = value;
	}

	inline static int32_t get_offset_of_resources_3() { return static_cast<int32_t>(offsetof(Koma_t3011766596, ___resources_3)); }
	inline int32_t get_resources_3() const { return ___resources_3; }
	inline int32_t* get_address_of_resources_3() { return &___resources_3; }
	inline void set_resources_3(int32_t value)
	{
		___resources_3 = value;
	}

	inline static int32_t get_offset_of_brokeKomaView_4() { return static_cast<int32_t>(offsetof(Koma_t3011766596, ___brokeKomaView_4)); }
	inline Sprite_t309593783 * get_brokeKomaView_4() const { return ___brokeKomaView_4; }
	inline Sprite_t309593783 ** get_address_of_brokeKomaView_4() { return &___brokeKomaView_4; }
	inline void set_brokeKomaView_4(Sprite_t309593783 * value)
	{
		___brokeKomaView_4 = value;
		Il2CppCodeGenWriteBarrier(&___brokeKomaView_4, value);
	}

	inline static int32_t get_offset_of_komaMaxTime_5() { return static_cast<int32_t>(offsetof(Koma_t3011766596, ___komaMaxTime_5)); }
	inline int32_t get_komaMaxTime_5() const { return ___komaMaxTime_5; }
	inline int32_t* get_address_of_komaMaxTime_5() { return &___komaMaxTime_5; }
	inline void set_komaMaxTime_5(int32_t value)
	{
		___komaMaxTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
