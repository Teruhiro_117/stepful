﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject[0...,0...]
struct GameObjectU5BU2CU5D_t3057952155;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Distribution
struct  Distribution_t1524299538  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[0...,0...] Distribution::prefab
	GameObjectU5BU2CU5D_t3057952155* ___prefab_2;
	// UnityEngine.GameObject Distribution::prefabResource
	GameObject_t1756533147 * ___prefabResource_3;
	// UnityEngine.GameObject Distribution::prefabEResource
	GameObject_t1756533147 * ___prefabEResource_4;
	// UnityEngine.GameObject Distribution::prefabBackGround
	GameObject_t1756533147 * ___prefabBackGround_5;
	// UnityEngine.GameObject Distribution::prefabRefresh
	GameObject_t1756533147 * ___prefabRefresh_6;

public:
	inline static int32_t get_offset_of_prefab_2() { return static_cast<int32_t>(offsetof(Distribution_t1524299538, ___prefab_2)); }
	inline GameObjectU5BU2CU5D_t3057952155* get_prefab_2() const { return ___prefab_2; }
	inline GameObjectU5BU2CU5D_t3057952155** get_address_of_prefab_2() { return &___prefab_2; }
	inline void set_prefab_2(GameObjectU5BU2CU5D_t3057952155* value)
	{
		___prefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_2, value);
	}

	inline static int32_t get_offset_of_prefabResource_3() { return static_cast<int32_t>(offsetof(Distribution_t1524299538, ___prefabResource_3)); }
	inline GameObject_t1756533147 * get_prefabResource_3() const { return ___prefabResource_3; }
	inline GameObject_t1756533147 ** get_address_of_prefabResource_3() { return &___prefabResource_3; }
	inline void set_prefabResource_3(GameObject_t1756533147 * value)
	{
		___prefabResource_3 = value;
		Il2CppCodeGenWriteBarrier(&___prefabResource_3, value);
	}

	inline static int32_t get_offset_of_prefabEResource_4() { return static_cast<int32_t>(offsetof(Distribution_t1524299538, ___prefabEResource_4)); }
	inline GameObject_t1756533147 * get_prefabEResource_4() const { return ___prefabEResource_4; }
	inline GameObject_t1756533147 ** get_address_of_prefabEResource_4() { return &___prefabEResource_4; }
	inline void set_prefabEResource_4(GameObject_t1756533147 * value)
	{
		___prefabEResource_4 = value;
		Il2CppCodeGenWriteBarrier(&___prefabEResource_4, value);
	}

	inline static int32_t get_offset_of_prefabBackGround_5() { return static_cast<int32_t>(offsetof(Distribution_t1524299538, ___prefabBackGround_5)); }
	inline GameObject_t1756533147 * get_prefabBackGround_5() const { return ___prefabBackGround_5; }
	inline GameObject_t1756533147 ** get_address_of_prefabBackGround_5() { return &___prefabBackGround_5; }
	inline void set_prefabBackGround_5(GameObject_t1756533147 * value)
	{
		___prefabBackGround_5 = value;
		Il2CppCodeGenWriteBarrier(&___prefabBackGround_5, value);
	}

	inline static int32_t get_offset_of_prefabRefresh_6() { return static_cast<int32_t>(offsetof(Distribution_t1524299538, ___prefabRefresh_6)); }
	inline GameObject_t1756533147 * get_prefabRefresh_6() const { return ___prefabRefresh_6; }
	inline GameObject_t1756533147 ** get_address_of_prefabRefresh_6() { return &___prefabRefresh_6; }
	inline void set_prefabRefresh_6(GameObject_t1756533147 * value)
	{
		___prefabRefresh_6 = value;
		Il2CppCodeGenWriteBarrier(&___prefabRefresh_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
