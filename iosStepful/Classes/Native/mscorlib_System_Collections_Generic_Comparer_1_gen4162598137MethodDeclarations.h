﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct Comparer_1_t4162598137;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor()
extern "C"  void Comparer_1__ctor_m4256891075_gshared (Comparer_1_t4162598137 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m4256891075(__this, method) ((  void (*) (Comparer_1_t4162598137 *, const MethodInfo*))Comparer_1__ctor_m4256891075_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Networking.LocalClient/InternalMsg>::.cctor()
extern "C"  void Comparer_1__cctor_m456193124_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m456193124(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m456193124_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1336015070_gshared (Comparer_1_t4162598137 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m1336015070(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t4162598137 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m1336015070_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Networking.LocalClient/InternalMsg>::get_Default()
extern "C"  Comparer_1_t4162598137 * Comparer_1_get_Default_m4156196795_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m4156196795(__this /* static, unused */, method) ((  Comparer_1_t4162598137 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m4156196795_gshared)(__this /* static, unused */, method)
