﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkDiscovery
struct NetworkDiscovery_t3838653650;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Networking.NetworkBroadcastResult>
struct Dictionary_2_t2561097244;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Networking.NetworkDiscovery::.ctor()
extern "C"  void NetworkDiscovery__ctor_m1301338405 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.NetworkDiscovery::get_broadcastPort()
extern "C"  int32_t NetworkDiscovery_get_broadcastPort_m1940464592 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_broadcastPort(System.Int32)
extern "C"  void NetworkDiscovery_set_broadcastPort_m3057535763 (NetworkDiscovery_t3838653650 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.NetworkDiscovery::get_broadcastKey()
extern "C"  int32_t NetworkDiscovery_get_broadcastKey_m2285413284 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_broadcastKey(System.Int32)
extern "C"  void NetworkDiscovery_set_broadcastKey_m514439229 (NetworkDiscovery_t3838653650 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.NetworkDiscovery::get_broadcastVersion()
extern "C"  int32_t NetworkDiscovery_get_broadcastVersion_m1672581151 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_broadcastVersion(System.Int32)
extern "C"  void NetworkDiscovery_set_broadcastVersion_m1551789670 (NetworkDiscovery_t3838653650 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.NetworkDiscovery::get_broadcastSubVersion()
extern "C"  int32_t NetworkDiscovery_get_broadcastSubVersion_m3377021923 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_broadcastSubVersion(System.Int32)
extern "C"  void NetworkDiscovery_set_broadcastSubVersion_m3568774640 (NetworkDiscovery_t3838653650 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.NetworkDiscovery::get_broadcastInterval()
extern "C"  int32_t NetworkDiscovery_get_broadcastInterval_m2716675766 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_broadcastInterval(System.Int32)
extern "C"  void NetworkDiscovery_set_broadcastInterval_m51832085 (NetworkDiscovery_t3838653650 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkDiscovery::get_useNetworkManager()
extern "C"  bool NetworkDiscovery_get_useNetworkManager_m532525582 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_useNetworkManager(System.Boolean)
extern "C"  void NetworkDiscovery_set_useNetworkManager_m182202893 (NetworkDiscovery_t3838653650 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.NetworkDiscovery::get_broadcastData()
extern "C"  String_t* NetworkDiscovery_get_broadcastData_m3979828438 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_broadcastData(System.String)
extern "C"  void NetworkDiscovery_set_broadcastData_m3358601133 (NetworkDiscovery_t3838653650 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkDiscovery::get_showGUI()
extern "C"  bool NetworkDiscovery_get_showGUI_m3987525012 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_showGUI(System.Boolean)
extern "C"  void NetworkDiscovery_set_showGUI_m3337648675 (NetworkDiscovery_t3838653650 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.NetworkDiscovery::get_offsetX()
extern "C"  int32_t NetworkDiscovery_get_offsetX_m329556463 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_offsetX(System.Int32)
extern "C"  void NetworkDiscovery_set_offsetX_m1899679264 (NetworkDiscovery_t3838653650 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.NetworkDiscovery::get_offsetY()
extern "C"  int32_t NetworkDiscovery_get_offsetY_m188393962 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_offsetY(System.Int32)
extern "C"  void NetworkDiscovery_set_offsetY_m518947457 (NetworkDiscovery_t3838653650 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.NetworkDiscovery::get_hostId()
extern "C"  int32_t NetworkDiscovery_get_hostId_m3721640003 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_hostId(System.Int32)
extern "C"  void NetworkDiscovery_set_hostId_m370971292 (NetworkDiscovery_t3838653650 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkDiscovery::get_running()
extern "C"  bool NetworkDiscovery_get_running_m2481752583 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_running(System.Boolean)
extern "C"  void NetworkDiscovery_set_running_m2439210474 (NetworkDiscovery_t3838653650 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkDiscovery::get_isServer()
extern "C"  bool NetworkDiscovery_get_isServer_m2977516167 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_isServer(System.Boolean)
extern "C"  void NetworkDiscovery_set_isServer_m211671852 (NetworkDiscovery_t3838653650 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkDiscovery::get_isClient()
extern "C"  bool NetworkDiscovery_get_isClient_m3058585035 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::set_isClient(System.Boolean)
extern "C"  void NetworkDiscovery_set_isClient_m3299237296 (NetworkDiscovery_t3838653650 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Networking.NetworkBroadcastResult> UnityEngine.Networking.NetworkDiscovery::get_broadcastsReceived()
extern "C"  Dictionary_2_t2561097244 * NetworkDiscovery_get_broadcastsReceived_m1095397958 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Networking.NetworkDiscovery::StringToBytes(System.String)
extern "C"  ByteU5BU5D_t3397334013* NetworkDiscovery_StringToBytes_m1238460230 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.NetworkDiscovery::BytesToString(System.Byte[])
extern "C"  String_t* NetworkDiscovery_BytesToString_m4112723556 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkDiscovery::Initialize()
extern "C"  bool NetworkDiscovery_Initialize_m203710797 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkDiscovery::StartAsClient()
extern "C"  bool NetworkDiscovery_StartAsClient_m387053616 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkDiscovery::StartAsServer()
extern "C"  bool NetworkDiscovery_StartAsServer_m3391338948 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::StopBroadcast()
extern "C"  void NetworkDiscovery_StopBroadcast_m2532290084 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::Update()
extern "C"  void NetworkDiscovery_Update_m3203444582 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::OnDestroy()
extern "C"  void NetworkDiscovery_OnDestroy_m3107159690 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::OnReceivedBroadcast(System.String,System.String)
extern "C"  void NetworkDiscovery_OnReceivedBroadcast_m330983496 (NetworkDiscovery_t3838653650 * __this, String_t* ___fromAddress0, String_t* ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkDiscovery::OnGUI()
extern "C"  void NetworkDiscovery_OnGUI_m215265295 (NetworkDiscovery_t3838653650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
