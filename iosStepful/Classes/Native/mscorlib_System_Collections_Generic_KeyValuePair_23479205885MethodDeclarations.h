﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23479205885.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo835211239.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4239967155_gshared (KeyValuePair_2_t3479205885 * __this, NetworkHash128_t835211239  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m4239967155(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3479205885 *, NetworkHash128_t835211239 , Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m4239967155_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::get_Key()
extern "C"  NetworkHash128_t835211239  KeyValuePair_2_get_Key_m857379697_gshared (KeyValuePair_2_t3479205885 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m857379697(__this, method) ((  NetworkHash128_t835211239  (*) (KeyValuePair_2_t3479205885 *, const MethodInfo*))KeyValuePair_2_get_Key_m857379697_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m833328204_gshared (KeyValuePair_2_t3479205885 * __this, NetworkHash128_t835211239  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m833328204(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3479205885 *, NetworkHash128_t835211239 , const MethodInfo*))KeyValuePair_2_set_Key_m833328204_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3149317201_gshared (KeyValuePair_2_t3479205885 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3149317201(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3479205885 *, const MethodInfo*))KeyValuePair_2_get_Value_m3149317201_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m443154412_gshared (KeyValuePair_2_t3479205885 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m443154412(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3479205885 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m443154412_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1004546060_gshared (KeyValuePair_2_t3479205885 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1004546060(__this, method) ((  String_t* (*) (KeyValuePair_2_t3479205885 *, const MethodInfo*))KeyValuePair_2_ToString_m1004546060_gshared)(__this, method)
