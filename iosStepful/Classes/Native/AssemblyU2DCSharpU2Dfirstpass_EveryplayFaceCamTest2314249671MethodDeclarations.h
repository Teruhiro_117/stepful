﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayFaceCamTest
struct EveryplayFaceCamTest_t2314249671;

#include "codegen/il2cpp-codegen.h"

// System.Void EveryplayFaceCamTest::.ctor()
extern "C"  void EveryplayFaceCamTest__ctor_m1308166180 (EveryplayFaceCamTest_t2314249671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayFaceCamTest::Awake()
extern "C"  void EveryplayFaceCamTest_Awake_m674056825 (EveryplayFaceCamTest_t2314249671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayFaceCamTest::Start()
extern "C"  void EveryplayFaceCamTest_Start_m2030955940 (EveryplayFaceCamTest_t2314249671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayFaceCamTest::Destroy()
extern "C"  void EveryplayFaceCamTest_Destroy_m1427295446 (EveryplayFaceCamTest_t2314249671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayFaceCamTest::CheckFaceCamRecordingPermission(System.Boolean)
extern "C"  void EveryplayFaceCamTest_CheckFaceCamRecordingPermission_m221574661 (EveryplayFaceCamTest_t2314249671 * __this, bool ___granted0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayFaceCamTest::OnGUI()
extern "C"  void EveryplayFaceCamTest_OnGUI_m1118772452 (EveryplayFaceCamTest_t2314249671 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
