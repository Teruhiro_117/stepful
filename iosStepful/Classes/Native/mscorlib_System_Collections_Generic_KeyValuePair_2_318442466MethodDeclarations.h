﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22290690628MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Networking.NetworkBroadcastResult>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3859895194(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t318442466 *, String_t*, NetworkBroadcastResult_t646317982 , const MethodInfo*))KeyValuePair_2__ctor_m3420001604_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Networking.NetworkBroadcastResult>::get_Key()
#define KeyValuePair_2_get_Key_m1476812584(__this, method) ((  String_t* (*) (KeyValuePair_2_t318442466 *, const MethodInfo*))KeyValuePair_2_get_Key_m2967481714_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Networking.NetworkBroadcastResult>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4100912173(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t318442466 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m2740226929_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Networking.NetworkBroadcastResult>::get_Value()
#define KeyValuePair_2_get_Value_m2388591912(__this, method) ((  NetworkBroadcastResult_t646317982  (*) (KeyValuePair_2_t318442466 *, const MethodInfo*))KeyValuePair_2_get_Value_m838596530_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Networking.NetworkBroadcastResult>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2005847125(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t318442466 *, NetworkBroadcastResult_t646317982 , const MethodInfo*))KeyValuePair_2_set_Value_m3010936665_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Networking.NetworkBroadcastResult>::ToString()
#define KeyValuePair_2_ToString_m2120138259(__this, method) ((  String_t* (*) (KeyValuePair_2_t318442466 *, const MethodInfo*))KeyValuePair_2_ToString_m2156236655_gshared)(__this, method)
