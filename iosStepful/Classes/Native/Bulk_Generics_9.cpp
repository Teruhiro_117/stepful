﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t2994157524;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t1585555208;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t3194695850;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t3702943073;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t3758245971;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t867319046;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1518803153;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t243638650;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// System.Collections.Generic.List`1<UnityEngine.Experimental.Director.Playable>
struct List_1_t3036666680;
// System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>
struct List_1_t1051718017;
// System.Collections.Generic.List`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct List_1_t246940051;
// System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct List_1_t346742854;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct List_1_t886216149;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct List_1_t1422497253;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct List_1_t4188176625;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct List_1_t4060582815;
// System.Collections.Generic.List`1<UnityEngine.Networking.QosType>
struct List_1_t1373013615;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t2425757932;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t2990399006;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t573379950;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1612828713;
// System.Collections.Generic.IEnumerable`1<System.Boolean>
struct IEnumerable_1_t4117701763;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_t1301098545;
// System.Collections.Generic.ICollection`1<System.Boolean>
struct ICollection_1_t482682727;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>
struct ReadOnlyCollection_1_t4011360410;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// System.Predicate`1<System.Boolean>
struct Predicate_1_t2268544833;
// System.Collections.Generic.IComparer`1<System.Boolean>
struct IComparer_1_t1780037840;
// System.Comparison`1<System.Boolean>
struct Comparison_1_t792346273;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t330981690;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t990929950;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ReadOnlyCollection_1_t224640337;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Predicate_1_t2776792056;
// System.Collections.Generic.IComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IComparer_1_t2288285063;
// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Comparison_1_t1300593496;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t2364004493;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3023952753;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t2257663140;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Predicate`1<System.Int32>
struct Predicate_1_t514847563;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t26340570;
// System.Comparison`1<System.Int32>
struct Comparison_1_t3333616299;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2875234987;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t643912417;
// System.Comparison`1<System.Object>
struct Comparison_1_t3951188146;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerable_1_t386284588;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1864648666;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ICollection_1_t1046232848;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ReadOnlyCollection_1_t279943235;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3304067486;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t2832094954;
// System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeNamedArgument>
struct IComparer_1_t2343587961;
// System.Comparison`1<System.Reflection.CustomAttributeNamedArgument>
struct Comparison_1_t1355896394;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerable_1_t1790324959;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3268689037;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ICollection_1_t2450273219;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t1683983606;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1075686591;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t4236135325;
// System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeTypedArgument>
struct IComparer_1_t3747628332;
// System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparison_1_t2759936765;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23823471070.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23823471070MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int164041245914MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21008373517.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21008373517MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_Label4243202660.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_973022757.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_973022757MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw4207851900.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21683227291.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21683227291MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22553450683.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22553450683MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22290690628.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22290690628MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo646317982.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3138797698.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23479205885.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23479205885MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo835211239.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo835211239MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22979060144.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22979060144MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Networ33998832.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Networ33998832MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21197192881.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21197192881MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2931030083.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2931030083MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23709506243.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23709506243MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID348058649.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1817063546.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1817063546MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2994157524.h"
#include "mscorlib_System_UInt322149682021.h"
#include "System_System_Collections_Generic_LinkedListNode_11585555208.h"
#include "mscorlib_System_ObjectDisposedException2695136451MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException2695136451.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2994157524MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedListNode_11585555208MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2729425524.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2729425524MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3194695850.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3237672747.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3237672747MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3702943073.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3292975645.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3292975645MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3758245971.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat402048720.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat402048720MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen867319046.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat980360738.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat980360738MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1053532827.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1053532827MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1518803153.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4073335620.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4073335620MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen243638650.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3220004478.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3220004478MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3685274804.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2571396354.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2571396354MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3036666680.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat586447691.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat586447691MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1051718017.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan1682596885.h"
#include "UnityEngine.Networking_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4076637021.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4076637021MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen246940051.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clien877818919.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4176439824.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4176439824MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen346742854.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat420945823.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat420945823MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen886216149.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1517095017.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat957226927.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat957226927MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1422497253.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2053376121.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3722906299.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3722906299MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4188176625.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo524088197.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3595312489.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3595312489MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4060582815.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo396494387.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat907743289.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat907743289MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1373013615.h"
#include "UnityEngine_UnityEngine_Networking_QosType2003892483.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1960487606.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1960487606MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2425757932.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2525128680.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2525128680MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2990399006.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat108109624.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat108109624MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen573379950.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558385.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558385MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558386.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558386MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558387.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1147558387MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828713.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3194695850MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3156209119.h"
#include "mscorlib_System_InvalidCastException3625212209.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo4011360410.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo4011360410MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2268544833.h"
#include "mscorlib_System_Predicate_1_gen2268544833MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2715583837MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen2715583837.h"
#include "mscorlib_System_Comparison_1_gen792346273.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3702943073MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol224640337.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol224640337MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2776792056.h"
#include "mscorlib_System_Predicate_1_gen2776792056MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3223831060MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3223831060.h"
#include "mscorlib_System_Comparison_1_gen1300593496.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2257663140.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2257663140MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen514847563.h"
#include "mscorlib_System_Predicate_1_gen514847563MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen961886567MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen961886567.h"
#include "mscorlib_System_Comparison_1_gen3333616299.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo2875234987MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1132419410.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1579458414.h"
#include "mscorlib_System_Comparison_1_gen3951188146.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3758245971MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol279943235.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol279943235MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2832094954.h"
#include "mscorlib_System_Predicate_1_gen2832094954MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3279133958MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3279133958.h"
#include "mscorlib_System_Comparison_1_gen1355896394.h"
#include "mscorlib_System_Collections_Generic_List_1_gen867319046MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1683983606.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1683983606MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen4236135325.h"
#include "mscorlib_System_Predicate_1_gen4236135325MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen388207033MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen388207033.h"
#include "mscorlib_System_Comparison_1_gen2759936765.h"

// System.Int32 System.Array::IndexOf<System.Boolean>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisBoolean_t3825574718_m313912672_gshared (Il2CppObject * __this /* static, unused */, BooleanU5BU5D_t3568034315* p0, bool p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisBoolean_t3825574718_m313912672(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, BooleanU5BU5D_t3568034315*, bool, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisBoolean_t3825574718_m313912672_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Boolean>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisBoolean_t3825574718_m3074508667_gshared (Il2CppObject * __this /* static, unused */, BooleanU5BU5D_t3568034315* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisBoolean_t3825574718_m3074508667(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, BooleanU5BU5D_t3568034315*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisBoolean_t3825574718_m3074508667_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Boolean>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C"  void Array_Sort_TisBoolean_t3825574718_m2555015387_gshared (Il2CppObject * __this /* static, unused */, BooleanU5BU5D_t3568034315* p0, int32_t p1, Comparison_1_t792346273 * p2, const MethodInfo* method);
#define Array_Sort_TisBoolean_t3825574718_m2555015387(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, BooleanU5BU5D_t3568034315*, int32_t, Comparison_1_t792346273 *, const MethodInfo*))Array_Sort_TisBoolean_t3825574718_m2555015387_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::Resize<System.Boolean>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisBoolean_t3825574718_m4023491748_gshared (Il2CppObject * __this /* static, unused */, BooleanU5BU5D_t3568034315** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisBoolean_t3825574718_m4023491748(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, BooleanU5BU5D_t3568034315**, int32_t, const MethodInfo*))Array_Resize_TisBoolean_t3825574718_m4023491748_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisKeyValuePair_2_t38854645_m1893623592_gshared (Il2CppObject * __this /* static, unused */, KeyValuePair_2U5BU5D_t2854920344* p0, KeyValuePair_2_t38854645  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisKeyValuePair_2_t38854645_m1893623592(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t2854920344*, KeyValuePair_2_t38854645 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisKeyValuePair_2_t38854645_m1893623592_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisKeyValuePair_2_t38854645_m3271520781_gshared (Il2CppObject * __this /* static, unused */, KeyValuePair_2U5BU5D_t2854920344* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisKeyValuePair_2_t38854645_m3271520781(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t2854920344*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisKeyValuePair_2_t38854645_m3271520781_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C"  void Array_Sort_TisKeyValuePair_2_t38854645_m533724509_gshared (Il2CppObject * __this /* static, unused */, KeyValuePair_2U5BU5D_t2854920344* p0, int32_t p1, Comparison_1_t1300593496 * p2, const MethodInfo* method);
#define Array_Sort_TisKeyValuePair_2_t38854645_m533724509(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t2854920344*, int32_t, Comparison_1_t1300593496 *, const MethodInfo*))Array_Sort_TisKeyValuePair_2_t38854645_m533724509_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::Resize<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisKeyValuePair_2_t38854645_m3783595720_gshared (Il2CppObject * __this /* static, unused */, KeyValuePair_2U5BU5D_t2854920344** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisKeyValuePair_2_t38854645_m3783595720(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t2854920344**, int32_t, const MethodInfo*))Array_Resize_TisKeyValuePair_2_t38854645_m3783595720_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Int32>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisInt32_t2071877448_m4287366004_gshared (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* p0, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisInt32_t2071877448_m4287366004(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3030399641*, int32_t, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisInt32_t2071877448_m4287366004_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Int32>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisInt32_t2071877448_m1860415737_gshared (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisInt32_t2071877448_m1860415737(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3030399641*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisInt32_t2071877448_m1860415737_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Int32>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C"  void Array_Sort_TisInt32_t2071877448_m186284849_gshared (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* p0, int32_t p1, Comparison_1_t3333616299 * p2, const MethodInfo* method);
#define Array_Sort_TisInt32_t2071877448_m186284849(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3030399641*, int32_t, Comparison_1_t3333616299 *, const MethodInfo*))Array_Sort_TisInt32_t2071877448_m186284849_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::Resize<System.Int32>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisInt32_t2071877448_m447637572_gshared (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisInt32_t2071877448_m447637572(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3030399641**, int32_t, const MethodInfo*))Array_Resize_TisInt32_t2071877448_m447637572_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m1815604637_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, Il2CppObject * p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m1815604637(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m1815604637_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisIl2CppObject_m1954851512_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisIl2CppObject_m1954851512(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisIl2CppObject_m1954851512_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C"  void Array_Sort_TisIl2CppObject_m3717288230_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, int32_t p1, Comparison_1_t3951188146 * p2, const MethodInfo* method);
#define Array_Sort_TisIl2CppObject_m3717288230(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, Comparison_1_t3951188146 *, const MethodInfo*))Array_Sort_TisIl2CppObject_m3717288230_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIl2CppObject_m4223007361_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisIl2CppObject_m4223007361(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134**, int32_t, const MethodInfo*))Array_Resize_TisIl2CppObject_m4223007361_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m2205974312_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486* p0, CustomAttributeNamedArgument_t94157543  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m2205974312(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m2205974312_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisCustomAttributeNamedArgument_t94157543_m2435281169_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeNamedArgument_t94157543_m2435281169(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisCustomAttributeNamedArgument_t94157543_m2435281169_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C"  void Array_Sort_TisCustomAttributeNamedArgument_t94157543_m3436077809_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486* p0, int32_t p1, Comparison_1_t1355896394 * p2, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeNamedArgument_t94157543_m3436077809(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, int32_t, Comparison_1_t1355896394 *, const MethodInfo*))Array_Sort_TisCustomAttributeNamedArgument_t94157543_m3436077809_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisCustomAttributeNamedArgument_t94157543_m3339240648_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeNamedArgument_t94157543_m3339240648(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486**, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeNamedArgument_t94157543_m3339240648_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m1984749829_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591* p0, CustomAttributeTypedArgument_t1498197914  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m1984749829(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m1984749829_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisCustomAttributeTypedArgument_t1498197914_m3745413134_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeTypedArgument_t1498197914_m3745413134(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisCustomAttributeTypedArgument_t1498197914_m3745413134_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C"  void Array_Sort_TisCustomAttributeTypedArgument_t1498197914_m1081752256_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591* p0, int32_t p1, Comparison_1_t2759936765 * p2, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeTypedArgument_t1498197914_m1081752256(__this /* static, unused */, p0, p1, p2, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, int32_t, Comparison_1_t2759936765 *, const MethodInfo*))Array_Sort_TisCustomAttributeTypedArgument_t1498197914_m1081752256_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisCustomAttributeTypedArgument_t1498197914_m939902121_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeTypedArgument_t1498197914_m939902121(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591**, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeTypedArgument_t1498197914_m939902121_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2070884156_gshared (KeyValuePair_2_t3823471070 * __this, int16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int16_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m4076108421((KeyValuePair_2_t3823471070 *)__this, (int16_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m4268247485((KeyValuePair_2_t3823471070 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2070884156_AdjustorThunk (Il2CppObject * __this, int16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3823471070 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3823471070 *>(__this + 1);
	KeyValuePair_2__ctor_m2070884156(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::get_Key()
extern "C"  int16_t KeyValuePair_2_get_Key_m2738192346_gshared (KeyValuePair_2_t3823471070 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = (int16_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int16_t KeyValuePair_2_get_Key_m2738192346_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3823471070 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3823471070 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2738192346(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4076108421_gshared (KeyValuePair_2_t3823471070 * __this, int16_t ___value0, const MethodInfo* method)
{
	{
		int16_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4076108421_AdjustorThunk (Il2CppObject * __this, int16_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3823471070 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3823471070 *>(__this + 1);
	KeyValuePair_2_set_Key_m4076108421(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m902390722_gshared (KeyValuePair_2_t3823471070 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m902390722_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3823471070 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3823471070 *>(__this + 1);
	return KeyValuePair_2_get_Value_m902390722(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4268247485_gshared (KeyValuePair_2_t3823471070 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m4268247485_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3823471070 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3823471070 *>(__this + 1);
	KeyValuePair_2_set_Value_m4268247485(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m878106415_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m878106415_gshared (KeyValuePair_2_t3823471070 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m878106415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int16_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int16_t L_2 = KeyValuePair_2_get_Key_m2738192346((KeyValuePair_2_t3823471070 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int16_t L_3 = KeyValuePair_2_get_Key_m2738192346((KeyValuePair_2_t3823471070 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int16_t)L_3;
		String_t* L_4 = Int16_ToString_m1247017262((int16_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m902390722((KeyValuePair_2_t3823471070 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m902390722((KeyValuePair_2_t3823471070 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m878106415_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3823471070 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3823471070 *>(__this + 1);
	return KeyValuePair_2_ToString_m878106415(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2177183229_gshared (KeyValuePair_2_t3132015601 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m2075879042((KeyValuePair_2_t3132015601 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m4270505802((KeyValuePair_2_t3132015601 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2177183229_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3132015601 *>(__this + 1);
	KeyValuePair_2__ctor_m2177183229(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1683812983_gshared (KeyValuePair_2_t3132015601 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1683812983_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3132015601 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1683812983(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2075879042_gshared (KeyValuePair_2_t3132015601 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2075879042_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3132015601 *>(__this + 1);
	KeyValuePair_2_set_Key_m2075879042(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1136571287_gshared (KeyValuePair_2_t3132015601 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m1136571287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3132015601 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1136571287(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4270505802_gshared (KeyValuePair_2_t3132015601 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m4270505802_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3132015601 *>(__this + 1);
	KeyValuePair_2_set_Value_m4270505802(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m910120950_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m910120950_gshared (KeyValuePair_2_t3132015601 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m910120950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1683812983((KeyValuePair_2_t3132015601 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1683812983((KeyValuePair_2_t3132015601 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m1136571287((KeyValuePair_2_t3132015601 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m1136571287((KeyValuePair_2_t3132015601 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m2960866144((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m910120950_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3132015601 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3132015601 *>(__this + 1);
	return KeyValuePair_2_ToString_m910120950(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3201181706_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1350990071((KeyValuePair_2_t3749587448 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m2726037047((KeyValuePair_2_t3749587448 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3201181706_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2__ctor_m3201181706(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1435832840_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1435832840_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1435832840(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1350990071_gshared (KeyValuePair_2_t3749587448 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1350990071_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2_set_Key_m1350990071(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3690000728_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3690000728_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3690000728(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2726037047_gshared (KeyValuePair_2_t3749587448 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2726037047_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	KeyValuePair_2_set_Value_m2726037047(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1391611625_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_gshared (KeyValuePair_2_t3749587448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1391611625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1435832840((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m3690000728((KeyValuePair_2_t3749587448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1391611625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3749587448 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3749587448 *>(__this + 1);
	return KeyValuePair_2_ToString_m1391611625(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1870434531_gshared (KeyValuePair_2_t1008373517 * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m3756867396((KeyValuePair_2_t1008373517 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Label_t4243202660  L_1 = ___value1;
		KeyValuePair_2_set_Value_m4153446916((KeyValuePair_2_t1008373517 *)__this, (Label_t4243202660 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1870434531_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Label_t4243202660  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1008373517 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1008373517 *>(__this + 1);
	KeyValuePair_2__ctor_m1870434531(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2216667373_gshared (KeyValuePair_2_t1008373517 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2216667373_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1008373517 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1008373517 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2216667373(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3756867396_gshared (KeyValuePair_2_t1008373517 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3756867396_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1008373517 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1008373517 *>(__this + 1);
	KeyValuePair_2_set_Key_m3756867396(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>::get_Value()
extern "C"  Label_t4243202660  KeyValuePair_2_get_Value_m590593229_gshared (KeyValuePair_2_t1008373517 * __this, const MethodInfo* method)
{
	{
		Label_t4243202660  L_0 = (Label_t4243202660 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  Label_t4243202660  KeyValuePair_2_get_Value_m590593229_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1008373517 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1008373517 *>(__this + 1);
	return KeyValuePair_2_get_Value_m590593229(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4153446916_gshared (KeyValuePair_2_t1008373517 * __this, Label_t4243202660  ___value0, const MethodInfo* method)
{
	{
		Label_t4243202660  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m4153446916_AdjustorThunk (Il2CppObject * __this, Label_t4243202660  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1008373517 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1008373517 *>(__this + 1);
	KeyValuePair_2_set_Value_m4153446916(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1190571640_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1190571640_gshared (KeyValuePair_2_t1008373517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1190571640_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Label_t4243202660  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m2216667373((KeyValuePair_2_t1008373517 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m2216667373((KeyValuePair_2_t1008373517 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Label_t4243202660  L_8 = KeyValuePair_2_get_Value_m590593229((KeyValuePair_2_t1008373517 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		Label_t4243202660  L_9 = KeyValuePair_2_get_Value_m590593229((KeyValuePair_2_t1008373517 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Label_t4243202660 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1190571640_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1008373517 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1008373517 *>(__this + 1);
	return KeyValuePair_2_ToString_m1190571640(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2381829160_gshared (KeyValuePair_2_t973022757 * __this, int32_t ___key0, ConnectionPendingPlayers_t4207851900  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m2060752417((KeyValuePair_2_t973022757 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ConnectionPendingPlayers_t4207851900  L_1 = ___value1;
		KeyValuePair_2_set_Value_m2955046537((KeyValuePair_2_t973022757 *)__this, (ConnectionPendingPlayers_t4207851900 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2381829160_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, ConnectionPendingPlayers_t4207851900  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t973022757 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t973022757 *>(__this + 1);
	KeyValuePair_2__ctor_m2381829160(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2308175658_gshared (KeyValuePair_2_t973022757 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2308175658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t973022757 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t973022757 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2308175658(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2060752417_gshared (KeyValuePair_2_t973022757 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2060752417_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t973022757 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t973022757 *>(__this + 1);
	KeyValuePair_2_set_Key_m2060752417(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::get_Value()
extern "C"  ConnectionPendingPlayers_t4207851900  KeyValuePair_2_get_Value_m2013503202_gshared (KeyValuePair_2_t973022757 * __this, const MethodInfo* method)
{
	{
		ConnectionPendingPlayers_t4207851900  L_0 = (ConnectionPendingPlayers_t4207851900 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  ConnectionPendingPlayers_t4207851900  KeyValuePair_2_get_Value_m2013503202_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t973022757 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t973022757 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2013503202(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2955046537_gshared (KeyValuePair_2_t973022757 * __this, ConnectionPendingPlayers_t4207851900  ___value0, const MethodInfo* method)
{
	{
		ConnectionPendingPlayers_t4207851900  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2955046537_AdjustorThunk (Il2CppObject * __this, ConnectionPendingPlayers_t4207851900  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t973022757 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t973022757 *>(__this + 1);
	KeyValuePair_2_set_Value_m2955046537(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m2029980823_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2029980823_gshared (KeyValuePair_2_t973022757 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2029980823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ConnectionPendingPlayers_t4207851900  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m2308175658((KeyValuePair_2_t973022757 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m2308175658((KeyValuePair_2_t973022757 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m2960866144((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		ConnectionPendingPlayers_t4207851900  L_8 = KeyValuePair_2_get_Value_m2013503202((KeyValuePair_2_t973022757 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		ConnectionPendingPlayers_t4207851900  L_9 = KeyValuePair_2_get_Value_m2013503202((KeyValuePair_2_t973022757 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (ConnectionPendingPlayers_t4207851900 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2029980823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t973022757 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t973022757 *>(__this + 1);
	return KeyValuePair_2_ToString_m2029980823(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4040336782_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1222844869((KeyValuePair_2_t1174980068 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		KeyValuePair_2_set_Value_m965533293((KeyValuePair_2_t1174980068 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4040336782_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2__ctor_m4040336782(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2113318928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2113318928(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1222844869_gshared (KeyValuePair_2_t1174980068 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1222844869_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2_set_Key_m1222844869(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m1916631176_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1916631176(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m965533293_gshared (KeyValuePair_2_t1174980068 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m965533293_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	KeyValuePair_2_set_Value_m965533293(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1739958171_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_gshared (KeyValuePair_2_t1174980068 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1739958171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2113318928((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		bool L_8 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = KeyValuePair_2_get_Value_m1916631176((KeyValuePair_2_t1174980068 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m1253164328((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1739958171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1174980068 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1174980068 *>(__this + 1);
	return KeyValuePair_2_ToString_m1739958171(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m400583398_gshared (KeyValuePair_2_t1683227291 * __this, Il2CppObject * ___key0, KeyValuePair_2_t38854645  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m3257200763((KeyValuePair_2_t1683227291 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645  L_1 = ___value1;
		KeyValuePair_2_set_Value_m815956739((KeyValuePair_2_t1683227291 *)__this, (KeyValuePair_2_t38854645 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m400583398_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, KeyValuePair_2_t38854645  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	KeyValuePair_2__ctor_m400583398(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4247498472_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4247498472_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4247498472(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3257200763_gshared (KeyValuePair_2_t1683227291 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3257200763_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	KeyValuePair_2_set_Key_m3257200763(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C"  KeyValuePair_2_t38854645  KeyValuePair_2_get_Value_m667219360_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t38854645  KeyValuePair_2_get_Value_m667219360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	return KeyValuePair_2_get_Value_m667219360(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m815956739_gshared (KeyValuePair_2_t1683227291 * __this, KeyValuePair_2_t38854645  ___value0, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m815956739_AdjustorThunk (Il2CppObject * __this, KeyValuePair_2_t38854645  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	KeyValuePair_2_set_Value_m815956739(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m4177387161_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m4177387161_gshared (KeyValuePair_2_t1683227291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4177387161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	KeyValuePair_2_t38854645  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m4247498472((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		KeyValuePair_2_t38854645  L_8 = KeyValuePair_2_get_Value_m667219360((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		KeyValuePair_2_t38854645  L_9 = KeyValuePair_2_get_Value_m667219360((KeyValuePair_2_t1683227291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (KeyValuePair_2_t38854645 )L_9;
		Il2CppClass* il2cpp_this_typeinfo_104 = IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5);
		String_t* L_10 = ((  String_t* (*) (Il2CppObject *, const MethodInfo*))il2cpp_this_typeinfo_104->vtable[3].methodPtr)((Il2CppObject *)il2cpp_codegen_fake_box((&V_1)), /*hidden argument*/il2cpp_this_typeinfo_104->vtable[3].method);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4177387161_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1683227291 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1683227291 *>(__this + 1);
	return KeyValuePair_2_ToString_m4177387161(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1877755778_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m1307112735((KeyValuePair_2_t3716250094 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m1921288671((KeyValuePair_2_t3716250094 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1877755778_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2__ctor_m1877755778(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1454531804_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1454531804(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1307112735_gshared (KeyValuePair_2_t3716250094 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1307112735_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2_set_Key_m1307112735(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3699669100_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3699669100(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1921288671_gshared (KeyValuePair_2_t3716250094 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1921288671_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	KeyValuePair_2_set_Value_m1921288671(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1394661909_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_gshared (KeyValuePair_2_t3716250094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1394661909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1454531804((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m3699669100((KeyValuePair_2_t3716250094 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m2960866144((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1394661909_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3716250094 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3716250094 *>(__this + 1);
	return KeyValuePair_2_ToString_m1394661909(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2353880237_gshared (KeyValuePair_2_t2553450683 * __this, Il2CppObject * ___key0, int64_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2592036086((KeyValuePair_2_t2553450683 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int64_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m99955926((KeyValuePair_2_t2553450683 *)__this, (int64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2353880237_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int64_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	KeyValuePair_2__ctor_m2353880237(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m289678647_gshared (KeyValuePair_2_t2553450683 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m289678647_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	return KeyValuePair_2_get_Key_m289678647(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2592036086_gshared (KeyValuePair_2_t2553450683 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2592036086_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	KeyValuePair_2_set_Key_m2592036086(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Value()
extern "C"  int64_t KeyValuePair_2_get_Value_m2459084999_gshared (KeyValuePair_2_t2553450683 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (int64_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int64_t KeyValuePair_2_get_Value_m2459084999_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2459084999(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m99955926_gshared (KeyValuePair_2_t2553450683 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m99955926_AdjustorThunk (Il2CppObject * __this, int64_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	KeyValuePair_2_set_Value_m99955926(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m2938889728_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2938889728_gshared (KeyValuePair_2_t2553450683 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2938889728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int64_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m289678647((KeyValuePair_2_t2553450683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m289678647((KeyValuePair_2_t2553450683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		int64_t L_8 = KeyValuePair_2_get_Value_m2459084999((KeyValuePair_2_t2553450683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int64_t L_9 = KeyValuePair_2_get_Value_m2459084999((KeyValuePair_2_t2553450683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int64_t)L_9;
		String_t* L_10 = Int64_ToString_m689375889((int64_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2938889728_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2553450683 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2553450683 *>(__this + 1);
	return KeyValuePair_2_ToString_m2938889728(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1640124561_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m744486900((KeyValuePair_2_t38854645 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1416408204((KeyValuePair_2_t38854645 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1640124561_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2__ctor_m1640124561(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2561166459_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2561166459_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2561166459(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m744486900_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m744486900_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2_set_Key_m744486900(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m499643803_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m499643803_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_get_Value_m499643803(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1416408204_gshared (KeyValuePair_2_t38854645 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1416408204_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	KeyValuePair_2_set_Value_m1416408204(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m2613351884_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2613351884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2561166459((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m499643803((KeyValuePair_2_t38854645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2613351884_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t38854645 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t38854645 *>(__this + 1);
	return KeyValuePair_2_ToString_m2613351884(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3420001604_gshared (KeyValuePair_2_t2290690628 * __this, Il2CppObject * ___key0, NetworkBroadcastResult_t646317982  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2740226929((KeyValuePair_2_t2290690628 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NetworkBroadcastResult_t646317982  L_1 = ___value1;
		KeyValuePair_2_set_Value_m3010936665((KeyValuePair_2_t2290690628 *)__this, (NetworkBroadcastResult_t646317982 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3420001604_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, NetworkBroadcastResult_t646317982  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2290690628 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2290690628 *>(__this + 1);
	KeyValuePair_2__ctor_m3420001604(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2967481714_gshared (KeyValuePair_2_t2290690628 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2967481714_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2290690628 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2290690628 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2967481714(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2740226929_gshared (KeyValuePair_2_t2290690628 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2740226929_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2290690628 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2290690628 *>(__this + 1);
	KeyValuePair_2_set_Key_m2740226929(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::get_Value()
extern "C"  NetworkBroadcastResult_t646317982  KeyValuePair_2_get_Value_m838596530_gshared (KeyValuePair_2_t2290690628 * __this, const MethodInfo* method)
{
	{
		NetworkBroadcastResult_t646317982  L_0 = (NetworkBroadcastResult_t646317982 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  NetworkBroadcastResult_t646317982  KeyValuePair_2_get_Value_m838596530_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2290690628 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2290690628 *>(__this + 1);
	return KeyValuePair_2_get_Value_m838596530(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3010936665_gshared (KeyValuePair_2_t2290690628 * __this, NetworkBroadcastResult_t646317982  ___value0, const MethodInfo* method)
{
	{
		NetworkBroadcastResult_t646317982  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3010936665_AdjustorThunk (Il2CppObject * __this, NetworkBroadcastResult_t646317982  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2290690628 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2290690628 *>(__this + 1);
	KeyValuePair_2_set_Value_m3010936665(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.Networking.NetworkBroadcastResult>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m2156236655_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2156236655_gshared (KeyValuePair_2_t2290690628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2156236655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	NetworkBroadcastResult_t646317982  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2967481714((KeyValuePair_2_t2290690628 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2967481714((KeyValuePair_2_t2290690628 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		NetworkBroadcastResult_t646317982  L_8 = KeyValuePair_2_get_Value_m838596530((KeyValuePair_2_t2290690628 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		NetworkBroadcastResult_t646317982  L_9 = KeyValuePair_2_get_Value_m838596530((KeyValuePair_2_t2290690628 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (NetworkBroadcastResult_t646317982 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2156236655_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2290690628 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2290690628 *>(__this + 1);
	return KeyValuePair_2_ToString_m2156236655(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3870834457_gshared (KeyValuePair_2_t488203048 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2339804284((KeyValuePair_2_t488203048 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m2019724268((KeyValuePair_2_t488203048 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3870834457_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	KeyValuePair_2__ctor_m3870834457(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m573362703_gshared (KeyValuePair_2_t488203048 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m573362703_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	return KeyValuePair_2_get_Key_m573362703(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2339804284_gshared (KeyValuePair_2_t488203048 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2339804284_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	KeyValuePair_2_set_Key_m2339804284(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1644876463_gshared (KeyValuePair_2_t488203048 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m1644876463_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1644876463(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2019724268_gshared (KeyValuePair_2_t488203048 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2019724268_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	KeyValuePair_2_set_Value_m2019724268(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m4238196864_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m4238196864_gshared (KeyValuePair_2_t488203048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4238196864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m573362703((KeyValuePair_2_t488203048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m1644876463((KeyValuePair_2_t488203048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m1644876463((KeyValuePair_2_t488203048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4238196864_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t488203048 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t488203048 *>(__this + 1);
	return KeyValuePair_2_ToString_m4238196864(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4239967155_gshared (KeyValuePair_2_t3479205885 * __this, NetworkHash128_t835211239  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		NetworkHash128_t835211239  L_0 = ___key0;
		KeyValuePair_2_set_Key_m833328204((KeyValuePair_2_t3479205885 *)__this, (NetworkHash128_t835211239 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m443154412((KeyValuePair_2_t3479205885 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4239967155_AdjustorThunk (Il2CppObject * __this, NetworkHash128_t835211239  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3479205885 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3479205885 *>(__this + 1);
	KeyValuePair_2__ctor_m4239967155(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::get_Key()
extern "C"  NetworkHash128_t835211239  KeyValuePair_2_get_Key_m857379697_gshared (KeyValuePair_2_t3479205885 * __this, const MethodInfo* method)
{
	{
		NetworkHash128_t835211239  L_0 = (NetworkHash128_t835211239 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  NetworkHash128_t835211239  KeyValuePair_2_get_Key_m857379697_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3479205885 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3479205885 *>(__this + 1);
	return KeyValuePair_2_get_Key_m857379697(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m833328204_gshared (KeyValuePair_2_t3479205885 * __this, NetworkHash128_t835211239  ___value0, const MethodInfo* method)
{
	{
		NetworkHash128_t835211239  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m833328204_AdjustorThunk (Il2CppObject * __this, NetworkHash128_t835211239  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3479205885 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3479205885 *>(__this + 1);
	KeyValuePair_2_set_Key_m833328204(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3149317201_gshared (KeyValuePair_2_t3479205885 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3149317201_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3479205885 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3479205885 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3149317201(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m443154412_gshared (KeyValuePair_2_t3479205885 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m443154412_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3479205885 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3479205885 *>(__this + 1);
	KeyValuePair_2_set_Value_m443154412(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkHash128,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1004546060_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1004546060_gshared (KeyValuePair_2_t3479205885 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1004546060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NetworkHash128_t835211239  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		NetworkHash128_t835211239  L_2 = KeyValuePair_2_get_Key_m857379697((KeyValuePair_2_t3479205885 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		NetworkHash128_t835211239  L_3 = KeyValuePair_2_get_Key_m857379697((KeyValuePair_2_t3479205885 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (NetworkHash128_t835211239 )L_3;
		String_t* L_4 = NetworkHash128_ToString_m506494205((NetworkHash128_t835211239 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m3149317201((KeyValuePair_2_t3479205885 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m3149317201((KeyValuePair_2_t3479205885 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1004546060_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3479205885 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3479205885 *>(__this + 1);
	return KeyValuePair_2_ToString_m1004546060(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m294348854_gshared (KeyValuePair_2_t2979060144 * __this, NetworkInstanceId_t33998832  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		NetworkInstanceId_t33998832  L_0 = ___key0;
		KeyValuePair_2_set_Key_m3463227267((KeyValuePair_2_t2979060144 *)__this, (NetworkInstanceId_t33998832 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1789552059((KeyValuePair_2_t2979060144 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m294348854_AdjustorThunk (Il2CppObject * __this, NetworkInstanceId_t33998832  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2979060144 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2979060144 *>(__this + 1);
	KeyValuePair_2__ctor_m294348854(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::get_Key()
extern "C"  NetworkInstanceId_t33998832  KeyValuePair_2_get_Key_m2311441456_gshared (KeyValuePair_2_t2979060144 * __this, const MethodInfo* method)
{
	{
		NetworkInstanceId_t33998832  L_0 = (NetworkInstanceId_t33998832 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  NetworkInstanceId_t33998832  KeyValuePair_2_get_Key_m2311441456_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2979060144 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2979060144 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2311441456(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3463227267_gshared (KeyValuePair_2_t2979060144 * __this, NetworkInstanceId_t33998832  ___value0, const MethodInfo* method)
{
	{
		NetworkInstanceId_t33998832  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3463227267_AdjustorThunk (Il2CppObject * __this, NetworkInstanceId_t33998832  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2979060144 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2979060144 *>(__this + 1);
	KeyValuePair_2_set_Key_m3463227267(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1496119512_gshared (KeyValuePair_2_t2979060144 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1496119512_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2979060144 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2979060144 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1496119512(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1789552059_gshared (KeyValuePair_2_t2979060144 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1789552059_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2979060144 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2979060144 *>(__this + 1);
	KeyValuePair_2_set_Value_m1789552059(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m147270409_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m147270409_gshared (KeyValuePair_2_t2979060144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m147270409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NetworkInstanceId_t33998832  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		NetworkInstanceId_t33998832  L_2 = KeyValuePair_2_get_Key_m2311441456((KeyValuePair_2_t2979060144 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		NetworkInstanceId_t33998832  L_3 = KeyValuePair_2_get_Key_m2311441456((KeyValuePair_2_t2979060144 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (NetworkInstanceId_t33998832 )L_3;
		String_t* L_4 = NetworkInstanceId_ToString_m3480450226((NetworkInstanceId_t33998832 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m1496119512((KeyValuePair_2_t2979060144 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m1496119512((KeyValuePair_2_t2979060144 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m147270409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2979060144 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2979060144 *>(__this + 1);
	return KeyValuePair_2_ToString_m147270409(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3453855065_gshared (KeyValuePair_2_t1197192881 * __this, NetworkSceneId_t2931030083  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		NetworkSceneId_t2931030083  L_0 = ___key0;
		KeyValuePair_2_set_Key_m3856356940((KeyValuePair_2_t1197192881 *)__this, (NetworkSceneId_t2931030083 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m4003642012((KeyValuePair_2_t1197192881 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3453855065_AdjustorThunk (Il2CppObject * __this, NetworkSceneId_t2931030083  ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1197192881 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1197192881 *>(__this + 1);
	KeyValuePair_2__ctor_m3453855065(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,System.Object>::get_Key()
extern "C"  NetworkSceneId_t2931030083  KeyValuePair_2_get_Key_m75033871_gshared (KeyValuePair_2_t1197192881 * __this, const MethodInfo* method)
{
	{
		NetworkSceneId_t2931030083  L_0 = (NetworkSceneId_t2931030083 )__this->get_key_0();
		return L_0;
	}
}
extern "C"  NetworkSceneId_t2931030083  KeyValuePair_2_get_Key_m75033871_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1197192881 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1197192881 *>(__this + 1);
	return KeyValuePair_2_get_Key_m75033871(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3856356940_gshared (KeyValuePair_2_t1197192881 * __this, NetworkSceneId_t2931030083  ___value0, const MethodInfo* method)
{
	{
		NetworkSceneId_t2931030083  L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3856356940_AdjustorThunk (Il2CppObject * __this, NetworkSceneId_t2931030083  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1197192881 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1197192881 *>(__this + 1);
	KeyValuePair_2_set_Key_m3856356940(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m438770735_gshared (KeyValuePair_2_t1197192881 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m438770735_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1197192881 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1197192881 *>(__this + 1);
	return KeyValuePair_2_get_Value_m438770735(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4003642012_gshared (KeyValuePair_2_t1197192881 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m4003642012_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1197192881 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1197192881 *>(__this + 1);
	KeyValuePair_2_set_Value_m4003642012(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkSceneId,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m334347088_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m334347088_gshared (KeyValuePair_2_t1197192881 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m334347088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NetworkSceneId_t2931030083  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		NetworkSceneId_t2931030083  L_2 = KeyValuePair_2_get_Key_m75033871((KeyValuePair_2_t1197192881 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		NetworkSceneId_t2931030083  L_3 = KeyValuePair_2_get_Key_m75033871((KeyValuePair_2_t1197192881 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (NetworkSceneId_t2931030083 )L_3;
		String_t* L_4 = NetworkSceneId_ToString_m3473187247((NetworkSceneId_t2931030083 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral811305474);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m438770735((KeyValuePair_2_t1197192881 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m438770735((KeyValuePair_2_t1197192881 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_12 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029425);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m334347088_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1197192881 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1197192881 *>(__this + 1);
	return KeyValuePair_2_ToString_m334347088(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1665737714_gshared (KeyValuePair_2_t3709506243 * __this, uint64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m62205575((KeyValuePair_2_t3709506243 *)__this, (uint64_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m223828967((KeyValuePair_2_t3709506243 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1665737714_AdjustorThunk (Il2CppObject * __this, uint64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3709506243 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3709506243 *>(__this + 1);
	KeyValuePair_2__ctor_m1665737714(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Key()
extern "C"  uint64_t KeyValuePair_2_get_Key_m3078650232_gshared (KeyValuePair_2_t3709506243 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (uint64_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  uint64_t KeyValuePair_2_get_Key_m3078650232_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3709506243 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3709506243 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3078650232(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m62205575_gshared (KeyValuePair_2_t3709506243 * __this, uint64_t ___value0, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m62205575_AdjustorThunk (Il2CppObject * __this, uint64_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3709506243 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3709506243 *>(__this + 1);
	KeyValuePair_2_set_Key_m62205575(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m549329768_gshared (KeyValuePair_2_t3709506243 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m549329768_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3709506243 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3709506243 *>(__this + 1);
	return KeyValuePair_2_get_Value_m549329768(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m223828967_gshared (KeyValuePair_2_t3709506243 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m223828967_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3709506243 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3709506243 *>(__this + 1);
	KeyValuePair_2_set_Value_m223828967(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t KeyValuePair_2_ToString_m1791447377_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1791447377_gshared (KeyValuePair_2_t3709506243 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1791447377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint64_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1642385972* G_B2_1 = NULL;
	StringU5BU5D_t1642385972* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1642385972* G_B1_1 = NULL;
	StringU5BU5D_t1642385972* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1642385972* G_B3_2 = NULL;
	StringU5BU5D_t1642385972* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1642385972* G_B5_1 = NULL;
	StringU5BU5D_t1642385972* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1642385972* G_B4_1 = NULL;
	StringU5BU5D_t1642385972* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1642385972* G_B6_2 = NULL;
	StringU5BU5D_t1642385972* G_B6_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral372029431);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029431);
		StringU5BU5D_t1642385972* L_1 = (StringU5BU5D_t1642385972*)L_0;
		uint64_t L_2 = KeyValuePair_2_get_Key_m3078650232((KeyValuePair_2_t3709506243 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		uint64_t L_3 = KeyValuePair_2_get_Key_m3078650232((KeyValuePair_2_t3709506243 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (uint64_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)G_B3_3;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral811305474);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_8 = (StringU5BU5D_t1642385972*)L_7;
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m549329768((KeyValuePair_2_t3709506243 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = KeyValuePair_2_get_Value_m549329768((KeyValuePair_2_t3709506243 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1642385972* L_13 = (StringU5BU5D_t1642385972*)G_B6_3;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029425);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029425);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1791447377_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3709506243 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3709506243 *>(__this + 1);
	return KeyValuePair_2_ToString_m1791447377(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1178974130;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern const uint32_t Enumerator__ctor_m1529567823_MetadataUsageId;
extern "C"  void Enumerator__ctor_m1529567823_gshared (Enumerator_t1817063546 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator__ctor_m1529567823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		__this->set_si_4(L_0);
		SerializationInfo_t228987430 * L_1 = (SerializationInfo_t228987430 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m1127314592((SerializationInfo_t228987430 *)L_1, (String_t*)_stringLiteral1178974130, (Type_t *)L_2, /*hidden argument*/NULL);
		__this->set_list_0(((LinkedList_1_t2994157524 *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		SerializationInfo_t228987430 * L_4 = (SerializationInfo_t228987430 *)__this->get_si_4();
		NullCheck((SerializationInfo_t228987430 *)L_4);
		int32_t L_5 = SerializationInfo_GetInt32_m4039439501((SerializationInfo_t228987430 *)L_4, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		__this->set_index_2(L_5);
		SerializationInfo_t228987430 * L_6 = (SerializationInfo_t228987430 *)__this->get_si_4();
		NullCheck((SerializationInfo_t228987430 *)L_6);
		uint32_t L_7 = SerializationInfo_GetUInt32_m3393505633((SerializationInfo_t228987430 *)L_6, (String_t*)_stringLiteral3617362, /*hidden argument*/NULL);
		__this->set_version_3(L_7);
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1529567823_AdjustorThunk (Il2CppObject * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator__ctor_m1529567823(_thisAdjusted, ___info0, ___context1, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m1586864815_gshared (Enumerator_t1817063546 * __this, LinkedList_1_t2994157524 * ___parent0, const MethodInfo* method)
{
	{
		__this->set_si_4((SerializationInfo_t228987430 *)NULL);
		LinkedList_1_t2994157524 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t2994157524 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1586864815_AdjustorThunk (Il2CppObject * __this, LinkedList_1_t2994157524 * ___parent0, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator__ctor_m1586864815(_thisAdjusted, ___parent0, method);
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3175039148_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_Current_m3158498407((Enumerator_t1817063546 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3175039148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3175039148(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1112410187;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m1061591080_MetadataUsageId;
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1061591080_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m1061591080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2994157524 * L_3 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral1112410187, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1061591080_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1061591080(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3579581747_MetadataUsageId;
extern "C"  void Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3579581747_gshared (Enumerator_t1817063546 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3579581747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		SerializationInfo_t228987430 * L_2 = ___info0;
		uint32_t L_3 = (uint32_t)__this->get_version_3();
		NullCheck((SerializationInfo_t228987430 *)L_2);
		SerializationInfo_AddValue_m383491877((SerializationInfo_t228987430 *)L_2, (String_t*)_stringLiteral3617362, (uint32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_4 = ___info0;
		int32_t L_5 = (int32_t)__this->get_index_2();
		NullCheck((SerializationInfo_t228987430 *)L_4);
		SerializationInfo_AddValue_m902275108((SerializationInfo_t228987430 *)L_4, (String_t*)_stringLiteral1460639766, (int32_t)L_5, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3579581747_AdjustorThunk (Il2CppObject * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3579581747(_thisAdjusted, ___info0, ___context1, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern Il2CppClass* IDeserializationCallback_t327125377_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2669336772_MetadataUsageId;
extern "C"  void Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2669336772_gshared (Enumerator_t1817063546 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2669336772_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		SerializationInfo_t228987430 * L_0 = (SerializationInfo_t228987430 *)__this->get_si_4();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		LinkedList_1_t2994157524 * L_1 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_1);
		SerializationInfo_t228987430 * L_2 = (SerializationInfo_t228987430 *)L_1->get_si_4();
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		LinkedList_1_t2994157524 * L_3 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		Enumerator_t1817063546  L_4 = (*(Enumerator_t1817063546 *)__this);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_4);
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(0 /* System.Void System.Runtime.Serialization.IDeserializationCallback::OnDeserialization(System.Object) */, IDeserializationCallback_t327125377_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
	}

IL_0032:
	{
		__this->set_si_4((SerializationInfo_t228987430 *)NULL);
		uint32_t L_6 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2994157524 * L_7 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_7);
		uint32_t L_8 = (uint32_t)L_7->get_version_1();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_index_2();
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		LinkedList_1_t2994157524 * L_10 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck((LinkedList_1_t2994157524 *)L_10);
		LinkedListNode_1_t1585555208 * L_11 = ((  LinkedListNode_1_t1585555208 * (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)((LinkedList_1_t2994157524 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (LinkedListNode_1_t1585555208 *)L_11;
		V_1 = (int32_t)0;
		goto IL_0079;
	}

IL_006e:
	{
		LinkedListNode_1_t1585555208 * L_12 = V_0;
		NullCheck(L_12);
		LinkedListNode_1_t1585555208 * L_13 = (LinkedListNode_1_t1585555208 *)L_12->get_forward_2();
		V_0 = (LinkedListNode_1_t1585555208 *)L_13;
		int32_t L_14 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0079:
	{
		int32_t L_15 = V_1;
		int32_t L_16 = (int32_t)__this->get_index_2();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_006e;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_17 = V_0;
		__this->set_current_1(L_17);
	}

IL_008c:
	{
		return;
	}
}
extern "C"  void Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2669336772_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m2669336772(_thisAdjusted, ___sender0, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_get_Current_m3158498407_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_get_Current_m3158498407_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m3158498407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t1585555208 * L_2 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t1585555208 * L_4 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t1585555208 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t1585555208 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3158498407_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	return Enumerator_get_Current_m3158498407(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1112410187;
extern const uint32_t Enumerator_MoveNext_m1957727328_MetadataUsageId;
extern "C"  bool Enumerator_MoveNext_m1957727328_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m1957727328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2994157524 * L_3 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t721527559 * L_5 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_5, (String_t*)_stringLiteral1112410187, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t2994157524 * L_7 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t1585555208 * L_8 = (LinkedListNode_1_t1585555208 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t1585555208 * L_9 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t1585555208 * L_10 = (LinkedListNode_1_t1585555208 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t1585555208 * L_11 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		LinkedList_1_t2994157524 * L_12 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t1585555208 * L_13 = (LinkedListNode_1_t1585555208 *)L_12->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_11) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t1585555208 * L_14 = (LinkedListNode_1_t1585555208 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
extern "C"  bool Enumerator_MoveNext_m1957727328_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	return Enumerator_MoveNext_m1957727328(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_Dispose_m3217456423_MetadataUsageId;
extern "C"  void Enumerator_Dispose_m3217456423_gshared (Enumerator_t1817063546 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m3217456423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t2695136451 * L_1 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t1585555208 *)NULL);
		__this->set_list_0((LinkedList_1_t2994157524 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3217456423_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1817063546 * _thisAdjusted = reinterpret_cast<Enumerator_t1817063546 *>(__this + 1);
	Enumerator_Dispose_m3217456423(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m1337573311_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m1337573311_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m1337573311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t1585555208 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m1570295574_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m1570295574_gshared (LinkedList_1_t2994157524 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m1570295574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		SerializationInfo_t228987430 * L_0 = ___info0;
		__this->set_si_4(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3874300024_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  LinkedListNode_1_t1585555208 * (*) (LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m3851520323_MetadataUsageId;
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m3851520323_gshared (LinkedList_1_t2994157524 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m3851520323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2994157524 *)__this, (ObjectU5BU5D_t3614634134*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4026686671_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		Enumerator_t1817063546  L_0 = ((  Enumerator_t1817063546  (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1817063546  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m1585231526_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		Enumerator_t1817063546  L_0 = ((  Enumerator_t1817063546  (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t1817063546  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m382194114_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m2849171683_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m550341923_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1414245146;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m1333791742_MetadataUsageId;
extern "C"  void LinkedList_1_VerifyReferencedNode_m1333791742_gshared (LinkedList_1_t2994157524 * __this, LinkedListNode_1_t1585555208 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m1333791742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedListNode_1_t1585555208 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1414245146, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t1585555208 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_2);
		LinkedList_1_t2994157524 * L_3 = ((  LinkedList_1_t2994157524 * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t1585555208 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((Il2CppObject*)(LinkedList_1_t2994157524 *)L_3) == ((Il2CppObject*)(LinkedList_1_t2994157524 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t721527559 * L_4 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C"  LinkedListNode_1_t1585555208 * LinkedList_1_AddLast_m3575571360_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		LinkedListNode_1_t1585555208 * L_2 = (LinkedListNode_1_t1585555208 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t1585555208 *, LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t1585555208 *)L_2;
		LinkedListNode_1_t1585555208 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		Il2CppObject * L_4 = ___value0;
		LinkedListNode_1_t1585555208 * L_5 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)L_5->get_back_3();
		LinkedListNode_1_t1585555208 * L_7 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		LinkedListNode_1_t1585555208 * L_8 = (LinkedListNode_1_t1585555208 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t1585555208 *, LinkedList_1_t2994157524 *, Il2CppObject *, LinkedListNode_1_t1585555208 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(L_8, (LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_4, (LinkedListNode_1_t1585555208 *)L_6, (LinkedListNode_1_t1585555208 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t1585555208 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t1585555208 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C"  void LinkedList_1_Clear_m3288132428_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m3220831510_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1585555208 *)L_0;
		LinkedListNode_1_t1585555208 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t1585555208 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t1585555208 * L_5 = V_0;
		NullCheck(L_5);
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)L_5->get_forward_2();
		V_0 = (LinkedListNode_1_t1585555208 *)L_6;
		LinkedListNode_1_t1585555208 * L_7 = V_0;
		LinkedListNode_1_t1585555208 * L_8 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_7) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_8))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern Il2CppCodeGenString* _stringLiteral374702583;
extern Il2CppCodeGenString* _stringLiteral2752284169;
extern const uint32_t LinkedList_1_CopyTo_m3700485924_MetadataUsageId;
extern "C"  void LinkedList_1_CopyTo_m3700485924_gshared (LinkedList_1_t2994157524 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m3700485924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		ObjectU5BU5D_t3614634134* L_3 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m3733237204((Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_5 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_5, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		ObjectU5BU5D_t3614634134* L_6 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Rank_m3837250695((Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t3259014390 * L_8 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_8, (String_t*)_stringLiteral1185213181, (String_t*)_stringLiteral374702583, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		ObjectU5BU5D_t3614634134* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		ObjectU5BU5D_t3614634134* L_11 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m3733237204((Il2CppArray *)(Il2CppArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t3259014390 * L_14 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_14, (String_t*)_stringLiteral2752284169, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t1585555208 * L_15 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1585555208 *)L_15;
		LinkedListNode_1_t1585555208 * L_16 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		ObjectU5BU5D_t3614634134* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t1585555208 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_19);
		Il2CppObject * L_20 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t1585555208 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t1585555208 * L_23 = (LinkedListNode_1_t1585555208 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t1585555208 *)L_23;
		LinkedListNode_1_t1585555208 * L_24 = V_0;
		LinkedListNode_1_t1585555208 * L_25 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_24) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C"  LinkedListNode_1_t1585555208 * LinkedList_1_Find_m1218372336_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t1585555208 *)L_0;
		LinkedListNode_1_t1585555208 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t1585555208 *)NULL;
	}

IL_000f:
	{
		Il2CppObject * L_2 = ___value0;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_4)
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		Il2CppObject * L_5 = ___value0;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t1585555208 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_7);
		if (!L_8)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t1585555208 * L_9 = V_0;
		return L_9;
	}

IL_0054:
	{
		LinkedListNode_1_t1585555208 * L_10 = V_0;
		NullCheck(L_10);
		LinkedListNode_1_t1585555208 * L_11 = (LinkedListNode_1_t1585555208 *)L_10->get_forward_2();
		V_0 = (LinkedListNode_1_t1585555208 *)L_11;
		LinkedListNode_1_t1585555208 * L_12 = V_0;
		LinkedListNode_1_t1585555208 * L_13 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_12) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_13))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t1585555208 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1817063546  LinkedList_1_GetEnumerator_m4181273888_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1817063546  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1586864815(&L_0, (LinkedList_1_t2994157524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1747804205;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern const uint32_t LinkedList_1_GetObjectData_m2848049047_MetadataUsageId;
extern "C"  void LinkedList_1_GetObjectData_m2848049047_gshared (LinkedList_1_t2994157524 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m2848049047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2994157524 *)__this, (ObjectU5BU5D_t3614634134*)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		SerializationInfo_t228987430 * L_2 = ___info0;
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_2);
		SerializationInfo_AddValue_m1781549036((SerializationInfo_t228987430 *)L_2, (String_t*)_stringLiteral1747804205, (Il2CppObject *)(Il2CppObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t228987430 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t228987430 *)L_5);
		SerializationInfo_AddValue_m383491877((SerializationInfo_t228987430 *)L_5, (String_t*)_stringLiteral3617362, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1747804205;
extern Il2CppCodeGenString* _stringLiteral3617362;
extern const uint32_t LinkedList_1_OnDeserialization_m264209791_MetadataUsageId;
extern "C"  void LinkedList_1_OnDeserialization_m264209791_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m264209791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t3614634134* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t228987430 * L_0 = (SerializationInfo_t228987430 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t228987430 * L_1 = (SerializationInfo_t228987430 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t228987430 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m1127314592((SerializationInfo_t228987430 *)L_1, (String_t*)_stringLiteral1747804205, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t3614634134* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_5 = V_0;
		V_2 = (ObjectU5BU5D_t3614634134*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		ObjectU5BU5D_t3614634134* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_1;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  LinkedListNode_1_t1585555208 * (*) (LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_3;
		ObjectU5BU5D_t3614634134* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t228987430 * L_14 = (SerializationInfo_t228987430 *)__this->get_si_4();
		NullCheck((SerializationInfo_t228987430 *)L_14);
		uint32_t L_15 = SerializationInfo_GetUInt32_m3393505633((SerializationInfo_t228987430 *)L_14, (String_t*)_stringLiteral3617362, /*hidden argument*/NULL);
		__this->set_version_1(L_15);
		__this->set_si_4((SerializationInfo_t228987430 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m2925241645_gshared (LinkedList_1_t2994157524 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		LinkedListNode_1_t1585555208 * L_1 = ((  LinkedListNode_1_t1585555208 * (*) (LinkedList_1_t2994157524 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((LinkedList_1_t2994157524 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t1585555208 *)L_1;
		LinkedListNode_1_t1585555208 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t1585555208 * L_3 = V_0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2994157524 *)__this, (LinkedListNode_1_t1585555208 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m18160224_gshared (LinkedList_1_t2994157524 * __this, LinkedListNode_1_t1585555208 * ___node0, const MethodInfo* method)
{
	{
		LinkedListNode_1_t1585555208 * L_0 = ___node0;
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((LinkedList_1_t2994157524 *)__this, (LinkedListNode_1_t1585555208 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t1585555208 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t1585555208 * L_3 = ___node0;
		LinkedListNode_1_t1585555208 * L_4 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_3) == ((Il2CppObject*)(LinkedListNode_1_t1585555208 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_5 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t1585555208 * L_6 = (LinkedListNode_1_t1585555208 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t1585555208 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t1585555208 *)L_8);
		((  void (*) (LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LinkedListNode_1_t1585555208 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m2904732739_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t1585555208 * L_1 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t1585555208 * L_2 = (LinkedListNode_1_t1585555208 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t2994157524 *)__this);
		((  void (*) (LinkedList_1_t2994157524 *, LinkedListNode_1_t1585555208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2994157524 *)__this, (LinkedListNode_1_t1585555208 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m402269195_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C"  LinkedListNode_1_t1585555208 * LinkedList_1_get_First_m732033480_gshared (LinkedList_1_t2994157524 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_first_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m231316848_gshared (LinkedListNode_1_t1585555208 * __this, LinkedList_1_t2994157524 * ___list0, Il2CppObject * ___value1, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2994157524 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t1585555208 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t1585555208 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m127450634_gshared (LinkedListNode_1_t1585555208 * __this, LinkedList_1_t2994157524 * ___list0, Il2CppObject * ___value1, LinkedListNode_1_t1585555208 * ___previousNode2, LinkedListNode_1_t1585555208 * ___nextNode3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2994157524 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t1585555208 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t1585555208 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t1585555208 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t1585555208 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C"  void LinkedListNode_1_Detach_m3110429670_gshared (LinkedListNode_1_t1585555208 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t1585555208 * V_0 = NULL;
	{
		LinkedListNode_1_t1585555208 * L_0 = (LinkedListNode_1_t1585555208 *)__this->get_back_3();
		LinkedListNode_1_t1585555208 * L_1 = (LinkedListNode_1_t1585555208 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t1585555208 * L_2 = (LinkedListNode_1_t1585555208 *)__this->get_forward_2();
		LinkedListNode_1_t1585555208 * L_3 = (LinkedListNode_1_t1585555208 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t1585555208 *)NULL;
		__this->set_back_3((LinkedListNode_1_t1585555208 *)NULL);
		LinkedListNode_1_t1585555208 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t2994157524 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C"  LinkedList_1_t2994157524 * LinkedListNode_1_get_List_m2861065156_gshared (LinkedListNode_1_t1585555208 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t2994157524 * L_0 = (LinkedList_1_t2994157524 *)__this->get_container_1();
		return L_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C"  Il2CppObject * LinkedListNode_1_get_Value_m201900116_gshared (LinkedListNode_1_t1585555208 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1827850134_gshared (Enumerator_t2729425524 * __this, List_1_t3194695850 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3194695850 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3194695850 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1827850134_AdjustorThunk (Il2CppObject * __this, List_1_t3194695850 * ___l0, const MethodInfo* method)
{
	Enumerator_t2729425524 * _thisAdjusted = reinterpret_cast<Enumerator_t2729425524 *>(__this + 1);
	Enumerator__ctor_m1827850134(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2206301276_gshared (Enumerator_t2729425524 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1105097172((Enumerator_t2729425524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2206301276_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2729425524 * _thisAdjusted = reinterpret_cast<Enumerator_t2729425524 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2206301276(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m769843674_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m769843674_gshared (Enumerator_t2729425524 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m769843674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1105097172((Enumerator_t2729425524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		bool L_2 = (bool)__this->get_current_3();
		bool L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m769843674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2729425524 * _thisAdjusted = reinterpret_cast<Enumerator_t2729425524 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m769843674(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::Dispose()
extern "C"  void Enumerator_Dispose_m1774507837_gshared (Enumerator_t2729425524 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3194695850 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1774507837_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2729425524 * _thisAdjusted = reinterpret_cast<Enumerator_t2729425524 *>(__this + 1);
	Enumerator_Dispose_m1774507837(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m1105097172_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1105097172_gshared (Enumerator_t2729425524 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1105097172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2729425524  L_1 = (*(Enumerator_t2729425524 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3194695850 * L_7 = (List_1_t3194695850 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1105097172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2729425524 * _thisAdjusted = reinterpret_cast<Enumerator_t2729425524 *>(__this + 1);
	Enumerator_VerifyState_m1105097172(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Boolean>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2337153888_gshared (Enumerator_t2729425524 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1105097172((Enumerator_t2729425524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3194695850 * L_2 = (List_1_t3194695850 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3194695850 * L_4 = (List_1_t3194695850 *)__this->get_l_0();
		NullCheck(L_4);
		BooleanU5BU5D_t3568034315* L_5 = (BooleanU5BU5D_t3568034315*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		bool L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2337153888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2729425524 * _thisAdjusted = reinterpret_cast<Enumerator_t2729425524 *>(__this + 1);
	return Enumerator_MoveNext_m2337153888(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Boolean>::get_Current()
extern "C"  bool Enumerator_get_Current_m538608053_gshared (Enumerator_t2729425524 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_current_3();
		return L_0;
	}
}
extern "C"  bool Enumerator_get_Current_m538608053_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2729425524 * _thisAdjusted = reinterpret_cast<Enumerator_t2729425524 *>(__this + 1);
	return Enumerator_get_Current_m538608053(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1246468418_gshared (Enumerator_t3237672747 * __this, List_1_t3702943073 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3702943073 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3702943073 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1246468418_AdjustorThunk (Il2CppObject * __this, List_1_t3702943073 * ___l0, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator__ctor_m1246468418(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m372155760_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1266371732((Enumerator_t3237672747 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m372155760_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m372155760(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1266371732((Enumerator_t3237672747 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		KeyValuePair_2_t38854645  L_2 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		KeyValuePair_2_t38854645  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1324042990_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1324042990(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m2341522715_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3702943073 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2341522715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator_Dispose_m2341522715(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m1266371732_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1266371732_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1266371732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3702943073 * L_0 = (List_1_t3702943073 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3237672747  L_1 = (*(Enumerator_t3237672747 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3702943073 * L_7 = (List_1_t3702943073 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1266371732_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	Enumerator_VerifyState_m1266371732(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3337940480_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1266371732((Enumerator_t3237672747 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3702943073 * L_2 = (List_1_t3702943073 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3702943073 * L_4 = (List_1_t3702943073 *)__this->get_l_0();
		NullCheck(L_4);
		KeyValuePair_2U5BU5D_t2854920344* L_5 = (KeyValuePair_2U5BU5D_t2854920344*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		KeyValuePair_2_t38854645  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3337940480_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	return Enumerator_MoveNext_m3337940480(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m305664535_gshared (Enumerator_t3237672747 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = (KeyValuePair_2_t38854645 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m305664535_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3237672747 * _thisAdjusted = reinterpret_cast<Enumerator_t3237672747 *>(__this + 1);
	return Enumerator_get_Current_m305664535(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1614742070_gshared (Enumerator_t975728254 * __this, List_1_t1440998580 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1440998580 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1614742070_AdjustorThunk (Il2CppObject * __this, List_1_t1440998580 * ___l0, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator__ctor_m1614742070(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1016756388_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1016756388_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1016756388(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2154261170_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2154261170(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1274756239_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1440998580 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1274756239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_Dispose_m1274756239(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2167629240_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2167629240_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2167629240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t975728254  L_1 = (*(Enumerator_t975728254 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1440998580 * L_7 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2167629240_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	Enumerator_VerifyState_m2167629240(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3078170540_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2167629240((Enumerator_t975728254 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1440998580 * L_2 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1440998580 * L_4 = (List_1_t1440998580 *)__this->get_l_0();
		NullCheck(L_4);
		Int32U5BU5D_t3030399641* L_5 = (Int32U5BU5D_t3030399641*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3078170540_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_MoveNext_m3078170540(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1471878379_gshared (Enumerator_t975728254 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m1471878379_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t975728254 * _thisAdjusted = reinterpret_cast<Enumerator_t975728254 *>(__this + 1);
	return Enumerator_get_Current_m1471878379(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3769601633_gshared (Enumerator_t1593300101 * __this, List_1_t2058570427 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2058570427 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3769601633_AdjustorThunk (Il2CppObject * __this, List_1_t2058570427 * ___l0, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator__ctor_m3769601633(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3440386353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3440386353(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_current_3();
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2853089017(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2058570427 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3736175406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_Dispose_m3736175406(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m825848279_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m825848279_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m825848279_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1593300101  L_1 = (*(Enumerator_t1593300101 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2058570427 * L_7 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m825848279_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	Enumerator_VerifyState_m825848279(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m825848279((Enumerator_t1593300101 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2058570427 * L_4 = (List_1_t2058570427 *)__this->get_l_0();
		NullCheck(L_4);
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m44995089_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_MoveNext_m44995089(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m3108634708_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_3();
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m3108634708_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1593300101 * _thisAdjusted = reinterpret_cast<Enumerator_t1593300101 *>(__this + 1);
	return Enumerator_get_Current_m3108634708(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3021143890_gshared (Enumerator_t3292975645 * __this, List_1_t3758245971 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3758245971 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3758245971 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3021143890_AdjustorThunk (Il2CppObject * __this, List_1_t3758245971 * ___l0, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator__ctor_m3021143890(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m610822832_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m610822832_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m610822832(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeNamedArgument_t94157543  L_2 = (CustomAttributeNamedArgument_t94157543 )__this->get_current_3();
		CustomAttributeNamedArgument_t94157543  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1278092846_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1278092846(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3704913451_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3758245971 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3704913451_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_Dispose_m3704913451(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m739025304_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m739025304_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m739025304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3758245971 * L_0 = (List_1_t3758245971 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3292975645  L_1 = (*(Enumerator_t3292975645 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3758245971 * L_7 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m739025304_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	Enumerator_VerifyState_m739025304(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m598197344_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m739025304((Enumerator_t3292975645 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3758245971 * L_2 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3758245971 * L_4 = (List_1_t3758245971 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		CustomAttributeNamedArgument_t94157543  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m598197344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_MoveNext_m598197344(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  Enumerator_get_Current_m3860473239_gshared (Enumerator_t3292975645 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeNamedArgument_t94157543  Enumerator_get_Current_m3860473239_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3292975645 * _thisAdjusted = reinterpret_cast<Enumerator_t3292975645 *>(__this + 1);
	return Enumerator_get_Current_m3860473239(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3421311553_gshared (Enumerator_t402048720 * __this, List_1_t867319046 * ___l0, const MethodInfo* method)
{
	{
		List_1_t867319046 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t867319046 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3421311553_AdjustorThunk (Il2CppObject * __this, List_1_t867319046 * ___l0, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator__ctor_m3421311553(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1436660297_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1436660297_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1436660297(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m355114893_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m355114893_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m355114893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeTypedArgument_t1498197914  L_2 = (CustomAttributeTypedArgument_t1498197914 )__this->get_current_3();
		CustomAttributeTypedArgument_t1498197914  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m355114893_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m355114893(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3434518394_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t867319046 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3434518394_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_Dispose_m3434518394(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m435841047_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m435841047_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m435841047_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t867319046 * L_0 = (List_1_t867319046 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t402048720  L_1 = (*(Enumerator_t402048720 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t867319046 * L_7 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m435841047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	Enumerator_VerifyState_m435841047(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1792725673_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m435841047((Enumerator_t402048720 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t867319046 * L_2 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t867319046 * L_4 = (List_1_t867319046 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_5 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		CustomAttributeTypedArgument_t1498197914  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1792725673_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_MoveNext_m1792725673(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  Enumerator_get_Current_m1371324410_gshared (Enumerator_t402048720 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeTypedArgument_t1498197914  Enumerator_get_Current_m1371324410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t402048720 * _thisAdjusted = reinterpret_cast<Enumerator_t402048720 *>(__this + 1);
	return Enumerator_get_Current_m1371324410(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2390830956_gshared (Enumerator_t980360738 * __this, List_1_t1445631064 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1445631064 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1445631064 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2390830956_AdjustorThunk (Il2CppObject * __this, List_1_t1445631064 * ___l0, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	Enumerator__ctor_m2390830956(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1924386654_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2678209574((Enumerator_t980360738 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1924386654_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1924386654(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m952445890_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m952445890_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m952445890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2678209574((Enumerator_t980360738 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		float L_2 = (float)__this->get_current_3();
		float L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m952445890_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m952445890(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m3373976255_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1445631064 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3373976255_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	Enumerator_Dispose_m3373976255(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2678209574_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2678209574_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2678209574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t980360738  L_1 = (*(Enumerator_t980360738 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1445631064 * L_7 = (List_1_t1445631064 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2678209574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	Enumerator_VerifyState_m2678209574(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1015452742_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2678209574((Enumerator_t980360738 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1445631064 * L_2 = (List_1_t1445631064 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1445631064 * L_4 = (List_1_t1445631064 *)__this->get_l_0();
		NullCheck(L_4);
		SingleU5BU5D_t577127397* L_5 = (SingleU5BU5D_t577127397*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		float L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1015452742_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	return Enumerator_MoveNext_m1015452742(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Single>::get_Current()
extern "C"  float Enumerator_get_Current_m2934617767_gshared (Enumerator_t980360738 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_current_3();
		return L_0;
	}
}
extern "C"  float Enumerator_get_Current_m2934617767_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t980360738 * _thisAdjusted = reinterpret_cast<Enumerator_t980360738 *>(__this + 1);
	return Enumerator_get_Current_m2934617767(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m596314019_gshared (Enumerator_t1053532827 * __this, List_1_t1518803153 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1518803153 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1518803153 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m596314019_AdjustorThunk (Il2CppObject * __this, List_1_t1518803153 * ___l0, const MethodInfo* method)
{
	Enumerator_t1053532827 * _thisAdjusted = reinterpret_cast<Enumerator_t1053532827 *>(__this + 1);
	Enumerator__ctor_m596314019(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3430271327_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3938742477((Enumerator_t1053532827 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3430271327_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1053532827 * _thisAdjusted = reinterpret_cast<Enumerator_t1053532827 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3430271327(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3845423283_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3845423283_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3845423283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3938742477((Enumerator_t1053532827 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		uint32_t L_2 = (uint32_t)__this->get_current_3();
		uint32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3845423283_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1053532827 * _thisAdjusted = reinterpret_cast<Enumerator_t1053532827 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3845423283(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::Dispose()
extern "C"  void Enumerator_Dispose_m3971230368_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1518803153 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3971230368_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1053532827 * _thisAdjusted = reinterpret_cast<Enumerator_t1053532827 *>(__this + 1);
	Enumerator_Dispose_m3971230368(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3938742477_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3938742477_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3938742477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1053532827  L_1 = (*(Enumerator_t1053532827 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1518803153 * L_7 = (List_1_t1518803153 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3938742477_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1053532827 * _thisAdjusted = reinterpret_cast<Enumerator_t1053532827 *>(__this + 1);
	Enumerator_VerifyState_m3938742477(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.UInt32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1386285227_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3938742477((Enumerator_t1053532827 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1518803153 * L_2 = (List_1_t1518803153 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1518803153 * L_4 = (List_1_t1518803153 *)__this->get_l_0();
		NullCheck(L_4);
		UInt32U5BU5D_t59386216* L_5 = (UInt32U5BU5D_t59386216*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		uint32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1386285227_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1053532827 * _thisAdjusted = reinterpret_cast<Enumerator_t1053532827 *>(__this + 1);
	return Enumerator_MoveNext_m1386285227(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.UInt32>::get_Current()
extern "C"  uint32_t Enumerator_get_Current_m3018462446_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  uint32_t Enumerator_get_Current_m3018462446_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1053532827 * _thisAdjusted = reinterpret_cast<Enumerator_t1053532827 *>(__this + 1);
	return Enumerator_get_Current_m3018462446(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2054046066_gshared (Enumerator_t4073335620 * __this, List_1_t243638650 * ___l0, const MethodInfo* method)
{
	{
		List_1_t243638650 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t243638650 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2054046066_AdjustorThunk (Il2CppObject * __this, List_1_t243638650 * ___l0, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator__ctor_m2054046066(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1344379320_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1677639504((Enumerator_t4073335620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1344379320_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1344379320(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1677639504((Enumerator_t4073335620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Color32_t874517518  L_2 = (Color32_t874517518 )__this->get_current_3();
		Color32_t874517518  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3979461448_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3979461448(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C"  void Enumerator_Dispose_m1300762389_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t243638650 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1300762389_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator_Dispose_m1300762389(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m1677639504_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1677639504_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1677639504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t243638650 * L_0 = (List_1_t243638650 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4073335620  L_1 = (*(Enumerator_t4073335620 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t243638650 * L_7 = (List_1_t243638650 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1677639504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	Enumerator_VerifyState_m1677639504(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2625246500_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1677639504((Enumerator_t4073335620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t243638650 * L_2 = (List_1_t243638650 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t243638650 * L_4 = (List_1_t243638650 *)__this->get_l_0();
		NullCheck(L_4);
		Color32U5BU5D_t30278651* L_5 = (Color32U5BU5D_t30278651*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Color32_t874517518  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2625246500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	return Enumerator_MoveNext_m2625246500(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C"  Color32_t874517518  Enumerator_get_Current_m1482710541_gshared (Enumerator_t4073335620 * __this, const MethodInfo* method)
{
	{
		Color32_t874517518  L_0 = (Color32_t874517518 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Color32_t874517518  Enumerator_get_Current_m1482710541_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4073335620 * _thisAdjusted = reinterpret_cast<Enumerator_t4073335620 *>(__this + 1);
	return Enumerator_get_Current_m1482710541(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3979168432_gshared (Enumerator_t3220004478 * __this, List_1_t3685274804 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3685274804 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3685274804 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3979168432_AdjustorThunk (Il2CppObject * __this, List_1_t3685274804 * ___l0, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator__ctor_m3979168432(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m336811426_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2948867230((Enumerator_t3220004478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m336811426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m336811426(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2948867230((Enumerator_t3220004478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		RaycastResult_t21186376  L_2 = (RaycastResult_t21186376 )__this->get_current_3();
		RaycastResult_t21186376  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3079057684_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3079057684(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void Enumerator_Dispose_m3455280711_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3685274804 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3455280711_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator_Dispose_m3455280711(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2948867230_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2948867230_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2948867230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3685274804 * L_0 = (List_1_t3685274804 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3220004478  L_1 = (*(Enumerator_t3220004478 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3685274804 * L_7 = (List_1_t3685274804 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2948867230_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	Enumerator_VerifyState_m2948867230(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2628556578_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2948867230((Enumerator_t3220004478 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3685274804 * L_2 = (List_1_t3685274804 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3685274804 * L_4 = (List_1_t3685274804 *)__this->get_l_0();
		NullCheck(L_4);
		RaycastResultU5BU5D_t603556505* L_5 = (RaycastResultU5BU5D_t603556505*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		RaycastResult_t21186376  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2628556578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	return Enumerator_MoveNext_m2628556578(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C"  RaycastResult_t21186376  Enumerator_get_Current_m2728219003_gshared (Enumerator_t3220004478 * __this, const MethodInfo* method)
{
	{
		RaycastResult_t21186376  L_0 = (RaycastResult_t21186376 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  RaycastResult_t21186376  Enumerator_get_Current_m2728219003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3220004478 * _thisAdjusted = reinterpret_cast<Enumerator_t3220004478 *>(__this + 1);
	return Enumerator_get_Current_m2728219003(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3272552146_gshared (Enumerator_t2571396354 * __this, List_1_t3036666680 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3036666680 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3036666680 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3272552146_AdjustorThunk (Il2CppObject * __this, List_1_t3036666680 * ___l0, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	Enumerator__ctor_m3272552146(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4029239424_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3501381548((Enumerator_t2571396354 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4029239424_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4029239424(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m4221072214_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4221072214_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m4221072214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3501381548((Enumerator_t2571396354 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Playable_t3667545548  L_2 = (Playable_t3667545548 )__this->get_current_3();
		Playable_t3667545548  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4221072214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4221072214(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::Dispose()
extern "C"  void Enumerator_Dispose_m2291099515_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3036666680 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2291099515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	Enumerator_Dispose_m2291099515(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3501381548_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3501381548_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3501381548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3036666680 * L_0 = (List_1_t3036666680 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2571396354  L_1 = (*(Enumerator_t2571396354 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3036666680 * L_7 = (List_1_t3036666680 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3501381548_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	Enumerator_VerifyState_m3501381548(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1706162224_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3501381548((Enumerator_t2571396354 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3036666680 * L_2 = (List_1_t3036666680 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3036666680 * L_4 = (List_1_t3036666680 *)__this->get_l_0();
		NullCheck(L_4);
		PlayableU5BU5D_t4034110853* L_5 = (PlayableU5BU5D_t4034110853*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Playable_t3667545548  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1706162224_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	return Enumerator_MoveNext_m1706162224(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Experimental.Director.Playable>::get_Current()
extern "C"  Playable_t3667545548  Enumerator_get_Current_m552964111_gshared (Enumerator_t2571396354 * __this, const MethodInfo* method)
{
	{
		Playable_t3667545548  L_0 = (Playable_t3667545548 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Playable_t3667545548  Enumerator_get_Current_m552964111_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2571396354 * _thisAdjusted = reinterpret_cast<Enumerator_t2571396354 *>(__this + 1);
	return Enumerator_get_Current_m552964111(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3323918463_gshared (Enumerator_t586447691 * __this, List_1_t1051718017 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1051718017 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1051718017 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3323918463_AdjustorThunk (Il2CppObject * __this, List_1_t1051718017 * ___l0, const MethodInfo* method)
{
	Enumerator_t586447691 * _thisAdjusted = reinterpret_cast<Enumerator_t586447691 *>(__this + 1);
	Enumerator__ctor_m3323918463(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m440946355_gshared (Enumerator_t586447691 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2775700245((Enumerator_t586447691 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m440946355_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t586447691 * _thisAdjusted = reinterpret_cast<Enumerator_t586447691 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m440946355(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3130640747_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3130640747_gshared (Enumerator_t586447691 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3130640747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2775700245((Enumerator_t586447691 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		ChannelPacket_t1682596885  L_2 = (ChannelPacket_t1682596885 )__this->get_current_3();
		ChannelPacket_t1682596885  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3130640747_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t586447691 * _thisAdjusted = reinterpret_cast<Enumerator_t586447691 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3130640747(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::Dispose()
extern "C"  void Enumerator_Dispose_m3809357816_gshared (Enumerator_t586447691 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1051718017 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3809357816_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t586447691 * _thisAdjusted = reinterpret_cast<Enumerator_t586447691 *>(__this + 1);
	Enumerator_Dispose_m3809357816(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2775700245_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2775700245_gshared (Enumerator_t586447691 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2775700245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1051718017 * L_0 = (List_1_t1051718017 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t586447691  L_1 = (*(Enumerator_t586447691 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1051718017 * L_7 = (List_1_t1051718017 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2775700245_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t586447691 * _thisAdjusted = reinterpret_cast<Enumerator_t586447691 *>(__this + 1);
	Enumerator_VerifyState_m2775700245(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1733603335_gshared (Enumerator_t586447691 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2775700245((Enumerator_t586447691 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1051718017 * L_2 = (List_1_t1051718017 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1051718017 * L_4 = (List_1_t1051718017 *)__this->get_l_0();
		NullCheck(L_4);
		ChannelPacketU5BU5D_t3883591672* L_5 = (ChannelPacketU5BU5D_t3883591672*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		ChannelPacket_t1682596885  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1733603335_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t586447691 * _thisAdjusted = reinterpret_cast<Enumerator_t586447691 *>(__this + 1);
	return Enumerator_MoveNext_m1733603335(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::get_Current()
extern "C"  ChannelPacket_t1682596885  Enumerator_get_Current_m1251173828_gshared (Enumerator_t586447691 * __this, const MethodInfo* method)
{
	{
		ChannelPacket_t1682596885  L_0 = (ChannelPacket_t1682596885 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  ChannelPacket_t1682596885  Enumerator_get_Current_m1251173828_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t586447691 * _thisAdjusted = reinterpret_cast<Enumerator_t586447691 *>(__this + 1);
	return Enumerator_get_Current_m1251173828(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2511073500_gshared (Enumerator_t4076637021 * __this, List_1_t246940051 * ___l0, const MethodInfo* method)
{
	{
		List_1_t246940051 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t246940051 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2511073500_AdjustorThunk (Il2CppObject * __this, List_1_t246940051 * ___l0, const MethodInfo* method)
{
	Enumerator_t4076637021 * _thisAdjusted = reinterpret_cast<Enumerator_t4076637021 *>(__this + 1);
	Enumerator__ctor_m2511073500(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m597985246_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m566577098((Enumerator_t4076637021 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m597985246_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4076637021 * _thisAdjusted = reinterpret_cast<Enumerator_t4076637021 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m597985246(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2336358082_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2336358082_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2336358082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m566577098((Enumerator_t4076637021 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		PendingOwner_t877818919  L_2 = (PendingOwner_t877818919 )__this->get_current_3();
		PendingOwner_t877818919  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2336358082_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4076637021 * _thisAdjusted = reinterpret_cast<Enumerator_t4076637021 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2336358082(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::Dispose()
extern "C"  void Enumerator_Dispose_m100793535_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t246940051 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m100793535_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4076637021 * _thisAdjusted = reinterpret_cast<Enumerator_t4076637021 *>(__this + 1);
	Enumerator_Dispose_m100793535(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m566577098_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m566577098_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m566577098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t246940051 * L_0 = (List_1_t246940051 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4076637021  L_1 = (*(Enumerator_t4076637021 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t246940051 * L_7 = (List_1_t246940051 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m566577098_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4076637021 * _thisAdjusted = reinterpret_cast<Enumerator_t4076637021 *>(__this + 1);
	Enumerator_VerifyState_m566577098(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2874416406_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m566577098((Enumerator_t4076637021 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t246940051 * L_2 = (List_1_t246940051 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t246940051 * L_4 = (List_1_t246940051 *)__this->get_l_0();
		NullCheck(L_4);
		PendingOwnerU5BU5D_t2720009054* L_5 = (PendingOwnerU5BU5D_t2720009054*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		PendingOwner_t877818919  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2874416406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4076637021 * _thisAdjusted = reinterpret_cast<Enumerator_t4076637021 *>(__this + 1);
	return Enumerator_MoveNext_m2874416406(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ClientScene/PendingOwner>::get_Current()
extern "C"  PendingOwner_t877818919  Enumerator_get_Current_m162665063_gshared (Enumerator_t4076637021 * __this, const MethodInfo* method)
{
	{
		PendingOwner_t877818919  L_0 = (PendingOwner_t877818919 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  PendingOwner_t877818919  Enumerator_get_Current_m162665063_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4076637021 * _thisAdjusted = reinterpret_cast<Enumerator_t4076637021 *>(__this + 1);
	return Enumerator_get_Current_m162665063(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2227776565_gshared (Enumerator_t4176439824 * __this, List_1_t346742854 * ___l0, const MethodInfo* method)
{
	{
		List_1_t346742854 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t346742854 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2227776565_AdjustorThunk (Il2CppObject * __this, List_1_t346742854 * ___l0, const MethodInfo* method)
{
	Enumerator_t4176439824 * _thisAdjusted = reinterpret_cast<Enumerator_t4176439824 *>(__this + 1);
	Enumerator__ctor_m2227776565(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3631846949_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3332355895((Enumerator_t4176439824 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3631846949_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4176439824 * _thisAdjusted = reinterpret_cast<Enumerator_t4176439824 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3631846949(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1820733745_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1820733745_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1820733745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3332355895((Enumerator_t4176439824 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		InternalMsg_t977621722  L_2 = (InternalMsg_t977621722 )__this->get_current_3();
		InternalMsg_t977621722  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1820733745_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4176439824 * _thisAdjusted = reinterpret_cast<Enumerator_t4176439824 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1820733745(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::Dispose()
extern "C"  void Enumerator_Dispose_m2619804142_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t346742854 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2619804142_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4176439824 * _thisAdjusted = reinterpret_cast<Enumerator_t4176439824 *>(__this + 1);
	Enumerator_Dispose_m2619804142(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3332355895_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3332355895_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3332355895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t346742854 * L_0 = (List_1_t346742854 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4176439824  L_1 = (*(Enumerator_t4176439824 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t346742854 * L_7 = (List_1_t346742854 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3332355895_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4176439824 * _thisAdjusted = reinterpret_cast<Enumerator_t4176439824 *>(__this + 1);
	Enumerator_VerifyState_m3332355895(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1625322877_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3332355895((Enumerator_t4176439824 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t346742854 * L_2 = (List_1_t346742854 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t346742854 * L_4 = (List_1_t346742854 *)__this->get_l_0();
		NullCheck(L_4);
		InternalMsgU5BU5D_t2023740543* L_5 = (InternalMsgU5BU5D_t2023740543*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		InternalMsg_t977621722  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1625322877_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4176439824 * _thisAdjusted = reinterpret_cast<Enumerator_t4176439824 *>(__this + 1);
	return Enumerator_MoveNext_m1625322877(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::get_Current()
extern "C"  InternalMsg_t977621722  Enumerator_get_Current_m2081821142_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method)
{
	{
		InternalMsg_t977621722  L_0 = (InternalMsg_t977621722 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  InternalMsg_t977621722  Enumerator_get_Current_m2081821142_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4176439824 * _thisAdjusted = reinterpret_cast<Enumerator_t4176439824 *>(__this + 1);
	return Enumerator_get_Current_m2081821142(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m578968982_gshared (Enumerator_t420945823 * __this, List_1_t886216149 * ___l0, const MethodInfo* method)
{
	{
		List_1_t886216149 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t886216149 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m578968982_AdjustorThunk (Il2CppObject * __this, List_1_t886216149 * ___l0, const MethodInfo* method)
{
	Enumerator_t420945823 * _thisAdjusted = reinterpret_cast<Enumerator_t420945823 *>(__this + 1);
	Enumerator__ctor_m578968982(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3810474372_gshared (Enumerator_t420945823 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2766416936((Enumerator_t420945823 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3810474372_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t420945823 * _thisAdjusted = reinterpret_cast<Enumerator_t420945823 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3810474372(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2299228178_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2299228178_gshared (Enumerator_t420945823 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2299228178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2766416936((Enumerator_t420945823 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		PendingPlayer_t1517095017  L_2 = (PendingPlayer_t1517095017 )__this->get_current_3();
		PendingPlayer_t1517095017  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2299228178_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t420945823 * _thisAdjusted = reinterpret_cast<Enumerator_t420945823 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2299228178(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Dispose()
extern "C"  void Enumerator_Dispose_m2059442621_gshared (Enumerator_t420945823 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t886216149 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2059442621_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t420945823 * _thisAdjusted = reinterpret_cast<Enumerator_t420945823 *>(__this + 1);
	Enumerator_Dispose_m2059442621(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2766416936_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2766416936_gshared (Enumerator_t420945823 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2766416936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t886216149 * L_0 = (List_1_t886216149 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t420945823  L_1 = (*(Enumerator_t420945823 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t886216149 * L_7 = (List_1_t886216149 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2766416936_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t420945823 * _thisAdjusted = reinterpret_cast<Enumerator_t420945823 *>(__this + 1);
	Enumerator_VerifyState_m2766416936(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2566888296_gshared (Enumerator_t420945823 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2766416936((Enumerator_t420945823 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t886216149 * L_2 = (List_1_t886216149 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t886216149 * L_4 = (List_1_t886216149 *)__this->get_l_0();
		NullCheck(L_4);
		PendingPlayerU5BU5D_t220373972* L_5 = (PendingPlayerU5BU5D_t220373972*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		PendingPlayer_t1517095017  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2566888296_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t420945823 * _thisAdjusted = reinterpret_cast<Enumerator_t420945823 *>(__this + 1);
	return Enumerator_MoveNext_m2566888296(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::get_Current()
extern "C"  PendingPlayer_t1517095017  Enumerator_get_Current_m2424673909_gshared (Enumerator_t420945823 * __this, const MethodInfo* method)
{
	{
		PendingPlayer_t1517095017  L_0 = (PendingPlayer_t1517095017 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  PendingPlayer_t1517095017  Enumerator_get_Current_m2424673909_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t420945823 * _thisAdjusted = reinterpret_cast<Enumerator_t420945823 *>(__this + 1);
	return Enumerator_get_Current_m2424673909(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1124433566_gshared (Enumerator_t957226927 * __this, List_1_t1422497253 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1422497253 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1422497253 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1124433566_AdjustorThunk (Il2CppObject * __this, List_1_t1422497253 * ___l0, const MethodInfo* method)
{
	Enumerator_t957226927 * _thisAdjusted = reinterpret_cast<Enumerator_t957226927 *>(__this + 1);
	Enumerator__ctor_m1124433566(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2732036044_gshared (Enumerator_t957226927 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m790311576((Enumerator_t957226927 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2732036044_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957226927 * _thisAdjusted = reinterpret_cast<Enumerator_t957226927 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2732036044(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1667010418_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1667010418_gshared (Enumerator_t957226927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1667010418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m790311576((Enumerator_t957226927 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		PendingPlayerInfo_t2053376121  L_2 = (PendingPlayerInfo_t2053376121 )__this->get_current_3();
		PendingPlayerInfo_t2053376121  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1667010418_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957226927 * _thisAdjusted = reinterpret_cast<Enumerator_t957226927 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1667010418(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m708056949_gshared (Enumerator_t957226927 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1422497253 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m708056949_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957226927 * _thisAdjusted = reinterpret_cast<Enumerator_t957226927 *>(__this + 1);
	Enumerator_Dispose_m708056949(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m790311576_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m790311576_gshared (Enumerator_t957226927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m790311576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1422497253 * L_0 = (List_1_t1422497253 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t957226927  L_1 = (*(Enumerator_t957226927 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1422497253 * L_7 = (List_1_t1422497253 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m790311576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957226927 * _thisAdjusted = reinterpret_cast<Enumerator_t957226927 *>(__this + 1);
	Enumerator_VerifyState_m790311576(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1827704352_gshared (Enumerator_t957226927 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m790311576((Enumerator_t957226927 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1422497253 * L_2 = (List_1_t1422497253 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1422497253 * L_4 = (List_1_t1422497253 *)__this->get_l_0();
		NullCheck(L_4);
		PendingPlayerInfoU5BU5D_t2199507332* L_5 = (PendingPlayerInfoU5BU5D_t2199507332*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		PendingPlayerInfo_t2053376121  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1827704352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957226927 * _thisAdjusted = reinterpret_cast<Enumerator_t957226927 *>(__this + 1);
	return Enumerator_MoveNext_m1827704352(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::get_Current()
extern "C"  PendingPlayerInfo_t2053376121  Enumerator_get_Current_m2426197637_gshared (Enumerator_t957226927 * __this, const MethodInfo* method)
{
	{
		PendingPlayerInfo_t2053376121  L_0 = (PendingPlayerInfo_t2053376121 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  PendingPlayerInfo_t2053376121  Enumerator_get_Current_m2426197637_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t957226927 * _thisAdjusted = reinterpret_cast<Enumerator_t957226927 *>(__this + 1);
	return Enumerator_get_Current_m2426197637(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3505010156_gshared (Enumerator_t3722906299 * __this, List_1_t4188176625 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4188176625 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4188176625 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3505010156_AdjustorThunk (Il2CppObject * __this, List_1_t4188176625 * ___l0, const MethodInfo* method)
{
	Enumerator_t3722906299 * _thisAdjusted = reinterpret_cast<Enumerator_t3722906299 *>(__this + 1);
	Enumerator__ctor_m3505010156(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2582989766_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m453828750((Enumerator_t3722906299 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2582989766_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3722906299 * _thisAdjusted = reinterpret_cast<Enumerator_t3722906299 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2582989766(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m4023888536_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4023888536_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m4023888536_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m453828750((Enumerator_t3722906299 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CRCMessageEntry_t524088197  L_2 = (CRCMessageEntry_t524088197 )__this->get_current_3();
		CRCMessageEntry_t524088197  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4023888536_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3722906299 * _thisAdjusted = reinterpret_cast<Enumerator_t3722906299 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m4023888536(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Dispose()
extern "C"  void Enumerator_Dispose_m3211405525_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4188176625 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3211405525_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3722906299 * _thisAdjusted = reinterpret_cast<Enumerator_t3722906299 *>(__this + 1);
	Enumerator_Dispose_m3211405525(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m453828750_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m453828750_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m453828750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4188176625 * L_0 = (List_1_t4188176625 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3722906299  L_1 = (*(Enumerator_t3722906299 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4188176625 * L_7 = (List_1_t4188176625 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m453828750_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3722906299 * _thisAdjusted = reinterpret_cast<Enumerator_t3722906299 *>(__this + 1);
	Enumerator_VerifyState_m453828750(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m24086994_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m453828750((Enumerator_t3722906299 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4188176625 * L_2 = (List_1_t4188176625 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4188176625 * L_4 = (List_1_t4188176625 *)__this->get_l_0();
		NullCheck(L_4);
		CRCMessageEntryU5BU5D_t2803924168* L_5 = (CRCMessageEntryU5BU5D_t2803924168*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		CRCMessageEntry_t524088197  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m24086994_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3722906299 * _thisAdjusted = reinterpret_cast<Enumerator_t3722906299 *>(__this + 1);
	return Enumerator_MoveNext_m24086994(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::get_Current()
extern "C"  CRCMessageEntry_t524088197  Enumerator_get_Current_m2486588269_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method)
{
	{
		CRCMessageEntry_t524088197  L_0 = (CRCMessageEntry_t524088197 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CRCMessageEntry_t524088197  Enumerator_get_Current_m2486588269_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3722906299 * _thisAdjusted = reinterpret_cast<Enumerator_t3722906299 *>(__this + 1);
	return Enumerator_get_Current_m2486588269(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m271251706_gshared (Enumerator_t3595312489 * __this, List_1_t4060582815 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4060582815 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4060582815 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m271251706_AdjustorThunk (Il2CppObject * __this, List_1_t4060582815 * ___l0, const MethodInfo* method)
{
	Enumerator_t3595312489 * _thisAdjusted = reinterpret_cast<Enumerator_t3595312489 *>(__this + 1);
	Enumerator__ctor_m271251706(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2908971816_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3601227088((Enumerator_t3595312489 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2908971816_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3595312489 * _thisAdjusted = reinterpret_cast<Enumerator_t3595312489 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2908971816(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m714227684_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m714227684_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m714227684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3601227088((Enumerator_t3595312489 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		PeerInfoPlayer_t396494387  L_2 = (PeerInfoPlayer_t396494387 )__this->get_current_3();
		PeerInfoPlayer_t396494387  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m714227684_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3595312489 * _thisAdjusted = reinterpret_cast<Enumerator_t3595312489 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m714227684(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Dispose()
extern "C"  void Enumerator_Dispose_m3825679319_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4060582815 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3825679319_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3595312489 * _thisAdjusted = reinterpret_cast<Enumerator_t3595312489 *>(__this + 1);
	Enumerator_Dispose_m3825679319(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3601227088_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3601227088_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3601227088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4060582815 * L_0 = (List_1_t4060582815 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3595312489  L_1 = (*(Enumerator_t3595312489 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4060582815 * L_7 = (List_1_t4060582815 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3601227088_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3595312489 * _thisAdjusted = reinterpret_cast<Enumerator_t3595312489 *>(__this + 1);
	Enumerator_VerifyState_m3601227088(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3774002024_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3601227088((Enumerator_t3595312489 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4060582815 * L_2 = (List_1_t4060582815 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4060582815 * L_4 = (List_1_t4060582815 *)__this->get_l_0();
		NullCheck(L_4);
		PeerInfoPlayerU5BU5D_t1632998306* L_5 = (PeerInfoPlayerU5BU5D_t1632998306*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		PeerInfoPlayer_t396494387  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3774002024_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3595312489 * _thisAdjusted = reinterpret_cast<Enumerator_t3595312489 *>(__this + 1);
	return Enumerator_MoveNext_m3774002024(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::get_Current()
extern "C"  PeerInfoPlayer_t396494387  Enumerator_get_Current_m2503784191_gshared (Enumerator_t3595312489 * __this, const MethodInfo* method)
{
	{
		PeerInfoPlayer_t396494387  L_0 = (PeerInfoPlayer_t396494387 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  PeerInfoPlayer_t396494387  Enumerator_get_Current_m2503784191_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3595312489 * _thisAdjusted = reinterpret_cast<Enumerator_t3595312489 *>(__this + 1);
	return Enumerator_get_Current_m2503784191(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.QosType>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m59977717_gshared (Enumerator_t907743289 * __this, List_1_t1373013615 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1373013615 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1373013615 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m59977717_AdjustorThunk (Il2CppObject * __this, List_1_t1373013615 * ___l0, const MethodInfo* method)
{
	Enumerator_t907743289 * _thisAdjusted = reinterpret_cast<Enumerator_t907743289 *>(__this + 1);
	Enumerator__ctor_m59977717(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.QosType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2357575333_gshared (Enumerator_t907743289 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4042102303((Enumerator_t907743289 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2357575333_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t907743289 * _thisAdjusted = reinterpret_cast<Enumerator_t907743289 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2357575333(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.QosType>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3303742809_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3303742809_gshared (Enumerator_t907743289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3303742809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m4042102303((Enumerator_t907743289 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3303742809_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t907743289 * _thisAdjusted = reinterpret_cast<Enumerator_t907743289 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3303742809(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.QosType>::Dispose()
extern "C"  void Enumerator_Dispose_m2256484782_gshared (Enumerator_t907743289 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1373013615 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2256484782_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t907743289 * _thisAdjusted = reinterpret_cast<Enumerator_t907743289 *>(__this + 1);
	Enumerator_Dispose_m2256484782(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.QosType>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m4042102303_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4042102303_gshared (Enumerator_t907743289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4042102303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1373013615 * L_0 = (List_1_t1373013615 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t907743289  L_1 = (*(Enumerator_t907743289 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1373013615 * L_7 = (List_1_t1373013615 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4042102303_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t907743289 * _thisAdjusted = reinterpret_cast<Enumerator_t907743289 *>(__this + 1);
	Enumerator_VerifyState_m4042102303(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.QosType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1724529181_gshared (Enumerator_t907743289 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m4042102303((Enumerator_t907743289 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1373013615 * L_2 = (List_1_t1373013615 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1373013615 * L_4 = (List_1_t1373013615 *)__this->get_l_0();
		NullCheck(L_4);
		QosTypeU5BU5D_t740748178* L_5 = (QosTypeU5BU5D_t740748178*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1724529181_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t907743289 * _thisAdjusted = reinterpret_cast<Enumerator_t907743289 *>(__this + 1);
	return Enumerator_MoveNext_m1724529181(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.QosType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m41742814_gshared (Enumerator_t907743289 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m41742814_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t907743289 * _thisAdjusted = reinterpret_cast<Enumerator_t907743289 *>(__this + 1);
	return Enumerator_get_Current_m41742814(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3512622280_gshared (Enumerator_t1960487606 * __this, List_1_t2425757932 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2425757932 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2425757932 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3512622280_AdjustorThunk (Il2CppObject * __this, List_1_t2425757932 * ___l0, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	Enumerator__ctor_m3512622280(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2200349770_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2358705882((Enumerator_t1960487606 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2200349770_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2200349770(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3461301268_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3461301268_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3461301268_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2358705882((Enumerator_t1960487606 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UICharInfo_t3056636800  L_2 = (UICharInfo_t3056636800 )__this->get_current_3();
		UICharInfo_t3056636800  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3461301268_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3461301268(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m3756179807_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2425757932 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3756179807_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	Enumerator_Dispose_m3756179807(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2358705882_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2358705882_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2358705882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2425757932 * L_0 = (List_1_t2425757932 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1960487606  L_1 = (*(Enumerator_t1960487606 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2425757932 * L_7 = (List_1_t2425757932 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2358705882_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	Enumerator_VerifyState_m2358705882(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m848781978_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2358705882((Enumerator_t1960487606 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2425757932 * L_2 = (List_1_t2425757932 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2425757932 * L_4 = (List_1_t2425757932 *)__this->get_l_0();
		NullCheck(L_4);
		UICharInfoU5BU5D_t2749705857* L_5 = (UICharInfoU5BU5D_t2749705857*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		UICharInfo_t3056636800  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m848781978_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	return Enumerator_MoveNext_m848781978(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C"  UICharInfo_t3056636800  Enumerator_get_Current_m3839136987_gshared (Enumerator_t1960487606 * __this, const MethodInfo* method)
{
	{
		UICharInfo_t3056636800  L_0 = (UICharInfo_t3056636800 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UICharInfo_t3056636800  Enumerator_get_Current_m3839136987_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1960487606 * _thisAdjusted = reinterpret_cast<Enumerator_t1960487606 *>(__this + 1);
	return Enumerator_get_Current_m3839136987(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3903095790_gshared (Enumerator_t2525128680 * __this, List_1_t2990399006 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2990399006 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2990399006 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3903095790_AdjustorThunk (Il2CppObject * __this, List_1_t2990399006 * ___l0, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	Enumerator__ctor_m3903095790(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m925111644_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m4188527104((Enumerator_t2525128680 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m925111644_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m925111644(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3228580602_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3228580602_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3228580602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m4188527104((Enumerator_t2525128680 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UILineInfo_t3621277874  L_2 = (UILineInfo_t3621277874 )__this->get_current_3();
		UILineInfo_t3621277874  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3228580602_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3228580602(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m3109097029_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2990399006 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3109097029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	Enumerator_Dispose_m3109097029(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m4188527104_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4188527104_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4188527104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2990399006 * L_0 = (List_1_t2990399006 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2525128680  L_1 = (*(Enumerator_t2525128680 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2990399006 * L_7 = (List_1_t2990399006 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m4188527104_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	Enumerator_VerifyState_m4188527104(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2504790928_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m4188527104((Enumerator_t2525128680 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2990399006 * L_2 = (List_1_t2990399006 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2990399006 * L_4 = (List_1_t2990399006 *)__this->get_l_0();
		NullCheck(L_4);
		UILineInfoU5BU5D_t3471944775* L_5 = (UILineInfoU5BU5D_t3471944775*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		UILineInfo_t3621277874  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2504790928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	return Enumerator_MoveNext_m2504790928(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C"  UILineInfo_t3621277874  Enumerator_get_Current_m657641165_gshared (Enumerator_t2525128680 * __this, const MethodInfo* method)
{
	{
		UILineInfo_t3621277874  L_0 = (UILineInfo_t3621277874 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UILineInfo_t3621277874  Enumerator_get_Current_m657641165_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2525128680 * _thisAdjusted = reinterpret_cast<Enumerator_t2525128680 *>(__this + 1);
	return Enumerator_get_Current_m657641165(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2578663110_gshared (Enumerator_t108109624 * __this, List_1_t573379950 * ___l0, const MethodInfo* method)
{
	{
		List_1_t573379950 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t573379950 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2578663110_AdjustorThunk (Il2CppObject * __this, List_1_t573379950 * ___l0, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	Enumerator__ctor_m2578663110(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3052395060_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2807892176((Enumerator_t108109624 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3052395060_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3052395060(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m38564970_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m38564970_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m38564970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2807892176((Enumerator_t108109624 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		UIVertex_t1204258818  L_2 = (UIVertex_t1204258818 )__this->get_current_3();
		UIVertex_t1204258818  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m38564970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m38564970(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C"  void Enumerator_Dispose_m1292917021_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t573379950 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1292917021_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	Enumerator_Dispose_m1292917021(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m2807892176_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2807892176_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2807892176_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t573379950 * L_0 = (List_1_t573379950 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t108109624  L_1 = (*(Enumerator_t108109624 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t573379950 * L_7 = (List_1_t573379950 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2807892176_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	Enumerator_VerifyState_m2807892176(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m138320264_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2807892176((Enumerator_t108109624 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t573379950 * L_2 = (List_1_t573379950 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t573379950 * L_4 = (List_1_t573379950 *)__this->get_l_0();
		NullCheck(L_4);
		UIVertexU5BU5D_t3048644023* L_5 = (UIVertexU5BU5D_t3048644023*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		UIVertex_t1204258818  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m138320264_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	return Enumerator_MoveNext_m138320264(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C"  UIVertex_t1204258818  Enumerator_get_Current_m2585076237_gshared (Enumerator_t108109624 * __this, const MethodInfo* method)
{
	{
		UIVertex_t1204258818  L_0 = (UIVertex_t1204258818 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  UIVertex_t1204258818  Enumerator_get_Current_m2585076237_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t108109624 * _thisAdjusted = reinterpret_cast<Enumerator_t108109624 *>(__this + 1);
	return Enumerator_get_Current_m2585076237(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3172601063_gshared (Enumerator_t1147558385 * __this, List_1_t1612828711 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1612828711 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1612828711 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3172601063_AdjustorThunk (Il2CppObject * __this, List_1_t1612828711 * ___l0, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	Enumerator__ctor_m3172601063(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1334470667_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3913376581((Enumerator_t1147558385 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1334470667_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1334470667(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3542273247_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3542273247_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3542273247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3913376581((Enumerator_t1147558385 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Vector2_t2243707579  L_2 = (Vector2_t2243707579 )__this->get_current_3();
		Vector2_t2243707579  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3542273247_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3542273247(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::Dispose()
extern "C"  void Enumerator_Dispose_m3717265706_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1612828711 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3717265706_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	Enumerator_Dispose_m3717265706(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3913376581_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3913376581_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3913376581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1612828711 * L_0 = (List_1_t1612828711 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1147558385  L_1 = (*(Enumerator_t1147558385 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1612828711 * L_7 = (List_1_t1612828711 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3913376581_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	Enumerator_VerifyState_m3913376581(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3483405135_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3913376581((Enumerator_t1147558385 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1612828711 * L_2 = (List_1_t1612828711 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1612828711 * L_4 = (List_1_t1612828711 *)__this->get_l_0();
		NullCheck(L_4);
		Vector2U5BU5D_t686124026* L_5 = (Vector2U5BU5D_t686124026*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Vector2_t2243707579  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3483405135_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	return Enumerator_MoveNext_m3483405135(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
extern "C"  Vector2_t2243707579  Enumerator_get_Current_m1551076836_gshared (Enumerator_t1147558385 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = (Vector2_t2243707579 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Vector2_t2243707579  Enumerator_get_Current_m1551076836_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558385 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558385 *>(__this + 1);
	return Enumerator_get_Current_m1551076836(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1365181512_gshared (Enumerator_t1147558386 * __this, List_1_t1612828712 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1612828712 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1612828712 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1365181512_AdjustorThunk (Il2CppObject * __this, List_1_t1612828712 * ___l0, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	Enumerator__ctor_m1365181512(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3796537546_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3639069574((Enumerator_t1147558386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3796537546_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3796537546(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1103666686_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1103666686_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1103666686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3639069574((Enumerator_t1147558386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Vector3_t2243707580  L_2 = (Vector3_t2243707580 )__this->get_current_3();
		Vector3_t2243707580  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1103666686_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1103666686(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C"  void Enumerator_Dispose_m3215924523_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1612828712 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3215924523_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	Enumerator_Dispose_m3215924523(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3639069574_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3639069574_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3639069574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1612828712 * L_0 = (List_1_t1612828712 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1147558386  L_1 = (*(Enumerator_t1147558386 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1612828712 * L_7 = (List_1_t1612828712 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3639069574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	Enumerator_VerifyState_m3639069574(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1367380970_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3639069574((Enumerator_t1147558386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1612828712 * L_2 = (List_1_t1612828712 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1612828712 * L_4 = (List_1_t1612828712 *)__this->get_l_0();
		NullCheck(L_4);
		Vector3U5BU5D_t1172311765* L_5 = (Vector3U5BU5D_t1172311765*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Vector3_t2243707580  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1367380970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	return Enumerator_MoveNext_m1367380970(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C"  Vector3_t2243707580  Enumerator_get_Current_m827571811_gshared (Enumerator_t1147558386 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = (Vector3_t2243707580 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  Enumerator_get_Current_m827571811_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558386 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558386 *>(__this + 1);
	return Enumerator_get_Current_m827571811(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m425576865_gshared (Enumerator_t1147558387 * __this, List_1_t1612828713 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1612828713 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1612828713 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m425576865_AdjustorThunk (Il2CppObject * __this, List_1_t1612828713 * ___l0, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	Enumerator__ctor_m425576865(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2621684617_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m3775669055((Enumerator_t1147558387 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2621684617_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2621684617(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3866069145_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3866069145_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3866069145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m3775669055((Enumerator_t1147558387 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m102359810(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Vector4_t2243707581  L_2 = (Vector4_t2243707581 )__this->get_current_3();
		Vector4_t2243707581  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3866069145_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3866069145(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::Dispose()
extern "C"  void Enumerator_Dispose_m2705653668_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1612828713 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2705653668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	Enumerator_Dispose_m2705653668(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1258650672;
extern const uint32_t Enumerator_VerifyState_m3775669055_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m3775669055_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m3775669055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1612828713 * L_0 = (List_1_t1612828713 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1147558387  L_1 = (*(Enumerator_t1147558387 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t2695136451 * L_5 = (ObjectDisposedException_t2695136451 *)il2cpp_codegen_object_new(ObjectDisposedException_t2695136451_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3156784572(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1612828713 * L_7 = (List_1_t1612828713 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t721527559 * L_9 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_9, (String_t*)_stringLiteral1258650672, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m3775669055_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	Enumerator_VerifyState_m3775669055(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3293920409_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m3775669055((Enumerator_t1147558387 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1612828713 * L_2 = (List_1_t1612828713 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1612828713 * L_4 = (List_1_t1612828713 *)__this->get_l_0();
		NullCheck(L_4);
		Vector4U5BU5D_t1658499504* L_5 = (Vector4U5BU5D_t1658499504*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Vector4_t2243707581  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3293920409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	return Enumerator_MoveNext_m3293920409(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector4>::get_Current()
extern "C"  Vector4_t2243707581  Enumerator_get_Current_m2657372766_gshared (Enumerator_t1147558387 * __this, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0 = (Vector4_t2243707581 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Vector4_t2243707581  Enumerator_get_Current_m2657372766_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1147558387 * _thisAdjusted = reinterpret_cast<Enumerator_t1147558387 *>(__this + 1);
	return Enumerator_get_Current_m2657372766(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::.ctor()
extern "C"  void List_1__ctor_m4088815761_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		BooleanU5BU5D_t3568034315* L_0 = ((List_1_t3194695850_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m195355150_gshared (List_1_t3194695850 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t3194695850 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		BooleanU5BU5D_t3568034315* L_3 = ((List_1_t3194695850_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t3194695850 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Boolean>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((BooleanU5BU5D_t3568034315*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t3194695850 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3404069002;
extern const uint32_t List_1__ctor_m4031648784_MetadataUsageId;
extern "C"  void List_1__ctor_m4031648784_gshared (List_1_t3194695850 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m4031648784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, (String_t*)_stringLiteral3404069002, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((BooleanU5BU5D_t3568034315*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::.cctor()
extern "C"  void List_1__cctor_m898180560_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t3194695850_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((BooleanU5BU5D_t3568034315*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Boolean>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m67794145_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3194695850 *)__this);
		Enumerator_t2729425524  L_0 = ((  Enumerator_t2729425524  (*) (List_1_t3194695850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3194695850 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t2729425524  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1079767561_gshared (List_1_t3194695850 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3568034315* L_0 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3071423402_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3194695850 *)__this);
		Enumerator_t2729425524  L_0 = ((  Enumerator_t2729425524  (*) (List_1_t3194695850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3194695850 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t2729425524  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Add_m249194841_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m249194841_gshared (List_1_t3194695850 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m249194841_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3194695850 *)__this);
			((  void (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t3194695850 *)__this, (bool)((*(bool*)((bool*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m3691758777_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m3691758777_gshared (List_1_t3194695850 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m3691758777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3194695850 *)__this);
			bool L_1 = ((  bool (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t3194695850 *)__this, (bool)((*(bool*)((bool*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m3305333863_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3305333863_gshared (List_1_t3194695850 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m3305333863_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3194695850 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t3194695850 *)__this, (bool)((*(bool*)((bool*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Insert_m905796272_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m905796272_gshared (List_1_t3194695850 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m905796272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t3194695850 *)__this);
			((  void (*) (List_1_t3194695850 *, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_1, (bool)((*(bool*)((bool*)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m2869574338_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m2869574338_gshared (List_1_t3194695850 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m2869574338_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3194695850 *)__this);
			((  bool (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t3194695850 *)__this, (bool)((*(bool*)((bool*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m58076922_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3525661301_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m18980517_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1252771920_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m634795177_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2639215682_gshared (List_1_t3194695850 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3194695850 *)__this);
		bool L_1 = ((  bool (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t List_1_System_Collections_IList_set_Item_m1346264815_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m1346264815_gshared (List_1_t3194695850 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m1346264815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t3194695850 *)__this);
			((  void (*) (List_1_t3194695850 *, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_0, (bool)((*(bool*)((bool*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::Add(T)
extern "C"  void List_1_Add_m2149021052_gshared (List_1_t3194695850 * __this, bool ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		BooleanU5BU5D_t3568034315* L_1 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		BooleanU5BU5D_t3568034315* L_2 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		bool L_6 = ___item0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (bool)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2750918313_gshared (List_1_t3194695850 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		BooleanU5BU5D_t3568034315* L_3 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t3194695850 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t3194695850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t3194695850 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1142687897_gshared (List_1_t3194695850 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Boolean>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		BooleanU5BU5D_t3568034315* L_5 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< BooleanU5BU5D_t3568034315*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Boolean>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (BooleanU5BU5D_t3568034315*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m2700213785_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m2700213785_gshared (List_1_t3194695850 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m2700213785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Boolean>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			bool L_3 = InterfaceFuncInvoker0< bool >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Boolean>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (bool)L_3;
			bool L_4 = V_0;
			NullCheck((List_1_t3194695850 *)__this);
			((  void (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t3194695850 *)__this, (bool)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3150556562_gshared (List_1_t3194695850 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t3194695850 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t3194695850 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t3194695850 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Boolean>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t4011360410 * List_1_AsReadOnly_m306891441_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t4011360410 * L_0 = (ReadOnlyCollection_1_t4011360410 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (ReadOnlyCollection_1_t4011360410 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_0, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::Clear()
extern "C"  void List_1_Clear_m4170865056_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3568034315* L_0 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		BooleanU5BU5D_t3568034315* L_1 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::Contains(T)
extern "C"  bool List_1_Contains_m4069231050_gshared (List_1_t3194695850 * __this, bool ___item0, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3568034315* L_0 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		bool L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, BooleanU5BU5D_t3568034315*, bool, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (BooleanU5BU5D_t3568034315*)L_0, (bool)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m926192388_gshared (List_1_t3194695850 * __this, BooleanU5BU5D_t3568034315* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3568034315* L_0 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		BooleanU5BU5D_t3568034315* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// T System.Collections.Generic.List`1<System.Boolean>::Find(System.Predicate`1<T>)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Find_m4225203534_MetadataUsageId;
extern "C"  bool List_1_Find_m4225203534_gshared (List_1_t3194695850 * __this, Predicate_1_t2268544833 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Find_m4225203534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool G_B3_0 = false;
	{
		Predicate_1_t2268544833 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2268544833 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t2268544833 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Predicate_1_t2268544833 * L_2 = ___match0;
		NullCheck((List_1_t3194695850 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t3194695850 *, int32_t, int32_t, Predicate_1_t2268544833 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t2268544833 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_002d;
		}
	}
	{
		BooleanU5BU5D_t3568034315* L_5 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		bool L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		G_B3_0 = L_8;
		goto IL_0036;
	}

IL_002d:
	{
		Initobj (Boolean_t3825574718_il2cpp_TypeInfo_var, (&V_1));
		bool L_9 = V_1;
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::CheckMatch(System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322341559;
extern const uint32_t List_1_CheckMatch_m1512395473_MetadataUsageId;
extern "C"  void List_1_CheckMatch_m1512395473_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2268544833 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckMatch_m1512395473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Predicate_1_t2268544833 * L_0 = ___match0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3322341559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3608752188_gshared (List_1_t3194695850 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2268544833 * ___match2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex0;
		int32_t L_1 = ___count1;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex0;
		V_1 = (int32_t)L_2;
		goto IL_0028;
	}

IL_000b:
	{
		Predicate_1_t2268544833 * L_3 = ___match2;
		BooleanU5BU5D_t3568034315* L_4 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		bool L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t2268544833 *)L_3);
		bool L_8 = ((  bool (*) (Predicate_1_t2268544833 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2268544833 *)L_3, (bool)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_8)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0024:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000b;
		}
	}
	{
		return (-1);
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Boolean>::GetEnumerator()
extern "C"  Enumerator_t2729425524  List_1_GetEnumerator_m2154818365_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2729425524  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1827850134(&L_0, (List_1_t3194695850 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1112750554_gshared (List_1_t3194695850 * __this, bool ___item0, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3568034315* L_0 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		bool L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, BooleanU5BU5D_t3568034315*, bool, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (BooleanU5BU5D_t3568034315*)L_0, (bool)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2997600557_gshared (List_1_t3194695850 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		BooleanU5BU5D_t3568034315* L_5 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_6 = ___start0;
		BooleanU5BU5D_t3568034315* L_7 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		BooleanU5BU5D_t3568034315* L_15 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_CheckIndex_m1558958008_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m1558958008_gshared (List_1_t3194695850 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m1558958008_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2048810847_gshared (List_1_t3194695850 * __this, int32_t ___index0, bool ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		BooleanU5BU5D_t3568034315* L_2 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		BooleanU5BU5D_t3568034315* L_4 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_5 = ___index0;
		bool L_6 = ___item1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (bool)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1063029886;
extern const uint32_t List_1_CheckCollection_m1722115714_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m1722115714_gshared (List_1_t3194695850 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m1722115714_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1063029886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::Remove(T)
extern "C"  bool List_1_Remove_m1880789723_gshared (List_1_t3194695850 * __this, bool ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = ___item0;
		NullCheck((List_1_t3194695850 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t3194695850 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m261710809_gshared (List_1_t3194695850 * __this, Predicate_1_t2268544833 * ___match0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Predicate_1_t2268544833 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2268544833 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t2268544833 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0011:
	{
		Predicate_1_t2268544833 * L_1 = ___match0;
		BooleanU5BU5D_t3568034315* L_2 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		bool L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck((Predicate_1_t2268544833 *)L_1);
		bool L_6 = ((  bool (*) (Predicate_1_t2268544833 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2268544833 *)L_1, (bool)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		goto IL_003d;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0011;
		}
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_004b;
		}
	}
	{
		return 0;
	}

IL_004b:
	{
		int32_t L_12 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_0099;
	}

IL_0062:
	{
		Predicate_1_t2268544833 * L_14 = ___match0;
		BooleanU5BU5D_t3568034315* L_15 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		bool L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck((Predicate_1_t2268544833 *)L_14);
		bool L_19 = ((  bool (*) (Predicate_1_t2268544833 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2268544833 *)L_14, (bool)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (L_19)
		{
			goto IL_0095;
		}
	}
	{
		BooleanU5BU5D_t3568034315* L_20 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_21 = V_0;
		int32_t L_22 = (int32_t)L_21;
		V_0 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
		BooleanU5BU5D_t3568034315* L_23 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		bool L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (bool)L_26);
	}

IL_0095:
	{
		int32_t L_27 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_28 = V_1;
		int32_t L_29 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_30 = V_1;
		int32_t L_31 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_30-(int32_t)L_31))) <= ((int32_t)0)))
		{
			goto IL_00bd;
		}
	}
	{
		BooleanU5BU5D_t3568034315* L_32 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_33 = V_0;
		int32_t L_34 = V_1;
		int32_t L_35 = V_0;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_32, (int32_t)L_33, (int32_t)((int32_t)((int32_t)L_34-(int32_t)L_35)), /*hidden argument*/NULL);
	}

IL_00bd:
	{
		int32_t L_36 = V_0;
		__this->set__size_2(L_36);
		int32_t L_37 = V_1;
		int32_t L_38 = V_0;
		return ((int32_t)((int32_t)L_37-(int32_t)L_38));
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_RemoveAt_m3516926931_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m3516926931_gshared (List_1_t3194695850 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m3516926931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		BooleanU5BU5D_t3568034315* L_5 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::Reverse()
extern "C"  void List_1_Reverse_m1100929517_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3568034315* L_0 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Array_Reverse_m3433347928(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)L_1, /*hidden argument*/NULL);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::Sort()
extern "C"  void List_1_Sort_m3884835435_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3568034315* L_0 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 32));
		Comparer_1_t2715583837 * L_2 = ((  Comparer_1_t2715583837 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		((  void (*) (Il2CppObject * /* static, unused */, BooleanU5BU5D_t3568034315*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (BooleanU5BU5D_t3568034315*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1807682591_gshared (List_1_t3194695850 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3568034315* L_0 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, BooleanU5BU5D_t3568034315*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (BooleanU5BU5D_t3568034315*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3554167432_gshared (List_1_t3194695850 * __this, Comparison_1_t792346273 * ___comparison0, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3568034315* L_0 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Comparison_1_t792346273 * L_2 = ___comparison0;
		((  void (*) (Il2CppObject * /* static, unused */, BooleanU5BU5D_t3568034315*, int32_t, Comparison_1_t792346273 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(NULL /*static, unused*/, (BooleanU5BU5D_t3568034315*)L_0, (int32_t)L_1, (Comparison_1_t792346273 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Boolean>::ToArray()
extern "C"  BooleanU5BU5D_t3568034315* List_1_ToArray_m1508874178_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	BooleanU5BU5D_t3568034315* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (BooleanU5BU5D_t3568034315*)((BooleanU5BU5D_t3568034315*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		BooleanU5BU5D_t3568034315* L_1 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		BooleanU5BU5D_t3568034315* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2363740072(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		BooleanU5BU5D_t3568034315* L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1336429574_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3318949796_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		BooleanU5BU5D_t3568034315* L_0 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m1190722713_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m1190722713_gshared (List_1_t3194695850 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m1190722713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m15523695(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		BooleanU5BU5D_t3568034315** L_3 = (BooleanU5BU5D_t3568034315**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, BooleanU5BU5D_t3568034315**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)(NULL /*static, unused*/, (BooleanU5BU5D_t3568034315**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::get_Count()
extern "C"  int32_t List_1_get_Count_m2965403065_gshared (List_1_t3194695850 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Boolean>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_get_Item_m152794991_MetadataUsageId;
extern "C"  bool List_1_get_Item_m152794991_gshared (List_1_t3194695850 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m152794991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		BooleanU5BU5D_t3568034315* L_3 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		bool L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Boolean>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_set_Item_m72704742_MetadataUsageId;
extern "C"  void List_1_set_Item_m72704742_gshared (List_1_t3194695850 * __this, int32_t ___index0, bool ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m72704742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3194695850 *)__this);
		((  void (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3194695850 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		BooleanU5BU5D_t3568034315* L_4 = (BooleanU5BU5D_t3568034315*)__this->get__items_1();
		int32_t L_5 = ___index0;
		bool L_6 = ___value1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (bool)L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void List_1__ctor_m3395850371_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		KeyValuePair_2U5BU5D_t2854920344* L_0 = ((List_1_t3702943073_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1780749362_gshared (List_1_t3702943073 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t3702943073 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		KeyValuePair_2U5BU5D_t2854920344* L_3 = ((List_1_t3702943073_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t3702943073 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((KeyValuePair_2U5BU5D_t2854920344*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t3702943073 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3404069002;
extern const uint32_t List_1__ctor_m766591248_MetadataUsageId;
extern "C"  void List_1__ctor_m766591248_gshared (List_1_t3702943073 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m766591248_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, (String_t*)_stringLiteral3404069002, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((KeyValuePair_2U5BU5D_t2854920344*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.cctor()
extern "C"  void List_1__cctor_m1434641296_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t3702943073_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((KeyValuePair_2U5BU5D_t2854920344*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3796738611_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3702943073 *)__this);
		Enumerator_t3237672747  L_0 = ((  Enumerator_t3237672747  (*) (List_1_t3702943073 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3702943073 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t3237672747  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2576612807_gshared (List_1_t3702943073 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2854920344* L_0 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1432929454_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3702943073 *)__this);
		Enumerator_t3237672747  L_0 = ((  Enumerator_t3237672747  (*) (List_1_t3702943073 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3702943073 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t3237672747  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Add_m247045711_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m247045711_gshared (List_1_t3702943073 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m247045711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3702943073 *)__this);
			((  void (*) (List_1_t3702943073 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t3702943073 *)__this, (KeyValuePair_2_t38854645 )((*(KeyValuePair_2_t38854645 *)((KeyValuePair_2_t38854645 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m2105487823_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m2105487823_gshared (List_1_t3702943073 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m2105487823_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3702943073 *)__this);
			bool L_1 = ((  bool (*) (List_1_t3702943073 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t3702943073 *)__this, (KeyValuePair_2_t38854645 )((*(KeyValuePair_2_t38854645 *)((KeyValuePair_2_t38854645 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m1189944129_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1189944129_gshared (List_1_t3702943073 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m1189944129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3702943073 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t3702943073 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t3702943073 *)__this, (KeyValuePair_2_t38854645 )((*(KeyValuePair_2_t38854645 *)((KeyValuePair_2_t38854645 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Insert_m628911364_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m628911364_gshared (List_1_t3702943073 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m628911364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t3702943073 *)__this);
			((  void (*) (List_1_t3702943073 *, int32_t, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_1, (KeyValuePair_2_t38854645 )((*(KeyValuePair_2_t38854645 *)((KeyValuePair_2_t38854645 *)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m1225249762_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m1225249762_gshared (List_1_t3702943073 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m1225249762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3702943073 *)__this);
			((  bool (*) (List_1_t3702943073 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t3702943073 *)__this, (KeyValuePair_2_t38854645 )((*(KeyValuePair_2_t38854645 *)((KeyValuePair_2_t38854645 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1135493682_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2666467511_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3129004603_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m4029492632_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3884900243_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2016766510_gshared (List_1_t3702943073 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3702943073 *)__this);
		KeyValuePair_2_t38854645  L_1 = ((  KeyValuePair_2_t38854645  (*) (List_1_t3702943073 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		KeyValuePair_2_t38854645  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t List_1_System_Collections_IList_set_Item_m2688890277_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m2688890277_gshared (List_1_t3702943073 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m2688890277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t3702943073 *)__this);
			((  void (*) (List_1_t3702943073 *, int32_t, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_0, (KeyValuePair_2_t38854645 )((*(KeyValuePair_2_t38854645 *)((KeyValuePair_2_t38854645 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(T)
extern "C"  void List_1_Add_m2976622552_gshared (List_1_t3702943073 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		KeyValuePair_2U5BU5D_t2854920344* L_1 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		KeyValuePair_2U5BU5D_t2854920344* L_2 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		KeyValuePair_2_t38854645  L_6 = ___item0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t38854645 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3802131643_gshared (List_1_t3702943073 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		KeyValuePair_2U5BU5D_t2854920344* L_3 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t3702943073 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t3702943073 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t3702943073 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1517435899_gshared (List_1_t3702943073 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		KeyValuePair_2U5BU5D_t2854920344* L_5 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t2854920344*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (KeyValuePair_2U5BU5D_t2854920344*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1675544139_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m1675544139_gshared (List_1_t3702943073 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1675544139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t38854645  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			KeyValuePair_2_t38854645  L_3 = InterfaceFuncInvoker0< KeyValuePair_2_t38854645  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (KeyValuePair_2_t38854645 )L_3;
			KeyValuePair_2_t38854645  L_4 = V_0;
			NullCheck((List_1_t3702943073 *)__this);
			((  void (*) (List_1_t3702943073 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t3702943073 *)__this, (KeyValuePair_2_t38854645 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2338930750_gshared (List_1_t3702943073 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t3702943073 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t3702943073 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t3702943073 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t224640337 * List_1_AsReadOnly_m4081447803_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t224640337 * L_0 = (ReadOnlyCollection_1_t224640337 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (ReadOnlyCollection_1_t224640337 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_0, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C"  void List_1_Clear_m4050912532_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2854920344* L_0 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		KeyValuePair_2U5BU5D_t2854920344* L_1 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(T)
extern "C"  bool List_1_Contains_m540253150_gshared (List_1_t3702943073 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2854920344* L_0 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		KeyValuePair_2_t38854645  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t2854920344*, KeyValuePair_2_t38854645 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (KeyValuePair_2U5BU5D_t2854920344*)L_0, (KeyValuePair_2_t38854645 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2558127868_gshared (List_1_t3702943073 * __this, KeyValuePair_2U5BU5D_t2854920344* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2854920344* L_0 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		KeyValuePair_2U5BU5D_t2854920344* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Find(System.Predicate`1<T>)
extern Il2CppClass* KeyValuePair_2_t38854645_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Find_m3414479938_MetadataUsageId;
extern "C"  KeyValuePair_2_t38854645  List_1_Find_m3414479938_gshared (List_1_t3702943073 * __this, Predicate_1_t2776792056 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Find_m3414479938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t38854645  V_1;
	memset(&V_1, 0, sizeof(V_1));
	KeyValuePair_2_t38854645  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		Predicate_1_t2776792056 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2776792056 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t2776792056 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Predicate_1_t2776792056 * L_2 = ___match0;
		NullCheck((List_1_t3702943073 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t3702943073 *, int32_t, int32_t, Predicate_1_t2776792056 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t2776792056 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_002d;
		}
	}
	{
		KeyValuePair_2U5BU5D_t2854920344* L_5 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		KeyValuePair_2_t38854645  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		G_B3_0 = L_8;
		goto IL_0036;
	}

IL_002d:
	{
		Initobj (KeyValuePair_2_t38854645_il2cpp_TypeInfo_var, (&V_1));
		KeyValuePair_2_t38854645  L_9 = V_1;
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckMatch(System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322341559;
extern const uint32_t List_1_CheckMatch_m956871711_MetadataUsageId;
extern "C"  void List_1_CheckMatch_m956871711_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2776792056 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckMatch_m956871711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Predicate_1_t2776792056 * L_0 = ___match0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3322341559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1833930924_gshared (List_1_t3702943073 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2776792056 * ___match2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex0;
		int32_t L_1 = ___count1;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex0;
		V_1 = (int32_t)L_2;
		goto IL_0028;
	}

IL_000b:
	{
		Predicate_1_t2776792056 * L_3 = ___match2;
		KeyValuePair_2U5BU5D_t2854920344* L_4 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		KeyValuePair_2_t38854645  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t2776792056 *)L_3);
		bool L_8 = ((  bool (*) (Predicate_1_t2776792056 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2776792056 *)L_3, (KeyValuePair_2_t38854645 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_8)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0024:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000b;
		}
	}
	{
		return (-1);
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C"  Enumerator_t3237672747  List_1_GetEnumerator_m4207298099_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3237672747  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1246468418(&L_0, (List_1_t3702943073 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m151931218_gshared (List_1_t3702943073 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2854920344* L_0 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		KeyValuePair_2_t38854645  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t2854920344*, KeyValuePair_2_t38854645 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (KeyValuePair_2U5BU5D_t2854920344*)L_0, (KeyValuePair_2_t38854645 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m90124003_gshared (List_1_t3702943073 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		KeyValuePair_2U5BU5D_t2854920344* L_5 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_6 = ___start0;
		KeyValuePair_2U5BU5D_t2854920344* L_7 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		KeyValuePair_2U5BU5D_t2854920344* L_15 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_CheckIndex_m922099068_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m922099068_gshared (List_1_t3702943073 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m922099068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2036500065_gshared (List_1_t3702943073 * __this, int32_t ___index0, KeyValuePair_2_t38854645  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		KeyValuePair_2U5BU5D_t2854920344* L_2 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		KeyValuePair_2U5BU5D_t2854920344* L_4 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_5 = ___index0;
		KeyValuePair_2_t38854645  L_6 = ___item1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t38854645 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1063029886;
extern const uint32_t List_1_CheckCollection_m478305602_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m478305602_gshared (List_1_t3702943073 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m478305602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1063029886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(T)
extern "C"  bool List_1_Remove_m4074802193_gshared (List_1_t3702943073 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		KeyValuePair_2_t38854645  L_0 = ___item0;
		NullCheck((List_1_t3702943073 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t3702943073 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t3702943073 *)__this, (KeyValuePair_2_t38854645 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3792437967_gshared (List_1_t3702943073 * __this, Predicate_1_t2776792056 * ___match0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Predicate_1_t2776792056 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2776792056 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t2776792056 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0011:
	{
		Predicate_1_t2776792056 * L_1 = ___match0;
		KeyValuePair_2U5BU5D_t2854920344* L_2 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		KeyValuePair_2_t38854645  L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck((Predicate_1_t2776792056 *)L_1);
		bool L_6 = ((  bool (*) (Predicate_1_t2776792056 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2776792056 *)L_1, (KeyValuePair_2_t38854645 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		goto IL_003d;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0011;
		}
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_004b;
		}
	}
	{
		return 0;
	}

IL_004b:
	{
		int32_t L_12 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_0099;
	}

IL_0062:
	{
		Predicate_1_t2776792056 * L_14 = ___match0;
		KeyValuePair_2U5BU5D_t2854920344* L_15 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		KeyValuePair_2_t38854645  L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck((Predicate_1_t2776792056 *)L_14);
		bool L_19 = ((  bool (*) (Predicate_1_t2776792056 *, KeyValuePair_2_t38854645 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2776792056 *)L_14, (KeyValuePair_2_t38854645 )L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (L_19)
		{
			goto IL_0095;
		}
	}
	{
		KeyValuePair_2U5BU5D_t2854920344* L_20 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_21 = V_0;
		int32_t L_22 = (int32_t)L_21;
		V_0 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
		KeyValuePair_2U5BU5D_t2854920344* L_23 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		KeyValuePair_2_t38854645  L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (KeyValuePair_2_t38854645 )L_26);
	}

IL_0095:
	{
		int32_t L_27 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_28 = V_1;
		int32_t L_29 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_30 = V_1;
		int32_t L_31 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_30-(int32_t)L_31))) <= ((int32_t)0)))
		{
			goto IL_00bd;
		}
	}
	{
		KeyValuePair_2U5BU5D_t2854920344* L_32 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_33 = V_0;
		int32_t L_34 = V_1;
		int32_t L_35 = V_0;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_32, (int32_t)L_33, (int32_t)((int32_t)((int32_t)L_34-(int32_t)L_35)), /*hidden argument*/NULL);
	}

IL_00bd:
	{
		int32_t L_36 = V_0;
		__this->set__size_2(L_36);
		int32_t L_37 = V_1;
		int32_t L_38 = V_0;
		return ((int32_t)((int32_t)L_37-(int32_t)L_38));
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_RemoveAt_m4120265373_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m4120265373_gshared (List_1_t3702943073 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m4120265373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		KeyValuePair_2U5BU5D_t2854920344* L_5 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Reverse()
extern "C"  void List_1_Reverse_m3795954547_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2854920344* L_0 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Array_Reverse_m3433347928(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)L_1, /*hidden argument*/NULL);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort()
extern "C"  void List_1_Sort_m2868944585_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2854920344* L_0 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 32));
		Comparer_1_t3223831060 * L_2 = ((  Comparer_1_t3223831060 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		((  void (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t2854920344*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (KeyValuePair_2U5BU5D_t2854920344*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3900980533_gshared (List_1_t3702943073 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2854920344* L_0 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t2854920344*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (KeyValuePair_2U5BU5D_t2854920344*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m4142558040_gshared (List_1_t3702943073 * __this, Comparison_1_t1300593496 * ___comparison0, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2854920344* L_0 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Comparison_1_t1300593496 * L_2 = ___comparison0;
		((  void (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t2854920344*, int32_t, Comparison_1_t1300593496 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(NULL /*static, unused*/, (KeyValuePair_2U5BU5D_t2854920344*)L_0, (int32_t)L_1, (Comparison_1_t1300593496 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t2854920344* List_1_ToArray_m3936280286_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	KeyValuePair_2U5BU5D_t2854920344* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (KeyValuePair_2U5BU5D_t2854920344*)((KeyValuePair_2U5BU5D_t2854920344*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		KeyValuePair_2U5BU5D_t2854920344* L_1 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		KeyValuePair_2U5BU5D_t2854920344* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2363740072(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		KeyValuePair_2U5BU5D_t2854920344* L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TrimExcess()
extern "C"  void List_1_TrimExcess_m969488990_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m115729064_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t2854920344* L_0 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m975330075_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m975330075_gshared (List_1_t3702943073 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m975330075_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m15523695(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		KeyValuePair_2U5BU5D_t2854920344** L_3 = (KeyValuePair_2U5BU5D_t2854920344**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t2854920344**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)(NULL /*static, unused*/, (KeyValuePair_2U5BU5D_t2854920344**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C"  int32_t List_1_get_Count_m1224543263_gshared (List_1_t3702943073 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_get_Item_m16937777_MetadataUsageId;
extern "C"  KeyValuePair_2_t38854645  List_1_get_Item_m16937777_gshared (List_1_t3702943073 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m16937777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		KeyValuePair_2U5BU5D_t2854920344* L_3 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		KeyValuePair_2_t38854645  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_set_Item_m1452453998_MetadataUsageId;
extern "C"  void List_1_set_Item_m1452453998_gshared (List_1_t3702943073 * __this, int32_t ___index0, KeyValuePair_2_t38854645  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m1452453998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3702943073 *)__this);
		((  void (*) (List_1_t3702943073 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3702943073 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		KeyValuePair_2U5BU5D_t2854920344* L_4 = (KeyValuePair_2U5BU5D_t2854920344*)__this->get__items_1();
		int32_t L_5 = ___index0;
		KeyValuePair_2_t38854645  L_6 = ___value1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t38854645 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m1017230911_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Int32U5BU5D_t3030399641* L_0 = ((List_1_t1440998580_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m87208054_gshared (List_1_t1440998580 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t1440998580 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		Int32U5BU5D_t3030399641* L_3 = ((List_1_t1440998580_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t1440998580 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((Int32U5BU5D_t3030399641*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t1440998580 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3404069002;
extern const uint32_t List_1__ctor_m2475747412_MetadataUsageId;
extern "C"  void List_1__ctor_m2475747412_gshared (List_1_t1440998580 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m2475747412_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, (String_t*)_stringLiteral3404069002, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((Int32U5BU5D_t3030399641*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C"  void List_1__cctor_m2189212316_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t1440998580_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((Int32U5BU5D_t3030399641*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2389584935_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1440998580 *)__this);
		Enumerator_t975728254  L_0 = ((  Enumerator_t975728254  (*) (List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1440998580 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t975728254  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m99573371_gshared (List_1_t1440998580 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2119276738_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1440998580 *)__this);
		Enumerator_t975728254  L_0 = ((  Enumerator_t975728254  (*) (List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t1440998580 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t975728254  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Add_m4110675067_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m4110675067_gshared (List_1_t1440998580 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m4110675067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1440998580 *)__this);
			((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m1798539219_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m1798539219_gshared (List_1_t1440998580 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m1798539219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1440998580 *)__this);
			bool L_1 = ((  bool (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m39706221_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m39706221_gshared (List_1_t1440998580 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m39706221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1440998580 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Insert_m3497683264_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m3497683264_gshared (List_1_t1440998580 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m3497683264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t1440998580 *)__this);
			((  void (*) (List_1_t1440998580 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_1, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m733406822_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m733406822_gshared (List_1_t1440998580 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m733406822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1440998580 *)__this);
			((  bool (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2370098094_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m180248307_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3733894943_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m899572676_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m813208831_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2850581314_gshared (List_1_t1440998580 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1440998580 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t List_1_System_Collections_IList_set_Item_m4222864089_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m4222864089_gshared (List_1_t1440998580 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m4222864089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t1440998580 *)__this);
			((  void (*) (List_1_t1440998580 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_0, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C"  void List_1_Add_m2828939739_gshared (List_1_t1440998580 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		Int32U5BU5D_t3030399641* L_1 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		Int32U5BU5D_t3030399641* L_2 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		int32_t L_6 = ___item0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2986672263_gshared (List_1_t1440998580 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		Int32U5BU5D_t3030399641* L_3 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t1440998580 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t1440998580 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m389745455_gshared (List_1_t1440998580 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		Int32U5BU5D_t3030399641* L_5 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< Int32U5BU5D_t3030399641*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Int32>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (Int32U5BU5D_t3030399641*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1869508559_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m1869508559_gshared (List_1_t1440998580 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1869508559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (int32_t)L_3;
			int32_t L_4 = V_0;
			NullCheck((List_1_t1440998580 *)__this);
			((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2567809379_gshared (List_1_t1440998580 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t1440998580 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t1440998580 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t1440998580 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Int32>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2257663140 * List_1_AsReadOnly_m3556741007_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t2257663140 * L_0 = (ReadOnlyCollection_1_t2257663140 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (ReadOnlyCollection_1_t2257663140 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_0, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C"  void List_1_Clear_m3644677550_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		Int32U5BU5D_t3030399641* L_1 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C"  bool List_1_Contains_m459703010_gshared (List_1_t1440998580 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3030399641*, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t3030399641*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2021584896_gshared (List_1_t1440998580 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		Int32U5BU5D_t3030399641* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// T System.Collections.Generic.List`1<System.Int32>::Find(System.Predicate`1<T>)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Find_m4088861214_MetadataUsageId;
extern "C"  int32_t List_1_Find_m4088861214_gshared (List_1_t1440998580 * __this, Predicate_1_t514847563 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Find_m4088861214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		Predicate_1_t514847563 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t514847563 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t514847563 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Predicate_1_t514847563 * L_2 = ___match0;
		NullCheck((List_1_t1440998580 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t1440998580 *, int32_t, int32_t, Predicate_1_t514847563 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t514847563 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_002d;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_5 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		int32_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		G_B3_0 = L_8;
		goto IL_0036;
	}

IL_002d:
	{
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_1));
		int32_t L_9 = V_1;
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckMatch(System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322341559;
extern const uint32_t List_1_CheckMatch_m2715809755_MetadataUsageId;
extern "C"  void List_1_CheckMatch_m2715809755_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t514847563 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckMatch_m2715809755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Predicate_1_t514847563 * L_0 = ___match0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3322341559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m4030875800_gshared (List_1_t1440998580 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t514847563 * ___match2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex0;
		int32_t L_1 = ___count1;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex0;
		V_1 = (int32_t)L_2;
		goto IL_0028;
	}

IL_000b:
	{
		Predicate_1_t514847563 * L_3 = ___match2;
		Int32U5BU5D_t3030399641* L_4 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t514847563 *)L_3);
		bool L_8 = ((  bool (*) (Predicate_1_t514847563 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t514847563 *)L_3, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_8)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0024:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000b;
		}
	}
	{
		return (-1);
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t975728254  List_1_GetEnumerator_m444823791_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		Enumerator_t975728254  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m1614742070(&L_0, (List_1_t1440998580 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3529832102_gshared (List_1_t1440998580 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3030399641*, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t3030399641*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2880167903_gshared (List_1_t1440998580 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_5 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_6 = ___start0;
		Int32U5BU5D_t3030399641* L_7 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_15 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_CheckIndex_m3609163576_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m3609163576_gshared (List_1_t1440998580 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m3609163576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2493743341_gshared (List_1_t1440998580 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Int32U5BU5D_t3030399641* L_2 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		Int32U5BU5D_t3030399641* L_4 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___item1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1063029886;
extern const uint32_t List_1_CheckCollection_m2486007558_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m2486007558_gshared (List_1_t1440998580 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m2486007558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1063029886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C"  bool List_1_Remove_m2616693989_gshared (List_1_t1440998580 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___item0;
		NullCheck((List_1_t1440998580 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2964742291_gshared (List_1_t1440998580 * __this, Predicate_1_t514847563 * ___match0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Predicate_1_t514847563 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t514847563 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t514847563 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0011:
	{
		Predicate_1_t514847563 * L_1 = ___match0;
		Int32U5BU5D_t3030399641* L_2 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck((Predicate_1_t514847563 *)L_1);
		bool L_6 = ((  bool (*) (Predicate_1_t514847563 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t514847563 *)L_1, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		goto IL_003d;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0011;
		}
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_004b;
		}
	}
	{
		return 0;
	}

IL_004b:
	{
		int32_t L_12 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_0099;
	}

IL_0062:
	{
		Predicate_1_t514847563 * L_14 = ___match0;
		Int32U5BU5D_t3030399641* L_15 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck((Predicate_1_t514847563 *)L_14);
		bool L_19 = ((  bool (*) (Predicate_1_t514847563 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t514847563 *)L_14, (int32_t)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (L_19)
		{
			goto IL_0095;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_20 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_21 = V_0;
		int32_t L_22 = (int32_t)L_21;
		V_0 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
		Int32U5BU5D_t3030399641* L_23 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		int32_t L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (int32_t)L_26);
	}

IL_0095:
	{
		int32_t L_27 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_28 = V_1;
		int32_t L_29 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_30 = V_1;
		int32_t L_31 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_30-(int32_t)L_31))) <= ((int32_t)0)))
		{
			goto IL_00bd;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_32 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_33 = V_0;
		int32_t L_34 = V_1;
		int32_t L_35 = V_0;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_32, (int32_t)L_33, (int32_t)((int32_t)((int32_t)L_34-(int32_t)L_35)), /*hidden argument*/NULL);
	}

IL_00bd:
	{
		int32_t L_36 = V_0;
		__this->set__size_2(L_36);
		int32_t L_37 = V_1;
		int32_t L_38 = V_0;
		return ((int32_t)((int32_t)L_37-(int32_t)L_38));
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_RemoveAt_m1644402641_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m1644402641_gshared (List_1_t1440998580 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m1644402641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		Int32U5BU5D_t3030399641* L_5 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Reverse()
extern "C"  void List_1_Reverse_m369022463_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Array_Reverse_m3433347928(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)L_1, /*hidden argument*/NULL);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort()
extern "C"  void List_1_Sort_m953537285_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 32));
		Comparer_1_t961886567 * L_2 = ((  Comparer_1_t961886567 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3030399641*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t3030399641*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3499938865_gshared (List_1_t1440998580 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3030399641*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t3030399641*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1518807012_gshared (List_1_t1440998580 * __this, Comparison_1_t3333616299 * ___comparison0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Comparison_1_t3333616299 * L_2 = ___comparison0;
		((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3030399641*, int32_t, Comparison_1_t3333616299 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t3030399641*)L_0, (int32_t)L_1, (Comparison_1_t3333616299 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C"  Int32U5BU5D_t3030399641* List_1_ToArray_m3223175690_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	Int32U5BU5D_t3030399641* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (Int32U5BU5D_t3030399641*)((Int32U5BU5D_t3030399641*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		Int32U5BU5D_t3030399641* L_1 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		Int32U5BU5D_t3030399641* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2363740072(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		Int32U5BU5D_t3030399641* L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::TrimExcess()
extern "C"  void List_1_TrimExcess_m4133698154_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m531373308_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m1511847951_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m1511847951_gshared (List_1_t1440998580 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m1511847951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m15523695(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		Int32U5BU5D_t3030399641** L_3 = (Int32U5BU5D_t3030399641**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t3030399641**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)(NULL /*static, unused*/, (Int32U5BU5D_t3030399641**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C"  int32_t List_1_get_Count_m852068579_gshared (List_1_t1440998580 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_get_Item_m4100789973_MetadataUsageId;
extern "C"  int32_t List_1_get_Item_m4100789973_gshared (List_1_t1440998580 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m4100789973_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		Int32U5BU5D_t3030399641* L_3 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_set_Item_m1852089066_MetadataUsageId;
extern "C"  void List_1_set_Item_m1852089066_gshared (List_1_t1440998580 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m1852089066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1440998580 *)__this);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1440998580 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		Int32U5BU5D_t3030399641* L_4 = (Int32U5BU5D_t3030399641*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___value1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m365405030_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_0 = ((List_1_t2058570427_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m454375187_gshared (List_1_t2058570427 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_3 = ((List_1_t2058570427_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3404069002;
extern const uint32_t List_1__ctor_m136460305_MetadataUsageId;
extern "C"  void List_1__ctor_m136460305_gshared (List_1_t2058570427 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m136460305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, (String_t*)_stringLiteral3404069002, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C"  void List_1__cctor_m138621019_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t2058570427_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t2058570427 *)__this);
		Enumerator_t1593300101  L_0 = ((  Enumerator_t1593300101  (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2058570427 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t1593300101  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared (List_1_t2058570427 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t2058570427 *)__this);
		Enumerator_t1593300101  L_0 = ((  Enumerator_t1593300101  (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t2058570427 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t1593300101  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Add_m1765626550_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m1765626550_gshared (List_1_t2058570427 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m1765626550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t2058570427 *)__this);
			((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m149594880_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m149594880_gshared (List_1_t2058570427 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m149594880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t2058570427 *)__this);
			bool L_1 = ((  bool (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m406088260_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m406088260_gshared (List_1_t2058570427 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m406088260_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t2058570427 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Insert_m3961795241_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m3961795241_gshared (List_1_t2058570427 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m3961795241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t2058570427 *)__this);
			((  void (*) (List_1_t2058570427 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_1, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m3415450529_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m3415450529_gshared (List_1_t2058570427 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m3415450529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t2058570427 *)__this);
			((  bool (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m936612973_gshared (List_1_t2058570427 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2058570427 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return L_1;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t List_1_System_Collections_IList_set_Item_m162109184_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m162109184_gshared (List_1_t2058570427 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m162109184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t2058570427 *)__this);
			((  void (*) (List_1_t2058570427 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C"  void List_1_Add_m567051994_gshared (List_1_t2058570427 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		Il2CppObject * L_6 = ___item0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m185971996_gshared (List_1_t2058570427 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t2058570427 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t2058570427 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1580067148_gshared (List_1_t2058570427 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< ObjectU5BU5D_t3614634134*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (ObjectU5BU5D_t3614634134*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m2489692396_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m2489692396_gshared (List_1_t2058570427 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m2489692396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (Il2CppObject *)L_3;
			Il2CppObject * L_4 = V_0;
			NullCheck((List_1_t2058570427 *)__this);
			((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3614127065_gshared (List_1_t2058570427 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2875234987 * List_1_AsReadOnly_m2563000362_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t2875234987 * L_0 = (ReadOnlyCollection_1_t2875234987 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (ReadOnlyCollection_1_t2875234987 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_0, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C"  bool List_1_Contains_m2577748987_gshared (List_1_t2058570427 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1758262197_gshared (List_1_t2058570427 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Find_m1725159095_MetadataUsageId;
extern "C"  Il2CppObject * List_1_Find_m1725159095_gshared (List_1_t2058570427 * __this, Predicate_1_t1132419410 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Find_m1725159095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * G_B3_0 = NULL;
	{
		Predicate_1_t1132419410 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1132419410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t1132419410 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Predicate_1_t1132419410 * L_2 = ___match0;
		NullCheck((List_1_t2058570427 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t2058570427 *, int32_t, int32_t, Predicate_1_t1132419410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t1132419410 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_002d;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		G_B3_0 = L_8;
		goto IL_0036;
	}

IL_002d:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_9 = V_1;
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322341559;
extern const uint32_t List_1_CheckMatch_m1196994270_MetadataUsageId;
extern "C"  void List_1_CheckMatch_m1196994270_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1132419410 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckMatch_m1196994270_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Predicate_1_t1132419410 * L_0 = ___match0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3322341559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3409004147_gshared (List_1_t2058570427 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1132419410 * ___match2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex0;
		int32_t L_1 = ___count1;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex0;
		V_1 = (int32_t)L_2;
		goto IL_0028;
	}

IL_000b:
	{
		Predicate_1_t1132419410 * L_3 = ___match2;
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t1132419410 *)L_3);
		bool L_8 = ((  bool (*) (Predicate_1_t1132419410 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t1132419410 *)L_3, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_8)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0024:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000b;
		}
	}
	{
		return (-1);
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m3294992758_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1593300101  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m3769601633(&L_0, (List_1_t2058570427 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2070479489_gshared (List_1_t2058570427 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3137156970_gshared (List_1_t2058570427 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_6 = ___start0;
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_CheckIndex_m524615377_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m524615377_gshared (List_1_t2058570427 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m524615377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m11735664_gshared (List_1_t2058570427 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_5 = ___index0;
		Il2CppObject * L_6 = ___item1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1063029886;
extern const uint32_t List_1_CheckCollection_m3968030679_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m3968030679_gshared (List_1_t2058570427 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m3968030679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1063029886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C"  bool List_1_Remove_m1271859478_gshared (List_1_t2058570427 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t2058570427 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2058570427 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2972055270_gshared (List_1_t2058570427 * __this, Predicate_1_t1132419410 * ___match0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Predicate_1_t1132419410 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1132419410 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t1132419410 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0011:
	{
		Predicate_1_t1132419410 * L_1 = ___match0;
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck((Predicate_1_t1132419410 *)L_1);
		bool L_6 = ((  bool (*) (Predicate_1_t1132419410 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t1132419410 *)L_1, (Il2CppObject *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		goto IL_003d;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0011;
		}
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_004b;
		}
	}
	{
		return 0;
	}

IL_004b:
	{
		int32_t L_12 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_0099;
	}

IL_0062:
	{
		Predicate_1_t1132419410 * L_14 = ___match0;
		ObjectU5BU5D_t3614634134* L_15 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		Il2CppObject * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck((Predicate_1_t1132419410 *)L_14);
		bool L_19 = ((  bool (*) (Predicate_1_t1132419410 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t1132419410 *)L_14, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (L_19)
		{
			goto IL_0095;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_20 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_21 = V_0;
		int32_t L_22 = (int32_t)L_21;
		V_0 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
		ObjectU5BU5D_t3614634134* L_23 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		Il2CppObject * L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (Il2CppObject *)L_26);
	}

IL_0095:
	{
		int32_t L_27 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_28 = V_1;
		int32_t L_29 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_30 = V_1;
		int32_t L_31 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_30-(int32_t)L_31))) <= ((int32_t)0)))
		{
			goto IL_00bd;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_32 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_33 = V_0;
		int32_t L_34 = V_1;
		int32_t L_35 = V_0;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_32, (int32_t)L_33, (int32_t)((int32_t)((int32_t)L_34-(int32_t)L_35)), /*hidden argument*/NULL);
	}

IL_00bd:
	{
		int32_t L_36 = V_0;
		__this->set__size_2(L_36);
		int32_t L_37 = V_1;
		int32_t L_38 = V_0;
		return ((int32_t)((int32_t)L_37-(int32_t)L_38));
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_RemoveAt_m3615096820_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m3615096820_gshared (List_1_t2058570427 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m3615096820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		ObjectU5BU5D_t3614634134* L_5 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C"  void List_1_Reverse_m4038478200_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Array_Reverse_m3433347928(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)L_1, /*hidden argument*/NULL);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C"  void List_1_Sort_m554162636_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 32));
		Comparer_1_t1579458414 * L_2 = ((  Comparer_1_t1579458414 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1776685136_gshared (List_1_t2058570427 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m785723827_gshared (List_1_t2058570427 * __this, Comparison_1_t3951188146 * ___comparison0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Comparison_1_t3951188146 * L_2 = ___comparison0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, int32_t, Comparison_1_t3951188146 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (int32_t)L_1, (Comparison_1_t3951188146 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t3614634134* List_1_ToArray_m546658539_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		ObjectU5BU5D_t3614634134* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2363740072(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1944241237_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3133733835_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m491101164_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m491101164_gshared (List_1_t2058570427 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m491101164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m15523695(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		ObjectU5BU5D_t3614634134** L_3 = (ObjectU5BU5D_t3614634134**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_get_Item_m1354830498_MetadataUsageId;
extern "C"  Il2CppObject * List_1_get_Item_m1354830498_gshared (List_1_t2058570427 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m1354830498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_set_Item_m4128108021_MetadataUsageId;
extern "C"  void List_1_set_Item_m4128108021_gshared (List_1_t2058570427 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m4128108021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t2058570427 *)__this);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2058570427 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get__items_1();
		int32_t L_5 = ___index0;
		Il2CppObject * L_6 = ___value1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void List_1__ctor_m62665571_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = ((List_1_t3758245971_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3395220262_gshared (List_1_t3758245971 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t3758245971 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = ((List_1_t3758245971_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t3758245971 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((CustomAttributeNamedArgumentU5BU5D_t3304067486*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t3758245971 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3404069002;
extern const uint32_t List_1__ctor_m2814377392_MetadataUsageId;
extern "C"  void List_1__ctor_m2814377392_gshared (List_1_t3758245971 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m2814377392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, (String_t*)_stringLiteral3404069002, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((CustomAttributeNamedArgumentU5BU5D_t3304067486*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C"  void List_1__cctor_m2406694916_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t3758245971_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((CustomAttributeNamedArgumentU5BU5D_t3304067486*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3911881107_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3758245971 *)__this);
		Enumerator_t3292975645  L_0 = ((  Enumerator_t3292975645  (*) (List_1_t3758245971 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3758245971 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t3292975645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m238914391_gshared (List_1_t3758245971 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2711440510_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3758245971 *)__this);
		Enumerator_t3292975645  L_0 = ((  Enumerator_t3292975645  (*) (List_1_t3758245971 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t3758245971 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t3292975645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Add_m2467317711_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m2467317711_gshared (List_1_t3758245971 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m2467317711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3758245971 *)__this);
			((  void (*) (List_1_t3758245971 *, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t3758245971 *)__this, (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m1445741711_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m1445741711_gshared (List_1_t3758245971 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m1445741711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3758245971 *)__this);
			bool L_1 = ((  bool (*) (List_1_t3758245971 *, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t3758245971 *)__this, (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m3337681989_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3337681989_gshared (List_1_t3758245971 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m3337681989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3758245971 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t3758245971 *, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t3758245971 *)__this, (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Insert_m2411507172_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m2411507172_gshared (List_1_t3758245971 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m2411507172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t3758245971 *)__this);
			((  void (*) (List_1_t3758245971 *, int32_t, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_1, (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m757548498_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m757548498_gshared (List_1_t3758245971 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m757548498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3758245971 *)__this);
			((  bool (*) (List_1_t3758245971 *, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t3758245971 *)__this, (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3598018290_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m42432439_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3463435867_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1122077912_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3489886467_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2717017342_gshared (List_1_t3758245971 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3758245971 *)__this);
		CustomAttributeNamedArgument_t94157543  L_1 = ((  CustomAttributeNamedArgument_t94157543  (*) (List_1_t3758245971 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		CustomAttributeNamedArgument_t94157543  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t List_1_System_Collections_IList_set_Item_m2322597873_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m2322597873_gshared (List_1_t3758245971 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m2322597873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t3758245971 *)__this);
			((  void (*) (List_1_t3758245971 *, int32_t, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t94157543 )((*(CustomAttributeNamedArgument_t94157543 *)((CustomAttributeNamedArgument_t94157543 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void List_1_Add_m1421473272_gshared (List_1_t3758245971 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_2 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		CustomAttributeNamedArgument_t94157543  L_6 = ___item0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t94157543 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1884976939_gshared (List_1_t3758245971 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t3758245971 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t3758245971 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t3758245971 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m4288303131_gshared (List_1_t3758245971 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< CustomAttributeNamedArgumentU5BU5D_t3304067486*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m2240424635_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m2240424635_gshared (List_1_t3758245971 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m2240424635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CustomAttributeNamedArgument_t94157543  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			CustomAttributeNamedArgument_t94157543  L_3 = InterfaceFuncInvoker0< CustomAttributeNamedArgument_t94157543  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (CustomAttributeNamedArgument_t94157543 )L_3;
			CustomAttributeNamedArgument_t94157543  L_4 = V_0;
			NullCheck((List_1_t3758245971 *)__this);
			((  void (*) (List_1_t3758245971 *, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t3758245971 *)__this, (CustomAttributeNamedArgument_t94157543 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m550906382_gshared (List_1_t3758245971 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t3758245971 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t3758245971 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t3758245971 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t279943235 * List_1_AsReadOnly_m4170173499_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t279943235 * L_0 = (ReadOnlyCollection_1_t279943235 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (ReadOnlyCollection_1_t279943235 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_0, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void List_1_Clear_m872023540_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool List_1_Contains_m2579468898_gshared (List_1_t3758245971 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3304934364_gshared (List_1_t3758245971 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Find(System.Predicate`1<T>)
extern Il2CppClass* CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Find_m928764838_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t94157543  List_1_Find_m928764838_gshared (List_1_t3758245971 * __this, Predicate_1_t2832094954 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Find_m928764838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CustomAttributeNamedArgument_t94157543  V_1;
	memset(&V_1, 0, sizeof(V_1));
	CustomAttributeNamedArgument_t94157543  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		Predicate_1_t2832094954 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2832094954 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t2832094954 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Predicate_1_t2832094954 * L_2 = ___match0;
		NullCheck((List_1_t3758245971 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t3758245971 *, int32_t, int32_t, Predicate_1_t2832094954 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t2832094954 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_002d;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		CustomAttributeNamedArgument_t94157543  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		G_B3_0 = L_8;
		goto IL_0036;
	}

IL_002d:
	{
		Initobj (CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var, (&V_1));
		CustomAttributeNamedArgument_t94157543  L_9 = V_1;
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckMatch(System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322341559;
extern const uint32_t List_1_CheckMatch_m1772343151_MetadataUsageId;
extern "C"  void List_1_CheckMatch_m1772343151_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2832094954 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckMatch_m1772343151_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Predicate_1_t2832094954 * L_0 = ___match0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3322341559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3484731440_gshared (List_1_t3758245971 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2832094954 * ___match2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex0;
		int32_t L_1 = ___count1;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex0;
		V_1 = (int32_t)L_2;
		goto IL_0028;
	}

IL_000b:
	{
		Predicate_1_t2832094954 * L_3 = ___match2;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_4 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		CustomAttributeNamedArgument_t94157543  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t2832094954 *)L_3);
		bool L_8 = ((  bool (*) (Predicate_1_t2832094954 *, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2832094954 *)L_3, (CustomAttributeNamedArgument_t94157543 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_8)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0024:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000b;
		}
	}
	{
		return (-1);
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Enumerator_t3292975645  List_1_GetEnumerator_m1960030979_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3292975645  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m3021143890(&L_0, (List_1_t3758245971 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3773642130_gshared (List_1_t3758245971 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3131270387_gshared (List_1_t3758245971 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_6 = ___start0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_7 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_15 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_CheckIndex_m2328469916_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m2328469916_gshared (List_1_t3758245971 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2328469916_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2347446741_gshared (List_1_t3758245971 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_2 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_4 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeNamedArgument_t94157543  L_6 = ___item1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t94157543 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1063029886;
extern const uint32_t List_1_CheckCollection_m702424990_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m702424990_gshared (List_1_t3758245971 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m702424990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1063029886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool List_1_Remove_m600476045_gshared (List_1_t3758245971 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeNamedArgument_t94157543  L_0 = ___item0;
		NullCheck((List_1_t3758245971 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t3758245971 *, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t3758245971 *)__this, (CustomAttributeNamedArgument_t94157543 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1556422543_gshared (List_1_t3758245971 * __this, Predicate_1_t2832094954 * ___match0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Predicate_1_t2832094954 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2832094954 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t2832094954 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0011:
	{
		Predicate_1_t2832094954 * L_1 = ___match0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_2 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		CustomAttributeNamedArgument_t94157543  L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck((Predicate_1_t2832094954 *)L_1);
		bool L_6 = ((  bool (*) (Predicate_1_t2832094954 *, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2832094954 *)L_1, (CustomAttributeNamedArgument_t94157543 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		goto IL_003d;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0011;
		}
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_004b;
		}
	}
	{
		return 0;
	}

IL_004b:
	{
		int32_t L_12 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_0099;
	}

IL_0062:
	{
		Predicate_1_t2832094954 * L_14 = ___match0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_15 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		CustomAttributeNamedArgument_t94157543  L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck((Predicate_1_t2832094954 *)L_14);
		bool L_19 = ((  bool (*) (Predicate_1_t2832094954 *, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t2832094954 *)L_14, (CustomAttributeNamedArgument_t94157543 )L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (L_19)
		{
			goto IL_0095;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_20 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_21 = V_0;
		int32_t L_22 = (int32_t)L_21;
		V_0 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_23 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		CustomAttributeNamedArgument_t94157543  L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (CustomAttributeNamedArgument_t94157543 )L_26);
	}

IL_0095:
	{
		int32_t L_27 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_28 = V_1;
		int32_t L_29 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_30 = V_1;
		int32_t L_31 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_30-(int32_t)L_31))) <= ((int32_t)0)))
		{
			goto IL_00bd;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_32 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_33 = V_0;
		int32_t L_34 = V_1;
		int32_t L_35 = V_0;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_32, (int32_t)L_33, (int32_t)((int32_t)((int32_t)L_34-(int32_t)L_35)), /*hidden argument*/NULL);
	}

IL_00bd:
	{
		int32_t L_36 = V_0;
		__this->set__size_2(L_36);
		int32_t L_37 = V_1;
		int32_t L_38 = V_0;
		return ((int32_t)((int32_t)L_37-(int32_t)L_38));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_RemoveAt_m694265537_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m694265537_gshared (List_1_t3758245971 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m694265537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Reverse()
extern "C"  void List_1_Reverse_m3464820627_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Array_Reverse_m3433347928(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)L_1, /*hidden argument*/NULL);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort()
extern "C"  void List_1_Sort_m3415942229_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 32));
		Comparer_1_t3279133958 * L_2 = ((  Comparer_1_t3279133958 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2494679553_gshared (List_1_t3758245971 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3761433676_gshared (List_1_t3758245971 * __this, Comparison_1_t1355896394 * ___comparison0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Comparison_1_t1355896394 * L_2 = ___comparison0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, int32_t, Comparison_1_t1355896394 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (int32_t)L_1, (Comparison_1_t1355896394 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::ToArray()
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3304067486* List_1_ToArray_m101334674_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t3304067486* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)((CustomAttributeNamedArgumentU5BU5D_t3304067486*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2363740072(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::TrimExcess()
extern "C"  void List_1_TrimExcess_m148071630_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m737897572_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m895816763_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m895816763_gshared (List_1_t3758245971 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m895816763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m15523695(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486** L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t List_1_get_Count_m746333615_gshared (List_1_t3758245971 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_get_Item_m1547593893_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t94157543  List_1_get_Item_m1547593893_gshared (List_1_t3758245971 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m1547593893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_set_Item_m3124475534_MetadataUsageId;
extern "C"  void List_1_set_Item_m3124475534_gshared (List_1_t3758245971 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m3124475534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3758245971 *)__this);
		((  void (*) (List_1_t3758245971 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3758245971 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_4 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeNamedArgument_t94157543  L_6 = ___value1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t94157543 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void List_1__ctor_m2672294496_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = ((List_1_t867319046_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m388665447_gshared (List_1_t867319046 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t867319046 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = ((List_1_t867319046_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t867319046 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((CustomAttributeTypedArgumentU5BU5D_t1075686591*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t867319046 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3404069002;
extern const uint32_t List_1__ctor_m1374227281_MetadataUsageId;
extern "C"  void List_1__ctor_m1374227281_gshared (List_1_t867319046 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m1374227281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_1 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_1, (String_t*)_stringLiteral3404069002, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((CustomAttributeTypedArgumentU5BU5D_t1075686591*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C"  void List_1__cctor_m964742127_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t867319046_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((CustomAttributeTypedArgumentU5BU5D_t1075686591*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1503548298_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t867319046 *)__this);
		Enumerator_t402048720  L_0 = ((  Enumerator_t402048720  (*) (List_1_t867319046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t867319046 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t402048720  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1530390632_gshared (List_1_t867319046 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m756554573_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t867319046 *)__this);
		Enumerator_t402048720  L_0 = ((  Enumerator_t402048720  (*) (List_1_t867319046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((List_1_t867319046 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		Enumerator_t402048720  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Add_m2159243884_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m2159243884_gshared (List_1_t867319046 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m2159243884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t867319046 *)__this);
			((  void (*) (List_1_t867319046 *, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t867319046 *)__this, (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m2320767470_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m2320767470_gshared (List_1_t867319046 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m2320767470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t867319046 *)__this);
			bool L_1 = ((  bool (*) (List_1_t867319046 *, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t867319046 *)__this, (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m1198382402_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1198382402_gshared (List_1_t867319046 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m1198382402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t867319046 *)__this);
			int32_t L_1 = ((  int32_t (*) (List_1_t867319046 *, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t867319046 *)__this, (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3168545717;
extern const uint32_t List_1_System_Collections_IList_Insert_m813883425_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m813883425_gshared (List_1_t867319046 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m813883425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t867319046 *)__this);
			((  void (*) (List_1_t867319046 *, int32_t, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_1, (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_2, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_3, (String_t*)_stringLiteral3168545717, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m2040310137_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m2040310137_gshared (List_1_t867319046 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m2040310137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t867319046 *)__this);
			((  bool (*) (List_1_t867319046 *, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t867319046 *)__this, (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1614481629_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1589801624_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1040733662_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1301385461_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m918797556_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2094199825_gshared (List_1_t867319046 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t867319046 *)__this);
		CustomAttributeTypedArgument_t1498197914  L_1 = ((  CustomAttributeTypedArgument_t1498197914  (*) (List_1_t867319046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		CustomAttributeTypedArgument_t1498197914  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3156209119_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t3625212209_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1803325615;
extern const uint32_t List_1_System_Collections_IList_set_Item_m462908230_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m462908230_gshared (List_1_t867319046 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m462908230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t867319046 *)__this);
			((  void (*) (List_1_t867319046 *, int32_t, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t1498197914 )((*(CustomAttributeTypedArgument_t1498197914 *)((CustomAttributeTypedArgument_t1498197914 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3156209119_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t3625212209_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, (String_t*)_stringLiteral1803325615, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void List_1_Add_m943275925_gshared (List_1_t867319046 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t867319046 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001a:
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_2 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		CustomAttributeTypedArgument_t1498197914  L_6 = ___item0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t1498197914 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1253877786_gshared (List_1_t867319046 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t867319046 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t867319046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((List_1_t867319046 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_5 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m2671311541(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3411511922_gshared (List_1_t867319046 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_5 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< CustomAttributeTypedArgumentU5BU5D_t1075686591*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), (Il2CppObject*)L_4, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1315238882_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m1315238882_gshared (List_1_t867319046 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1315238882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CustomAttributeTypedArgument_t1498197914  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			CustomAttributeTypedArgument_t1498197914  L_3 = InterfaceFuncInvoker0< CustomAttributeTypedArgument_t1498197914  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (CustomAttributeTypedArgument_t1498197914 )L_3;
			CustomAttributeTypedArgument_t1498197914  L_4 = V_0;
			NullCheck((List_1_t867319046 *)__this);
			((  void (*) (List_1_t867319046 *, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t867319046 *)__this, (CustomAttributeTypedArgument_t1498197914 )L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1961118505_gshared (List_1_t867319046 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((List_1_t867319046 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t867319046 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((List_1_t867319046 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1683983606 * List_1_AsReadOnly_m1705673780_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t1683983606 * L_0 = (ReadOnlyCollection_1_t1683983606 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 22));
		((  void (*) (ReadOnlyCollection_1_t1683983606 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23)->methodPointer)(L_0, (Il2CppObject*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 23));
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void List_1_Clear_m4218787945_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool List_1_Contains_m201418743_gshared (List_1_t867319046 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1257394493_gshared (List_1_t867319046 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Find(System.Predicate`1<T>)
extern Il2CppClass* CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var;
extern const uint32_t List_1_Find_m1730628159_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t1498197914  List_1_Find_m1730628159_gshared (List_1_t867319046 * __this, Predicate_1_t4236135325 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_Find_m1730628159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	CustomAttributeTypedArgument_t1498197914  V_1;
	memset(&V_1, 0, sizeof(V_1));
	CustomAttributeTypedArgument_t1498197914  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		Predicate_1_t4236135325 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t4236135325 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t4236135325 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Predicate_1_t4236135325 * L_2 = ___match0;
		NullCheck((List_1_t867319046 *)__this);
		int32_t L_3 = ((  int32_t (*) (List_1_t867319046 *, int32_t, int32_t, Predicate_1_t4236135325 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((List_1_t867319046 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t4236135325 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_002d;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_5 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		CustomAttributeTypedArgument_t1498197914  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		G_B3_0 = L_8;
		goto IL_0036;
	}

IL_002d:
	{
		Initobj (CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var, (&V_1));
		CustomAttributeTypedArgument_t1498197914  L_9 = V_1;
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckMatch(System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322341559;
extern const uint32_t List_1_CheckMatch_m3223332392_MetadataUsageId;
extern "C"  void List_1_CheckMatch_m3223332392_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t4236135325 * ___match0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckMatch_m3223332392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Predicate_1_t4236135325 * L_0 = ___match0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral3322341559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2077176567_gshared (List_1_t867319046 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t4236135325 * ___match2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex0;
		int32_t L_1 = ___count1;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex0;
		V_1 = (int32_t)L_2;
		goto IL_0028;
	}

IL_000b:
	{
		Predicate_1_t4236135325 * L_3 = ___match2;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_4 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		CustomAttributeTypedArgument_t1498197914  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t4236135325 *)L_3);
		bool L_8 = ((  bool (*) (Predicate_1_t4236135325 *, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t4236135325 *)L_3, (CustomAttributeTypedArgument_t1498197914 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_8)
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_9 = V_1;
		return L_9;
	}

IL_0024:
	{
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000b;
		}
	}
	{
		return (-1);
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Enumerator_t402048720  List_1_GetEnumerator_m1475908476_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		Enumerator_t402048720  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m3421311553(&L_0, (List_1_t867319046 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1434084853_gshared (List_1_t867319046 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m230554188_gshared (List_1_t867319046 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_5 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_6 = ___start0;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_7 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_15 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_CheckIndex_m2515123737_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m2515123737_gshared (List_1_t867319046 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2515123737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3381965982_gshared (List_1_t867319046 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_2 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t867319046 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_4 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeTypedArgument_t1498197914  L_6 = ___item1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t1498197914 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1063029886;
extern const uint32_t List_1_CheckCollection_m2608305187_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m2608305187_gshared (List_1_t867319046 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m2608305187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, (String_t*)_stringLiteral1063029886, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool List_1_Remove_m2218182224_gshared (List_1_t867319046 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = ___item0;
		NullCheck((List_1_t867319046 *)__this);
		int32_t L_1 = ((  int32_t (*) (List_1_t867319046 *, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t867319046 *)__this, (CustomAttributeTypedArgument_t1498197914 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m810331748_gshared (List_1_t867319046 * __this, Predicate_1_t4236135325 * ___match0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Predicate_1_t4236135325 * L_0 = ___match0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t4236135325 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Predicate_1_t4236135325 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		V_0 = (int32_t)0;
		goto IL_0031;
	}

IL_0011:
	{
		Predicate_1_t4236135325 * L_1 = ___match0;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_2 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		CustomAttributeTypedArgument_t1498197914  L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck((Predicate_1_t4236135325 *)L_1);
		bool L_6 = ((  bool (*) (Predicate_1_t4236135325 *, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t4236135325 *)L_1, (CustomAttributeTypedArgument_t1498197914 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		goto IL_003d;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0011;
		}
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_004b;
		}
	}
	{
		return 0;
	}

IL_004b:
	{
		int32_t L_12 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)L_13+(int32_t)1));
		goto IL_0099;
	}

IL_0062:
	{
		Predicate_1_t4236135325 * L_14 = ___match0;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_15 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		CustomAttributeTypedArgument_t1498197914  L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck((Predicate_1_t4236135325 *)L_14);
		bool L_19 = ((  bool (*) (Predicate_1_t4236135325 *, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)((Predicate_1_t4236135325 *)L_14, (CustomAttributeTypedArgument_t1498197914 )L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		if (L_19)
		{
			goto IL_0095;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_20 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_21 = V_0;
		int32_t L_22 = (int32_t)L_21;
		V_0 = (int32_t)((int32_t)((int32_t)L_22+(int32_t)1));
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_23 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		int32_t L_25 = L_24;
		CustomAttributeTypedArgument_t1498197914  L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (CustomAttributeTypedArgument_t1498197914 )L_26);
	}

IL_0095:
	{
		int32_t L_27 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_28 = V_1;
		int32_t L_29 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_30 = V_1;
		int32_t L_31 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_30-(int32_t)L_31))) <= ((int32_t)0)))
		{
			goto IL_00bd;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_32 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_33 = V_0;
		int32_t L_34 = V_1;
		int32_t L_35 = V_0;
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_32, (int32_t)L_33, (int32_t)((int32_t)((int32_t)L_34-(int32_t)L_35)), /*hidden argument*/NULL);
	}

IL_00bd:
	{
		int32_t L_36 = V_0;
		__this->set__size_2(L_36);
		int32_t L_37 = V_1;
		int32_t L_38 = V_0;
		return ((int32_t)((int32_t)L_37-(int32_t)L_38));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_RemoveAt_m1271632082_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m1271632082_gshared (List_1_t867319046 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m1271632082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_5 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m782967417(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Reverse()
extern "C"  void List_1_Reverse_m3362906046_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Array_Reverse_m3433347928(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)L_1, /*hidden argument*/NULL);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort()
extern "C"  void List_1_Sort_m3454751890_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 32));
		Comparer_1_t388207033 * L_2 = ((  Comparer_1_t388207033 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m980853918_gshared (List_1_t867319046 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 33));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1395775863_gshared (List_1_t867319046 * __this, Comparison_1_t2759936765 * ___comparison0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Comparison_1_t2759936765 * L_2 = ___comparison0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, int32_t, Comparison_1_t2759936765 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (int32_t)L_1, (Comparison_1_t2759936765 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ToArray()
extern "C"  CustomAttributeTypedArgumentU5BU5D_t1075686591* List_1_ToArray_m1103831931_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t1075686591* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)((CustomAttributeTypedArgumentU5BU5D_t1075686591*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (uint32_t)L_0));
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2363740072(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2860576477_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3131467143_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m3082973746_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m3082973746_gshared (List_1_t867319046 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m3082973746_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m15523695(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591** L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 35));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t List_1_get_Count_m3939916508_gshared (List_1_t867319046 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_get_Item_m22907878_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t1498197914  List_1_get_Item_m22907878_gshared (List_1_t867319046 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m22907878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t List_1_set_Item_m1062416045_MetadataUsageId;
extern "C"  void List_1_set_Item_m1062416045_gshared (List_1_t867319046 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m1062416045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t867319046 *)__this);
		((  void (*) (List_1_t867319046 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t867319046 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_3 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_3, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_4 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeTypedArgument_t1498197914  L_6 = ___value1;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t1498197914 )L_6);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
