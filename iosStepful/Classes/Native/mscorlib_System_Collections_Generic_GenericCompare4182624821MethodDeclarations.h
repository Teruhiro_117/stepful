﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.UInt32>
struct GenericComparer_1_t4182624821;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.UInt32>::.ctor()
extern "C"  void GenericComparer_1__ctor_m1431381186_gshared (GenericComparer_1_t4182624821 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m1431381186(__this, method) ((  void (*) (GenericComparer_1_t4182624821 *, const MethodInfo*))GenericComparer_1__ctor_m1431381186_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.UInt32>::Compare(T,T)
extern "C"  int32_t GenericComparer_1_Compare_m1920782057_gshared (GenericComparer_1_t4182624821 * __this, uint32_t ___x0, uint32_t ___y1, const MethodInfo* method);
#define GenericComparer_1_Compare_m1920782057(__this, ___x0, ___y1, method) ((  int32_t (*) (GenericComparer_1_t4182624821 *, uint32_t, uint32_t, const MethodInfo*))GenericComparer_1_Compare_m1920782057_gshared)(__this, ___x0, ___y1, method)
