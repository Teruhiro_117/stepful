﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct List_1_t886216149;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat420945823.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1517095017.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m578968982_gshared (Enumerator_t420945823 * __this, List_1_t886216149 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m578968982(__this, ___l0, method) ((  void (*) (Enumerator_t420945823 *, List_1_t886216149 *, const MethodInfo*))Enumerator__ctor_m578968982_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3810474372_gshared (Enumerator_t420945823 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3810474372(__this, method) ((  void (*) (Enumerator_t420945823 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3810474372_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2299228178_gshared (Enumerator_t420945823 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2299228178(__this, method) ((  Il2CppObject * (*) (Enumerator_t420945823 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2299228178_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Dispose()
extern "C"  void Enumerator_Dispose_m2059442621_gshared (Enumerator_t420945823 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2059442621(__this, method) ((  void (*) (Enumerator_t420945823 *, const MethodInfo*))Enumerator_Dispose_m2059442621_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2766416936_gshared (Enumerator_t420945823 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2766416936(__this, method) ((  void (*) (Enumerator_t420945823 *, const MethodInfo*))Enumerator_VerifyState_m2766416936_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2566888296_gshared (Enumerator_t420945823 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2566888296(__this, method) ((  bool (*) (Enumerator_t420945823 *, const MethodInfo*))Enumerator_MoveNext_m2566888296_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::get_Current()
extern "C"  PendingPlayer_t1517095017  Enumerator_get_Current_m2424673909_gshared (Enumerator_t420945823 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2424673909(__this, method) ((  PendingPlayer_t1517095017  (*) (Enumerator_t420945823 *, const MethodInfo*))Enumerator_get_Current_m2424673909_gshared)(__this, method)
