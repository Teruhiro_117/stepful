﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct ReadOnlyCollection_1_t1163407414;
// System.Collections.Generic.IList`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct IList_1_t1518562323;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.LocalClient/InternalMsg[]
struct InternalMsgU5BU5D_t2023740543;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct IEnumerator_1_t2748112845;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1883606958_gshared (ReadOnlyCollection_1_t1163407414 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1883606958(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1883606958_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2230783502_gshared (ReadOnlyCollection_1_t1163407414 * __this, InternalMsg_t977621722  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2230783502(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, InternalMsg_t977621722 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2230783502_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2962013634_gshared (ReadOnlyCollection_1_t1163407414 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2962013634(__this, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2962013634_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m223287563_gshared (ReadOnlyCollection_1_t1163407414 * __this, int32_t ___index0, InternalMsg_t977621722  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m223287563(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, int32_t, InternalMsg_t977621722 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m223287563_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2932318259_gshared (ReadOnlyCollection_1_t1163407414 * __this, InternalMsg_t977621722  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2932318259(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1163407414 *, InternalMsg_t977621722 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2932318259_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m254461479_gshared (ReadOnlyCollection_1_t1163407414 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m254461479(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m254461479_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  InternalMsg_t977621722  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3684112523_gshared (ReadOnlyCollection_1_t1163407414 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3684112523(__this, ___index0, method) ((  InternalMsg_t977621722  (*) (ReadOnlyCollection_1_t1163407414 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3684112523_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3608235060_gshared (ReadOnlyCollection_1_t1163407414 * __this, int32_t ___index0, InternalMsg_t977621722  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3608235060(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, int32_t, InternalMsg_t977621722 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3608235060_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3155848268_gshared (ReadOnlyCollection_1_t1163407414 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3155848268(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1163407414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3155848268_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3222152957_gshared (ReadOnlyCollection_1_t1163407414 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3222152957(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3222152957_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3314440220_gshared (ReadOnlyCollection_1_t1163407414 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3314440220(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1163407414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3314440220_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1706959357_gshared (ReadOnlyCollection_1_t1163407414 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1706959357(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1163407414 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1706959357_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m946707581_gshared (ReadOnlyCollection_1_t1163407414 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m946707581(__this, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m946707581_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2270655037_gshared (ReadOnlyCollection_1_t1163407414 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2270655037(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1163407414 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2270655037_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m790048295_gshared (ReadOnlyCollection_1_t1163407414 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m790048295(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1163407414 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m790048295_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3447647630_gshared (ReadOnlyCollection_1_t1163407414 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3447647630(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3447647630_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3552188396_gshared (ReadOnlyCollection_1_t1163407414 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3552188396(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3552188396_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3032924772_gshared (ReadOnlyCollection_1_t1163407414 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3032924772(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3032924772_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m343633873_gshared (ReadOnlyCollection_1_t1163407414 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m343633873(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1163407414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m343633873_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2170224913_gshared (ReadOnlyCollection_1_t1163407414 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2170224913(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1163407414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2170224913_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3202578182_gshared (ReadOnlyCollection_1_t1163407414 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3202578182(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1163407414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3202578182_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2350179301_gshared (ReadOnlyCollection_1_t1163407414 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2350179301(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1163407414 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2350179301_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3119006852_gshared (ReadOnlyCollection_1_t1163407414 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3119006852(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1163407414 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3119006852_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2004975791_gshared (ReadOnlyCollection_1_t1163407414 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2004975791(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2004975791_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2713667012_gshared (ReadOnlyCollection_1_t1163407414 * __this, InternalMsg_t977621722  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2713667012(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1163407414 *, InternalMsg_t977621722 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2713667012_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m932294514_gshared (ReadOnlyCollection_1_t1163407414 * __this, InternalMsgU5BU5D_t2023740543* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m932294514(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1163407414 *, InternalMsgU5BU5D_t2023740543*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m932294514_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3929971893_gshared (ReadOnlyCollection_1_t1163407414 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3929971893(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1163407414 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3929971893_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2366039588_gshared (ReadOnlyCollection_1_t1163407414 * __this, InternalMsg_t977621722  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2366039588(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1163407414 *, InternalMsg_t977621722 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2366039588_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2371378381_gshared (ReadOnlyCollection_1_t1163407414 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2371378381(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1163407414 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2371378381_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>::get_Item(System.Int32)
extern "C"  InternalMsg_t977621722  ReadOnlyCollection_1_get_Item_m3334202911_gshared (ReadOnlyCollection_1_t1163407414 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3334202911(__this, ___index0, method) ((  InternalMsg_t977621722  (*) (ReadOnlyCollection_1_t1163407414 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3334202911_gshared)(__this, ___index0, method)
