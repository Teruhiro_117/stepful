﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayRecButtons/Button
struct Button_t3262243951;
// EveryplayRecButtons/TextureAtlasSrc
struct TextureAtlasSrc_t2048635151;
// EveryplayRecButtons/ButtonTapped
struct ButtonTapped_t3122824015;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_2048635151.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_3122824015.h"

// System.Void EveryplayRecButtons/Button::.ctor(EveryplayRecButtons/TextureAtlasSrc,EveryplayRecButtons/TextureAtlasSrc,EveryplayRecButtons/ButtonTapped)
extern "C"  void Button__ctor_m345536917 (Button_t3262243951 * __this, TextureAtlasSrc_t2048635151 * ___bg0, TextureAtlasSrc_t2048635151 * ___title1, ButtonTapped_t3122824015 * ___buttonTapped2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
