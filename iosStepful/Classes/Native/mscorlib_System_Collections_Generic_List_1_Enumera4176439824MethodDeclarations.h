﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct List_1_t346742854;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4176439824.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2227776565_gshared (Enumerator_t4176439824 * __this, List_1_t346742854 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2227776565(__this, ___l0, method) ((  void (*) (Enumerator_t4176439824 *, List_1_t346742854 *, const MethodInfo*))Enumerator__ctor_m2227776565_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3631846949_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3631846949(__this, method) ((  void (*) (Enumerator_t4176439824 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3631846949_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1820733745_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1820733745(__this, method) ((  Il2CppObject * (*) (Enumerator_t4176439824 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1820733745_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::Dispose()
extern "C"  void Enumerator_Dispose_m2619804142_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2619804142(__this, method) ((  void (*) (Enumerator_t4176439824 *, const MethodInfo*))Enumerator_Dispose_m2619804142_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3332355895_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3332355895(__this, method) ((  void (*) (Enumerator_t4176439824 *, const MethodInfo*))Enumerator_VerifyState_m3332355895_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1625322877_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1625322877(__this, method) ((  bool (*) (Enumerator_t4176439824 *, const MethodInfo*))Enumerator_MoveNext_m1625322877_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::get_Current()
extern "C"  InternalMsg_t977621722  Enumerator_get_Current_m2081821142_gshared (Enumerator_t4176439824 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2081821142(__this, method) ((  InternalMsg_t977621722  (*) (Enumerator_t4176439824 *, const MethodInfo*))Enumerator_get_Current_m2081821142_gshared)(__this, method)
