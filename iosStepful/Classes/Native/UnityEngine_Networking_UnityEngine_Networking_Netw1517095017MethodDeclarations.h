﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkLobbyManager/PendingPlayer
struct PendingPlayer_t1517095017;
struct PendingPlayer_t1517095017_marshaled_pinvoke;
struct PendingPlayer_t1517095017_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct PendingPlayer_t1517095017;
struct PendingPlayer_t1517095017_marshaled_pinvoke;

extern "C" void PendingPlayer_t1517095017_marshal_pinvoke(const PendingPlayer_t1517095017& unmarshaled, PendingPlayer_t1517095017_marshaled_pinvoke& marshaled);
extern "C" void PendingPlayer_t1517095017_marshal_pinvoke_back(const PendingPlayer_t1517095017_marshaled_pinvoke& marshaled, PendingPlayer_t1517095017& unmarshaled);
extern "C" void PendingPlayer_t1517095017_marshal_pinvoke_cleanup(PendingPlayer_t1517095017_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PendingPlayer_t1517095017;
struct PendingPlayer_t1517095017_marshaled_com;

extern "C" void PendingPlayer_t1517095017_marshal_com(const PendingPlayer_t1517095017& unmarshaled, PendingPlayer_t1517095017_marshaled_com& marshaled);
extern "C" void PendingPlayer_t1517095017_marshal_com_back(const PendingPlayer_t1517095017_marshaled_com& marshaled, PendingPlayer_t1517095017& unmarshaled);
extern "C" void PendingPlayer_t1517095017_marshal_com_cleanup(PendingPlayer_t1517095017_marshaled_com& marshaled);
