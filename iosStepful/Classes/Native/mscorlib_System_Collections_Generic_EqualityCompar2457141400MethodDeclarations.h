﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkHash128>
struct DefaultComparer_t2457141400;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo835211239.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkHash128>::.ctor()
extern "C"  void DefaultComparer__ctor_m1939348160_gshared (DefaultComparer_t2457141400 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1939348160(__this, method) ((  void (*) (DefaultComparer_t2457141400 *, const MethodInfo*))DefaultComparer__ctor_m1939348160_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkHash128>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3786258187_gshared (DefaultComparer_t2457141400 * __this, NetworkHash128_t835211239  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3786258187(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2457141400 *, NetworkHash128_t835211239 , const MethodInfo*))DefaultComparer_GetHashCode_m3786258187_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkHash128>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1211745171_gshared (DefaultComparer_t2457141400 * __this, NetworkHash128_t835211239  ___x0, NetworkHash128_t835211239  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1211745171(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2457141400 *, NetworkHash128_t835211239 , NetworkHash128_t835211239 , const MethodInfo*))DefaultComparer_Equals_m1211745171_gshared)(__this, ___x0, ___y1, method)
