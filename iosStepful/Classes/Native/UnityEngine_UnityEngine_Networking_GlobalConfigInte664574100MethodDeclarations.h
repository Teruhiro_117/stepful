﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.GlobalConfigInternal
struct GlobalConfigInternal_t664574100;
// UnityEngine.Networking.GlobalConfig
struct GlobalConfig_t2977893073;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_GlobalConfig2977893073.h"

// System.Void UnityEngine.Networking.GlobalConfigInternal::.ctor(UnityEngine.Networking.GlobalConfig)
extern "C"  void GlobalConfigInternal__ctor_m471413481 (GlobalConfigInternal_t664574100 * __this, GlobalConfig_t2977893073 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.GlobalConfigInternal::InitWrapper()
extern "C"  void GlobalConfigInternal_InitWrapper_m4177250664 (GlobalConfigInternal_t664574100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.GlobalConfigInternal::InitThreadAwakeTimeout(System.UInt32)
extern "C"  void GlobalConfigInternal_InitThreadAwakeTimeout_m2022754881 (GlobalConfigInternal_t664574100 * __this, uint32_t ___ms0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.GlobalConfigInternal::InitReactorModel(System.Byte)
extern "C"  void GlobalConfigInternal_InitReactorModel_m2943743707 (GlobalConfigInternal_t664574100 * __this, uint8_t ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.GlobalConfigInternal::InitReactorMaximumReceivedMessages(System.UInt16)
extern "C"  void GlobalConfigInternal_InitReactorMaximumReceivedMessages_m1542945852 (GlobalConfigInternal_t664574100 * __this, uint16_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.GlobalConfigInternal::InitReactorMaximumSentMessages(System.UInt16)
extern "C"  void GlobalConfigInternal_InitReactorMaximumSentMessages_m2965361179 (GlobalConfigInternal_t664574100 * __this, uint16_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.GlobalConfigInternal::InitMaxPacketSize(System.UInt16)
extern "C"  void GlobalConfigInternal_InitMaxPacketSize_m621888544 (GlobalConfigInternal_t664574100 * __this, uint16_t ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.GlobalConfigInternal::Dispose()
extern "C"  void GlobalConfigInternal_Dispose_m1710925344 (GlobalConfigInternal_t664574100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.GlobalConfigInternal::Finalize()
extern "C"  void GlobalConfigInternal_Finalize_m2214807529 (GlobalConfigInternal_t664574100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
