﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_973022757.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw4207851900.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2381829160_gshared (KeyValuePair_2_t973022757 * __this, int32_t ___key0, ConnectionPendingPlayers_t4207851900  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2381829160(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t973022757 *, int32_t, ConnectionPendingPlayers_t4207851900 , const MethodInfo*))KeyValuePair_2__ctor_m2381829160_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2308175658_gshared (KeyValuePair_2_t973022757 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2308175658(__this, method) ((  int32_t (*) (KeyValuePair_2_t973022757 *, const MethodInfo*))KeyValuePair_2_get_Key_m2308175658_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2060752417_gshared (KeyValuePair_2_t973022757 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2060752417(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t973022757 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2060752417_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::get_Value()
extern "C"  ConnectionPendingPlayers_t4207851900  KeyValuePair_2_get_Value_m2013503202_gshared (KeyValuePair_2_t973022757 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2013503202(__this, method) ((  ConnectionPendingPlayers_t4207851900  (*) (KeyValuePair_2_t973022757 *, const MethodInfo*))KeyValuePair_2_get_Value_m2013503202_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2955046537_gshared (KeyValuePair_2_t973022757 * __this, ConnectionPendingPlayers_t4207851900  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2955046537(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t973022757 *, ConnectionPendingPlayers_t4207851900 , const MethodInfo*))KeyValuePair_2_set_Value_m2955046537_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.Networking.NetworkMigrationManager/ConnectionPendingPlayers>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2029980823_gshared (KeyValuePair_2_t973022757 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2029980823(__this, method) ((  String_t* (*) (KeyValuePair_2_t973022757 *, const MethodInfo*))KeyValuePair_2_ToString_m2029980823_gshared)(__this, method)
