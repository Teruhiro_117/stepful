﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_HumanBone1529896151.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4078305555.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1693994278.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Cust3423099547.h"
#include "UnityEngine_UnityEngine_TextGenerationError780770201.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings2543476768.h"
#include "UnityEngine_UnityEngine_TextGenerator647235000.h"
#include "UnityEngine_UnityEngine_TextAlignment1418134952.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2027154177.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode3668245347.h"
#include "UnityEngine_UnityEngine_GUIText2411476300.h"
#include "UnityEngine_UnityEngine_Font4239498691.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCal1272078033.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases3522132132.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743.h"
#include "UnityEngine_UnityEngine_CanvasRenderer261436805.h"
#include "UnityEngine_UnityEngine_Event3028476042.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngine_EventType3919834026.h"
#include "UnityEngine_UnityEngine_EventModifiers2690251474.h"
#include "UnityEngine_UnityEngine_GUI4082743951.h"
#include "UnityEngine_UnityEngine_GUI_ScrollViewState2792222924.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000.h"
#include "UnityEngine_UnityEngine_FocusType488772178.h"
#include "UnityEngine_UnityEngine_GUILayout2579273657.h"
#include "UnityEngine_UnityEngine_GUILayoutOption4183744904.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_Type4024155706.h"
#include "UnityEngine_UnityEngine_GUILayoutGroup3975363388.h"
#include "UnityEngine_UnityEngine_GUIScrollGroup755788567.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry3828586629.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility996096873.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCac3120781045.h"
#include "UnityEngine_UnityEngine_GUISettings622856320.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegat3594822336.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545.h"
#include "UnityEngine_UnityEngine_FontStyle2764949590.h"
#include "UnityEngine_UnityEngine_ImagePosition3491916276.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_TextClipping2573530411.h"
#include "UnityEngine_UnityEngine_GUITargetAttribute863467180.h"
#include "UnityEngine_UnityEngine_ExitGUIException1618397098.h"
#include "UnityEngine_UnityEngine_GUIUtility3275770671.h"
#include "UnityEngine_UnityEngine_SliderState1595681032.h"
#include "UnityEngine_UnityEngine_TextEditor3975561390.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin1119726228.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType593718391.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3138797698.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments2834709342.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelec1327795077.h"
#include "UnityEngine_UnityEngineInternal_WebRequestUtils4100941042.h"
#include "UnityEngine_UnityEngine_Networking_Match_Request1834273339.h"
#include "UnityEngine_UnityEngine_Networking_Match_ResponseB1952642056.h"
#include "UnityEngine_UnityEngine_Networking_Match_Response999782913.h"
#include "UnityEngine_UnityEngine_Networking_Match_BasicResp1945770051.h"
#include "UnityEngine_UnityEngine_Networking_Match_CreateMat2247754072.h"
#include "UnityEngine_UnityEngine_Networking_Match_CreateMat1656355148.h"
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatch2675294872.h"
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatch1256242636.h"
#include "UnityEngine_UnityEngine_Networking_Match_DropConne3261607412.h"
#include "UnityEngine_UnityEngine_Networking_Match_DropConne3742478148.h"
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchR746807584.h"
#include "UnityEngine_UnityEngine_Networking_Match_MatchDire3750452272.h"
#include "UnityEngine_UnityEngine_Networking_Match_MatchDesc1011518406.h"
#include "UnityEngine_UnityEngine_Networking_Match_ListMatch2092237412.h"
#include "UnityEngine_UnityEngine_Networking_Types_SourceID1840552406.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID348058649.h"
#include "UnityEngine_UnityEngine_Networking_Types_NodeID3569487935.h"
#include "UnityEngine_UnityEngine_Networking_Types_HostPrior2800759508.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkAc2931214851.h"
#include "UnityEngine_UnityEngine_Networking_Utility2692878198.h"
#include "UnityEngine_UnityEngine_Networking_Match_MatchInfo668842927.h"
#include "UnityEngine_UnityEngine_Networking_Match_MatchInfo3179110907.h"
#include "UnityEngine_UnityEngine_Networking_Match_MatchInfo2001338440.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMat259788815.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMa1347328110.h"
#include "UnityEngine_UnityEngine_Networking_NetworkTransport746957803.h"
#include "UnityEngine_UnityEngine_Networking_NetworkEventTyp3809130132.h"
#include "UnityEngine_UnityEngine_Networking_QosType2003892483.h"
#include "UnityEngine_UnityEngine_Networking_NetworkError2010663956.h"
#include "UnityEngine_UnityEngine_Networking_ReactorModel14967895.h"
#include "UnityEngine_UnityEngine_Networking_ChannelQOS1324696500.h"
#include "UnityEngine_UnityEngine_Networking_ConnectionConfi1350910390.h"
#include "UnityEngine_UnityEngine_Networking_HostTopology3602650143.h"
#include "UnityEngine_UnityEngine_Networking_GlobalConfig2977893073.h"
#include "UnityEngine_UnityEngine_Networking_ConnectionSimul3039400316.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (HumanBone_t1529896151)+ sizeof (Il2CppObject), sizeof(HumanBone_t1529896151_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2100[3] = 
{
	HumanBone_t1529896151::get_offset_of_m_BoneName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t1529896151::get_offset_of_m_HumanName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t1529896151::get_offset_of_limit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (RuntimeAnimatorController_t670468573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (AnimatorControllerPlayable_t4078305555)+ sizeof (Il2CppObject), sizeof(AnimatorControllerPlayable_t4078305555 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2102[1] = 
{
	AnimatorControllerPlayable_t4078305555::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (AnimationPlayable_t1693994278)+ sizeof (Il2CppObject), sizeof(AnimationPlayable_t1693994278 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2103[1] = 
{
	AnimationPlayable_t1693994278::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (CustomAnimationPlayable_t3423099547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[1] = 
{
	CustomAnimationPlayable_t3423099547::get_offset_of_handle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (TextGenerationError_t780770201)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2105[5] = 
{
	TextGenerationError_t780770201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (TextGenerationSettings_t2543476768)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[18] = 
{
	TextGenerationSettings_t2543476768::get_offset_of_font_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_lineSpacing_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_richText_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_scaleFactor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_fontStyle_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_textAnchor_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_alignByGeometry_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextForBestFit_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextMinSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextMaxSize_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_updateBounds_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_verticalOverflow_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_horizontalOverflow_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_generationExtents_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_pivot_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_generateOutOfBounds_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (TextGenerator_t647235000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[11] = 
{
	TextGenerator_t647235000::get_offset_of_m_Ptr_0(),
	TextGenerator_t647235000::get_offset_of_m_LastString_1(),
	TextGenerator_t647235000::get_offset_of_m_LastSettings_2(),
	TextGenerator_t647235000::get_offset_of_m_HasGenerated_3(),
	TextGenerator_t647235000::get_offset_of_m_LastValid_4(),
	TextGenerator_t647235000::get_offset_of_m_Verts_5(),
	TextGenerator_t647235000::get_offset_of_m_Characters_6(),
	TextGenerator_t647235000::get_offset_of_m_Lines_7(),
	TextGenerator_t647235000::get_offset_of_m_CachedVerts_8(),
	TextGenerator_t647235000::get_offset_of_m_CachedCharacters_9(),
	TextGenerator_t647235000::get_offset_of_m_CachedLines_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (TextAlignment_t1418134952)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2108[4] = 
{
	TextAlignment_t1418134952::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (TextAnchor_t112990806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2109[10] = 
{
	TextAnchor_t112990806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (HorizontalWrapMode_t2027154177)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2110[3] = 
{
	HorizontalWrapMode_t2027154177::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (VerticalWrapMode_t3668245347)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2111[3] = 
{
	VerticalWrapMode_t3668245347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (GUIText_t2411476300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (Font_t4239498691), -1, sizeof(Font_t4239498691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2113[2] = 
{
	Font_t4239498691_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t4239498691::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (FontTextureRebuildCallback_t1272078033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (UICharInfo_t3056636800)+ sizeof (Il2CppObject), sizeof(UICharInfo_t3056636800 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2115[2] = 
{
	UICharInfo_t3056636800::get_offset_of_cursorPos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UICharInfo_t3056636800::get_offset_of_charWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (UILineInfo_t3621277874)+ sizeof (Il2CppObject), sizeof(UILineInfo_t3621277874 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2116[3] = 
{
	UILineInfo_t3621277874::get_offset_of_startCharIdx_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t3621277874::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t3621277874::get_offset_of_topY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (UIVertex_t1204258818)+ sizeof (Il2CppObject), sizeof(UIVertex_t1204258818 ), sizeof(UIVertex_t1204258818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2117[9] = 
{
	UIVertex_t1204258818::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_color_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_uv0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_uv1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_tangent_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818_StaticFields::get_offset_of_s_DefaultColor_6(),
	UIVertex_t1204258818_StaticFields::get_offset_of_s_DefaultTangent_7(),
	UIVertex_t1204258818_StaticFields::get_offset_of_simpleVert_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (RectTransformUtility_t2941082270), -1, sizeof(RectTransformUtility_t2941082270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2118[1] = 
{
	RectTransformUtility_t2941082270_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (RenderMode_t4280533217)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2119[4] = 
{
	RenderMode_t4280533217::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (Canvas_t209405766), -1, sizeof(Canvas_t209405766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2120[1] = 
{
	Canvas_t209405766_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (WillRenderCanvases_t3522132132), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (CanvasGroup_t3296560743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (CanvasRenderer_t261436805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (Event_t3028476042), sizeof(Event_t3028476042_marshaled_pinvoke), sizeof(Event_t3028476042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2125[4] = 
{
	Event_t3028476042::get_offset_of_m_Ptr_0(),
	Event_t3028476042_StaticFields::get_offset_of_s_Current_1(),
	Event_t3028476042_StaticFields::get_offset_of_s_MasterEvent_2(),
	Event_t3028476042_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (KeyCode_t2283395152)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2126[322] = 
{
	KeyCode_t2283395152::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (EventType_t3919834026)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2127[31] = 
{
	EventType_t3919834026::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (EventModifiers_t2690251474)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2128[9] = 
{
	EventModifiers_t2690251474::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (GUI_t4082743951), -1, sizeof(GUI_t4082743951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2129[12] = 
{
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollStepSize_0(),
	GUI_t4082743951_StaticFields::get_offset_of_U3CnextScrollStepTimeU3Ek__BackingField_1(),
	GUI_t4082743951_StaticFields::get_offset_of_s_HotTextField_2(),
	GUI_t4082743951_StaticFields::get_offset_of_s_BoxHash_3(),
	GUI_t4082743951_StaticFields::get_offset_of_s_RepeatButtonHash_4(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ToggleHash_5(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ButtonGridHash_6(),
	GUI_t4082743951_StaticFields::get_offset_of_s_SliderHash_7(),
	GUI_t4082743951_StaticFields::get_offset_of_s_BeginGroupHash_8(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollviewHash_9(),
	GUI_t4082743951_StaticFields::get_offset_of_s_Skin_10(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollViewStates_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (ScrollViewState_t2792222924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[2] = 
{
	ScrollViewState_t2792222924::get_offset_of_apply_0(),
	ScrollViewState_t2792222924::get_offset_of_hasScrollTo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (WindowFunction_t3486805455), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (GUIContent_t4210063000), -1, sizeof(GUIContent_t4210063000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2132[7] = 
{
	GUIContent_t4210063000::get_offset_of_m_Text_0(),
	GUIContent_t4210063000::get_offset_of_m_Image_1(),
	GUIContent_t4210063000::get_offset_of_m_Tooltip_2(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_Text_3(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_Image_4(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_TextImage_5(),
	GUIContent_t4210063000_StaticFields::get_offset_of_none_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (FocusType_t488772178)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2133[4] = 
{
	FocusType_t488772178::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (GUILayout_t2579273657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (GUILayoutOption_t4183744904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[2] = 
{
	GUILayoutOption_t4183744904::get_offset_of_type_0(),
	GUILayoutOption_t4183744904::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (Type_t4024155706)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2136[15] = 
{
	Type_t4024155706::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (GUILayoutGroup_t3975363388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[17] = 
{
	GUILayoutGroup_t3975363388::get_offset_of_entries_10(),
	GUILayoutGroup_t3975363388::get_offset_of_isVertical_11(),
	GUILayoutGroup_t3975363388::get_offset_of_resetCoords_12(),
	GUILayoutGroup_t3975363388::get_offset_of_spacing_13(),
	GUILayoutGroup_t3975363388::get_offset_of_sameSize_14(),
	GUILayoutGroup_t3975363388::get_offset_of_isWindow_15(),
	GUILayoutGroup_t3975363388::get_offset_of_windowID_16(),
	GUILayoutGroup_t3975363388::get_offset_of_m_Cursor_17(),
	GUILayoutGroup_t3975363388::get_offset_of_m_StretchableCountX_18(),
	GUILayoutGroup_t3975363388::get_offset_of_m_StretchableCountY_19(),
	GUILayoutGroup_t3975363388::get_offset_of_m_UserSpecifiedWidth_20(),
	GUILayoutGroup_t3975363388::get_offset_of_m_UserSpecifiedHeight_21(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMinWidth_22(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMaxWidth_23(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMinHeight_24(),
	GUILayoutGroup_t3975363388::get_offset_of_m_ChildMaxHeight_25(),
	GUILayoutGroup_t3975363388::get_offset_of_m_Margin_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (GUIScrollGroup_t755788567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[12] = 
{
	GUIScrollGroup_t755788567::get_offset_of_calcMinWidth_27(),
	GUIScrollGroup_t755788567::get_offset_of_calcMaxWidth_28(),
	GUIScrollGroup_t755788567::get_offset_of_calcMinHeight_29(),
	GUIScrollGroup_t755788567::get_offset_of_calcMaxHeight_30(),
	GUIScrollGroup_t755788567::get_offset_of_clientWidth_31(),
	GUIScrollGroup_t755788567::get_offset_of_clientHeight_32(),
	GUIScrollGroup_t755788567::get_offset_of_allowHorizontalScroll_33(),
	GUIScrollGroup_t755788567::get_offset_of_allowVerticalScroll_34(),
	GUIScrollGroup_t755788567::get_offset_of_needsHorizontalScrollbar_35(),
	GUIScrollGroup_t755788567::get_offset_of_needsVerticalScrollbar_36(),
	GUIScrollGroup_t755788567::get_offset_of_horizontalScrollbar_37(),
	GUIScrollGroup_t755788567::get_offset_of_verticalScrollbar_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (GUILayoutEntry_t3828586629), -1, sizeof(GUILayoutEntry_t3828586629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2139[10] = 
{
	GUILayoutEntry_t3828586629::get_offset_of_minWidth_0(),
	GUILayoutEntry_t3828586629::get_offset_of_maxWidth_1(),
	GUILayoutEntry_t3828586629::get_offset_of_minHeight_2(),
	GUILayoutEntry_t3828586629::get_offset_of_maxHeight_3(),
	GUILayoutEntry_t3828586629::get_offset_of_rect_4(),
	GUILayoutEntry_t3828586629::get_offset_of_stretchWidth_5(),
	GUILayoutEntry_t3828586629::get_offset_of_stretchHeight_6(),
	GUILayoutEntry_t3828586629::get_offset_of_m_Style_7(),
	GUILayoutEntry_t3828586629_StaticFields::get_offset_of_kDummyRect_8(),
	GUILayoutEntry_t3828586629_StaticFields::get_offset_of_indent_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (GUILayoutUtility_t996096873), -1, sizeof(GUILayoutUtility_t996096873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2140[5] = 
{
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_s_StoredLayouts_0(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_s_StoredWindows_1(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_current_2(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_kDummyRect_3(),
	GUILayoutUtility_t996096873_StaticFields::get_offset_of_s_SpaceStyle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (LayoutCache_t3120781045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[3] = 
{
	LayoutCache_t3120781045::get_offset_of_topLevel_0(),
	LayoutCache_t3120781045::get_offset_of_layoutGroups_1(),
	LayoutCache_t3120781045::get_offset_of_windows_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (GUISettings_t622856320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[5] = 
{
	GUISettings_t622856320::get_offset_of_m_DoubleClickSelectsWord_0(),
	GUISettings_t622856320::get_offset_of_m_TripleClickSelectsLine_1(),
	GUISettings_t622856320::get_offset_of_m_CursorColor_2(),
	GUISettings_t622856320::get_offset_of_m_CursorFlashSpeed_3(),
	GUISettings_t622856320::get_offset_of_m_SelectionColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (GUISkin_t1436893342), -1, sizeof(GUISkin_t1436893342_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2143[27] = 
{
	GUISkin_t1436893342::get_offset_of_m_Font_2(),
	GUISkin_t1436893342::get_offset_of_m_box_3(),
	GUISkin_t1436893342::get_offset_of_m_button_4(),
	GUISkin_t1436893342::get_offset_of_m_toggle_5(),
	GUISkin_t1436893342::get_offset_of_m_label_6(),
	GUISkin_t1436893342::get_offset_of_m_textField_7(),
	GUISkin_t1436893342::get_offset_of_m_textArea_8(),
	GUISkin_t1436893342::get_offset_of_m_window_9(),
	GUISkin_t1436893342::get_offset_of_m_horizontalSlider_10(),
	GUISkin_t1436893342::get_offset_of_m_horizontalSliderThumb_11(),
	GUISkin_t1436893342::get_offset_of_m_verticalSlider_12(),
	GUISkin_t1436893342::get_offset_of_m_verticalSliderThumb_13(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbar_14(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbarThumb_15(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbarLeftButton_16(),
	GUISkin_t1436893342::get_offset_of_m_horizontalScrollbarRightButton_17(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbar_18(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbarThumb_19(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbarUpButton_20(),
	GUISkin_t1436893342::get_offset_of_m_verticalScrollbarDownButton_21(),
	GUISkin_t1436893342::get_offset_of_m_ScrollView_22(),
	GUISkin_t1436893342::get_offset_of_m_CustomStyles_23(),
	GUISkin_t1436893342::get_offset_of_m_Settings_24(),
	GUISkin_t1436893342_StaticFields::get_offset_of_ms_Error_25(),
	GUISkin_t1436893342::get_offset_of_m_Styles_26(),
	GUISkin_t1436893342_StaticFields::get_offset_of_m_SkinChanged_27(),
	GUISkin_t1436893342_StaticFields::get_offset_of_current_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (SkinChangedDelegate_t3594822336), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (GUIStyleState_t3801000545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[3] = 
{
	GUIStyleState_t3801000545::get_offset_of_m_Ptr_0(),
	GUIStyleState_t3801000545::get_offset_of_m_SourceStyle_1(),
	GUIStyleState_t3801000545::get_offset_of_m_Background_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (FontStyle_t2764949590)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2146[5] = 
{
	FontStyle_t2764949590::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (ImagePosition_t3491916276)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2147[5] = 
{
	ImagePosition_t3491916276::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (GUIStyle_t1799908754), -1, sizeof(GUIStyle_t1799908754_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2148[16] = 
{
	GUIStyle_t1799908754::get_offset_of_m_Ptr_0(),
	GUIStyle_t1799908754::get_offset_of_m_Normal_1(),
	GUIStyle_t1799908754::get_offset_of_m_Hover_2(),
	GUIStyle_t1799908754::get_offset_of_m_Active_3(),
	GUIStyle_t1799908754::get_offset_of_m_Focused_4(),
	GUIStyle_t1799908754::get_offset_of_m_OnNormal_5(),
	GUIStyle_t1799908754::get_offset_of_m_OnHover_6(),
	GUIStyle_t1799908754::get_offset_of_m_OnActive_7(),
	GUIStyle_t1799908754::get_offset_of_m_OnFocused_8(),
	GUIStyle_t1799908754::get_offset_of_m_Border_9(),
	GUIStyle_t1799908754::get_offset_of_m_Padding_10(),
	GUIStyle_t1799908754::get_offset_of_m_Margin_11(),
	GUIStyle_t1799908754::get_offset_of_m_Overflow_12(),
	GUIStyle_t1799908754::get_offset_of_m_FontInternal_13(),
	GUIStyle_t1799908754_StaticFields::get_offset_of_showKeyboardFocus_14(),
	GUIStyle_t1799908754_StaticFields::get_offset_of_s_None_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (TextClipping_t2573530411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2149[3] = 
{
	TextClipping_t2573530411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (GUITargetAttribute_t863467180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[1] = 
{
	GUITargetAttribute_t863467180::get_offset_of_displayMask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (ExitGUIException_t1618397098), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (GUIUtility_t3275770671), -1, sizeof(GUIUtility_t3275770671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2152[5] = 
{
	GUIUtility_t3275770671_StaticFields::get_offset_of_s_SkinMode_0(),
	GUIUtility_t3275770671_StaticFields::get_offset_of_s_OriginalID_1(),
	GUIUtility_t3275770671_StaticFields::get_offset_of_U3CguiIsExitingU3Ek__BackingField_2(),
	GUIUtility_t3275770671_StaticFields::get_offset_of_s_EditorScreenPointOffset_3(),
	GUIUtility_t3275770671_StaticFields::get_offset_of_s_HasKeyboardFocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (SliderState_t1595681032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[3] = 
{
	SliderState_t1595681032::get_offset_of_dragStartPos_0(),
	SliderState_t1595681032::get_offset_of_dragStartValue_1(),
	SliderState_t1595681032::get_offset_of_isDragging_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (TextEditor_t3975561390), -1, sizeof(TextEditor_t3975561390_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2154[24] = 
{
	TextEditor_t3975561390::get_offset_of_keyboardOnScreen_0(),
	TextEditor_t3975561390::get_offset_of_controlID_1(),
	TextEditor_t3975561390::get_offset_of_style_2(),
	TextEditor_t3975561390::get_offset_of_multiline_3(),
	TextEditor_t3975561390::get_offset_of_hasHorizontalCursorPos_4(),
	TextEditor_t3975561390::get_offset_of_isPasswordField_5(),
	TextEditor_t3975561390::get_offset_of_m_HasFocus_6(),
	TextEditor_t3975561390::get_offset_of_scrollOffset_7(),
	TextEditor_t3975561390::get_offset_of_m_Content_8(),
	TextEditor_t3975561390::get_offset_of_m_Position_9(),
	TextEditor_t3975561390::get_offset_of_m_CursorIndex_10(),
	TextEditor_t3975561390::get_offset_of_m_SelectIndex_11(),
	TextEditor_t3975561390::get_offset_of_m_RevealCursor_12(),
	TextEditor_t3975561390::get_offset_of_graphicalCursorPos_13(),
	TextEditor_t3975561390::get_offset_of_graphicalSelectCursorPos_14(),
	TextEditor_t3975561390::get_offset_of_m_MouseDragSelectsWholeWords_15(),
	TextEditor_t3975561390::get_offset_of_m_DblClickInitPos_16(),
	TextEditor_t3975561390::get_offset_of_m_DblClickSnap_17(),
	TextEditor_t3975561390::get_offset_of_m_bJustSelected_18(),
	TextEditor_t3975561390::get_offset_of_m_iAltCursorPos_19(),
	TextEditor_t3975561390::get_offset_of_oldText_20(),
	TextEditor_t3975561390::get_offset_of_oldPos_21(),
	TextEditor_t3975561390::get_offset_of_oldSelectPos_22(),
	TextEditor_t3975561390_StaticFields::get_offset_of_s_Keyactions_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (DblClickSnapping_t1119726228)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2155[3] = 
{
	DblClickSnapping_t1119726228::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (CharacterType_t593718391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2156[5] = 
{
	CharacterType_t593718391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (TextEditOp_t3138797698)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2157[51] = 
{
	TextEditOp_t3138797698::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (Internal_DrawArguments_t2834709342)+ sizeof (Il2CppObject), sizeof(Internal_DrawArguments_t2834709342 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2158[6] = 
{
	Internal_DrawArguments_t2834709342::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_isHover_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_isActive_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_on_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t2834709342::get_offset_of_hasKeyboardFocus_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (Internal_DrawWithTextSelectionArguments_t1327795077)+ sizeof (Il2CppObject), sizeof(Internal_DrawWithTextSelectionArguments_t1327795077 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2159[11] = 
{
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_firstPos_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_lastPos_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_cursorColor_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_selectionColor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_isHover_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_isActive_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_on_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_hasKeyboardFocus_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t1327795077::get_offset_of_drawSelectionAsComposition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (WebRequestUtils_t4100941042), -1, sizeof(WebRequestUtils_t4100941042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2160[1] = 
{
	WebRequestUtils_t4100941042_StaticFields::get_offset_of_domainRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (Request_t1834273339), -1, sizeof(Request_t1834273339_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2161[5] = 
{
	Request_t1834273339_StaticFields::get_offset_of_currentVersion_0(),
	Request_t1834273339::get_offset_of_U3CsourceIdU3Ek__BackingField_1(),
	Request_t1834273339::get_offset_of_U3CprojectIdU3Ek__BackingField_2(),
	Request_t1834273339::get_offset_of_U3CaccessTokenStringU3Ek__BackingField_3(),
	Request_t1834273339::get_offset_of_U3CdomainU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (ResponseBase_t1952642056), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (Response_t999782913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[2] = 
{
	Response_t999782913::get_offset_of_U3CsuccessU3Ek__BackingField_0(),
	Response_t999782913::get_offset_of_U3CextendedInfoU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (BasicResponse_t1945770051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (CreateMatchRequest_t2247754072), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[8] = 
{
	CreateMatchRequest_t2247754072::get_offset_of_U3CnameU3Ek__BackingField_5(),
	CreateMatchRequest_t2247754072::get_offset_of_U3CsizeU3Ek__BackingField_6(),
	CreateMatchRequest_t2247754072::get_offset_of_U3CpublicAddressU3Ek__BackingField_7(),
	CreateMatchRequest_t2247754072::get_offset_of_U3CprivateAddressU3Ek__BackingField_8(),
	CreateMatchRequest_t2247754072::get_offset_of_U3CeloScoreU3Ek__BackingField_9(),
	CreateMatchRequest_t2247754072::get_offset_of_U3CadvertiseU3Ek__BackingField_10(),
	CreateMatchRequest_t2247754072::get_offset_of_U3CpasswordU3Ek__BackingField_11(),
	CreateMatchRequest_t2247754072::get_offset_of_U3CmatchAttributesU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (CreateMatchResponse_t1656355148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[7] = 
{
	CreateMatchResponse_t1656355148::get_offset_of_U3CaddressU3Ek__BackingField_2(),
	CreateMatchResponse_t1656355148::get_offset_of_U3CportU3Ek__BackingField_3(),
	CreateMatchResponse_t1656355148::get_offset_of_U3CdomainU3Ek__BackingField_4(),
	CreateMatchResponse_t1656355148::get_offset_of_U3CnetworkIdU3Ek__BackingField_5(),
	CreateMatchResponse_t1656355148::get_offset_of_U3CaccessTokenStringU3Ek__BackingField_6(),
	CreateMatchResponse_t1656355148::get_offset_of_U3CnodeIdU3Ek__BackingField_7(),
	CreateMatchResponse_t1656355148::get_offset_of_U3CusingRelayU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (JoinMatchRequest_t2675294872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[5] = 
{
	JoinMatchRequest_t2675294872::get_offset_of_U3CnetworkIdU3Ek__BackingField_5(),
	JoinMatchRequest_t2675294872::get_offset_of_U3CpublicAddressU3Ek__BackingField_6(),
	JoinMatchRequest_t2675294872::get_offset_of_U3CprivateAddressU3Ek__BackingField_7(),
	JoinMatchRequest_t2675294872::get_offset_of_U3CeloScoreU3Ek__BackingField_8(),
	JoinMatchRequest_t2675294872::get_offset_of_U3CpasswordU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (JoinMatchResponse_t1256242636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[7] = 
{
	JoinMatchResponse_t1256242636::get_offset_of_U3CaddressU3Ek__BackingField_2(),
	JoinMatchResponse_t1256242636::get_offset_of_U3CportU3Ek__BackingField_3(),
	JoinMatchResponse_t1256242636::get_offset_of_U3CdomainU3Ek__BackingField_4(),
	JoinMatchResponse_t1256242636::get_offset_of_U3CnetworkIdU3Ek__BackingField_5(),
	JoinMatchResponse_t1256242636::get_offset_of_U3CaccessTokenStringU3Ek__BackingField_6(),
	JoinMatchResponse_t1256242636::get_offset_of_U3CnodeIdU3Ek__BackingField_7(),
	JoinMatchResponse_t1256242636::get_offset_of_U3CusingRelayU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (DropConnectionRequest_t3261607412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[2] = 
{
	DropConnectionRequest_t3261607412::get_offset_of_U3CnetworkIdU3Ek__BackingField_5(),
	DropConnectionRequest_t3261607412::get_offset_of_U3CnodeIdU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (DropConnectionResponse_t3742478148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[2] = 
{
	DropConnectionResponse_t3742478148::get_offset_of_U3CnetworkIdU3Ek__BackingField_2(),
	DropConnectionResponse_t3742478148::get_offset_of_U3CnodeIdU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (ListMatchRequest_t746807584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[8] = 
{
	ListMatchRequest_t746807584::get_offset_of_U3CpageSizeU3Ek__BackingField_5(),
	ListMatchRequest_t746807584::get_offset_of_U3CpageNumU3Ek__BackingField_6(),
	ListMatchRequest_t746807584::get_offset_of_U3CnameFilterU3Ek__BackingField_7(),
	ListMatchRequest_t746807584::get_offset_of_U3CfilterOutPrivateMatchesU3Ek__BackingField_8(),
	ListMatchRequest_t746807584::get_offset_of_U3CeloScoreU3Ek__BackingField_9(),
	ListMatchRequest_t746807584::get_offset_of_U3CmatchAttributeFilterLessThanU3Ek__BackingField_10(),
	ListMatchRequest_t746807584::get_offset_of_U3CmatchAttributeFilterEqualToU3Ek__BackingField_11(),
	ListMatchRequest_t746807584::get_offset_of_U3CmatchAttributeFilterGreaterThanU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (MatchDirectConnectInfo_t3750452272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[4] = 
{
	MatchDirectConnectInfo_t3750452272::get_offset_of_U3CnodeIdU3Ek__BackingField_0(),
	MatchDirectConnectInfo_t3750452272::get_offset_of_U3CpublicAddressU3Ek__BackingField_1(),
	MatchDirectConnectInfo_t3750452272::get_offset_of_U3CprivateAddressU3Ek__BackingField_2(),
	MatchDirectConnectInfo_t3750452272::get_offset_of_U3ChostPriorityU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (MatchDesc_t1011518406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[9] = 
{
	MatchDesc_t1011518406::get_offset_of_U3CnetworkIdU3Ek__BackingField_0(),
	MatchDesc_t1011518406::get_offset_of_U3CnameU3Ek__BackingField_1(),
	MatchDesc_t1011518406::get_offset_of_U3CaverageEloScoreU3Ek__BackingField_2(),
	MatchDesc_t1011518406::get_offset_of_U3CmaxSizeU3Ek__BackingField_3(),
	MatchDesc_t1011518406::get_offset_of_U3CcurrentSizeU3Ek__BackingField_4(),
	MatchDesc_t1011518406::get_offset_of_U3CisPrivateU3Ek__BackingField_5(),
	MatchDesc_t1011518406::get_offset_of_U3CmatchAttributesU3Ek__BackingField_6(),
	MatchDesc_t1011518406::get_offset_of_U3ChostNodeIdU3Ek__BackingField_7(),
	MatchDesc_t1011518406::get_offset_of_U3CdirectConnectInfosU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (ListMatchResponse_t2092237412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[1] = 
{
	ListMatchResponse_t2092237412::get_offset_of_U3CmatchesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (SourceID_t1840552406)+ sizeof (Il2CppObject), sizeof(uint64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2176[2] = 
{
	SourceID_t1840552406::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (NetworkID_t348058649)+ sizeof (Il2CppObject), sizeof(uint64_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2177[2] = 
{
	NetworkID_t348058649::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (NodeID_t3569487935)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2178[2] = 
{
	NodeID_t3569487935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (HostPriority_t2800759508)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2179[2] = 
{
	HostPriority_t2800759508::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (NetworkAccessToken_t2931214851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[2] = 
{
	0,
	NetworkAccessToken_t2931214851::get_offset_of_array_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (Utility_t2692878198), -1, sizeof(Utility_t2692878198_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2181[1] = 
{
	Utility_t2692878198_StaticFields::get_offset_of_s_dictTokens_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (MatchInfo_t668842927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[7] = 
{
	MatchInfo_t668842927::get_offset_of_U3CaddressU3Ek__BackingField_0(),
	MatchInfo_t668842927::get_offset_of_U3CportU3Ek__BackingField_1(),
	MatchInfo_t668842927::get_offset_of_U3CdomainU3Ek__BackingField_2(),
	MatchInfo_t668842927::get_offset_of_U3CnetworkIdU3Ek__BackingField_3(),
	MatchInfo_t668842927::get_offset_of_U3CaccessTokenU3Ek__BackingField_4(),
	MatchInfo_t668842927::get_offset_of_U3CnodeIdU3Ek__BackingField_5(),
	MatchInfo_t668842927::get_offset_of_U3CusingRelayU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (MatchInfoSnapshot_t3179110907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[9] = 
{
	MatchInfoSnapshot_t3179110907::get_offset_of_U3CnetworkIdU3Ek__BackingField_0(),
	MatchInfoSnapshot_t3179110907::get_offset_of_U3ChostNodeIdU3Ek__BackingField_1(),
	MatchInfoSnapshot_t3179110907::get_offset_of_U3CnameU3Ek__BackingField_2(),
	MatchInfoSnapshot_t3179110907::get_offset_of_U3CaverageEloScoreU3Ek__BackingField_3(),
	MatchInfoSnapshot_t3179110907::get_offset_of_U3CmaxSizeU3Ek__BackingField_4(),
	MatchInfoSnapshot_t3179110907::get_offset_of_U3CcurrentSizeU3Ek__BackingField_5(),
	MatchInfoSnapshot_t3179110907::get_offset_of_U3CisPrivateU3Ek__BackingField_6(),
	MatchInfoSnapshot_t3179110907::get_offset_of_U3CmatchAttributesU3Ek__BackingField_7(),
	MatchInfoSnapshot_t3179110907::get_offset_of_U3CdirectConnectInfosU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (MatchInfoDirectConnectSnapshot_t2001338440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[4] = 
{
	MatchInfoDirectConnectSnapshot_t2001338440::get_offset_of_U3CnodeIdU3Ek__BackingField_0(),
	MatchInfoDirectConnectSnapshot_t2001338440::get_offset_of_U3CpublicAddressU3Ek__BackingField_1(),
	MatchInfoDirectConnectSnapshot_t2001338440::get_offset_of_U3CprivateAddressU3Ek__BackingField_2(),
	MatchInfoDirectConnectSnapshot_t2001338440::get_offset_of_U3ChostPriorityU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (NetworkMatch_t259788815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[1] = 
{
	NetworkMatch_t259788815::get_offset_of_m_BaseUri_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (BasicResponseDelegate_t1347328110), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (NetworkTransport_t746957803), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (NetworkEventType_t3809130132)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2191[6] = 
{
	NetworkEventType_t3809130132::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (QosType_t2003892483)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2192[10] = 
{
	QosType_t2003892483::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (NetworkError_t2010663956)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2193[13] = 
{
	NetworkError_t2010663956::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (ReactorModel_t14967895)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2194[3] = 
{
	ReactorModel_t14967895::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (ChannelQOS_t1324696500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[1] = 
{
	ChannelQOS_t1324696500::get_offset_of_m_Type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (ConnectionConfig_t1350910390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[21] = 
{
	0,
	ConnectionConfig_t1350910390::get_offset_of_m_PacketSize_1(),
	ConnectionConfig_t1350910390::get_offset_of_m_FragmentSize_2(),
	ConnectionConfig_t1350910390::get_offset_of_m_ResendTimeout_3(),
	ConnectionConfig_t1350910390::get_offset_of_m_DisconnectTimeout_4(),
	ConnectionConfig_t1350910390::get_offset_of_m_ConnectTimeout_5(),
	ConnectionConfig_t1350910390::get_offset_of_m_MinUpdateTimeout_6(),
	ConnectionConfig_t1350910390::get_offset_of_m_PingTimeout_7(),
	ConnectionConfig_t1350910390::get_offset_of_m_ReducedPingTimeout_8(),
	ConnectionConfig_t1350910390::get_offset_of_m_AllCostTimeout_9(),
	ConnectionConfig_t1350910390::get_offset_of_m_NetworkDropThreshold_10(),
	ConnectionConfig_t1350910390::get_offset_of_m_OverflowDropThreshold_11(),
	ConnectionConfig_t1350910390::get_offset_of_m_MaxConnectionAttempt_12(),
	ConnectionConfig_t1350910390::get_offset_of_m_AckDelay_13(),
	ConnectionConfig_t1350910390::get_offset_of_m_MaxCombinedReliableMessageSize_14(),
	ConnectionConfig_t1350910390::get_offset_of_m_MaxCombinedReliableMessageCount_15(),
	ConnectionConfig_t1350910390::get_offset_of_m_MaxSentMessageQueueSize_16(),
	ConnectionConfig_t1350910390::get_offset_of_m_IsAcksLong_17(),
	ConnectionConfig_t1350910390::get_offset_of_m_UsePlatformSpecificProtocols_18(),
	ConnectionConfig_t1350910390::get_offset_of_m_WebSocketReceiveBufferMaxSize_19(),
	ConnectionConfig_t1350910390::get_offset_of_m_Channels_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (HostTopology_t3602650143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[6] = 
{
	HostTopology_t3602650143::get_offset_of_m_DefConfig_0(),
	HostTopology_t3602650143::get_offset_of_m_MaxDefConnections_1(),
	HostTopology_t3602650143::get_offset_of_m_SpecialConnections_2(),
	HostTopology_t3602650143::get_offset_of_m_ReceivedMessagePoolSize_3(),
	HostTopology_t3602650143::get_offset_of_m_SentMessagePoolSize_4(),
	HostTopology_t3602650143::get_offset_of_m_MessagePoolSizeGrowthFactor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (GlobalConfig_t2977893073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[5] = 
{
	GlobalConfig_t2977893073::get_offset_of_m_ThreadAwakeTimeout_0(),
	GlobalConfig_t2977893073::get_offset_of_m_ReactorModel_1(),
	GlobalConfig_t2977893073::get_offset_of_m_ReactorMaximumReceivedMessages_2(),
	GlobalConfig_t2977893073::get_offset_of_m_ReactorMaximumSentMessages_3(),
	GlobalConfig_t2977893073::get_offset_of_m_MaxPacketSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (ConnectionSimulatorConfig_t3039400316), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
