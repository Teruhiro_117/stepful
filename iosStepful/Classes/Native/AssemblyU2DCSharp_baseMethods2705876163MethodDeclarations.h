﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// baseMethods
struct baseMethods_t2705876163;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void baseMethods::.ctor()
extern "C"  void baseMethods__ctor_m1615982932 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::Init(System.String)
extern "C"  void baseMethods_Init_m3651539526 (baseMethods_t2705876163 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::allPaint()
extern "C"  void baseMethods_allPaint_m4031034429 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean baseMethods::judge()
extern "C"  bool baseMethods_judge_m1432015939 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::getResources(System.Int32)
extern "C"  void baseMethods_getResources_m2257601282 (baseMethods_t2705876163 * __this, int32_t ___resource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::getMyResources(System.Int32)
extern "C"  void baseMethods_getMyResources_m1880650416 (baseMethods_t2705876163 * __this, int32_t ___resource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::getEResources(System.Int32)
extern "C"  void baseMethods_getEResources_m992289761 (baseMethods_t2705876163 * __this, int32_t ___resource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::OffTimeBar(System.Int32,System.Int32)
extern "C"  void baseMethods_OffTimeBar_m3958558077 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::OffTimeBar2(System.Int32,System.Int32)
extern "C"  void baseMethods_OffTimeBar2_m299894443 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::OnTimeBar(System.Int32,System.Int32)
extern "C"  void baseMethods_OnTimeBar_m1542858583 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::OnTimeBar2(System.Int32,System.Int32)
extern "C"  void baseMethods_OnTimeBar2_m345907837 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::printColorBottom(System.Int32,System.Int32)
extern "C"  void baseMethods_printColorBottom_m2694251173 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::printColorAbove(System.Int32,System.Int32)
extern "C"  void baseMethods_printColorAbove_m553988101 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::allPrintColorBottom()
extern "C"  void baseMethods_allPrintColorBottom_m2081970466 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::halfPrintColorBottom(System.Boolean)
extern "C"  void baseMethods_halfPrintColorBottom_m2366856657 (baseMethods_t2705876163 * __this, bool ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::setColorWhite()
extern "C"  void baseMethods_setColorWhite_m2690390344 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::setColorBlack()
extern "C"  void baseMethods_setColorBlack_m980638986 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::setColorRed()
extern "C"  void baseMethods_setColorRed_m3950151350 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::setColorBlue()
extern "C"  void baseMethods_setColorBlue_m4082331027 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::setColorGreen()
extern "C"  void baseMethods_setColorGreen_m2130263350 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::setColorClear()
extern "C"  void baseMethods_setColorClear_m4166068504 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::setRedWhite()
extern "C"  void baseMethods_setRedWhite_m3951861276 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::lineRow(System.Int32,System.Int32)
extern "C"  void baseMethods_lineRow_m2784445234 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::canTouchAll(System.Boolean)
extern "C"  void baseMethods_canTouchAll_m280133149 (baseMethods_t2705876163 * __this, bool ___TF0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::canTouchArrangeMode()
extern "C"  void baseMethods_canTouchArrangeMode_m1634389688 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::canTouchHalf()
extern "C"  void baseMethods_canTouchHalf_m1396259078 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::cloudy()
extern "C"  void baseMethods_cloudy_m3964171594 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::nonSatelite()
extern "C"  void baseMethods_nonSatelite_m140178688 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::ClearEPlace()
extern "C"  void baseMethods_ClearEPlace_m1537656273 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::OnSatelite()
extern "C"  void baseMethods_OnSatelite_m2024278172 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void baseMethods::DestroySatelite()
extern "C"  void baseMethods_DestroySatelite_m3304756467 (baseMethods_t2705876163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
