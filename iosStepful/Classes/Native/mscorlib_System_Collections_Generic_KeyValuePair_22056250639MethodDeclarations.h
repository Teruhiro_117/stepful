﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22979060144MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,UnityEngine.Networking.NetworkIdentity>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m619444955(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2056250639 *, NetworkInstanceId_t33998832 , NetworkIdentity_t1766639790 *, const MethodInfo*))KeyValuePair_2__ctor_m294348854_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,UnityEngine.Networking.NetworkIdentity>::get_Key()
#define KeyValuePair_2_get_Key_m759726017(__this, method) ((  NetworkInstanceId_t33998832  (*) (KeyValuePair_2_t2056250639 *, const MethodInfo*))KeyValuePair_2_get_Key_m2311441456_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,UnityEngine.Networking.NetworkIdentity>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m41553026(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2056250639 *, NetworkInstanceId_t33998832 , const MethodInfo*))KeyValuePair_2_set_Key_m3463227267_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,UnityEngine.Networking.NetworkIdentity>::get_Value()
#define KeyValuePair_2_get_Value_m1792769825(__this, method) ((  NetworkIdentity_t1766639790 * (*) (KeyValuePair_2_t2056250639 *, const MethodInfo*))KeyValuePair_2_get_Value_m1496119512_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,UnityEngine.Networking.NetworkIdentity>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1622485738(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2056250639 *, NetworkIdentity_t1766639790 *, const MethodInfo*))KeyValuePair_2_set_Value_m1789552059_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.NetworkInstanceId,UnityEngine.Networking.NetworkIdentity>::ToString()
#define KeyValuePair_2_ToString_m1916176918(__this, method) ((  String_t* (*) (KeyValuePair_2_t2056250639 *, const MethodInfo*))KeyValuePair_2_ToString_m147270409_gshared)(__this, method)
