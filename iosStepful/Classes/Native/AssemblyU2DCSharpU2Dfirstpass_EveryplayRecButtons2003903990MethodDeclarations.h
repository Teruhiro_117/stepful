﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayRecButtons
struct EveryplayRecButtons_t2003903990;
// EveryplayRecButtons/Button
struct Button_t3262243951;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_3262243951.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void EveryplayRecButtons::.ctor()
extern "C"  void EveryplayRecButtons__ctor_m553216977 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::Awake()
extern "C"  void EveryplayRecButtons_Awake_m3555848656 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::Destroy()
extern "C"  void EveryplayRecButtons_Destroy_m1427959579 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::SetButtonVisible(EveryplayRecButtons/Button,System.Boolean)
extern "C"  void EveryplayRecButtons_SetButtonVisible_m3014996681 (EveryplayRecButtons_t2003903990 * __this, Button_t3262243951 * ___button0, bool ___visible1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::ReplaceVisibleButton(EveryplayRecButtons/Button,EveryplayRecButtons/Button)
extern "C"  void EveryplayRecButtons_ReplaceVisibleButton_m2850053193 (EveryplayRecButtons_t2003903990 * __this, Button_t3262243951 * ___button0, Button_t3262243951 * ___replacementButton1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::StartRecording()
extern "C"  void EveryplayRecButtons_StartRecording_m1425756496 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::StopRecording()
extern "C"  void EveryplayRecButtons_StopRecording_m1817812284 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::RecordingStarted()
extern "C"  void EveryplayRecButtons_RecordingStarted_m2508165025 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::RecordingStopped()
extern "C"  void EveryplayRecButtons_RecordingStopped_m2345808949 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::ReadyForRecording(System.Boolean)
extern "C"  void EveryplayRecButtons_ReadyForRecording_m1360139799 (EveryplayRecButtons_t2003903990 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::FaceCamRecordingPermission(System.Boolean)
extern "C"  void EveryplayRecButtons_FaceCamRecordingPermission_m3188918626 (EveryplayRecButtons_t2003903990 * __this, bool ___granted0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::FaceCamToggle()
extern "C"  void EveryplayRecButtons_FaceCamToggle_m1400819525 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::OpenEveryplay()
extern "C"  void EveryplayRecButtons_OpenEveryplay_m4060651158 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::EditVideo()
extern "C"  void EveryplayRecButtons_EditVideo_m3846525876 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::ShareVideo()
extern "C"  void EveryplayRecButtons_ShareVideo_m1973277741 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::Update()
extern "C"  void EveryplayRecButtons_Update_m1198130542 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::OnGUI()
extern "C"  void EveryplayRecButtons_OnGUI_m410492671 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::DrawTexture(System.Single,System.Single,System.Single,System.Single,UnityEngine.Texture2D,UnityEngine.Rect)
extern "C"  void EveryplayRecButtons_DrawTexture_m925030179 (EveryplayRecButtons_t2003903990 * __this, float ___x0, float ___y1, float ___width2, float ___height3, Texture2D_t3542995729 * ___texture4, Rect_t3681755626  ___uvRect5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::DrawButtons()
extern "C"  void EveryplayRecButtons_DrawButtons_m2512355848 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::DrawBackround(System.Int32)
extern "C"  void EveryplayRecButtons_DrawBackround_m2870053337 (EveryplayRecButtons_t2003903990 * __this, int32_t ___containerHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::DrawButton(EveryplayRecButtons/Button,UnityEngine.Color)
extern "C"  void EveryplayRecButtons_DrawButton_m4200336054 (EveryplayRecButtons_t2003903990 * __this, Button_t3262243951 * ___button0, Color_t2020392075  ___tintColor1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EveryplayRecButtons::CalculateContainerHeight()
extern "C"  int32_t EveryplayRecButtons_CalculateContainerHeight_m449657747 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayRecButtons::UpdateContainerOffset(System.Int32)
extern "C"  void EveryplayRecButtons_UpdateContainerOffset_m1773314275 (EveryplayRecButtons_t2003903990 * __this, int32_t ___containerHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single EveryplayRecButtons::GetScalingByResolution()
extern "C"  float EveryplayRecButtons_GetScalingByResolution_m2106876889 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
