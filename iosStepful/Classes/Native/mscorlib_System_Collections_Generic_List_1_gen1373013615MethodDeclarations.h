﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.QosType>
struct List_1_t1373013615;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.QosType>
struct IEnumerable_1_t2296019528;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.QosType>
struct IEnumerator_1_t3774383606;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.QosType>
struct ICollection_1_t2955967788;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>
struct ReadOnlyCollection_1_t2189678175;
// UnityEngine.Networking.QosType[]
struct QosTypeU5BU5D_t740748178;
// System.Predicate`1<UnityEngine.Networking.QosType>
struct Predicate_1_t446862598;
// System.Collections.Generic.IComparer`1<UnityEngine.Networking.QosType>
struct IComparer_1_t4253322901;
// System.Comparison`1<UnityEngine.Networking.QosType>
struct Comparison_1_t3265631334;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Networking_QosType2003892483.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat907743289.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::.ctor()
extern "C"  void List_1__ctor_m1400939642_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1__ctor_m1400939642(__this, method) ((  void (*) (List_1_t1373013615 *, const MethodInfo*))List_1__ctor_m1400939642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3575555791_gshared (List_1_t1373013615 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3575555791(__this, ___collection0, method) ((  void (*) (List_1_t1373013615 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3575555791_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2681613925_gshared (List_1_t1373013615 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m2681613925(__this, ___capacity0, method) ((  void (*) (List_1_t1373013615 *, int32_t, const MethodInfo*))List_1__ctor_m2681613925_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::.cctor()
extern "C"  void List_1__cctor_m1573670607_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1573670607(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1573670607_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2830964798_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2830964798(__this, method) ((  Il2CppObject* (*) (List_1_t1373013615 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2830964798_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1563763124_gshared (List_1_t1373013615 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1563763124(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1373013615 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1563763124_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m853205705_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m853205705(__this, method) ((  Il2CppObject * (*) (List_1_t1373013615 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m853205705_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m890155504_gshared (List_1_t1373013615 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m890155504(__this, ___item0, method) ((  int32_t (*) (List_1_t1373013615 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m890155504_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1128738682_gshared (List_1_t1373013615 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1128738682(__this, ___item0, method) ((  bool (*) (List_1_t1373013615 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1128738682_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m213313562_gshared (List_1_t1373013615 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m213313562(__this, ___item0, method) ((  int32_t (*) (List_1_t1373013615 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m213313562_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2484680133_gshared (List_1_t1373013615 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2484680133(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1373013615 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2484680133_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3878468309_gshared (List_1_t1373013615 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3878468309(__this, ___item0, method) ((  void (*) (List_1_t1373013615 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3878468309_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2660321593_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2660321593(__this, method) ((  bool (*) (List_1_t1373013615 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2660321593_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3958190676_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3958190676(__this, method) ((  bool (*) (List_1_t1373013615 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3958190676_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m256298074_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m256298074(__this, method) ((  Il2CppObject * (*) (List_1_t1373013615 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m256298074_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1699075161_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1699075161(__this, method) ((  bool (*) (List_1_t1373013615 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1699075161_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3956876280_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3956876280(__this, method) ((  bool (*) (List_1_t1373013615 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3956876280_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1721316445_gshared (List_1_t1373013615 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1721316445(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1373013615 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1721316445_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3569717126_gshared (List_1_t1373013615 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3569717126(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1373013615 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3569717126_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::Add(T)
extern "C"  void List_1_Add_m153760161_gshared (List_1_t1373013615 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m153760161(__this, ___item0, method) ((  void (*) (List_1_t1373013615 *, int32_t, const MethodInfo*))List_1_Add_m153760161_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1242922094_gshared (List_1_t1373013615 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1242922094(__this, ___newCount0, method) ((  void (*) (List_1_t1373013615 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1242922094_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1051480582_gshared (List_1_t1373013615 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m1051480582(__this, ___collection0, method) ((  void (*) (List_1_t1373013615 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1051480582_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3611682918_gshared (List_1_t1373013615 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3611682918(__this, ___enumerable0, method) ((  void (*) (List_1_t1373013615 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3611682918_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3307388645_gshared (List_1_t1373013615 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3307388645(__this, ___collection0, method) ((  void (*) (List_1_t1373013615 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3307388645_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2189678175 * List_1_AsReadOnly_m3656198296_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3656198296(__this, method) ((  ReadOnlyCollection_1_t2189678175 * (*) (List_1_t1373013615 *, const MethodInfo*))List_1_AsReadOnly_m3656198296_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::Clear()
extern "C"  void List_1_Clear_m2524904253_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_Clear_m2524904253(__this, method) ((  void (*) (List_1_t1373013615 *, const MethodInfo*))List_1_Clear_m2524904253_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::Contains(T)
extern "C"  bool List_1_Contains_m3212181431_gshared (List_1_t1373013615 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3212181431(__this, ___item0, method) ((  bool (*) (List_1_t1373013615 *, int32_t, const MethodInfo*))List_1_Contains_m3212181431_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3818437305_gshared (List_1_t1373013615 * __this, QosTypeU5BU5D_t740748178* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3818437305(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1373013615 *, QosTypeU5BU5D_t740748178*, int32_t, const MethodInfo*))List_1_CopyTo_m3818437305_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m1556896711_gshared (List_1_t1373013615 * __this, Predicate_1_t446862598 * ___match0, const MethodInfo* method);
#define List_1_Find_m1556896711(__this, ___match0, method) ((  int32_t (*) (List_1_t1373013615 *, Predicate_1_t446862598 *, const MethodInfo*))List_1_Find_m1556896711_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1765487620_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t446862598 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1765487620(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t446862598 *, const MethodInfo*))List_1_CheckMatch_m1765487620_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m4272800239_gshared (List_1_t1373013615 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t446862598 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m4272800239(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1373013615 *, int32_t, int32_t, Predicate_1_t446862598 *, const MethodInfo*))List_1_GetIndex_m4272800239_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::GetEnumerator()
extern "C"  Enumerator_t907743289  List_1_GetEnumerator_m4176427616_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m4176427616(__this, method) ((  Enumerator_t907743289  (*) (List_1_t1373013615 *, const MethodInfo*))List_1_GetEnumerator_m4176427616_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2960815569_gshared (List_1_t1373013615 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2960815569(__this, ___item0, method) ((  int32_t (*) (List_1_t1373013615 *, int32_t, const MethodInfo*))List_1_IndexOf_m2960815569_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m273492368_gshared (List_1_t1373013615 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m273492368(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1373013615 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m273492368_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2655752061_gshared (List_1_t1373013615 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2655752061(__this, ___index0, method) ((  void (*) (List_1_t1373013615 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2655752061_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m4026180094_gshared (List_1_t1373013615 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m4026180094(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1373013615 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m4026180094_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m977534867_gshared (List_1_t1373013615 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m977534867(__this, ___collection0, method) ((  void (*) (List_1_t1373013615 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m977534867_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::Remove(T)
extern "C"  bool List_1_Remove_m1909029144_gshared (List_1_t1373013615 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m1909029144(__this, ___item0, method) ((  bool (*) (List_1_t1373013615 *, int32_t, const MethodInfo*))List_1_Remove_m1909029144_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2995280168_gshared (List_1_t1373013615 * __this, Predicate_1_t446862598 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2995280168(__this, ___match0, method) ((  int32_t (*) (List_1_t1373013615 *, Predicate_1_t446862598 *, const MethodInfo*))List_1_RemoveAll_m2995280168_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m92427370_gshared (List_1_t1373013615 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m92427370(__this, ___index0, method) ((  void (*) (List_1_t1373013615 *, int32_t, const MethodInfo*))List_1_RemoveAt_m92427370_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::Reverse()
extern "C"  void List_1_Reverse_m2665508458_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_Reverse_m2665508458(__this, method) ((  void (*) (List_1_t1373013615 *, const MethodInfo*))List_1_Reverse_m2665508458_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::Sort()
extern "C"  void List_1_Sort_m1583579178_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_Sort_m1583579178(__this, method) ((  void (*) (List_1_t1373013615 *, const MethodInfo*))List_1_Sort_m1583579178_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3994926878_gshared (List_1_t1373013615 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3994926878(__this, ___comparer0, method) ((  void (*) (List_1_t1373013615 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3994926878_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2239509463_gshared (List_1_t1373013615 * __this, Comparison_1_t3265631334 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2239509463(__this, ___comparison0, method) ((  void (*) (List_1_t1373013615 *, Comparison_1_t3265631334 *, const MethodInfo*))List_1_Sort_m2239509463_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::ToArray()
extern "C"  QosTypeU5BU5D_t740748178* List_1_ToArray_m1548935739_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_ToArray_m1548935739(__this, method) ((  QosTypeU5BU5D_t740748178* (*) (List_1_t1373013615 *, const MethodInfo*))List_1_ToArray_m1548935739_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1782528305_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1782528305(__this, method) ((  void (*) (List_1_t1373013615 *, const MethodInfo*))List_1_TrimExcess_m1782528305_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m972347887_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m972347887(__this, method) ((  int32_t (*) (List_1_t1373013615 *, const MethodInfo*))List_1_get_Capacity_m972347887_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m127679846_gshared (List_1_t1373013615 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m127679846(__this, ___value0, method) ((  void (*) (List_1_t1373013615 *, int32_t, const MethodInfo*))List_1_set_Capacity_m127679846_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::get_Count()
extern "C"  int32_t List_1_get_Count_m125205506_gshared (List_1_t1373013615 * __this, const MethodInfo* method);
#define List_1_get_Count_m125205506(__this, method) ((  int32_t (*) (List_1_t1373013615 *, const MethodInfo*))List_1_get_Count_m125205506_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m4175032553_gshared (List_1_t1373013615 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4175032553(__this, ___index0, method) ((  int32_t (*) (List_1_t1373013615 *, int32_t, const MethodInfo*))List_1_get_Item_m4175032553_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.QosType>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1900485169_gshared (List_1_t1373013615 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m1900485169(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1373013615 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m1900485169_gshared)(__this, ___index0, ___value1, method)
