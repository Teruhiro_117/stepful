﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot
struct MatchInfoDirectConnectSnapshot_t2001338440;
// UnityEngine.Networking.Match.MatchDirectConnectInfo
struct MatchDirectConnectInfo_t3750452272;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_Match_MatchDire3750452272.h"
#include "UnityEngine_UnityEngine_Networking_Types_NodeID3569487935.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Networking_Types_HostPrior2800759508.h"

// System.Void UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot::.ctor(UnityEngine.Networking.Match.MatchDirectConnectInfo)
extern "C"  void MatchInfoDirectConnectSnapshot__ctor_m2398530466 (MatchInfoDirectConnectSnapshot_t2001338440 * __this, MatchDirectConnectInfo_t3750452272 * ___matchDirectConnectInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern "C"  void MatchInfoDirectConnectSnapshot_set_nodeId_m105089935 (MatchInfoDirectConnectSnapshot_t2001338440 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot::set_publicAddress(System.String)
extern "C"  void MatchInfoDirectConnectSnapshot_set_publicAddress_m3523876248 (MatchInfoDirectConnectSnapshot_t2001338440 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot::set_privateAddress(System.String)
extern "C"  void MatchInfoDirectConnectSnapshot_set_privateAddress_m574864894 (MatchInfoDirectConnectSnapshot_t2001338440 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.MatchInfoSnapshot/MatchInfoDirectConnectSnapshot::set_hostPriority(UnityEngine.Networking.Types.HostPriority)
extern "C"  void MatchInfoDirectConnectSnapshot_set_hostPriority_m763233711 (MatchInfoDirectConnectSnapshot_t2001338440 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
