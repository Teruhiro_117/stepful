﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<EveryplayRecButtons/Button>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3461568574(__this, ___l0, method) ((  void (*) (Enumerator_t2166094757 *, List_1_t2631365083 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<EveryplayRecButtons/Button>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1107304812(__this, method) ((  void (*) (Enumerator_t2166094757 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<EveryplayRecButtons/Button>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2163355122(__this, method) ((  Il2CppObject * (*) (Enumerator_t2166094757 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<EveryplayRecButtons/Button>::Dispose()
#define Enumerator_Dispose_m463829143(__this, method) ((  void (*) (Enumerator_t2166094757 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<EveryplayRecButtons/Button>::VerifyState()
#define Enumerator_VerifyState_m1175579416(__this, method) ((  void (*) (Enumerator_t2166094757 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<EveryplayRecButtons/Button>::MoveNext()
#define Enumerator_MoveNext_m3703810831(__this, method) ((  bool (*) (Enumerator_t2166094757 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<EveryplayRecButtons/Button>::get_Current()
#define Enumerator_get_Current_m3398575995(__this, method) ((  Button_t3262243951 * (*) (Enumerator_t2166094757 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
