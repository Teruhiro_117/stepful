﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<UnityEngine.Networking.QosType>
struct Comparer_1_t893901602;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Networking.QosType>::.ctor()
extern "C"  void Comparer_1__ctor_m3454026667_gshared (Comparer_1_t893901602 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m3454026667(__this, method) ((  void (*) (Comparer_1_t893901602 *, const MethodInfo*))Comparer_1__ctor_m3454026667_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Networking.QosType>::.cctor()
extern "C"  void Comparer_1__cctor_m1204769612_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m1204769612(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m1204769612_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Networking.QosType>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m347179158_gshared (Comparer_1_t893901602 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m347179158(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t893901602 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m347179158_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Networking.QosType>::get_Default()
extern "C"  Comparer_1_t893901602 * Comparer_1_get_Default_m506115963_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m506115963(__this /* static, unused */, method) ((  Comparer_1_t893901602 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m506115963_gshared)(__this /* static, unused */, method)
