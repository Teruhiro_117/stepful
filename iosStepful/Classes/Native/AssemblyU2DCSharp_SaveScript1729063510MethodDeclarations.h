﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SaveScript
struct SaveScript_t1729063510;

#include "codegen/il2cpp-codegen.h"

// System.Void SaveScript::.ctor()
extern "C"  void SaveScript__ctor_m3581886929 (SaveScript_t1729063510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveScript::SaveText()
extern "C"  void SaveScript_SaveText_m374362473 (SaveScript_t1729063510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
