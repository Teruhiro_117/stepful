﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/UploadDidCompleteDelegate
struct UploadDidCompleteDelegate_t1564565876;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/UploadDidCompleteDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void UploadDidCompleteDelegate__ctor_m3967701987 (UploadDidCompleteDelegate_t1564565876 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/UploadDidCompleteDelegate::Invoke(System.Int32)
extern "C"  void UploadDidCompleteDelegate_Invoke_m3224334404 (UploadDidCompleteDelegate_t1564565876 * __this, int32_t ___videoId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/UploadDidCompleteDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UploadDidCompleteDelegate_BeginInvoke_m3582390865 (UploadDidCompleteDelegate_t1564565876 * __this, int32_t ___videoId0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/UploadDidCompleteDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void UploadDidCompleteDelegate_EndInvoke_m1332152417 (UploadDidCompleteDelegate_t1564565876 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
