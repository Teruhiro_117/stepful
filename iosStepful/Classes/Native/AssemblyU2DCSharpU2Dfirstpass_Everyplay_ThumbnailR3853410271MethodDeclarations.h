﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/ThumbnailReadyAtTextureIdDelegate
struct ThumbnailReadyAtTextureIdDelegate_t3853410271;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/ThumbnailReadyAtTextureIdDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ThumbnailReadyAtTextureIdDelegate__ctor_m818508762 (ThumbnailReadyAtTextureIdDelegate_t3853410271 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/ThumbnailReadyAtTextureIdDelegate::Invoke(System.Int32,System.Boolean)
extern "C"  void ThumbnailReadyAtTextureIdDelegate_Invoke_m419038684 (ThumbnailReadyAtTextureIdDelegate_t3853410271 * __this, int32_t ___textureId0, bool ___portrait1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/ThumbnailReadyAtTextureIdDelegate::BeginInvoke(System.Int32,System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ThumbnailReadyAtTextureIdDelegate_BeginInvoke_m1723076673 (ThumbnailReadyAtTextureIdDelegate_t3853410271 * __this, int32_t ___textureId0, bool ___portrait1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/ThumbnailReadyAtTextureIdDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ThumbnailReadyAtTextureIdDelegate_EndInvoke_m2924983148 (ThumbnailReadyAtTextureIdDelegate_t3853410271 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
