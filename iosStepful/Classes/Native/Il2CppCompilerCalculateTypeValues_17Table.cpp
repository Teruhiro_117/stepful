﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Configuration_NetConfigurationHan415947373.h"
#include "System_System_Net_Configuration_NetSectionGroup2546413291.h"
#include "System_System_Net_Configuration_PerformanceCounter1606215027.h"
#include "System_System_Net_Configuration_ProxyElement1414493002.h"
#include "System_System_Net_Configuration_ProxyElement_Bypas3271087145.h"
#include "System_System_Net_Configuration_ProxyElement_UseSy1457033350.h"
#include "System_System_Net_Configuration_ProxyElement_AutoDe735769531.h"
#include "System_System_Net_Configuration_RequestCachingSect1642835391.h"
#include "System_System_Net_Configuration_ServicePointManage1132364388.h"
#include "System_System_Net_Configuration_SettingsSection2300716058.h"
#include "System_System_Net_Configuration_SocketElement792962077.h"
#include "System_System_Net_Configuration_WebProxyScriptElem1017943775.h"
#include "System_System_Net_Configuration_WebRequestModuleEl2218695785.h"
#include "System_System_Net_Configuration_WebRequestModuleEl4070853259.h"
#include "System_System_Net_Configuration_WebRequestModuleHa3598459681.h"
#include "System_System_Net_Configuration_WebRequestModulesS3717257007.h"
#include "System_System_Net_CookieCollection521422364.h"
#include "System_System_Net_CookieCollection_CookieCollectio3570802680.h"
#include "System_System_Net_CookieContainer2808809223.h"
#include "System_System_Net_Cookie3154017544.h"
#include "System_System_Net_CookieException1505724635.h"
#include "System_System_Net_DecompressionMethods2530166567.h"
#include "System_System_Net_DefaultCertificatePolicy2545332216.h"
#include "System_System_Net_Dns1335526197.h"
#include "System_System_Net_Dns_GetHostAddressesCallback1126586509.h"
#include "System_System_Net_EndPoint4156119363.h"
#include "System_System_Net_FileWebRequestCreator1109072211.h"
#include "System_System_Net_FileWebRequest1571840111.h"
#include "System_System_Net_FileWebRequest_FileWebStream1952319648.h"
#include "System_System_Net_FileWebRequest_GetResponseCallba3725471744.h"
#include "System_System_Net_FileWebResponse1934981865.h"
#include "System_System_Net_FtpAsyncResult770082413.h"
#include "System_System_Net_FtpDataStream3588258764.h"
#include "System_System_Net_FtpDataStream_WriteDelegate888270799.h"
#include "System_System_Net_FtpDataStream_ReadDelegate1559754630.h"
#include "System_System_Net_FtpRequestCreator3711983251.h"
#include "System_System_Net_FtpStatusCode1448112771.h"
#include "System_System_Net_FtpWebRequest3120721823.h"
#include "System_System_Net_FtpWebRequest_RequestState4256633122.h"
#include "System_System_Net_FtpStatus3714482970.h"
#include "System_System_Net_FtpWebResponse2609078769.h"
#include "System_System_Net_GlobalProxySelection2251180943.h"
#include "System_System_Net_HttpRequestCreator1416559589.h"
#include "System_System_Net_HttpStatusCode1898409641.h"
#include "System_System_Net_HttpVersion1276659706.h"
#include "System_System_Net_HttpWebRequest1951404513.h"
#include "System_System_Net_HttpWebResponse2828383075.h"
#include "System_System_Net_CookieParser1405985527.h"
#include "System_System_Net_IPAddress1399971723.h"
#include "System_System_Net_IPEndPoint2615413766.h"
#include "System_System_Net_IPHostEntry994738509.h"
#include "System_System_Net_IPv6Address2596635879.h"
#include "System_System_Net_NetConfig1609737885.h"
#include "System_System_Net_NetworkCredential1714133953.h"
#include "System_System_Net_ProtocolViolationException4263317570.h"
#include "System_System_Net_Security_AuthenticatedStream1183414097.h"
#include "System_System_Net_Security_AuthenticationLevel2424130044.h"
#include "System_System_Net_SecurityProtocolType3099771628.h"
#include "System_System_Net_Security_SslStream1853163792.h"
#include "System_System_Net_Security_SslStream_U3CBeginAuthe1358332250.h"
#include "System_System_Net_Security_SslPolicyErrors1928581431.h"
#include "System_System_Net_ServicePoint2765344313.h"
#include "System_System_Net_ServicePointManager745663000.h"
#include "System_System_Net_ServicePointManager_SPKey1552752485.h"
#include "System_System_Net_ServicePointManager_ChainValidat1155887809.h"
#include "System_System_Net_SocketAddress838303055.h"
#include "System_System_Net_Sockets_AddressFamily303362630.h"
#include "System_System_Net_Sockets_LingerOption1165263720.h"
#include "System_System_Net_Sockets_MulticastOption2505469155.h"
#include "System_System_Net_Sockets_NetworkStream581172200.h"
#include "System_System_Net_Sockets_ProtocolType2178963134.h"
#include "System_System_Net_Sockets_SelectMode3413969319.h"
#include "System_System_Net_Sockets_Socket3821512045.h"
#include "System_System_Net_Sockets_Socket_SocketOperation3328960782.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncResult2959281146.h"
#include "System_System_Net_Sockets_Socket_Worker1317165246.h"
#include "System_System_Net_Sockets_Socket_SocketAsyncCall3737776727.h"
#include "System_System_Net_Sockets_SocketError307542793.h"
#include "System_System_Net_Sockets_SocketException1618573604.h"
#include "System_System_Net_Sockets_SocketFlags2353657790.h"
#include "System_System_Net_Sockets_SocketOptionLevel1505247880.h"
#include "System_System_Net_Sockets_SocketOptionName1089121285.h"
#include "System_System_Net_Sockets_SocketShutdown3247039417.h"
#include "System_System_Net_Sockets_SocketType1143498533.h"
#include "System_System_Net_WebAsyncResult905414499.h"
#include "System_System_Net_ReadState657568301.h"
#include "System_System_Net_WebConnection324679648.h"
#include "System_System_Net_WebConnection_AbortHelper2895113041.h"
#include "System_System_Net_WebConnectionData3550260432.h"
#include "System_System_Net_WebConnectionGroup3242458773.h"
#include "System_System_Net_WebConnectionStream1922483508.h"
#include "System_System_Net_WebException3368933679.h"
#include "System_System_Net_WebExceptionStatus1169373531.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (NetConfigurationHandler_t415947373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (NetSectionGroup_t2546413291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (PerformanceCountersElement_t1606215027), -1, sizeof(PerformanceCountersElement_t1606215027_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1702[2] = 
{
	PerformanceCountersElement_t1606215027_StaticFields::get_offset_of_enabledProp_13(),
	PerformanceCountersElement_t1606215027_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (ProxyElement_t1414493002), -1, sizeof(ProxyElement_t1414493002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1703[6] = 
{
	ProxyElement_t1414493002_StaticFields::get_offset_of_properties_13(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_autoDetectProp_14(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_bypassOnLocalProp_15(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_proxyAddressProp_16(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_scriptLocationProp_17(),
	ProxyElement_t1414493002_StaticFields::get_offset_of_useSystemDefaultProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (BypassOnLocalValues_t3271087145)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1704[4] = 
{
	BypassOnLocalValues_t3271087145::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (UseSystemDefaultValues_t1457033350)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1705[4] = 
{
	UseSystemDefaultValues_t1457033350::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (AutoDetectValues_t735769531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1706[4] = 
{
	AutoDetectValues_t735769531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (RequestCachingSection_t1642835391), -1, sizeof(RequestCachingSection_t1642835391_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1707[7] = 
{
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_properties_17(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_defaultFtpCachePolicyProp_18(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_defaultHttpCachePolicyProp_19(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_defaultPolicyLevelProp_20(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_disableAllCachingProp_21(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_isPrivateCacheProp_22(),
	RequestCachingSection_t1642835391_StaticFields::get_offset_of_unspecifiedMaximumAgeProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (ServicePointManagerElement_t1132364388), -1, sizeof(ServicePointManagerElement_t1132364388_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1708[7] = 
{
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_properties_13(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_checkCertificateNameProp_14(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_checkCertificateRevocationListProp_15(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_dnsRefreshTimeoutProp_16(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_enableDnsRoundRobinProp_17(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_expect100ContinueProp_18(),
	ServicePointManagerElement_t1132364388_StaticFields::get_offset_of_useNagleAlgorithmProp_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (SettingsSection_t2300716058), -1, sizeof(SettingsSection_t2300716058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1709[7] = 
{
	SettingsSection_t2300716058_StaticFields::get_offset_of_properties_17(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_httpWebRequestProp_18(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_ipv6Prop_19(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_performanceCountersProp_20(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_servicePointManagerProp_21(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_webProxyScriptProp_22(),
	SettingsSection_t2300716058_StaticFields::get_offset_of_socketProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (SocketElement_t792962077), -1, sizeof(SocketElement_t792962077_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1710[3] = 
{
	SocketElement_t792962077_StaticFields::get_offset_of_properties_13(),
	SocketElement_t792962077_StaticFields::get_offset_of_alwaysUseCompletionPortsForAcceptProp_14(),
	SocketElement_t792962077_StaticFields::get_offset_of_alwaysUseCompletionPortsForConnectProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (WebProxyScriptElement_t1017943775), -1, sizeof(WebProxyScriptElement_t1017943775_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1711[2] = 
{
	WebProxyScriptElement_t1017943775_StaticFields::get_offset_of_downloadTimeoutProp_13(),
	WebProxyScriptElement_t1017943775_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (WebRequestModuleElementCollection_t2218695785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (WebRequestModuleElement_t4070853259), -1, sizeof(WebRequestModuleElement_t4070853259_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1713[3] = 
{
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_properties_13(),
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_prefixProp_14(),
	WebRequestModuleElement_t4070853259_StaticFields::get_offset_of_typeProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (WebRequestModuleHandler_t3598459681), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (WebRequestModulesSection_t3717257007), -1, sizeof(WebRequestModulesSection_t3717257007_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1715[2] = 
{
	WebRequestModulesSection_t3717257007_StaticFields::get_offset_of_properties_17(),
	WebRequestModulesSection_t3717257007_StaticFields::get_offset_of_webRequestModulesProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (CookieCollection_t521422364), -1, sizeof(CookieCollection_t521422364_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1716[2] = 
{
	CookieCollection_t521422364::get_offset_of_list_0(),
	CookieCollection_t521422364_StaticFields::get_offset_of_Comparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (CookieCollectionComparer_t3570802680), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (CookieContainer_t2808809223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[4] = 
{
	CookieContainer_t2808809223::get_offset_of_capacity_0(),
	CookieContainer_t2808809223::get_offset_of_perDomainCapacity_1(),
	CookieContainer_t2808809223::get_offset_of_maxCookieSize_2(),
	CookieContainer_t2808809223::get_offset_of_cookies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (Cookie_t3154017544), -1, sizeof(Cookie_t3154017544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1719[18] = 
{
	Cookie_t3154017544::get_offset_of_comment_0(),
	Cookie_t3154017544::get_offset_of_commentUri_1(),
	Cookie_t3154017544::get_offset_of_discard_2(),
	Cookie_t3154017544::get_offset_of_domain_3(),
	Cookie_t3154017544::get_offset_of_expires_4(),
	Cookie_t3154017544::get_offset_of_httpOnly_5(),
	Cookie_t3154017544::get_offset_of_name_6(),
	Cookie_t3154017544::get_offset_of_path_7(),
	Cookie_t3154017544::get_offset_of_port_8(),
	Cookie_t3154017544::get_offset_of_ports_9(),
	Cookie_t3154017544::get_offset_of_secure_10(),
	Cookie_t3154017544::get_offset_of_timestamp_11(),
	Cookie_t3154017544::get_offset_of_val_12(),
	Cookie_t3154017544::get_offset_of_version_13(),
	Cookie_t3154017544_StaticFields::get_offset_of_reservedCharsName_14(),
	Cookie_t3154017544_StaticFields::get_offset_of_portSeparators_15(),
	Cookie_t3154017544_StaticFields::get_offset_of_tspecials_16(),
	Cookie_t3154017544::get_offset_of_exact_domain_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (CookieException_t1505724635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (DecompressionMethods_t2530166567)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1721[4] = 
{
	DecompressionMethods_t2530166567::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (DefaultCertificatePolicy_t2545332216), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (Dns_t1335526197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (GetHostAddressesCallback_t1126586509), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (EndPoint_t4156119363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (FileWebRequestCreator_t1109072211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (FileWebRequest_t1571840111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[14] = 
{
	FileWebRequest_t1571840111::get_offset_of_uri_6(),
	FileWebRequest_t1571840111::get_offset_of_webHeaders_7(),
	FileWebRequest_t1571840111::get_offset_of_credentials_8(),
	FileWebRequest_t1571840111::get_offset_of_connectionGroup_9(),
	FileWebRequest_t1571840111::get_offset_of_contentLength_10(),
	FileWebRequest_t1571840111::get_offset_of_fileAccess_11(),
	FileWebRequest_t1571840111::get_offset_of_method_12(),
	FileWebRequest_t1571840111::get_offset_of_proxy_13(),
	FileWebRequest_t1571840111::get_offset_of_preAuthenticate_14(),
	FileWebRequest_t1571840111::get_offset_of_timeout_15(),
	FileWebRequest_t1571840111::get_offset_of_webResponse_16(),
	FileWebRequest_t1571840111::get_offset_of_requestEndEvent_17(),
	FileWebRequest_t1571840111::get_offset_of_requesting_18(),
	FileWebRequest_t1571840111::get_offset_of_asyncResponding_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (FileWebStream_t1952319648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[1] = 
{
	FileWebStream_t1952319648::get_offset_of_webRequest_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (GetResponseCallback_t3725471744), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (FileWebResponse_t1934981865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[5] = 
{
	FileWebResponse_t1934981865::get_offset_of_responseUri_1(),
	FileWebResponse_t1934981865::get_offset_of_fileStream_2(),
	FileWebResponse_t1934981865::get_offset_of_contentLength_3(),
	FileWebResponse_t1934981865::get_offset_of_webHeaders_4(),
	FileWebResponse_t1934981865::get_offset_of_disposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (FtpAsyncResult_t770082413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[9] = 
{
	FtpAsyncResult_t770082413::get_offset_of_response_0(),
	FtpAsyncResult_t770082413::get_offset_of_waitHandle_1(),
	FtpAsyncResult_t770082413::get_offset_of_exception_2(),
	FtpAsyncResult_t770082413::get_offset_of_callback_3(),
	FtpAsyncResult_t770082413::get_offset_of_stream_4(),
	FtpAsyncResult_t770082413::get_offset_of_state_5(),
	FtpAsyncResult_t770082413::get_offset_of_completed_6(),
	FtpAsyncResult_t770082413::get_offset_of_synch_7(),
	FtpAsyncResult_t770082413::get_offset_of_locker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (FtpDataStream_t3588258764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[5] = 
{
	FtpDataStream_t3588258764::get_offset_of_request_2(),
	FtpDataStream_t3588258764::get_offset_of_networkStream_3(),
	FtpDataStream_t3588258764::get_offset_of_disposed_4(),
	FtpDataStream_t3588258764::get_offset_of_isRead_5(),
	FtpDataStream_t3588258764::get_offset_of_totalRead_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (WriteDelegate_t888270799), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (ReadDelegate_t1559754630), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (FtpRequestCreator_t3711983251), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (FtpStatusCode_t1448112771)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1736[38] = 
{
	FtpStatusCode_t1448112771::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (FtpWebRequest_t3120721823), -1, sizeof(FtpWebRequest_t3120721823_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1737[31] = 
{
	FtpWebRequest_t3120721823::get_offset_of_requestUri_6(),
	FtpWebRequest_t3120721823::get_offset_of_file_name_7(),
	FtpWebRequest_t3120721823::get_offset_of_servicePoint_8(),
	FtpWebRequest_t3120721823::get_offset_of_origDataStream_9(),
	FtpWebRequest_t3120721823::get_offset_of_dataStream_10(),
	FtpWebRequest_t3120721823::get_offset_of_controlStream_11(),
	FtpWebRequest_t3120721823::get_offset_of_controlReader_12(),
	FtpWebRequest_t3120721823::get_offset_of_credentials_13(),
	FtpWebRequest_t3120721823::get_offset_of_hostEntry_14(),
	FtpWebRequest_t3120721823::get_offset_of_localEndPoint_15(),
	FtpWebRequest_t3120721823::get_offset_of_proxy_16(),
	FtpWebRequest_t3120721823::get_offset_of_timeout_17(),
	FtpWebRequest_t3120721823::get_offset_of_rwTimeout_18(),
	FtpWebRequest_t3120721823::get_offset_of_offset_19(),
	FtpWebRequest_t3120721823::get_offset_of_binary_20(),
	FtpWebRequest_t3120721823::get_offset_of_enableSsl_21(),
	FtpWebRequest_t3120721823::get_offset_of_usePassive_22(),
	FtpWebRequest_t3120721823::get_offset_of_keepAlive_23(),
	FtpWebRequest_t3120721823::get_offset_of_method_24(),
	FtpWebRequest_t3120721823::get_offset_of_renameTo_25(),
	FtpWebRequest_t3120721823::get_offset_of_locker_26(),
	FtpWebRequest_t3120721823::get_offset_of_requestState_27(),
	FtpWebRequest_t3120721823::get_offset_of_asyncResult_28(),
	FtpWebRequest_t3120721823::get_offset_of_ftpResponse_29(),
	FtpWebRequest_t3120721823::get_offset_of_requestStream_30(),
	FtpWebRequest_t3120721823::get_offset_of_initial_path_31(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_supportedCommands_32(),
	FtpWebRequest_t3120721823::get_offset_of_callback_33(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_34(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_35(),
	FtpWebRequest_t3120721823_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (RequestState_t4256633122)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1738[10] = 
{
	RequestState_t4256633122::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (FtpStatus_t3714482970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[2] = 
{
	FtpStatus_t3714482970::get_offset_of_statusCode_0(),
	FtpStatus_t3714482970::get_offset_of_statusDescription_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (FtpWebResponse_t2609078769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[12] = 
{
	FtpWebResponse_t2609078769::get_offset_of_stream_1(),
	FtpWebResponse_t2609078769::get_offset_of_uri_2(),
	FtpWebResponse_t2609078769::get_offset_of_statusCode_3(),
	FtpWebResponse_t2609078769::get_offset_of_lastModified_4(),
	FtpWebResponse_t2609078769::get_offset_of_bannerMessage_5(),
	FtpWebResponse_t2609078769::get_offset_of_welcomeMessage_6(),
	FtpWebResponse_t2609078769::get_offset_of_exitMessage_7(),
	FtpWebResponse_t2609078769::get_offset_of_statusDescription_8(),
	FtpWebResponse_t2609078769::get_offset_of_method_9(),
	FtpWebResponse_t2609078769::get_offset_of_disposed_10(),
	FtpWebResponse_t2609078769::get_offset_of_request_11(),
	FtpWebResponse_t2609078769::get_offset_of_contentLength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (GlobalProxySelection_t2251180943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (HttpRequestCreator_t1416559589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (HttpStatusCode_t1898409641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1743[47] = 
{
	HttpStatusCode_t1898409641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (HttpVersion_t1276659706), -1, sizeof(HttpVersion_t1276659706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1744[2] = 
{
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t1276659706_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (HttpWebRequest_t1951404513), -1, sizeof(HttpWebRequest_t1951404513_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1745[50] = 
{
	HttpWebRequest_t1951404513::get_offset_of_requestUri_6(),
	HttpWebRequest_t1951404513::get_offset_of_actualUri_7(),
	HttpWebRequest_t1951404513::get_offset_of_hostChanged_8(),
	HttpWebRequest_t1951404513::get_offset_of_allowAutoRedirect_9(),
	HttpWebRequest_t1951404513::get_offset_of_allowBuffering_10(),
	HttpWebRequest_t1951404513::get_offset_of_certificates_11(),
	HttpWebRequest_t1951404513::get_offset_of_connectionGroup_12(),
	HttpWebRequest_t1951404513::get_offset_of_contentLength_13(),
	HttpWebRequest_t1951404513::get_offset_of_continueDelegate_14(),
	HttpWebRequest_t1951404513::get_offset_of_cookieContainer_15(),
	HttpWebRequest_t1951404513::get_offset_of_credentials_16(),
	HttpWebRequest_t1951404513::get_offset_of_haveResponse_17(),
	HttpWebRequest_t1951404513::get_offset_of_haveRequest_18(),
	HttpWebRequest_t1951404513::get_offset_of_requestSent_19(),
	HttpWebRequest_t1951404513::get_offset_of_webHeaders_20(),
	HttpWebRequest_t1951404513::get_offset_of_keepAlive_21(),
	HttpWebRequest_t1951404513::get_offset_of_maxAutoRedirect_22(),
	HttpWebRequest_t1951404513::get_offset_of_mediaType_23(),
	HttpWebRequest_t1951404513::get_offset_of_method_24(),
	HttpWebRequest_t1951404513::get_offset_of_initialMethod_25(),
	HttpWebRequest_t1951404513::get_offset_of_pipelined_26(),
	HttpWebRequest_t1951404513::get_offset_of_preAuthenticate_27(),
	HttpWebRequest_t1951404513::get_offset_of_usedPreAuth_28(),
	HttpWebRequest_t1951404513::get_offset_of_version_29(),
	HttpWebRequest_t1951404513::get_offset_of_actualVersion_30(),
	HttpWebRequest_t1951404513::get_offset_of_proxy_31(),
	HttpWebRequest_t1951404513::get_offset_of_sendChunked_32(),
	HttpWebRequest_t1951404513::get_offset_of_servicePoint_33(),
	HttpWebRequest_t1951404513::get_offset_of_timeout_34(),
	HttpWebRequest_t1951404513::get_offset_of_writeStream_35(),
	HttpWebRequest_t1951404513::get_offset_of_webResponse_36(),
	HttpWebRequest_t1951404513::get_offset_of_asyncWrite_37(),
	HttpWebRequest_t1951404513::get_offset_of_asyncRead_38(),
	HttpWebRequest_t1951404513::get_offset_of_abortHandler_39(),
	HttpWebRequest_t1951404513::get_offset_of_aborted_40(),
	HttpWebRequest_t1951404513::get_offset_of_redirects_41(),
	HttpWebRequest_t1951404513::get_offset_of_expectContinue_42(),
	HttpWebRequest_t1951404513::get_offset_of_authCompleted_43(),
	HttpWebRequest_t1951404513::get_offset_of_bodyBuffer_44(),
	HttpWebRequest_t1951404513::get_offset_of_bodyBufferLength_45(),
	HttpWebRequest_t1951404513::get_offset_of_getResponseCalled_46(),
	HttpWebRequest_t1951404513::get_offset_of_saved_exc_47(),
	HttpWebRequest_t1951404513::get_offset_of_locker_48(),
	HttpWebRequest_t1951404513::get_offset_of_is_ntlm_auth_49(),
	HttpWebRequest_t1951404513::get_offset_of_finished_reading_50(),
	HttpWebRequest_t1951404513::get_offset_of_WebConnection_51(),
	HttpWebRequest_t1951404513::get_offset_of_auto_decomp_52(),
	HttpWebRequest_t1951404513_StaticFields::get_offset_of_defaultMaxResponseHeadersLength_53(),
	HttpWebRequest_t1951404513::get_offset_of_readWriteTimeout_54(),
	HttpWebRequest_t1951404513::get_offset_of_unsafe_auth_blah_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (HttpWebResponse_t2828383075), -1, sizeof(HttpWebResponse_t2828383075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1746[14] = 
{
	HttpWebResponse_t2828383075::get_offset_of_uri_1(),
	HttpWebResponse_t2828383075::get_offset_of_webHeaders_2(),
	HttpWebResponse_t2828383075::get_offset_of_cookieCollection_3(),
	HttpWebResponse_t2828383075::get_offset_of_method_4(),
	HttpWebResponse_t2828383075::get_offset_of_version_5(),
	HttpWebResponse_t2828383075::get_offset_of_statusCode_6(),
	HttpWebResponse_t2828383075::get_offset_of_statusDescription_7(),
	HttpWebResponse_t2828383075::get_offset_of_contentLength_8(),
	HttpWebResponse_t2828383075::get_offset_of_contentType_9(),
	HttpWebResponse_t2828383075::get_offset_of_cookie_container_10(),
	HttpWebResponse_t2828383075::get_offset_of_disposed_11(),
	HttpWebResponse_t2828383075::get_offset_of_stream_12(),
	HttpWebResponse_t2828383075::get_offset_of_cookieExpiresFormats_13(),
	HttpWebResponse_t2828383075_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (CookieParser_t1405985527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[3] = 
{
	CookieParser_t1405985527::get_offset_of_header_0(),
	CookieParser_t1405985527::get_offset_of_pos_1(),
	CookieParser_t1405985527::get_offset_of_length_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (IPAddress_t1399971723), -1, sizeof(IPAddress_t1399971723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1753[11] = 
{
	IPAddress_t1399971723::get_offset_of_m_Address_0(),
	IPAddress_t1399971723::get_offset_of_m_Family_1(),
	IPAddress_t1399971723::get_offset_of_m_Numbers_2(),
	IPAddress_t1399971723::get_offset_of_m_ScopeId_3(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Any_4(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Broadcast_5(),
	IPAddress_t1399971723_StaticFields::get_offset_of_Loopback_6(),
	IPAddress_t1399971723_StaticFields::get_offset_of_None_7(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Any_8(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6Loopback_9(),
	IPAddress_t1399971723_StaticFields::get_offset_of_IPv6None_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (IPEndPoint_t2615413766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[2] = 
{
	IPEndPoint_t2615413766::get_offset_of_address_0(),
	IPEndPoint_t2615413766::get_offset_of_port_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (IPHostEntry_t994738509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[3] = 
{
	IPHostEntry_t994738509::get_offset_of_addressList_0(),
	IPHostEntry_t994738509::get_offset_of_aliases_1(),
	IPHostEntry_t994738509::get_offset_of_hostName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (IPv6Address_t2596635879), -1, sizeof(IPv6Address_t2596635879_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1756[5] = 
{
	IPv6Address_t2596635879::get_offset_of_address_0(),
	IPv6Address_t2596635879::get_offset_of_prefixLength_1(),
	IPv6Address_t2596635879::get_offset_of_scopeId_2(),
	IPv6Address_t2596635879_StaticFields::get_offset_of_Loopback_3(),
	IPv6Address_t2596635879_StaticFields::get_offset_of_Unspecified_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (NetConfig_t1609737885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[2] = 
{
	NetConfig_t1609737885::get_offset_of_ipv6Enabled_0(),
	NetConfig_t1609737885::get_offset_of_MaxResponseHeadersLength_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (NetworkCredential_t1714133953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[3] = 
{
	NetworkCredential_t1714133953::get_offset_of_userName_0(),
	NetworkCredential_t1714133953::get_offset_of_password_1(),
	NetworkCredential_t1714133953::get_offset_of_domain_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (ProtocolViolationException_t4263317570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (AuthenticatedStream_t1183414097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[2] = 
{
	AuthenticatedStream_t1183414097::get_offset_of_innerStream_2(),
	AuthenticatedStream_t1183414097::get_offset_of_leaveStreamOpen_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (AuthenticationLevel_t2424130044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1763[4] = 
{
	AuthenticationLevel_t2424130044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (SecurityProtocolType_t3099771628)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1764[3] = 
{
	SecurityProtocolType_t3099771628::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (SslStream_t1853163792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[3] = 
{
	SslStream_t1853163792::get_offset_of_ssl_stream_4(),
	SslStream_t1853163792::get_offset_of_validation_callback_5(),
	SslStream_t1853163792::get_offset_of_selection_callback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1358332250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[2] = 
{
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1358332250::get_offset_of_clientCertificates_0(),
	U3CBeginAuthenticateAsClientU3Ec__AnonStorey7_t1358332250::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (SslPolicyErrors_t1928581431)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1767[5] = 
{
	SslPolicyErrors_t1928581431::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (ServicePoint_t2765344313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[17] = 
{
	ServicePoint_t2765344313::get_offset_of_uri_0(),
	ServicePoint_t2765344313::get_offset_of_connectionLimit_1(),
	ServicePoint_t2765344313::get_offset_of_maxIdleTime_2(),
	ServicePoint_t2765344313::get_offset_of_currentConnections_3(),
	ServicePoint_t2765344313::get_offset_of_idleSince_4(),
	ServicePoint_t2765344313::get_offset_of_protocolVersion_5(),
	ServicePoint_t2765344313::get_offset_of_certificate_6(),
	ServicePoint_t2765344313::get_offset_of_clientCertificate_7(),
	ServicePoint_t2765344313::get_offset_of_host_8(),
	ServicePoint_t2765344313::get_offset_of_usesProxy_9(),
	ServicePoint_t2765344313::get_offset_of_groups_10(),
	ServicePoint_t2765344313::get_offset_of_sendContinue_11(),
	ServicePoint_t2765344313::get_offset_of_useConnect_12(),
	ServicePoint_t2765344313::get_offset_of_locker_13(),
	ServicePoint_t2765344313::get_offset_of_hostE_14(),
	ServicePoint_t2765344313::get_offset_of_useNagle_15(),
	ServicePoint_t2765344313::get_offset_of_endPointCallback_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (ServicePointManager_t745663000), -1, sizeof(ServicePointManager_t745663000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1769[11] = 
{
	ServicePointManager_t745663000_StaticFields::get_offset_of_servicePoints_0(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_policy_1(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_defaultConnectionLimit_2(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_maxServicePointIdleTime_3(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_maxServicePoints_4(),
	ServicePointManager_t745663000_StaticFields::get_offset_of__checkCRL_5(),
	ServicePointManager_t745663000_StaticFields::get_offset_of__securityProtocol_6(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_expectContinue_7(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_useNagle_8(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_server_cert_cb_9(),
	ServicePointManager_t745663000_StaticFields::get_offset_of_manager_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (SPKey_t1552752485), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[2] = 
{
	SPKey_t1552752485::get_offset_of_uri_0(),
	SPKey_t1552752485::get_offset_of_use_connect_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (ChainValidationHelper_t1155887809), -1, sizeof(ChainValidationHelper_t1155887809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1771[4] = 
{
	ChainValidationHelper_t1155887809::get_offset_of_sender_0(),
	ChainValidationHelper_t1155887809::get_offset_of_host_1(),
	ChainValidationHelper_t1155887809_StaticFields::get_offset_of_is_macosx_2(),
	ChainValidationHelper_t1155887809_StaticFields::get_offset_of_s_flags_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (SocketAddress_t838303055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1772[1] = 
{
	SocketAddress_t838303055::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (AddressFamily_t303362630)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1773[32] = 
{
	AddressFamily_t303362630::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (LingerOption_t1165263720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[2] = 
{
	LingerOption_t1165263720::get_offset_of_enabled_0(),
	LingerOption_t1165263720::get_offset_of_seconds_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (MulticastOption_t2505469155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (NetworkStream_t581172200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[6] = 
{
	NetworkStream_t581172200::get_offset_of_access_2(),
	NetworkStream_t581172200::get_offset_of_socket_3(),
	NetworkStream_t581172200::get_offset_of_owns_socket_4(),
	NetworkStream_t581172200::get_offset_of_readable_5(),
	NetworkStream_t581172200::get_offset_of_writeable_6(),
	NetworkStream_t581172200::get_offset_of_disposed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (ProtocolType_t2178963134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1777[26] = 
{
	ProtocolType_t2178963134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (SelectMode_t3413969319)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1778[4] = 
{
	SelectMode_t3413969319::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (Socket_t3821512045), -1, sizeof(Socket_t3821512045_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1779[22] = 
{
	Socket_t3821512045::get_offset_of_readQ_0(),
	Socket_t3821512045::get_offset_of_writeQ_1(),
	Socket_t3821512045::get_offset_of_islistening_2(),
	Socket_t3821512045::get_offset_of_MinListenPort_3(),
	Socket_t3821512045::get_offset_of_MaxListenPort_4(),
	Socket_t3821512045_StaticFields::get_offset_of_ipv4Supported_5(),
	Socket_t3821512045_StaticFields::get_offset_of_ipv6Supported_6(),
	Socket_t3821512045::get_offset_of_linger_timeout_7(),
	Socket_t3821512045::get_offset_of_socket_8(),
	Socket_t3821512045::get_offset_of_address_family_9(),
	Socket_t3821512045::get_offset_of_socket_type_10(),
	Socket_t3821512045::get_offset_of_protocol_type_11(),
	Socket_t3821512045::get_offset_of_blocking_12(),
	Socket_t3821512045::get_offset_of_blocking_thread_13(),
	Socket_t3821512045::get_offset_of_isbound_14(),
	Socket_t3821512045_StaticFields::get_offset_of_current_bind_count_15(),
	Socket_t3821512045::get_offset_of_max_bind_count_16(),
	Socket_t3821512045::get_offset_of_connected_17(),
	Socket_t3821512045::get_offset_of_closed_18(),
	Socket_t3821512045::get_offset_of_disposed_19(),
	Socket_t3821512045::get_offset_of_seed_endpoint_20(),
	Socket_t3821512045_StaticFields::get_offset_of_check_socket_policy_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (SocketOperation_t3328960782)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1780[15] = 
{
	SocketOperation_t3328960782::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (SocketAsyncResult_t2959281146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[25] = 
{
	SocketAsyncResult_t2959281146::get_offset_of_Sock_0(),
	SocketAsyncResult_t2959281146::get_offset_of_handle_1(),
	SocketAsyncResult_t2959281146::get_offset_of_state_2(),
	SocketAsyncResult_t2959281146::get_offset_of_callback_3(),
	SocketAsyncResult_t2959281146::get_offset_of_waithandle_4(),
	SocketAsyncResult_t2959281146::get_offset_of_delayedException_5(),
	SocketAsyncResult_t2959281146::get_offset_of_EndPoint_6(),
	SocketAsyncResult_t2959281146::get_offset_of_Buffer_7(),
	SocketAsyncResult_t2959281146::get_offset_of_Offset_8(),
	SocketAsyncResult_t2959281146::get_offset_of_Size_9(),
	SocketAsyncResult_t2959281146::get_offset_of_SockFlags_10(),
	SocketAsyncResult_t2959281146::get_offset_of_AcceptSocket_11(),
	SocketAsyncResult_t2959281146::get_offset_of_Addresses_12(),
	SocketAsyncResult_t2959281146::get_offset_of_Port_13(),
	SocketAsyncResult_t2959281146::get_offset_of_Buffers_14(),
	SocketAsyncResult_t2959281146::get_offset_of_ReuseSocket_15(),
	SocketAsyncResult_t2959281146::get_offset_of_acc_socket_16(),
	SocketAsyncResult_t2959281146::get_offset_of_total_17(),
	SocketAsyncResult_t2959281146::get_offset_of_completed_sync_18(),
	SocketAsyncResult_t2959281146::get_offset_of_completed_19(),
	SocketAsyncResult_t2959281146::get_offset_of_blocking_20(),
	SocketAsyncResult_t2959281146::get_offset_of_error_21(),
	SocketAsyncResult_t2959281146::get_offset_of_operation_22(),
	SocketAsyncResult_t2959281146::get_offset_of_ares_23(),
	SocketAsyncResult_t2959281146::get_offset_of_EndCalled_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (Worker_t1317165246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[3] = 
{
	Worker_t1317165246::get_offset_of_result_0(),
	Worker_t1317165246::get_offset_of_requireSocketSecurity_1(),
	Worker_t1317165246::get_offset_of_send_so_far_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (SocketAsyncCall_t3737776727), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (SocketError_t307542793)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1784[48] = 
{
	SocketError_t307542793::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (SocketException_t1618573604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (SocketFlags_t2353657790)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1786[11] = 
{
	SocketFlags_t2353657790::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (SocketOptionLevel_t1505247880)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1787[6] = 
{
	SocketOptionLevel_t1505247880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (SocketOptionName_t1089121285)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1788[44] = 
{
	SocketOptionName_t1089121285::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (SocketShutdown_t3247039417)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1789[4] = 
{
	SocketShutdown_t3247039417::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (SocketType_t1143498533)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1790[7] = 
{
	SocketType_t1143498533::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (WebAsyncResult_t905414499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[17] = 
{
	WebAsyncResult_t905414499::get_offset_of_handle_0(),
	WebAsyncResult_t905414499::get_offset_of_synch_1(),
	WebAsyncResult_t905414499::get_offset_of_isCompleted_2(),
	WebAsyncResult_t905414499::get_offset_of_cb_3(),
	WebAsyncResult_t905414499::get_offset_of_state_4(),
	WebAsyncResult_t905414499::get_offset_of_nbytes_5(),
	WebAsyncResult_t905414499::get_offset_of_innerAsyncResult_6(),
	WebAsyncResult_t905414499::get_offset_of_callbackDone_7(),
	WebAsyncResult_t905414499::get_offset_of_exc_8(),
	WebAsyncResult_t905414499::get_offset_of_response_9(),
	WebAsyncResult_t905414499::get_offset_of_writeStream_10(),
	WebAsyncResult_t905414499::get_offset_of_buffer_11(),
	WebAsyncResult_t905414499::get_offset_of_offset_12(),
	WebAsyncResult_t905414499::get_offset_of_size_13(),
	WebAsyncResult_t905414499::get_offset_of_locker_14(),
	WebAsyncResult_t905414499::get_offset_of_EndCalled_15(),
	WebAsyncResult_t905414499::get_offset_of_AsyncWriteAll_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (ReadState_t657568301)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1792[5] = 
{
	ReadState_t657568301::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (WebConnection_t324679648), -1, sizeof(WebConnection_t324679648_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1793[32] = 
{
	WebConnection_t324679648::get_offset_of_sPoint_0(),
	WebConnection_t324679648::get_offset_of_nstream_1(),
	WebConnection_t324679648::get_offset_of_socket_2(),
	WebConnection_t324679648::get_offset_of_socketLock_3(),
	WebConnection_t324679648::get_offset_of_status_4(),
	WebConnection_t324679648::get_offset_of_initConn_5(),
	WebConnection_t324679648::get_offset_of_keepAlive_6(),
	WebConnection_t324679648::get_offset_of_buffer_7(),
	WebConnection_t324679648_StaticFields::get_offset_of_readDoneDelegate_8(),
	WebConnection_t324679648::get_offset_of_abortHandler_9(),
	WebConnection_t324679648::get_offset_of_abortHelper_10(),
	WebConnection_t324679648::get_offset_of_readState_11(),
	WebConnection_t324679648::get_offset_of_Data_12(),
	WebConnection_t324679648::get_offset_of_chunkedRead_13(),
	WebConnection_t324679648::get_offset_of_chunkStream_14(),
	WebConnection_t324679648::get_offset_of_queue_15(),
	WebConnection_t324679648::get_offset_of_reused_16(),
	WebConnection_t324679648::get_offset_of_position_17(),
	WebConnection_t324679648::get_offset_of_busy_18(),
	WebConnection_t324679648::get_offset_of_priority_request_19(),
	WebConnection_t324679648::get_offset_of_ntlm_credentials_20(),
	WebConnection_t324679648::get_offset_of_ntlm_authenticated_21(),
	WebConnection_t324679648::get_offset_of_unsafe_sharing_22(),
	WebConnection_t324679648::get_offset_of_ssl_23(),
	WebConnection_t324679648::get_offset_of_certsAvailable_24(),
	WebConnection_t324679648::get_offset_of_connect_exception_25(),
	WebConnection_t324679648_StaticFields::get_offset_of_classLock_26(),
	WebConnection_t324679648_StaticFields::get_offset_of_sslStream_27(),
	WebConnection_t324679648_StaticFields::get_offset_of_piClient_28(),
	WebConnection_t324679648_StaticFields::get_offset_of_piServer_29(),
	WebConnection_t324679648_StaticFields::get_offset_of_piTrustFailure_30(),
	WebConnection_t324679648_StaticFields::get_offset_of_method_GetSecurityPolicyFromNonMainThread_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (AbortHelper_t2895113041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[1] = 
{
	AbortHelper_t2895113041::get_offset_of_Connection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (WebConnectionData_t3550260432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[7] = 
{
	WebConnectionData_t3550260432::get_offset_of_request_0(),
	WebConnectionData_t3550260432::get_offset_of_StatusCode_1(),
	WebConnectionData_t3550260432::get_offset_of_StatusDescription_2(),
	WebConnectionData_t3550260432::get_offset_of_Headers_3(),
	WebConnectionData_t3550260432::get_offset_of_Version_4(),
	WebConnectionData_t3550260432::get_offset_of_stream_5(),
	WebConnectionData_t3550260432::get_offset_of_Challenge_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (WebConnectionGroup_t3242458773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[5] = 
{
	WebConnectionGroup_t3242458773::get_offset_of_sPoint_0(),
	WebConnectionGroup_t3242458773::get_offset_of_name_1(),
	WebConnectionGroup_t3242458773::get_offset_of_connections_2(),
	WebConnectionGroup_t3242458773::get_offset_of_rnd_3(),
	WebConnectionGroup_t3242458773::get_offset_of_queue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (WebConnectionStream_t1922483508), -1, sizeof(WebConnectionStream_t1922483508_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1797[27] = 
{
	WebConnectionStream_t1922483508_StaticFields::get_offset_of_crlf_2(),
	WebConnectionStream_t1922483508::get_offset_of_isRead_3(),
	WebConnectionStream_t1922483508::get_offset_of_cnc_4(),
	WebConnectionStream_t1922483508::get_offset_of_request_5(),
	WebConnectionStream_t1922483508::get_offset_of_readBuffer_6(),
	WebConnectionStream_t1922483508::get_offset_of_readBufferOffset_7(),
	WebConnectionStream_t1922483508::get_offset_of_readBufferSize_8(),
	WebConnectionStream_t1922483508::get_offset_of_contentLength_9(),
	WebConnectionStream_t1922483508::get_offset_of_totalRead_10(),
	WebConnectionStream_t1922483508::get_offset_of_totalWritten_11(),
	WebConnectionStream_t1922483508::get_offset_of_nextReadCalled_12(),
	WebConnectionStream_t1922483508::get_offset_of_pendingReads_13(),
	WebConnectionStream_t1922483508::get_offset_of_pendingWrites_14(),
	WebConnectionStream_t1922483508::get_offset_of_pending_15(),
	WebConnectionStream_t1922483508::get_offset_of_allowBuffering_16(),
	WebConnectionStream_t1922483508::get_offset_of_sendChunked_17(),
	WebConnectionStream_t1922483508::get_offset_of_writeBuffer_18(),
	WebConnectionStream_t1922483508::get_offset_of_requestWritten_19(),
	WebConnectionStream_t1922483508::get_offset_of_headers_20(),
	WebConnectionStream_t1922483508::get_offset_of_disposed_21(),
	WebConnectionStream_t1922483508::get_offset_of_headersSent_22(),
	WebConnectionStream_t1922483508::get_offset_of_locker_23(),
	WebConnectionStream_t1922483508::get_offset_of_initRead_24(),
	WebConnectionStream_t1922483508::get_offset_of_read_eof_25(),
	WebConnectionStream_t1922483508::get_offset_of_complete_request_written_26(),
	WebConnectionStream_t1922483508::get_offset_of_read_timeout_27(),
	WebConnectionStream_t1922483508::get_offset_of_write_timeout_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (WebException_t3368933679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[2] = 
{
	WebException_t3368933679::get_offset_of_response_12(),
	WebException_t3368933679::get_offset_of_status_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (WebExceptionStatus_t1169373531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1799[22] = 
{
	WebExceptionStatus_t1169373531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
