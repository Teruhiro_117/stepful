﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>
struct ReadOnlyCollection_1_t2189678175;
// System.Collections.Generic.IList`1<UnityEngine.Networking.QosType>
struct IList_1_t2544833084;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.QosType[]
struct QosTypeU5BU5D_t740748178;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.QosType>
struct IEnumerator_1_t3774383606;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_QosType2003892483.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2624458382_gshared (ReadOnlyCollection_1_t2189678175 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2624458382(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2624458382_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1214226702_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1214226702(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1214226702_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4137827786_gshared (ReadOnlyCollection_1_t2189678175 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4137827786(__this, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4137827786_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m288068595_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m288068595(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m288068595_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m173559611_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m173559611(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m173559611_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4011289991_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4011289991(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m4011289991_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m51295299_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m51295299(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m51295299_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m991228884_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m991228884(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m991228884_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2990283660_gshared (ReadOnlyCollection_1_t2189678175 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2990283660(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2189678175 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2990283660_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2163666613_gshared (ReadOnlyCollection_1_t2189678175 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2163666613(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2163666613_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2390752276_gshared (ReadOnlyCollection_1_t2189678175 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2390752276(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2189678175 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2390752276_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1353720029_gshared (ReadOnlyCollection_1_t2189678175 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1353720029(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2189678175 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1353720029_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2743946237_gshared (ReadOnlyCollection_1_t2189678175 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2743946237(__this, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2743946237_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m624153877_gshared (ReadOnlyCollection_1_t2189678175 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m624153877(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2189678175 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m624153877_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2408962543_gshared (ReadOnlyCollection_1_t2189678175 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2408962543(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2189678175 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2408962543_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m466354710_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m466354710(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m466354710_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m275750476_gshared (ReadOnlyCollection_1_t2189678175 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m275750476(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m275750476_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m55496452_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m55496452(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m55496452_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2479982129_gshared (ReadOnlyCollection_1_t2189678175 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2479982129(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2189678175 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2479982129_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1839815977_gshared (ReadOnlyCollection_1_t2189678175 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1839815977(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2189678175 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m1839815977_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m68905606_gshared (ReadOnlyCollection_1_t2189678175 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m68905606(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2189678175 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m68905606_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1531762349_gshared (ReadOnlyCollection_1_t2189678175 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1531762349(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2189678175 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1531762349_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m829273212_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m829273212(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m829273212_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3612943407_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3612943407(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3612943407_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m255342756_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m255342756(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m255342756_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1480831306_gshared (ReadOnlyCollection_1_t2189678175 * __this, QosTypeU5BU5D_t740748178* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1480831306(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2189678175 *, QosTypeU5BU5D_t740748178*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1480831306_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1186945021_gshared (ReadOnlyCollection_1_t2189678175 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1186945021(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2189678175 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1186945021_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m476017892_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m476017892(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m476017892_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3903429965_gshared (ReadOnlyCollection_1_t2189678175 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3903429965(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2189678175 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3903429965_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.QosType>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m2122607391_gshared (ReadOnlyCollection_1_t2189678175 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2122607391(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2189678175 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2122607391_gshared)(__this, ___index0, method)
