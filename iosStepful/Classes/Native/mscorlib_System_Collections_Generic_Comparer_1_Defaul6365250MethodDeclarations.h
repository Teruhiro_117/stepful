﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct DefaultComparer_t6365250;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1517095017.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.ctor()
extern "C"  void DefaultComparer__ctor_m3036391359_gshared (DefaultComparer_t6365250 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3036391359(__this, method) ((  void (*) (DefaultComparer_t6365250 *, const MethodInfo*))DefaultComparer__ctor_m3036391359_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3338616378_gshared (DefaultComparer_t6365250 * __this, PendingPlayer_t1517095017  ___x0, PendingPlayer_t1517095017  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3338616378(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t6365250 *, PendingPlayer_t1517095017 , PendingPlayer_t1517095017 , const MethodInfo*))DefaultComparer_Compare_m3338616378_gshared)(__this, ___x0, ___y1, method)
