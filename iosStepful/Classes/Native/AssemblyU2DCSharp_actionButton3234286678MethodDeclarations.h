﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// actionButton
struct actionButton_t3234286678;

#include "codegen/il2cpp-codegen.h"

// System.Void actionButton::.ctor()
extern "C"  void actionButton__ctor_m619258599 (actionButton_t3234286678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void actionButton::Start()
extern "C"  void actionButton_Start_m4164753571 (actionButton_t3234286678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void actionButton::Update()
extern "C"  void actionButton_Update_m3923835296 (actionButton_t3234286678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void actionButton::OnClick()
extern "C"  void actionButton_OnClick_m1181668582 (actionButton_t3234286678 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
