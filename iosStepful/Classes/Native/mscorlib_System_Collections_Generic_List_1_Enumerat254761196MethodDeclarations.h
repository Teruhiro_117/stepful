﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ConnectionConfig>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2139871662(__this, ___l0, method) ((  void (*) (Enumerator_t254761196 *, List_1_t720031522 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ConnectionConfig>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1083330332(__this, method) ((  void (*) (Enumerator_t254761196 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ConnectionConfig>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4132251268(__this, method) ((  Il2CppObject * (*) (Enumerator_t254761196 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ConnectionConfig>::Dispose()
#define Enumerator_Dispose_m2198846529(__this, method) ((  void (*) (Enumerator_t254761196 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ConnectionConfig>::VerifyState()
#define Enumerator_VerifyState_m2597306184(__this, method) ((  void (*) (Enumerator_t254761196 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ConnectionConfig>::MoveNext()
#define Enumerator_MoveNext_m3474517296(__this, method) ((  bool (*) (Enumerator_t254761196 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ConnectionConfig>::get_Current()
#define Enumerator_get_Current_m2293835113(__this, method) ((  ConnectionConfig_t1350910390 * (*) (Enumerator_t254761196 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
