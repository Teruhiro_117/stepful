﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkSystem.AnimationTriggerMessage
struct AnimationTriggerMessage_t949407937;
// UnityEngine.Networking.NetworkReader
struct NetworkReader_t3187690923;
// UnityEngine.Networking.NetworkWriter
struct NetworkWriter_t560143343;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3187690923.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo560143343.h"

// System.Void UnityEngine.Networking.NetworkSystem.AnimationTriggerMessage::.ctor()
extern "C"  void AnimationTriggerMessage__ctor_m3994932353 (AnimationTriggerMessage_t949407937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkSystem.AnimationTriggerMessage::Deserialize(UnityEngine.Networking.NetworkReader)
extern "C"  void AnimationTriggerMessage_Deserialize_m3924579702 (AnimationTriggerMessage_t949407937 * __this, NetworkReader_t3187690923 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkSystem.AnimationTriggerMessage::Serialize(UnityEngine.Networking.NetworkWriter)
extern "C"  void AnimationTriggerMessage_Serialize_m4046433713 (AnimationTriggerMessage_t949407937 * __this, NetworkWriter_t560143343 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
