﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>
struct List_1_t1051718017;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat586447691.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan1682596885.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3323918463_gshared (Enumerator_t586447691 * __this, List_1_t1051718017 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3323918463(__this, ___l0, method) ((  void (*) (Enumerator_t586447691 *, List_1_t1051718017 *, const MethodInfo*))Enumerator__ctor_m3323918463_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m440946355_gshared (Enumerator_t586447691 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m440946355(__this, method) ((  void (*) (Enumerator_t586447691 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m440946355_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3130640747_gshared (Enumerator_t586447691 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3130640747(__this, method) ((  Il2CppObject * (*) (Enumerator_t586447691 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3130640747_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::Dispose()
extern "C"  void Enumerator_Dispose_m3809357816_gshared (Enumerator_t586447691 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3809357816(__this, method) ((  void (*) (Enumerator_t586447691 *, const MethodInfo*))Enumerator_Dispose_m3809357816_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2775700245_gshared (Enumerator_t586447691 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2775700245(__this, method) ((  void (*) (Enumerator_t586447691 *, const MethodInfo*))Enumerator_VerifyState_m2775700245_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1733603335_gshared (Enumerator_t586447691 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1733603335(__this, method) ((  bool (*) (Enumerator_t586447691 *, const MethodInfo*))Enumerator_MoveNext_m1733603335_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelPacket>::get_Current()
extern "C"  ChannelPacket_t1682596885  Enumerator_get_Current_m1251173828_gshared (Enumerator_t586447691 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1251173828(__this, method) ((  ChannelPacket_t1682596885  (*) (Enumerator_t586447691 *, const MethodInfo*))Enumerator_get_Current_m1251173828_gshared)(__this, method)
