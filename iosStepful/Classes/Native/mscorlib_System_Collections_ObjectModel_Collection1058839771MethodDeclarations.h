﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct Collection_1_t1058839771;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.NetworkLobbyManager/PendingPlayer[]
struct PendingPlayerU5BU5D_t220373972;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct IEnumerator_1_t3287586140;
// System.Collections.Generic.IList`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct IList_1_t2058035618;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1517095017.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.ctor()
extern "C"  void Collection_1__ctor_m335954128_gshared (Collection_1_t1058839771 * __this, const MethodInfo* method);
#define Collection_1__ctor_m335954128(__this, method) ((  void (*) (Collection_1_t1058839771 *, const MethodInfo*))Collection_1__ctor_m335954128_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m700593723_gshared (Collection_1_t1058839771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m700593723(__this, method) ((  bool (*) (Collection_1_t1058839771 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m700593723_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m117740244_gshared (Collection_1_t1058839771 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m117740244(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1058839771 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m117740244_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3275739719_gshared (Collection_1_t1058839771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3275739719(__this, method) ((  Il2CppObject * (*) (Collection_1_t1058839771 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3275739719_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3783817736_gshared (Collection_1_t1058839771 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3783817736(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1058839771 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3783817736_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1881194662_gshared (Collection_1_t1058839771 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1881194662(__this, ___value0, method) ((  bool (*) (Collection_1_t1058839771 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1881194662_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2518796670_gshared (Collection_1_t1058839771 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2518796670(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1058839771 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2518796670_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3600231671_gshared (Collection_1_t1058839771 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3600231671(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1058839771 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3600231671_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1879335295_gshared (Collection_1_t1058839771 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1879335295(__this, ___value0, method) ((  void (*) (Collection_1_t1058839771 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1879335295_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m961747808_gshared (Collection_1_t1058839771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m961747808(__this, method) ((  bool (*) (Collection_1_t1058839771 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m961747808_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m650539582_gshared (Collection_1_t1058839771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m650539582(__this, method) ((  Il2CppObject * (*) (Collection_1_t1058839771 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m650539582_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m789319711_gshared (Collection_1_t1058839771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m789319711(__this, method) ((  bool (*) (Collection_1_t1058839771 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m789319711_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2926590796_gshared (Collection_1_t1058839771 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2926590796(__this, method) ((  bool (*) (Collection_1_t1058839771 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2926590796_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m812497355_gshared (Collection_1_t1058839771 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m812497355(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1058839771 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m812497355_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1656199166_gshared (Collection_1_t1058839771 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1656199166(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1058839771 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1656199166_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Add(T)
extern "C"  void Collection_1_Add_m42841851_gshared (Collection_1_t1058839771 * __this, PendingPlayer_t1517095017  ___item0, const MethodInfo* method);
#define Collection_1_Add_m42841851(__this, ___item0, method) ((  void (*) (Collection_1_t1058839771 *, PendingPlayer_t1517095017 , const MethodInfo*))Collection_1_Add_m42841851_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Clear()
extern "C"  void Collection_1_Clear_m2195775815_gshared (Collection_1_t1058839771 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2195775815(__this, method) ((  void (*) (Collection_1_t1058839771 *, const MethodInfo*))Collection_1_Clear_m2195775815_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2465290705_gshared (Collection_1_t1058839771 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2465290705(__this, method) ((  void (*) (Collection_1_t1058839771 *, const MethodInfo*))Collection_1_ClearItems_m2465290705_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Contains(T)
extern "C"  bool Collection_1_Contains_m3774409609_gshared (Collection_1_t1058839771 * __this, PendingPlayer_t1517095017  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3774409609(__this, ___item0, method) ((  bool (*) (Collection_1_t1058839771 *, PendingPlayer_t1517095017 , const MethodInfo*))Collection_1_Contains_m3774409609_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m937717247_gshared (Collection_1_t1058839771 * __this, PendingPlayerU5BU5D_t220373972* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m937717247(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1058839771 *, PendingPlayerU5BU5D_t220373972*, int32_t, const MethodInfo*))Collection_1_CopyTo_m937717247_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m946650644_gshared (Collection_1_t1058839771 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m946650644(__this, method) ((  Il2CppObject* (*) (Collection_1_t1058839771 *, const MethodInfo*))Collection_1_GetEnumerator_m946650644_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2479092459_gshared (Collection_1_t1058839771 * __this, PendingPlayer_t1517095017  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2479092459(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1058839771 *, PendingPlayer_t1517095017 , const MethodInfo*))Collection_1_IndexOf_m2479092459_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3600034290_gshared (Collection_1_t1058839771 * __this, int32_t ___index0, PendingPlayer_t1517095017  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3600034290(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1058839771 *, int32_t, PendingPlayer_t1517095017 , const MethodInfo*))Collection_1_Insert_m3600034290_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m763261873_gshared (Collection_1_t1058839771 * __this, int32_t ___index0, PendingPlayer_t1517095017  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m763261873(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1058839771 *, int32_t, PendingPlayer_t1517095017 , const MethodInfo*))Collection_1_InsertItem_m763261873_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Remove(T)
extern "C"  bool Collection_1_Remove_m4198932004_gshared (Collection_1_t1058839771 * __this, PendingPlayer_t1517095017  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m4198932004(__this, ___item0, method) ((  bool (*) (Collection_1_t1058839771 *, PendingPlayer_t1517095017 , const MethodInfo*))Collection_1_Remove_m4198932004_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m104393086_gshared (Collection_1_t1058839771 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m104393086(__this, ___index0, method) ((  void (*) (Collection_1_t1058839771 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m104393086_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2760648904_gshared (Collection_1_t1058839771 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2760648904(__this, ___index0, method) ((  void (*) (Collection_1_t1058839771 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2760648904_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m4294300248_gshared (Collection_1_t1058839771 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m4294300248(__this, method) ((  int32_t (*) (Collection_1_t1058839771 *, const MethodInfo*))Collection_1_get_Count_m4294300248_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::get_Item(System.Int32)
extern "C"  PendingPlayer_t1517095017  Collection_1_get_Item_m3357284370_gshared (Collection_1_t1058839771 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3357284370(__this, ___index0, method) ((  PendingPlayer_t1517095017  (*) (Collection_1_t1058839771 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3357284370_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m245852299_gshared (Collection_1_t1058839771 * __this, int32_t ___index0, PendingPlayer_t1517095017  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m245852299(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1058839771 *, int32_t, PendingPlayer_t1517095017 , const MethodInfo*))Collection_1_set_Item_m245852299_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3929409214_gshared (Collection_1_t1058839771 * __this, int32_t ___index0, PendingPlayer_t1517095017  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3929409214(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1058839771 *, int32_t, PendingPlayer_t1517095017 , const MethodInfo*))Collection_1_SetItem_m3929409214_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2891097117_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2891097117(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2891097117_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::ConvertItem(System.Object)
extern "C"  PendingPlayer_t1517095017  Collection_1_ConvertItem_m892543809_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m892543809(__this /* static, unused */, ___item0, method) ((  PendingPlayer_t1517095017  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m892543809_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m233222109_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m233222109(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m233222109_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1950092437_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1950092437(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1950092437_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m275003934_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m275003934(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m275003934_gshared)(__this /* static, unused */, ___list0, method)
