﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2931030083.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityEngine.Networking.NetworkSceneId::.ctor(System.UInt32)
extern "C"  void NetworkSceneId__ctor_m1430598044 (NetworkSceneId_t2931030083 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkSceneId::IsEmpty()
extern "C"  bool NetworkSceneId_IsEmpty_m836272103 (NetworkSceneId_t2931030083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.NetworkSceneId::GetHashCode()
extern "C"  int32_t NetworkSceneId_GetHashCode_m3425964463 (NetworkSceneId_t2931030083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkSceneId::Equals(System.Object)
extern "C"  bool NetworkSceneId_Equals_m2468819137 (NetworkSceneId_t2931030083 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkSceneId::op_Equality(UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkSceneId)
extern "C"  bool NetworkSceneId_op_Equality_m1440460684 (Il2CppObject * __this /* static, unused */, NetworkSceneId_t2931030083  ___c10, NetworkSceneId_t2931030083  ___c21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.NetworkSceneId::op_Inequality(UnityEngine.Networking.NetworkSceneId,UnityEngine.Networking.NetworkSceneId)
extern "C"  bool NetworkSceneId_op_Inequality_m1222740771 (Il2CppObject * __this /* static, unused */, NetworkSceneId_t2931030083  ___c10, NetworkSceneId_t2931030083  ___c21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.NetworkSceneId::ToString()
extern "C"  String_t* NetworkSceneId_ToString_m3473187247 (NetworkSceneId_t2931030083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.Networking.NetworkSceneId::get_Value()
extern "C"  uint32_t NetworkSceneId_get_Value_m4063040405 (NetworkSceneId_t2931030083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
