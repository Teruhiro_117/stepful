﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt32>
struct DefaultComparer_t638952254;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt32>::.ctor()
extern "C"  void DefaultComparer__ctor_m3735006318_gshared (DefaultComparer_t638952254 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3735006318(__this, method) ((  void (*) (DefaultComparer_t638952254 *, const MethodInfo*))DefaultComparer__ctor_m3735006318_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt32>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3359099829_gshared (DefaultComparer_t638952254 * __this, uint32_t ___x0, uint32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3359099829(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t638952254 *, uint32_t, uint32_t, const MethodInfo*))DefaultComparer_Compare_m3359099829_gshared)(__this, ___x0, ___y1, method)
