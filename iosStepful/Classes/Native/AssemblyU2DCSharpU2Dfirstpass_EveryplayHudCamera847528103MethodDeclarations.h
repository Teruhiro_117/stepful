﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayHudCamera
struct EveryplayHudCamera_t847528103;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void EveryplayHudCamera::.ctor()
extern "C"  void EveryplayHudCamera__ctor_m1447002134 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayHudCamera::Awake()
extern "C"  void EveryplayHudCamera_Awake_m3859285561 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayHudCamera::OnDestroy()
extern "C"  void EveryplayHudCamera_OnDestroy_m3602730979 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayHudCamera::OnEnable()
extern "C"  void EveryplayHudCamera_OnEnable_m2236775166 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayHudCamera::OnDisable()
extern "C"  void EveryplayHudCamera_OnDisable_m1865417241 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayHudCamera::Subscribe(System.Boolean)
extern "C"  void EveryplayHudCamera_Subscribe_m1644800095 (EveryplayHudCamera_t847528103 * __this, bool ___subscribe0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayHudCamera::ReadyForRecording(System.Boolean)
extern "C"  void EveryplayHudCamera_ReadyForRecording_m4131802134 (EveryplayHudCamera_t847528103 * __this, bool ___ready0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayHudCamera::OnPreRender()
extern "C"  void EveryplayHudCamera_OnPreRender_m2946483768 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr EveryplayHudCamera::EveryplayGetUnityRenderEventPtr()
extern "C"  IntPtr_t EveryplayHudCamera_EveryplayGetUnityRenderEventPtr_m59318425 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
