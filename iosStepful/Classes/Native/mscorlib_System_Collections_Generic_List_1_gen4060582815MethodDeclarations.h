﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct List_1_t4060582815;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct IEnumerable_1_t688621432;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct IEnumerator_1_t2166985510;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct ICollection_1_t1348569692;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct ReadOnlyCollection_1_t582280079;
// UnityEngine.Networking.NetworkSystem.PeerInfoPlayer[]
struct PeerInfoPlayerU5BU5D_t1632998306;
// System.Predicate`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct Predicate_1_t3134431798;
// System.Collections.Generic.IComparer`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct IComparer_1_t2645924805;
// System.Comparison`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct Comparison_1_t1658233238;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo396494387.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3595312489.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.ctor()
extern "C"  void List_1__ctor_m2832588353_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1__ctor_m2832588353(__this, method) ((  void (*) (List_1_t4060582815 *, const MethodInfo*))List_1__ctor_m2832588353_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3783780075_gshared (List_1_t4060582815 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3783780075(__this, ___collection0, method) ((  void (*) (List_1_t4060582815 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3783780075_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1371198170_gshared (List_1_t4060582815 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1371198170(__this, ___capacity0, method) ((  void (*) (List_1_t4060582815 *, int32_t, const MethodInfo*))List_1__ctor_m1371198170_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.cctor()
extern "C"  void List_1__cctor_m513608254_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m513608254(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m513608254_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1031703843_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1031703843(__this, method) ((  Il2CppObject* (*) (List_1_t4060582815 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1031703843_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m961846895_gshared (List_1_t4060582815 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m961846895(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4060582815 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m961846895_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2368459652_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2368459652(__this, method) ((  Il2CppObject * (*) (List_1_t4060582815 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m2368459652_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3171472543_gshared (List_1_t4060582815 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3171472543(__this, ___item0, method) ((  int32_t (*) (List_1_t4060582815 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3171472543_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m578638855_gshared (List_1_t4060582815 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m578638855(__this, ___item0, method) ((  bool (*) (List_1_t4060582815 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m578638855_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2954263213_gshared (List_1_t4060582815 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2954263213(__this, ___item0, method) ((  int32_t (*) (List_1_t4060582815 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2954263213_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m606762998_gshared (List_1_t4060582815 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m606762998(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4060582815 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m606762998_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1891931080_gshared (List_1_t4060582815 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1891931080(__this, ___item0, method) ((  void (*) (List_1_t4060582815 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1891931080_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3301188120_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3301188120(__this, method) ((  bool (*) (List_1_t4060582815 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3301188120_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m673130935_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m673130935(__this, method) ((  bool (*) (List_1_t4060582815 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m673130935_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3616547103_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3616547103(__this, method) ((  Il2CppObject * (*) (List_1_t4060582815 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3616547103_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3763742178_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3763742178(__this, method) ((  bool (*) (List_1_t4060582815 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3763742178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1553730683_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1553730683(__this, method) ((  bool (*) (List_1_t4060582815 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1553730683_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3617124758_gshared (List_1_t4060582815 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3617124758(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4060582815 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3617124758_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1708919581_gshared (List_1_t4060582815 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1708919581(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4060582815 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1708919581_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Add(T)
extern "C"  void List_1_Add_m797118837_gshared (List_1_t4060582815 * __this, PeerInfoPlayer_t396494387  ___item0, const MethodInfo* method);
#define List_1_Add_m797118837(__this, ___item0, method) ((  void (*) (List_1_t4060582815 *, PeerInfoPlayer_t396494387 , const MethodInfo*))List_1_Add_m797118837_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2534833715_gshared (List_1_t4060582815 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2534833715(__this, ___newCount0, method) ((  void (*) (List_1_t4060582815 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2534833715_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3094538963_gshared (List_1_t4060582815 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3094538963(__this, ___collection0, method) ((  void (*) (List_1_t4060582815 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3094538963_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3152602435_gshared (List_1_t4060582815 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3152602435(__this, ___enumerable0, method) ((  void (*) (List_1_t4060582815 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3152602435_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m404723180_gshared (List_1_t4060582815 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m404723180(__this, ___collection0, method) ((  void (*) (List_1_t4060582815 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m404723180_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t582280079 * List_1_AsReadOnly_m1739968659_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1739968659(__this, method) ((  ReadOnlyCollection_1_t582280079 * (*) (List_1_t4060582815 *, const MethodInfo*))List_1_AsReadOnly_m1739968659_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Clear()
extern "C"  void List_1_Clear_m767500934_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_Clear_m767500934(__this, method) ((  void (*) (List_1_t4060582815 *, const MethodInfo*))List_1_Clear_m767500934_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Contains(T)
extern "C"  bool List_1_Contains_m2054981520_gshared (List_1_t4060582815 * __this, PeerInfoPlayer_t396494387  ___item0, const MethodInfo* method);
#define List_1_Contains_m2054981520(__this, ___item0, method) ((  bool (*) (List_1_t4060582815 *, PeerInfoPlayer_t396494387 , const MethodInfo*))List_1_Contains_m2054981520_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m4142741310_gshared (List_1_t4060582815 * __this, PeerInfoPlayerU5BU5D_t1632998306* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m4142741310(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4060582815 *, PeerInfoPlayerU5BU5D_t1632998306*, int32_t, const MethodInfo*))List_1_CopyTo_m4142741310_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Find(System.Predicate`1<T>)
extern "C"  PeerInfoPlayer_t396494387  List_1_Find_m3665674718_gshared (List_1_t4060582815 * __this, Predicate_1_t3134431798 * ___match0, const MethodInfo* method);
#define List_1_Find_m3665674718(__this, ___match0, method) ((  PeerInfoPlayer_t396494387  (*) (List_1_t4060582815 *, Predicate_1_t3134431798 *, const MethodInfo*))List_1_Find_m3665674718_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m996386055_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3134431798 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m996386055(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3134431798 *, const MethodInfo*))List_1_CheckMatch_m996386055_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2124310974_gshared (List_1_t4060582815 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3134431798 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2124310974(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4060582815 *, int32_t, int32_t, Predicate_1_t3134431798 *, const MethodInfo*))List_1_GetIndex_m2124310974_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::GetEnumerator()
extern "C"  Enumerator_t3595312489  List_1_GetEnumerator_m1986814723_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1986814723(__this, method) ((  Enumerator_t3595312489  (*) (List_1_t4060582815 *, const MethodInfo*))List_1_GetEnumerator_m1986814723_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1825665384_gshared (List_1_t4060582815 * __this, PeerInfoPlayer_t396494387  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m1825665384(__this, ___item0, method) ((  int32_t (*) (List_1_t4060582815 *, PeerInfoPlayer_t396494387 , const MethodInfo*))List_1_IndexOf_m1825665384_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2741692131_gshared (List_1_t4060582815 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2741692131(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4060582815 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2741692131_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m801697294_gshared (List_1_t4060582815 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m801697294(__this, ___index0, method) ((  void (*) (List_1_t4060582815 *, int32_t, const MethodInfo*))List_1_CheckIndex_m801697294_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2315359753_gshared (List_1_t4060582815 * __this, int32_t ___index0, PeerInfoPlayer_t396494387  ___item1, const MethodInfo* method);
#define List_1_Insert_m2315359753(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4060582815 *, int32_t, PeerInfoPlayer_t396494387 , const MethodInfo*))List_1_Insert_m2315359753_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m235921076_gshared (List_1_t4060582815 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m235921076(__this, ___collection0, method) ((  void (*) (List_1_t4060582815 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m235921076_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Remove(T)
extern "C"  bool List_1_Remove_m549880321_gshared (List_1_t4060582815 * __this, PeerInfoPlayer_t396494387  ___item0, const MethodInfo* method);
#define List_1_Remove_m549880321(__this, ___item0, method) ((  bool (*) (List_1_t4060582815 *, PeerInfoPlayer_t396494387 , const MethodInfo*))List_1_Remove_m549880321_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1755803775_gshared (List_1_t4060582815 * __this, Predicate_1_t3134431798 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1755803775(__this, ___match0, method) ((  int32_t (*) (List_1_t4060582815 *, Predicate_1_t3134431798 *, const MethodInfo*))List_1_RemoveAll_m1755803775_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3143903779_gshared (List_1_t4060582815 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3143903779(__this, ___index0, method) ((  void (*) (List_1_t4060582815 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3143903779_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Reverse()
extern "C"  void List_1_Reverse_m3858166747_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_Reverse_m3858166747(__this, method) ((  void (*) (List_1_t4060582815 *, const MethodInfo*))List_1_Reverse_m3858166747_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Sort()
extern "C"  void List_1_Sort_m3119890457_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_Sort_m3119890457(__this, method) ((  void (*) (List_1_t4060582815 *, const MethodInfo*))List_1_Sort_m3119890457_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1031666941_gshared (List_1_t4060582815 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1031666941(__this, ___comparer0, method) ((  void (*) (List_1_t4060582815 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1031666941_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2367483174_gshared (List_1_t4060582815 * __this, Comparison_1_t1658233238 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2367483174(__this, ___comparison0, method) ((  void (*) (List_1_t4060582815 *, Comparison_1_t1658233238 *, const MethodInfo*))List_1_Sort_m2367483174_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::ToArray()
extern "C"  PeerInfoPlayerU5BU5D_t1632998306* List_1_ToArray_m3942823211_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_ToArray_m3942823211(__this, method) ((  PeerInfoPlayerU5BU5D_t1632998306* (*) (List_1_t4060582815 *, const MethodInfo*))List_1_ToArray_m3942823211_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3638228700_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3638228700(__this, method) ((  void (*) (List_1_t4060582815 *, const MethodInfo*))List_1_TrimExcess_m3638228700_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1865877282_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1865877282(__this, method) ((  int32_t (*) (List_1_t4060582815 *, const MethodInfo*))List_1_get_Capacity_m1865877282_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m361059603_gshared (List_1_t4060582815 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m361059603(__this, ___value0, method) ((  void (*) (List_1_t4060582815 *, int32_t, const MethodInfo*))List_1_set_Capacity_m361059603_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::get_Count()
extern "C"  int32_t List_1_get_Count_m3036295265_gshared (List_1_t4060582815 * __this, const MethodInfo* method);
#define List_1_get_Count_m3036295265(__this, method) ((  int32_t (*) (List_1_t4060582815 *, const MethodInfo*))List_1_get_Count_m3036295265_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::get_Item(System.Int32)
extern "C"  PeerInfoPlayer_t396494387  List_1_get_Item_m4283325401_gshared (List_1_t4060582815 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4283325401(__this, ___index0, method) ((  PeerInfoPlayer_t396494387  (*) (List_1_t4060582815 *, int32_t, const MethodInfo*))List_1_get_Item_m4283325401_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2775792780_gshared (List_1_t4060582815 * __this, int32_t ___index0, PeerInfoPlayer_t396494387  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2775792780(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4060582815 *, int32_t, PeerInfoPlayer_t396494387 , const MethodInfo*))List_1_set_Item_m2775792780_gshared)(__this, ___index0, ___value1, method)
