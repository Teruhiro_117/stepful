﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ESkytree
struct ESkytree_t2245705948;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ESkytree::.ctor()
extern "C"  void ESkytree__ctor_m3421849381 (ESkytree_t2245705948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ESkytree::Init(System.String)
extern "C"  void ESkytree_Init_m4005599381 (ESkytree_t2245705948 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
