﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Koma
struct Koma_t3011766596;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Koma::.ctor()
extern "C"  void Koma__ctor_m1354333883 (Koma_t3011766596 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Koma::Init(System.String)
extern "C"  void Koma_Init_m2356132927 (Koma_t3011766596 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Koma::Move(System.Int32,System.Int32)
extern "C"  void Koma_Move_m1070855718 (Koma_t3011766596 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Koma::Draw(System.Int32,System.Int32)
extern "C"  void Koma_Draw_m1486938147 (Koma_t3011766596 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Koma::Erase(System.Int32,System.Int32)
extern "C"  void Koma_Erase_m3965959631 (Koma_t3011766596 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Koma::Action(System.Int32)
extern "C"  void Koma_Action_m1381793788 (Koma_t3011766596 * __this, int32_t ___resources0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
