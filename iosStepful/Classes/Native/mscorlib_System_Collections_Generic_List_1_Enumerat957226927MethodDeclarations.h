﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct List_1_t1422497253;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat957226927.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2053376121.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1124433566_gshared (Enumerator_t957226927 * __this, List_1_t1422497253 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1124433566(__this, ___l0, method) ((  void (*) (Enumerator_t957226927 *, List_1_t1422497253 *, const MethodInfo*))Enumerator__ctor_m1124433566_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2732036044_gshared (Enumerator_t957226927 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2732036044(__this, method) ((  void (*) (Enumerator_t957226927 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2732036044_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1667010418_gshared (Enumerator_t957226927 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1667010418(__this, method) ((  Il2CppObject * (*) (Enumerator_t957226927 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1667010418_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m708056949_gshared (Enumerator_t957226927 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m708056949(__this, method) ((  void (*) (Enumerator_t957226927 *, const MethodInfo*))Enumerator_Dispose_m708056949_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m790311576_gshared (Enumerator_t957226927 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m790311576(__this, method) ((  void (*) (Enumerator_t957226927 *, const MethodInfo*))Enumerator_VerifyState_m790311576_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1827704352_gshared (Enumerator_t957226927 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1827704352(__this, method) ((  bool (*) (Enumerator_t957226927 *, const MethodInfo*))Enumerator_MoveNext_m1827704352_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::get_Current()
extern "C"  PendingPlayerInfo_t2053376121  Enumerator_get_Current_m2426197637_gshared (Enumerator_t957226927 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2426197637(__this, method) ((  PendingPlayerInfo_t2053376121  (*) (Enumerator_t957226927 *, const MethodInfo*))Enumerator_get_Current_m2426197637_gshared)(__this, method)
