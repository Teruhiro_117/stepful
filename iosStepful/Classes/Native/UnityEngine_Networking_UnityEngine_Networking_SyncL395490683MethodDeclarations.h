﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync1055719745MethodDeclarations.h"

// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.String>::.ctor(System.Object,System.IntPtr)
#define SyncListChanged__ctor_m3096844301(__this, ___object0, ___method1, method) ((  void (*) (SyncListChanged_t395490683 *, Il2CppObject *, IntPtr_t, const MethodInfo*))SyncListChanged__ctor_m853520105_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.String>::Invoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
#define SyncListChanged_Invoke_m178876483(__this, ___op0, ___itemIndex1, method) ((  void (*) (SyncListChanged_t395490683 *, int32_t, int32_t, const MethodInfo*))SyncListChanged_Invoke_m931180591_gshared)(__this, ___op0, ___itemIndex1, method)
// System.IAsyncResult UnityEngine.Networking.SyncList`1/SyncListChanged<System.String>::BeginInvoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,System.AsyncCallback,System.Object)
#define SyncListChanged_BeginInvoke_m276794616(__this, ___op0, ___itemIndex1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (SyncListChanged_t395490683 *, int32_t, int32_t, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))SyncListChanged_BeginInvoke_m1984733246_gshared)(__this, ___op0, ___itemIndex1, ___callback2, ___object3, method)
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.String>::EndInvoke(System.IAsyncResult)
#define SyncListChanged_EndInvoke_m2379609339(__this, ___result0, method) ((  void (*) (SyncListChanged_t395490683 *, Il2CppObject *, const MethodInfo*))SyncListChanged_EndInvoke_m523398343_gshared)(__this, ___result0, method)
