﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/FaceCamSessionStartedDelegate
struct FaceCamSessionStartedDelegate_t1733547424;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/FaceCamSessionStartedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void FaceCamSessionStartedDelegate__ctor_m1763728449 (FaceCamSessionStartedDelegate_t1733547424 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/FaceCamSessionStartedDelegate::Invoke()
extern "C"  void FaceCamSessionStartedDelegate_Invoke_m1284990449 (FaceCamSessionStartedDelegate_t1733547424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/FaceCamSessionStartedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FaceCamSessionStartedDelegate_BeginInvoke_m4215735148 (FaceCamSessionStartedDelegate_t1733547424 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/FaceCamSessionStartedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void FaceCamSessionStartedDelegate_EndInvoke_m2537038431 (FaceCamSessionStartedDelegate_t1733547424 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
