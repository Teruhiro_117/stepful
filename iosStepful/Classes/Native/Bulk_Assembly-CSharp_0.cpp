﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// actionButton
struct actionButton_t3234286678;
// banmen
struct banmen_t2510752313;
// System.Object
struct Il2CppObject;
// arrangeKoma
struct arrangeKoma_t3423810832;
// System.Byte[0...,0...]
struct ByteU5BU2CU5D_t3397334014;
// System.Int32[0...,0...]
struct Int32U5BU2CU5D_t3030399642;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Koma
struct Koma_t3011766596;
// baseMethods
struct baseMethods_t2705876163;
// Singleton
struct Singleton_t1770047133;
// createCanvas
struct createCanvas_t3354593624;
// createSingleton
struct createSingleton_t313015705;
// Data
struct Data_t3569509720;
// Distribution
struct Distribution_t1524299538;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t410733016;
// Drone
struct Drone_t860454962;
// EDrone
struct EDrone_t2061607325;
// EFactory
struct EFactory_t2050856767;
// EGRobot
struct EGRobot_t1059295608;
// EHuman
struct EHuman_t715499684;
// EMissile
struct EMissile_t3534618099;
// Empty
struct Empty_t4012560223;
// ESkytree
struct ESkytree_t2245705948;
// ESRobot
struct ESRobot_t2546702684;
// Factory
struct Factory_t931158954;
// Futi
struct Futi_t612262014;
// GRobot
struct GRobot_t456225233;
// Human
struct Human_t1156088493;
// inputName
struct inputName_t1068137479;
// Missile
struct Missile_t813944928;
// MouseLook
struct MouseLook_t2643707144;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// OpeningButton
struct OpeningButton_t2416718932;
// pushButton
struct pushButton_t4005487804;
// SaveScript
struct SaveScript_t1729063510;
// selectKoma
struct selectKoma_t1760479012;
// selectPlace
struct selectPlace_t742894733;
// showData
struct showData_t3631251573;
// Skytree
struct Skytree_t850646749;
// SmoothCameraOrbit
struct SmoothCameraOrbit_t604892967;
// soundOnOff
struct soundOnOff_t408115747;
// SRobot
struct SRobot_t456246757;
// StableAspect
struct StableAspect_t3385526103;
// UnityEngine.Camera
struct Camera_t189460977;
// startToggle
struct startToggle_t123871076;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_actionButton3234286678.h"
#include "AssemblyU2DCSharp_actionButton3234286678MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_banmen2510752313MethodDeclarations.h"
#include "AssemblyU2DCSharp_banmen2510752313.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_arrangeKoma3423810832.h"
#include "AssemblyU2DCSharp_arrangeKoma3423810832MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "AssemblyU2DCSharp_arrangeKoma_KOMAINF446687068.h"
#include "AssemblyU2DCSharp_arrangeKoma_KOMAINF446687068MethodDeclarations.h"
#include "AssemblyU2DCSharp_baseMethods2705876163MethodDeclarations.h"
#include "AssemblyU2DCSharp_Empty4012560223MethodDeclarations.h"
#include "AssemblyU2DCSharp_Human1156088493MethodDeclarations.h"
#include "AssemblyU2DCSharp_GRobot456225233MethodDeclarations.h"
#include "AssemblyU2DCSharp_SRobot456246757MethodDeclarations.h"
#include "AssemblyU2DCSharp_Missile813944928MethodDeclarations.h"
#include "AssemblyU2DCSharp_Factory931158954MethodDeclarations.h"
#include "AssemblyU2DCSharp_Drone860454962MethodDeclarations.h"
#include "AssemblyU2DCSharp_Skytree850646749MethodDeclarations.h"
#include "AssemblyU2DCSharp_EHuman715499684MethodDeclarations.h"
#include "AssemblyU2DCSharp_EGRobot1059295608MethodDeclarations.h"
#include "AssemblyU2DCSharp_ESRobot2546702684MethodDeclarations.h"
#include "AssemblyU2DCSharp_EMissile3534618099MethodDeclarations.h"
#include "AssemblyU2DCSharp_EFactory2050856767MethodDeclarations.h"
#include "AssemblyU2DCSharp_EDrone2061607325MethodDeclarations.h"
#include "AssemblyU2DCSharp_ESkytree2245705948MethodDeclarations.h"
#include "AssemblyU2DCSharp_Futi612262014MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283MethodDeclarations.h"
#include "AssemblyU2DCSharp_baseMethods2705876163.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_Koma3011766596.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283.h"
#include "AssemblyU2DCSharp_Empty4012560223.h"
#include "AssemblyU2DCSharp_Human1156088493.h"
#include "AssemblyU2DCSharp_GRobot456225233.h"
#include "AssemblyU2DCSharp_SRobot456246757.h"
#include "AssemblyU2DCSharp_Missile813944928.h"
#include "AssemblyU2DCSharp_Factory931158954.h"
#include "AssemblyU2DCSharp_Drone860454962.h"
#include "AssemblyU2DCSharp_Skytree850646749.h"
#include "AssemblyU2DCSharp_EHuman715499684.h"
#include "AssemblyU2DCSharp_EGRobot1059295608.h"
#include "AssemblyU2DCSharp_ESRobot2546702684.h"
#include "AssemblyU2DCSharp_EMissile3534618099.h"
#include "AssemblyU2DCSharp_EFactory2050856767.h"
#include "AssemblyU2DCSharp_EDrone2061607325.h"
#include "AssemblyU2DCSharp_ESkytree2245705948.h"
#include "AssemblyU2DCSharp_Futi612262014.h"
#include "AssemblyU2DCSharp_Koma3011766596MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay1799077027MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "AssemblyU2DCSharp_banmen_KOMAINF1054450899.h"
#include "AssemblyU2DCSharp_banmen_KOMAINF1054450899MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharp_Singleton1770047133MethodDeclarations.h"
#include "AssemblyU2DCSharp_Singleton1770047133.h"
#include "AssemblyU2DCSharp_createCanvas3354593624.h"
#include "AssemblyU2DCSharp_createCanvas3354593624MethodDeclarations.h"
#include "AssemblyU2DCSharp_createSingleton313015705.h"
#include "AssemblyU2DCSharp_createSingleton313015705MethodDeclarations.h"
#include "AssemblyU2DCSharp_Data3569509720.h"
#include "AssemblyU2DCSharp_Data3569509720MethodDeclarations.h"
#include "AssemblyU2DCSharp_Distribution1524299538.h"
#include "AssemblyU2DCSharp_Distribution1524299538MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster410733016.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_inputName1068137479.h"
#include "AssemblyU2DCSharp_inputName1068137479MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530.h"
#include "AssemblyU2DCSharp_MouseLook2643707144.h"
#include "AssemblyU2DCSharp_MouseLook2643707144MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_MouseLook_RotationAxes2150291266.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "AssemblyU2DCSharp_MouseLook_RotationAxes2150291266MethodDeclarations.h"
#include "AssemblyU2DCSharp_OpeningButton2416718932.h"
#include "AssemblyU2DCSharp_OpeningButton2416718932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "AssemblyU2DCSharp_pushButton4005487804.h"
#include "AssemblyU2DCSharp_pushButton4005487804MethodDeclarations.h"
#include "AssemblyU2DCSharp_SaveScript1729063510.h"
#include "AssemblyU2DCSharp_SaveScript1729063510MethodDeclarations.h"
#include "AssemblyU2DCSharp_selectKoma1760479012.h"
#include "AssemblyU2DCSharp_selectKoma1760479012MethodDeclarations.h"
#include "AssemblyU2DCSharp_selectPlace742894733.h"
#include "AssemblyU2DCSharp_selectPlace742894733MethodDeclarations.h"
#include "AssemblyU2DCSharp_showData3631251573.h"
#include "AssemblyU2DCSharp_showData3631251573MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "UnityEngine_UnityEngine_AudioListener1996719162MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001MethodDeclarations.h"
#include "AssemblyU2DCSharp_SmoothCameraOrbit604892967.h"
#include "AssemblyU2DCSharp_SmoothCameraOrbit604892967MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "AssemblyU2DCSharp_soundOnOff408115747.h"
#include "AssemblyU2DCSharp_soundOnOff408115747MethodDeclarations.h"
#include "AssemblyU2DCSharp_StableAspect3385526103.h"
#include "AssemblyU2DCSharp_StableAspect3385526103MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "AssemblyU2DCSharp_startToggle123871076.h"
#include "AssemblyU2DCSharp_startToggle123871076MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<banmen>()
#define GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266(__this, method) ((  banmen_t2510752313 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2042527209_m4162535761(__this, method) ((  Image_t2042527209 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m2884518678_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define Resources_Load_TisIl2CppObject_m2884518678(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2884518678_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Resources::Load<UnityEngine.Sprite>(System.String)
#define Resources_Load_TisSprite_t309593783_m1838468444(__this /* static, unused */, p0, method) ((  Sprite_t309593783 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2884518678_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m1217399699(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Slider>()
#define GameObject_GetComponent_TisSlider_t297367283_m33033319(__this, method) ((  Slider_t297367283 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m447919519(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1756533147_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<Singleton>()
#define GameObject_GetComponent_TisSingleton_t1770047133_m2880166840(__this, method) ((  Singleton_t1770047133 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3813873105(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Canvas>()
#define GameObject_AddComponent_TisCanvas_t209405766_m2311451932(__this, method) ((  Canvas_t209405766 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.GraphicRaycaster>()
#define GameObject_AddComponent_TisGraphicRaycaster_t410733016_m2075178258(__this, method) ((  GraphicRaycaster_t410733016 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Canvas>()
#define GameObject_GetComponent_TisCanvas_t209405766_m195193039(__this, method) ((  Canvas_t209405766 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t4233889191_m88144364(__this, method) ((  Rigidbody_t4233889191 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<arrangeKoma>()
#define GameObject_GetComponent_TisarrangeKoma_t3423810832_m115679363(__this, method) ((  arrangeKoma_t3423810832 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m3276577584(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Toggle>()
#define Component_GetComponent_TisToggle_t3976754468_m3994209854(__this, method) ((  Toggle_t3976754468 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void actionButton::.ctor()
extern "C"  void actionButton__ctor_m619258599 (actionButton_t3234286678 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void actionButton::Start()
extern Il2CppCodeGenString* _stringLiteral3901280377;
extern const uint32_t actionButton_Start_m4164753571_MetadataUsageId;
extern "C"  void actionButton_Start_m4164753571 (actionButton_t3234286678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (actionButton_Start_m4164753571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3901280377, /*hidden argument*/NULL);
		__this->set_refObj_2(L_0);
		return;
	}
}
// System.Void actionButton::Update()
extern "C"  void actionButton_Update_m3923835296 (actionButton_t3234286678 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void actionButton::OnClick()
extern const MethodInfo* GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266_MethodInfo_var;
extern const uint32_t actionButton_OnClick_m1181668582_MetadataUsageId;
extern "C"  void actionButton_OnClick_m1181668582 (actionButton_t3234286678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (actionButton_OnClick_m1181668582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	banmen_t2510752313 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_refObj_2();
		NullCheck(L_0);
		banmen_t2510752313 * L_1 = GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266(L_0, /*hidden argument*/GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266_MethodInfo_var);
		V_0 = L_1;
		banmen_t2510752313 * L_2 = V_0;
		int32_t L_3 = __this->get_actionNum_3();
		NullCheck(L_2);
		banmen_setKomaNumber_m1079153342(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void arrangeKoma::.ctor()
extern "C"  void arrangeKoma__ctor_m1481271067 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void arrangeKoma::Start()
extern "C"  void arrangeKoma_Start_m1732940823 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	{
		arrangeKoma_Init_m3905406573(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void arrangeKoma::Update()
extern "C"  void arrangeKoma_Update_m1760216046 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void arrangeKoma::Init()
extern "C"  void arrangeKoma_Init_m3905406573 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	{
		arrangeKoma_setKoma_m1256667485(__this, /*hidden argument*/NULL);
		arrangeKoma_setColorWhite_m729711447(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[0...,0...] arrangeKoma::getStartBoard()
extern Il2CppClass* arrangeKoma_t3423810832_il2cpp_TypeInfo_var;
extern const uint32_t arrangeKoma_getStartBoard_m4029400191_MetadataUsageId;
extern "C"  ByteU5BU2CU5D_t3397334014* arrangeKoma_getStartBoard_m4029400191 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_getStartBoard_m4029400191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU2CU5D_t3397334014* L_0 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_startBoard_2();
		return (ByteU5BU2CU5D_t3397334014*)L_0;
	}
}
// System.Boolean arrangeKoma::getHuman()
extern "C"  bool arrangeKoma_getHuman_m3006929540 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_human_4();
		return L_0;
	}
}
// System.Int32[0...,0...] arrangeKoma::getFactoryLocation()
extern Il2CppClass* arrangeKoma_t3423810832_il2cpp_TypeInfo_var;
extern const uint32_t arrangeKoma_getFactoryLocation_m3685241018_MetadataUsageId;
extern "C"  Int32U5BU2CU5D_t3030399642* arrangeKoma_getFactoryLocation_m3685241018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_getFactoryLocation_m3685241018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU2CU5D_t3030399642* L_0 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_factoryLocation_17();
		return (Int32U5BU2CU5D_t3030399642*)L_0;
	}
}
// System.Int32[0...,0...] arrangeKoma::getBarrier()
extern Il2CppClass* arrangeKoma_t3423810832_il2cpp_TypeInfo_var;
extern const uint32_t arrangeKoma_getBarrier_m3597185560_MetadataUsageId;
extern "C"  Int32U5BU2CU5D_t3030399642* arrangeKoma_getBarrier_m3597185560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_getBarrier_m3597185560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU2CU5D_t3030399642* L_0 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_barrier_16();
		return (Int32U5BU2CU5D_t3030399642*)L_0;
	}
}
// System.Int32 arrangeKoma::getMyResources()
extern Il2CppClass* arrangeKoma_t3423810832_il2cpp_TypeInfo_var;
extern const uint32_t arrangeKoma_getMyResources_m4065310466_MetadataUsageId;
extern "C"  int32_t arrangeKoma_getMyResources_m4065310466 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_getMyResources_m4065310466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_myResources_19();
		return L_0;
	}
}
// System.Void arrangeKoma::setKoma()
extern Il2CppClass* ByteU5BU2CU5D_t3397334014_il2cpp_TypeInfo_var;
extern Il2CppClass* arrangeKoma_t3423810832_il2cpp_TypeInfo_var;
extern Il2CppClass* BooleanU5BU2CU5D_t3568034316_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var;
extern const uint32_t arrangeKoma_setKoma_m1256667485_MetadataUsageId;
extern "C"  void arrangeKoma_setKoma_m1256667485 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_setKoma_m1256667485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		il2cpp_array_size_t L_1[] = { (il2cpp_array_size_t)8, (il2cpp_array_size_t)8 };
		ByteU5BU2CU5D_t3397334014* L_0 = (ByteU5BU2CU5D_t3397334014*)GenArrayNew(ByteU5BU2CU5D_t3397334014_il2cpp_TypeInfo_var, L_1);
		((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->set_startBoard_2((ByteU5BU2CU5D_t3397334014*)L_0);
		il2cpp_array_size_t L_3[] = { (il2cpp_array_size_t)2, (il2cpp_array_size_t)6 };
		BooleanU5BU2CU5D_t3568034316* L_2 = (BooleanU5BU2CU5D_t3568034316*)GenArrayNew(BooleanU5BU2CU5D_t3568034316_il2cpp_TypeInfo_var, L_3);
		__this->set_canTouchPlace_3((BooleanU5BU2CU5D_t3568034316*)L_2);
		il2cpp_array_size_t L_5[] = { (il2cpp_array_size_t)2, (il2cpp_array_size_t)2 };
		Int32U5BU2CU5D_t3030399642* L_4 = (Int32U5BU2CU5D_t3030399642*)GenArrayNew(Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var, L_5);
		((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->set_barrier_16((Int32U5BU2CU5D_t3030399642*)L_4);
		Int32U5BU2CU5D_t3030399642* L_6 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_barrier_16();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_6);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_6)->SetAt(0, 0, (-1));
		Int32U5BU2CU5D_t3030399642* L_7 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_barrier_16();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7)->SetAt(0, 1, (-1));
		il2cpp_array_size_t L_9[] = { (il2cpp_array_size_t)2, (il2cpp_array_size_t)2 };
		Int32U5BU2CU5D_t3030399642* L_8 = (Int32U5BU2CU5D_t3030399642*)GenArrayNew(Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var, L_9);
		((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->set_factoryLocation_17((Int32U5BU2CU5D_t3030399642*)L_8);
		Int32U5BU2CU5D_t3030399642* L_10 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_factoryLocation_17();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_10);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_10)->SetAt(0, 0, (-1));
		Int32U5BU2CU5D_t3030399642* L_11 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_factoryLocation_17();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_11);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_11)->SetAt(0, 1, (-1));
		((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->set_myResources_19(((int32_t)40));
		__this->set_resources_5((bool)1);
		__this->set_human_4((bool)0);
		V_0 = 0;
		goto IL_00a4;
	}

IL_0081:
	{
		V_1 = 0;
		goto IL_0099;
	}

IL_0088:
	{
		ByteU5BU2CU5D_t3397334014* L_12 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_startBoard_2();
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		NullCheck((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_12);
		((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_12)->SetAt(L_13, L_14, 0);
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_16 = V_1;
		if ((((int32_t)L_16) < ((int32_t)6)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_17 = V_0;
		V_0 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_00a4:
	{
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) < ((int32_t)2)))
		{
			goto IL_0081;
		}
	}
	{
		V_2 = 0;
		goto IL_00ee;
	}

IL_00b2:
	{
		ByteU5BU2CU5D_t3397334014* L_19 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_startBoard_2();
		int32_t L_20 = V_2;
		NullCheck((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_19);
		((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_19)->SetAt(L_20, 0, ((int32_t)15));
		ByteU5BU2CU5D_t3397334014* L_21 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_startBoard_2();
		int32_t L_22 = V_2;
		NullCheck((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_21);
		((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_21)->SetAt(L_22, 7, ((int32_t)15));
		ByteU5BU2CU5D_t3397334014* L_23 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_startBoard_2();
		int32_t L_24 = V_2;
		NullCheck((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_23);
		((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_23)->SetAt(0, L_24, ((int32_t)15));
		ByteU5BU2CU5D_t3397334014* L_25 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_startBoard_2();
		int32_t L_26 = V_2;
		NullCheck((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_25);
		((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_25)->SetAt(7, L_26, ((int32_t)15));
		int32_t L_27 = V_2;
		V_2 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00ee:
	{
		int32_t L_28 = V_2;
		if ((((int32_t)L_28) < ((int32_t)8)))
		{
			goto IL_00b2;
		}
	}
	{
		arrangeKoma_canNotTouchAll_m2596981714(__this, /*hidden argument*/NULL);
		Int32U5BU2CU5D_t3030399642* L_29 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_barrier_16();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_29);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_29)->SetAt(1, 0, 2);
		Int32U5BU2CU5D_t3030399642* L_30 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_barrier_16();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_30);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_30)->SetAt(1, 1, 1);
		Int32U5BU2CU5D_t3030399642* L_31 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_factoryLocation_17();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_31);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_31)->SetAt(1, 0, 4);
		Int32U5BU2CU5D_t3030399642* L_32 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_factoryLocation_17();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_32);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_32)->SetAt(1, 1, 5);
		return;
	}
}
// System.Void arrangeKoma::printKoma()
extern Il2CppClass* arrangeKoma_t3423810832_il2cpp_TypeInfo_var;
extern const uint32_t arrangeKoma_printKoma_m261318148_MetadataUsageId;
extern "C"  void arrangeKoma_printKoma_m261318148 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_printKoma_m261318148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BooleanU5BU2CU5D_t3568034316* L_0 = __this->get_canTouchPlace_3();
		int32_t L_1 = __this->get_line_10();
		int32_t L_2 = __this->get_row_11();
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_0);
		bool L_3 = ((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_0)->GetAt(L_1, L_2);
		if (!L_3)
		{
			goto IL_010c;
		}
	}
	{
		bool L_4 = __this->get_human_4();
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_5 = __this->get_komaNum_7();
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_010c;
		}
	}

IL_0033:
	{
		arrangeKoma_canNotTouchAll_m2596981714(__this, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_komaNum_7();
		arrangeKoma_countResources_m2933092726(__this, L_6, /*hidden argument*/NULL);
		bool L_7 = __this->get_resources_5();
		if (!L_7)
		{
			goto IL_009f;
		}
	}
	{
		ByteU5BU2CU5D_t3397334014* L_8 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_startBoard_2();
		int32_t L_9 = __this->get_line_10();
		int32_t L_10 = __this->get_row_11();
		NullCheck((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_8);
		uint8_t L_11 = ((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_8)->GetAt(L_9, L_10);
		arrangeKoma_changeKomaResources_m4200569561(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = __this->get_line_10();
		int32_t L_13 = __this->get_row_11();
		arrangeKoma_paint_m769033625(__this, L_12, L_13, /*hidden argument*/NULL);
		ByteU5BU2CU5D_t3397334014* L_14 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_startBoard_2();
		int32_t L_15 = __this->get_row_11();
		int32_t L_16 = __this->get_line_10();
		int32_t L_17 = __this->get_komaNum_7();
		NullCheck((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_14);
		((ByteU5BU2CU5D_t3397334014*)(ByteU5BU2CU5D_t3397334014*)L_14)->SetAt(((int32_t)((int32_t)L_15+(int32_t)1)), ((int32_t)((int32_t)L_16+(int32_t)5)), (((int32_t)((uint8_t)L_17))));
	}

IL_009f:
	{
		int32_t L_18 = __this->get_komaNum_7();
		if ((!(((uint32_t)L_18) == ((uint32_t)7))))
		{
			goto IL_00d8;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_19 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_barrier_16();
		int32_t L_20 = __this->get_row_11();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_19);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_19)->SetAt(0, 0, ((int32_t)((int32_t)L_20+(int32_t)1)));
		Int32U5BU2CU5D_t3030399642* L_21 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_barrier_16();
		int32_t L_22 = __this->get_line_10();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_21);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_21)->SetAt(0, 1, ((int32_t)((int32_t)L_22+(int32_t)5)));
		goto IL_010c;
	}

IL_00d8:
	{
		int32_t L_23 = __this->get_komaNum_7();
		if ((!(((uint32_t)L_23) == ((uint32_t)5))))
		{
			goto IL_010c;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_24 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_factoryLocation_17();
		int32_t L_25 = __this->get_row_11();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_24);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_24)->SetAt(0, 0, ((int32_t)((int32_t)L_25+(int32_t)1)));
		Int32U5BU2CU5D_t3030399642* L_26 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_factoryLocation_17();
		int32_t L_27 = __this->get_line_10();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_26);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_26)->SetAt(0, 1, ((int32_t)((int32_t)L_27+(int32_t)5)));
	}

IL_010c:
	{
		return;
	}
}
// System.Void arrangeKoma::paint(System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral281567643;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern const uint32_t arrangeKoma_paint_m769033625_MetadataUsageId;
extern "C"  void arrangeKoma_paint_m769033625 (arrangeKoma_t3423810832 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_paint_m769033625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		arrangeKoma_setColorWhite_m729711447(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral281567643);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral281567643);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = ___r1;
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral372029313);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = ___l0;
		int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = GameObject_Find_m836511350(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Image_t2042527209 * L_12 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_11, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		float L_13 = __this->get_changeRed_12();
		float L_14 = __this->get_changeGreen_13();
		float L_15 = __this->get_changeBlue_14();
		float L_16 = __this->get_changeAlpha_15();
		Color_t2020392075  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Color__ctor_m1909920690(&L_17, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_17);
		ObjectU5BU5D_t3614634134* L_18 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral281567643);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral281567643);
		ObjectU5BU5D_t3614634134* L_19 = L_18;
		int32_t L_20 = ___r1;
		int32_t L_21 = ((int32_t)((int32_t)L_20+(int32_t)1));
		Il2CppObject * L_22 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_22);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = L_19;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral372029313);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_24 = L_23;
		int32_t L_25 = ___l0;
		int32_t L_26 = ((int32_t)((int32_t)L_25+(int32_t)1));
		Il2CppObject * L_27 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_27);
		String_t* L_28 = String_Concat_m3881798623(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_29 = GameObject_Find_m836511350(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Image_t2042527209 * L_30 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_29, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		__this->set_img_8(L_30);
		Image_t2042527209 * L_31 = __this->get_img_8();
		Sprite_t309593783 * L_32 = __this->get_koma_6();
		NullCheck(L_31);
		Image_set_sprite_m1800056820(L_31, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void arrangeKoma::allPrintColorBottom()
extern "C"  void arrangeKoma_allPrintColorBottom_m969512469 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_0029;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_001e;
	}

IL_000e:
	{
		int32_t L_0 = V_0;
		int32_t L_1 = V_1;
		arrangeKoma_printColorBottom_m1192818386(__this, ((int32_t)((int32_t)L_0+(int32_t)1)), ((int32_t)((int32_t)L_1+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		V_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) < ((int32_t)6)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)6)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void arrangeKoma::printColorBottom(System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral281567643;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern Il2CppCodeGenString* _stringLiteral2347644584;
extern const uint32_t arrangeKoma_printColorBottom_m1192818386_MetadataUsageId;
extern "C"  void arrangeKoma_printColorBottom_m1192818386 (arrangeKoma_t3423810832 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_printColorBottom_m1192818386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral281567643);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral281567643);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = ___l0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral372029313);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = ___r1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral2347644584);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2347644584);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3881798623(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = GameObject_Find_m836511350(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Image_t2042527209 * L_13 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		float L_14 = __this->get_changeRed_12();
		float L_15 = __this->get_changeGreen_13();
		float L_16 = __this->get_changeBlue_14();
		float L_17 = __this->get_changeAlpha_15();
		Color_t2020392075  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Color__ctor_m1909920690(&L_18, L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_18);
		return;
	}
}
// System.Void arrangeKoma::printColorAbove(System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral281567643;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern const uint32_t arrangeKoma_printColorAbove_m2660003704_MetadataUsageId;
extern "C"  void arrangeKoma_printColorAbove_m2660003704 (arrangeKoma_t3423810832 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_printColorAbove_m2660003704_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral281567643);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral281567643);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = ___l0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral372029313);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = ___r1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = GameObject_Find_m836511350(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Image_t2042527209 * L_12 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_11, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		float L_13 = __this->get_changeRed_12();
		float L_14 = __this->get_changeGreen_13();
		float L_15 = __this->get_changeBlue_14();
		float L_16 = __this->get_changeAlpha_15();
		Color_t2020392075  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Color__ctor_m1909920690(&L_17, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_17);
		return;
	}
}
// System.Void arrangeKoma::setColorWhite()
extern "C"  void arrangeKoma_setColorWhite_m729711447 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	{
		__this->set_changeRed_12((1.0f));
		__this->set_changeGreen_13((1.0f));
		__this->set_changeBlue_14((1.0f));
		__this->set_changeAlpha_15((1.0f));
		return;
	}
}
// System.Void arrangeKoma::setColorRed()
extern "C"  void arrangeKoma_setColorRed_m3294458039 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	{
		__this->set_changeRed_12((1.0f));
		__this->set_changeGreen_13((0.0f));
		__this->set_changeBlue_14((0.0f));
		__this->set_changeAlpha_15((1.0f));
		return;
	}
}
// System.Void arrangeKoma::setColorBlue()
extern "C"  void arrangeKoma_setColorBlue_m4212109396 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	{
		__this->set_changeRed_12((0.0f));
		__this->set_changeGreen_13((0.0f));
		__this->set_changeBlue_14((1.0f));
		__this->set_changeAlpha_15((1.0f));
		return;
	}
}
// System.Void arrangeKoma::setColorClear()
extern "C"  void arrangeKoma_setColorClear_m3548991013 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	{
		__this->set_changeAlpha_15((0.0f));
		return;
	}
}
// System.Void arrangeKoma::setAllWhite()
extern "C"  void arrangeKoma_setAllWhite_m832677259 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		arrangeKoma_setColorWhite_m729711447(__this, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_002f;
	}

IL_000d:
	{
		V_1 = 0;
		goto IL_0024;
	}

IL_0014:
	{
		int32_t L_0 = V_0;
		int32_t L_1 = V_1;
		arrangeKoma_printColorBottom_m1192818386(__this, ((int32_t)((int32_t)L_0+(int32_t)1)), ((int32_t)((int32_t)L_1+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		V_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) < ((int32_t)6)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)6)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}
}
// System.Void arrangeKoma::inputKoma(System.Int32)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3222005778;
extern Il2CppCodeGenString* _stringLiteral2230921881;
extern Il2CppCodeGenString* _stringLiteral3139759631;
extern Il2CppCodeGenString* _stringLiteral71042275;
extern Il2CppCodeGenString* _stringLiteral2055881876;
extern Il2CppCodeGenString* _stringLiteral1506717344;
extern Il2CppCodeGenString* _stringLiteral1121820566;
extern Il2CppCodeGenString* _stringLiteral2994424589;
extern const uint32_t arrangeKoma_inputKoma_m2346436234_MetadataUsageId;
extern "C"  void arrangeKoma_inputKoma_m2346436234 (arrangeKoma_t3423810832 * __this, int32_t ___num0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_inputKoma_m2346436234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___num0;
		if (L_0 == 0)
		{
			goto IL_0119;
		}
		if (L_0 == 1)
		{
			goto IL_00f7;
		}
		if (L_0 == 2)
		{
			goto IL_00d5;
		}
		if (L_0 == 3)
		{
			goto IL_00b3;
		}
		if (L_0 == 4)
		{
			goto IL_0091;
		}
		if (L_0 == 5)
		{
			goto IL_006f;
		}
		if (L_0 == 6)
		{
			goto IL_004d;
		}
		if (L_0 == 7)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_013b;
	}

IL_002b:
	{
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral3222005778, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		__this->set_koma_6(L_1);
		arrangeKoma_canTouchAll_m931637095(__this, /*hidden argument*/NULL);
		int32_t L_2 = ___num0;
		__this->set_komaNum_7(L_2);
		goto IL_013b;
	}

IL_004d:
	{
		Sprite_t309593783 * L_3 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2230921881, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		__this->set_koma_6(L_3);
		int32_t L_4 = ___num0;
		__this->set_komaNum_7(L_4);
		arrangeKoma_canTouchAll_m931637095(__this, /*hidden argument*/NULL);
		goto IL_013b;
	}

IL_006f:
	{
		Sprite_t309593783 * L_5 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral3139759631, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		__this->set_koma_6(L_5);
		int32_t L_6 = ___num0;
		__this->set_komaNum_7(L_6);
		arrangeKoma_canTouchAll_m931637095(__this, /*hidden argument*/NULL);
		goto IL_013b;
	}

IL_0091:
	{
		Sprite_t309593783 * L_7 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral71042275, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		__this->set_koma_6(L_7);
		int32_t L_8 = ___num0;
		__this->set_komaNum_7(L_8);
		arrangeKoma_canTouchAll_m931637095(__this, /*hidden argument*/NULL);
		goto IL_013b;
	}

IL_00b3:
	{
		Sprite_t309593783 * L_9 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2055881876, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		__this->set_koma_6(L_9);
		int32_t L_10 = ___num0;
		__this->set_komaNum_7(L_10);
		arrangeKoma_canTouchAll_m931637095(__this, /*hidden argument*/NULL);
		goto IL_013b;
	}

IL_00d5:
	{
		Sprite_t309593783 * L_11 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral1506717344, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		__this->set_koma_6(L_11);
		int32_t L_12 = ___num0;
		__this->set_komaNum_7(L_12);
		arrangeKoma_canTouchAll_m931637095(__this, /*hidden argument*/NULL);
		goto IL_013b;
	}

IL_00f7:
	{
		Sprite_t309593783 * L_13 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral1121820566, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		__this->set_koma_6(L_13);
		int32_t L_14 = ___num0;
		__this->set_komaNum_7(L_14);
		arrangeKoma_canTouchAll_m931637095(__this, /*hidden argument*/NULL);
		goto IL_013b;
	}

IL_0119:
	{
		Sprite_t309593783 * L_15 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2994424589, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		__this->set_koma_6(L_15);
		int32_t L_16 = ___num0;
		__this->set_komaNum_7(L_16);
		arrangeKoma_canTouchAll_m931637095(__this, /*hidden argument*/NULL);
		goto IL_013b;
	}

IL_013b:
	{
		return;
	}
}
// System.Void arrangeKoma::changeKomaResources(System.Int32)
extern "C"  void arrangeKoma_changeKomaResources_m4200569561 (arrangeKoma_t3423810832 * __this, int32_t ___num0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___num0;
		if (L_0 == 0)
		{
			goto IL_0081;
		}
		if (L_0 == 1)
		{
			goto IL_0075;
		}
		if (L_0 == 2)
		{
			goto IL_0069;
		}
		if (L_0 == 3)
		{
			goto IL_005d;
		}
		if (L_0 == 4)
		{
			goto IL_0051;
		}
		if (L_0 == 5)
		{
			goto IL_0044;
		}
		if (L_0 == 6)
		{
			goto IL_0038;
		}
		if (L_0 == 7)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_0086;
	}

IL_002b:
	{
		arrangeKoma_getResources_m1274042461(__this, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_0038:
	{
		arrangeKoma_getResources_m1274042461(__this, 3, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_0044:
	{
		arrangeKoma_getResources_m1274042461(__this, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_0051:
	{
		arrangeKoma_getResources_m1274042461(__this, 8, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_005d:
	{
		arrangeKoma_getResources_m1274042461(__this, 4, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_0069:
	{
		arrangeKoma_getResources_m1274042461(__this, 5, /*hidden argument*/NULL);
		goto IL_0086;
	}

IL_0075:
	{
		__this->set_human_4((bool)0);
		goto IL_0086;
	}

IL_0081:
	{
		goto IL_0086;
	}

IL_0086:
	{
		return;
	}
}
// System.Void arrangeKoma::countResources(System.Int32)
extern Il2CppClass* arrangeKoma_t3423810832_il2cpp_TypeInfo_var;
extern const uint32_t arrangeKoma_countResources_m2933092726_MetadataUsageId;
extern "C"  void arrangeKoma_countResources_m2933092726 (arrangeKoma_t3423810832 * __this, int32_t ___num0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_countResources_m2933092726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___num0;
		if (L_0 == 0)
		{
			goto IL_0140;
		}
		if (L_0 == 1)
		{
			goto IL_012d;
		}
		if (L_0 == 2)
		{
			goto IL_0102;
		}
		if (L_0 == 3)
		{
			goto IL_00d7;
		}
		if (L_0 == 4)
		{
			goto IL_00ac;
		}
		if (L_0 == 5)
		{
			goto IL_0081;
		}
		if (L_0 == 6)
		{
			goto IL_0056;
		}
		if (L_0 == 7)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_014c;
	}

IL_002b:
	{
		int32_t L_1 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_myResources_19();
		if ((((int32_t)L_1) <= ((int32_t)8)))
		{
			goto IL_004a;
		}
	}
	{
		arrangeKoma_getResources_m1274042461(__this, ((int32_t)-9), /*hidden argument*/NULL);
		__this->set_resources_5((bool)1);
		goto IL_0051;
	}

IL_004a:
	{
		__this->set_resources_5((bool)0);
	}

IL_0051:
	{
		goto IL_014c;
	}

IL_0056:
	{
		int32_t L_2 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_myResources_19();
		if ((((int32_t)L_2) <= ((int32_t)2)))
		{
			goto IL_0075;
		}
	}
	{
		arrangeKoma_getResources_m1274042461(__this, ((int32_t)-3), /*hidden argument*/NULL);
		__this->set_resources_5((bool)1);
		goto IL_007c;
	}

IL_0075:
	{
		__this->set_resources_5((bool)0);
	}

IL_007c:
	{
		goto IL_014c;
	}

IL_0081:
	{
		int32_t L_3 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_myResources_19();
		if ((((int32_t)L_3) <= ((int32_t)8)))
		{
			goto IL_00a0;
		}
	}
	{
		arrangeKoma_getResources_m1274042461(__this, ((int32_t)-9), /*hidden argument*/NULL);
		__this->set_resources_5((bool)1);
		goto IL_00a7;
	}

IL_00a0:
	{
		__this->set_resources_5((bool)0);
	}

IL_00a7:
	{
		goto IL_014c;
	}

IL_00ac:
	{
		int32_t L_4 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_myResources_19();
		if ((((int32_t)L_4) <= ((int32_t)7)))
		{
			goto IL_00cb;
		}
	}
	{
		arrangeKoma_getResources_m1274042461(__this, ((int32_t)-8), /*hidden argument*/NULL);
		__this->set_resources_5((bool)1);
		goto IL_00d2;
	}

IL_00cb:
	{
		__this->set_resources_5((bool)0);
	}

IL_00d2:
	{
		goto IL_014c;
	}

IL_00d7:
	{
		int32_t L_5 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_myResources_19();
		if ((((int32_t)L_5) <= ((int32_t)3)))
		{
			goto IL_00f6;
		}
	}
	{
		arrangeKoma_getResources_m1274042461(__this, ((int32_t)-4), /*hidden argument*/NULL);
		__this->set_resources_5((bool)1);
		goto IL_00fd;
	}

IL_00f6:
	{
		__this->set_resources_5((bool)0);
	}

IL_00fd:
	{
		goto IL_014c;
	}

IL_0102:
	{
		int32_t L_6 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_myResources_19();
		if ((((int32_t)L_6) <= ((int32_t)4)))
		{
			goto IL_0121;
		}
	}
	{
		arrangeKoma_getResources_m1274042461(__this, ((int32_t)-5), /*hidden argument*/NULL);
		__this->set_resources_5((bool)1);
		goto IL_0128;
	}

IL_0121:
	{
		__this->set_resources_5((bool)0);
	}

IL_0128:
	{
		goto IL_014c;
	}

IL_012d:
	{
		__this->set_human_4((bool)1);
		__this->set_resources_5((bool)1);
		goto IL_014c;
	}

IL_0140:
	{
		__this->set_resources_5((bool)1);
		goto IL_014c;
	}

IL_014c:
	{
		return;
	}
}
// System.Void arrangeKoma::lineRow(System.Int32,System.Int32)
extern "C"  void arrangeKoma_lineRow_m1340912179 (arrangeKoma_t3423810832 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___l0;
		__this->set_line_10(L_0);
		int32_t L_1 = ___r1;
		__this->set_row_11(L_1);
		return;
	}
}
// System.Void arrangeKoma::canNotTouchAll()
extern "C"  void arrangeKoma_canNotTouchAll_m2596981714 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_002b;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_0020;
	}

IL_000e:
	{
		BooleanU5BU2CU5D_t3568034316* L_0 = __this->get_canTouchPlace_3();
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_0);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_0)->SetAt(L_1, L_2, (bool)0);
		int32_t L_3 = V_1;
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) < ((int32_t)6)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)2)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void arrangeKoma::canTouchAll()
extern "C"  void arrangeKoma_canTouchAll_m931637095 (arrangeKoma_t3423810832 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_002b;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_0020;
	}

IL_000e:
	{
		BooleanU5BU2CU5D_t3568034316* L_0 = __this->get_canTouchPlace_3();
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_0);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_0)->SetAt(L_1, L_2, (bool)1);
		int32_t L_3 = V_1;
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) < ((int32_t)6)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)2)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void arrangeKoma::getResources(System.Int32)
extern Il2CppClass* arrangeKoma_t3423810832_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral956797836;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t arrangeKoma_getResources_m1274042461_MetadataUsageId;
extern "C"  void arrangeKoma_getResources_m1274042461 (arrangeKoma_t3423810832 * __this, int32_t ___resource0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (arrangeKoma_getResources_m1274042461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral956797836, /*hidden argument*/NULL);
		NullCheck(L_0);
		Text_t356221433 * L_1 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_9(L_1);
		int32_t L_2 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_myResources_19();
		int32_t L_3 = ___resource0;
		((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->set_myResources_19(((int32_t)((int32_t)L_2+(int32_t)L_3)));
		Text_t356221433 * L_4 = __this->get_text_9();
		int32_t L_5 = ((arrangeKoma_t3423810832_StaticFields*)arrangeKoma_t3423810832_il2cpp_TypeInfo_var->static_fields)->get_myResources_19();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral372029310, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_8);
		return;
	}
}
// System.Void banmen::.ctor()
extern "C"  void banmen__ctor_m372770190 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void banmen::Start()
extern "C"  void banmen_Start_m342229766 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		banmen_Init_m453326946(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void banmen::Init()
extern Il2CppClass* baseMethods_t2705876163_il2cpp_TypeInfo_var;
extern Il2CppClass* KomaU5BU2CU5D_t1205703982_il2cpp_TypeInfo_var;
extern Il2CppClass* BooleanU5BU2CU5D_t3568034316_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var;
extern Il2CppClass* SliderU5BU2CU5D_t1144817635_il2cpp_TypeInfo_var;
extern Il2CppClass* Empty_t4012560223_il2cpp_TypeInfo_var;
extern Il2CppClass* Human_t1156088493_il2cpp_TypeInfo_var;
extern Il2CppClass* GRobot_t456225233_il2cpp_TypeInfo_var;
extern Il2CppClass* SRobot_t456246757_il2cpp_TypeInfo_var;
extern Il2CppClass* Missile_t813944928_il2cpp_TypeInfo_var;
extern Il2CppClass* Factory_t931158954_il2cpp_TypeInfo_var;
extern Il2CppClass* Drone_t860454962_il2cpp_TypeInfo_var;
extern Il2CppClass* Skytree_t850646749_il2cpp_TypeInfo_var;
extern Il2CppClass* EHuman_t715499684_il2cpp_TypeInfo_var;
extern Il2CppClass* EGRobot_t1059295608_il2cpp_TypeInfo_var;
extern Il2CppClass* ESRobot_t2546702684_il2cpp_TypeInfo_var;
extern Il2CppClass* EMissile_t3534618099_il2cpp_TypeInfo_var;
extern Il2CppClass* EFactory_t2050856767_il2cpp_TypeInfo_var;
extern Il2CppClass* EDrone_t2061607325_il2cpp_TypeInfo_var;
extern Il2CppClass* ESkytree_t2245705948_il2cpp_TypeInfo_var;
extern Il2CppClass* Futi_t612262014_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSlider_t297367283_m33033319_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3901280377;
extern Il2CppCodeGenString* _stringLiteral2496419060;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern Il2CppCodeGenString* _stringLiteral2347644584;
extern const uint32_t banmen_Init_m453326946_MetadataUsageId;
extern "C"  void banmen_Init_m453326946 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (banmen_Init_m453326946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		__this->set_halfOfKoma_22(8);
		__this->set_arrangeMode_13((bool)1);
		baseMethods_t2705876163 * L_0 = (baseMethods_t2705876163 *)il2cpp_codegen_object_new(baseMethods_t2705876163_il2cpp_TypeInfo_var);
		baseMethods__ctor_m1615982932(L_0, /*hidden argument*/NULL);
		__this->set_baseM_2(L_0);
		baseMethods_t2705876163 * L_1 = __this->get_baseM_2();
		NullCheck(L_1);
		baseMethods_Init_m3651539526(L_1, _stringLiteral3901280377, /*hidden argument*/NULL);
		banmen_reset_m2092301357(__this, /*hidden argument*/NULL);
		il2cpp_array_size_t L_3[] = { (il2cpp_array_size_t)8, (il2cpp_array_size_t)8 };
		KomaU5BU2CU5D_t1205703982* L_2 = (KomaU5BU2CU5D_t1205703982*)GenArrayNew(KomaU5BU2CU5D_t1205703982_il2cpp_TypeInfo_var, L_3);
		__this->set_Board_3((KomaU5BU2CU5D_t1205703982*)L_2);
		il2cpp_array_size_t L_5[] = { (il2cpp_array_size_t)8, (il2cpp_array_size_t)8 };
		BooleanU5BU2CU5D_t3568034316* L_4 = (BooleanU5BU2CU5D_t3568034316*)GenArrayNew(BooleanU5BU2CU5D_t3568034316_il2cpp_TypeInfo_var, L_5);
		__this->set_canTouchPlace_4((BooleanU5BU2CU5D_t3568034316*)L_4);
		__this->set_humanExist_16((bool)0);
		il2cpp_array_size_t L_7[] = { (il2cpp_array_size_t)6, (il2cpp_array_size_t)6 };
		BooleanU5BU2CU5D_t3568034316* L_6 = (BooleanU5BU2CU5D_t3568034316*)GenArrayNew(BooleanU5BU2CU5D_t3568034316_il2cpp_TypeInfo_var, L_7);
		__this->set_broken_5((BooleanU5BU2CU5D_t3568034316*)L_6);
		V_0 = 0;
		goto IL_0088;
	}

IL_0064:
	{
		V_1 = 0;
		goto IL_007d;
	}

IL_006b:
	{
		BooleanU5BU2CU5D_t3568034316* L_8 = __this->get_broken_5();
		int32_t L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_8);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_8)->SetAt(L_9, L_10, (bool)1);
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_007d:
	{
		int32_t L_12 = V_1;
		if ((((int32_t)L_12) < ((int32_t)6)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0088:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) < ((int32_t)6)))
		{
			goto IL_0064;
		}
	}
	{
		il2cpp_array_size_t L_16[] = { (il2cpp_array_size_t)2, (il2cpp_array_size_t)2 };
		Int32U5BU2CU5D_t3030399642* L_15 = (Int32U5BU2CU5D_t3030399642*)GenArrayNew(Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var, L_16);
		__this->set_barrier_6((Int32U5BU2CU5D_t3030399642*)L_15);
		il2cpp_array_size_t L_18[] = { (il2cpp_array_size_t)2, (il2cpp_array_size_t)2 };
		Int32U5BU2CU5D_t3030399642* L_17 = (Int32U5BU2CU5D_t3030399642*)GenArrayNew(Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var, L_18);
		__this->set_factoryLocation_7((Int32U5BU2CU5D_t3030399642*)L_17);
		il2cpp_array_size_t L_20[] = { (il2cpp_array_size_t)2, (il2cpp_array_size_t)2 };
		Int32U5BU2CU5D_t3030399642* L_19 = (Int32U5BU2CU5D_t3030399642*)GenArrayNew(Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var, L_20);
		__this->set_humanPosition_8((Int32U5BU2CU5D_t3030399642*)L_19);
		V_2 = 0;
		goto IL_00fd;
	}

IL_00bd:
	{
		V_3 = 0;
		goto IL_00f2;
	}

IL_00c4:
	{
		Int32U5BU2CU5D_t3030399642* L_21 = __this->get_barrier_6();
		int32_t L_22 = V_2;
		int32_t L_23 = V_3;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_21);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_21)->SetAt(L_22, L_23, (-1));
		Int32U5BU2CU5D_t3030399642* L_24 = __this->get_factoryLocation_7();
		int32_t L_25 = V_2;
		int32_t L_26 = V_3;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_24);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_24)->SetAt(L_25, L_26, (-1));
		Int32U5BU2CU5D_t3030399642* L_27 = __this->get_humanPosition_8();
		int32_t L_28 = V_2;
		int32_t L_29 = V_3;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_27);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_27)->SetAt(L_28, L_29, (-1));
		int32_t L_30 = V_3;
		V_3 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00f2:
	{
		int32_t L_31 = V_3;
		if ((((int32_t)L_31) < ((int32_t)2)))
		{
			goto IL_00c4;
		}
	}
	{
		int32_t L_32 = V_2;
		V_2 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00fd:
	{
		int32_t L_33 = V_2;
		if ((((int32_t)L_33) < ((int32_t)2)))
		{
			goto IL_00bd;
		}
	}
	{
		il2cpp_array_size_t L_35[] = { (il2cpp_array_size_t)6, (il2cpp_array_size_t)6 };
		Int32U5BU2CU5D_t3030399642* L_34 = (Int32U5BU2CU5D_t3030399642*)GenArrayNew(Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var, L_35);
		__this->set_komaTime_9((Int32U5BU2CU5D_t3030399642*)L_34);
		il2cpp_array_size_t L_37[] = { (il2cpp_array_size_t)6, (il2cpp_array_size_t)6 };
		SliderU5BU2CU5D_t1144817635* L_36 = (SliderU5BU2CU5D_t1144817635*)GenArrayNew(SliderU5BU2CU5D_t1144817635_il2cpp_TypeInfo_var, L_37);
		__this->set_komaSlider_10((SliderU5BU2CU5D_t1144817635*)L_36);
		il2cpp_array_size_t L_39[] = { (il2cpp_array_size_t)6, (il2cpp_array_size_t)6 };
		Int32U5BU2CU5D_t3030399642* L_38 = (Int32U5BU2CU5D_t3030399642*)GenArrayNew(Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var, L_39);
		__this->set_EkomaTime_11((Int32U5BU2CU5D_t3030399642*)L_38);
		il2cpp_array_size_t L_41[] = { (il2cpp_array_size_t)6, (il2cpp_array_size_t)6 };
		SliderU5BU2CU5D_t1144817635* L_40 = (SliderU5BU2CU5D_t1144817635*)GenArrayNew(SliderU5BU2CU5D_t1144817635_il2cpp_TypeInfo_var, L_41);
		__this->set_EkomaSlider_12((SliderU5BU2CU5D_t1144817635*)L_40);
		Empty_t4012560223 * L_42 = (Empty_t4012560223 *)il2cpp_codegen_object_new(Empty_t4012560223_il2cpp_TypeInfo_var);
		Empty__ctor_m4179857912(L_42, /*hidden argument*/NULL);
		__this->set_empty_41(L_42);
		Human_t1156088493 * L_43 = (Human_t1156088493 *)il2cpp_codegen_object_new(Human_t1156088493_il2cpp_TypeInfo_var);
		Human__ctor_m4119456528(L_43, /*hidden argument*/NULL);
		__this->set_human_42(L_43);
		GRobot_t456225233 * L_44 = (GRobot_t456225233 *)il2cpp_codegen_object_new(GRobot_t456225233_il2cpp_TypeInfo_var);
		GRobot__ctor_m1963847546(L_44, /*hidden argument*/NULL);
		__this->set_gRobot_43(L_44);
		SRobot_t456246757 * L_45 = (SRobot_t456246757 *)il2cpp_codegen_object_new(SRobot_t456246757_il2cpp_TypeInfo_var);
		SRobot__ctor_m1465433094(L_45, /*hidden argument*/NULL);
		__this->set_sRobot_44(L_45);
		Missile_t813944928 * L_46 = (Missile_t813944928 *)il2cpp_codegen_object_new(Missile_t813944928_il2cpp_TypeInfo_var);
		Missile__ctor_m2545921555(L_46, /*hidden argument*/NULL);
		__this->set_missile_45(L_46);
		Factory_t931158954 * L_47 = (Factory_t931158954 *)il2cpp_codegen_object_new(Factory_t931158954_il2cpp_TypeInfo_var);
		Factory__ctor_m2197654087(L_47, /*hidden argument*/NULL);
		__this->set_factory_46(L_47);
		Drone_t860454962 * L_48 = (Drone_t860454962 *)il2cpp_codegen_object_new(Drone_t860454962_il2cpp_TypeInfo_var);
		Drone__ctor_m960290629(L_48, /*hidden argument*/NULL);
		__this->set_drone_47(L_48);
		Skytree_t850646749 * L_49 = (Skytree_t850646749 *)il2cpp_codegen_object_new(Skytree_t850646749_il2cpp_TypeInfo_var);
		Skytree__ctor_m1984419468(L_49, /*hidden argument*/NULL);
		__this->set_skytree_48(L_49);
		EHuman_t715499684 * L_50 = (EHuman_t715499684 *)il2cpp_codegen_object_new(EHuman_t715499684_il2cpp_TypeInfo_var);
		EHuman__ctor_m516293241(L_50, /*hidden argument*/NULL);
		__this->set_Ehuman_49(L_50);
		EGRobot_t1059295608 * L_51 = (EGRobot_t1059295608 *)il2cpp_codegen_object_new(EGRobot_t1059295608_il2cpp_TypeInfo_var);
		EGRobot__ctor_m4001745305(L_51, /*hidden argument*/NULL);
		__this->set_EgRobot_50(L_51);
		ESRobot_t2546702684 * L_52 = (ESRobot_t2546702684 *)il2cpp_codegen_object_new(ESRobot_t2546702684_il2cpp_TypeInfo_var);
		ESRobot__ctor_m4281893077(L_52, /*hidden argument*/NULL);
		__this->set_EsRobot_51(L_52);
		EMissile_t3534618099 * L_53 = (EMissile_t3534618099 *)il2cpp_codegen_object_new(EMissile_t3534618099_il2cpp_TypeInfo_var);
		EMissile__ctor_m3541219220(L_53, /*hidden argument*/NULL);
		__this->set_Emissile_52(L_53);
		EFactory_t2050856767 * L_54 = (EFactory_t2050856767 *)il2cpp_codegen_object_new(EFactory_t2050856767_il2cpp_TypeInfo_var);
		EFactory__ctor_m2230763454(L_54, /*hidden argument*/NULL);
		__this->set_Efactory_53(L_54);
		EDrone_t2061607325 * L_55 = (EDrone_t2061607325 *)il2cpp_codegen_object_new(EDrone_t2061607325_il2cpp_TypeInfo_var);
		EDrone__ctor_m3222050530(L_55, /*hidden argument*/NULL);
		__this->set_Edrone_54(L_55);
		ESkytree_t2245705948 * L_56 = (ESkytree_t2245705948 *)il2cpp_codegen_object_new(ESkytree_t2245705948_il2cpp_TypeInfo_var);
		ESkytree__ctor_m3421849381(L_56, /*hidden argument*/NULL);
		__this->set_Eskytree_55(L_56);
		Futi_t612262014 * L_57 = (Futi_t612262014 *)il2cpp_codegen_object_new(Futi_t612262014_il2cpp_TypeInfo_var);
		Futi__ctor_m1883418413(L_57, /*hidden argument*/NULL);
		__this->set_futi_56(L_57);
		Empty_t4012560223 * L_58 = __this->get_empty_41();
		NullCheck(L_58);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_58, _stringLiteral3901280377);
		Human_t1156088493 * L_59 = __this->get_human_42();
		NullCheck(L_59);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_59, _stringLiteral3901280377);
		GRobot_t456225233 * L_60 = __this->get_gRobot_43();
		NullCheck(L_60);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_60, _stringLiteral3901280377);
		SRobot_t456246757 * L_61 = __this->get_sRobot_44();
		NullCheck(L_61);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_61, _stringLiteral3901280377);
		Missile_t813944928 * L_62 = __this->get_missile_45();
		NullCheck(L_62);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_62, _stringLiteral3901280377);
		Factory_t931158954 * L_63 = __this->get_factory_46();
		NullCheck(L_63);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_63, _stringLiteral3901280377);
		Drone_t860454962 * L_64 = __this->get_drone_47();
		NullCheck(L_64);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_64, _stringLiteral3901280377);
		Skytree_t850646749 * L_65 = __this->get_skytree_48();
		NullCheck(L_65);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_65, _stringLiteral3901280377);
		EHuman_t715499684 * L_66 = __this->get_Ehuman_49();
		NullCheck(L_66);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_66, _stringLiteral3901280377);
		EGRobot_t1059295608 * L_67 = __this->get_EgRobot_50();
		NullCheck(L_67);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_67, _stringLiteral3901280377);
		ESRobot_t2546702684 * L_68 = __this->get_EsRobot_51();
		NullCheck(L_68);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_68, _stringLiteral3901280377);
		EMissile_t3534618099 * L_69 = __this->get_Emissile_52();
		NullCheck(L_69);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_69, _stringLiteral3901280377);
		EFactory_t2050856767 * L_70 = __this->get_Efactory_53();
		NullCheck(L_70);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_70, _stringLiteral3901280377);
		EDrone_t2061607325 * L_71 = __this->get_Edrone_54();
		NullCheck(L_71);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_71, _stringLiteral3901280377);
		ESkytree_t2245705948 * L_72 = __this->get_Eskytree_55();
		NullCheck(L_72);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void Koma::Init(System.String) */, L_72, _stringLiteral3901280377);
		baseMethods_t2705876163 * L_73 = __this->get_baseM_2();
		NullCheck(L_73);
		baseMethods_canTouchAll_m280133149(L_73, (bool)1, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_74 = __this->get_baseM_2();
		NullCheck(L_74);
		baseMethods_canTouchArrangeMode_m1634389688(L_74, /*hidden argument*/NULL);
		__this->set_wall_32((bool)1);
		__this->set_enemy_31((bool)0);
		__this->set_satelite_33((bool)1);
		__this->set_Esatelite_34((bool)1);
		__this->set_winLose_17((bool)0);
		__this->set_myResources_39(((int32_t)20));
		__this->set_enemyResources_40(((int32_t)20));
		baseMethods_t2705876163 * L_75 = __this->get_baseM_2();
		NullCheck(L_75);
		baseMethods_getMyResources_m1880650416(L_75, 0, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_76 = __this->get_baseM_2();
		NullCheck(L_76);
		baseMethods_getEResources_m992289761(L_76, 0, /*hidden argument*/NULL);
		__this->set_myFactoryTime_37(((int32_t)101));
		__this->set_enemyFactoryTime_38(((int32_t)101));
		__this->set_kariKomaNum_36((-1));
		Empty_t4012560223 * L_77 = __this->get_empty_41();
		__this->set_kariKoma_35(L_77);
		V_4 = 0;
		goto IL_03bb;
	}

IL_0365:
	{
		KomaU5BU2CU5D_t1205703982* L_78 = __this->get_Board_3();
		int32_t L_79 = V_4;
		Futi_t612262014 * L_80 = __this->get_futi_56();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_78);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_78)->SetAt(L_79, 0, L_80);
		KomaU5BU2CU5D_t1205703982* L_81 = __this->get_Board_3();
		int32_t L_82 = V_4;
		Futi_t612262014 * L_83 = __this->get_futi_56();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_81);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_81)->SetAt(L_82, 7, L_83);
		KomaU5BU2CU5D_t1205703982* L_84 = __this->get_Board_3();
		int32_t L_85 = V_4;
		Futi_t612262014 * L_86 = __this->get_futi_56();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_84);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_84)->SetAt(0, L_85, L_86);
		KomaU5BU2CU5D_t1205703982* L_87 = __this->get_Board_3();
		int32_t L_88 = V_4;
		Futi_t612262014 * L_89 = __this->get_futi_56();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_87);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_87)->SetAt(7, L_88, L_89);
		int32_t L_90 = V_4;
		V_4 = ((int32_t)((int32_t)L_90+(int32_t)1));
	}

IL_03bb:
	{
		int32_t L_91 = V_4;
		if ((((int32_t)L_91) < ((int32_t)8)))
		{
			goto IL_0365;
		}
	}
	{
		V_5 = 0;
		goto IL_0400;
	}

IL_03cb:
	{
		V_6 = 0;
		goto IL_03f2;
	}

IL_03d3:
	{
		KomaU5BU2CU5D_t1205703982* L_92 = __this->get_Board_3();
		int32_t L_93 = V_5;
		int32_t L_94 = V_6;
		Empty_t4012560223 * L_95 = __this->get_empty_41();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_92);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_92)->SetAt(((int32_t)((int32_t)L_93+(int32_t)1)), ((int32_t)((int32_t)L_94+(int32_t)1)), L_95);
		int32_t L_96 = V_6;
		V_6 = ((int32_t)((int32_t)L_96+(int32_t)1));
	}

IL_03f2:
	{
		int32_t L_97 = V_6;
		if ((((int32_t)L_97) < ((int32_t)6)))
		{
			goto IL_03d3;
		}
	}
	{
		int32_t L_98 = V_5;
		V_5 = ((int32_t)((int32_t)L_98+(int32_t)1));
	}

IL_0400:
	{
		int32_t L_99 = V_5;
		if ((((int32_t)L_99) < ((int32_t)6)))
		{
			goto IL_03cb;
		}
	}
	{
		KomaU5BU2CU5D_t1205703982* L_100 = __this->get_Board_3();
		EHuman_t715499684 * L_101 = __this->get_Ehuman_49();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_100);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_100)->SetAt(2, 2, L_101);
		KomaU5BU2CU5D_t1205703982* L_102 = __this->get_Board_3();
		EDrone_t2061607325 * L_103 = __this->get_Edrone_54();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_102);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_102)->SetAt(1, 1, L_103);
		KomaU5BU2CU5D_t1205703982* L_104 = __this->get_Board_3();
		EGRobot_t1059295608 * L_105 = __this->get_EgRobot_50();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_104);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_104)->SetAt(6, 1, L_105);
		KomaU5BU2CU5D_t1205703982* L_106 = __this->get_Board_3();
		ESRobot_t2546702684 * L_107 = __this->get_EsRobot_51();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_106);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_106)->SetAt(5, 1, L_107);
		KomaU5BU2CU5D_t1205703982* L_108 = __this->get_Board_3();
		EMissile_t3534618099 * L_109 = __this->get_Emissile_52();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_108);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_108)->SetAt(4, 1, L_109);
		KomaU5BU2CU5D_t1205703982* L_110 = __this->get_Board_3();
		ESkytree_t2245705948 * L_111 = __this->get_Eskytree_55();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_110);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_110)->SetAt(3, 1, L_111);
		KomaU5BU2CU5D_t1205703982* L_112 = __this->get_Board_3();
		EFactory_t2050856767 * L_113 = __this->get_Efactory_53();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_112);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_112)->SetAt(2, 1, L_113);
		KomaU5BU2CU5D_t1205703982* L_114 = __this->get_Board_3();
		EGRobot_t1059295608 * L_115 = __this->get_EgRobot_50();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_114);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_114)->SetAt(1, 2, L_115);
		V_7 = 0;
		goto IL_05dc;
	}

IL_04a8:
	{
		V_8 = 0;
		goto IL_05ce;
	}

IL_04b0:
	{
		Int32U5BU2CU5D_t3030399642* L_116 = __this->get_komaTime_9();
		int32_t L_117 = V_7;
		int32_t L_118 = V_8;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_116);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_116)->SetAt(L_117, L_118, 0);
		SliderU5BU2CU5D_t1144817635* L_119 = __this->get_komaSlider_10();
		int32_t L_120 = V_7;
		int32_t L_121 = V_8;
		ObjectU5BU5D_t3614634134* L_122 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_122);
		ArrayElementTypeCheck (L_122, _stringLiteral2496419060);
		(L_122)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2496419060);
		ObjectU5BU5D_t3614634134* L_123 = L_122;
		int32_t L_124 = V_7;
		int32_t L_125 = ((int32_t)((int32_t)L_124+(int32_t)1));
		Il2CppObject * L_126 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_125);
		NullCheck(L_123);
		ArrayElementTypeCheck (L_123, L_126);
		(L_123)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_126);
		ObjectU5BU5D_t3614634134* L_127 = L_123;
		NullCheck(L_127);
		ArrayElementTypeCheck (L_127, _stringLiteral372029313);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_128 = L_127;
		int32_t L_129 = V_8;
		int32_t L_130 = ((int32_t)((int32_t)L_129+(int32_t)1));
		Il2CppObject * L_131 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_130);
		NullCheck(L_128);
		ArrayElementTypeCheck (L_128, L_131);
		(L_128)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_131);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_132 = String_Concat_m3881798623(NULL /*static, unused*/, L_128, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_133 = GameObject_Find_m836511350(NULL /*static, unused*/, L_132, /*hidden argument*/NULL);
		NullCheck(L_133);
		Slider_t297367283 * L_134 = GameObject_GetComponent_TisSlider_t297367283_m33033319(L_133, /*hidden argument*/GameObject_GetComponent_TisSlider_t297367283_m33033319_MethodInfo_var);
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_119);
		((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_119)->SetAt(L_120, L_121, L_134);
		SliderU5BU2CU5D_t1144817635* L_135 = __this->get_komaSlider_10();
		int32_t L_136 = V_7;
		int32_t L_137 = V_8;
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_135);
		Slider_t297367283 * L_138 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_135)->GetAt(L_136, L_137);
		NullCheck(L_138);
		Slider_set_maxValue_m2951480075(L_138, (60.0f), /*hidden argument*/NULL);
		Int32U5BU2CU5D_t3030399642* L_139 = __this->get_EkomaTime_11();
		int32_t L_140 = V_7;
		int32_t L_141 = V_8;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_139);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_139)->SetAt(L_140, L_141, 0);
		SliderU5BU2CU5D_t1144817635* L_142 = __this->get_EkomaSlider_12();
		int32_t L_143 = V_7;
		int32_t L_144 = V_8;
		ObjectU5BU5D_t3614634134* L_145 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_145);
		ArrayElementTypeCheck (L_145, _stringLiteral2496419060);
		(L_145)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2496419060);
		ObjectU5BU5D_t3614634134* L_146 = L_145;
		int32_t L_147 = V_7;
		int32_t L_148 = ((int32_t)((int32_t)L_147+(int32_t)1));
		Il2CppObject * L_149 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_148);
		NullCheck(L_146);
		ArrayElementTypeCheck (L_146, L_149);
		(L_146)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_149);
		ObjectU5BU5D_t3614634134* L_150 = L_146;
		NullCheck(L_150);
		ArrayElementTypeCheck (L_150, _stringLiteral372029313);
		(L_150)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_151 = L_150;
		int32_t L_152 = V_8;
		int32_t L_153 = ((int32_t)((int32_t)L_152+(int32_t)1));
		Il2CppObject * L_154 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_153);
		NullCheck(L_151);
		ArrayElementTypeCheck (L_151, L_154);
		(L_151)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_154);
		ObjectU5BU5D_t3614634134* L_155 = L_151;
		NullCheck(L_155);
		ArrayElementTypeCheck (L_155, _stringLiteral2347644584);
		(L_155)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2347644584);
		String_t* L_156 = String_Concat_m3881798623(NULL /*static, unused*/, L_155, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_157 = GameObject_Find_m836511350(NULL /*static, unused*/, L_156, /*hidden argument*/NULL);
		NullCheck(L_157);
		Slider_t297367283 * L_158 = GameObject_GetComponent_TisSlider_t297367283_m33033319(L_157, /*hidden argument*/GameObject_GetComponent_TisSlider_t297367283_m33033319_MethodInfo_var);
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_142);
		((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_142)->SetAt(L_143, L_144, L_158);
		SliderU5BU2CU5D_t1144817635* L_159 = __this->get_EkomaSlider_12();
		int32_t L_160 = V_7;
		int32_t L_161 = V_8;
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_159);
		Slider_t297367283 * L_162 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_159)->GetAt(L_160, L_161);
		NullCheck(L_162);
		Slider_set_maxValue_m2951480075(L_162, (60.0f), /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_163 = __this->get_baseM_2();
		int32_t L_164 = V_7;
		int32_t L_165 = V_8;
		NullCheck(L_163);
		baseMethods_OffTimeBar_m3958558077(L_163, ((int32_t)((int32_t)L_164+(int32_t)1)), ((int32_t)((int32_t)L_165+(int32_t)1)), /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_166 = __this->get_baseM_2();
		int32_t L_167 = V_7;
		int32_t L_168 = V_8;
		NullCheck(L_166);
		baseMethods_OffTimeBar2_m299894443(L_166, ((int32_t)((int32_t)L_167+(int32_t)1)), ((int32_t)((int32_t)L_168+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_169 = V_8;
		V_8 = ((int32_t)((int32_t)L_169+(int32_t)1));
	}

IL_05ce:
	{
		int32_t L_170 = V_8;
		if ((((int32_t)L_170) < ((int32_t)6)))
		{
			goto IL_04b0;
		}
	}
	{
		int32_t L_171 = V_7;
		V_7 = ((int32_t)((int32_t)L_171+(int32_t)1));
	}

IL_05dc:
	{
		int32_t L_172 = V_7;
		if ((((int32_t)L_172) < ((int32_t)6)))
		{
			goto IL_04a8;
		}
	}
	{
		return;
	}
}
// System.Void banmen::Update()
extern "C"  void banmen_Update_m2680782557 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_00cb;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_00c0;
	}

IL_000e:
	{
		Int32U5BU2CU5D_t3030399642* L_0 = __this->get_komaTime_9();
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0);
		int32_t L_3 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0)->GetAt(L_1, L_2);
		SliderU5BU2CU5D_t1144817635* L_4 = __this->get_komaSlider_10();
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_4);
		Slider_t297367283 * L_7 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_4)->GetAt(L_5, L_6);
		NullCheck(L_7);
		float L_8 = Slider_get_maxValue_m3319962262(L_7, /*hidden argument*/NULL);
		if ((!(((float)(((float)((float)L_3)))) < ((float)L_8))))
		{
			goto IL_0065;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_9 = __this->get_komaTime_9();
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_9);
		int32_t* L_12 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_9)->GetAddressAt(L_10, L_11);
		int32_t* L_13 = L_12;
		*((int32_t*)(L_13)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_13))+(int32_t)1));
		SliderU5BU2CU5D_t1144817635* L_14 = __this->get_komaSlider_10();
		int32_t L_15 = V_0;
		int32_t L_16 = V_1;
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_14);
		Slider_t297367283 * L_17 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_14)->GetAt(L_15, L_16);
		Int32U5BU2CU5D_t3030399642* L_18 = __this->get_komaTime_9();
		int32_t L_19 = V_0;
		int32_t L_20 = V_1;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_18);
		int32_t L_21 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_18)->GetAt(L_19, L_20);
		NullCheck(L_17);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_17, (((float)((float)L_21))));
	}

IL_0065:
	{
		Int32U5BU2CU5D_t3030399642* L_22 = __this->get_EkomaTime_11();
		int32_t L_23 = V_0;
		int32_t L_24 = V_1;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_22);
		int32_t L_25 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_22)->GetAt(L_23, L_24);
		SliderU5BU2CU5D_t1144817635* L_26 = __this->get_EkomaSlider_12();
		int32_t L_27 = V_0;
		int32_t L_28 = V_1;
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_26);
		Slider_t297367283 * L_29 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_26)->GetAt(L_27, L_28);
		NullCheck(L_29);
		float L_30 = Slider_get_maxValue_m3319962262(L_29, /*hidden argument*/NULL);
		if ((!(((float)(((float)((float)L_25)))) < ((float)L_30))))
		{
			goto IL_00bc;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_31 = __this->get_EkomaTime_11();
		int32_t L_32 = V_0;
		int32_t L_33 = V_1;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_31);
		int32_t* L_34 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_31)->GetAddressAt(L_32, L_33);
		int32_t* L_35 = L_34;
		*((int32_t*)(L_35)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_35))+(int32_t)1));
		SliderU5BU2CU5D_t1144817635* L_36 = __this->get_EkomaSlider_12();
		int32_t L_37 = V_0;
		int32_t L_38 = V_1;
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_36);
		Slider_t297367283 * L_39 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_36)->GetAt(L_37, L_38);
		Int32U5BU2CU5D_t3030399642* L_40 = __this->get_EkomaTime_11();
		int32_t L_41 = V_0;
		int32_t L_42 = V_1;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_40);
		int32_t L_43 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_40)->GetAt(L_41, L_42);
		NullCheck(L_39);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_39, (((float)((float)L_43))));
	}

IL_00bc:
	{
		int32_t L_44 = V_1;
		V_1 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_45 = V_1;
		if ((((int32_t)L_45) < ((int32_t)6)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_46 = V_0;
		V_0 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_00cb:
	{
		int32_t L_47 = V_0;
		if ((((int32_t)L_47) < ((int32_t)6)))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_48 = __this->get_myFactoryTime_37();
		if ((((int32_t)L_48) >= ((int32_t)((int32_t)100))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_49 = __this->get_myFactoryTime_37();
		__this->set_myFactoryTime_37(((int32_t)((int32_t)L_49+(int32_t)1)));
		goto IL_0183;
	}

IL_00f2:
	{
		int32_t L_50 = __this->get_myFactoryTime_37();
		if ((!(((uint32_t)L_50) == ((uint32_t)((int32_t)100)))))
		{
			goto IL_0183;
		}
	}
	{
		KomaU5BU2CU5D_t1205703982* L_51 = __this->get_Board_3();
		Int32U5BU2CU5D_t3030399642* L_52 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_52);
		int32_t L_53 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_52)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_54 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_54);
		int32_t L_55 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_54)->GetAt(0, 1);
		Koma_t3011766596 * L_56 = __this->get_kariKoma_35();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_51);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_51)->SetAt(L_53, ((int32_t)((int32_t)L_55-(int32_t)1)), L_56);
		Int32U5BU2CU5D_t3030399642* L_57 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_57);
		int32_t L_58 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_57)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_59 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_59);
		int32_t L_60 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_59)->GetAt(0, 1);
		banmen_paint_m3356220390(__this, L_58, ((int32_t)((int32_t)L_60-(int32_t)1)), /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_61 = __this->get_baseM_2();
		Int32U5BU2CU5D_t3030399642* L_62 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_62);
		int32_t L_63 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_62)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_64 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_64);
		int32_t L_65 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_64)->GetAt(0, 1);
		NullCheck(L_61);
		baseMethods_OnTimeBar_m1542858583(L_61, L_63, ((int32_t)((int32_t)L_65-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_66 = __this->get_myFactoryTime_37();
		__this->set_myFactoryTime_37(((int32_t)((int32_t)L_66+(int32_t)1)));
	}

IL_0183:
	{
		int32_t L_67 = __this->get_enemyFactoryTime_38();
		if ((((int32_t)L_67) >= ((int32_t)((int32_t)100))))
		{
			goto IL_019e;
		}
	}
	{
		int32_t L_68 = __this->get_enemyFactoryTime_38();
		__this->set_enemyFactoryTime_38(((int32_t)((int32_t)L_68+(int32_t)1)));
	}

IL_019e:
	{
		int32_t L_69 = __this->get_enemyFactoryTime_38();
		if ((!(((uint32_t)L_69) == ((uint32_t)((int32_t)100)))))
		{
			goto IL_022f;
		}
	}
	{
		KomaU5BU2CU5D_t1205703982* L_70 = __this->get_Board_3();
		Int32U5BU2CU5D_t3030399642* L_71 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_71);
		int32_t L_72 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_71)->GetAt(1, 0);
		Int32U5BU2CU5D_t3030399642* L_73 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_73);
		int32_t L_74 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_73)->GetAt(1, 1);
		Koma_t3011766596 * L_75 = __this->get_kariKoma_35();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_70);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_70)->SetAt(L_72, ((int32_t)((int32_t)L_74+(int32_t)1)), L_75);
		Int32U5BU2CU5D_t3030399642* L_76 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_76);
		int32_t L_77 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_76)->GetAt(1, 0);
		Int32U5BU2CU5D_t3030399642* L_78 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_78);
		int32_t L_79 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_78)->GetAt(1, 1);
		banmen_paint_m3356220390(__this, L_77, ((int32_t)((int32_t)L_79+(int32_t)1)), /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_80 = __this->get_baseM_2();
		Int32U5BU2CU5D_t3030399642* L_81 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_81);
		int32_t L_82 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_81)->GetAt(1, 0);
		Int32U5BU2CU5D_t3030399642* L_83 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_83);
		int32_t L_84 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_83)->GetAt(1, 1);
		NullCheck(L_80);
		baseMethods_OnTimeBar_m1542858583(L_80, L_82, ((int32_t)((int32_t)L_84+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_85 = __this->get_enemyFactoryTime_38();
		__this->set_enemyFactoryTime_38(((int32_t)((int32_t)L_85+(int32_t)1)));
	}

IL_022f:
	{
		bool L_86 = __this->get_arrangeMode_13();
		if (!L_86)
		{
			goto IL_030f;
		}
	}
	{
		int32_t L_87 = __this->get_bl_23();
		if ((!(((uint32_t)L_87) == ((uint32_t)(-1)))))
		{
			goto IL_0247;
		}
	}
	{
		return;
	}

IL_0247:
	{
		int32_t L_88 = __this->get_kariKomaNum_36();
		Koma_t3011766596 * L_89 = banmen_inputKoma_m3899739766(__this, L_88, /*hidden argument*/NULL);
		Skytree_t850646749 * L_90 = __this->get_skytree_48();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_89) == ((Il2CppObject*)(Skytree_t850646749 *)L_90))))
		{
			goto IL_0289;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_91 = __this->get_barrier_6();
		int32_t L_92 = __this->get_bl_23();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_91);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_91)->SetAt(1, 0, L_92);
		Int32U5BU2CU5D_t3030399642* L_93 = __this->get_barrier_6();
		int32_t L_94 = __this->get_br_24();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_93);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_93)->SetAt(1, 1, L_94);
		goto IL_02c6;
	}

IL_0289:
	{
		int32_t L_95 = __this->get_kariKomaNum_36();
		Koma_t3011766596 * L_96 = banmen_inputKoma_m3899739766(__this, L_95, /*hidden argument*/NULL);
		Factory_t931158954 * L_97 = __this->get_factory_46();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_96) == ((Il2CppObject*)(Factory_t931158954 *)L_97))))
		{
			goto IL_02c6;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_98 = __this->get_factoryLocation_7();
		int32_t L_99 = __this->get_bl_23();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_98);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_98)->SetAt(1, 0, L_99);
		Int32U5BU2CU5D_t3030399642* L_100 = __this->get_factoryLocation_7();
		int32_t L_101 = __this->get_br_24();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_100);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_100)->SetAt(1, 1, L_101);
	}

IL_02c6:
	{
		int32_t L_102 = __this->get_kariKomaNum_36();
		Koma_t3011766596 * L_103 = banmen_inputKoma_m3899739766(__this, ((int32_t)((int32_t)L_102+(int32_t)7)), /*hidden argument*/NULL);
		__this->set_kariKoma_35(L_103);
		KomaU5BU2CU5D_t1205703982* L_104 = __this->get_Board_3();
		int32_t L_105 = __this->get_bl_23();
		int32_t L_106 = __this->get_br_24();
		Koma_t3011766596 * L_107 = __this->get_kariKoma_35();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_104);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_104)->SetAt(L_105, L_106, L_107);
		int32_t L_108 = __this->get_bl_23();
		int32_t L_109 = __this->get_br_24();
		banmen_paint_m3356220390(__this, L_108, L_109, /*hidden argument*/NULL);
		banmen_reset_m2092301357(__this, /*hidden argument*/NULL);
	}

IL_030f:
	{
		return;
	}
}
// System.Void banmen::arrangeOffDo()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t banmen_arrangeOffDo_m4084751936_MetadataUsageId;
extern "C"  void banmen_arrangeOffDo_m4084751936 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (banmen_arrangeOffDo_m4084751936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool L_0 = __this->get_humanExist_16();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_StartRecording_m3582706357(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_arrangeMode_13((bool)0);
		baseMethods_t2705876163 * L_1 = __this->get_baseM_2();
		NullCheck(L_1);
		baseMethods_setColorClear_m4166068504(L_1, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_00ee;
	}

IL_002a:
	{
		V_1 = 0;
		goto IL_00e3;
	}

IL_0031:
	{
		Int32U5BU2CU5D_t3030399642* L_2 = __this->get_komaTime_9();
		int32_t L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_2);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_2)->SetAt(L_3, L_4, 0);
		SliderU5BU2CU5D_t1144817635* L_5 = __this->get_komaSlider_10();
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_5);
		Slider_t297367283 * L_8 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_5)->GetAt(L_6, L_7);
		NullCheck(L_8);
		Slider_set_maxValue_m2951480075(L_8, (60.0f), /*hidden argument*/NULL);
		Int32U5BU2CU5D_t3030399642* L_9 = __this->get_EkomaTime_11();
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_9);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_9)->SetAt(L_10, L_11, 0);
		SliderU5BU2CU5D_t1144817635* L_12 = __this->get_EkomaSlider_12();
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_12);
		Slider_t297367283 * L_15 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_12)->GetAt(L_13, L_14);
		NullCheck(L_15);
		Slider_set_maxValue_m2951480075(L_15, (60.0f), /*hidden argument*/NULL);
		KomaU5BU2CU5D_t1205703982* L_16 = __this->get_Board_3();
		int32_t L_17 = V_0;
		int32_t L_18 = V_1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_16);
		Koma_t3011766596 * L_19 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_16)->GetAt(((int32_t)((int32_t)L_17+(int32_t)1)), ((int32_t)((int32_t)L_18+(int32_t)1)));
		Empty_t4012560223 * L_20 = __this->get_empty_41();
		if ((((Il2CppObject*)(Koma_t3011766596 *)L_19) == ((Il2CppObject*)(Empty_t4012560223 *)L_20)))
		{
			goto IL_00df;
		}
	}
	{
		KomaU5BU2CU5D_t1205703982* L_21 = __this->get_Board_3();
		int32_t L_22 = V_0;
		int32_t L_23 = V_1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_21);
		Koma_t3011766596 * L_24 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_21)->GetAt(((int32_t)((int32_t)L_22+(int32_t)1)), ((int32_t)((int32_t)L_23+(int32_t)1)));
		NullCheck(L_24);
		int32_t L_25 = L_24->get_komaNumber_2();
		int32_t L_26 = __this->get_halfOfKoma_22();
		if ((((int32_t)L_25) >= ((int32_t)L_26)))
		{
			goto IL_00ce;
		}
	}
	{
		baseMethods_t2705876163 * L_27 = __this->get_baseM_2();
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		NullCheck(L_27);
		baseMethods_OnTimeBar_m1542858583(L_27, ((int32_t)((int32_t)L_28+(int32_t)1)), ((int32_t)((int32_t)L_29+(int32_t)1)), /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00ce:
	{
		baseMethods_t2705876163 * L_30 = __this->get_baseM_2();
		int32_t L_31 = V_0;
		int32_t L_32 = V_1;
		NullCheck(L_30);
		baseMethods_OnTimeBar2_m345907837(L_30, ((int32_t)((int32_t)L_31+(int32_t)1)), ((int32_t)((int32_t)L_32+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_00df:
	{
		int32_t L_33 = V_1;
		V_1 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00e3:
	{
		int32_t L_34 = V_1;
		if ((((int32_t)L_34) < ((int32_t)6)))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_35 = V_0;
		V_0 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00ee:
	{
		int32_t L_36 = V_0;
		if ((((int32_t)L_36) < ((int32_t)6)))
		{
			goto IL_002a;
		}
	}
	{
		baseMethods_t2705876163 * L_37 = __this->get_baseM_2();
		NullCheck(L_37);
		baseMethods_setColorWhite_m2690390344(L_37, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_0129;
	}

IL_0107:
	{
		baseMethods_t2705876163 * L_38 = __this->get_baseM_2();
		int32_t L_39 = V_2;
		NullCheck(L_38);
		baseMethods_printColorBottom_m2694251173(L_38, ((int32_t)((int32_t)L_39+(int32_t)1)), 5, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_40 = __this->get_baseM_2();
		int32_t L_41 = V_2;
		NullCheck(L_40);
		baseMethods_printColorBottom_m2694251173(L_40, ((int32_t)((int32_t)L_41+(int32_t)1)), 6, /*hidden argument*/NULL);
		int32_t L_42 = V_2;
		V_2 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_0129:
	{
		int32_t L_43 = V_2;
		if ((((int32_t)L_43) < ((int32_t)6)))
		{
			goto IL_0107;
		}
	}
	{
		banmen_paintEnemyArea_m3798202179(__this, /*hidden argument*/NULL);
		__this->set_kariKomaNum_36((-1));
		baseMethods_t2705876163 * L_44 = __this->get_baseM_2();
		NullCheck(L_44);
		baseMethods_canTouchAll_m280133149(L_44, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void banmen::paintEnemyArea()
extern "C"  void banmen_paintEnemyArea_m3798202179 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_0029;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_001e;
	}

IL_000e:
	{
		int32_t L_0 = V_1;
		int32_t L_1 = V_0;
		banmen_paint_m3356220390(__this, ((int32_t)((int32_t)L_0+(int32_t)1)), ((int32_t)((int32_t)L_1+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		V_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) < ((int32_t)6)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)2)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void banmen::win()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral865308920;
extern Il2CppCodeGenString* _stringLiteral1034273496;
extern Il2CppCodeGenString* _stringLiteral2850094717;
extern Il2CppCodeGenString* _stringLiteral2577045853;
extern const uint32_t banmen_win_m588564420_MetadataUsageId;
extern "C"  void banmen_win_m588564420 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (banmen_win_m588564420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	Text_t356221433 * V_3 = NULL;
	{
		Object_t1021602117 * L_0 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral865308920, /*hidden argument*/NULL);
		V_0 = ((GameObject_t1756533147 *)CastclassSealed(L_0, GameObject_t1756533147_il2cpp_TypeInfo_var));
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1034273496, /*hidden argument*/NULL);
		V_1 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_3 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		V_2 = L_3;
		GameObject_t1756533147 * L_4 = V_2;
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = V_1;
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_SetParent_m1963830867(L_5, L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2850094717, /*hidden argument*/NULL);
		NullCheck(L_8);
		Text_t356221433 * L_9 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_8, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		V_3 = L_9;
		Text_t356221433 * L_10 = V_3;
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, _stringLiteral2577045853);
		return;
	}
}
// System.Void banmen::lose()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral865308920;
extern Il2CppCodeGenString* _stringLiteral1034273496;
extern Il2CppCodeGenString* _stringLiteral2850094717;
extern Il2CppCodeGenString* _stringLiteral1601838694;
extern const uint32_t banmen_lose_m1266767581_MetadataUsageId;
extern "C"  void banmen_lose_m1266767581 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (banmen_lose_m1266767581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	Text_t356221433 * V_3 = NULL;
	{
		Object_t1021602117 * L_0 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral865308920, /*hidden argument*/NULL);
		V_0 = ((GameObject_t1756533147 *)CastclassSealed(L_0, GameObject_t1756533147_il2cpp_TypeInfo_var));
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1034273496, /*hidden argument*/NULL);
		V_1 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_3 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_2, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		V_2 = L_3;
		GameObject_t1756533147 * L_4 = V_2;
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = V_1;
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_SetParent_m1963830867(L_5, L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2850094717, /*hidden argument*/NULL);
		NullCheck(L_8);
		Text_t356221433 * L_9 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_8, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		V_3 = L_9;
		Text_t356221433 * L_10 = V_3;
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, _stringLiteral1601838694);
		return;
	}
}
// System.Void banmen::setKoma()
extern "C"  void banmen_setKoma_m222615202 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_line_20();
		__this->set_bl_23(L_0);
		int32_t L_1 = __this->get_row_21();
		__this->set_br_24(L_1);
		KomaU5BU2CU5D_t1205703982* L_2 = __this->get_Board_3();
		int32_t L_3 = __this->get_line_20();
		int32_t L_4 = __this->get_row_21();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_2);
		Koma_t3011766596 * L_5 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_2)->GetAt(L_3, L_4);
		Empty_t4012560223 * L_6 = __this->get_empty_41();
		if ((((Il2CppObject*)(Koma_t3011766596 *)L_5) == ((Il2CppObject*)(Empty_t4012560223 *)L_6)))
		{
			goto IL_0061;
		}
	}
	{
		baseMethods_t2705876163 * L_7 = __this->get_baseM_2();
		KomaU5BU2CU5D_t1205703982* L_8 = __this->get_Board_3();
		int32_t L_9 = __this->get_line_20();
		int32_t L_10 = __this->get_row_21();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_8);
		Koma_t3011766596 * L_11 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_8)->GetAt(L_9, L_10);
		NullCheck(L_11);
		int32_t L_12 = L_11->get_resources_3();
		NullCheck(L_7);
		baseMethods_getResources_m2257601282(L_7, L_12, /*hidden argument*/NULL);
	}

IL_0061:
	{
		KomaU5BU2CU5D_t1205703982* L_13 = __this->get_Board_3();
		int32_t L_14 = __this->get_line_20();
		int32_t L_15 = __this->get_row_21();
		Koma_t3011766596 * L_16 = __this->get_kariKoma_35();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_13);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_13)->SetAt(L_14, L_15, L_16);
		int32_t L_17 = __this->get_line_20();
		int32_t L_18 = __this->get_row_21();
		banmen_paint_m3356220390(__this, L_17, L_18, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_19 = __this->get_baseM_2();
		Koma_t3011766596 * L_20 = __this->get_kariKoma_35();
		NullCheck(L_20);
		int32_t L_21 = L_20->get_resources_3();
		NullCheck(L_19);
		baseMethods_getResources_m2257601282(L_19, ((-L_21)), /*hidden argument*/NULL);
		Koma_t3011766596 * L_22 = __this->get_kariKoma_35();
		Human_t1156088493 * L_23 = __this->get_human_42();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_22) == ((Il2CppObject*)(Human_t1156088493 *)L_23))))
		{
			goto IL_0143;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_24 = __this->get_humanPosition_8();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_24);
		int32_t L_25 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_24)->GetAt(0, 0);
		if ((((int32_t)L_25) == ((int32_t)(-1))))
		{
			goto IL_0116;
		}
	}
	{
		KomaU5BU2CU5D_t1205703982* L_26 = __this->get_Board_3();
		Int32U5BU2CU5D_t3030399642* L_27 = __this->get_humanPosition_8();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_27);
		int32_t L_28 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_27)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_29 = __this->get_humanPosition_8();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_29);
		int32_t L_30 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_29)->GetAt(0, 1);
		Empty_t4012560223 * L_31 = __this->get_empty_41();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_26);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_26)->SetAt(L_28, L_30, L_31);
		Int32U5BU2CU5D_t3030399642* L_32 = __this->get_humanPosition_8();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_32);
		int32_t L_33 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_32)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_34 = __this->get_humanPosition_8();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_34);
		int32_t L_35 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_34)->GetAt(0, 1);
		banmen_paint_m3356220390(__this, L_33, L_35, /*hidden argument*/NULL);
	}

IL_0116:
	{
		__this->set_humanExist_16((bool)1);
		Int32U5BU2CU5D_t3030399642* L_36 = __this->get_humanPosition_8();
		int32_t L_37 = __this->get_line_20();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_36);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_36)->SetAt(0, 0, L_37);
		Int32U5BU2CU5D_t3030399642* L_38 = __this->get_humanPosition_8();
		int32_t L_39 = __this->get_row_21();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_38);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_38)->SetAt(0, 1, L_39);
	}

IL_0143:
	{
		KomaU5BU2CU5D_t1205703982* L_40 = __this->get_Board_3();
		int32_t L_41 = __this->get_line_20();
		int32_t L_42 = __this->get_row_21();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_40);
		Koma_t3011766596 * L_43 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_40)->GetAt(L_41, L_42);
		Skytree_t850646749 * L_44 = __this->get_skytree_48();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_43) == ((Il2CppObject*)(Skytree_t850646749 *)L_44))))
		{
			goto IL_0190;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_45 = __this->get_barrier_6();
		int32_t L_46 = __this->get_line_20();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_45);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_45)->SetAt(0, 0, L_46);
		Int32U5BU2CU5D_t3030399642* L_47 = __this->get_barrier_6();
		int32_t L_48 = __this->get_row_21();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_47);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_47)->SetAt(0, 1, L_48);
		goto IL_01d8;
	}

IL_0190:
	{
		KomaU5BU2CU5D_t1205703982* L_49 = __this->get_Board_3();
		int32_t L_50 = __this->get_line_20();
		int32_t L_51 = __this->get_row_21();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_49);
		Koma_t3011766596 * L_52 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_49)->GetAt(L_50, L_51);
		Factory_t931158954 * L_53 = __this->get_factory_46();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_52) == ((Il2CppObject*)(Factory_t931158954 *)L_53))))
		{
			goto IL_01d8;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_54 = __this->get_factoryLocation_7();
		int32_t L_55 = __this->get_line_20();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_54);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_54)->SetAt(0, 0, L_55);
		Int32U5BU2CU5D_t3030399642* L_56 = __this->get_factoryLocation_7();
		int32_t L_57 = __this->get_row_21();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_56);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_56)->SetAt(0, 1, L_57);
	}

IL_01d8:
	{
		return;
	}
}
// System.Void banmen::setKomaNumber(System.Int32)
extern "C"  void banmen_setKomaNumber_m1079153342 (banmen_t2510752313 * __this, int32_t ___komaNumber0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_arrangeMode_13();
		if (!L_0)
		{
			goto IL_008a;
		}
	}
	{
		int32_t L_1 = ___komaNumber0;
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_001a;
		}
	}
	{
		banmen_arrangeOffDo_m4084751936(__this, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		int32_t L_2 = ___komaNumber0;
		Koma_t3011766596 * L_3 = banmen_inputKoma_m3899739766(__this, L_2, /*hidden argument*/NULL);
		__this->set_kariKoma_35(L_3);
		Koma_t3011766596 * L_4 = __this->get_kariKoma_35();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_resources_3();
		int32_t L_6 = __this->get_myResources_39();
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			goto IL_003e;
		}
	}
	{
		return;
	}

IL_003e:
	{
		baseMethods_t2705876163 * L_7 = __this->get_baseM_2();
		NullCheck(L_7);
		baseMethods_setColorRed_m3950151350(L_7, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0072;
	}

IL_0050:
	{
		baseMethods_t2705876163 * L_8 = __this->get_baseM_2();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		baseMethods_printColorBottom_m2694251173(L_8, ((int32_t)((int32_t)L_9+(int32_t)1)), 5, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_10 = __this->get_baseM_2();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		baseMethods_printColorBottom_m2694251173(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), 6, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) < ((int32_t)6)))
		{
			goto IL_0050;
		}
	}
	{
		baseMethods_t2705876163 * L_14 = __this->get_baseM_2();
		NullCheck(L_14);
		baseMethods_canTouchAll_m280133149(L_14, (bool)1, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_008a:
	{
		int32_t L_15 = ___komaNumber0;
		banmen_Produce_m2731612623(__this, L_15, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void banmen::whichPlayer()
extern "C"  void banmen_whichPlayer_m4081184614 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_arrangeMode_13();
		if (!L_0)
		{
			goto IL_009b;
		}
	}
	{
		BooleanU5BU2CU5D_t3568034316* L_1 = __this->get_canTouchPlace_4();
		int32_t L_2 = __this->get_line_20();
		int32_t L_3 = __this->get_row_21();
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_1);
		bool L_4 = ((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_1)->GetAt(L_2, L_3);
		if (L_4)
		{
			goto IL_0028;
		}
	}
	{
		return;
	}

IL_0028:
	{
		Koma_t3011766596 * L_5 = __this->get_kariKoma_35();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_resources_3();
		int32_t L_7 = __this->get_myResources_39();
		if ((((int32_t)L_6) <= ((int32_t)L_7)))
		{
			goto IL_003f;
		}
	}
	{
		return;
	}

IL_003f:
	{
		banmen_setKoma_m222615202(__this, /*hidden argument*/NULL);
		__this->set_bl_23((-1));
		__this->set_br_24((-1));
		baseMethods_t2705876163 * L_8 = __this->get_baseM_2();
		NullCheck(L_8);
		baseMethods_setColorWhite_m2690390344(L_8, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_9 = __this->get_baseM_2();
		NullCheck(L_9);
		baseMethods_canTouchAll_m280133149(L_9, (bool)0, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0093;
	}

IL_0071:
	{
		baseMethods_t2705876163 * L_10 = __this->get_baseM_2();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		baseMethods_printColorBottom_m2694251173(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), 5, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_12 = __this->get_baseM_2();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		baseMethods_printColorBottom_m2694251173(L_12, ((int32_t)((int32_t)L_13+(int32_t)1)), 6, /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0093:
	{
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) < ((int32_t)6)))
		{
			goto IL_0071;
		}
	}
	{
		return;
	}

IL_009b:
	{
		banmen_processKoma_m2439105839(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void banmen::processKoma()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t banmen_processKoma_m2439105839_MetadataUsageId;
extern "C"  void banmen_processKoma_m2439105839 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (banmen_processKoma_m2439105839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		BooleanU5BU2CU5D_t3568034316* L_0 = __this->get_canTouchPlace_4();
		int32_t L_1 = __this->get_line_20();
		int32_t L_2 = __this->get_row_21();
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_0);
		bool L_3 = ((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_0)->GetAt(L_1, L_2);
		if (L_3)
		{
			goto IL_003a;
		}
	}
	{
		banmen_reset_m2092301357(__this, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_4 = __this->get_baseM_2();
		NullCheck(L_4);
		baseMethods_canTouchAll_m280133149(L_4, (bool)1, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_5 = __this->get_baseM_2();
		NullCheck(L_5);
		baseMethods_setRedWhite_m3951861276(L_5, /*hidden argument*/NULL);
		return;
	}

IL_003a:
	{
		int32_t L_6 = __this->get_bl_23();
		if ((!(((uint32_t)L_6) == ((uint32_t)(-1)))))
		{
			goto IL_0105;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_7 = __this->get_komaTime_9();
		int32_t L_8 = __this->get_line_20();
		int32_t L_9 = __this->get_row_21();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7);
		int32_t L_10 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7)->GetAt(((int32_t)((int32_t)L_8-(int32_t)1)), ((int32_t)((int32_t)L_9-(int32_t)1)));
		SliderU5BU2CU5D_t1144817635* L_11 = __this->get_komaSlider_10();
		int32_t L_12 = __this->get_line_20();
		int32_t L_13 = __this->get_row_21();
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_11);
		Slider_t297367283 * L_14 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_11)->GetAt(((int32_t)((int32_t)L_12-(int32_t)1)), ((int32_t)((int32_t)L_13-(int32_t)1)));
		NullCheck(L_14);
		float L_15 = Slider_get_maxValue_m3319962262(L_14, /*hidden argument*/NULL);
		if ((((float)(((float)((float)L_10)))) == ((float)L_15)))
		{
			goto IL_0088;
		}
	}
	{
		return;
	}

IL_0088:
	{
		BooleanU5BU2CU5D_t3568034316* L_16 = __this->get_broken_5();
		int32_t L_17 = __this->get_line_20();
		int32_t L_18 = __this->get_row_21();
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_16);
		bool L_19 = ((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_16)->GetAt(((int32_t)((int32_t)L_17-(int32_t)1)), ((int32_t)((int32_t)L_18-(int32_t)1)));
		if (L_19)
		{
			goto IL_00a9;
		}
	}
	{
		return;
	}

IL_00a9:
	{
		int32_t L_20 = __this->get_line_20();
		__this->set_bl_23(L_20);
		int32_t L_21 = __this->get_row_21();
		__this->set_br_24(L_21);
		baseMethods_t2705876163 * L_22 = __this->get_baseM_2();
		NullCheck(L_22);
		baseMethods_canTouchAll_m280133149(L_22, (bool)0, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_23 = __this->get_baseM_2();
		NullCheck(L_23);
		baseMethods_setColorRed_m3950151350(L_23, /*hidden argument*/NULL);
		KomaU5BU2CU5D_t1205703982* L_24 = __this->get_Board_3();
		int32_t L_25 = __this->get_bl_23();
		int32_t L_26 = __this->get_br_24();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_24);
		Koma_t3011766596 * L_27 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_24)->GetAt(L_25, L_26);
		int32_t L_28 = __this->get_bl_23();
		int32_t L_29 = __this->get_br_24();
		NullCheck(L_27);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(5 /* System.Void Koma::Move(System.Int32,System.Int32) */, L_27, L_28, L_29);
		goto IL_0193;
	}

IL_0105:
	{
		int32_t L_30 = __this->get_line_20();
		__this->set_al_25(L_30);
		int32_t L_31 = __this->get_row_21();
		__this->set_ar_26(L_31);
		baseMethods_t2705876163 * L_32 = __this->get_baseM_2();
		NullCheck(L_32);
		bool L_33 = baseMethods_judge_m1432015939(L_32, /*hidden argument*/NULL);
		__this->set_winLose_17(L_33);
		KomaU5BU2CU5D_t1205703982* L_34 = __this->get_Board_3();
		int32_t L_35 = __this->get_bl_23();
		int32_t L_36 = __this->get_br_24();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_34);
		Koma_t3011766596 * L_37 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_34)->GetAt(L_35, L_36);
		KomaU5BU2CU5D_t1205703982* L_38 = __this->get_Board_3();
		int32_t L_39 = __this->get_al_25();
		int32_t L_40 = __this->get_ar_26();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_38);
		Koma_t3011766596 * L_41 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_38)->GetAt(L_39, L_40);
		NullCheck(L_41);
		int32_t L_42 = L_41->get_resources_3();
		NullCheck(L_37);
		VirtActionInvoker1< int32_t >::Invoke(8 /* System.Void Koma::Action(System.Int32) */, L_37, L_42);
		bool L_43 = __this->get_winLose_17();
		if (!L_43)
		{
			goto IL_0176;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_StopRecording_m3761179305(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0176:
	{
		baseMethods_t2705876163 * L_44 = __this->get_baseM_2();
		NullCheck(L_44);
		baseMethods_setRedWhite_m3951861276(L_44, /*hidden argument*/NULL);
		banmen_reset_m2092301357(__this, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_45 = __this->get_baseM_2();
		NullCheck(L_45);
		baseMethods_canTouchAll_m280133149(L_45, (bool)1, /*hidden argument*/NULL);
	}

IL_0193:
	{
		return;
	}
}
// System.Void banmen::reTime()
extern "C"  void banmen_reTime_m4028066712 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		KomaU5BU2CU5D_t1205703982* L_0 = __this->get_Board_3();
		int32_t L_1 = __this->get_al_25();
		int32_t L_2 = __this->get_ar_26();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0);
		Koma_t3011766596 * L_3 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0)->GetAt(L_1, L_2);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_komaNumber_2();
		int32_t L_5 = __this->get_halfOfKoma_22();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_00d5;
		}
	}
	{
		baseMethods_t2705876163 * L_6 = __this->get_baseM_2();
		NullCheck(L_6);
		baseMethods_setColorClear_m4166068504(L_6, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_7 = __this->get_baseM_2();
		int32_t L_8 = __this->get_bl_23();
		int32_t L_9 = __this->get_br_24();
		NullCheck(L_7);
		baseMethods_OffTimeBar_m3958558077(L_7, L_8, L_9, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_10 = __this->get_baseM_2();
		int32_t L_11 = __this->get_al_25();
		int32_t L_12 = __this->get_ar_26();
		NullCheck(L_10);
		baseMethods_OffTimeBar2_m299894443(L_10, L_11, L_12, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_13 = __this->get_baseM_2();
		int32_t L_14 = __this->get_al_25();
		int32_t L_15 = __this->get_ar_26();
		NullCheck(L_13);
		baseMethods_OnTimeBar_m1542858583(L_13, L_14, L_15, /*hidden argument*/NULL);
		Int32U5BU2CU5D_t3030399642* L_16 = __this->get_komaTime_9();
		int32_t L_17 = __this->get_al_25();
		int32_t L_18 = __this->get_ar_26();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_16);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_16)->SetAt(((int32_t)((int32_t)L_17-(int32_t)1)), ((int32_t)((int32_t)L_18-(int32_t)1)), 0);
		SliderU5BU2CU5D_t1144817635* L_19 = __this->get_komaSlider_10();
		int32_t L_20 = __this->get_al_25();
		int32_t L_21 = __this->get_ar_26();
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_19);
		Slider_t297367283 * L_22 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_19)->GetAt(((int32_t)((int32_t)L_20-(int32_t)1)), ((int32_t)((int32_t)L_21-(int32_t)1)));
		KomaU5BU2CU5D_t1205703982* L_23 = __this->get_Board_3();
		int32_t L_24 = __this->get_al_25();
		int32_t L_25 = __this->get_ar_26();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_23);
		Koma_t3011766596 * L_26 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_23)->GetAt(L_24, L_25);
		NullCheck(L_26);
		int32_t L_27 = L_26->get_komaMaxTime_5();
		NullCheck(L_22);
		Slider_set_maxValue_m2951480075(L_22, (((float)((float)L_27))), /*hidden argument*/NULL);
		goto IL_01a3;
	}

IL_00d5:
	{
		baseMethods_t2705876163 * L_28 = __this->get_baseM_2();
		NullCheck(L_28);
		baseMethods_setColorClear_m4166068504(L_28, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_29 = __this->get_baseM_2();
		int32_t L_30 = __this->get_bl_23();
		int32_t L_31 = __this->get_br_24();
		NullCheck(L_29);
		baseMethods_OffTimeBar2_m299894443(L_29, L_30, L_31, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_32 = __this->get_baseM_2();
		int32_t L_33 = __this->get_al_25();
		int32_t L_34 = __this->get_ar_26();
		NullCheck(L_32);
		baseMethods_OffTimeBar_m3958558077(L_32, L_33, L_34, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_35 = __this->get_baseM_2();
		int32_t L_36 = __this->get_al_25();
		int32_t L_37 = __this->get_ar_26();
		NullCheck(L_35);
		baseMethods_OnTimeBar2_m345907837(L_35, L_36, L_37, /*hidden argument*/NULL);
		Int32U5BU2CU5D_t3030399642* L_38 = __this->get_EkomaTime_11();
		int32_t L_39 = __this->get_al_25();
		int32_t L_40 = __this->get_ar_26();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_38);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_38)->SetAt(((int32_t)((int32_t)L_39-(int32_t)1)), ((int32_t)((int32_t)L_40-(int32_t)1)), 0);
		SliderU5BU2CU5D_t1144817635* L_41 = __this->get_EkomaSlider_12();
		int32_t L_42 = __this->get_al_25();
		int32_t L_43 = __this->get_ar_26();
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_41);
		Slider_t297367283 * L_44 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_41)->GetAt(((int32_t)((int32_t)L_42-(int32_t)1)), ((int32_t)((int32_t)L_43-(int32_t)1)));
		KomaU5BU2CU5D_t1205703982* L_45 = __this->get_Board_3();
		int32_t L_46 = __this->get_al_25();
		int32_t L_47 = __this->get_ar_26();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_45);
		Koma_t3011766596 * L_48 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_45)->GetAt(L_46, L_47);
		NullCheck(L_48);
		int32_t L_49 = L_48->get_komaMaxTime_5();
		NullCheck(L_44);
		Slider_set_maxValue_m2951480075(L_44, (((float)((float)L_49))), /*hidden argument*/NULL);
		SliderU5BU2CU5D_t1144817635* L_50 = __this->get_EkomaSlider_12();
		int32_t L_51 = __this->get_al_25();
		int32_t L_52 = __this->get_ar_26();
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_50);
		Slider_t297367283 * L_53 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_50)->GetAt(((int32_t)((int32_t)L_51-(int32_t)1)), ((int32_t)((int32_t)L_52-(int32_t)1)));
		NullCheck(L_53);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_53, (0.0f));
	}

IL_01a3:
	{
		return;
	}
}
// System.Void banmen::timeChangeMissile()
extern "C"  void banmen_timeChangeMissile_m382147847 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		Int32U5BU2CU5D_t3030399642* L_0 = __this->get_komaTime_9();
		int32_t L_1 = __this->get_bl_23();
		int32_t L_2 = __this->get_br_24();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0)->SetAt(((int32_t)((int32_t)L_1-(int32_t)1)), ((int32_t)((int32_t)L_2-(int32_t)1)), 0);
		SliderU5BU2CU5D_t1144817635* L_3 = __this->get_komaSlider_10();
		int32_t L_4 = __this->get_bl_23();
		int32_t L_5 = __this->get_br_24();
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_3);
		Slider_t297367283 * L_6 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_3)->GetAt(((int32_t)((int32_t)L_4-(int32_t)1)), ((int32_t)((int32_t)L_5-(int32_t)1)));
		NullCheck(L_6);
		Slider_set_maxValue_m2951480075(L_6, (80.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void banmen::timeChangeFactory()
extern "C"  void banmen_timeChangeFactory_m1579757147 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		Int32U5BU2CU5D_t3030399642* L_0 = __this->get_komaTime_9();
		Int32U5BU2CU5D_t3030399642* L_1 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_1);
		int32_t L_2 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_1)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_3 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3);
		int32_t L_4 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3)->GetAt(0, 1);
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0)->SetAt(((int32_t)((int32_t)L_2-(int32_t)1)), ((int32_t)((int32_t)L_4-(int32_t)1)), 0);
		SliderU5BU2CU5D_t1144817635* L_5 = __this->get_komaSlider_10();
		Int32U5BU2CU5D_t3030399642* L_6 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_6);
		int32_t L_7 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_6)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_8 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_8);
		int32_t L_9 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_8)->GetAt(0, 1);
		NullCheck((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_5);
		Slider_t297367283 * L_10 = ((SliderU5BU2CU5D_t1144817635*)(SliderU5BU2CU5D_t1144817635*)L_5)->GetAt(((int32_t)((int32_t)L_7-(int32_t)1)), ((int32_t)((int32_t)L_9-(int32_t)1)));
		NullCheck(L_10);
		Slider_set_maxValue_m2951480075(L_10, (100.0f), /*hidden argument*/NULL);
		__this->set_myFactoryTime_37(0);
		return;
	}
}
// System.Void banmen::drawBarrier()
extern "C"  void banmen_drawBarrier_m1973980853 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		KomaU5BU2CU5D_t1205703982* L_0 = __this->get_Board_3();
		int32_t L_1 = __this->get_bl_23();
		int32_t L_2 = __this->get_br_24();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0);
		Koma_t3011766596 * L_3 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0)->GetAt(L_1, L_2);
		NullCheck(L_3);
		int32_t L_4 = L_3->get_komaNumber_2();
		if ((((int32_t)L_4) <= ((int32_t)7)))
		{
			goto IL_005b;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_5 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5);
		int32_t L_6 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5)->GetAt(0, 0);
		if ((!(((uint32_t)L_6) == ((uint32_t)(-1)))))
		{
			goto IL_0036;
		}
	}
	{
		return;
	}

IL_0036:
	{
		Int32U5BU2CU5D_t3030399642* L_7 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7);
		int32_t L_8 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_9 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_9);
		int32_t L_10 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_9)->GetAt(0, 1);
		banmen_barrierBlueToWhite_m3077042043(__this, L_8, L_10, /*hidden argument*/NULL);
		goto IL_008f;
	}

IL_005b:
	{
		Int32U5BU2CU5D_t3030399642* L_11 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_11);
		int32_t L_12 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_11)->GetAt(1, 0);
		if ((!(((uint32_t)L_12) == ((uint32_t)(-1)))))
		{
			goto IL_006f;
		}
	}
	{
		return;
	}

IL_006f:
	{
		Int32U5BU2CU5D_t3030399642* L_13 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_13);
		int32_t L_14 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_13)->GetAt(1, 0);
		Int32U5BU2CU5D_t3030399642* L_15 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_15);
		int32_t L_16 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_15)->GetAt(1, 1);
		banmen_barrierBlueToWhite_m3077042043(__this, L_14, L_16, /*hidden argument*/NULL);
	}

IL_008f:
	{
		return;
	}
}
// System.Void banmen::paint(System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3766742095;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern const uint32_t banmen_paint_m3356220390_MetadataUsageId;
extern "C"  void banmen_paint_m3356220390 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (banmen_paint_m3356220390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		KomaU5BU2CU5D_t1205703982* L_0 = __this->get_Board_3();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0);
		Koma_t3011766596 * L_3 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0)->GetAt(L_1, L_2);
		Empty_t4012560223 * L_4 = __this->get_empty_41();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_3) == ((Il2CppObject*)(Empty_t4012560223 *)L_4))))
		{
			goto IL_0028;
		}
	}
	{
		baseMethods_t2705876163 * L_5 = __this->get_baseM_2();
		NullCheck(L_5);
		baseMethods_setColorClear_m4166068504(L_5, /*hidden argument*/NULL);
		goto IL_0033;
	}

IL_0028:
	{
		baseMethods_t2705876163 * L_6 = __this->get_baseM_2();
		NullCheck(L_6);
		baseMethods_setColorWhite_m2690390344(L_6, /*hidden argument*/NULL);
	}

IL_0033:
	{
		ObjectU5BU5D_t3614634134* L_7 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral3766742095);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3766742095);
		ObjectU5BU5D_t3614634134* L_8 = L_7;
		int32_t L_9 = ___l0;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral372029313);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_13 = L_12;
		int32_t L_14 = ___r1;
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m3881798623(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = GameObject_Find_m836511350(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Image_t2042527209 * L_19 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_18, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		float L_20 = __this->get_changeRed_27();
		float L_21 = __this->get_changeGreen_28();
		float L_22 = __this->get_changeBlue_29();
		float L_23 = __this->get_changeAlpha_30();
		Color_t2020392075  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Color__ctor_m1909920690(&L_24, L_20, L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_19, L_24);
		ObjectU5BU5D_t3614634134* L_25 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral3766742095);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3766742095);
		ObjectU5BU5D_t3614634134* L_26 = L_25;
		int32_t L_27 = ___l0;
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_29);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_29);
		ObjectU5BU5D_t3614634134* L_30 = L_26;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, _stringLiteral372029313);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_31 = L_30;
		int32_t L_32 = ___r1;
		int32_t L_33 = L_32;
		Il2CppObject * L_34 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_34);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_34);
		String_t* L_35 = String_Concat_m3881798623(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_36 = GameObject_Find_m836511350(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Image_t2042527209 * L_37 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_36, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		__this->set_img_18(L_37);
		BooleanU5BU2CU5D_t3568034316* L_38 = __this->get_broken_5();
		int32_t L_39 = ___l0;
		int32_t L_40 = ___r1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_38);
		bool L_41 = ((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_38)->GetAt(((int32_t)((int32_t)L_39-(int32_t)1)), ((int32_t)((int32_t)L_40-(int32_t)1)));
		if (!L_41)
		{
			goto IL_0101;
		}
	}
	{
		Image_t2042527209 * L_42 = __this->get_img_18();
		KomaU5BU2CU5D_t1205703982* L_43 = __this->get_Board_3();
		int32_t L_44 = ___l0;
		int32_t L_45 = ___r1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_43);
		Koma_t3011766596 * L_46 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_43)->GetAt(L_44, L_45);
		NullCheck(L_46);
		Sprite_t309593783 * L_47 = L_46->get_komaView_1();
		NullCheck(L_42);
		Image_set_sprite_m1800056820(L_42, L_47, /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_0101:
	{
		Image_t2042527209 * L_48 = __this->get_img_18();
		KomaU5BU2CU5D_t1205703982* L_49 = __this->get_Board_3();
		int32_t L_50 = ___l0;
		int32_t L_51 = ___r1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_49);
		Koma_t3011766596 * L_52 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_49)->GetAt(L_50, L_51);
		NullCheck(L_52);
		Sprite_t309593783 * L_53 = L_52->get_brokeKomaView_4();
		NullCheck(L_48);
		Image_set_sprite_m1800056820(L_48, L_53, /*hidden argument*/NULL);
	}

IL_011e:
	{
		return;
	}
}
// System.Void banmen::rePaint()
extern "C"  void banmen_rePaint_m1556028161 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_bl_23();
		int32_t L_1 = __this->get_br_24();
		banmen_paint_m3356220390(__this, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_al_25();
		int32_t L_3 = __this->get_ar_26();
		banmen_paint_m3356220390(__this, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void banmen::changeSprite()
extern "C"  void banmen_changeSprite_m109808171 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		KomaU5BU2CU5D_t1205703982* L_0 = __this->get_Board_3();
		int32_t L_1 = __this->get_al_25();
		int32_t L_2 = __this->get_ar_26();
		KomaU5BU2CU5D_t1205703982* L_3 = __this->get_Board_3();
		int32_t L_4 = __this->get_bl_23();
		int32_t L_5 = __this->get_br_24();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_3);
		Koma_t3011766596 * L_6 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_3)->GetAt(L_4, L_5);
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0)->SetAt(L_1, L_2, L_6);
		KomaU5BU2CU5D_t1205703982* L_7 = __this->get_Board_3();
		int32_t L_8 = __this->get_bl_23();
		int32_t L_9 = __this->get_br_24();
		Empty_t4012560223 * L_10 = __this->get_empty_41();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_7);
		((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_7)->SetAt(L_8, L_9, L_10);
		BooleanU5BU2CU5D_t3568034316* L_11 = __this->get_broken_5();
		int32_t L_12 = __this->get_al_25();
		int32_t L_13 = __this->get_ar_26();
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_11);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_11)->SetAt(((int32_t)((int32_t)L_12-(int32_t)1)), ((int32_t)((int32_t)L_13-(int32_t)1)), (bool)1);
		return;
	}
}
// Koma banmen::inputKoma(System.Int32)
extern "C"  Koma_t3011766596 * banmen_inputKoma_m3899739766 (banmen_t2510752313 * __this, int32_t ___komaNum0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___komaNum0;
		if (L_0 == 0)
		{
			goto IL_00a9;
		}
		if (L_0 == 1)
		{
			goto IL_00a2;
		}
		if (L_0 == 2)
		{
			goto IL_009b;
		}
		if (L_0 == 3)
		{
			goto IL_0094;
		}
		if (L_0 == 4)
		{
			goto IL_008d;
		}
		if (L_0 == 5)
		{
			goto IL_0086;
		}
		if (L_0 == 6)
		{
			goto IL_007f;
		}
		if (L_0 == 7)
		{
			goto IL_0078;
		}
		if (L_0 == 8)
		{
			goto IL_0071;
		}
		if (L_0 == 9)
		{
			goto IL_006a;
		}
		if (L_0 == 10)
		{
			goto IL_0063;
		}
		if (L_0 == 11)
		{
			goto IL_005c;
		}
		if (L_0 == 12)
		{
			goto IL_0055;
		}
		if (L_0 == 13)
		{
			goto IL_004e;
		}
		if (L_0 == 14)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_00b0;
	}

IL_0047:
	{
		ESkytree_t2245705948 * L_1 = __this->get_Eskytree_55();
		return L_1;
	}

IL_004e:
	{
		EDrone_t2061607325 * L_2 = __this->get_Edrone_54();
		return L_2;
	}

IL_0055:
	{
		EFactory_t2050856767 * L_3 = __this->get_Efactory_53();
		return L_3;
	}

IL_005c:
	{
		EMissile_t3534618099 * L_4 = __this->get_Emissile_52();
		return L_4;
	}

IL_0063:
	{
		ESRobot_t2546702684 * L_5 = __this->get_EsRobot_51();
		return L_5;
	}

IL_006a:
	{
		EGRobot_t1059295608 * L_6 = __this->get_EgRobot_50();
		return L_6;
	}

IL_0071:
	{
		EHuman_t715499684 * L_7 = __this->get_Ehuman_49();
		return L_7;
	}

IL_0078:
	{
		Skytree_t850646749 * L_8 = __this->get_skytree_48();
		return L_8;
	}

IL_007f:
	{
		Drone_t860454962 * L_9 = __this->get_drone_47();
		return L_9;
	}

IL_0086:
	{
		Factory_t931158954 * L_10 = __this->get_factory_46();
		return L_10;
	}

IL_008d:
	{
		Missile_t813944928 * L_11 = __this->get_missile_45();
		return L_11;
	}

IL_0094:
	{
		SRobot_t456246757 * L_12 = __this->get_sRobot_44();
		return L_12;
	}

IL_009b:
	{
		GRobot_t456225233 * L_13 = __this->get_gRobot_43();
		return L_13;
	}

IL_00a2:
	{
		Human_t1156088493 * L_14 = __this->get_human_42();
		return L_14;
	}

IL_00a9:
	{
		Empty_t4012560223 * L_15 = __this->get_empty_41();
		return L_15;
	}

IL_00b0:
	{
		Empty_t4012560223 * L_16 = __this->get_empty_41();
		return L_16;
	}
}
// System.Void banmen::atackMissile()
extern "C"  void banmen_atackMissile_m1714889658 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_satelite_33();
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		baseMethods_t2705876163 * L_1 = __this->get_baseM_2();
		NullCheck(L_1);
		baseMethods_canTouchAll_m280133149(L_1, (bool)1, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_2 = __this->get_baseM_2();
		NullCheck(L_2);
		baseMethods_allPrintColorBottom_m2081970466(L_2, /*hidden argument*/NULL);
		Int32U5BU2CU5D_t3030399642* L_3 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3);
		int32_t L_4 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3)->GetAt(1, 0);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0036;
		}
	}
	{
		return;
	}

IL_0036:
	{
		Int32U5BU2CU5D_t3030399642* L_5 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5);
		int32_t L_6 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5)->GetAt(1, 0);
		Int32U5BU2CU5D_t3030399642* L_7 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7);
		int32_t L_8 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7)->GetAt(1, 1);
		banmen_barrierPoint_m2170502217(__this, L_6, L_8, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_005b:
	{
		baseMethods_t2705876163 * L_9 = __this->get_baseM_2();
		NullCheck(L_9);
		baseMethods_canTouchHalf_m1396259078(L_9, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_10 = __this->get_baseM_2();
		NullCheck(L_10);
		baseMethods_halfPrintColorBottom_m2366856657(L_10, (bool)0, /*hidden argument*/NULL);
	}

IL_0072:
	{
		return;
	}
}
// System.Void banmen::atackEMissile()
extern "C"  void banmen_atackEMissile_m1668074645 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_Esatelite_34();
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		baseMethods_t2705876163 * L_1 = __this->get_baseM_2();
		NullCheck(L_1);
		baseMethods_canTouchAll_m280133149(L_1, (bool)1, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_2 = __this->get_baseM_2();
		NullCheck(L_2);
		baseMethods_allPrintColorBottom_m2081970466(L_2, /*hidden argument*/NULL);
		Int32U5BU2CU5D_t3030399642* L_3 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3);
		int32_t L_4 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3)->GetAt(0, 0);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0036;
		}
	}
	{
		return;
	}

IL_0036:
	{
		Int32U5BU2CU5D_t3030399642* L_5 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5);
		int32_t L_6 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_7 = __this->get_barrier_6();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7);
		int32_t L_8 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7)->GetAt(0, 1);
		banmen_barrierPoint_m2170502217(__this, L_6, L_8, /*hidden argument*/NULL);
		goto IL_0072;
	}

IL_005b:
	{
		baseMethods_t2705876163 * L_9 = __this->get_baseM_2();
		NullCheck(L_9);
		baseMethods_canTouchHalf_m1396259078(L_9, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_10 = __this->get_baseM_2();
		NullCheck(L_10);
		baseMethods_halfPrintColorBottom_m2366856657(L_10, (bool)1, /*hidden argument*/NULL);
	}

IL_0072:
	{
		return;
	}
}
// System.Void banmen::destroyByMissile()
extern "C"  void banmen_destroyByMissile_m1609839829 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		BooleanU5BU2CU5D_t3568034316* L_0 = __this->get_broken_5();
		int32_t L_1 = __this->get_al_25();
		int32_t L_2 = __this->get_ar_26();
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_0);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_0)->SetAt(((int32_t)((int32_t)L_1-(int32_t)1)), ((int32_t)((int32_t)L_2-(int32_t)1)), (bool)0);
		int32_t L_3 = __this->get_al_25();
		int32_t L_4 = __this->get_ar_26();
		banmen_paint_m3356220390(__this, L_3, L_4, /*hidden argument*/NULL);
		banmen_timeChangeMissile_m382147847(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void banmen::barrierPoint(System.Int32,System.Int32)
extern "C"  void banmen_barrierPoint_m2170502217 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		baseMethods_t2705876163 * L_0 = __this->get_baseM_2();
		NullCheck(L_0);
		baseMethods_setColorBlue_m4082331027(L_0, /*hidden argument*/NULL);
		V_0 = (-1);
		goto IL_006d;
	}

IL_0012:
	{
		V_1 = (-1);
		goto IL_0062;
	}

IL_0019:
	{
		int32_t L_1 = ___l0;
		int32_t L_2 = V_0;
		if (!((int32_t)((int32_t)L_1+(int32_t)L_2)))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_3 = ___l0;
		int32_t L_4 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_3+(int32_t)L_4))) == ((int32_t)7)))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_5 = ___r1;
		int32_t L_6 = V_1;
		if (!((int32_t)((int32_t)L_5+(int32_t)L_6)))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_7 = ___r1;
		int32_t L_8 = V_1;
		if ((((int32_t)((int32_t)((int32_t)L_7+(int32_t)L_8))) == ((int32_t)7)))
		{
			goto IL_005e;
		}
	}
	{
		BooleanU5BU2CU5D_t3568034316* L_9 = __this->get_canTouchPlace_4();
		int32_t L_10 = ___l0;
		int32_t L_11 = V_0;
		int32_t L_12 = ___r1;
		int32_t L_13 = V_1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_9);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_9)->SetAt(((int32_t)((int32_t)L_10+(int32_t)L_11)), ((int32_t)((int32_t)L_12+(int32_t)L_13)), (bool)0);
		baseMethods_t2705876163 * L_14 = __this->get_baseM_2();
		int32_t L_15 = ___l0;
		int32_t L_16 = V_0;
		int32_t L_17 = ___r1;
		int32_t L_18 = V_1;
		NullCheck(L_14);
		baseMethods_printColorBottom_m2694251173(L_14, ((int32_t)((int32_t)L_15+(int32_t)L_16)), ((int32_t)((int32_t)L_17+(int32_t)L_18)), /*hidden argument*/NULL);
	}

IL_005e:
	{
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0062:
	{
		int32_t L_20 = V_1;
		if ((((int32_t)L_20) < ((int32_t)2)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_21 = V_0;
		V_0 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_006d:
	{
		int32_t L_22 = V_0;
		if ((((int32_t)L_22) < ((int32_t)2)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void banmen::barrierBlueToWhite(System.Int32,System.Int32)
extern "C"  void banmen_barrierBlueToWhite_m3077042043 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		baseMethods_t2705876163 * L_0 = __this->get_baseM_2();
		NullCheck(L_0);
		baseMethods_setColorWhite_m2690390344(L_0, /*hidden argument*/NULL);
		V_0 = (-1);
		goto IL_005b;
	}

IL_0012:
	{
		V_1 = (-1);
		goto IL_0050;
	}

IL_0019:
	{
		int32_t L_1 = ___l0;
		int32_t L_2 = V_0;
		if (!((int32_t)((int32_t)L_1+(int32_t)L_2)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_3 = ___l0;
		int32_t L_4 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_3+(int32_t)L_4))) == ((int32_t)7)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_5 = ___r1;
		int32_t L_6 = V_1;
		if (!((int32_t)((int32_t)L_5+(int32_t)L_6)))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_7 = ___r1;
		int32_t L_8 = V_1;
		if ((((int32_t)((int32_t)((int32_t)L_7+(int32_t)L_8))) == ((int32_t)7)))
		{
			goto IL_004c;
		}
	}
	{
		baseMethods_t2705876163 * L_9 = __this->get_baseM_2();
		int32_t L_10 = ___l0;
		int32_t L_11 = V_0;
		int32_t L_12 = ___r1;
		int32_t L_13 = V_1;
		NullCheck(L_9);
		baseMethods_printColorBottom_m2694251173(L_9, ((int32_t)((int32_t)L_10+(int32_t)L_11)), ((int32_t)((int32_t)L_12+(int32_t)L_13)), /*hidden argument*/NULL);
	}

IL_004c:
	{
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_15 = V_1;
		if ((((int32_t)L_15) < ((int32_t)2)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) < ((int32_t)2)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void banmen::isThereKoma(System.Int32,System.Int32)
extern "C"  void banmen_isThereKoma_m207992560 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		KomaU5BU2CU5D_t1205703982* L_0 = __this->get_Board_3();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0);
		Koma_t3011766596 * L_3 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0)->GetAt(L_1, L_2);
		Empty_t4012560223 * L_4 = __this->get_empty_41();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_3) == ((Il2CppObject*)(Empty_t4012560223 *)L_4))))
		{
			goto IL_0034;
		}
	}
	{
		BooleanU5BU2CU5D_t3568034316* L_5 = __this->get_canTouchPlace_4();
		int32_t L_6 = ___l0;
		int32_t L_7 = ___r1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_5);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_5)->SetAt(L_6, L_7, (bool)1);
		baseMethods_t2705876163 * L_8 = __this->get_baseM_2();
		int32_t L_9 = ___l0;
		int32_t L_10 = ___r1;
		NullCheck(L_8);
		baseMethods_printColorBottom_m2694251173(L_8, L_9, L_10, /*hidden argument*/NULL);
		return;
	}

IL_0034:
	{
		KomaU5BU2CU5D_t1205703982* L_11 = __this->get_Board_3();
		int32_t L_12 = ___l0;
		int32_t L_13 = ___r1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_11);
		Koma_t3011766596 * L_14 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_11)->GetAt(L_12, L_13);
		Futi_t612262014 * L_15 = __this->get_futi_56();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_14) == ((Il2CppObject*)(Futi_t612262014 *)L_15))))
		{
			goto IL_0054;
		}
	}
	{
		__this->set_wall_32((bool)0);
		return;
	}

IL_0054:
	{
		KomaU5BU2CU5D_t1205703982* L_16 = __this->get_Board_3();
		int32_t L_17 = ___l0;
		int32_t L_18 = ___r1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_16);
		Koma_t3011766596 * L_19 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_16)->GetAt(L_17, L_18);
		NullCheck(L_19);
		int32_t L_20 = L_19->get_komaNumber_2();
		if ((((int32_t)L_20) <= ((int32_t)7)))
		{
			goto IL_009a;
		}
	}
	{
		bool L_21 = __this->get_enemy_31();
		if (L_21)
		{
			goto IL_009a;
		}
	}
	{
		BooleanU5BU2CU5D_t3568034316* L_22 = __this->get_canTouchPlace_4();
		int32_t L_23 = ___l0;
		int32_t L_24 = ___r1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_22);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_22)->SetAt(L_23, L_24, (bool)1);
		baseMethods_t2705876163 * L_25 = __this->get_baseM_2();
		int32_t L_26 = ___l0;
		int32_t L_27 = ___r1;
		NullCheck(L_25);
		baseMethods_printColorBottom_m2694251173(L_25, L_26, L_27, /*hidden argument*/NULL);
		__this->set_wall_32((bool)0);
		return;
	}

IL_009a:
	{
		KomaU5BU2CU5D_t1205703982* L_28 = __this->get_Board_3();
		int32_t L_29 = ___l0;
		int32_t L_30 = ___r1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_28);
		Koma_t3011766596 * L_31 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_28)->GetAt(L_29, L_30);
		NullCheck(L_31);
		int32_t L_32 = L_31->get_komaNumber_2();
		int32_t L_33 = __this->get_halfOfKoma_22();
		if ((((int32_t)L_32) >= ((int32_t)L_33)))
		{
			goto IL_00e5;
		}
	}
	{
		bool L_34 = __this->get_enemy_31();
		if (!L_34)
		{
			goto IL_00e5;
		}
	}
	{
		BooleanU5BU2CU5D_t3568034316* L_35 = __this->get_canTouchPlace_4();
		int32_t L_36 = ___l0;
		int32_t L_37 = ___r1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_35);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_35)->SetAt(L_36, L_37, (bool)1);
		baseMethods_t2705876163 * L_38 = __this->get_baseM_2();
		int32_t L_39 = ___l0;
		int32_t L_40 = ___r1;
		NullCheck(L_38);
		baseMethods_printColorBottom_m2694251173(L_38, L_39, L_40, /*hidden argument*/NULL);
		__this->set_wall_32((bool)0);
		return;
	}

IL_00e5:
	{
		BooleanU5BU2CU5D_t3568034316* L_41 = __this->get_broken_5();
		int32_t L_42 = ___l0;
		int32_t L_43 = ___r1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_41);
		bool L_44 = ((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_41)->GetAt(((int32_t)((int32_t)L_42-(int32_t)1)), ((int32_t)((int32_t)L_43-(int32_t)1)));
		if (L_44)
		{
			goto IL_011e;
		}
	}
	{
		BooleanU5BU2CU5D_t3568034316* L_45 = __this->get_canTouchPlace_4();
		int32_t L_46 = ___l0;
		int32_t L_47 = ___r1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_45);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_45)->SetAt(L_46, L_47, (bool)1);
		baseMethods_t2705876163 * L_48 = __this->get_baseM_2();
		int32_t L_49 = ___l0;
		int32_t L_50 = ___r1;
		NullCheck(L_48);
		baseMethods_printColorBottom_m2694251173(L_48, L_49, L_50, /*hidden argument*/NULL);
		__this->set_wall_32((bool)0);
		return;
	}

IL_011e:
	{
		__this->set_wall_32((bool)0);
		return;
	}
}
// System.Void banmen::isThereFuti(System.Int32,System.Int32)
extern "C"  void banmen_isThereFuti_m1442766142 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		KomaU5BU2CU5D_t1205703982* L_0 = __this->get_Board_3();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0);
		Koma_t3011766596 * L_3 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0)->GetAt(L_1, L_2);
		Futi_t612262014 * L_4 = __this->get_futi_56();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_3) == ((Il2CppObject*)(Futi_t612262014 *)L_4))))
		{
			goto IL_0024;
		}
	}
	{
		__this->set_wall_32((bool)0);
		goto IL_003f;
	}

IL_0024:
	{
		BooleanU5BU2CU5D_t3568034316* L_5 = __this->get_canTouchPlace_4();
		int32_t L_6 = ___l0;
		int32_t L_7 = ___r1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_5);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_5)->SetAt(L_6, L_7, (bool)1);
		baseMethods_t2705876163 * L_8 = __this->get_baseM_2();
		int32_t L_9 = ___l0;
		int32_t L_10 = ___r1;
		NullCheck(L_8);
		baseMethods_printColorBottom_m2694251173(L_8, L_9, L_10, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void banmen::isThereDraw(System.Int32,System.Int32)
extern "C"  void banmen_isThereDraw_m900625674 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		KomaU5BU2CU5D_t1205703982* L_0 = __this->get_Board_3();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0);
		Koma_t3011766596 * L_3 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0)->GetAt(L_1, L_2);
		Futi_t612262014 * L_4 = __this->get_futi_56();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_3) == ((Il2CppObject*)(Futi_t612262014 *)L_4))))
		{
			goto IL_0024;
		}
	}
	{
		__this->set_wall_32((bool)0);
		goto IL_0044;
	}

IL_0024:
	{
		int32_t L_5 = ___l0;
		int32_t L_6 = ___r1;
		banmen_paint_m3356220390(__this, L_5, L_6, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_7 = __this->get_baseM_2();
		NullCheck(L_7);
		baseMethods_setColorWhite_m2690390344(L_7, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_8 = __this->get_baseM_2();
		int32_t L_9 = ___l0;
		int32_t L_10 = ___r1;
		NullCheck(L_8);
		baseMethods_printColorBottom_m2694251173(L_8, L_9, L_10, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void banmen::isThereErase(System.Int32,System.Int32)
extern "C"  void banmen_isThereErase_m2459852564 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		KomaU5BU2CU5D_t1205703982* L_0 = __this->get_Board_3();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0);
		Koma_t3011766596 * L_3 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_0)->GetAt(L_1, L_2);
		Futi_t612262014 * L_4 = __this->get_futi_56();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_3) == ((Il2CppObject*)(Futi_t612262014 *)L_4))))
		{
			goto IL_0024;
		}
	}
	{
		__this->set_wall_32((bool)0);
		goto IL_0049;
	}

IL_0024:
	{
		baseMethods_t2705876163 * L_5 = __this->get_baseM_2();
		NullCheck(L_5);
		baseMethods_setColorClear_m4166068504(L_5, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_6 = __this->get_baseM_2();
		int32_t L_7 = ___l0;
		int32_t L_8 = ___r1;
		NullCheck(L_6);
		baseMethods_printColorAbove_m553988101(L_6, L_7, L_8, /*hidden argument*/NULL);
		baseMethods_t2705876163 * L_9 = __this->get_baseM_2();
		int32_t L_10 = ___l0;
		int32_t L_11 = ___r1;
		NullCheck(L_9);
		baseMethods_printColorBottom_m2694251173(L_9, L_10, L_11, /*hidden argument*/NULL);
	}

IL_0049:
	{
		return;
	}
}
// System.Void banmen::reset()
extern "C"  void banmen_reset_m2092301357 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		__this->set_bl_23((-1));
		__this->set_br_24((-1));
		__this->set_al_25((-1));
		__this->set_ar_26((-1));
		return;
	}
}
// System.Void banmen::ProduceGRobot()
extern "C"  void banmen_ProduceGRobot_m3664332021 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		Int32U5BU2CU5D_t3030399642* L_0 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0);
		int32_t L_1 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0)->GetAt(0, 0);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		KomaU5BU2CU5D_t1205703982* L_2 = __this->get_Board_3();
		Int32U5BU2CU5D_t3030399642* L_3 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3);
		int32_t L_4 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_5 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5);
		int32_t L_6 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5)->GetAt(0, 1);
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_2);
		Koma_t3011766596 * L_7 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_2)->GetAt(L_4, ((int32_t)((int32_t)L_6-(int32_t)1)));
		Empty_t4012560223 * L_8 = __this->get_empty_41();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_7) == ((Il2CppObject*)(Empty_t4012560223 *)L_8))))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_9 = __this->get_myResources_39();
		if ((((int32_t)L_9) < ((int32_t)5)))
		{
			goto IL_0071;
		}
	}
	{
		baseMethods_t2705876163 * L_10 = __this->get_baseM_2();
		NullCheck(L_10);
		baseMethods_getMyResources_m1880650416(L_10, ((int32_t)-5), /*hidden argument*/NULL);
		banmen_timeChangeFactory_m1579757147(__this, /*hidden argument*/NULL);
		GRobot_t456225233 * L_11 = __this->get_gRobot_43();
		__this->set_kariKoma_35(L_11);
	}

IL_0071:
	{
		return;
	}
}
// System.Void banmen::ProduceSRobot()
extern "C"  void banmen_ProduceSRobot_m4010844345 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		Int32U5BU2CU5D_t3030399642* L_0 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0);
		int32_t L_1 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0)->GetAt(0, 0);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		KomaU5BU2CU5D_t1205703982* L_2 = __this->get_Board_3();
		Int32U5BU2CU5D_t3030399642* L_3 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3);
		int32_t L_4 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_5 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5);
		int32_t L_6 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5)->GetAt(0, 1);
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_2);
		Koma_t3011766596 * L_7 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_2)->GetAt(L_4, ((int32_t)((int32_t)L_6-(int32_t)1)));
		Empty_t4012560223 * L_8 = __this->get_empty_41();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_7) == ((Il2CppObject*)(Empty_t4012560223 *)L_8))))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_9 = __this->get_myResources_39();
		if ((((int32_t)L_9) < ((int32_t)4)))
		{
			goto IL_0071;
		}
	}
	{
		baseMethods_t2705876163 * L_10 = __this->get_baseM_2();
		NullCheck(L_10);
		baseMethods_getMyResources_m1880650416(L_10, ((int32_t)-4), /*hidden argument*/NULL);
		banmen_timeChangeFactory_m1579757147(__this, /*hidden argument*/NULL);
		SRobot_t456246757 * L_11 = __this->get_sRobot_44();
		__this->set_kariKoma_35(L_11);
	}

IL_0071:
	{
		return;
	}
}
// System.Void banmen::ProduceDrone()
extern "C"  void banmen_ProduceDrone_m526588790 (banmen_t2510752313 * __this, const MethodInfo* method)
{
	{
		Int32U5BU2CU5D_t3030399642* L_0 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0);
		int32_t L_1 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0)->GetAt(0, 0);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		KomaU5BU2CU5D_t1205703982* L_2 = __this->get_Board_3();
		Int32U5BU2CU5D_t3030399642* L_3 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3);
		int32_t L_4 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_3)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_5 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5);
		int32_t L_6 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5)->GetAt(0, 1);
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_2);
		Koma_t3011766596 * L_7 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_2)->GetAt(L_4, ((int32_t)((int32_t)L_6-(int32_t)1)));
		Empty_t4012560223 * L_8 = __this->get_empty_41();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_7) == ((Il2CppObject*)(Empty_t4012560223 *)L_8))))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_9 = __this->get_myResources_39();
		if ((((int32_t)L_9) < ((int32_t)3)))
		{
			goto IL_0071;
		}
	}
	{
		baseMethods_t2705876163 * L_10 = __this->get_baseM_2();
		NullCheck(L_10);
		baseMethods_getMyResources_m1880650416(L_10, ((int32_t)-3), /*hidden argument*/NULL);
		banmen_timeChangeFactory_m1579757147(__this, /*hidden argument*/NULL);
		Drone_t860454962 * L_11 = __this->get_drone_47();
		__this->set_kariKoma_35(L_11);
	}

IL_0071:
	{
		return;
	}
}
// System.Void banmen::Produce(System.Int32)
extern "C"  void banmen_Produce_m2731612623 (banmen_t2510752313 * __this, int32_t ___komaNum0, const MethodInfo* method)
{
	{
		Int32U5BU2CU5D_t3030399642* L_0 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0);
		int32_t L_1 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0)->GetAt(0, 0);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		int32_t L_2 = ___komaNum0;
		Koma_t3011766596 * L_3 = banmen_inputKoma_m3899739766(__this, L_2, /*hidden argument*/NULL);
		__this->set_kariKoma_35(L_3);
		KomaU5BU2CU5D_t1205703982* L_4 = __this->get_Board_3();
		Int32U5BU2CU5D_t3030399642* L_5 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5);
		int32_t L_6 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_5)->GetAt(0, 0);
		Int32U5BU2CU5D_t3030399642* L_7 = __this->get_factoryLocation_7();
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7);
		int32_t L_8 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_7)->GetAt(0, 1);
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_4);
		Koma_t3011766596 * L_9 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_4)->GetAt(L_6, ((int32_t)((int32_t)L_8-(int32_t)1)));
		Empty_t4012560223 * L_10 = __this->get_empty_41();
		if ((!(((Il2CppObject*)(Koma_t3011766596 *)L_9) == ((Il2CppObject*)(Empty_t4012560223 *)L_10))))
		{
			goto IL_0086;
		}
	}
	{
		int32_t L_11 = __this->get_myResources_39();
		Koma_t3011766596 * L_12 = __this->get_kariKoma_35();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_resources_3();
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0086;
		}
	}
	{
		baseMethods_t2705876163 * L_14 = __this->get_baseM_2();
		Koma_t3011766596 * L_15 = __this->get_kariKoma_35();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_resources_3();
		NullCheck(L_14);
		baseMethods_getMyResources_m1880650416(L_14, ((-L_16)), /*hidden argument*/NULL);
		banmen_timeChangeFactory_m1579757147(__this, /*hidden argument*/NULL);
	}

IL_0086:
	{
		return;
	}
}
// System.Void baseMethods::.ctor()
extern "C"  void baseMethods__ctor_m1615982932 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void baseMethods::Init(System.String)
extern const MethodInfo* GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266_MethodInfo_var;
extern const uint32_t baseMethods_Init_m3651539526_MetadataUsageId;
extern "C"  void baseMethods_Init_m3651539526 (baseMethods_t2705876163 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_Init_m3651539526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		banmen_t2510752313 * L_2 = GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266(L_1, /*hidden argument*/GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266_MethodInfo_var);
		__this->set_ban_0(L_2);
		return;
	}
}
// System.Void baseMethods::allPaint()
extern "C"  void baseMethods_allPaint_m4031034429 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_0054;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_0049;
	}

IL_000e:
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		KomaU5BU2CU5D_t1205703982* L_1 = L_0->get_Board_3();
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_1);
		Koma_t3011766596 * L_4 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_1)->GetAt(((int32_t)((int32_t)L_2+(int32_t)1)), ((int32_t)((int32_t)L_3+(int32_t)1)));
		banmen_t2510752313 * L_5 = __this->get_ban_0();
		NullCheck(L_5);
		Empty_t4012560223 * L_6 = L_5->get_empty_41();
		if ((((Il2CppObject*)(Koma_t3011766596 *)L_4) == ((Il2CppObject*)(Empty_t4012560223 *)L_6)))
		{
			goto IL_0045;
		}
	}
	{
		banmen_t2510752313 * L_7 = __this->get_ban_0();
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_7);
		banmen_paint_m3356220390(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), ((int32_t)((int32_t)L_9+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0045:
	{
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)6)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) < ((int32_t)6)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Boolean baseMethods::judge()
extern const MethodInfo* GameObject_GetComponent_TisSingleton_t1770047133_m2880166840_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2462408499;
extern const uint32_t baseMethods_judge_m1432015939_MetadataUsageId;
extern "C"  bool baseMethods_judge_m1432015939 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_judge_m1432015939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		KomaU5BU2CU5D_t1205703982* L_1 = L_0->get_Board_3();
		banmen_t2510752313 * L_2 = __this->get_ban_0();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_al_25();
		banmen_t2510752313 * L_4 = __this->get_ban_0();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_ar_26();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_1);
		Koma_t3011766596 * L_6 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_1)->GetAt(L_3, L_5);
		NullCheck(L_6);
		int32_t L_7 = L_6->get_komaNumber_2();
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_0076;
		}
	}
	{
		banmen_t2510752313 * L_8 = __this->get_ban_0();
		NullCheck(L_8);
		banmen_lose_m1266767581(L_8, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2462408499, /*hidden argument*/NULL);
		__this->set_refObj_1(L_9);
		GameObject_t1756533147 * L_10 = __this->get_refObj_1();
		NullCheck(L_10);
		Singleton_t1770047133 * L_11 = GameObject_GetComponent_TisSingleton_t1770047133_m2880166840(L_10, /*hidden argument*/GameObject_GetComponent_TisSingleton_t1770047133_m2880166840_MethodInfo_var);
		__this->set_singleton_2(L_11);
		Singleton_t1770047133 * L_12 = __this->get_singleton_2();
		NullCheck(L_12);
		Singleton_CountWinLose_m4154155941(L_12, (bool)0, /*hidden argument*/NULL);
		Singleton_t1770047133 * L_13 = __this->get_singleton_2();
		NullCheck(L_13);
		Singleton_Save_m574479149(L_13, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0076:
	{
		banmen_t2510752313 * L_14 = __this->get_ban_0();
		NullCheck(L_14);
		KomaU5BU2CU5D_t1205703982* L_15 = L_14->get_Board_3();
		banmen_t2510752313 * L_16 = __this->get_ban_0();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_al_25();
		banmen_t2510752313 * L_18 = __this->get_ban_0();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_ar_26();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_15);
		Koma_t3011766596 * L_20 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_15)->GetAt(L_17, L_19);
		NullCheck(L_20);
		int32_t L_21 = L_20->get_komaNumber_2();
		if ((!(((uint32_t)L_21) == ((uint32_t)8))))
		{
			goto IL_00ec;
		}
	}
	{
		banmen_t2510752313 * L_22 = __this->get_ban_0();
		NullCheck(L_22);
		banmen_win_m588564420(L_22, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2462408499, /*hidden argument*/NULL);
		__this->set_refObj_1(L_23);
		GameObject_t1756533147 * L_24 = __this->get_refObj_1();
		NullCheck(L_24);
		Singleton_t1770047133 * L_25 = GameObject_GetComponent_TisSingleton_t1770047133_m2880166840(L_24, /*hidden argument*/GameObject_GetComponent_TisSingleton_t1770047133_m2880166840_MethodInfo_var);
		__this->set_singleton_2(L_25);
		Singleton_t1770047133 * L_26 = __this->get_singleton_2();
		NullCheck(L_26);
		Singleton_CountWinLose_m4154155941(L_26, (bool)1, /*hidden argument*/NULL);
		Singleton_t1770047133 * L_27 = __this->get_singleton_2();
		NullCheck(L_27);
		Singleton_Save_m574479149(L_27, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_00ec:
	{
		return (bool)0;
	}
}
// System.Void baseMethods::getResources(System.Int32)
extern "C"  void baseMethods_getResources_m2257601282 (baseMethods_t2705876163 * __this, int32_t ___resource0, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		KomaU5BU2CU5D_t1205703982* L_1 = L_0->get_Board_3();
		banmen_t2510752313 * L_2 = __this->get_ban_0();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_bl_23();
		banmen_t2510752313 * L_4 = __this->get_ban_0();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_br_24();
		NullCheck((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_1);
		Koma_t3011766596 * L_6 = ((KomaU5BU2CU5D_t1205703982*)(KomaU5BU2CU5D_t1205703982*)L_1)->GetAt(L_3, L_5);
		NullCheck(L_6);
		int32_t L_7 = L_6->get_komaNumber_2();
		banmen_t2510752313 * L_8 = __this->get_ban_0();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_halfOfKoma_22();
		if ((((int32_t)L_7) >= ((int32_t)L_9)))
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_10 = ___resource0;
		baseMethods_getMyResources_m1880650416(__this, L_10, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_0047:
	{
		int32_t L_11 = ___resource0;
		baseMethods_getEResources_m992289761(__this, L_11, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void baseMethods::getMyResources(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2369149690;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t baseMethods_getMyResources_m1880650416_MetadataUsageId;
extern "C"  void baseMethods_getMyResources_m1880650416 (baseMethods_t2705876163 * __this, int32_t ___resource0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_getMyResources_m1880650416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2369149690, /*hidden argument*/NULL);
		NullCheck(L_1);
		Text_t356221433 * L_2 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_1, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		NullCheck(L_0);
		L_0->set_text_19(L_2);
		banmen_t2510752313 * L_3 = __this->get_ban_0();
		banmen_t2510752313 * L_4 = __this->get_ban_0();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_myResources_39();
		int32_t L_6 = ___resource0;
		NullCheck(L_3);
		L_3->set_myResources_39(((int32_t)((int32_t)L_5+(int32_t)L_6)));
		banmen_t2510752313 * L_7 = __this->get_ban_0();
		NullCheck(L_7);
		Text_t356221433 * L_8 = L_7->get_text_19();
		banmen_t2510752313 * L_9 = __this->get_ban_0();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_myResources_39();
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral372029310, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_13);
		return;
	}
}
// System.Void baseMethods::getEResources(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1560833635;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t baseMethods_getEResources_m992289761_MetadataUsageId;
extern "C"  void baseMethods_getEResources_m992289761 (baseMethods_t2705876163 * __this, int32_t ___resource0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_getEResources_m992289761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1560833635, /*hidden argument*/NULL);
		NullCheck(L_1);
		Text_t356221433 * L_2 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_1, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		NullCheck(L_0);
		L_0->set_text_19(L_2);
		banmen_t2510752313 * L_3 = __this->get_ban_0();
		banmen_t2510752313 * L_4 = __this->get_ban_0();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_enemyResources_40();
		int32_t L_6 = ___resource0;
		NullCheck(L_3);
		L_3->set_enemyResources_40(((int32_t)((int32_t)L_5+(int32_t)L_6)));
		banmen_t2510752313 * L_7 = __this->get_ban_0();
		NullCheck(L_7);
		Text_t356221433 * L_8 = L_7->get_text_19();
		banmen_t2510752313 * L_9 = __this->get_ban_0();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_enemyResources_40();
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral372029310, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_13);
		return;
	}
}
// System.Void baseMethods::OffTimeBar(System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2496419060;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern Il2CppCodeGenString* _stringLiteral165035981;
extern Il2CppCodeGenString* _stringLiteral2804829999;
extern const uint32_t baseMethods_OffTimeBar_m3958558077_MetadataUsageId;
extern "C"  void baseMethods_OffTimeBar_m3958558077 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_OffTimeBar_m3958558077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2496419060);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2496419060);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = ___l0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral372029313);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = ___r1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral165035981);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral165035981);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3881798623(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = GameObject_Find_m836511350(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Image_t2042527209 * L_13 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		banmen_t2510752313 * L_14 = __this->get_ban_0();
		NullCheck(L_14);
		float L_15 = L_14->get_changeRed_27();
		banmen_t2510752313 * L_16 = __this->get_ban_0();
		NullCheck(L_16);
		float L_17 = L_16->get_changeGreen_28();
		banmen_t2510752313 * L_18 = __this->get_ban_0();
		NullCheck(L_18);
		float L_19 = L_18->get_changeBlue_29();
		banmen_t2510752313 * L_20 = __this->get_ban_0();
		NullCheck(L_20);
		float L_21 = L_20->get_changeAlpha_30();
		Color_t2020392075  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Color__ctor_m1909920690(&L_22, L_15, L_17, L_19, L_21, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_22);
		ObjectU5BU5D_t3614634134* L_23 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral2496419060);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2496419060);
		ObjectU5BU5D_t3614634134* L_24 = L_23;
		int32_t L_25 = ___l0;
		int32_t L_26 = L_25;
		Il2CppObject * L_27 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_27);
		ObjectU5BU5D_t3614634134* L_28 = L_24;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral372029313);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_29 = L_28;
		int32_t L_30 = ___r1;
		int32_t L_31 = L_30;
		Il2CppObject * L_32 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_32);
		ObjectU5BU5D_t3614634134* L_33 = L_29;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, _stringLiteral2804829999);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2804829999);
		String_t* L_34 = String_Concat_m3881798623(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = GameObject_Find_m836511350(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Image_t2042527209 * L_36 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_35, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		banmen_t2510752313 * L_37 = __this->get_ban_0();
		NullCheck(L_37);
		float L_38 = L_37->get_changeRed_27();
		banmen_t2510752313 * L_39 = __this->get_ban_0();
		NullCheck(L_39);
		float L_40 = L_39->get_changeGreen_28();
		banmen_t2510752313 * L_41 = __this->get_ban_0();
		NullCheck(L_41);
		float L_42 = L_41->get_changeBlue_29();
		banmen_t2510752313 * L_43 = __this->get_ban_0();
		NullCheck(L_43);
		float L_44 = L_43->get_changeAlpha_30();
		Color_t2020392075  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Color__ctor_m1909920690(&L_45, L_38, L_40, L_42, L_44, /*hidden argument*/NULL);
		NullCheck(L_36);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_36, L_45);
		return;
	}
}
// System.Void baseMethods::OffTimeBar2(System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2496419060;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern Il2CppCodeGenString* _stringLiteral3333099489;
extern Il2CppCodeGenString* _stringLiteral1347599939;
extern const uint32_t baseMethods_OffTimeBar2_m299894443_MetadataUsageId;
extern "C"  void baseMethods_OffTimeBar2_m299894443 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_OffTimeBar2_m299894443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2496419060);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2496419060);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = ___l0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral372029313);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = ___r1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3333099489);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3333099489);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3881798623(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = GameObject_Find_m836511350(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Image_t2042527209 * L_13 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		banmen_t2510752313 * L_14 = __this->get_ban_0();
		NullCheck(L_14);
		float L_15 = L_14->get_changeRed_27();
		banmen_t2510752313 * L_16 = __this->get_ban_0();
		NullCheck(L_16);
		float L_17 = L_16->get_changeGreen_28();
		banmen_t2510752313 * L_18 = __this->get_ban_0();
		NullCheck(L_18);
		float L_19 = L_18->get_changeBlue_29();
		banmen_t2510752313 * L_20 = __this->get_ban_0();
		NullCheck(L_20);
		float L_21 = L_20->get_changeAlpha_30();
		Color_t2020392075  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Color__ctor_m1909920690(&L_22, L_15, L_17, L_19, L_21, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_22);
		ObjectU5BU5D_t3614634134* L_23 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral2496419060);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2496419060);
		ObjectU5BU5D_t3614634134* L_24 = L_23;
		int32_t L_25 = ___l0;
		int32_t L_26 = L_25;
		Il2CppObject * L_27 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_27);
		ObjectU5BU5D_t3614634134* L_28 = L_24;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral372029313);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_29 = L_28;
		int32_t L_30 = ___r1;
		int32_t L_31 = L_30;
		Il2CppObject * L_32 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_32);
		ObjectU5BU5D_t3614634134* L_33 = L_29;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, _stringLiteral1347599939);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1347599939);
		String_t* L_34 = String_Concat_m3881798623(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = GameObject_Find_m836511350(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Image_t2042527209 * L_36 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_35, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		banmen_t2510752313 * L_37 = __this->get_ban_0();
		NullCheck(L_37);
		float L_38 = L_37->get_changeRed_27();
		banmen_t2510752313 * L_39 = __this->get_ban_0();
		NullCheck(L_39);
		float L_40 = L_39->get_changeGreen_28();
		banmen_t2510752313 * L_41 = __this->get_ban_0();
		NullCheck(L_41);
		float L_42 = L_41->get_changeBlue_29();
		banmen_t2510752313 * L_43 = __this->get_ban_0();
		NullCheck(L_43);
		float L_44 = L_43->get_changeAlpha_30();
		Color_t2020392075  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Color__ctor_m1909920690(&L_45, L_38, L_40, L_42, L_44, /*hidden argument*/NULL);
		NullCheck(L_36);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_36, L_45);
		return;
	}
}
// System.Void baseMethods::OnTimeBar(System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2496419060;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern Il2CppCodeGenString* _stringLiteral165035981;
extern Il2CppCodeGenString* _stringLiteral2804829999;
extern const uint32_t baseMethods_OnTimeBar_m1542858583_MetadataUsageId;
extern "C"  void baseMethods_OnTimeBar_m1542858583 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_OnTimeBar_m1542858583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		baseMethods_setColorBlack_m980638986(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2496419060);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2496419060);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = ___l0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral372029313);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = ___r1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral165035981);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral165035981);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3881798623(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = GameObject_Find_m836511350(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Image_t2042527209 * L_13 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		banmen_t2510752313 * L_14 = __this->get_ban_0();
		NullCheck(L_14);
		float L_15 = L_14->get_changeRed_27();
		banmen_t2510752313 * L_16 = __this->get_ban_0();
		NullCheck(L_16);
		float L_17 = L_16->get_changeGreen_28();
		banmen_t2510752313 * L_18 = __this->get_ban_0();
		NullCheck(L_18);
		float L_19 = L_18->get_changeBlue_29();
		banmen_t2510752313 * L_20 = __this->get_ban_0();
		NullCheck(L_20);
		float L_21 = L_20->get_changeAlpha_30();
		Color_t2020392075  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Color__ctor_m1909920690(&L_22, L_15, L_17, L_19, L_21, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_22);
		baseMethods_setColorGreen_m2130263350(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_23 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral2496419060);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2496419060);
		ObjectU5BU5D_t3614634134* L_24 = L_23;
		int32_t L_25 = ___l0;
		int32_t L_26 = L_25;
		Il2CppObject * L_27 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_27);
		ObjectU5BU5D_t3614634134* L_28 = L_24;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral372029313);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_29 = L_28;
		int32_t L_30 = ___r1;
		int32_t L_31 = L_30;
		Il2CppObject * L_32 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_32);
		ObjectU5BU5D_t3614634134* L_33 = L_29;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, _stringLiteral2804829999);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2804829999);
		String_t* L_34 = String_Concat_m3881798623(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = GameObject_Find_m836511350(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Image_t2042527209 * L_36 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_35, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		banmen_t2510752313 * L_37 = __this->get_ban_0();
		NullCheck(L_37);
		float L_38 = L_37->get_changeRed_27();
		banmen_t2510752313 * L_39 = __this->get_ban_0();
		NullCheck(L_39);
		float L_40 = L_39->get_changeGreen_28();
		banmen_t2510752313 * L_41 = __this->get_ban_0();
		NullCheck(L_41);
		float L_42 = L_41->get_changeBlue_29();
		banmen_t2510752313 * L_43 = __this->get_ban_0();
		NullCheck(L_43);
		float L_44 = L_43->get_changeAlpha_30();
		Color_t2020392075  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Color__ctor_m1909920690(&L_45, L_38, L_40, L_42, L_44, /*hidden argument*/NULL);
		NullCheck(L_36);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_36, L_45);
		return;
	}
}
// System.Void baseMethods::OnTimeBar2(System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2496419060;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern Il2CppCodeGenString* _stringLiteral3333099489;
extern Il2CppCodeGenString* _stringLiteral1347599939;
extern const uint32_t baseMethods_OnTimeBar2_m345907837_MetadataUsageId;
extern "C"  void baseMethods_OnTimeBar2_m345907837 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_OnTimeBar2_m345907837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		baseMethods_setColorBlack_m980638986(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral2496419060);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2496419060);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = ___l0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral372029313);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = ___r1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3333099489);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3333099489);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3881798623(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = GameObject_Find_m836511350(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Image_t2042527209 * L_13 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		banmen_t2510752313 * L_14 = __this->get_ban_0();
		NullCheck(L_14);
		float L_15 = L_14->get_changeRed_27();
		banmen_t2510752313 * L_16 = __this->get_ban_0();
		NullCheck(L_16);
		float L_17 = L_16->get_changeGreen_28();
		banmen_t2510752313 * L_18 = __this->get_ban_0();
		NullCheck(L_18);
		float L_19 = L_18->get_changeBlue_29();
		banmen_t2510752313 * L_20 = __this->get_ban_0();
		NullCheck(L_20);
		float L_21 = L_20->get_changeAlpha_30();
		Color_t2020392075  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Color__ctor_m1909920690(&L_22, L_15, L_17, L_19, L_21, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_22);
		baseMethods_setColorGreen_m2130263350(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_23 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral2496419060);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2496419060);
		ObjectU5BU5D_t3614634134* L_24 = L_23;
		int32_t L_25 = ___l0;
		int32_t L_26 = L_25;
		Il2CppObject * L_27 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_27);
		ObjectU5BU5D_t3614634134* L_28 = L_24;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteral372029313);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_29 = L_28;
		int32_t L_30 = ___r1;
		int32_t L_31 = L_30;
		Il2CppObject * L_32 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_32);
		ObjectU5BU5D_t3614634134* L_33 = L_29;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, _stringLiteral1347599939);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1347599939);
		String_t* L_34 = String_Concat_m3881798623(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = GameObject_Find_m836511350(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Image_t2042527209 * L_36 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_35, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		banmen_t2510752313 * L_37 = __this->get_ban_0();
		NullCheck(L_37);
		float L_38 = L_37->get_changeRed_27();
		banmen_t2510752313 * L_39 = __this->get_ban_0();
		NullCheck(L_39);
		float L_40 = L_39->get_changeGreen_28();
		banmen_t2510752313 * L_41 = __this->get_ban_0();
		NullCheck(L_41);
		float L_42 = L_41->get_changeBlue_29();
		banmen_t2510752313 * L_43 = __this->get_ban_0();
		NullCheck(L_43);
		float L_44 = L_43->get_changeAlpha_30();
		Color_t2020392075  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Color__ctor_m1909920690(&L_45, L_38, L_40, L_42, L_44, /*hidden argument*/NULL);
		NullCheck(L_36);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_36, L_45);
		return;
	}
}
// System.Void baseMethods::printColorBottom(System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3766742095;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern Il2CppCodeGenString* _stringLiteral2347644584;
extern const uint32_t baseMethods_printColorBottom_m2694251173_MetadataUsageId;
extern "C"  void baseMethods_printColorBottom_m2694251173 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_printColorBottom_m2694251173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3766742095);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3766742095);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = ___l0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral372029313);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = ___r1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral2347644584);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral2347644584);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m3881798623(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = GameObject_Find_m836511350(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Image_t2042527209 * L_13 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		banmen_t2510752313 * L_14 = __this->get_ban_0();
		NullCheck(L_14);
		float L_15 = L_14->get_changeRed_27();
		banmen_t2510752313 * L_16 = __this->get_ban_0();
		NullCheck(L_16);
		float L_17 = L_16->get_changeGreen_28();
		banmen_t2510752313 * L_18 = __this->get_ban_0();
		NullCheck(L_18);
		float L_19 = L_18->get_changeBlue_29();
		banmen_t2510752313 * L_20 = __this->get_ban_0();
		NullCheck(L_20);
		float L_21 = L_20->get_changeAlpha_30();
		Color_t2020392075  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Color__ctor_m1909920690(&L_22, L_15, L_17, L_19, L_21, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_22);
		return;
	}
}
// System.Void baseMethods::printColorAbove(System.Int32,System.Int32)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3766742095;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern const uint32_t baseMethods_printColorAbove_m553988101_MetadataUsageId;
extern "C"  void baseMethods_printColorAbove_m553988101 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_printColorAbove_m553988101_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3766742095);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3766742095);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = ___l0;
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral372029313);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int32_t L_7 = ___r1;
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3881798623(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = GameObject_Find_m836511350(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Image_t2042527209 * L_12 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_11, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		banmen_t2510752313 * L_13 = __this->get_ban_0();
		NullCheck(L_13);
		float L_14 = L_13->get_changeRed_27();
		banmen_t2510752313 * L_15 = __this->get_ban_0();
		NullCheck(L_15);
		float L_16 = L_15->get_changeGreen_28();
		banmen_t2510752313 * L_17 = __this->get_ban_0();
		NullCheck(L_17);
		float L_18 = L_17->get_changeBlue_29();
		banmen_t2510752313 * L_19 = __this->get_ban_0();
		NullCheck(L_19);
		float L_20 = L_19->get_changeAlpha_30();
		Color_t2020392075  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Color__ctor_m1909920690(&L_21, L_14, L_16, L_18, L_20, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_21);
		return;
	}
}
// System.Void baseMethods::allPrintColorBottom()
extern "C"  void baseMethods_allPrintColorBottom_m2081970466 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_0029;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_001e;
	}

IL_000e:
	{
		int32_t L_0 = V_0;
		int32_t L_1 = V_1;
		baseMethods_printColorBottom_m2694251173(__this, ((int32_t)((int32_t)L_0+(int32_t)1)), ((int32_t)((int32_t)L_1+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		V_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) < ((int32_t)6)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)6)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void baseMethods::halfPrintColorBottom(System.Boolean)
extern "C"  void baseMethods_halfPrintColorBottom_m2366856657 (baseMethods_t2705876163 * __this, bool ___e0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		bool L_0 = ___e0;
		if (!L_0)
		{
			goto IL_003b;
		}
	}
	{
		V_0 = 0;
		goto IL_002f;
	}

IL_000d:
	{
		V_1 = 0;
		goto IL_0024;
	}

IL_0014:
	{
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		baseMethods_printColorBottom_m2694251173(__this, ((int32_t)((int32_t)L_1+(int32_t)1)), ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_3 = V_1;
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) < ((int32_t)3)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)6)))
		{
			goto IL_000d;
		}
	}
	{
		goto IL_006b;
	}

IL_003b:
	{
		V_2 = 0;
		goto IL_0064;
	}

IL_0042:
	{
		V_3 = 0;
		goto IL_0059;
	}

IL_0049:
	{
		int32_t L_7 = V_2;
		int32_t L_8 = V_3;
		baseMethods_printColorBottom_m2694251173(__this, ((int32_t)((int32_t)L_7+(int32_t)1)), ((int32_t)((int32_t)L_8+(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0059:
	{
		int32_t L_10 = V_3;
		if ((((int32_t)L_10) < ((int32_t)3)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0064:
	{
		int32_t L_12 = V_2;
		if ((((int32_t)L_12) < ((int32_t)6)))
		{
			goto IL_0042;
		}
	}

IL_006b:
	{
		return;
	}
}
// System.Void baseMethods::setColorWhite()
extern "C"  void baseMethods_setColorWhite_m2690390344 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		L_0->set_changeRed_27((1.0f));
		banmen_t2510752313 * L_1 = __this->get_ban_0();
		NullCheck(L_1);
		L_1->set_changeGreen_28((1.0f));
		banmen_t2510752313 * L_2 = __this->get_ban_0();
		NullCheck(L_2);
		L_2->set_changeBlue_29((1.0f));
		banmen_t2510752313 * L_3 = __this->get_ban_0();
		NullCheck(L_3);
		L_3->set_changeAlpha_30((1.0f));
		return;
	}
}
// System.Void baseMethods::setColorBlack()
extern "C"  void baseMethods_setColorBlack_m980638986 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		L_0->set_changeRed_27((0.0f));
		banmen_t2510752313 * L_1 = __this->get_ban_0();
		NullCheck(L_1);
		L_1->set_changeGreen_28((0.0f));
		banmen_t2510752313 * L_2 = __this->get_ban_0();
		NullCheck(L_2);
		L_2->set_changeBlue_29((0.0f));
		banmen_t2510752313 * L_3 = __this->get_ban_0();
		NullCheck(L_3);
		L_3->set_changeAlpha_30((1.0f));
		return;
	}
}
// System.Void baseMethods::setColorRed()
extern "C"  void baseMethods_setColorRed_m3950151350 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		L_0->set_changeRed_27((1.0f));
		banmen_t2510752313 * L_1 = __this->get_ban_0();
		NullCheck(L_1);
		L_1->set_changeGreen_28((0.0f));
		banmen_t2510752313 * L_2 = __this->get_ban_0();
		NullCheck(L_2);
		L_2->set_changeBlue_29((0.0f));
		banmen_t2510752313 * L_3 = __this->get_ban_0();
		NullCheck(L_3);
		L_3->set_changeAlpha_30((1.0f));
		return;
	}
}
// System.Void baseMethods::setColorBlue()
extern "C"  void baseMethods_setColorBlue_m4082331027 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		L_0->set_changeRed_27((0.0f));
		banmen_t2510752313 * L_1 = __this->get_ban_0();
		NullCheck(L_1);
		L_1->set_changeGreen_28((0.0f));
		banmen_t2510752313 * L_2 = __this->get_ban_0();
		NullCheck(L_2);
		L_2->set_changeBlue_29((1.0f));
		banmen_t2510752313 * L_3 = __this->get_ban_0();
		NullCheck(L_3);
		L_3->set_changeAlpha_30((1.0f));
		return;
	}
}
// System.Void baseMethods::setColorGreen()
extern "C"  void baseMethods_setColorGreen_m2130263350 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		L_0->set_changeRed_27((0.0f));
		banmen_t2510752313 * L_1 = __this->get_ban_0();
		NullCheck(L_1);
		L_1->set_changeGreen_28((1.0f));
		banmen_t2510752313 * L_2 = __this->get_ban_0();
		NullCheck(L_2);
		L_2->set_changeBlue_29((0.0f));
		banmen_t2510752313 * L_3 = __this->get_ban_0();
		NullCheck(L_3);
		L_3->set_changeAlpha_30((1.0f));
		return;
	}
}
// System.Void baseMethods::setColorClear()
extern "C"  void baseMethods_setColorClear_m4166068504 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		L_0->set_changeAlpha_30((0.0f));
		return;
	}
}
// System.Void baseMethods::setRedWhite()
extern "C"  void baseMethods_setRedWhite_m3951861276 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		baseMethods_setColorWhite_m2690390344(__this, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_004a;
	}

IL_000d:
	{
		V_1 = 0;
		goto IL_003f;
	}

IL_0014:
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		BooleanU5BU2CU5D_t3568034316* L_1 = L_0->get_canTouchPlace_4();
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_1);
		bool L_4 = ((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_1)->GetAt(((int32_t)((int32_t)L_2+(int32_t)1)), ((int32_t)((int32_t)L_3+(int32_t)1)));
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		baseMethods_printColorBottom_m2694251173(__this, ((int32_t)((int32_t)L_5+(int32_t)1)), ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) < ((int32_t)6)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) < ((int32_t)6)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}
}
// System.Void baseMethods::lineRow(System.Int32,System.Int32)
extern "C"  void baseMethods_lineRow_m2784445234 (baseMethods_t2705876163 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		int32_t L_1 = ___l0;
		NullCheck(L_0);
		L_0->set_line_20(L_1);
		banmen_t2510752313 * L_2 = __this->get_ban_0();
		int32_t L_3 = ___r1;
		NullCheck(L_2);
		L_2->set_row_21(L_3);
		return;
	}
}
// System.Void baseMethods::canTouchAll(System.Boolean)
extern "C"  void baseMethods_canTouchAll_m280133149 (baseMethods_t2705876163 * __this, bool ___TF0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_0034;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_0029;
	}

IL_000e:
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		BooleanU5BU2CU5D_t3568034316* L_1 = L_0->get_canTouchPlace_4();
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		bool L_4 = ___TF0;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_1);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_1)->SetAt(((int32_t)((int32_t)L_2+(int32_t)1)), ((int32_t)((int32_t)L_3+(int32_t)1)), L_4);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) < ((int32_t)6)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) < ((int32_t)6)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void baseMethods::canTouchArrangeMode()
extern "C"  void baseMethods_canTouchArrangeMode_m1634389688 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		goto IL_005d;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_0029;
	}

IL_000e:
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		BooleanU5BU2CU5D_t3568034316* L_1 = L_0->get_canTouchPlace_4();
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_1);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_1)->SetAt(((int32_t)((int32_t)L_2+(int32_t)1)), ((int32_t)((int32_t)L_3+(int32_t)1)), (bool)0);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) < ((int32_t)4)))
		{
			goto IL_000e;
		}
	}
	{
		V_2 = 0;
		goto IL_0052;
	}

IL_0037:
	{
		banmen_t2510752313 * L_6 = __this->get_ban_0();
		NullCheck(L_6);
		BooleanU5BU2CU5D_t3568034316* L_7 = L_6->get_canTouchPlace_4();
		int32_t L_8 = V_0;
		int32_t L_9 = V_2;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_7);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_7)->SetAt(((int32_t)((int32_t)L_8+(int32_t)1)), ((int32_t)((int32_t)L_9+(int32_t)5)), (bool)1);
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0052:
	{
		int32_t L_11 = V_2;
		if ((((int32_t)L_11) < ((int32_t)2)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) < ((int32_t)6)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void baseMethods::canTouchHalf()
extern "C"  void baseMethods_canTouchHalf_m1396259078 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_0034;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_0029;
	}

IL_000e:
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		BooleanU5BU2CU5D_t3568034316* L_1 = L_0->get_canTouchPlace_4();
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_1);
		((BooleanU5BU2CU5D_t3568034316*)(BooleanU5BU2CU5D_t3568034316*)L_1)->SetAt(((int32_t)((int32_t)L_2+(int32_t)1)), ((int32_t)((int32_t)L_3+(int32_t)4)), (bool)1);
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) < ((int32_t)3)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)6)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void baseMethods::cloudy()
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1215038349;
extern Il2CppCodeGenString* _stringLiteral816499700;
extern const uint32_t baseMethods_cloudy_m3964171594_MetadataUsageId;
extern "C"  void baseMethods_cloudy_m3964171594 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_cloudy_m3964171594_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		baseMethods_ClearEPlace_m1537656273(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1215038349, /*hidden argument*/NULL);
		NullCheck(L_0);
		Image_t2042527209 * L_1 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_0, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral816499700, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		NullCheck(L_1);
		Image_set_sprite_m1800056820(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void baseMethods::nonSatelite()
extern const MethodInfo* GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1215038349;
extern const uint32_t baseMethods_nonSatelite_m140178688_MetadataUsageId;
extern "C"  void baseMethods_nonSatelite_m140178688 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (baseMethods_nonSatelite_m140178688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		baseMethods_ClearEPlace_m1537656273(__this, /*hidden argument*/NULL);
		baseMethods_setColorClear_m4166068504(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1215038349, /*hidden argument*/NULL);
		NullCheck(L_0);
		Image_t2042527209 * L_1 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_0, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_MethodInfo_var);
		banmen_t2510752313 * L_2 = __this->get_ban_0();
		NullCheck(L_2);
		float L_3 = L_2->get_changeRed_27();
		banmen_t2510752313 * L_4 = __this->get_ban_0();
		NullCheck(L_4);
		float L_5 = L_4->get_changeGreen_28();
		banmen_t2510752313 * L_6 = __this->get_ban_0();
		NullCheck(L_6);
		float L_7 = L_6->get_changeBlue_29();
		banmen_t2510752313 * L_8 = __this->get_ban_0();
		NullCheck(L_8);
		float L_9 = L_8->get_changeAlpha_30();
		Color_t2020392075  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color__ctor_m1909920690(&L_10, L_3, L_5, L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_1, L_10);
		return;
	}
}
// System.Void baseMethods::ClearEPlace()
extern "C"  void baseMethods_ClearEPlace_m1537656273 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		baseMethods_setColorClear_m4166068504(__this, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_0047;
	}

IL_000d:
	{
		V_1 = 0;
		goto IL_003c;
	}

IL_0014:
	{
		int32_t L_0 = V_1;
		int32_t L_1 = V_0;
		baseMethods_printColorBottom_m2694251173(__this, ((int32_t)((int32_t)L_0+(int32_t)1)), ((int32_t)((int32_t)L_1+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		baseMethods_printColorAbove_m553988101(__this, ((int32_t)((int32_t)L_2+(int32_t)1)), ((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		baseMethods_OffTimeBar_m3958558077(__this, ((int32_t)((int32_t)L_4+(int32_t)1)), ((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) < ((int32_t)6)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) < ((int32_t)3)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}
}
// System.Void baseMethods::OnSatelite()
extern "C"  void baseMethods_OnSatelite_m2024278172 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		L_0->set_satelite_33((bool)1);
		return;
	}
}
// System.Void baseMethods::DestroySatelite()
extern "C"  void baseMethods_DestroySatelite_m3304756467 (baseMethods_t2705876163 * __this, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		L_0->set_Esatelite_34((bool)0);
		return;
	}
}
// System.Void createCanvas::.ctor()
extern "C"  void createCanvas__ctor_m1636400783 (createCanvas_t3354593624 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void createCanvas::Start()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3523698745;
extern Il2CppCodeGenString* _stringLiteral2821606529;
extern Il2CppCodeGenString* _stringLiteral717002942;
extern Il2CppCodeGenString* _stringLiteral702671978;
extern Il2CppCodeGenString* _stringLiteral533385193;
extern Il2CppCodeGenString* _stringLiteral42465407;
extern const uint32_t createCanvas_Start_m3083297603_MetadataUsageId;
extern "C"  void createCanvas_Start_m3083297603 (createCanvas_t3354593624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (createCanvas_Start_m3083297603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_canvasNum_2();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002a;
		}
		if (L_1 == 1)
		{
			goto IL_0050;
		}
		if (L_1 == 2)
		{
			goto IL_0076;
		}
		if (L_1 == 3)
		{
			goto IL_009c;
		}
		if (L_1 == 4)
		{
			goto IL_00c2;
		}
		if (L_1 == 5)
		{
			goto IL_00e8;
		}
	}
	{
		goto IL_010e;
	}

IL_002a:
	{
		Object_t1021602117 * L_2 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral3523698745, /*hidden argument*/NULL);
		__this->set_prefab_3(((GameObject_t1756533147 *)CastclassSealed(L_2, GameObject_t1756533147_il2cpp_TypeInfo_var)));
		GameObject_t1756533147 * L_3 = __this->get_prefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		goto IL_010e;
	}

IL_0050:
	{
		Object_t1021602117 * L_4 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral2821606529, /*hidden argument*/NULL);
		__this->set_prefab_3(((GameObject_t1756533147 *)CastclassSealed(L_4, GameObject_t1756533147_il2cpp_TypeInfo_var)));
		GameObject_t1756533147 * L_5 = __this->get_prefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_5, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		goto IL_010e;
	}

IL_0076:
	{
		Object_t1021602117 * L_6 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral717002942, /*hidden argument*/NULL);
		__this->set_prefab_3(((GameObject_t1756533147 *)CastclassSealed(L_6, GameObject_t1756533147_il2cpp_TypeInfo_var)));
		GameObject_t1756533147 * L_7 = __this->get_prefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_7, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		goto IL_010e;
	}

IL_009c:
	{
		Object_t1021602117 * L_8 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral702671978, /*hidden argument*/NULL);
		__this->set_prefab_3(((GameObject_t1756533147 *)CastclassSealed(L_8, GameObject_t1756533147_il2cpp_TypeInfo_var)));
		GameObject_t1756533147 * L_9 = __this->get_prefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_9, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		goto IL_010e;
	}

IL_00c2:
	{
		Object_t1021602117 * L_10 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral533385193, /*hidden argument*/NULL);
		__this->set_prefab_3(((GameObject_t1756533147 *)CastclassSealed(L_10, GameObject_t1756533147_il2cpp_TypeInfo_var)));
		GameObject_t1756533147 * L_11 = __this->get_prefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_11, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		goto IL_010e;
	}

IL_00e8:
	{
		Object_t1021602117 * L_12 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral42465407, /*hidden argument*/NULL);
		__this->set_prefab_3(((GameObject_t1756533147 *)CastclassSealed(L_12, GameObject_t1756533147_il2cpp_TypeInfo_var)));
		GameObject_t1756533147 * L_13 = __this->get_prefab_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_13, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		goto IL_010e;
	}

IL_010e:
	{
		return;
	}
}
// System.Void createCanvas::Update()
extern "C"  void createCanvas_Update_m656525814 (createCanvas_t3354593624 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void createSingleton::.ctor()
extern "C"  void createSingleton__ctor_m736849240 (createSingleton_t313015705 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void createSingleton::Start()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1135039634;
extern const uint32_t createSingleton_Start_m3605344032_MetadataUsageId;
extern "C"  void createSingleton_Start_m3605344032 (createSingleton_t313015705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (createSingleton_Start_m3605344032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral1135039634, /*hidden argument*/NULL);
		V_0 = ((GameObject_t1756533147 *)CastclassSealed(L_0, GameObject_t1756533147_il2cpp_TypeInfo_var));
		GameObject_t1756533147 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_1, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		return;
	}
}
// System.Void createSingleton::Update()
extern "C"  void createSingleton_Update_m4196438813 (createSingleton_t313015705 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Data::.ctor()
extern "C"  void Data__ctor_m613971867 (Data_t3569509720 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Data::Start()
extern "C"  void Data_Start_m260840599 (Data_t3569509720 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Data::Update()
extern "C"  void Data_Update_m3009678490 (Data_t3569509720 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Distribution::.ctor()
extern "C"  void Distribution__ctor_m664235853 (Distribution_t1524299538 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Distribution::Start()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObjectU5BU2CU5D_t3057952155_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisCanvas_t209405766_m2311451932_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisGraphicRaycaster_t410733016_m2075178258_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1047780110;
extern Il2CppCodeGenString* _stringLiteral829804034;
extern Il2CppCodeGenString* _stringLiteral1544959998;
extern Il2CppCodeGenString* _stringLiteral2461699058;
extern Il2CppCodeGenString* _stringLiteral661134445;
extern Il2CppCodeGenString* _stringLiteral1004061230;
extern Il2CppCodeGenString* _stringLiteral372029313;
extern const uint32_t Distribution_Start_m3147594557_MetadataUsageId;
extern "C"  void Distribution_Start_m3147594557 (Distribution_t1524299538 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Distribution_Start_m3147594557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Canvas_t209405766 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	GameObject_t1756533147 * V_3 = NULL;
	GameObject_t1756533147 * V_4 = NULL;
	GameObject_t1756533147 * V_5 = NULL;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	{
		GameObject_t1756533147 * L_0 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_0, _stringLiteral1047780110, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		NullCheck(L_1);
		Canvas_t209405766 * L_2 = GameObject_AddComponent_TisCanvas_t209405766_m2311451932(L_1, /*hidden argument*/GameObject_AddComponent_TisCanvas_t209405766_m2311451932_MethodInfo_var);
		V_1 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		GameObject_AddComponent_TisGraphicRaycaster_t410733016_m2075178258(L_3, /*hidden argument*/GameObject_AddComponent_TisGraphicRaycaster_t410733016_m2075178258_MethodInfo_var);
		Canvas_t209405766 * L_4 = V_1;
		NullCheck(L_4);
		Canvas_set_renderMode_m4114781335(L_4, 0, /*hidden argument*/NULL);
		Object_t1021602117 * L_5 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral829804034, /*hidden argument*/NULL);
		V_2 = ((GameObject_t1756533147 *)CastclassSealed(L_5, GameObject_t1756533147_il2cpp_TypeInfo_var));
		Object_t1021602117 * L_6 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral1544959998, /*hidden argument*/NULL);
		V_3 = ((GameObject_t1756533147 *)CastclassSealed(L_6, GameObject_t1756533147_il2cpp_TypeInfo_var));
		Object_t1021602117 * L_7 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral2461699058, /*hidden argument*/NULL);
		V_4 = ((GameObject_t1756533147 *)CastclassSealed(L_7, GameObject_t1756533147_il2cpp_TypeInfo_var));
		Object_t1021602117 * L_8 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral661134445, /*hidden argument*/NULL);
		V_5 = ((GameObject_t1756533147 *)CastclassSealed(L_8, GameObject_t1756533147_il2cpp_TypeInfo_var));
		int32_t L_9 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = (((float)((float)L_9)));
		float L_10 = V_6;
		V_7 = ((float)((float)L_10/(float)(6.0f)));
		int32_t L_11 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_8 = (((float)((float)L_11)));
		GameObject_t1756533147 * L_12 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_13 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_12, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		__this->set_prefabBackGround_5(L_13);
		GameObject_t1756533147 * L_14 = __this->get_prefabBackGround_5();
		NullCheck(L_14);
		Transform_t3275118058 * L_15 = GameObject_get_transform_m909382139(L_14, /*hidden argument*/NULL);
		Canvas_t209405766 * L_16 = V_1;
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_SetParent_m1963830867(L_15, L_17, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = V_3;
		GameObject_t1756533147 * L_19 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_18, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		__this->set_prefabResource_3(L_19);
		GameObject_t1756533147 * L_20 = __this->get_prefabResource_3();
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		Canvas_t209405766 * L_22 = V_1;
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_SetParent_m1963830867(L_21, L_23, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_24 = __this->get_prefabResource_3();
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = GameObject_get_transform_m909382139(L_24, /*hidden argument*/NULL);
		float L_26 = V_7;
		float L_27 = V_8;
		float L_28 = V_7;
		Vector2_t2243707579  L_29;
		memset(&L_29, 0, sizeof(L_29));
		Vector2__ctor_m3067419446(&L_29, L_26, ((float)((float)((float)((float)L_27/(float)(3.0f)))-(float)L_28)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_30 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_position_m2469242620(L_25, L_30, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_31 = V_3;
		GameObject_t1756533147 * L_32 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_31, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		__this->set_prefabEResource_4(L_32);
		GameObject_t1756533147 * L_33 = __this->get_prefabEResource_4();
		NullCheck(L_33);
		Transform_t3275118058 * L_34 = GameObject_get_transform_m909382139(L_33, /*hidden argument*/NULL);
		Canvas_t209405766 * L_35 = V_1;
		NullCheck(L_35);
		Transform_t3275118058 * L_36 = Component_get_transform_m2697483695(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_SetParent_m1963830867(L_34, L_36, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_37 = __this->get_prefabEResource_4();
		NullCheck(L_37);
		Transform_t3275118058 * L_38 = GameObject_get_transform_m909382139(L_37, /*hidden argument*/NULL);
		float L_39 = V_7;
		float L_40 = V_8;
		float L_41 = V_7;
		Vector2_t2243707579  L_42;
		memset(&L_42, 0, sizeof(L_42));
		Vector2__ctor_m3067419446(&L_42, L_39, ((float)((float)((float)((float)L_40/(float)(3.0f)))+(float)((float)((float)L_41*(float)(6.0f))))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_43 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_set_position_m2469242620(L_38, L_43, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_44 = V_5;
		GameObject_t1756533147 * L_45 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_44, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		__this->set_prefabRefresh_6(L_45);
		GameObject_t1756533147 * L_46 = __this->get_prefabRefresh_6();
		NullCheck(L_46);
		Transform_t3275118058 * L_47 = GameObject_get_transform_m909382139(L_46, /*hidden argument*/NULL);
		Canvas_t209405766 * L_48 = V_1;
		NullCheck(L_48);
		Transform_t3275118058 * L_49 = Component_get_transform_m2697483695(L_48, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_SetParent_m1963830867(L_47, L_49, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_50 = __this->get_prefabRefresh_6();
		NullCheck(L_50);
		Transform_t3275118058 * L_51 = GameObject_get_transform_m909382139(L_50, /*hidden argument*/NULL);
		float L_52 = V_7;
		float L_53 = V_8;
		Vector2_t2243707579  L_54;
		memset(&L_54, 0, sizeof(L_54));
		Vector2__ctor_m3067419446(&L_54, ((float)((float)L_52*(float)(6.0f))), ((float)((float)((float)((float)L_53/(float)(3.0f)))-(float)(10.0f))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_55 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_set_position_m2469242620(L_51, L_55, /*hidden argument*/NULL);
		il2cpp_array_size_t L_57[] = { (il2cpp_array_size_t)6, (il2cpp_array_size_t)6 };
		GameObjectU5BU2CU5D_t3057952155* L_56 = (GameObjectU5BU2CU5D_t3057952155*)GenArrayNew(GameObjectU5BU2CU5D_t3057952155_il2cpp_TypeInfo_var, L_57);
		__this->set_prefab_2((GameObjectU5BU2CU5D_t3057952155*)L_56);
		V_9 = 0;
		goto IL_027c;
	}

IL_01a3:
	{
		V_10 = 0;
		goto IL_026e;
	}

IL_01ab:
	{
		GameObjectU5BU2CU5D_t3057952155* L_58 = __this->get_prefab_2();
		int32_t L_59 = V_9;
		int32_t L_60 = V_10;
		GameObject_t1756533147 * L_61 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_62 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_61, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		NullCheck((GameObjectU5BU2CU5D_t3057952155*)(GameObjectU5BU2CU5D_t3057952155*)L_58);
		((GameObjectU5BU2CU5D_t3057952155*)(GameObjectU5BU2CU5D_t3057952155*)L_58)->SetAt(L_59, L_60, L_62);
		GameObjectU5BU2CU5D_t3057952155* L_63 = __this->get_prefab_2();
		int32_t L_64 = V_9;
		int32_t L_65 = V_10;
		NullCheck((GameObjectU5BU2CU5D_t3057952155*)(GameObjectU5BU2CU5D_t3057952155*)L_63);
		GameObject_t1756533147 * L_66 = ((GameObjectU5BU2CU5D_t3057952155*)(GameObjectU5BU2CU5D_t3057952155*)L_63)->GetAt(L_64, L_65);
		NullCheck(L_66);
		Transform_t3275118058 * L_67 = GameObject_get_transform_m909382139(L_66, /*hidden argument*/NULL);
		Canvas_t209405766 * L_68 = V_1;
		NullCheck(L_68);
		Transform_t3275118058 * L_69 = Component_get_transform_m2697483695(L_68, /*hidden argument*/NULL);
		NullCheck(L_67);
		Transform_SetParent_m1963830867(L_67, L_69, (bool)0, /*hidden argument*/NULL);
		GameObjectU5BU2CU5D_t3057952155* L_70 = __this->get_prefab_2();
		int32_t L_71 = V_9;
		int32_t L_72 = V_10;
		NullCheck((GameObjectU5BU2CU5D_t3057952155*)(GameObjectU5BU2CU5D_t3057952155*)L_70);
		GameObject_t1756533147 * L_73 = ((GameObjectU5BU2CU5D_t3057952155*)(GameObjectU5BU2CU5D_t3057952155*)L_70)->GetAt(L_71, L_72);
		NullCheck(L_73);
		Transform_t3275118058 * L_74 = GameObject_get_transform_m909382139(L_73, /*hidden argument*/NULL);
		float L_75 = V_7;
		int32_t L_76 = V_9;
		float L_77 = V_7;
		float L_78 = V_7;
		int32_t L_79 = V_10;
		float L_80 = V_8;
		Vector2_t2243707579  L_81;
		memset(&L_81, 0, sizeof(L_81));
		Vector2__ctor_m3067419446(&L_81, ((float)((float)((float)((float)L_75*(float)(((float)((float)L_76)))))+(float)((float)((float)L_77/(float)(2.0f))))), ((float)((float)((float)((float)L_78*(float)(((float)((float)L_79)))))+(float)((float)((float)L_80/(float)(3.0f))))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_82 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_81, /*hidden argument*/NULL);
		NullCheck(L_74);
		Transform_set_position_m2469242620(L_74, L_82, /*hidden argument*/NULL);
		GameObjectU5BU2CU5D_t3057952155* L_83 = __this->get_prefab_2();
		int32_t L_84 = V_9;
		int32_t L_85 = V_10;
		NullCheck((GameObjectU5BU2CU5D_t3057952155*)(GameObjectU5BU2CU5D_t3057952155*)L_83);
		GameObject_t1756533147 * L_86 = ((GameObjectU5BU2CU5D_t3057952155*)(GameObjectU5BU2CU5D_t3057952155*)L_83)->GetAt(L_84, L_85);
		ObjectU5BU5D_t3614634134* L_87 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_87);
		ArrayElementTypeCheck (L_87, _stringLiteral1004061230);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1004061230);
		ObjectU5BU5D_t3614634134* L_88 = L_87;
		int32_t L_89 = V_9;
		int32_t L_90 = ((int32_t)((int32_t)L_89+(int32_t)1));
		Il2CppObject * L_91 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_90);
		NullCheck(L_88);
		ArrayElementTypeCheck (L_88, L_91);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_91);
		ObjectU5BU5D_t3614634134* L_92 = L_88;
		NullCheck(L_92);
		ArrayElementTypeCheck (L_92, _stringLiteral372029313);
		(L_92)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral372029313);
		ObjectU5BU5D_t3614634134* L_93 = L_92;
		int32_t L_94 = V_10;
		int32_t L_95 = ((int32_t)((int32_t)L_94+(int32_t)1));
		Il2CppObject * L_96 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_95);
		NullCheck(L_93);
		ArrayElementTypeCheck (L_93, L_96);
		(L_93)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_96);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_97 = String_Concat_m3881798623(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		NullCheck(L_86);
		Object_set_name_m4157836998(L_86, L_97, /*hidden argument*/NULL);
		int32_t L_98 = V_10;
		V_10 = ((int32_t)((int32_t)L_98+(int32_t)1));
	}

IL_026e:
	{
		int32_t L_99 = V_10;
		if ((((int32_t)L_99) < ((int32_t)6)))
		{
			goto IL_01ab;
		}
	}
	{
		int32_t L_100 = V_9;
		V_9 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_027c:
	{
		int32_t L_101 = V_9;
		if ((((int32_t)L_101) < ((int32_t)6)))
		{
			goto IL_01a3;
		}
	}
	{
		return;
	}
}
// System.Void Distribution::Update()
extern "C"  void Distribution_Update_m1453560724 (Distribution_t1524299538 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Drone::.ctor()
extern "C"  void Drone__ctor_m960290629 (Drone_t860454962 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Drone::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2230921881;
extern Il2CppCodeGenString* _stringLiteral3981521668;
extern const uint32_t Drone_Init_m760530005_MetadataUsageId;
extern "C"  void Drone_Init_m760530005 (Drone_t860454962 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Drone_Init_m760530005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2230921881, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral3981521668, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(6);
		((Koma_t3011766596 *)__this)->set_resources_3(3);
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)30));
		return;
	}
}
// System.Void Drone::Move(System.Int32,System.Int32)
extern "C"  void Drone_Move_m1067477320 (Drone_t860454962 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		L_0->set_wall_32((bool)1);
		V_0 = 1;
		goto IL_0026;
	}

IL_0013:
	{
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_2 = ___l0;
		int32_t L_3 = ___r1;
		int32_t L_4 = V_0;
		NullCheck(L_1);
		banmen_isThereFuti_m1442766142(L_1, L_2, ((int32_t)((int32_t)L_3-(int32_t)L_4)), /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0026:
	{
		banmen_t2510752313 * L_6 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_6);
		bool L_7 = L_6->get_wall_32();
		if (L_7)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = 1;
		banmen_t2510752313 * L_8 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_8);
		L_8->set_wall_32((bool)1);
		goto IL_005c;
	}

IL_0049:
	{
		banmen_t2510752313 * L_9 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_10 = ___l0;
		int32_t L_11 = ___r1;
		int32_t L_12 = V_0;
		NullCheck(L_9);
		banmen_isThereFuti_m1442766142(L_9, L_10, ((int32_t)((int32_t)L_11+(int32_t)L_12)), /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_005c:
	{
		banmen_t2510752313 * L_14 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_14);
		bool L_15 = L_14->get_wall_32();
		if (L_15)
		{
			goto IL_0049;
		}
	}
	{
		V_0 = 1;
		banmen_t2510752313 * L_16 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_16);
		L_16->set_wall_32((bool)1);
		goto IL_0092;
	}

IL_007f:
	{
		banmen_t2510752313 * L_17 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_18 = ___l0;
		int32_t L_19 = V_0;
		int32_t L_20 = ___r1;
		NullCheck(L_17);
		banmen_isThereFuti_m1442766142(L_17, ((int32_t)((int32_t)L_18+(int32_t)L_19)), L_20, /*hidden argument*/NULL);
		int32_t L_21 = V_0;
		V_0 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0092:
	{
		banmen_t2510752313 * L_22 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_22);
		bool L_23 = L_22->get_wall_32();
		if (L_23)
		{
			goto IL_007f;
		}
	}
	{
		V_0 = 1;
		banmen_t2510752313 * L_24 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_24);
		L_24->set_wall_32((bool)1);
		goto IL_00c8;
	}

IL_00b5:
	{
		banmen_t2510752313 * L_25 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_26 = ___l0;
		int32_t L_27 = V_0;
		int32_t L_28 = ___r1;
		NullCheck(L_25);
		banmen_isThereFuti_m1442766142(L_25, ((int32_t)((int32_t)L_26-(int32_t)L_27)), L_28, /*hidden argument*/NULL);
		int32_t L_29 = V_0;
		V_0 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00c8:
	{
		banmen_t2510752313 * L_30 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_30);
		bool L_31 = L_30->get_wall_32();
		if (L_31)
		{
			goto IL_00b5;
		}
	}
	{
		banmen_t2510752313 * L_32 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_32);
		L_32->set_wall_32((bool)1);
		return;
	}
}
// System.Void EDrone::.ctor()
extern "C"  void EDrone__ctor_m3222050530 (EDrone_t2061607325 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EDrone::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2188565914;
extern Il2CppCodeGenString* _stringLiteral3981521668;
extern const uint32_t EDrone_Init_m3295800248_MetadataUsageId;
extern "C"  void EDrone_Init_m3295800248 (EDrone_t2061607325 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EDrone_Init_m3295800248_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2188565914, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral3981521668, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(((int32_t)13));
		((Koma_t3011766596 *)__this)->set_resources_3(3);
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)30));
		return;
	}
}
// System.Void EDrone::Move(System.Int32,System.Int32)
extern "C"  void EDrone_Move_m1270797897 (EDrone_t2061607325 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		L_0->set_enemy_31((bool)1);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_1);
		L_1->set_wall_32((bool)1);
		V_0 = 1;
		goto IL_0032;
	}

IL_001f:
	{
		banmen_t2510752313 * L_2 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_3 = ___l0;
		int32_t L_4 = ___r1;
		int32_t L_5 = V_0;
		NullCheck(L_2);
		banmen_isThereFuti_m1442766142(L_2, L_3, ((int32_t)((int32_t)L_4-(int32_t)L_5)), /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0032:
	{
		banmen_t2510752313 * L_7 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_7);
		bool L_8 = L_7->get_wall_32();
		if (L_8)
		{
			goto IL_001f;
		}
	}
	{
		V_0 = 1;
		banmen_t2510752313 * L_9 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_9);
		L_9->set_wall_32((bool)1);
		goto IL_0068;
	}

IL_0055:
	{
		banmen_t2510752313 * L_10 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_11 = ___l0;
		int32_t L_12 = ___r1;
		int32_t L_13 = V_0;
		NullCheck(L_10);
		banmen_isThereFuti_m1442766142(L_10, L_11, ((int32_t)((int32_t)L_12+(int32_t)L_13)), /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0068:
	{
		banmen_t2510752313 * L_15 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_15);
		bool L_16 = L_15->get_wall_32();
		if (L_16)
		{
			goto IL_0055;
		}
	}
	{
		V_0 = 1;
		banmen_t2510752313 * L_17 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_17);
		L_17->set_wall_32((bool)1);
		goto IL_009e;
	}

IL_008b:
	{
		banmen_t2510752313 * L_18 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_19 = ___l0;
		int32_t L_20 = V_0;
		int32_t L_21 = ___r1;
		NullCheck(L_18);
		banmen_isThereFuti_m1442766142(L_18, ((int32_t)((int32_t)L_19+(int32_t)L_20)), L_21, /*hidden argument*/NULL);
		int32_t L_22 = V_0;
		V_0 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_009e:
	{
		banmen_t2510752313 * L_23 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_23);
		bool L_24 = L_23->get_wall_32();
		if (L_24)
		{
			goto IL_008b;
		}
	}
	{
		V_0 = 1;
		banmen_t2510752313 * L_25 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_25);
		L_25->set_wall_32((bool)1);
		goto IL_00d4;
	}

IL_00c1:
	{
		banmen_t2510752313 * L_26 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_27 = ___l0;
		int32_t L_28 = V_0;
		int32_t L_29 = ___r1;
		NullCheck(L_26);
		banmen_isThereFuti_m1442766142(L_26, ((int32_t)((int32_t)L_27-(int32_t)L_28)), L_29, /*hidden argument*/NULL);
		int32_t L_30 = V_0;
		V_0 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00d4:
	{
		banmen_t2510752313 * L_31 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_31);
		bool L_32 = L_31->get_wall_32();
		if (L_32)
		{
			goto IL_00c1;
		}
	}
	{
		banmen_t2510752313 * L_33 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_33);
		L_33->set_wall_32((bool)1);
		banmen_t2510752313 * L_34 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_34);
		L_34->set_enemy_31((bool)0);
		return;
	}
}
// System.Void EFactory::.ctor()
extern "C"  void EFactory__ctor_m2230763454 (EFactory_t2050856767 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EFactory::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1430468714;
extern Il2CppCodeGenString* _stringLiteral3536310473;
extern const uint32_t EFactory_Init_m3144594972_MetadataUsageId;
extern "C"  void EFactory_Init_m3144594972 (EFactory_t2050856767 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EFactory_Init_m3144594972_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral1430468714, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral3536310473, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(((int32_t)12));
		((Koma_t3011766596 *)__this)->set_resources_3(((int32_t)10));
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)100));
		return;
	}
}
// System.Void EGRobot::.ctor()
extern "C"  void EGRobot__ctor_m4001745305 (EGRobot_t1059295608 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EGRobot::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1997637721;
extern Il2CppCodeGenString* _stringLiteral4236756470;
extern const uint32_t EGRobot_Init_m1503361377_MetadataUsageId;
extern "C"  void EGRobot_Init_m1503361377 (EGRobot_t1059295608 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EGRobot_Init_m1503361377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral1997637721, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral4236756470, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(((int32_t)9));
		((Koma_t3011766596 *)__this)->set_resources_3(5);
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)50));
		return;
	}
}
// System.Void EGRobot::Move(System.Int32,System.Int32)
extern "C"  void EGRobot_Move_m3918904246 (EGRobot_t1059295608 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		L_0->set_enemy_31((bool)1);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_2 = ___l0;
		int32_t L_3 = ___r1;
		NullCheck(L_1);
		banmen_isThereKoma_m207992560(L_1, L_2, ((int32_t)((int32_t)L_3-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_4 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_5 = ___l0;
		int32_t L_6 = ___r1;
		NullCheck(L_4);
		banmen_isThereKoma_m207992560(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_7 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_8 = ___l0;
		int32_t L_9 = ___r1;
		NullCheck(L_7);
		banmen_isThereKoma_m207992560(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), ((int32_t)((int32_t)L_9+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_10 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_11 = ___l0;
		int32_t L_12 = ___r1;
		NullCheck(L_10);
		banmen_isThereKoma_m207992560(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), L_12, /*hidden argument*/NULL);
		banmen_t2510752313 * L_13 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_14 = ___l0;
		int32_t L_15 = ___r1;
		NullCheck(L_13);
		banmen_isThereKoma_m207992560(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)), L_15, /*hidden argument*/NULL);
		banmen_t2510752313 * L_16 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_17 = ___l0;
		int32_t L_18 = ___r1;
		NullCheck(L_16);
		banmen_isThereKoma_m207992560(L_16, L_17, ((int32_t)((int32_t)L_18+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_19 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_19);
		L_19->set_enemy_31((bool)0);
		return;
	}
}
// System.Void EGRobot::Draw(System.Int32,System.Int32)
extern "C"  void EGRobot_Draw_m4086571065 (EGRobot_t1059295608 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		L_0->set_enemy_31((bool)1);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_2 = ___l0;
		int32_t L_3 = ___r1;
		NullCheck(L_1);
		banmen_isThereDraw_m900625674(L_1, L_2, ((int32_t)((int32_t)L_3-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_4 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_5 = ___l0;
		int32_t L_6 = ___r1;
		NullCheck(L_4);
		banmen_isThereDraw_m900625674(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_7 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_8 = ___l0;
		int32_t L_9 = ___r1;
		NullCheck(L_7);
		banmen_isThereDraw_m900625674(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), ((int32_t)((int32_t)L_9+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_10 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_11 = ___l0;
		int32_t L_12 = ___r1;
		NullCheck(L_10);
		banmen_isThereDraw_m900625674(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), L_12, /*hidden argument*/NULL);
		banmen_t2510752313 * L_13 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_14 = ___l0;
		int32_t L_15 = ___r1;
		NullCheck(L_13);
		banmen_isThereDraw_m900625674(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)), L_15, /*hidden argument*/NULL);
		banmen_t2510752313 * L_16 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_17 = ___l0;
		int32_t L_18 = ___r1;
		NullCheck(L_16);
		banmen_isThereDraw_m900625674(L_16, L_17, ((int32_t)((int32_t)L_18+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_19 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_19);
		L_19->set_enemy_31((bool)0);
		return;
	}
}
// System.Void EGRobot::Erase(System.Int32,System.Int32)
extern "C"  void EGRobot_Erase_m564378421 (EGRobot_t1059295608 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		L_0->set_enemy_31((bool)1);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_2 = ___l0;
		int32_t L_3 = ___r1;
		NullCheck(L_1);
		banmen_isThereErase_m2459852564(L_1, L_2, ((int32_t)((int32_t)L_3-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_4 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_5 = ___l0;
		int32_t L_6 = ___r1;
		NullCheck(L_4);
		banmen_isThereErase_m2459852564(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_7 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_8 = ___l0;
		int32_t L_9 = ___r1;
		NullCheck(L_7);
		banmen_isThereErase_m2459852564(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), ((int32_t)((int32_t)L_9+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_10 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_11 = ___l0;
		int32_t L_12 = ___r1;
		NullCheck(L_10);
		banmen_isThereErase_m2459852564(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), L_12, /*hidden argument*/NULL);
		banmen_t2510752313 * L_13 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_14 = ___l0;
		int32_t L_15 = ___r1;
		NullCheck(L_13);
		banmen_isThereErase_m2459852564(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)), L_15, /*hidden argument*/NULL);
		banmen_t2510752313 * L_16 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_17 = ___l0;
		int32_t L_18 = ___r1;
		NullCheck(L_16);
		banmen_isThereErase_m2459852564(L_16, L_17, ((int32_t)((int32_t)L_18+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_19 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_19);
		L_19->set_enemy_31((bool)0);
		return;
	}
}
// System.Void EHuman::.ctor()
extern "C"  void EHuman__ctor_m516293241 (EHuman_t715499684 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EHuman::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2484191629;
extern const uint32_t EHuman_Init_m186255137_MetadataUsageId;
extern "C"  void EHuman_Init_m186255137 (EHuman_t715499684 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EHuman_Init_m186255137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2484191629, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(8);
		((Koma_t3011766596 *)__this)->set_resources_3(5);
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)100));
		return;
	}
}
// System.Void EHuman::Move(System.Int32,System.Int32)
extern "C"  void EHuman_Move_m3808495210 (EHuman_t715499684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		L_0->set_enemy_31((bool)1);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_2 = ___l0;
		int32_t L_3 = ___r1;
		NullCheck(L_1);
		banmen_isThereKoma_m207992560(L_1, L_2, ((int32_t)((int32_t)L_3-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_4 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_5 = ___l0;
		int32_t L_6 = ___r1;
		NullCheck(L_4);
		banmen_isThereKoma_m207992560(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_7 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_8 = ___l0;
		int32_t L_9 = ___r1;
		NullCheck(L_7);
		banmen_isThereKoma_m207992560(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_10 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_11 = ___l0;
		int32_t L_12 = ___r1;
		NullCheck(L_10);
		banmen_isThereKoma_m207992560(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), L_12, /*hidden argument*/NULL);
		banmen_t2510752313 * L_13 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_14 = ___l0;
		int32_t L_15 = ___r1;
		NullCheck(L_13);
		banmen_isThereKoma_m207992560(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)), L_15, /*hidden argument*/NULL);
		banmen_t2510752313 * L_16 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_17 = ___l0;
		int32_t L_18 = ___r1;
		NullCheck(L_16);
		banmen_isThereKoma_m207992560(L_16, L_17, ((int32_t)((int32_t)L_18+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_19 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_20 = ___l0;
		int32_t L_21 = ___r1;
		NullCheck(L_19);
		banmen_isThereKoma_m207992560(L_19, ((int32_t)((int32_t)L_20-(int32_t)1)), ((int32_t)((int32_t)L_21+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_22 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_23 = ___l0;
		int32_t L_24 = ___r1;
		NullCheck(L_22);
		banmen_isThereKoma_m207992560(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)), ((int32_t)((int32_t)L_24+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_25 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_25);
		L_25->set_enemy_31((bool)0);
		return;
	}
}
// System.Void EHuman::Draw(System.Int32,System.Int32)
extern "C"  void EHuman_Draw_m2815985313 (EHuman_t715499684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		L_0->set_enemy_31((bool)1);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_2 = ___l0;
		int32_t L_3 = ___r1;
		NullCheck(L_1);
		banmen_isThereDraw_m900625674(L_1, L_2, ((int32_t)((int32_t)L_3-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_4 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_5 = ___l0;
		int32_t L_6 = ___r1;
		NullCheck(L_4);
		banmen_isThereDraw_m900625674(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_7 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_8 = ___l0;
		int32_t L_9 = ___r1;
		NullCheck(L_7);
		banmen_isThereDraw_m900625674(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_10 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_11 = ___l0;
		int32_t L_12 = ___r1;
		NullCheck(L_10);
		banmen_isThereDraw_m900625674(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), L_12, /*hidden argument*/NULL);
		banmen_t2510752313 * L_13 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_14 = ___l0;
		int32_t L_15 = ___r1;
		NullCheck(L_13);
		banmen_isThereDraw_m900625674(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)), L_15, /*hidden argument*/NULL);
		banmen_t2510752313 * L_16 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_17 = ___l0;
		int32_t L_18 = ___r1;
		NullCheck(L_16);
		banmen_isThereDraw_m900625674(L_16, L_17, ((int32_t)((int32_t)L_18+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_19 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_20 = ___l0;
		int32_t L_21 = ___r1;
		NullCheck(L_19);
		banmen_isThereDraw_m900625674(L_19, ((int32_t)((int32_t)L_20-(int32_t)1)), ((int32_t)((int32_t)L_21+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_22 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_23 = ___l0;
		int32_t L_24 = ___r1;
		NullCheck(L_22);
		banmen_isThereDraw_m900625674(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)), ((int32_t)((int32_t)L_24+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_25 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_25);
		L_25->set_enemy_31((bool)0);
		return;
	}
}
// System.Void EHuman::Erase(System.Int32,System.Int32)
extern "C"  void EHuman_Erase_m2034365109 (EHuman_t715499684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		L_0->set_enemy_31((bool)1);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_2 = ___l0;
		int32_t L_3 = ___r1;
		NullCheck(L_1);
		banmen_isThereErase_m2459852564(L_1, L_2, ((int32_t)((int32_t)L_3-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_4 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_5 = ___l0;
		int32_t L_6 = ___r1;
		NullCheck(L_4);
		banmen_isThereErase_m2459852564(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_7 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_8 = ___l0;
		int32_t L_9 = ___r1;
		NullCheck(L_7);
		banmen_isThereErase_m2459852564(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_10 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_11 = ___l0;
		int32_t L_12 = ___r1;
		NullCheck(L_10);
		banmen_isThereErase_m2459852564(L_10, ((int32_t)((int32_t)L_11+(int32_t)1)), L_12, /*hidden argument*/NULL);
		banmen_t2510752313 * L_13 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_14 = ___l0;
		int32_t L_15 = ___r1;
		NullCheck(L_13);
		banmen_isThereErase_m2459852564(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)), L_15, /*hidden argument*/NULL);
		banmen_t2510752313 * L_16 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_17 = ___l0;
		int32_t L_18 = ___r1;
		NullCheck(L_16);
		banmen_isThereErase_m2459852564(L_16, L_17, ((int32_t)((int32_t)L_18+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_19 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_20 = ___l0;
		int32_t L_21 = ___r1;
		NullCheck(L_19);
		banmen_isThereErase_m2459852564(L_19, ((int32_t)((int32_t)L_20-(int32_t)1)), ((int32_t)((int32_t)L_21+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_22 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_23 = ___l0;
		int32_t L_24 = ___r1;
		NullCheck(L_22);
		banmen_isThereErase_m2459852564(L_22, ((int32_t)((int32_t)L_23+(int32_t)1)), ((int32_t)((int32_t)L_24+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_25 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_25);
		L_25->set_wall_32((bool)1);
		banmen_t2510752313 * L_26 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_26);
		L_26->set_enemy_31((bool)0);
		return;
	}
}
// System.Void EMissile::.ctor()
extern "C"  void EMissile__ctor_m3541219220 (EMissile_t3534618099 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EMissile::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4122800672;
extern Il2CppCodeGenString* _stringLiteral576037949;
extern const uint32_t EMissile_Init_m3737596326_MetadataUsageId;
extern "C"  void EMissile_Init_m3737596326 (EMissile_t3534618099 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EMissile_Init_m3737596326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral4122800672, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral576037949, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(((int32_t)11));
		((Koma_t3011766596 *)__this)->set_resources_3(8);
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)80));
		return;
	}
}
// System.Void EMissile::Move(System.Int32,System.Int32)
extern "C"  void EMissile_Move_m827381263 (EMissile_t3534618099 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		banmen_atackEMissile_m1668074645(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EMissile::Action(System.Int32)
extern "C"  void EMissile_Action_m2403800549 (EMissile_t3534618099 * __this, int32_t ___resource0, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		banmen_destroyByMissile_m1609839829(L_0, /*hidden argument*/NULL);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_1);
		banmen_drawBarrier_m1973980853(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Empty::.ctor()
extern "C"  void Empty__ctor_m4179857912 (Empty_t4012560223 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Empty::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2994424589;
extern const uint32_t Empty_Init_m1995398434_MetadataUsageId;
extern "C"  void Empty_Init_m1995398434 (Empty_t4012560223 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Empty_Init_m1995398434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2994424589, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(0);
		((Koma_t3011766596 *)__this)->set_resources_3(0);
		return;
	}
}
// System.Void Empty::Move(System.Int32,System.Int32)
extern "C"  void Empty_Move_m501850743 (Empty_t4012560223 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		banmen_reset_m2092301357(L_0, /*hidden argument*/NULL);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_1);
		baseMethods_t2705876163 * L_2 = L_1->get_baseM_2();
		NullCheck(L_2);
		baseMethods_canTouchAll_m280133149(L_2, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ESkytree::.ctor()
extern "C"  void ESkytree__ctor_m3421849381 (ESkytree_t2245705948 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ESkytree::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1348549033;
extern Il2CppCodeGenString* _stringLiteral2406430924;
extern const uint32_t ESkytree_Init_m4005599381_MetadataUsageId;
extern "C"  void ESkytree_Init_m4005599381 (ESkytree_t2245705948 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ESkytree_Init_m4005599381_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral1348549033, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2406430924, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(((int32_t)14));
		((Koma_t3011766596 *)__this)->set_resources_3(((int32_t)10));
		return;
	}
}
// System.Void ESRobot::.ctor()
extern "C"  void ESRobot__ctor_m4281893077 (ESRobot_t2546702684 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ESRobot::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral492783797;
extern Il2CppCodeGenString* _stringLiteral1221785882;
extern const uint32_t ESRobot_Init_m3417921989_MetadataUsageId;
extern "C"  void ESRobot_Init_m3417921989 (ESRobot_t2546702684 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ESRobot_Init_m3417921989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral492783797, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral1221785882, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(((int32_t)10));
		((Koma_t3011766596 *)__this)->set_resources_3(4);
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)40));
		return;
	}
}
// System.Void ESRobot::Move(System.Int32,System.Int32)
extern "C"  void ESRobot_Move_m1220482802 (ESRobot_t2546702684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		L_0->set_enemy_31((bool)1);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_2 = ___l0;
		int32_t L_3 = ___r1;
		NullCheck(L_1);
		banmen_isThereKoma_m207992560(L_1, L_2, ((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_4 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_5 = ___l0;
		int32_t L_6 = ___r1;
		NullCheck(L_4);
		banmen_isThereKoma_m207992560(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_7 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_8 = ___l0;
		int32_t L_9 = ___r1;
		NullCheck(L_7);
		banmen_isThereKoma_m207992560(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_10 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_11 = ___l0;
		int32_t L_12 = ___r1;
		NullCheck(L_10);
		banmen_isThereKoma_m207992560(L_10, ((int32_t)((int32_t)L_11-(int32_t)1)), ((int32_t)((int32_t)L_12+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_13 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_14 = ___l0;
		int32_t L_15 = ___r1;
		NullCheck(L_13);
		banmen_isThereKoma_m207992560(L_13, ((int32_t)((int32_t)L_14+(int32_t)1)), ((int32_t)((int32_t)L_15+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_16 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_16);
		L_16->set_enemy_31((bool)0);
		return;
	}
}
// System.Void ESRobot::Draw(System.Int32,System.Int32)
extern "C"  void ESRobot_Draw_m3193670653 (ESRobot_t2546702684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		L_0->set_enemy_31((bool)1);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_2 = ___l0;
		int32_t L_3 = ___r1;
		NullCheck(L_1);
		banmen_isThereDraw_m900625674(L_1, L_2, ((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_4 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_5 = ___l0;
		int32_t L_6 = ___r1;
		NullCheck(L_4);
		banmen_isThereDraw_m900625674(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_7 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_8 = ___l0;
		int32_t L_9 = ___r1;
		NullCheck(L_7);
		banmen_isThereDraw_m900625674(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_10 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_11 = ___l0;
		int32_t L_12 = ___r1;
		NullCheck(L_10);
		banmen_isThereDraw_m900625674(L_10, ((int32_t)((int32_t)L_11-(int32_t)1)), ((int32_t)((int32_t)L_12+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_13 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_14 = ___l0;
		int32_t L_15 = ___r1;
		NullCheck(L_13);
		banmen_isThereDraw_m900625674(L_13, ((int32_t)((int32_t)L_14+(int32_t)1)), ((int32_t)((int32_t)L_15+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_16 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_16);
		L_16->set_enemy_31((bool)0);
		return;
	}
}
// System.Void ESRobot::Erase(System.Int32,System.Int32)
extern "C"  void ESRobot_Erase_m4105753849 (ESRobot_t2546702684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		L_0->set_enemy_31((bool)1);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_2 = ___l0;
		int32_t L_3 = ___r1;
		NullCheck(L_1);
		banmen_isThereErase_m2459852564(L_1, L_2, ((int32_t)((int32_t)L_3+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_4 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_5 = ___l0;
		int32_t L_6 = ___r1;
		NullCheck(L_4);
		banmen_isThereErase_m2459852564(L_4, ((int32_t)((int32_t)L_5-(int32_t)1)), ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_7 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_8 = ___l0;
		int32_t L_9 = ___r1;
		NullCheck(L_7);
		banmen_isThereErase_m2459852564(L_7, ((int32_t)((int32_t)L_8+(int32_t)1)), ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_10 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_11 = ___l0;
		int32_t L_12 = ___r1;
		NullCheck(L_10);
		banmen_isThereErase_m2459852564(L_10, ((int32_t)((int32_t)L_11-(int32_t)1)), ((int32_t)((int32_t)L_12+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_13 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_14 = ___l0;
		int32_t L_15 = ___r1;
		NullCheck(L_13);
		banmen_isThereErase_m2459852564(L_13, ((int32_t)((int32_t)L_14+(int32_t)1)), ((int32_t)((int32_t)L_15+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_16 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_16);
		L_16->set_enemy_31((bool)0);
		return;
	}
}
// System.Void Factory::.ctor()
extern "C"  void Factory__ctor_m2197654087 (Factory_t931158954 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Factory::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3139759631;
extern Il2CppCodeGenString* _stringLiteral3916019680;
extern const uint32_t Factory_Init_m1507447763_MetadataUsageId;
extern "C"  void Factory_Init_m1507447763 (Factory_t931158954 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Factory_Init_m1507447763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral3139759631, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral3916019680, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(5);
		((Koma_t3011766596 *)__this)->set_resources_3(((int32_t)10));
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)100));
		return;
	}
}
// System.Void Futi::.ctor()
extern "C"  void Futi__ctor_m1883418413 (Futi_t612262014 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Futi::Start()
extern "C"  void Futi_Start_m1837758333 (Futi_t612262014 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GRobot::.ctor()
extern "C"  void GRobot__ctor_m1963847546 (GRobot_t456225233 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GRobot::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1506717344;
extern Il2CppCodeGenString* _stringLiteral2707795379;
extern const uint32_t GRobot_Init_m3680419648_MetadataUsageId;
extern "C"  void GRobot_Init_m3680419648 (GRobot_t456225233 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GRobot_Init_m3680419648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral1506717344, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2707795379, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(2);
		((Koma_t3011766596 *)__this)->set_resources_3(5);
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)50));
		return;
	}
}
// System.Void GRobot::Move(System.Int32,System.Int32)
extern "C"  void GRobot_Move_m656884969 (GRobot_t456225233 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck(L_0);
		banmen_isThereKoma_m207992560(L_0, L_1, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_3 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_4 = ___l0;
		int32_t L_5 = ___r1;
		NullCheck(L_3);
		banmen_isThereKoma_m207992560(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), ((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_6 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_7 = ___l0;
		int32_t L_8 = ___r1;
		NullCheck(L_6);
		banmen_isThereKoma_m207992560(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_9 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_10 = ___l0;
		int32_t L_11 = ___r1;
		NullCheck(L_9);
		banmen_isThereKoma_m207992560(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), L_11, /*hidden argument*/NULL);
		banmen_t2510752313 * L_12 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_13 = ___l0;
		int32_t L_14 = ___r1;
		NullCheck(L_12);
		banmen_isThereKoma_m207992560(L_12, ((int32_t)((int32_t)L_13-(int32_t)1)), L_14, /*hidden argument*/NULL);
		banmen_t2510752313 * L_15 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_16 = ___l0;
		int32_t L_17 = ___r1;
		NullCheck(L_15);
		banmen_isThereKoma_m207992560(L_15, L_16, ((int32_t)((int32_t)L_17+(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void GRobot::Draw(System.Int32,System.Int32)
extern "C"  void GRobot_Draw_m3014666978 (GRobot_t456225233 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck(L_0);
		banmen_isThereDraw_m900625674(L_0, L_1, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_3 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_4 = ___l0;
		int32_t L_5 = ___r1;
		NullCheck(L_3);
		banmen_isThereDraw_m900625674(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), ((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_6 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_7 = ___l0;
		int32_t L_8 = ___r1;
		NullCheck(L_6);
		banmen_isThereDraw_m900625674(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_9 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_10 = ___l0;
		int32_t L_11 = ___r1;
		NullCheck(L_9);
		banmen_isThereDraw_m900625674(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), L_11, /*hidden argument*/NULL);
		banmen_t2510752313 * L_12 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_13 = ___l0;
		int32_t L_14 = ___r1;
		NullCheck(L_12);
		banmen_isThereDraw_m900625674(L_12, ((int32_t)((int32_t)L_13-(int32_t)1)), L_14, /*hidden argument*/NULL);
		banmen_t2510752313 * L_15 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_16 = ___l0;
		int32_t L_17 = ___r1;
		NullCheck(L_15);
		banmen_isThereDraw_m900625674(L_15, L_16, ((int32_t)((int32_t)L_17+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_18 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_18);
		L_18->set_wall_32((bool)1);
		return;
	}
}
// System.Void GRobot::Erase(System.Int32,System.Int32)
extern "C"  void GRobot_Erase_m3020905980 (GRobot_t456225233 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck(L_0);
		banmen_isThereErase_m2459852564(L_0, L_1, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_3 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_4 = ___l0;
		int32_t L_5 = ___r1;
		NullCheck(L_3);
		banmen_isThereErase_m2459852564(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), ((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_6 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_7 = ___l0;
		int32_t L_8 = ___r1;
		NullCheck(L_6);
		banmen_isThereErase_m2459852564(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_9 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_10 = ___l0;
		int32_t L_11 = ___r1;
		NullCheck(L_9);
		banmen_isThereErase_m2459852564(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), L_11, /*hidden argument*/NULL);
		banmen_t2510752313 * L_12 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_13 = ___l0;
		int32_t L_14 = ___r1;
		NullCheck(L_12);
		banmen_isThereErase_m2459852564(L_12, ((int32_t)((int32_t)L_13-(int32_t)1)), L_14, /*hidden argument*/NULL);
		banmen_t2510752313 * L_15 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_16 = ___l0;
		int32_t L_17 = ___r1;
		NullCheck(L_15);
		banmen_isThereErase_m2459852564(L_15, L_16, ((int32_t)((int32_t)L_17+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_18 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_18);
		L_18->set_wall_32((bool)1);
		return;
	}
}
// System.Void Human::.ctor()
extern "C"  void Human__ctor_m4119456528 (Human_t1156088493 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Human::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1121820566;
extern const uint32_t Human_Init_m20722698_MetadataUsageId;
extern "C"  void Human_Init_m20722698 (Human_t1156088493 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Human_Init_m20722698_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral1121820566, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(1);
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)100));
		return;
	}
}
// System.Void Human::Move(System.Int32,System.Int32)
extern "C"  void Human_Move_m2550404821 (Human_t1156088493 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck(L_0);
		banmen_isThereKoma_m207992560(L_0, L_1, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_3 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_4 = ___l0;
		int32_t L_5 = ___r1;
		NullCheck(L_3);
		banmen_isThereKoma_m207992560(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), ((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_6 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_7 = ___l0;
		int32_t L_8 = ___r1;
		NullCheck(L_6);
		banmen_isThereKoma_m207992560(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_9 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_10 = ___l0;
		int32_t L_11 = ___r1;
		NullCheck(L_9);
		banmen_isThereKoma_m207992560(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), L_11, /*hidden argument*/NULL);
		banmen_t2510752313 * L_12 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_13 = ___l0;
		int32_t L_14 = ___r1;
		NullCheck(L_12);
		banmen_isThereKoma_m207992560(L_12, ((int32_t)((int32_t)L_13-(int32_t)1)), L_14, /*hidden argument*/NULL);
		banmen_t2510752313 * L_15 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_16 = ___l0;
		int32_t L_17 = ___r1;
		NullCheck(L_15);
		banmen_isThereKoma_m207992560(L_15, L_16, ((int32_t)((int32_t)L_17+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_18 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_19 = ___l0;
		int32_t L_20 = ___r1;
		NullCheck(L_18);
		banmen_isThereKoma_m207992560(L_18, ((int32_t)((int32_t)L_19-(int32_t)1)), ((int32_t)((int32_t)L_20+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_21 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_22 = ___l0;
		int32_t L_23 = ___r1;
		NullCheck(L_21);
		banmen_isThereKoma_m207992560(L_21, ((int32_t)((int32_t)L_22+(int32_t)1)), ((int32_t)((int32_t)L_23+(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Human::Draw(System.Int32,System.Int32)
extern "C"  void Human_Draw_m66723152 (Human_t1156088493 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck(L_0);
		banmen_isThereDraw_m900625674(L_0, L_1, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_3 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_4 = ___l0;
		int32_t L_5 = ___r1;
		NullCheck(L_3);
		banmen_isThereDraw_m900625674(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), ((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_6 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_7 = ___l0;
		int32_t L_8 = ___r1;
		NullCheck(L_6);
		banmen_isThereDraw_m900625674(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_9 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_10 = ___l0;
		int32_t L_11 = ___r1;
		NullCheck(L_9);
		banmen_isThereDraw_m900625674(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), L_11, /*hidden argument*/NULL);
		banmen_t2510752313 * L_12 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_13 = ___l0;
		int32_t L_14 = ___r1;
		NullCheck(L_12);
		banmen_isThereDraw_m900625674(L_12, ((int32_t)((int32_t)L_13-(int32_t)1)), L_14, /*hidden argument*/NULL);
		banmen_t2510752313 * L_15 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_16 = ___l0;
		int32_t L_17 = ___r1;
		NullCheck(L_15);
		banmen_isThereDraw_m900625674(L_15, L_16, ((int32_t)((int32_t)L_17+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_18 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_19 = ___l0;
		int32_t L_20 = ___r1;
		NullCheck(L_18);
		banmen_isThereDraw_m900625674(L_18, ((int32_t)((int32_t)L_19-(int32_t)1)), ((int32_t)((int32_t)L_20+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_21 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_22 = ___l0;
		int32_t L_23 = ___r1;
		NullCheck(L_21);
		banmen_isThereDraw_m900625674(L_21, ((int32_t)((int32_t)L_22+(int32_t)1)), ((int32_t)((int32_t)L_23+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_24 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_24);
		L_24->set_wall_32((bool)1);
		return;
	}
}
// System.Void Human::Erase(System.Int32,System.Int32)
extern "C"  void Human_Erase_m446363430 (Human_t1156088493 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck(L_0);
		banmen_isThereErase_m2459852564(L_0, L_1, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_3 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_4 = ___l0;
		int32_t L_5 = ___r1;
		NullCheck(L_3);
		banmen_isThereErase_m2459852564(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), ((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_6 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_7 = ___l0;
		int32_t L_8 = ___r1;
		NullCheck(L_6);
		banmen_isThereErase_m2459852564(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_9 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_10 = ___l0;
		int32_t L_11 = ___r1;
		NullCheck(L_9);
		banmen_isThereErase_m2459852564(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), L_11, /*hidden argument*/NULL);
		banmen_t2510752313 * L_12 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_13 = ___l0;
		int32_t L_14 = ___r1;
		NullCheck(L_12);
		banmen_isThereErase_m2459852564(L_12, ((int32_t)((int32_t)L_13-(int32_t)1)), L_14, /*hidden argument*/NULL);
		banmen_t2510752313 * L_15 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_16 = ___l0;
		int32_t L_17 = ___r1;
		NullCheck(L_15);
		banmen_isThereErase_m2459852564(L_15, L_16, ((int32_t)((int32_t)L_17+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_18 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_19 = ___l0;
		int32_t L_20 = ___r1;
		NullCheck(L_18);
		banmen_isThereErase_m2459852564(L_18, ((int32_t)((int32_t)L_19-(int32_t)1)), ((int32_t)((int32_t)L_20+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_21 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_22 = ___l0;
		int32_t L_23 = ___r1;
		NullCheck(L_21);
		banmen_isThereErase_m2459852564(L_21, ((int32_t)((int32_t)L_22+(int32_t)1)), ((int32_t)((int32_t)L_23+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_24 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_24);
		L_24->set_wall_32((bool)1);
		return;
	}
}
// System.Void inputName::.ctor()
extern "C"  void inputName__ctor_m2866011510 (inputName_t1068137479 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void inputName::Start()
extern "C"  void inputName_Start_m3658301090 (inputName_t1068137479 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void inputName::Update()
extern "C"  void inputName_Update_m3174464491 (inputName_t1068137479 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void inputName::OnClick()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1047780110;
extern Il2CppCodeGenString* _stringLiteral2663497738;
extern const uint32_t inputName_OnClick_m3622305209_MetadataUsageId;
extern "C"  void inputName_OnClick_m3622305209 (inputName_t1068137479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (inputName_OnClick_m3622305209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Canvas_t209405766 * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1047780110, /*hidden argument*/NULL);
		__this->set_canvasObject_3(L_0);
		Object_t1021602117 * L_1 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral2663497738, /*hidden argument*/NULL);
		V_0 = ((GameObject_t1756533147 *)CastclassSealed(L_1, GameObject_t1756533147_il2cpp_TypeInfo_var));
		GameObject_t1756533147 * L_2 = __this->get_canvasObject_3();
		NullCheck(L_2);
		Canvas_t209405766 * L_3 = GameObject_GetComponent_TisCanvas_t209405766_m195193039(L_2, /*hidden argument*/GameObject_GetComponent_TisCanvas_t209405766_m195193039_MethodInfo_var);
		V_1 = L_3;
		GameObject_t1756533147 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_5 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		__this->set_prefabResource_4(L_5);
		GameObject_t1756533147 * L_6 = __this->get_prefabResource_4();
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		Canvas_t209405766 * L_8 = V_1;
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_SetParent_m1963830867(L_7, L_9, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void inputName::SaveText()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSingleton_t1770047133_m2880166840_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2462408499;
extern Il2CppCodeGenString* _stringLiteral1713765239;
extern Il2CppCodeGenString* _stringLiteral2026617104;
extern const uint32_t inputName_SaveText_m2989475918_MetadataUsageId;
extern "C"  void inputName_SaveText_m2989475918 (inputName_t1068137479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (inputName_SaveText_m2989475918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Singleton_t1770047133 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2462408499, /*hidden argument*/NULL);
		__this->set_obj_2(L_0);
		GameObject_t1756533147 * L_1 = __this->get_obj_2();
		NullCheck(L_1);
		Singleton_t1770047133 * L_2 = GameObject_GetComponent_TisSingleton_t1770047133_m2880166840(L_1, /*hidden argument*/GameObject_GetComponent_TisSingleton_t1770047133_m2880166840_MethodInfo_var);
		V_0 = L_2;
		InputField_t1631627530 * L_3 = __this->get_inputField_7();
		NullCheck(L_3);
		String_t* L_4 = InputField_get_text_m409351770(L_3, /*hidden argument*/NULL);
		__this->set_str_6(L_4);
		Singleton_t1770047133 * L_5 = V_0;
		String_t* L_6 = __this->get_str_6();
		NullCheck(L_5);
		Singleton_setAccountName_m4172620012(L_5, L_6, /*hidden argument*/NULL);
		InputField_t1631627530 * L_7 = __this->get_inputField_7();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_7);
		InputField_set_text_m114077119(L_7, L_8, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_9 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1713765239, /*hidden argument*/NULL);
		NullCheck(L_9);
		Text_t356221433 * L_10 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_9, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_5(L_10);
		Text_t356221433 * L_11 = __this->get_text_5();
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_t1770047133 * L_13 = V_0;
		NullCheck(L_13);
		String_t* L_14 = Singleton_getAccountName_m4227199261(L_13, /*hidden argument*/NULL);
		String_t* L_15 = String_Concat_m2596409543(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_15);
		GameObject_t1756533147 * L_16 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2026617104, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Koma::.ctor()
extern "C"  void Koma__ctor_m1354333883 (Koma_t3011766596 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Koma::Init(System.String)
extern const MethodInfo* GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266_MethodInfo_var;
extern const uint32_t Koma_Init_m2356132927_MetadataUsageId;
extern "C"  void Koma_Init_m2356132927 (Koma_t3011766596 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Koma_Init_m2356132927_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		GameObject_t1756533147 * L_1 = GameObject_Find_m836511350(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		banmen_t2510752313 * L_2 = GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266(L_1, /*hidden argument*/GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266_MethodInfo_var);
		__this->set_ban_0(L_2);
		return;
	}
}
// System.Void Koma::Move(System.Int32,System.Int32)
extern "C"  void Koma_Move_m1070855718 (Koma_t3011766596 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Koma::Draw(System.Int32,System.Int32)
extern "C"  void Koma_Draw_m1486938147 (Koma_t3011766596 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Koma::Erase(System.Int32,System.Int32)
extern "C"  void Koma_Erase_m3965959631 (Koma_t3011766596 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Koma::Action(System.Int32)
extern "C"  void Koma_Action_m1381793788 (Koma_t3011766596 * __this, int32_t ___resources0, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = __this->get_ban_0();
		NullCheck(L_0);
		baseMethods_t2705876163 * L_1 = L_0->get_baseM_2();
		int32_t L_2 = ___resources0;
		NullCheck(L_1);
		baseMethods_getResources_m2257601282(L_1, L_2, /*hidden argument*/NULL);
		banmen_t2510752313 * L_3 = __this->get_ban_0();
		NullCheck(L_3);
		banmen_changeSprite_m109808171(L_3, /*hidden argument*/NULL);
		banmen_t2510752313 * L_4 = __this->get_ban_0();
		NullCheck(L_4);
		banmen_reTime_m4028066712(L_4, /*hidden argument*/NULL);
		banmen_t2510752313 * L_5 = __this->get_ban_0();
		NullCheck(L_5);
		banmen_rePaint_m1556028161(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Missile::.ctor()
extern "C"  void Missile__ctor_m2545921555 (Missile_t813944928 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Missile::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral71042275;
extern Il2CppCodeGenString* _stringLiteral3564949942;
extern const uint32_t Missile_Init_m2886599655_MetadataUsageId;
extern "C"  void Missile_Init_m2886599655 (Missile_t813944928 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Missile_Init_m2886599655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral71042275, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral3564949942, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(4);
		((Koma_t3011766596 *)__this)->set_resources_3(8);
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)80));
		return;
	}
}
// System.Void Missile::Move(System.Int32,System.Int32)
extern "C"  void Missile_Move_m4106143890 (Missile_t813944928 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		banmen_atackMissile_m1714889658(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Missile::Action(System.Int32)
extern "C"  void Missile_Action_m1343903756 (Missile_t813944928 * __this, int32_t ___resource0, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_0);
		banmen_destroyByMissile_m1609839829(L_0, /*hidden argument*/NULL);
		banmen_t2510752313 * L_1 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_1);
		banmen_drawBarrier_m1973980853(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MouseLook::.ctor()
extern "C"  void MouseLook__ctor_m2657785993 (MouseLook_t2643707144 * __this, const MethodInfo* method)
{
	{
		__this->set_sensitivityX_3((15.0f));
		__this->set_sensitivityY_4((15.0f));
		__this->set_minimumX_5((-360.0f));
		__this->set_maximumX_6((360.0f));
		__this->set_minimumY_7((-60.0f));
		__this->set_maximumY_8((60.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MouseLook::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1307534077;
extern Il2CppCodeGenString* _stringLiteral1307534078;
extern const uint32_t MouseLook_Update_m2110964150_MetadataUsageId;
extern "C"  void MouseLook_Update_m2110964150 (MouseLook_t2643707144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseLook_Update_m2110964150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t4030073918  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Quaternion_t4030073918  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		int32_t L_0 = __this->get_axes_2();
		if (L_0)
		{
			goto IL_00c5;
		}
	}
	{
		float L_1 = __this->get_rotationX_9();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534077, /*hidden argument*/NULL);
		float L_3 = __this->get_sensitivityX_3();
		__this->set_rotationX_9(((float)((float)L_1+(float)((float)((float)L_2*(float)L_3)))));
		float L_4 = __this->get_rotationY_10();
		float L_5 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534078, /*hidden argument*/NULL);
		float L_6 = __this->get_sensitivityY_4();
		__this->set_rotationY_10(((float)((float)L_4+(float)((float)((float)L_5*(float)L_6)))));
		float L_7 = __this->get_rotationX_9();
		float L_8 = __this->get_minimumX_5();
		float L_9 = __this->get_maximumX_6();
		float L_10 = MouseLook_ClampAngle_m1252663080(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->set_rotationX_9(L_10);
		float L_11 = __this->get_rotationY_10();
		float L_12 = __this->get_minimumY_7();
		float L_13 = __this->get_maximumY_8();
		float L_14 = MouseLook_ClampAngle_m1252663080(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		__this->set_rotationY_10(L_14);
		float L_15 = __this->get_rotationX_9();
		Vector3_t2243707580  L_16 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_17 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		float L_18 = __this->get_rotationY_10();
		Vector3_t2243707580  L_19 = Vector3_get_left_m2429378123(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_20 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_22 = __this->get_originalRotation_11();
		Quaternion_t4030073918  L_23 = V_0;
		Quaternion_t4030073918  L_24 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_25 = V_1;
		Quaternion_t4030073918  L_26 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_localRotation_m2055111962(L_21, L_26, /*hidden argument*/NULL);
		goto IL_019c;
	}

IL_00c5:
	{
		int32_t L_27 = __this->get_axes_2();
		if ((!(((uint32_t)L_27) == ((uint32_t)1))))
		{
			goto IL_0139;
		}
	}
	{
		float L_28 = __this->get_rotationX_9();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_29 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534077, /*hidden argument*/NULL);
		float L_30 = __this->get_sensitivityX_3();
		__this->set_rotationX_9(((float)((float)L_28+(float)((float)((float)L_29*(float)L_30)))));
		float L_31 = __this->get_rotationX_9();
		float L_32 = __this->get_minimumX_5();
		float L_33 = __this->get_maximumX_6();
		float L_34 = MouseLook_ClampAngle_m1252663080(NULL /*static, unused*/, L_31, L_32, L_33, /*hidden argument*/NULL);
		__this->set_rotationX_9(L_34);
		float L_35 = __this->get_rotationX_9();
		Vector3_t2243707580  L_36 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_37 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_2 = L_37;
		Transform_t3275118058 * L_38 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_39 = __this->get_originalRotation_11();
		Quaternion_t4030073918  L_40 = V_2;
		Quaternion_t4030073918  L_41 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_set_localRotation_m2055111962(L_38, L_41, /*hidden argument*/NULL);
		goto IL_019c;
	}

IL_0139:
	{
		float L_42 = __this->get_rotationY_10();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_43 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534078, /*hidden argument*/NULL);
		float L_44 = __this->get_sensitivityY_4();
		__this->set_rotationY_10(((float)((float)L_42+(float)((float)((float)L_43*(float)L_44)))));
		float L_45 = __this->get_rotationY_10();
		float L_46 = __this->get_minimumY_7();
		float L_47 = __this->get_maximumY_8();
		float L_48 = MouseLook_ClampAngle_m1252663080(NULL /*static, unused*/, L_45, L_46, L_47, /*hidden argument*/NULL);
		__this->set_rotationY_10(L_48);
		float L_49 = __this->get_rotationY_10();
		Vector3_t2243707580  L_50 = Vector3_get_left_m2429378123(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_51 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, L_49, L_50, /*hidden argument*/NULL);
		V_3 = L_51;
		Transform_t3275118058 * L_52 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_53 = __this->get_originalRotation_11();
		Quaternion_t4030073918  L_54 = V_3;
		Quaternion_t4030073918  L_55 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		Transform_set_localRotation_m2055111962(L_52, L_55, /*hidden argument*/NULL);
	}

IL_019c:
	{
		return;
	}
}
// System.Void MouseLook::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t4233889191_m88144364_MethodInfo_var;
extern const uint32_t MouseLook_Start_m770045177_MetadataUsageId;
extern "C"  void MouseLook_Start_m770045177 (MouseLook_t2643707144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseLook_Start_m770045177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody_t4233889191 * L_0 = Component_GetComponent_TisRigidbody_t4233889191_m88144364(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m88144364_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Rigidbody_t4233889191 * L_2 = Component_GetComponent_TisRigidbody_t4233889191_m88144364(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m88144364_MethodInfo_var);
		NullCheck(L_2);
		Rigidbody_set_freezeRotation_m2131864169(L_2, (bool)1, /*hidden argument*/NULL);
	}

IL_001c:
	{
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Quaternion_t4030073918  L_4 = Transform_get_localRotation_m4001487205(L_3, /*hidden argument*/NULL);
		__this->set_originalRotation_11(L_4);
		return;
	}
}
// System.Single MouseLook::ClampAngle(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t MouseLook_ClampAngle_m1252663080_MetadataUsageId;
extern "C"  float MouseLook_ClampAngle_m1252663080 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MouseLook_ClampAngle_m1252663080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___angle0;
		if ((!(((float)L_0) < ((float)(-360.0f)))))
		{
			goto IL_0014;
		}
	}
	{
		float L_1 = ___angle0;
		___angle0 = ((float)((float)L_1+(float)(360.0f)));
	}

IL_0014:
	{
		float L_2 = ___angle0;
		if ((!(((float)L_2) > ((float)(360.0f)))))
		{
			goto IL_0028;
		}
	}
	{
		float L_3 = ___angle0;
		___angle0 = ((float)((float)L_3-(float)(360.0f)));
	}

IL_0028:
	{
		float L_4 = ___angle0;
		float L_5 = ___min1;
		float L_6 = ___max2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void OpeningButton::.ctor()
extern "C"  void OpeningButton__ctor_m663800617 (OpeningButton_t2416718932 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OpeningButton::Start()
extern "C"  void OpeningButton_Start_m4151042585 (OpeningButton_t2416718932 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void OpeningButton::Update()
extern "C"  void OpeningButton_Update_m758039582 (OpeningButton_t2416718932 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void OpeningButton::OnClick()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3524152506;
extern Il2CppCodeGenString* _stringLiteral2602936930;
extern Il2CppCodeGenString* _stringLiteral3298283466;
extern Il2CppCodeGenString* _stringLiteral376201520;
extern Il2CppCodeGenString* _stringLiteral435493768;
extern const uint32_t OpeningButton_OnClick_m4075053516_MetadataUsageId;
extern "C"  void OpeningButton_OnClick_m4075053516 (OpeningButton_t2416718932 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OpeningButton_OnClick_m4075053516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_stageNum_2();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002e;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
		if (L_1 == 2)
		{
			goto IL_004c;
		}
		if (L_1 == 3)
		{
			goto IL_0051;
		}
		if (L_1 == 4)
		{
			goto IL_0060;
		}
		if (L_1 == 5)
		{
			goto IL_006f;
		}
		if (L_1 == 6)
		{
			goto IL_0079;
		}
	}
	{
		goto IL_0088;
	}

IL_002e:
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral3524152506, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_003d:
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2602936930, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_004c:
	{
		goto IL_0088;
	}

IL_0051:
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral3298283466, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0060:
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral376201520, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_006f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_PlayLastRecording_m123861831(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0079:
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral435493768, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0088:
	{
		return;
	}
}
// System.Void pushButton::.ctor()
extern "C"  void pushButton__ctor_m292811691 (pushButton_t4005487804 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void pushButton::Start()
extern Il2CppCodeGenString* _stringLiteral3901280377;
extern const uint32_t pushButton_Start_m3144148455_MetadataUsageId;
extern "C"  void pushButton_Start_m3144148455 (pushButton_t4005487804 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (pushButton_Start_m3144148455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3901280377, /*hidden argument*/NULL);
		__this->set_refObj_2(L_0);
		return;
	}
}
// System.Void pushButton::Update()
extern "C"  void pushButton_Update_m44191066 (pushButton_t4005487804 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void pushButton::OnClick()
extern const MethodInfo* GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266_MethodInfo_var;
extern const uint32_t pushButton_OnClick_m1687122768_MetadataUsageId;
extern "C"  void pushButton_OnClick_m1687122768 (pushButton_t4005487804 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (pushButton_OnClick_m1687122768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	banmen_t2510752313 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_refObj_2();
		NullCheck(L_0);
		banmen_t2510752313 * L_1 = GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266(L_0, /*hidden argument*/GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266_MethodInfo_var);
		V_0 = L_1;
		banmen_t2510752313 * L_2 = V_0;
		NullCheck(L_2);
		baseMethods_t2705876163 * L_3 = L_2->get_baseM_2();
		int32_t L_4 = __this->get_line_3();
		int32_t L_5 = __this->get_row_4();
		NullCheck(L_3);
		baseMethods_lineRow_m2784445234(L_3, L_4, L_5, /*hidden argument*/NULL);
		banmen_t2510752313 * L_6 = V_0;
		NullCheck(L_6);
		banmen_whichPlayer_m4081184614(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void pushButton::OnPointerEnter()
extern const MethodInfo* GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266_MethodInfo_var;
extern const uint32_t pushButton_OnPointerEnter_m3031795593_MetadataUsageId;
extern "C"  void pushButton_OnPointerEnter_m3031795593 (pushButton_t4005487804 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (pushButton_OnPointerEnter_m3031795593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	banmen_t2510752313 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_refObj_2();
		NullCheck(L_0);
		banmen_t2510752313 * L_1 = GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266(L_0, /*hidden argument*/GameObject_GetComponent_Tisbanmen_t2510752313_m3279592266_MethodInfo_var);
		V_0 = L_1;
		banmen_t2510752313 * L_2 = V_0;
		NullCheck(L_2);
		baseMethods_t2705876163 * L_3 = L_2->get_baseM_2();
		NullCheck(L_3);
		baseMethods_setColorBlack_m980638986(L_3, /*hidden argument*/NULL);
		banmen_t2510752313 * L_4 = V_0;
		NullCheck(L_4);
		baseMethods_t2705876163 * L_5 = L_4->get_baseM_2();
		int32_t L_6 = __this->get_line_3();
		int32_t L_7 = __this->get_row_4();
		NullCheck(L_5);
		baseMethods_printColorAbove_m553988101(L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void pushButton::OnPointerExit()
extern "C"  void pushButton_OnPointerExit_m2966859661 (pushButton_t4005487804 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SaveScript::.ctor()
extern "C"  void SaveScript__ctor_m3581886929 (SaveScript_t1729063510 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SaveScript::SaveText()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t SaveScript_SaveText_m374362473_MetadataUsageId;
extern "C"  void SaveScript_SaveText_m374362473 (SaveScript_t1729063510 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SaveScript_SaveText_m374362473_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InputField_t1631627530 * L_0 = __this->get_inputField_3();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m409351770(L_0, /*hidden argument*/NULL);
		__this->set_str_2(L_1);
		Text_t356221433 * L_2 = __this->get_text_4();
		String_t* L_3 = __this->get_str_2();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_3);
		InputField_t1631627530 * L_4 = __this->get_inputField_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_4);
		InputField_set_text_m114077119(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void selectKoma::.ctor()
extern "C"  void selectKoma__ctor_m3523864131 (selectKoma_t1760479012 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void selectKoma::Start()
extern Il2CppCodeGenString* _stringLiteral477942458;
extern const uint32_t selectKoma_Start_m913817551_MetadataUsageId;
extern "C"  void selectKoma_Start_m913817551 (selectKoma_t1760479012 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (selectKoma_Start_m913817551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral477942458, /*hidden argument*/NULL);
		__this->set_refObj_2(L_0);
		return;
	}
}
// System.Void selectKoma::Update()
extern "C"  void selectKoma_Update_m48568258 (selectKoma_t1760479012 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void selectKoma::OnClick()
extern const MethodInfo* GameObject_GetComponent_TisarrangeKoma_t3423810832_m115679363_MethodInfo_var;
extern const uint32_t selectKoma_OnClick_m1450290360_MetadataUsageId;
extern "C"  void selectKoma_OnClick_m1450290360 (selectKoma_t1760479012 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (selectKoma_OnClick_m1450290360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	arrangeKoma_t3423810832 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_refObj_2();
		NullCheck(L_0);
		arrangeKoma_t3423810832 * L_1 = GameObject_GetComponent_TisarrangeKoma_t3423810832_m115679363(L_0, /*hidden argument*/GameObject_GetComponent_TisarrangeKoma_t3423810832_m115679363_MethodInfo_var);
		V_0 = L_1;
		arrangeKoma_t3423810832 * L_2 = V_0;
		int32_t L_3 = __this->get_selectNum_3();
		NullCheck(L_2);
		arrangeKoma_inputKoma_m2346436234(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void selectPlace::.ctor()
extern "C"  void selectPlace__ctor_m2383638438 (selectPlace_t742894733 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void selectPlace::Start()
extern Il2CppCodeGenString* _stringLiteral477942458;
extern const uint32_t selectPlace_Start_m1096605630_MetadataUsageId;
extern "C"  void selectPlace_Start_m1096605630 (selectPlace_t742894733 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (selectPlace_Start_m1096605630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral477942458, /*hidden argument*/NULL);
		__this->set_refObj_2(L_0);
		return;
	}
}
// System.Void selectPlace::Update()
extern "C"  void selectPlace_Update_m3720865529 (selectPlace_t742894733 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void selectPlace::OnClick()
extern const MethodInfo* GameObject_GetComponent_TisarrangeKoma_t3423810832_m115679363_MethodInfo_var;
extern const uint32_t selectPlace_OnClick_m2105193043_MetadataUsageId;
extern "C"  void selectPlace_OnClick_m2105193043 (selectPlace_t742894733 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (selectPlace_OnClick_m2105193043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	arrangeKoma_t3423810832 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_refObj_2();
		NullCheck(L_0);
		arrangeKoma_t3423810832 * L_1 = GameObject_GetComponent_TisarrangeKoma_t3423810832_m115679363(L_0, /*hidden argument*/GameObject_GetComponent_TisarrangeKoma_t3423810832_m115679363_MethodInfo_var);
		V_0 = L_1;
		arrangeKoma_t3423810832 * L_2 = V_0;
		int32_t L_3 = __this->get_line_3();
		int32_t L_4 = __this->get_row_4();
		NullCheck(L_2);
		arrangeKoma_lineRow_m1340912179(L_2, L_3, L_4, /*hidden argument*/NULL);
		arrangeKoma_t3423810832 * L_5 = V_0;
		NullCheck(L_5);
		arrangeKoma_printKoma_m261318148(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void showData::.ctor()
extern "C"  void showData__ctor_m3344537568 (showData_t3631251573 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void showData::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSingleton_t1770047133_m2880166840_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2462408499;
extern Il2CppCodeGenString* _stringLiteral1713765239;
extern Il2CppCodeGenString* _stringLiteral238582248;
extern Il2CppCodeGenString* _stringLiteral151078324;
extern Il2CppCodeGenString* _stringLiteral4023369527;
extern Il2CppCodeGenString* _stringLiteral2342157100;
extern Il2CppCodeGenString* _stringLiteral784050957;
extern Il2CppCodeGenString* _stringLiteral372029305;
extern Il2CppCodeGenString* _stringLiteral930819712;
extern Il2CppCodeGenString* _stringLiteral3189088222;
extern Il2CppCodeGenString* _stringLiteral2471016498;
extern const uint32_t showData_Start_m2293015712_MetadataUsageId;
extern "C"  void showData_Start_m2293015712 (showData_t3631251573 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (showData_Start_m2293015712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2462408499, /*hidden argument*/NULL);
		__this->set_refObj_3(L_0);
		GameObject_t1756533147 * L_1 = __this->get_refObj_3();
		NullCheck(L_1);
		Singleton_t1770047133 * L_2 = GameObject_GetComponent_TisSingleton_t1770047133_m2880166840(L_1, /*hidden argument*/GameObject_GetComponent_TisSingleton_t1770047133_m2880166840_MethodInfo_var);
		__this->set_singleton_4(L_2);
		GameObject_t1756533147 * L_3 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1713765239, /*hidden argument*/NULL);
		NullCheck(L_3);
		Text_t356221433 * L_4 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_3, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_4);
		Text_t356221433 * L_5 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_t1770047133 * L_7 = __this->get_singleton_4();
		NullCheck(L_7);
		String_t* L_8 = Singleton_getAccountName_m4227199261(L_7, /*hidden argument*/NULL);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_9);
		GameObject_t1756533147 * L_10 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral238582248, /*hidden argument*/NULL);
		NullCheck(L_10);
		Text_t356221433 * L_11 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_10, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_11);
		Text_t356221433 * L_12 = __this->get_text_2();
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_t1770047133 * L_14 = __this->get_singleton_4();
		NullCheck(L_14);
		int32_t L_15 = Singleton_getNumOfWin_m3262476063(L_14, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_16);
		String_t* L_18 = String_Concat_m2000667605(NULL /*static, unused*/, L_13, L_17, _stringLiteral151078324, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_18);
		GameObject_t1756533147 * L_19 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral4023369527, /*hidden argument*/NULL);
		NullCheck(L_19);
		Text_t356221433 * L_20 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_19, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_20);
		Text_t356221433 * L_21 = __this->get_text_2();
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_t1770047133 * L_23 = __this->get_singleton_4();
		NullCheck(L_23);
		int32_t L_24 = Singleton_getNumOfLose_m3025985308(L_23, /*hidden argument*/NULL);
		int32_t L_25 = L_24;
		Il2CppObject * L_26 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_25);
		String_t* L_27 = String_Concat_m2000667605(NULL /*static, unused*/, L_22, L_26, _stringLiteral2342157100, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, L_27);
		GameObject_t1756533147 * L_28 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral784050957, /*hidden argument*/NULL);
		NullCheck(L_28);
		Text_t356221433 * L_29 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_28, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_29);
		Singleton_t1770047133 * L_30 = __this->get_singleton_4();
		NullCheck(L_30);
		int32_t L_31 = Singleton_getNumOfWin_m3262476063(L_30, /*hidden argument*/NULL);
		Singleton_t1770047133 * L_32 = __this->get_singleton_4();
		NullCheck(L_32);
		int32_t L_33 = Singleton_getNumOfLose_m3025985308(L_32, /*hidden argument*/NULL);
		if (((int32_t)((int32_t)L_31+(int32_t)L_33)))
		{
			goto IL_011a;
		}
	}
	{
		Text_t356221433 * L_34 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_34);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_34, L_35);
		goto IL_018a;
	}

IL_011a:
	{
		Singleton_t1770047133 * L_36 = __this->get_singleton_4();
		NullCheck(L_36);
		int32_t L_37 = Singleton_getNumOfWin_m3262476063(L_36, /*hidden argument*/NULL);
		Singleton_t1770047133 * L_38 = __this->get_singleton_4();
		NullCheck(L_38);
		int32_t L_39 = Singleton_getNumOfWin_m3262476063(L_38, /*hidden argument*/NULL);
		Singleton_t1770047133 * L_40 = __this->get_singleton_4();
		NullCheck(L_40);
		int32_t L_41 = Singleton_getNumOfLose_m3025985308(L_40, /*hidden argument*/NULL);
		__this->set_average_5(((double)((double)((double)((double)(1.0)*(double)(((double)((double)L_37)))))/(double)(((double)((double)((int32_t)((int32_t)L_39+(int32_t)L_41))))))));
		double L_42 = __this->get_average_5();
		__this->set_average_5(((double)((double)L_42*(double)(100.0))));
		Text_t356221433 * L_43 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		double L_45 = __this->get_average_5();
		double L_46 = L_45;
		Il2CppObject * L_47 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_46);
		String_t* L_48 = String_Concat_m2000667605(NULL /*static, unused*/, L_44, L_47, _stringLiteral372029305, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_43, L_48);
	}

IL_018a:
	{
		GameObject_t1756533147 * L_49 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral930819712, /*hidden argument*/NULL);
		NullCheck(L_49);
		Text_t356221433 * L_50 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_49, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_50);
		Text_t356221433 * L_51 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_52 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_t1770047133 * L_53 = __this->get_singleton_4();
		NullCheck(L_53);
		int32_t L_54 = Singleton_getRensyoNum_m68857382(L_53, /*hidden argument*/NULL);
		int32_t L_55 = L_54;
		Il2CppObject * L_56 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_55);
		String_t* L_57 = String_Concat_m2000667605(NULL /*static, unused*/, L_52, L_56, _stringLiteral3189088222, /*hidden argument*/NULL);
		NullCheck(L_51);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_51, L_57);
		GameObject_t1756533147 * L_58 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2471016498, /*hidden argument*/NULL);
		NullCheck(L_58);
		Text_t356221433 * L_59 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_58, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_59);
		Text_t356221433 * L_60 = __this->get_text_2();
		String_t* L_61 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_t1770047133 * L_62 = __this->get_singleton_4();
		NullCheck(L_62);
		int32_t L_63 = Singleton_getMaxRensyoNum_m1807931810(L_62, /*hidden argument*/NULL);
		int32_t L_64 = L_63;
		Il2CppObject * L_65 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_64);
		String_t* L_66 = String_Concat_m2000667605(NULL /*static, unused*/, L_61, L_65, _stringLiteral3189088222, /*hidden argument*/NULL);
		NullCheck(L_60);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_60, L_66);
		return;
	}
}
// System.Void showData::reShow()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSingleton_t1770047133_m2880166840_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2462408499;
extern Il2CppCodeGenString* _stringLiteral1713765239;
extern Il2CppCodeGenString* _stringLiteral238582248;
extern Il2CppCodeGenString* _stringLiteral151078324;
extern Il2CppCodeGenString* _stringLiteral4023369527;
extern Il2CppCodeGenString* _stringLiteral2342157100;
extern Il2CppCodeGenString* _stringLiteral784050957;
extern Il2CppCodeGenString* _stringLiteral372029305;
extern Il2CppCodeGenString* _stringLiteral930819712;
extern Il2CppCodeGenString* _stringLiteral3189088222;
extern Il2CppCodeGenString* _stringLiteral2471016498;
extern const uint32_t showData_reShow_m3436802746_MetadataUsageId;
extern "C"  void showData_reShow_m3436802746 (showData_t3631251573 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (showData_reShow_m3436802746_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2462408499, /*hidden argument*/NULL);
		__this->set_refObj_3(L_0);
		GameObject_t1756533147 * L_1 = __this->get_refObj_3();
		NullCheck(L_1);
		Singleton_t1770047133 * L_2 = GameObject_GetComponent_TisSingleton_t1770047133_m2880166840(L_1, /*hidden argument*/GameObject_GetComponent_TisSingleton_t1770047133_m2880166840_MethodInfo_var);
		__this->set_singleton_4(L_2);
		GameObject_t1756533147 * L_3 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1713765239, /*hidden argument*/NULL);
		NullCheck(L_3);
		Text_t356221433 * L_4 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_3, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_4);
		Text_t356221433 * L_5 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_t1770047133 * L_7 = __this->get_singleton_4();
		NullCheck(L_7);
		String_t* L_8 = Singleton_getAccountName_m4227199261(L_7, /*hidden argument*/NULL);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_9);
		GameObject_t1756533147 * L_10 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral238582248, /*hidden argument*/NULL);
		NullCheck(L_10);
		Text_t356221433 * L_11 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_10, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_11);
		Text_t356221433 * L_12 = __this->get_text_2();
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_t1770047133 * L_14 = __this->get_singleton_4();
		NullCheck(L_14);
		int32_t L_15 = Singleton_getNumOfWin_m3262476063(L_14, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_16);
		String_t* L_18 = String_Concat_m2000667605(NULL /*static, unused*/, L_13, L_17, _stringLiteral151078324, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_18);
		GameObject_t1756533147 * L_19 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral4023369527, /*hidden argument*/NULL);
		NullCheck(L_19);
		Text_t356221433 * L_20 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_19, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_20);
		Text_t356221433 * L_21 = __this->get_text_2();
		String_t* L_22 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_t1770047133 * L_23 = __this->get_singleton_4();
		NullCheck(L_23);
		int32_t L_24 = Singleton_getNumOfLose_m3025985308(L_23, /*hidden argument*/NULL);
		int32_t L_25 = L_24;
		Il2CppObject * L_26 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_25);
		String_t* L_27 = String_Concat_m2000667605(NULL /*static, unused*/, L_22, L_26, _stringLiteral2342157100, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, L_27);
		GameObject_t1756533147 * L_28 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral784050957, /*hidden argument*/NULL);
		NullCheck(L_28);
		Text_t356221433 * L_29 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_28, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_29);
		Singleton_t1770047133 * L_30 = __this->get_singleton_4();
		NullCheck(L_30);
		int32_t L_31 = Singleton_getNumOfWin_m3262476063(L_30, /*hidden argument*/NULL);
		Singleton_t1770047133 * L_32 = __this->get_singleton_4();
		NullCheck(L_32);
		int32_t L_33 = Singleton_getNumOfLose_m3025985308(L_32, /*hidden argument*/NULL);
		if (((int32_t)((int32_t)L_31+(int32_t)L_33)))
		{
			goto IL_011a;
		}
	}
	{
		Text_t356221433 * L_34 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_34);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_34, L_35);
		goto IL_018a;
	}

IL_011a:
	{
		Singleton_t1770047133 * L_36 = __this->get_singleton_4();
		NullCheck(L_36);
		int32_t L_37 = Singleton_getNumOfWin_m3262476063(L_36, /*hidden argument*/NULL);
		Singleton_t1770047133 * L_38 = __this->get_singleton_4();
		NullCheck(L_38);
		int32_t L_39 = Singleton_getNumOfWin_m3262476063(L_38, /*hidden argument*/NULL);
		Singleton_t1770047133 * L_40 = __this->get_singleton_4();
		NullCheck(L_40);
		int32_t L_41 = Singleton_getNumOfLose_m3025985308(L_40, /*hidden argument*/NULL);
		__this->set_average_5(((double)((double)((double)((double)(1.0)*(double)(((double)((double)L_37)))))/(double)(((double)((double)((int32_t)((int32_t)L_39+(int32_t)L_41))))))));
		double L_42 = __this->get_average_5();
		__this->set_average_5(((double)((double)L_42*(double)(100.0))));
		Text_t356221433 * L_43 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		double L_45 = __this->get_average_5();
		double L_46 = L_45;
		Il2CppObject * L_47 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_46);
		String_t* L_48 = String_Concat_m2000667605(NULL /*static, unused*/, L_44, L_47, _stringLiteral372029305, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_43, L_48);
	}

IL_018a:
	{
		GameObject_t1756533147 * L_49 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral930819712, /*hidden argument*/NULL);
		NullCheck(L_49);
		Text_t356221433 * L_50 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_49, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_50);
		Text_t356221433 * L_51 = __this->get_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_52 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_t1770047133 * L_53 = __this->get_singleton_4();
		NullCheck(L_53);
		int32_t L_54 = Singleton_getRensyoNum_m68857382(L_53, /*hidden argument*/NULL);
		int32_t L_55 = L_54;
		Il2CppObject * L_56 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_55);
		String_t* L_57 = String_Concat_m2000667605(NULL /*static, unused*/, L_52, L_56, _stringLiteral3189088222, /*hidden argument*/NULL);
		NullCheck(L_51);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_51, L_57);
		GameObject_t1756533147 * L_58 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2471016498, /*hidden argument*/NULL);
		NullCheck(L_58);
		Text_t356221433 * L_59 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_58, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_text_2(L_59);
		Text_t356221433 * L_60 = __this->get_text_2();
		String_t* L_61 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_t1770047133 * L_62 = __this->get_singleton_4();
		NullCheck(L_62);
		int32_t L_63 = Singleton_getMaxRensyoNum_m1807931810(L_62, /*hidden argument*/NULL);
		int32_t L_64 = L_63;
		Il2CppObject * L_65 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_64);
		String_t* L_66 = String_Concat_m2000667605(NULL /*static, unused*/, L_61, L_65, _stringLiteral3189088222, /*hidden argument*/NULL);
		NullCheck(L_60);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_60, L_66);
		return;
	}
}
// System.Void showData::Update()
extern "C"  void showData_Update_m263602305 (showData_t3631251573 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Singleton::.ctor()
extern "C"  void Singleton__ctor_m676879296 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton::Awake()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Singleton_t1770047133_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Singleton_Awake_m3364215385_MetadataUsageId;
extern "C"  void Singleton_Awake_m3364215385 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_Awake_m3364215385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_0 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = DateTime_ToString_m1117481977((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = Random_Range_m694320887(NULL /*static, unused*/, ((int32_t)100), ((int32_t)1000), /*hidden argument*/NULL);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_t1770047133_il2cpp_TypeInfo_var);
		bool L_6 = ((Singleton_t1770047133_StaticFields*)Singleton_t1770047133_il2cpp_TypeInfo_var->static_fields)->get_existsInstance_2();
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}

IL_0044:
	{
		Singleton_Init_m3535994028(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Singleton_t1770047133_il2cpp_TypeInfo_var);
		((Singleton_t1770047133_StaticFields*)Singleton_t1770047133_il2cpp_TypeInfo_var->static_fields)->set_existsInstance_2((bool)1);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton::soundOff()
extern "C"  void Singleton_soundOff_m842928774 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	{
		AudioListener_set_volume_m1233107753(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton::soundOn()
extern "C"  void Singleton_soundOn_m1062527022 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	{
		AudioListener_set_volume_m1233107753(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton::setAccountName(System.String)
extern Il2CppCodeGenString* _stringLiteral2039374422;
extern const uint32_t Singleton_setAccountName_m4172620012_MetadataUsageId;
extern "C"  void Singleton_setAccountName_m4172620012 (Singleton_t1770047133 * __this, String_t* ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_setAccountName_m4172620012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral2039374422, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton::setInitBool(System.Int32)
extern Il2CppCodeGenString* _stringLiteral2139206996;
extern const uint32_t Singleton_setInitBool_m2992378539_MetadataUsageId;
extern "C"  void Singleton_setInitBool_m2992378539 (Singleton_t1770047133 * __this, int32_t ___num0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_setInitBool_m2992378539_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___num0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2139206996, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton::setNumOfWin(System.Int32)
extern Il2CppCodeGenString* _stringLiteral3120483719;
extern const uint32_t Singleton_setNumOfWin_m3889254428_MetadataUsageId;
extern "C"  void Singleton_setNumOfWin_m3889254428 (Singleton_t1770047133 * __this, int32_t ___num0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_setNumOfWin_m3889254428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___num0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral3120483719, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton::setNumOfLose(System.Int32)
extern Il2CppCodeGenString* _stringLiteral3391556362;
extern const uint32_t Singleton_setNumOfLose_m162589789_MetadataUsageId;
extern "C"  void Singleton_setNumOfLose_m162589789 (Singleton_t1770047133 * __this, int32_t ___num0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_setNumOfLose_m162589789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___num0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral3391556362, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton::setRensyoNum(System.Int32)
extern Il2CppCodeGenString* _stringLiteral449492432;
extern const uint32_t Singleton_setRensyoNum_m309863713_MetadataUsageId;
extern "C"  void Singleton_setRensyoNum_m309863713 (Singleton_t1770047133 * __this, int32_t ___num0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_setRensyoNum_m309863713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___num0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral449492432, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton::setMaxRensyoNum(System.Int32)
extern Il2CppCodeGenString* _stringLiteral4068519142;
extern const uint32_t Singleton_setMaxRensyoNum_m2081612021_MetadataUsageId;
extern "C"  void Singleton_setMaxRensyoNum_m2081612021 (Singleton_t1770047133 * __this, int32_t ___num0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_setMaxRensyoNum_m2081612021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___num0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral4068519142, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton::setUniqueId(System.String)
extern Il2CppCodeGenString* _stringLiteral7599308;
extern const uint32_t Singleton_setUniqueId_m652611108_MetadataUsageId;
extern "C"  void Singleton_setUniqueId_m652611108 (Singleton_t1770047133 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_setUniqueId_m652611108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___id0;
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral7599308, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Singleton::getAccountName()
extern Il2CppCodeGenString* _stringLiteral2039374422;
extern const uint32_t Singleton_getAccountName_m4227199261_MetadataUsageId;
extern "C"  String_t* Singleton_getAccountName_m4227199261 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_getAccountName_m4227199261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, _stringLiteral2039374422, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Singleton::getInitBool()
extern Il2CppCodeGenString* _stringLiteral2139206996;
extern const uint32_t Singleton_getInitBool_m1924461332_MetadataUsageId;
extern "C"  int32_t Singleton_getInitBool_m1924461332 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_getInitBool_m1924461332_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2139206996, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Singleton::getNumOfWin()
extern Il2CppCodeGenString* _stringLiteral3120483719;
extern const uint32_t Singleton_getNumOfWin_m3262476063_MetadataUsageId;
extern "C"  int32_t Singleton_getNumOfWin_m3262476063 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_getNumOfWin_m3262476063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral3120483719, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Singleton::getNumOfLose()
extern Il2CppCodeGenString* _stringLiteral3391556362;
extern const uint32_t Singleton_getNumOfLose_m3025985308_MetadataUsageId;
extern "C"  int32_t Singleton_getNumOfLose_m3025985308 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_getNumOfLose_m3025985308_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral3391556362, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Singleton::getRensyoNum()
extern Il2CppCodeGenString* _stringLiteral449492432;
extern const uint32_t Singleton_getRensyoNum_m68857382_MetadataUsageId;
extern "C"  int32_t Singleton_getRensyoNum_m68857382 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_getRensyoNum_m68857382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral449492432, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Singleton::getMaxRensyoNum()
extern Il2CppCodeGenString* _stringLiteral4068519142;
extern const uint32_t Singleton_getMaxRensyoNum_m1807931810_MetadataUsageId;
extern "C"  int32_t Singleton_getMaxRensyoNum_m1807931810 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_getMaxRensyoNum_m1807931810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral4068519142, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String Singleton::getUniqueId()
extern Il2CppCodeGenString* _stringLiteral7599308;
extern const uint32_t Singleton_getUniqueId_m206607097_MetadataUsageId;
extern "C"  String_t* Singleton_getUniqueId_m206607097 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_getUniqueId_m206607097_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, _stringLiteral7599308, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Singleton::Init()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Singleton_Init_m3535994028_MetadataUsageId;
extern "C"  void Singleton_Init_m3535994028 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Singleton_Init_m3535994028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = Singleton_getInitBool_m1924461332(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0068;
		}
	}
	{
		Singleton_setNumOfWin_m3889254428(__this, 0, /*hidden argument*/NULL);
		Singleton_setNumOfLose_m162589789(__this, 0, /*hidden argument*/NULL);
		Singleton_setRensyoNum_m309863713(__this, 0, /*hidden argument*/NULL);
		Singleton_setMaxRensyoNum_m2081612021(__this, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Singleton_setAccountName_m4172620012(__this, L_1, /*hidden argument*/NULL);
		Singleton_setInitBool_m2992378539(__this, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_2 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = DateTime_ToString_m1117481977((&V_0), /*hidden argument*/NULL);
		int32_t L_4 = Random_Range_m694320887(NULL /*static, unused*/, ((int32_t)100), ((int32_t)1000), /*hidden argument*/NULL);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7 = String_Concat_m56707527(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		Singleton_setUniqueId_m652611108(__this, L_7, /*hidden argument*/NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void Singleton::CountWinLose(System.Boolean)
extern "C"  void Singleton_CountWinLose_m4154155941 (Singleton_t1770047133 * __this, bool ___win0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool L_0 = ___win0;
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_1 = Singleton_getNumOfWin_m3262476063(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
		int32_t L_3 = V_0;
		Singleton_setNumOfWin_m3889254428(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Singleton_getRensyoNum_m68857382(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
		int32_t L_6 = V_1;
		Singleton_setRensyoNum_m309863713(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		int32_t L_8 = Singleton_getMaxRensyoNum_m1807931810(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)L_8)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_9 = V_1;
		Singleton_setMaxRensyoNum_m2081612021(__this, L_9, /*hidden argument*/NULL);
	}

IL_003d:
	{
		goto IL_005b;
	}

IL_0042:
	{
		int32_t L_10 = Singleton_getNumOfLose_m3025985308(__this, /*hidden argument*/NULL);
		V_2 = L_10;
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
		int32_t L_12 = V_2;
		Singleton_setNumOfLose_m162589789(__this, L_12, /*hidden argument*/NULL);
		Singleton_setRensyoNum_m309863713(__this, 0, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void Singleton::Save()
extern "C"  void Singleton_Save_m574479149 (Singleton_t1770047133 * __this, const MethodInfo* method)
{
	{
		PlayerPrefs_Save_m212854761(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Singleton::.cctor()
extern "C"  void Singleton__cctor_m926949583 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Skytree::.ctor()
extern "C"  void Skytree__ctor_m1984419468 (Skytree_t850646749 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Skytree::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3222005778;
extern Il2CppCodeGenString* _stringLiteral2477782847;
extern const uint32_t Skytree_Init_m2468843630_MetadataUsageId;
extern "C"  void Skytree_Init_m2468843630 (Skytree_t850646749 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Skytree_Init_m2468843630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral3222005778, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2477782847, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(7);
		((Koma_t3011766596 *)__this)->set_resources_3(((int32_t)10));
		return;
	}
}
// System.Void SmoothCameraOrbit::.ctor()
extern "C"  void SmoothCameraOrbit__ctor_m2399836750 (SmoothCameraOrbit_t604892967 * __this, const MethodInfo* method)
{
	{
		__this->set_distance_4((5.0f));
		__this->set_maxDistance_5((20.0f));
		__this->set_minDistance_6((0.6f));
		__this->set_xSpeed_7((200.0f));
		__this->set_ySpeed_8((200.0f));
		__this->set_yMinLimit_9(((int32_t)-80));
		__this->set_yMaxLimit_10(((int32_t)80));
		__this->set_zoomRate_11(((int32_t)40));
		__this->set_panSpeed_12((0.3f));
		__this->set_zoomDampening_13((5.0f));
		__this->set_autoRotate_14((1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmoothCameraOrbit::Start()
extern "C"  void SmoothCameraOrbit_Start_m1776320706 (SmoothCameraOrbit_t604892967 * __this, const MethodInfo* method)
{
	{
		SmoothCameraOrbit_Init_m1057359038(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmoothCameraOrbit::OnEnable()
extern "C"  void SmoothCameraOrbit_OnEnable_m4129528578 (SmoothCameraOrbit_t604892967 * __this, const MethodInfo* method)
{
	{
		SmoothCameraOrbit_Init_m1057359038(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmoothCameraOrbit::Init()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral200192950;
extern const uint32_t SmoothCameraOrbit_Init_m1057359038_MetadataUsageId;
extern "C"  void SmoothCameraOrbit_Init_m1057359038 (SmoothCameraOrbit_t604892967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothCameraOrbit_Init_m1057359038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = __this->get_target_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0058;
		}
	}
	{
		GameObject_t1756533147 * L_2 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_2, _stringLiteral200192950, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t2243707580  L_6 = Transform_get_position_m1104419803(L_5, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_forward_m1833488937(L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_distance_4();
		Vector3_t2243707580  L_10 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_6, L_10, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_m2469242620(L_4, L_11, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_12 = V_0;
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = GameObject_get_transform_m909382139(L_12, /*hidden argument*/NULL);
		__this->set_target_2(L_13);
	}

IL_0058:
	{
		float L_14 = __this->get_distance_4();
		__this->set_currentDistance_17(L_14);
		float L_15 = __this->get_distance_4();
		__this->set_desiredDistance_18(L_15);
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t2243707580  L_17 = Transform_get_position_m1104419803(L_16, /*hidden argument*/NULL);
		__this->set_position_22(L_17);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Quaternion_t4030073918  L_19 = Transform_get_rotation_m1033555130(L_18, /*hidden argument*/NULL);
		__this->set_rotation_21(L_19);
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Quaternion_t4030073918  L_21 = Transform_get_rotation_m1033555130(L_20, /*hidden argument*/NULL);
		__this->set_currentRotation_19(L_21);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Quaternion_t4030073918  L_23 = Transform_get_rotation_m1033555130(L_22, /*hidden argument*/NULL);
		__this->set_desiredRotation_20(L_23);
		Vector3_t2243707580  L_24 = Vector3_get_right_m1884123822(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t2243707580  L_26 = Transform_get_right_m440863970(L_25, /*hidden argument*/NULL);
		float L_27 = Vector3_Angle_m2552334978(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		__this->set_xDeg_15(L_27);
		Vector3_t2243707580  L_28 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t2243707580  L_30 = Transform_get_up_m1603627763(L_29, /*hidden argument*/NULL);
		float L_31 = Vector3_Angle_m2552334978(NULL /*static, unused*/, L_28, L_30, /*hidden argument*/NULL);
		__this->set_yDeg_16(L_31);
		Transform_t3275118058 * L_32 = __this->get_target_2();
		NullCheck(L_32);
		Vector3_t2243707580  L_33 = Transform_get_position_m1104419803(L_32, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_34 = __this->get_rotation_21();
		Vector3_t2243707580  L_35 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_36 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		float L_37 = __this->get_currentDistance_17();
		Vector3_t2243707580  L_38 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		Vector3_t2243707580  L_39 = __this->get_targetOffset_3();
		Vector3_t2243707580  L_40 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_33, L_40, /*hidden argument*/NULL);
		__this->set_position_22(L_41);
		return;
	}
}
// System.Void SmoothCameraOrbit::LateUpdate()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1307534078;
extern Il2CppCodeGenString* _stringLiteral1307534077;
extern Il2CppCodeGenString* _stringLiteral1684512047;
extern const uint32_t SmoothCameraOrbit_LateUpdate_m1656838375_MetadataUsageId;
extern "C"  void SmoothCameraOrbit_LateUpdate_m1656838375 (SmoothCameraOrbit_t604892967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothCameraOrbit_LateUpdate_m1656838375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0065;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)308), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0065;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)306), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0065;
		}
	}
	{
		float L_3 = __this->get_desiredDistance_18();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_4 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534078, /*hidden argument*/NULL);
		float L_5 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_zoomRate_11();
		float L_7 = __this->get_desiredDistance_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_8 = fabsf(L_7);
		__this->set_desiredDistance_18(((float)((float)L_3-(float)((float)((float)((float)((float)((float)((float)((float)((float)L_4*(float)L_5))*(float)(((float)((float)L_6)))))*(float)(0.125f)))*(float)L_8)))));
		goto IL_0266;
	}

IL_0065:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_9 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0153;
		}
	}
	{
		float L_10 = __this->get_xDeg_15();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_11 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534077, /*hidden argument*/NULL);
		float L_12 = __this->get_xSpeed_7();
		__this->set_xDeg_15(((float)((float)L_10+(float)((float)((float)((float)((float)L_11*(float)L_12))*(float)(0.02f))))));
		float L_13 = __this->get_yDeg_16();
		float L_14 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1307534078, /*hidden argument*/NULL);
		float L_15 = __this->get_ySpeed_8();
		__this->set_yDeg_16(((float)((float)L_13-(float)((float)((float)((float)((float)L_14*(float)L_15))*(float)(0.02f))))));
		float L_16 = __this->get_yDeg_16();
		int32_t L_17 = __this->get_yMinLimit_9();
		int32_t L_18 = __this->get_yMaxLimit_10();
		float L_19 = SmoothCameraOrbit_ClampAngle_m922049763(NULL /*static, unused*/, L_16, (((float)((float)L_17))), (((float)((float)L_18))), /*hidden argument*/NULL);
		__this->set_yDeg_16(L_19);
		float L_20 = __this->get_yDeg_16();
		float L_21 = __this->get_xDeg_15();
		Quaternion_t4030073918  L_22 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, L_20, L_21, (0.0f), /*hidden argument*/NULL);
		__this->set_desiredRotation_20(L_22);
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Quaternion_t4030073918  L_24 = Transform_get_rotation_m1033555130(L_23, /*hidden argument*/NULL);
		__this->set_currentRotation_19(L_24);
		Quaternion_t4030073918  L_25 = __this->get_currentRotation_19();
		Quaternion_t4030073918  L_26 = __this->get_desiredRotation_20();
		float L_27 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_28 = __this->get_zoomDampening_13();
		Quaternion_t4030073918  L_29 = Quaternion_Lerp_m3623055223(NULL /*static, unused*/, L_25, L_26, ((float)((float)L_27*(float)L_28)), /*hidden argument*/NULL);
		__this->set_rotation_21(L_29);
		Transform_t3275118058 * L_30 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_31 = __this->get_rotation_21();
		NullCheck(L_30);
		Transform_set_rotation_m3411284563(L_30, L_31, /*hidden argument*/NULL);
		__this->set_idleTimer_23((0.0f));
		__this->set_idleSmooth_24((0.0f));
		goto IL_0266;
	}

IL_0153:
	{
		float L_32 = __this->get_idleTimer_23();
		float L_33 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_idleTimer_23(((float)((float)L_32+(float)L_33)));
		float L_34 = __this->get_idleTimer_23();
		float L_35 = __this->get_autoRotate_14();
		if ((!(((float)L_34) > ((float)L_35))))
		{
			goto IL_01e0;
		}
	}
	{
		float L_36 = __this->get_autoRotate_14();
		if ((!(((float)L_36) > ((float)(0.0f)))))
		{
			goto IL_01e0;
		}
	}
	{
		float L_37 = __this->get_idleSmooth_24();
		float L_38 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_39 = __this->get_idleSmooth_24();
		__this->set_idleSmooth_24(((float)((float)L_37+(float)((float)((float)((float)((float)L_38+(float)L_39))*(float)(0.005f))))));
		float L_40 = __this->get_idleSmooth_24();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_41 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_40, (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_idleSmooth_24(L_41);
		float L_42 = __this->get_xDeg_15();
		float L_43 = __this->get_xSpeed_7();
		float L_44 = __this->get_idleSmooth_24();
		__this->set_xDeg_15(((float)((float)L_42+(float)((float)((float)((float)((float)L_43*(float)(0.001f)))*(float)L_44)))));
	}

IL_01e0:
	{
		float L_45 = __this->get_yDeg_16();
		int32_t L_46 = __this->get_yMinLimit_9();
		int32_t L_47 = __this->get_yMaxLimit_10();
		float L_48 = SmoothCameraOrbit_ClampAngle_m922049763(NULL /*static, unused*/, L_45, (((float)((float)L_46))), (((float)((float)L_47))), /*hidden argument*/NULL);
		__this->set_yDeg_16(L_48);
		float L_49 = __this->get_yDeg_16();
		float L_50 = __this->get_xDeg_15();
		Quaternion_t4030073918  L_51 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, L_49, L_50, (0.0f), /*hidden argument*/NULL);
		__this->set_desiredRotation_20(L_51);
		Transform_t3275118058 * L_52 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_52);
		Quaternion_t4030073918  L_53 = Transform_get_rotation_m1033555130(L_52, /*hidden argument*/NULL);
		__this->set_currentRotation_19(L_53);
		Quaternion_t4030073918  L_54 = __this->get_currentRotation_19();
		Quaternion_t4030073918  L_55 = __this->get_desiredRotation_20();
		float L_56 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_57 = __this->get_zoomDampening_13();
		Quaternion_t4030073918  L_58 = Quaternion_Lerp_m3623055223(NULL /*static, unused*/, L_54, L_55, ((float)((float)((float)((float)L_56*(float)L_57))*(float)(2.0f))), /*hidden argument*/NULL);
		__this->set_rotation_21(L_58);
		Transform_t3275118058 * L_59 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_60 = __this->get_rotation_21();
		NullCheck(L_59);
		Transform_set_rotation_m3411284563(L_59, L_60, /*hidden argument*/NULL);
	}

IL_0266:
	{
		float L_61 = __this->get_desiredDistance_18();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_62 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1684512047, /*hidden argument*/NULL);
		float L_63 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_64 = __this->get_zoomRate_11();
		float L_65 = __this->get_desiredDistance_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_66 = fabsf(L_65);
		__this->set_desiredDistance_18(((float)((float)L_61-(float)((float)((float)((float)((float)((float)((float)L_62*(float)L_63))*(float)(((float)((float)L_64)))))*(float)L_66)))));
		float L_67 = __this->get_desiredDistance_18();
		float L_68 = __this->get_minDistance_6();
		float L_69 = __this->get_maxDistance_5();
		float L_70 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_67, L_68, L_69, /*hidden argument*/NULL);
		__this->set_desiredDistance_18(L_70);
		float L_71 = __this->get_currentDistance_17();
		float L_72 = __this->get_desiredDistance_18();
		float L_73 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_74 = __this->get_zoomDampening_13();
		float L_75 = Mathf_Lerp_m1686556575(NULL /*static, unused*/, L_71, L_72, ((float)((float)L_73*(float)L_74)), /*hidden argument*/NULL);
		__this->set_currentDistance_17(L_75);
		Transform_t3275118058 * L_76 = __this->get_target_2();
		NullCheck(L_76);
		Vector3_t2243707580  L_77 = Transform_get_position_m1104419803(L_76, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_78 = __this->get_rotation_21();
		Vector3_t2243707580  L_79 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_80 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_78, L_79, /*hidden argument*/NULL);
		float L_81 = __this->get_currentDistance_17();
		Vector3_t2243707580  L_82 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_80, L_81, /*hidden argument*/NULL);
		Vector3_t2243707580  L_83 = __this->get_targetOffset_3();
		Vector3_t2243707580  L_84 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_82, L_83, /*hidden argument*/NULL);
		Vector3_t2243707580  L_85 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_77, L_84, /*hidden argument*/NULL);
		__this->set_position_22(L_85);
		Transform_t3275118058 * L_86 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_87 = __this->get_position_22();
		NullCheck(L_86);
		Transform_set_position_m2469242620(L_86, L_87, /*hidden argument*/NULL);
		return;
	}
}
// System.Single SmoothCameraOrbit::ClampAngle(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SmoothCameraOrbit_ClampAngle_m922049763_MetadataUsageId;
extern "C"  float SmoothCameraOrbit_ClampAngle_m922049763 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothCameraOrbit_ClampAngle_m922049763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___angle0;
		if ((!(((float)L_0) < ((float)(-360.0f)))))
		{
			goto IL_0014;
		}
	}
	{
		float L_1 = ___angle0;
		___angle0 = ((float)((float)L_1+(float)(360.0f)));
	}

IL_0014:
	{
		float L_2 = ___angle0;
		if ((!(((float)L_2) > ((float)(360.0f)))))
		{
			goto IL_0028;
		}
	}
	{
		float L_3 = ___angle0;
		___angle0 = ((float)((float)L_3-(float)(360.0f)));
	}

IL_0028:
	{
		float L_4 = ___angle0;
		float L_5 = ___min1;
		float L_6 = ___max2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void soundOnOff::.ctor()
extern "C"  void soundOnOff__ctor_m2820888082 (soundOnOff_t408115747 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void soundOnOff::Start()
extern Il2CppCodeGenString* _stringLiteral2462408499;
extern const uint32_t soundOnOff_Start_m3976287054_MetadataUsageId;
extern "C"  void soundOnOff_Start_m3976287054 (soundOnOff_t408115747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (soundOnOff_Start_m3976287054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral2462408499, /*hidden argument*/NULL);
		__this->set_obj_2(L_0);
		__this->set_onOff_3((bool)0);
		return;
	}
}
// System.Void soundOnOff::Update()
extern "C"  void soundOnOff_Update_m3653385803 (soundOnOff_t408115747 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void soundOnOff::OnClick()
extern const MethodInfo* GameObject_GetComponent_TisSingleton_t1770047133_m2880166840_MethodInfo_var;
extern const uint32_t soundOnOff_OnClick_m1428459721_MetadataUsageId;
extern "C"  void soundOnOff_OnClick_m1428459721 (soundOnOff_t408115747 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (soundOnOff_OnClick_m1428459721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Singleton_t1770047133 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = __this->get_obj_2();
		NullCheck(L_0);
		Singleton_t1770047133 * L_1 = GameObject_GetComponent_TisSingleton_t1770047133_m2880166840(L_0, /*hidden argument*/GameObject_GetComponent_TisSingleton_t1770047133_m2880166840_MethodInfo_var);
		V_0 = L_1;
		bool L_2 = __this->get_onOff_3();
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Singleton_t1770047133 * L_3 = V_0;
		NullCheck(L_3);
		Singleton_soundOn_m1062527022(L_3, /*hidden argument*/NULL);
		__this->set_onOff_3((bool)0);
		goto IL_0036;
	}

IL_0029:
	{
		Singleton_t1770047133 * L_4 = V_0;
		NullCheck(L_4);
		Singleton_soundOff_m842928774(L_4, /*hidden argument*/NULL);
		__this->set_onOff_3((bool)1);
	}

IL_0036:
	{
		return;
	}
}
// System.Void SRobot::.ctor()
extern "C"  void SRobot__ctor_m1465433094 (SRobot_t456246757 * __this, const MethodInfo* method)
{
	{
		Koma__ctor_m1354333883(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SRobot::Init(System.String)
extern const MethodInfo* Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2055881876;
extern Il2CppCodeGenString* _stringLiteral1671489855;
extern const uint32_t SRobot_Init_m2528330196_MetadataUsageId;
extern "C"  void SRobot_Init_m2528330196 (SRobot_t456246757 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SRobot_Init_m2528330196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___str0;
		Koma_Init_m2356132927(__this, L_0, /*hidden argument*/NULL);
		Sprite_t309593783 * L_1 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral2055881876, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_komaView_1(L_1);
		Sprite_t309593783 * L_2 = Resources_Load_TisSprite_t309593783_m1838468444(NULL /*static, unused*/, _stringLiteral1671489855, /*hidden argument*/Resources_Load_TisSprite_t309593783_m1838468444_MethodInfo_var);
		((Koma_t3011766596 *)__this)->set_brokeKomaView_4(L_2);
		((Koma_t3011766596 *)__this)->set_komaNumber_2(3);
		((Koma_t3011766596 *)__this)->set_resources_3(4);
		((Koma_t3011766596 *)__this)->set_komaMaxTime_5(((int32_t)40));
		return;
	}
}
// System.Void SRobot::Move(System.Int32,System.Int32)
extern "C"  void SRobot_Move_m2909660533 (SRobot_t456246757 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck(L_0);
		banmen_isThereKoma_m207992560(L_0, L_1, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_3 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_4 = ___l0;
		int32_t L_5 = ___r1;
		NullCheck(L_3);
		banmen_isThereKoma_m207992560(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), ((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_6 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_7 = ___l0;
		int32_t L_8 = ___r1;
		NullCheck(L_6);
		banmen_isThereKoma_m207992560(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_9 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_10 = ___l0;
		int32_t L_11 = ___r1;
		NullCheck(L_9);
		banmen_isThereKoma_m207992560(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)), ((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_12 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_13 = ___l0;
		int32_t L_14 = ___r1;
		NullCheck(L_12);
		banmen_isThereKoma_m207992560(L_12, ((int32_t)((int32_t)L_13+(int32_t)1)), ((int32_t)((int32_t)L_14+(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SRobot::Draw(System.Int32,System.Int32)
extern "C"  void SRobot_Draw_m1781548374 (SRobot_t456246757 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck(L_0);
		banmen_isThereDraw_m900625674(L_0, L_1, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_3 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_4 = ___l0;
		int32_t L_5 = ___r1;
		NullCheck(L_3);
		banmen_isThereDraw_m900625674(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), ((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_6 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_7 = ___l0;
		int32_t L_8 = ___r1;
		NullCheck(L_6);
		banmen_isThereDraw_m900625674(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_9 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_10 = ___l0;
		int32_t L_11 = ___r1;
		NullCheck(L_9);
		banmen_isThereDraw_m900625674(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)), ((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_12 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_13 = ___l0;
		int32_t L_14 = ___r1;
		NullCheck(L_12);
		banmen_isThereDraw_m900625674(L_12, ((int32_t)((int32_t)L_13+(int32_t)1)), ((int32_t)((int32_t)L_14+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_15 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_15);
		L_15->set_wall_32((bool)1);
		return;
	}
}
// System.Void SRobot::Erase(System.Int32,System.Int32)
extern "C"  void SRobot_Erase_m4091973232 (SRobot_t456246757 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method)
{
	{
		banmen_t2510752313 * L_0 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_1 = ___l0;
		int32_t L_2 = ___r1;
		NullCheck(L_0);
		banmen_isThereErase_m2459852564(L_0, L_1, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_3 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_4 = ___l0;
		int32_t L_5 = ___r1;
		NullCheck(L_3);
		banmen_isThereErase_m2459852564(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), ((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_6 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_7 = ___l0;
		int32_t L_8 = ___r1;
		NullCheck(L_6);
		banmen_isThereErase_m2459852564(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_9 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_10 = ___l0;
		int32_t L_11 = ___r1;
		NullCheck(L_9);
		banmen_isThereErase_m2459852564(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)), ((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_12 = ((Koma_t3011766596 *)__this)->get_ban_0();
		int32_t L_13 = ___l0;
		int32_t L_14 = ___r1;
		NullCheck(L_12);
		banmen_isThereErase_m2459852564(L_12, ((int32_t)((int32_t)L_13+(int32_t)1)), ((int32_t)((int32_t)L_14+(int32_t)1)), /*hidden argument*/NULL);
		banmen_t2510752313 * L_15 = ((Koma_t3011766596 *)__this)->get_ban_0();
		NullCheck(L_15);
		L_15->set_wall_32((bool)1);
		return;
	}
}
// System.Void StableAspect::.ctor()
extern "C"  void StableAspect__ctor_m227441362 (StableAspect_t3385526103 * __this, const MethodInfo* method)
{
	{
		__this->set_width_3((640.0f));
		__this->set_height_4((960.0f));
		__this->set_pixelPerUnit_5((100.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StableAspect::Awake()
extern const MethodInfo* Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var;
extern const uint32_t StableAspect_Awake_m3893144231_MetadataUsageId;
extern "C"  void StableAspect_Awake_m3893144231 (StableAspect_t3385526103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StableAspect_Awake_m3893144231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		int32_t L_0 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)(((float)((float)L_0)))/(float)(((float)((float)L_1)))));
		float L_2 = __this->get_height_4();
		float L_3 = __this->get_width_3();
		V_1 = ((float)((float)L_2/(float)L_3));
		Camera_t189460977 * L_4 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_MethodInfo_var);
		__this->set_cam_2(L_4);
		Camera_t189460977 * L_5 = __this->get_cam_2();
		float L_6 = __this->get_height_4();
		float L_7 = __this->get_pixelPerUnit_5();
		NullCheck(L_5);
		Camera_set_orthographicSize_m2708824189(L_5, ((float)((float)((float)((float)L_6/(float)(2.0f)))/(float)L_7)), /*hidden argument*/NULL);
		float L_8 = V_1;
		float L_9 = V_0;
		if ((!(((float)L_8) > ((float)L_9))))
		{
			goto IL_0098;
		}
	}
	{
		float L_10 = __this->get_height_4();
		int32_t L_11 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_10/(float)(((float)((float)L_11)))));
		float L_12 = __this->get_width_3();
		int32_t L_13 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = V_2;
		V_3 = ((float)((float)L_12/(float)((float)((float)(((float)((float)L_13)))*(float)L_14))));
		Camera_t189460977 * L_15 = __this->get_cam_2();
		float L_16 = V_3;
		float L_17 = V_3;
		Rect_t3681755626  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Rect__ctor_m1220545469(&L_18, ((float)((float)((float)((float)(1.0f)-(float)L_16))/(float)(2.0f))), (0.0f), L_17, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Camera_set_rect_m1838810502(L_15, L_18, /*hidden argument*/NULL);
		goto IL_00e3;
	}

IL_0098:
	{
		float L_19 = __this->get_width_3();
		int32_t L_20 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = ((float)((float)L_19/(float)(((float)((float)L_20)))));
		float L_21 = __this->get_height_4();
		int32_t L_22 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = V_4;
		V_5 = ((float)((float)L_21/(float)((float)((float)(((float)((float)L_22)))*(float)L_23))));
		Camera_t189460977 * L_24 = __this->get_cam_2();
		float L_25 = V_5;
		float L_26 = V_5;
		Rect_t3681755626  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Rect__ctor_m1220545469(&L_27, (0.0f), ((float)((float)((float)((float)(1.0f)-(float)L_25))/(float)(2.0f))), (1.0f), L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		Camera_set_rect_m1838810502(L_24, L_27, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		return;
	}
}
// System.Void startToggle::.ctor()
extern "C"  void startToggle__ctor_m1931058105 (startToggle_t123871076 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void startToggle::Start()
extern const MethodInfo* Component_GetComponent_TisToggle_t3976754468_m3994209854_MethodInfo_var;
extern const uint32_t startToggle_Start_m1519248745_MetadataUsageId;
extern "C"  void startToggle_Start_m1519248745 (startToggle_t123871076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (startToggle_Start_m1519248745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Toggle_t3976754468 * L_0 = Component_GetComponent_TisToggle_t3976754468_m3994209854(__this, /*hidden argument*/Component_GetComponent_TisToggle_t3976754468_m3994209854_MethodInfo_var);
		__this->set_toggle_2(L_0);
		Toggle_t3976754468 * L_1 = __this->get_toggle_2();
		NullCheck(L_1);
		Toggle_set_isOn_m4022556286(L_1, (bool)0, /*hidden argument*/NULL);
		Toggle_t3976754468 * L_2 = __this->get_toggle_2();
		NullCheck(L_2);
		Toggle_set_isOn_m4022556286(L_2, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void startToggle::Update()
extern "C"  void startToggle_Update_m2149680814 (startToggle_t123871076 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
