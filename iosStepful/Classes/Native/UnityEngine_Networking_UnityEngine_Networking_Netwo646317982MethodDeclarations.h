﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkBroadcastResult
struct NetworkBroadcastResult_t646317982;
struct NetworkBroadcastResult_t646317982_marshaled_pinvoke;
struct NetworkBroadcastResult_t646317982_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct NetworkBroadcastResult_t646317982;
struct NetworkBroadcastResult_t646317982_marshaled_pinvoke;

extern "C" void NetworkBroadcastResult_t646317982_marshal_pinvoke(const NetworkBroadcastResult_t646317982& unmarshaled, NetworkBroadcastResult_t646317982_marshaled_pinvoke& marshaled);
extern "C" void NetworkBroadcastResult_t646317982_marshal_pinvoke_back(const NetworkBroadcastResult_t646317982_marshaled_pinvoke& marshaled, NetworkBroadcastResult_t646317982& unmarshaled);
extern "C" void NetworkBroadcastResult_t646317982_marshal_pinvoke_cleanup(NetworkBroadcastResult_t646317982_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct NetworkBroadcastResult_t646317982;
struct NetworkBroadcastResult_t646317982_marshaled_com;

extern "C" void NetworkBroadcastResult_t646317982_marshal_com(const NetworkBroadcastResult_t646317982& unmarshaled, NetworkBroadcastResult_t646317982_marshaled_com& marshaled);
extern "C" void NetworkBroadcastResult_t646317982_marshal_com_back(const NetworkBroadcastResult_t646317982_marshaled_com& marshaled, NetworkBroadcastResult_t646317982& unmarshaled);
extern "C" void NetworkBroadcastResult_t646317982_marshal_com_cleanup(NetworkBroadcastResult_t646317982_marshaled_com& marshaled);
