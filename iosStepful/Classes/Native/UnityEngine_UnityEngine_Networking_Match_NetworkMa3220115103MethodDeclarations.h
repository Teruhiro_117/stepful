﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMat945754175MethodDeclarations.h"

// System.Void UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<UnityEngine.Networking.Match.MatchInfo>::.ctor(System.Object,System.IntPtr)
#define DataResponseDelegate_1__ctor_m1222355432(__this, ___object0, ___method1, method) ((  void (*) (DataResponseDelegate_1_t3220115103 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DataResponseDelegate_1__ctor_m2222754223_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<UnityEngine.Networking.Match.MatchInfo>::Invoke(System.Boolean,System.String,T)
#define DataResponseDelegate_1_Invoke_m3541062195(__this, ___success0, ___extendedInfo1, ___responseData2, method) ((  void (*) (DataResponseDelegate_1_t3220115103 *, bool, String_t*, MatchInfo_t668842927 *, const MethodInfo*))DataResponseDelegate_1_Invoke_m2892839852_gshared)(__this, ___success0, ___extendedInfo1, ___responseData2, method)
// System.IAsyncResult UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<UnityEngine.Networking.Match.MatchInfo>::BeginInvoke(System.Boolean,System.String,T,System.AsyncCallback,System.Object)
#define DataResponseDelegate_1_BeginInvoke_m3891644296(__this, ___success0, ___extendedInfo1, ___responseData2, ___callback3, ___object4, method) ((  Il2CppObject * (*) (DataResponseDelegate_1_t3220115103 *, bool, String_t*, MatchInfo_t668842927 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))DataResponseDelegate_1_BeginInvoke_m2356550577_gshared)(__this, ___success0, ___extendedInfo1, ___responseData2, ___callback3, ___object4, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<UnityEngine.Networking.Match.MatchInfo>::EndInvoke(System.IAsyncResult)
#define DataResponseDelegate_1_EndInvoke_m367161960(__this, ___result0, method) ((  void (*) (DataResponseDelegate_1_t3220115103 *, Il2CppObject *, const MethodInfo*))DataResponseDelegate_1_EndInvoke_m1252280981_gshared)(__this, ___result0, method)
