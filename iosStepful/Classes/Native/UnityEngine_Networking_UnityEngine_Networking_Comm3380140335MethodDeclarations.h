﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.CommandAttribute
struct CommandAttribute_t3380140335;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.CommandAttribute::.ctor()
extern "C"  void CommandAttribute__ctor_m258512434 (CommandAttribute_t3380140335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
