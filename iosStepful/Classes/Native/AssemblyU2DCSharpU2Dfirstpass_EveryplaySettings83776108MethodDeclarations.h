﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplaySettings
struct EveryplaySettings_t83776108;

#include "codegen/il2cpp-codegen.h"

// System.Void EveryplaySettings::.ctor()
extern "C"  void EveryplaySettings__ctor_m1650251901 (EveryplaySettings_t83776108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EveryplaySettings::get_IsEnabled()
extern "C"  bool EveryplaySettings_get_IsEnabled_m4208345999 (EveryplaySettings_t83776108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EveryplaySettings::get_IsValid()
extern "C"  bool EveryplaySettings_get_IsValid_m586503010 (EveryplaySettings_t83776108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
