﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.UInt32>
struct List_1_t1518803153;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1053532827.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m596314019_gshared (Enumerator_t1053532827 * __this, List_1_t1518803153 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m596314019(__this, ___l0, method) ((  void (*) (Enumerator_t1053532827 *, List_1_t1518803153 *, const MethodInfo*))Enumerator__ctor_m596314019_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3430271327_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3430271327(__this, method) ((  void (*) (Enumerator_t1053532827 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3430271327_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3845423283_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3845423283(__this, method) ((  Il2CppObject * (*) (Enumerator_t1053532827 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3845423283_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::Dispose()
extern "C"  void Enumerator_Dispose_m3971230368_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3971230368(__this, method) ((  void (*) (Enumerator_t1053532827 *, const MethodInfo*))Enumerator_Dispose_m3971230368_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m3938742477_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m3938742477(__this, method) ((  void (*) (Enumerator_t1053532827 *, const MethodInfo*))Enumerator_VerifyState_m3938742477_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.UInt32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1386285227_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1386285227(__this, method) ((  bool (*) (Enumerator_t1053532827 *, const MethodInfo*))Enumerator_MoveNext_m1386285227_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.UInt32>::get_Current()
extern "C"  uint32_t Enumerator_get_Current_m3018462446_gshared (Enumerator_t1053532827 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3018462446(__this, method) ((  uint32_t (*) (Enumerator_t1053532827 *, const MethodInfo*))Enumerator_get_Current_m3018462446_gshared)(__this, method)
