﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.ClientScene/PendingOwner>
struct DefaultComparer_t3662056448;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clien877818919.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.ClientScene/PendingOwner>::.ctor()
extern "C"  void DefaultComparer__ctor_m399243965_gshared (DefaultComparer_t3662056448 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m399243965(__this, method) ((  void (*) (DefaultComparer_t3662056448 *, const MethodInfo*))DefaultComparer__ctor_m399243965_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.ClientScene/PendingOwner>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m112615914_gshared (DefaultComparer_t3662056448 * __this, PendingOwner_t877818919  ___x0, PendingOwner_t877818919  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m112615914(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3662056448 *, PendingOwner_t877818919 , PendingOwner_t877818919 , const MethodInfo*))DefaultComparer_Compare_m112615914_gshared)(__this, ___x0, ___y1, method)
