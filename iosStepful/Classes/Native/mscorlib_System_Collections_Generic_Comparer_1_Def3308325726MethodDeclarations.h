﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct DefaultComparer_t3308325726;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo524088197.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.ctor()
extern "C"  void DefaultComparer__ctor_m479087423_gshared (DefaultComparer_t3308325726 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m479087423(__this, method) ((  void (*) (DefaultComparer_t3308325726 *, const MethodInfo*))DefaultComparer__ctor_m479087423_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3213737252_gshared (DefaultComparer_t3308325726 * __this, CRCMessageEntry_t524088197  ___x0, CRCMessageEntry_t524088197  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3213737252(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3308325726 *, CRCMessageEntry_t524088197 , CRCMessageEntry_t524088197 , const MethodInfo*))DefaultComparer_Compare_m3213737252_gshared)(__this, ___x0, ___y1, method)
