﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct ReadOnlyCollection_1_t1063604611;
// System.Collections.Generic.IList`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct IList_1_t1418759520;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.ClientScene/PendingOwner[]
struct PendingOwnerU5BU5D_t2720009054;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct IEnumerator_1_t2648310042;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clien877818919.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2974270769_gshared (ReadOnlyCollection_1_t1063604611 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2974270769(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2974270769_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m82666915_gshared (ReadOnlyCollection_1_t1063604611 * __this, PendingOwner_t877818919  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m82666915(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, PendingOwner_t877818919 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m82666915_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2762974247_gshared (ReadOnlyCollection_1_t1063604611 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2762974247(__this, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2762974247_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m837116382_gshared (ReadOnlyCollection_1_t1063604611 * __this, int32_t ___index0, PendingOwner_t877818919  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m837116382(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, int32_t, PendingOwner_t877818919 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m837116382_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2303997816_gshared (ReadOnlyCollection_1_t1063604611 * __this, PendingOwner_t877818919  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2303997816(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1063604611 *, PendingOwner_t877818919 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2303997816_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1790403466_gshared (ReadOnlyCollection_1_t1063604611 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1790403466(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1790403466_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  PendingOwner_t877818919  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3597886036_gshared (ReadOnlyCollection_1_t1063604611 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3597886036(__this, ___index0, method) ((  PendingOwner_t877818919  (*) (ReadOnlyCollection_1_t1063604611 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3597886036_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2553057503_gshared (ReadOnlyCollection_1_t1063604611 * __this, int32_t ___index0, PendingOwner_t877818919  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2553057503(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, int32_t, PendingOwner_t877818919 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2553057503_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1475243283_gshared (ReadOnlyCollection_1_t1063604611 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1475243283(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1063604611 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1475243283_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3168577224_gshared (ReadOnlyCollection_1_t1063604611 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3168577224(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3168577224_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m549211591_gshared (ReadOnlyCollection_1_t1063604611 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m549211591(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1063604611 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m549211591_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3415187780_gshared (ReadOnlyCollection_1_t1063604611 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3415187780(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1063604611 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3415187780_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m232149966_gshared (ReadOnlyCollection_1_t1063604611 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m232149966(__this, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m232149966_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2843553898_gshared (ReadOnlyCollection_1_t1063604611 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2843553898(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1063604611 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2843553898_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4071247890_gshared (ReadOnlyCollection_1_t1063604611 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4071247890(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1063604611 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m4071247890_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1484078375_gshared (ReadOnlyCollection_1_t1063604611 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1484078375(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1484078375_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1936549687_gshared (ReadOnlyCollection_1_t1063604611 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1936549687(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1936549687_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3875314945_gshared (ReadOnlyCollection_1_t1063604611 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3875314945(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3875314945_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m278331604_gshared (ReadOnlyCollection_1_t1063604611 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m278331604(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1063604611 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m278331604_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m12382200_gshared (ReadOnlyCollection_1_t1063604611 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m12382200(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1063604611 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m12382200_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3852874527_gshared (ReadOnlyCollection_1_t1063604611 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3852874527(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1063604611 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3852874527_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3585072696_gshared (ReadOnlyCollection_1_t1063604611 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3585072696(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1063604611 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3585072696_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1504085071_gshared (ReadOnlyCollection_1_t1063604611 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1504085071(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1063604611 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1504085071_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3884668694_gshared (ReadOnlyCollection_1_t1063604611 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3884668694(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3884668694_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m310030837_gshared (ReadOnlyCollection_1_t1063604611 * __this, PendingOwner_t877818919  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m310030837(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1063604611 *, PendingOwner_t877818919 , const MethodInfo*))ReadOnlyCollection_1_Contains_m310030837_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2279614871_gshared (ReadOnlyCollection_1_t1063604611 * __this, PendingOwnerU5BU5D_t2720009054* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2279614871(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1063604611 *, PendingOwnerU5BU5D_t2720009054*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2279614871_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2749337064_gshared (ReadOnlyCollection_1_t1063604611 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2749337064(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1063604611 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2749337064_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m1395288931_gshared (ReadOnlyCollection_1_t1063604611 * __this, PendingOwner_t877818919  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m1395288931(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1063604611 *, PendingOwner_t877818919 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m1395288931_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m571880084_gshared (ReadOnlyCollection_1_t1063604611 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m571880084(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1063604611 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m571880084_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ClientScene/PendingOwner>::get_Item(System.Int32)
extern "C"  PendingOwner_t877818919  ReadOnlyCollection_1_get_Item_m932897960_gshared (ReadOnlyCollection_1_t1063604611 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m932897960(__this, ___index0, method) ((  PendingOwner_t877818919  (*) (ReadOnlyCollection_1_t1063604611 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m932897960_gshared)(__this, ___index0, method)
