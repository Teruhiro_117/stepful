﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.HostTopologyInternal
struct HostTopologyInternal_t516387376;
// UnityEngine.Networking.HostTopology
struct HostTopology_t3602650143;
// UnityEngine.Networking.ConnectionConfigInternal
struct ConnectionConfigInternal_t197008027;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_HostTopology3602650143.h"
#include "UnityEngine_UnityEngine_Networking_ConnectionConfig197008027.h"

// System.Void UnityEngine.Networking.HostTopologyInternal::.ctor(UnityEngine.Networking.HostTopology)
extern "C"  void HostTopologyInternal__ctor_m115265193 (HostTopologyInternal_t516387376 * __this, HostTopology_t3602650143 * ___topology0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.HostTopologyInternal::InitWrapper(UnityEngine.Networking.ConnectionConfigInternal,System.Int32)
extern "C"  void HostTopologyInternal_InitWrapper_m2830130791 (HostTopologyInternal_t516387376 * __this, ConnectionConfigInternal_t197008027 * ___config0, int32_t ___maxDefaultConnections1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.HostTopologyInternal::AddSpecialConnectionConfig(UnityEngine.Networking.ConnectionConfigInternal)
extern "C"  int32_t HostTopologyInternal_AddSpecialConnectionConfig_m2487040073 (HostTopologyInternal_t516387376 * __this, ConnectionConfigInternal_t197008027 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.HostTopologyInternal::AddSpecialConnectionConfigWrapper(UnityEngine.Networking.ConnectionConfigInternal)
extern "C"  int32_t HostTopologyInternal_AddSpecialConnectionConfigWrapper_m349563044 (HostTopologyInternal_t516387376 * __this, ConnectionConfigInternal_t197008027 * ___config0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.HostTopologyInternal::InitOtherParameters(UnityEngine.Networking.HostTopology)
extern "C"  void HostTopologyInternal_InitOtherParameters_m2884554997 (HostTopologyInternal_t516387376 * __this, HostTopology_t3602650143 * ___topology0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.HostTopologyInternal::InitReceivedPoolSize(System.UInt16)
extern "C"  void HostTopologyInternal_InitReceivedPoolSize_m3372106925 (HostTopologyInternal_t516387376 * __this, uint16_t ___pool0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.HostTopologyInternal::InitSentMessagePoolSize(System.UInt16)
extern "C"  void HostTopologyInternal_InitSentMessagePoolSize_m2049462741 (HostTopologyInternal_t516387376 * __this, uint16_t ___pool0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.HostTopologyInternal::InitMessagePoolSizeGrowthFactor(System.Single)
extern "C"  void HostTopologyInternal_InitMessagePoolSizeGrowthFactor_m2731826350 (HostTopologyInternal_t516387376 * __this, float ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.HostTopologyInternal::Dispose()
extern "C"  void HostTopologyInternal_Dispose_m1274467438 (HostTopologyInternal_t516387376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.HostTopologyInternal::Finalize()
extern "C"  void HostTopologyInternal_Finalize_m2903411093 (HostTopologyInternal_t516387376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
