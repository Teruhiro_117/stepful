﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkManagerHUD
struct NetworkManagerHUD_t1541348254;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.NetworkManagerHUD::.ctor()
extern "C"  void NetworkManagerHUD__ctor_m4090077201 (NetworkManagerHUD_t1541348254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkManagerHUD::Awake()
extern "C"  void NetworkManagerHUD_Awake_m3789377298 (NetworkManagerHUD_t1541348254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkManagerHUD::Update()
extern "C"  void NetworkManagerHUD_Update_m3584758340 (NetworkManagerHUD_t1541348254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkManagerHUD::OnGUI()
extern "C"  void NetworkManagerHUD_OnGUI_m698402647 (NetworkManagerHUD_t1541348254 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
