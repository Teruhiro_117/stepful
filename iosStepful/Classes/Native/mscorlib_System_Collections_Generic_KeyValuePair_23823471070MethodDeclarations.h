﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23823471070.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2070884156_gshared (KeyValuePair_2_t3823471070 * __this, int16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2070884156(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3823471070 *, int16_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m2070884156_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::get_Key()
extern "C"  int16_t KeyValuePair_2_get_Key_m2738192346_gshared (KeyValuePair_2_t3823471070 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2738192346(__this, method) ((  int16_t (*) (KeyValuePair_2_t3823471070 *, const MethodInfo*))KeyValuePair_2_get_Key_m2738192346_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4076108421_gshared (KeyValuePair_2_t3823471070 * __this, int16_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m4076108421(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3823471070 *, int16_t, const MethodInfo*))KeyValuePair_2_set_Key_m4076108421_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m902390722_gshared (KeyValuePair_2_t3823471070 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m902390722(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3823471070 *, const MethodInfo*))KeyValuePair_2_get_Value_m902390722_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4268247485_gshared (KeyValuePair_2_t3823471070 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m4268247485(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3823471070 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m4268247485_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int16,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m878106415_gshared (KeyValuePair_2_t3823471070 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m878106415(__this, method) ((  String_t* (*) (KeyValuePair_2_t3823471070 *, const MethodInfo*))KeyValuePair_2_ToString_m878106415_gshared)(__this, method)
