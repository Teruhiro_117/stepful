﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ESRobot
struct ESRobot_t2546702684;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ESRobot::.ctor()
extern "C"  void ESRobot__ctor_m4281893077 (ESRobot_t2546702684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ESRobot::Init(System.String)
extern "C"  void ESRobot_Init_m3417921989 (ESRobot_t2546702684 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ESRobot::Move(System.Int32,System.Int32)
extern "C"  void ESRobot_Move_m1220482802 (ESRobot_t2546702684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ESRobot::Draw(System.Int32,System.Int32)
extern "C"  void ESRobot_Draw_m3193670653 (ESRobot_t2546702684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ESRobot::Erase(System.Int32,System.Int32)
extern "C"  void ESRobot_Erase_m4105753849 (ESRobot_t2546702684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
