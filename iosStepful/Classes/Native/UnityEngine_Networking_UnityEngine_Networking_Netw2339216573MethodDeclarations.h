﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkMessage
struct NetworkMessage_t2339216573;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.NetworkMessage::.ctor()
extern "C"  void NetworkMessage__ctor_m1233751920 (NetworkMessage_t2339216573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.NetworkMessage::Dump(System.Byte[],System.Int32)
extern "C"  String_t* NetworkMessage_Dump_m1200375675 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___payload0, int32_t ___sz1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
