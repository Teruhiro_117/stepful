﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.HashSet`1<UnityEngine.Networking.NetworkInstanceId>
struct HashSet_1_t2662426982;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Collections_Generic_HashSet_1_E1150742824.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Networ33998832.h"

// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkInstanceId>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C"  void Enumerator__ctor_m1263474025_gshared (Enumerator_t1150742824 * __this, HashSet_1_t2662426982 * ___hashset0, const MethodInfo* method);
#define Enumerator__ctor_m1263474025(__this, ___hashset0, method) ((  void (*) (Enumerator_t1150742824 *, HashSet_1_t2662426982 *, const MethodInfo*))Enumerator__ctor_m1263474025_gshared)(__this, ___hashset0, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkInstanceId>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m590394547_gshared (Enumerator_t1150742824 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m590394547(__this, method) ((  Il2CppObject * (*) (Enumerator_t1150742824 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m590394547_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkInstanceId>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2706822983_gshared (Enumerator_t1150742824 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2706822983(__this, method) ((  void (*) (Enumerator_t1150742824 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2706822983_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkInstanceId>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m514625158_gshared (Enumerator_t1150742824 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m514625158(__this, method) ((  bool (*) (Enumerator_t1150742824 *, const MethodInfo*))Enumerator_MoveNext_m514625158_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkInstanceId>::get_Current()
extern "C"  NetworkInstanceId_t33998832  Enumerator_get_Current_m1440506732_gshared (Enumerator_t1150742824 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1440506732(__this, method) ((  NetworkInstanceId_t33998832  (*) (Enumerator_t1150742824 *, const MethodInfo*))Enumerator_get_Current_m1440506732_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkInstanceId>::Dispose()
extern "C"  void Enumerator_Dispose_m3328587452_gshared (Enumerator_t1150742824 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3328587452(__this, method) ((  void (*) (Enumerator_t1150742824 *, const MethodInfo*))Enumerator_Dispose_m3328587452_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.Networking.NetworkInstanceId>::CheckState()
extern "C"  void Enumerator_CheckState_m3258916632_gshared (Enumerator_t1150742824 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m3258916632(__this, method) ((  void (*) (Enumerator_t1150742824 *, const MethodInfo*))Enumerator_CheckState_m3258916632_gshared)(__this, method)
