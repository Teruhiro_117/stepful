﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay
struct Everyplay_t1799077027;
// Everyplay/WasClosedDelegate
struct WasClosedDelegate_t946300984;
// Everyplay/ReadyForRecordingDelegate
struct ReadyForRecordingDelegate_t1593758596;
// Everyplay/RecordingStartedDelegate
struct RecordingStartedDelegate_t5060419;
// Everyplay/RecordingStoppedDelegate
struct RecordingStoppedDelegate_t3008025639;
// Everyplay/FaceCamSessionStartedDelegate
struct FaceCamSessionStartedDelegate_t1733547424;
// Everyplay/FaceCamRecordingPermissionDelegate
struct FaceCamRecordingPermissionDelegate_t1670731619;
// Everyplay/FaceCamSessionStoppedDelegate
struct FaceCamSessionStoppedDelegate_t1894731428;
// Everyplay/ThumbnailReadyAtTextureIdDelegate
struct ThumbnailReadyAtTextureIdDelegate_t3853410271;
// Everyplay/ThumbnailTextureReadyDelegate
struct ThumbnailTextureReadyDelegate_t2948235259;
// Everyplay/UploadDidStartDelegate
struct UploadDidStartDelegate_t1871027361;
// Everyplay/UploadDidProgressDelegate
struct UploadDidProgressDelegate_t2069570344;
// Everyplay/UploadDidCompleteDelegate
struct UploadDidCompleteDelegate_t1564565876;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// Everyplay/RequestReadyDelegate
struct RequestReadyDelegate_t4013578405;
// Everyplay/RequestFailedDelegate
struct RequestFailedDelegate_t3135434785;
// System.Object
struct Il2CppObject;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_WasClosedDe946300984.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_ReadyForRe1593758596.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RecordingStar5060419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RecordingS3008025639.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamSes1733547424.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamRec1670731619.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamSes1894731428.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_ThumbnailR3853410271.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_ThumbnailT2948235259.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_UploadDidS1871027361.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_UploadDidP2069570344.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_UploadDidC1564565876.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RequestRea4013578405.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RequestFai3135434785.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamRec3536210470.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamPre2262691368.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void Everyplay::.ctor()
extern "C"  void Everyplay__ctor_m1083411806 (Everyplay_t1799077027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_WasClosed(Everyplay/WasClosedDelegate)
extern "C"  void Everyplay_add_WasClosed_m3659009735 (Il2CppObject * __this /* static, unused */, WasClosedDelegate_t946300984 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_WasClosed(Everyplay/WasClosedDelegate)
extern "C"  void Everyplay_remove_WasClosed_m483262024 (Il2CppObject * __this /* static, unused */, WasClosedDelegate_t946300984 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_ReadyForRecording(Everyplay/ReadyForRecordingDelegate)
extern "C"  void Everyplay_add_ReadyForRecording_m3347212935 (Il2CppObject * __this /* static, unused */, ReadyForRecordingDelegate_t1593758596 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_ReadyForRecording(Everyplay/ReadyForRecordingDelegate)
extern "C"  void Everyplay_remove_ReadyForRecording_m557751688 (Il2CppObject * __this /* static, unused */, ReadyForRecordingDelegate_t1593758596 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_RecordingStarted(Everyplay/RecordingStartedDelegate)
extern "C"  void Everyplay_add_RecordingStarted_m1975802831 (Il2CppObject * __this /* static, unused */, RecordingStartedDelegate_t5060419 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_RecordingStarted(Everyplay/RecordingStartedDelegate)
extern "C"  void Everyplay_remove_RecordingStarted_m1408178652 (Il2CppObject * __this /* static, unused */, RecordingStartedDelegate_t5060419 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_RecordingStopped(Everyplay/RecordingStoppedDelegate)
extern "C"  void Everyplay_add_RecordingStopped_m2601012783 (Il2CppObject * __this /* static, unused */, RecordingStoppedDelegate_t3008025639 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_RecordingStopped(Everyplay/RecordingStoppedDelegate)
extern "C"  void Everyplay_remove_RecordingStopped_m550227836 (Il2CppObject * __this /* static, unused */, RecordingStoppedDelegate_t3008025639 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_FaceCamSessionStarted(Everyplay/FaceCamSessionStartedDelegate)
extern "C"  void Everyplay_add_FaceCamSessionStarted_m3896409671 (Il2CppObject * __this /* static, unused */, FaceCamSessionStartedDelegate_t1733547424 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_FaceCamSessionStarted(Everyplay/FaceCamSessionStartedDelegate)
extern "C"  void Everyplay_remove_FaceCamSessionStarted_m2342726600 (Il2CppObject * __this /* static, unused */, FaceCamSessionStartedDelegate_t1733547424 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_FaceCamRecordingPermission(Everyplay/FaceCamRecordingPermissionDelegate)
extern "C"  void Everyplay_add_FaceCamRecordingPermission_m574143351 (Il2CppObject * __this /* static, unused */, FaceCamRecordingPermissionDelegate_t1670731619 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_FaceCamRecordingPermission(Everyplay/FaceCamRecordingPermissionDelegate)
extern "C"  void Everyplay_remove_FaceCamRecordingPermission_m4283361868 (Il2CppObject * __this /* static, unused */, FaceCamRecordingPermissionDelegate_t1670731619 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_FaceCamSessionStopped(Everyplay/FaceCamSessionStoppedDelegate)
extern "C"  void Everyplay_add_FaceCamSessionStopped_m2491956679 (Il2CppObject * __this /* static, unused */, FaceCamSessionStoppedDelegate_t1894731428 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_FaceCamSessionStopped(Everyplay/FaceCamSessionStoppedDelegate)
extern "C"  void Everyplay_remove_FaceCamSessionStopped_m3063767112 (Il2CppObject * __this /* static, unused */, FaceCamSessionStoppedDelegate_t1894731428 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_ThumbnailReadyAtTextureId(Everyplay/ThumbnailReadyAtTextureIdDelegate)
extern "C"  void Everyplay_add_ThumbnailReadyAtTextureId_m1102520263 (Il2CppObject * __this /* static, unused */, ThumbnailReadyAtTextureIdDelegate_t3853410271 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_ThumbnailReadyAtTextureId(Everyplay/ThumbnailReadyAtTextureIdDelegate)
extern "C"  void Everyplay_remove_ThumbnailReadyAtTextureId_m2712126024 (Il2CppObject * __this /* static, unused */, ThumbnailReadyAtTextureIdDelegate_t3853410271 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_ThumbnailTextureReady(Everyplay/ThumbnailTextureReadyDelegate)
extern "C"  void Everyplay_add_ThumbnailTextureReady_m1784642183 (Il2CppObject * __this /* static, unused */, ThumbnailTextureReadyDelegate_t2948235259 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_ThumbnailTextureReady(Everyplay/ThumbnailTextureReadyDelegate)
extern "C"  void Everyplay_remove_ThumbnailTextureReady_m3139354760 (Il2CppObject * __this /* static, unused */, ThumbnailTextureReadyDelegate_t2948235259 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_UploadDidStart(Everyplay/UploadDidStartDelegate)
extern "C"  void Everyplay_add_UploadDidStart_m2150585455 (Il2CppObject * __this /* static, unused */, UploadDidStartDelegate_t1871027361 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_UploadDidStart(Everyplay/UploadDidStartDelegate)
extern "C"  void Everyplay_remove_UploadDidStart_m1799453144 (Il2CppObject * __this /* static, unused */, UploadDidStartDelegate_t1871027361 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_UploadDidProgress(Everyplay/UploadDidProgressDelegate)
extern "C"  void Everyplay_add_UploadDidProgress_m1561866631 (Il2CppObject * __this /* static, unused */, UploadDidProgressDelegate_t2069570344 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_UploadDidProgress(Everyplay/UploadDidProgressDelegate)
extern "C"  void Everyplay_remove_UploadDidProgress_m4200454792 (Il2CppObject * __this /* static, unused */, UploadDidProgressDelegate_t2069570344 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::add_UploadDidComplete(Everyplay/UploadDidCompleteDelegate)
extern "C"  void Everyplay_add_UploadDidComplete_m176217159 (Il2CppObject * __this /* static, unused */, UploadDidCompleteDelegate_t1564565876 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::remove_UploadDidComplete(Everyplay/UploadDidCompleteDelegate)
extern "C"  void Everyplay_remove_UploadDidComplete_m3377286600 (Il2CppObject * __this /* static, unused */, UploadDidCompleteDelegate_t1564565876 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Everyplay Everyplay::get_EveryplayInstance()
extern "C"  Everyplay_t1799077027 * Everyplay_get_EveryplayInstance_m3310382687 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::Initialize()
extern "C"  void Everyplay_Initialize_m1735770310 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::Show()
extern "C"  void Everyplay_Show_m681744495 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::ShowWithPath(System.String)
extern "C"  void Everyplay_ShowWithPath_m3146621956 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::PlayVideoWithURL(System.String)
extern "C"  void Everyplay_PlayVideoWithURL_m504561604 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::PlayVideoWithDictionary(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Everyplay_PlayVideoWithDictionary_m2012575572 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dict0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::MakeRequest(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,Everyplay/RequestReadyDelegate,Everyplay/RequestFailedDelegate)
extern "C"  void Everyplay_MakeRequest_m2840565148 (Il2CppObject * __this /* static, unused */, String_t* ___method0, String_t* ___url1, Dictionary_2_t309261261 * ___data2, RequestReadyDelegate_t4013578405 * ___readyDelegate3, RequestFailedDelegate_t3135434785 * ___failedDelegate4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Everyplay::AccessToken()
extern "C"  String_t* Everyplay_AccessToken_m3705224328 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::ShowSharingModal()
extern "C"  void Everyplay_ShowSharingModal_m2400588728 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::PlayLastRecording()
extern "C"  void Everyplay_PlayLastRecording_m123861831 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::StartRecording()
extern "C"  void Everyplay_StartRecording_m3582706357 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::StopRecording()
extern "C"  void Everyplay_StopRecording_m3761179305 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::PauseRecording()
extern "C"  void Everyplay_PauseRecording_m3227906851 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::ResumeRecording()
extern "C"  void Everyplay_ResumeRecording_m2915910952 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::IsRecording()
extern "C"  bool Everyplay_IsRecording_m568570791 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::IsRecordingSupported()
extern "C"  bool Everyplay_IsRecordingSupported_m144924673 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::IsPaused()
extern "C"  bool Everyplay_IsPaused_m3073713592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::SnapshotRenderbuffer()
extern "C"  bool Everyplay_SnapshotRenderbuffer_m3941835888 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::IsSupported()
extern "C"  bool Everyplay_IsSupported_m2107210238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::IsSingleCoreDevice()
extern "C"  bool Everyplay_IsSingleCoreDevice_m2390517655 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Everyplay::GetUserInterfaceIdiom()
extern "C"  int32_t Everyplay_GetUserInterfaceIdiom_m2004127790 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetMetadata(System.String,System.Object)
extern "C"  void Everyplay_SetMetadata_m2665767207 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Il2CppObject * ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetMetadata(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C"  void Everyplay_SetMetadata_m2574869324 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dict0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetTargetFPS(System.Int32)
extern "C"  void Everyplay_SetTargetFPS_m2278303051 (Il2CppObject * __this /* static, unused */, int32_t ___fps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetMotionFactor(System.Int32)
extern "C"  void Everyplay_SetMotionFactor_m680416294 (Il2CppObject * __this /* static, unused */, int32_t ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetAudioResamplerQuality(System.Int32)
extern "C"  void Everyplay_SetAudioResamplerQuality_m4023239051 (Il2CppObject * __this /* static, unused */, int32_t ___quality0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetMaxRecordingMinutesLength(System.Int32)
extern "C"  void Everyplay_SetMaxRecordingMinutesLength_m1251936971 (Il2CppObject * __this /* static, unused */, int32_t ___minutes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetMaxRecordingSecondsLength(System.Int32)
extern "C"  void Everyplay_SetMaxRecordingSecondsLength_m3278065353 (Il2CppObject * __this /* static, unused */, int32_t ___seconds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetLowMemoryDevice(System.Boolean)
extern "C"  void Everyplay_SetLowMemoryDevice_m2440438802 (Il2CppObject * __this /* static, unused */, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetDisableSingleCoreDevices(System.Boolean)
extern "C"  void Everyplay_SetDisableSingleCoreDevices_m3853165939 (Il2CppObject * __this /* static, unused */, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::FaceCamIsVideoRecordingSupported()
extern "C"  bool Everyplay_FaceCamIsVideoRecordingSupported_m299330274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::FaceCamIsAudioRecordingSupported()
extern "C"  bool Everyplay_FaceCamIsAudioRecordingSupported_m818624605 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::FaceCamIsHeadphonesPluggedIn()
extern "C"  bool Everyplay_FaceCamIsHeadphonesPluggedIn_m1073951028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::FaceCamIsSessionRunning()
extern "C"  bool Everyplay_FaceCamIsSessionRunning_m3953733397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::FaceCamIsRecordingPermissionGranted()
extern "C"  bool Everyplay_FaceCamIsRecordingPermissionGranted_m3102847209 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Everyplay::FaceCamAudioPeakLevel()
extern "C"  float Everyplay_FaceCamAudioPeakLevel_m1600399543 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Everyplay::FaceCamAudioPowerLevel()
extern "C"  float Everyplay_FaceCamAudioPowerLevel_m3465111755 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetMonitorAudioLevels(System.Boolean)
extern "C"  void Everyplay_FaceCamSetMonitorAudioLevels_m156233898 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetRecordingMode(Everyplay/FaceCamRecordingMode)
extern "C"  void Everyplay_FaceCamSetRecordingMode_m2388711916 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetAudioOnly(System.Boolean)
extern "C"  void Everyplay_FaceCamSetAudioOnly_m4118761357 (Il2CppObject * __this /* static, unused */, bool ___audioOnly0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetPreviewVisible(System.Boolean)
extern "C"  void Everyplay_FaceCamSetPreviewVisible_m3505686335 (Il2CppObject * __this /* static, unused */, bool ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetPreviewScaleRetina(System.Boolean)
extern "C"  void Everyplay_FaceCamSetPreviewScaleRetina_m564042752 (Il2CppObject * __this /* static, unused */, bool ___autoScale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetPreviewSideWidth(System.Int32)
extern "C"  void Everyplay_FaceCamSetPreviewSideWidth_m2582819180 (Il2CppObject * __this /* static, unused */, int32_t ___width0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetPreviewBorderWidth(System.Int32)
extern "C"  void Everyplay_FaceCamSetPreviewBorderWidth_m4144496013 (Il2CppObject * __this /* static, unused */, int32_t ___width0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetPreviewPositionX(System.Int32)
extern "C"  void Everyplay_FaceCamSetPreviewPositionX_m1625010144 (Il2CppObject * __this /* static, unused */, int32_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetPreviewPositionY(System.Int32)
extern "C"  void Everyplay_FaceCamSetPreviewPositionY_m906599039 (Il2CppObject * __this /* static, unused */, int32_t ___y0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetPreviewBorderColor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Everyplay_FaceCamSetPreviewBorderColor_m2502318011 (Il2CppObject * __this /* static, unused */, float ___r0, float ___g1, float ___b2, float ___a3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetPreviewOrigin(Everyplay/FaceCamPreviewOrigin)
extern "C"  void Everyplay_FaceCamSetPreviewOrigin_m1150375596 (Il2CppObject * __this /* static, unused */, int32_t ___origin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetTargetTexture(UnityEngine.Texture2D)
extern "C"  void Everyplay_FaceCamSetTargetTexture_m2609324012 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetTargetTextureId(System.Int32)
extern "C"  void Everyplay_FaceCamSetTargetTextureId_m3672435410 (Il2CppObject * __this /* static, unused */, int32_t ___textureId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetTargetTextureWidth(System.Int32)
extern "C"  void Everyplay_FaceCamSetTargetTextureWidth_m3178038257 (Il2CppObject * __this /* static, unused */, int32_t ___textureWidth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamSetTargetTextureHeight(System.Int32)
extern "C"  void Everyplay_FaceCamSetTargetTextureHeight_m1571811864 (Il2CppObject * __this /* static, unused */, int32_t ___textureHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamStartSession()
extern "C"  void Everyplay_FaceCamStartSession_m2600985758 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamRequestRecordingPermission()
extern "C"  void Everyplay_FaceCamRequestRecordingPermission_m3770143701 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::FaceCamStopSession()
extern "C"  void Everyplay_FaceCamStopSession_m1826517360 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetThumbnailTargetTexture(UnityEngine.Texture2D)
extern "C"  void Everyplay_SetThumbnailTargetTexture_m2121221160 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetThumbnailTargetTextureId(System.Int32)
extern "C"  void Everyplay_SetThumbnailTargetTextureId_m1453661614 (Il2CppObject * __this /* static, unused */, int32_t ___textureId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetThumbnailTargetTextureWidth(System.Int32)
extern "C"  void Everyplay_SetThumbnailTargetTextureWidth_m1053357747 (Il2CppObject * __this /* static, unused */, int32_t ___textureWidth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::SetThumbnailTargetTextureHeight(System.Int32)
extern "C"  void Everyplay_SetThumbnailTargetTextureHeight_m1830783752 (Il2CppObject * __this /* static, unused */, int32_t ___textureHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::TakeThumbnail()
extern "C"  void Everyplay_TakeThumbnail_m1670072747 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::IsReadyForRecording()
extern "C"  bool Everyplay_IsReadyForRecording_m4208154795 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::RemoveAllEventHandlers()
extern "C"  void Everyplay_RemoveAllEventHandlers_m3444867980 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::Reset()
extern "C"  void Everyplay_Reset_m1986807329 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::AddTestButtons(UnityEngine.GameObject)
extern "C"  void Everyplay_AddTestButtons_m1954086306 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::AsyncMakeRequest(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,Everyplay/RequestReadyDelegate,Everyplay/RequestFailedDelegate)
extern "C"  void Everyplay_AsyncMakeRequest_m1154701314 (Everyplay_t1799077027 * __this, String_t* ___method0, String_t* ___url1, Dictionary_2_t309261261 * ___data2, RequestReadyDelegate_t4013578405 * ___readyDelegate3, RequestFailedDelegate_t3135434785 * ___failedDelegate4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Everyplay::MakeRequestEnumerator(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,Everyplay/RequestReadyDelegate,Everyplay/RequestFailedDelegate)
extern "C"  Il2CppObject * Everyplay_MakeRequestEnumerator_m1853830506 (Everyplay_t1799077027 * __this, String_t* ___method0, String_t* ___url1, Dictionary_2_t309261261 * ___data2, RequestReadyDelegate_t4013578405 * ___readyDelegate3, RequestFailedDelegate_t3135434785 * ___failedDelegate4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::OnApplicationQuit()
extern "C"  void Everyplay_OnApplicationQuit_m2101183012 (Everyplay_t1799077027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayHidden(System.String)
extern "C"  void Everyplay_EveryplayHidden_m1197865333 (Everyplay_t1799077027 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayReadyForRecording(System.String)
extern "C"  void Everyplay_EveryplayReadyForRecording_m3456378644 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayRecordingStarted(System.String)
extern "C"  void Everyplay_EveryplayRecordingStarted_m3414975027 (Everyplay_t1799077027 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayRecordingStopped(System.String)
extern "C"  void Everyplay_EveryplayRecordingStopped_m3585386003 (Everyplay_t1799077027 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSessionStarted(System.String)
extern "C"  void Everyplay_EveryplayFaceCamSessionStarted_m1477758232 (Everyplay_t1799077027 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamRecordingPermission(System.String)
extern "C"  void Everyplay_EveryplayFaceCamRecordingPermission_m3565058039 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSessionStopped(System.String)
extern "C"  void Everyplay_EveryplayFaceCamSessionStopped_m3917869784 (Everyplay_t1799077027 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayThumbnailReadyAtTextureId(System.String)
extern "C"  void Everyplay_EveryplayThumbnailReadyAtTextureId_m4253971571 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayThumbnailTextureReady(System.String)
extern "C"  void Everyplay_EveryplayThumbnailTextureReady_m3447023487 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayUploadDidStart(System.String)
extern "C"  void Everyplay_EveryplayUploadDidStart_m2462853237 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayUploadDidProgress(System.String)
extern "C"  void Everyplay_EveryplayUploadDidProgress_m488542120 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayUploadDidComplete(System.String)
extern "C"  void Everyplay_EveryplayUploadDidComplete_m580978396 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::InitEveryplay(System.String,System.String,System.String,System.String)
extern "C"  void Everyplay_InitEveryplay_m927127947 (Il2CppObject * __this /* static, unused */, String_t* ___clientId0, String_t* ___clientSecret1, String_t* ___redirectURI2, String_t* ___gameObjectName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayShow()
extern "C"  void Everyplay_EveryplayShow_m946367502 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayShowWithPath(System.String)
extern "C"  void Everyplay_EveryplayShowWithPath_m1436735135 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayPlayVideoWithURL(System.String)
extern "C"  void Everyplay_EveryplayPlayVideoWithURL_m1833305119 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayPlayVideoWithDictionary(System.String)
extern "C"  void Everyplay_EveryplayPlayVideoWithDictionary_m1579715712 (Il2CppObject * __this /* static, unused */, String_t* ___dic0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Everyplay::EveryplayAccountAccessToken()
extern "C"  String_t* Everyplay_EveryplayAccountAccessToken_m848432640 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayShowSharingModal()
extern "C"  void Everyplay_EveryplayShowSharingModal_m2705975637 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayPlayLastRecording()
extern "C"  void Everyplay_EveryplayPlayLastRecording_m3548469286 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayStartRecording()
extern "C"  void Everyplay_EveryplayStartRecording_m9627532 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayStopRecording()
extern "C"  void Everyplay_EveryplayStopRecording_m1776955136 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayPauseRecording()
extern "C"  void Everyplay_EveryplayPauseRecording_m998977122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayResumeRecording()
extern "C"  void Everyplay_EveryplayResumeRecording_m248112421 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::EveryplayIsRecording()
extern "C"  bool Everyplay_EveryplayIsRecording_m3853108226 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::EveryplayIsRecordingSupported()
extern "C"  bool Everyplay_EveryplayIsRecordingSupported_m1388371902 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::EveryplayIsPaused()
extern "C"  bool Everyplay_EveryplayIsPaused_m3761251281 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::EveryplaySnapshotRenderbuffer()
extern "C"  bool Everyplay_EveryplaySnapshotRenderbuffer_m1557380991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplaySetMetadata(System.String)
extern "C"  void Everyplay_EveryplaySetMetadata_m2926221680 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplaySetTargetFPS(System.Int32)
extern "C"  void Everyplay_EveryplaySetTargetFPS_m410279722 (Il2CppObject * __this /* static, unused */, int32_t ___fps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplaySetMotionFactor(System.Int32)
extern "C"  void Everyplay_EveryplaySetMotionFactor_m1496152793 (Il2CppObject * __this /* static, unused */, int32_t ___factor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplaySetMaxRecordingMinutesLength(System.Int32)
extern "C"  void Everyplay_EveryplaySetMaxRecordingMinutesLength_m3939274140 (Il2CppObject * __this /* static, unused */, int32_t ___minutes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplaySetMaxRecordingSecondsLength(System.Int32)
extern "C"  void Everyplay_EveryplaySetMaxRecordingSecondsLength_m1274558896 (Il2CppObject * __this /* static, unused */, int32_t ___seconds0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplaySetLowMemoryDevice(System.Boolean)
extern "C"  void Everyplay_EveryplaySetLowMemoryDevice_m155866533 (Il2CppObject * __this /* static, unused */, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplaySetDisableSingleCoreDevices(System.Boolean)
extern "C"  void Everyplay_EveryplaySetDisableSingleCoreDevices_m1396293428 (Il2CppObject * __this /* static, unused */, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::EveryplayIsSupported()
extern "C"  bool Everyplay_EveryplayIsSupported_m2495406309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::EveryplayIsSingleCoreDevice()
extern "C"  bool Everyplay_EveryplayIsSingleCoreDevice_m2355195836 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Everyplay::EveryplayGetUserInterfaceIdiom()
extern "C"  int32_t Everyplay_EveryplayGetUserInterfaceIdiom_m298146343 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::EveryplayFaceCamIsVideoRecordingSupported()
extern "C"  bool Everyplay_EveryplayFaceCamIsVideoRecordingSupported_m1933775331 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::EveryplayFaceCamIsAudioRecordingSupported()
extern "C"  bool Everyplay_EveryplayFaceCamIsAudioRecordingSupported_m622408608 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::EveryplayFaceCamIsHeadphonesPluggedIn()
extern "C"  bool Everyplay_EveryplayFaceCamIsHeadphonesPluggedIn_m2030515277 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::EveryplayFaceCamIsSessionRunning()
extern "C"  bool Everyplay_EveryplayFaceCamIsSessionRunning_m3581958402 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Everyplay::EveryplayFaceCamIsRecordingPermissionGranted()
extern "C"  bool Everyplay_EveryplayFaceCamIsRecordingPermissionGranted_m3961460134 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Everyplay::EveryplayFaceCamAudioPeakLevel()
extern "C"  float Everyplay_EveryplayFaceCamAudioPeakLevel_m199553448 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Everyplay::EveryplayFaceCamAudioPowerLevel()
extern "C"  float Everyplay_EveryplayFaceCamAudioPowerLevel_m3853690490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetMonitorAudioLevels(System.Boolean)
extern "C"  void Everyplay_EveryplayFaceCamSetMonitorAudioLevels_m127210877 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetRecordingMode(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetRecordingMode_m1780428062 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetAudioOnly(System.Boolean)
extern "C"  void Everyplay_EveryplayFaceCamSetAudioOnly_m3301962502 (Il2CppObject * __this /* static, unused */, bool ___audioOnly0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetPreviewVisible(System.Boolean)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewVisible_m275566432 (Il2CppObject * __this /* static, unused */, bool ___visible0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetPreviewScaleRetina(System.Boolean)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewScaleRetina_m2186195837 (Il2CppObject * __this /* static, unused */, bool ___autoScale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetPreviewSideWidth(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewSideWidth_m2664091223 (Il2CppObject * __this /* static, unused */, int32_t ___width0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetPreviewBorderWidth(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewBorderWidth_m3607310900 (Il2CppObject * __this /* static, unused */, int32_t ___width0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetPreviewPositionX(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewPositionX_m2443346075 (Il2CppObject * __this /* static, unused */, int32_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetPreviewPositionY(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewPositionY_m3939587552 (Il2CppObject * __this /* static, unused */, int32_t ___y0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetPreviewBorderColor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewBorderColor_m3525118556 (Il2CppObject * __this /* static, unused */, float ___r0, float ___g1, float ___b2, float ___a3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetPreviewOrigin(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewOrigin_m397951628 (Il2CppObject * __this /* static, unused */, int32_t ___origin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetTargetTexture(System.IntPtr)
extern "C"  void Everyplay_EveryplayFaceCamSetTargetTexture_m3120082225 (Il2CppObject * __this /* static, unused */, IntPtr_t ___texturePtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetTargetTextureId(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetTargetTextureId_m614786215 (Il2CppObject * __this /* static, unused */, int32_t ___textureId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetTargetTextureWidth(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetTargetTextureWidth_m539522378 (Il2CppObject * __this /* static, unused */, int32_t ___textureWidth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamSetTargetTextureHeight(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetTargetTextureHeight_m450171829 (Il2CppObject * __this /* static, unused */, int32_t ___textureHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamStartSession()
extern "C"  void Everyplay_EveryplayFaceCamStartSession_m563630289 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamRequestRecordingPermission()
extern "C"  void Everyplay_EveryplayFaceCamRequestRecordingPermission_m3269278668 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayFaceCamStopSession()
extern "C"  void Everyplay_EveryplayFaceCamStopSession_m2774136573 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplaySetThumbnailTargetTexture(System.IntPtr)
extern "C"  void Everyplay_EveryplaySetThumbnailTargetTexture_m1799038923 (Il2CppObject * __this /* static, unused */, IntPtr_t ___texturePtr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplaySetThumbnailTargetTextureId(System.Int32)
extern "C"  void Everyplay_EveryplaySetThumbnailTargetTextureId_m3960249425 (Il2CppObject * __this /* static, unused */, int32_t ___textureId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplaySetThumbnailTargetTextureWidth(System.Int32)
extern "C"  void Everyplay_EveryplaySetThumbnailTargetTextureWidth_m379731554 (Il2CppObject * __this /* static, unused */, int32_t ___textureWidth0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplaySetThumbnailTargetTextureHeight(System.Int32)
extern "C"  void Everyplay_EveryplaySetThumbnailTargetTextureHeight_m3729298403 (Il2CppObject * __this /* static, unused */, int32_t ___textureHeight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::EveryplayTakeThumbnail()
extern "C"  void Everyplay_EveryplayTakeThumbnail_m1202186698 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay::.cctor()
extern "C"  void Everyplay__cctor_m2517547459 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
