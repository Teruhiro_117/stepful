﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMat945754175MethodDeclarations.h"

// System.Void UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot>>::.ctor(System.Object,System.IntPtr)
#define DataResponseDelegate_1__ctor_m1637775298(__this, ___object0, ___method1, method) ((  void (*) (DataResponseDelegate_1_t804536919 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DataResponseDelegate_1__ctor_m2222754223_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot>>::Invoke(System.Boolean,System.String,T)
#define DataResponseDelegate_1_Invoke_m688144889(__this, ___success0, ___extendedInfo1, ___responseData2, method) ((  void (*) (DataResponseDelegate_1_t804536919 *, bool, String_t*, List_1_t2548232039 *, const MethodInfo*))DataResponseDelegate_1_Invoke_m2892839852_gshared)(__this, ___success0, ___extendedInfo1, ___responseData2, method)
// System.IAsyncResult UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot>>::BeginInvoke(System.Boolean,System.String,T,System.AsyncCallback,System.Object)
#define DataResponseDelegate_1_BeginInvoke_m3227681290(__this, ___success0, ___extendedInfo1, ___responseData2, ___callback3, ___object4, method) ((  Il2CppObject * (*) (DataResponseDelegate_1_t804536919 *, bool, String_t*, List_1_t2548232039 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))DataResponseDelegate_1_BeginInvoke_m2356550577_gshared)(__this, ___success0, ___extendedInfo1, ___responseData2, ___callback3, ___object4, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchInfoSnapshot>>::EndInvoke(System.IAsyncResult)
#define DataResponseDelegate_1_EndInvoke_m1180169314(__this, ___result0, method) ((  void (*) (DataResponseDelegate_1_t804536919 *, Il2CppObject *, const MethodInfo*))DataResponseDelegate_1_EndInvoke_m1252280981_gshared)(__this, ___result0, method)
