﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>
struct ReadOnlyCollection_1_t2335467713;
// System.Collections.Generic.IList`1<System.UInt32>
struct IList_1_t2690622622;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t3920173144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1262780440_gshared (ReadOnlyCollection_1_t2335467713 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1262780440(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1262780440_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m781001256_gshared (ReadOnlyCollection_1_t2335467713 * __this, uint32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m781001256(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, uint32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m781001256_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3326029860_gshared (ReadOnlyCollection_1_t2335467713 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3326029860(__this, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3326029860_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m228613869_gshared (ReadOnlyCollection_1_t2335467713 * __this, int32_t ___index0, uint32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m228613869(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, int32_t, uint32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m228613869_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1216878941_gshared (ReadOnlyCollection_1_t2335467713 * __this, uint32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1216878941(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2335467713 *, uint32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1216878941_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2770029441_gshared (ReadOnlyCollection_1_t2335467713 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2770029441(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2770029441_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3652719189_gshared (ReadOnlyCollection_1_t2335467713 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3652719189(__this, ___index0, method) ((  uint32_t (*) (ReadOnlyCollection_1_t2335467713 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3652719189_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m607502826_gshared (ReadOnlyCollection_1_t2335467713 * __this, int32_t ___index0, uint32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m607502826(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, int32_t, uint32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m607502826_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m833774954_gshared (ReadOnlyCollection_1_t2335467713 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m833774954(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2335467713 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m833774954_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3317689451_gshared (ReadOnlyCollection_1_t2335467713 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3317689451(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3317689451_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2271326870_gshared (ReadOnlyCollection_1_t2335467713 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2271326870(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2335467713 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2271326870_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2487210195_gshared (ReadOnlyCollection_1_t2335467713 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2487210195(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2335467713 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2487210195_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3376722675_gshared (ReadOnlyCollection_1_t2335467713 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3376722675(__this, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3376722675_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m4165887219_gshared (ReadOnlyCollection_1_t2335467713 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m4165887219(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2335467713 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m4165887219_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3381212493_gshared (ReadOnlyCollection_1_t2335467713 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3381212493(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2335467713 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3381212493_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m34069572_gshared (ReadOnlyCollection_1_t2335467713 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m34069572(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m34069572_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m379196426_gshared (ReadOnlyCollection_1_t2335467713 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m379196426(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m379196426_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1221106122_gshared (ReadOnlyCollection_1_t2335467713 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1221106122(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1221106122_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m841966603_gshared (ReadOnlyCollection_1_t2335467713 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m841966603(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2335467713 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m841966603_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3038071547_gshared (ReadOnlyCollection_1_t2335467713 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3038071547(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2335467713 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3038071547_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m299263776_gshared (ReadOnlyCollection_1_t2335467713 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m299263776(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2335467713 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m299263776_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3919269631_gshared (ReadOnlyCollection_1_t2335467713 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3919269631(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2335467713 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3919269631_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2069020132_gshared (ReadOnlyCollection_1_t2335467713 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2069020132(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2335467713 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2069020132_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m950802741_gshared (ReadOnlyCollection_1_t2335467713 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m950802741(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m950802741_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1359526242_gshared (ReadOnlyCollection_1_t2335467713 * __this, uint32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1359526242(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2335467713 *, uint32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m1359526242_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3666547772_gshared (ReadOnlyCollection_1_t2335467713 * __this, UInt32U5BU5D_t59386216* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3666547772(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2335467713 *, UInt32U5BU5D_t59386216*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3666547772_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1884429251_gshared (ReadOnlyCollection_1_t2335467713 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1884429251(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2335467713 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1884429251_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2987839706_gshared (ReadOnlyCollection_1_t2335467713 * __this, uint32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2987839706(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2335467713 *, uint32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2987839706_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m1673697443_gshared (ReadOnlyCollection_1_t2335467713 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m1673697443(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2335467713 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m1673697443_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::get_Item(System.Int32)
extern "C"  uint32_t ReadOnlyCollection_1_get_Item_m2428836761_gshared (ReadOnlyCollection_1_t2335467713 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2428836761(__this, ___index0, method) ((  uint32_t (*) (ReadOnlyCollection_1_t2335467713 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2428836761_gshared)(__this, ___index0, method)
