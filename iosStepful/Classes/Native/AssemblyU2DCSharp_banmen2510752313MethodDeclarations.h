﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// banmen
struct banmen_t2510752313;
// Koma
struct Koma_t3011766596;

#include "codegen/il2cpp-codegen.h"

// System.Void banmen::.ctor()
extern "C"  void banmen__ctor_m372770190 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::Start()
extern "C"  void banmen_Start_m342229766 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::Init()
extern "C"  void banmen_Init_m453326946 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::Update()
extern "C"  void banmen_Update_m2680782557 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::arrangeOffDo()
extern "C"  void banmen_arrangeOffDo_m4084751936 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::paintEnemyArea()
extern "C"  void banmen_paintEnemyArea_m3798202179 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::win()
extern "C"  void banmen_win_m588564420 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::lose()
extern "C"  void banmen_lose_m1266767581 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::setKoma()
extern "C"  void banmen_setKoma_m222615202 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::setKomaNumber(System.Int32)
extern "C"  void banmen_setKomaNumber_m1079153342 (banmen_t2510752313 * __this, int32_t ___komaNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::whichPlayer()
extern "C"  void banmen_whichPlayer_m4081184614 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::processKoma()
extern "C"  void banmen_processKoma_m2439105839 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::reTime()
extern "C"  void banmen_reTime_m4028066712 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::timeChangeMissile()
extern "C"  void banmen_timeChangeMissile_m382147847 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::timeChangeFactory()
extern "C"  void banmen_timeChangeFactory_m1579757147 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::drawBarrier()
extern "C"  void banmen_drawBarrier_m1973980853 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::paint(System.Int32,System.Int32)
extern "C"  void banmen_paint_m3356220390 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::rePaint()
extern "C"  void banmen_rePaint_m1556028161 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::changeSprite()
extern "C"  void banmen_changeSprite_m109808171 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Koma banmen::inputKoma(System.Int32)
extern "C"  Koma_t3011766596 * banmen_inputKoma_m3899739766 (banmen_t2510752313 * __this, int32_t ___komaNum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::atackMissile()
extern "C"  void banmen_atackMissile_m1714889658 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::atackEMissile()
extern "C"  void banmen_atackEMissile_m1668074645 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::destroyByMissile()
extern "C"  void banmen_destroyByMissile_m1609839829 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::barrierPoint(System.Int32,System.Int32)
extern "C"  void banmen_barrierPoint_m2170502217 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::barrierBlueToWhite(System.Int32,System.Int32)
extern "C"  void banmen_barrierBlueToWhite_m3077042043 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::isThereKoma(System.Int32,System.Int32)
extern "C"  void banmen_isThereKoma_m207992560 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::isThereFuti(System.Int32,System.Int32)
extern "C"  void banmen_isThereFuti_m1442766142 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::isThereDraw(System.Int32,System.Int32)
extern "C"  void banmen_isThereDraw_m900625674 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::isThereErase(System.Int32,System.Int32)
extern "C"  void banmen_isThereErase_m2459852564 (banmen_t2510752313 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::reset()
extern "C"  void banmen_reset_m2092301357 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::ProduceGRobot()
extern "C"  void banmen_ProduceGRobot_m3664332021 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::ProduceSRobot()
extern "C"  void banmen_ProduceSRobot_m4010844345 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::ProduceDrone()
extern "C"  void banmen_ProduceDrone_m526588790 (banmen_t2510752313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void banmen::Produce(System.Int32)
extern "C"  void banmen_Produce_m2731612623 (banmen_t2510752313 * __this, int32_t ___komaNum0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
