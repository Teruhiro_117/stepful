﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct List_1_t886216149;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct IEnumerable_1_t1809222062;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct IEnumerator_1_t3287586140;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct ICollection_1_t2469170322;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct ReadOnlyCollection_1_t1702880709;
// UnityEngine.Networking.NetworkLobbyManager/PendingPlayer[]
struct PendingPlayerU5BU5D_t220373972;
// System.Predicate`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct Predicate_1_t4255032428;
// System.Collections.Generic.IComparer`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct IComparer_1_t3766525435;
// System.Comparison`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct Comparison_1_t2778833868;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1517095017.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat420945823.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.ctor()
extern "C"  void List_1__ctor_m2085049879_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1__ctor_m2085049879(__this, method) ((  void (*) (List_1_t886216149 *, const MethodInfo*))List_1__ctor_m2085049879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3093120418_gshared (List_1_t886216149 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3093120418(__this, ___collection0, method) ((  void (*) (List_1_t886216149 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3093120418_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1505529376_gshared (List_1_t886216149 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1505529376(__this, ___capacity0, method) ((  void (*) (List_1_t886216149 *, int32_t, const MethodInfo*))List_1__ctor_m1505529376_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.cctor()
extern "C"  void List_1__cctor_m1736333924_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1736333924(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1736333924_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3413264913_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3413264913(__this, method) ((  Il2CppObject* (*) (List_1_t886216149 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3413264913_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m525330513_gshared (List_1_t886216149 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m525330513(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t886216149 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m525330513_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m932698242_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m932698242(__this, method) ((  Il2CppObject * (*) (List_1_t886216149 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m932698242_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m62312049_gshared (List_1_t886216149 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m62312049(__this, ___item0, method) ((  int32_t (*) (List_1_t886216149 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m62312049_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1625097649_gshared (List_1_t886216149 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1625097649(__this, ___item0, method) ((  bool (*) (List_1_t886216149 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1625097649_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m786244003_gshared (List_1_t886216149 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m786244003(__this, ___item0, method) ((  int32_t (*) (List_1_t886216149 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m786244003_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m853662632_gshared (List_1_t886216149 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m853662632(__this, ___index0, ___item1, method) ((  void (*) (List_1_t886216149 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m853662632_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1963953306_gshared (List_1_t886216149 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1963953306(__this, ___item0, method) ((  void (*) (List_1_t886216149 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1963953306_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4261904730_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4261904730(__this, method) ((  bool (*) (List_1_t886216149 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4261904730_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m2146023605_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2146023605(__this, method) ((  bool (*) (List_1_t886216149 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m2146023605_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3395241261_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3395241261(__this, method) ((  Il2CppObject * (*) (List_1_t886216149 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3395241261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2655683016_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2655683016(__this, method) ((  bool (*) (List_1_t886216149 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2655683016_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m4095303633_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m4095303633(__this, method) ((  bool (*) (List_1_t886216149 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m4095303633_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1247675114_gshared (List_1_t886216149 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1247675114(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t886216149 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1247675114_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m166629371_gshared (List_1_t886216149 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m166629371(__this, ___index0, ___value1, method) ((  void (*) (List_1_t886216149 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m166629371_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Add(T)
extern "C"  void List_1_Add_m430912515_gshared (List_1_t886216149 * __this, PendingPlayer_t1517095017  ___item0, const MethodInfo* method);
#define List_1_Add_m430912515(__this, ___item0, method) ((  void (*) (List_1_t886216149 *, PendingPlayer_t1517095017 , const MethodInfo*))List_1_Add_m430912515_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3690352001_gshared (List_1_t886216149 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3690352001(__this, ___newCount0, method) ((  void (*) (List_1_t886216149 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3690352001_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2858430121_gshared (List_1_t886216149 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2858430121(__this, ___collection0, method) ((  void (*) (List_1_t886216149 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2858430121_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m288549321_gshared (List_1_t886216149 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m288549321(__this, ___enumerable0, method) ((  void (*) (List_1_t886216149 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m288549321_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2522878498_gshared (List_1_t886216149 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2522878498(__this, ___collection0, method) ((  void (*) (List_1_t886216149 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2522878498_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1702880709 * List_1_AsReadOnly_m477540017_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m477540017(__this, method) ((  ReadOnlyCollection_1_t1702880709 * (*) (List_1_t886216149 *, const MethodInfo*))List_1_AsReadOnly_m477540017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Clear()
extern "C"  void List_1_Clear_m1273568294_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_Clear_m1273568294(__this, method) ((  void (*) (List_1_t886216149 *, const MethodInfo*))List_1_Clear_m1273568294_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Contains(T)
extern "C"  bool List_1_Contains_m2285082630_gshared (List_1_t886216149 * __this, PendingPlayer_t1517095017  ___item0, const MethodInfo* method);
#define List_1_Contains_m2285082630(__this, ___item0, method) ((  bool (*) (List_1_t886216149 *, PendingPlayer_t1517095017 , const MethodInfo*))List_1_Contains_m2285082630_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m90808876_gshared (List_1_t886216149 * __this, PendingPlayerU5BU5D_t220373972* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m90808876(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t886216149 *, PendingPlayerU5BU5D_t220373972*, int32_t, const MethodInfo*))List_1_CopyTo_m90808876_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Find(System.Predicate`1<T>)
extern "C"  PendingPlayer_t1517095017  List_1_Find_m3090228746_gshared (List_1_t886216149 * __this, Predicate_1_t4255032428 * ___match0, const MethodInfo* method);
#define List_1_Find_m3090228746(__this, ___match0, method) ((  PendingPlayer_t1517095017  (*) (List_1_t886216149 *, Predicate_1_t4255032428 *, const MethodInfo*))List_1_Find_m3090228746_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m819461505_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t4255032428 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m819461505(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t4255032428 *, const MethodInfo*))List_1_CheckMatch_m819461505_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m525837912_gshared (List_1_t886216149 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t4255032428 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m525837912(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t886216149 *, int32_t, int32_t, Predicate_1_t4255032428 *, const MethodInfo*))List_1_GetIndex_m525837912_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::GetEnumerator()
extern "C"  Enumerator_t420945823  List_1_GetEnumerator_m1624813413_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1624813413(__this, method) ((  Enumerator_t420945823  (*) (List_1_t886216149 *, const MethodInfo*))List_1_GetEnumerator_m1624813413_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3958800866_gshared (List_1_t886216149 * __this, PendingPlayer_t1517095017  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3958800866(__this, ___item0, method) ((  int32_t (*) (List_1_t886216149 *, PendingPlayer_t1517095017 , const MethodInfo*))List_1_IndexOf_m3958800866_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m728973429_gshared (List_1_t886216149 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m728973429(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t886216149 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m728973429_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1928741744_gshared (List_1_t886216149 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1928741744(__this, ___index0, method) ((  void (*) (List_1_t886216149 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1928741744_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3028558067_gshared (List_1_t886216149 * __this, int32_t ___index0, PendingPlayer_t1517095017  ___item1, const MethodInfo* method);
#define List_1_Insert_m3028558067(__this, ___index0, ___item1, method) ((  void (*) (List_1_t886216149 *, int32_t, PendingPlayer_t1517095017 , const MethodInfo*))List_1_Insert_m3028558067_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2306730222_gshared (List_1_t886216149 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2306730222(__this, ___collection0, method) ((  void (*) (List_1_t886216149 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2306730222_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Remove(T)
extern "C"  bool List_1_Remove_m3655811711_gshared (List_1_t886216149 * __this, PendingPlayer_t1517095017  ___item0, const MethodInfo* method);
#define List_1_Remove_m3655811711(__this, ___item0, method) ((  bool (*) (List_1_t886216149 *, PendingPlayer_t1517095017 , const MethodInfo*))List_1_Remove_m3655811711_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m183351513_gshared (List_1_t886216149 * __this, Predicate_1_t4255032428 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m183351513(__this, ___match0, method) ((  int32_t (*) (List_1_t886216149 *, Predicate_1_t4255032428 *, const MethodInfo*))List_1_RemoveAll_m183351513_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3974305255_gshared (List_1_t886216149 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3974305255(__this, ___index0, method) ((  void (*) (List_1_t886216149 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3974305255_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Reverse()
extern "C"  void List_1_Reverse_m3796480285_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_Reverse_m3796480285(__this, method) ((  void (*) (List_1_t886216149 *, const MethodInfo*))List_1_Reverse_m3796480285_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Sort()
extern "C"  void List_1_Sort_m812517591_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_Sort_m812517591(__this, method) ((  void (*) (List_1_t886216149 *, const MethodInfo*))List_1_Sort_m812517591_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m4194244515_gshared (List_1_t886216149 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m4194244515(__this, ___comparer0, method) ((  void (*) (List_1_t886216149 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m4194244515_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2612339868_gshared (List_1_t886216149 * __this, Comparison_1_t2778833868 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2612339868(__this, ___comparison0, method) ((  void (*) (List_1_t886216149 *, Comparison_1_t2778833868 *, const MethodInfo*))List_1_Sort_m2612339868_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::ToArray()
extern "C"  PendingPlayerU5BU5D_t220373972* List_1_ToArray_m514741654_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_ToArray_m514741654(__this, method) ((  PendingPlayerU5BU5D_t220373972* (*) (List_1_t886216149 *, const MethodInfo*))List_1_ToArray_m514741654_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1591800926_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1591800926(__this, method) ((  void (*) (List_1_t886216149 *, const MethodInfo*))List_1_TrimExcess_m1591800926_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m4262584296_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m4262584296(__this, method) ((  int32_t (*) (List_1_t886216149 *, const MethodInfo*))List_1_get_Capacity_m4262584296_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m855543881_gshared (List_1_t886216149 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m855543881(__this, ___value0, method) ((  void (*) (List_1_t886216149 *, int32_t, const MethodInfo*))List_1_set_Capacity_m855543881_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::get_Count()
extern "C"  int32_t List_1_get_Count_m2667657083_gshared (List_1_t886216149 * __this, const MethodInfo* method);
#define List_1_get_Count_m2667657083(__this, method) ((  int32_t (*) (List_1_t886216149 *, const MethodInfo*))List_1_get_Count_m2667657083_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::get_Item(System.Int32)
extern "C"  PendingPlayer_t1517095017  List_1_get_Item_m1084381514_gshared (List_1_t886216149 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1084381514(__this, ___index0, method) ((  PendingPlayer_t1517095017  (*) (List_1_t886216149 *, int32_t, const MethodInfo*))List_1_get_Item_m1084381514_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3324652734_gshared (List_1_t886216149 * __this, int32_t ___index0, PendingPlayer_t1517095017  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3324652734(__this, ___index0, ___value1, method) ((  void (*) (List_1_t886216149 *, int32_t, PendingPlayer_t1517095017 , const MethodInfo*))List_1_set_Item_m3324652734_gshared)(__this, ___index0, ___value1, method)
