﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>
struct ReadOnlyCollection_1_t1868382577;
// System.Collections.Generic.IList`1<UnityEngine.Networking.ChannelPacket>
struct IList_1_t2223537486;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.ChannelPacket[]
struct ChannelPacketU5BU5D_t3883591672;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.ChannelPacket>
struct IEnumerator_1_t3453088008;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan1682596885.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3230375872_gshared (ReadOnlyCollection_1_t1868382577 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3230375872(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3230375872_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1930201320_gshared (ReadOnlyCollection_1_t1868382577 * __this, ChannelPacket_t1682596885  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1930201320(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, ChannelPacket_t1682596885 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1930201320_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2623012004_gshared (ReadOnlyCollection_1_t1868382577 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2623012004(__this, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2623012004_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1445045825_gshared (ReadOnlyCollection_1_t1868382577 * __this, int32_t ___index0, ChannelPacket_t1682596885  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1445045825(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, int32_t, ChannelPacket_t1682596885 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1445045825_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3505526849_gshared (ReadOnlyCollection_1_t1868382577 * __this, ChannelPacket_t1682596885  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3505526849(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1868382577 *, ChannelPacket_t1682596885 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3505526849_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2554961157_gshared (ReadOnlyCollection_1_t1868382577 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2554961157(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2554961157_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  ChannelPacket_t1682596885  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m734854553_gshared (ReadOnlyCollection_1_t1868382577 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m734854553(__this, ___index0, method) ((  ChannelPacket_t1682596885  (*) (ReadOnlyCollection_1_t1868382577 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m734854553_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2730710490_gshared (ReadOnlyCollection_1_t1868382577 * __this, int32_t ___index0, ChannelPacket_t1682596885  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2730710490(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, int32_t, ChannelPacket_t1682596885 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2730710490_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1369601770_gshared (ReadOnlyCollection_1_t1868382577 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1369601770(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1868382577 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1369601770_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1865101575_gshared (ReadOnlyCollection_1_t1868382577 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1865101575(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1865101575_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3645068814_gshared (ReadOnlyCollection_1_t1868382577 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3645068814(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1868382577 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3645068814_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3545218631_gshared (ReadOnlyCollection_1_t1868382577 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3545218631(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1868382577 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3545218631_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m4237249879_gshared (ReadOnlyCollection_1_t1868382577 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m4237249879(__this, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m4237249879_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1311485407_gshared (ReadOnlyCollection_1_t1868382577 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1311485407(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1868382577 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1311485407_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3110833845_gshared (ReadOnlyCollection_1_t1868382577 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3110833845(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1868382577 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3110833845_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m1879368436_gshared (ReadOnlyCollection_1_t1868382577 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1879368436(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m1879368436_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2616419226_gshared (ReadOnlyCollection_1_t1868382577 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2616419226(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2616419226_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3612430610_gshared (ReadOnlyCollection_1_t1868382577 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3612430610(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3612430610_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3699972335_gshared (ReadOnlyCollection_1_t1868382577 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3699972335(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1868382577 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3699972335_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3277388427_gshared (ReadOnlyCollection_1_t1868382577 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3277388427(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1868382577 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3277388427_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m483387152_gshared (ReadOnlyCollection_1_t1868382577 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m483387152(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1868382577 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m483387152_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3368437043_gshared (ReadOnlyCollection_1_t1868382577 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3368437043(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1868382577 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3368437043_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2271251822_gshared (ReadOnlyCollection_1_t1868382577 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2271251822(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1868382577 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2271251822_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3549048377_gshared (ReadOnlyCollection_1_t1868382577 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3549048377(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3549048377_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2769789226_gshared (ReadOnlyCollection_1_t1868382577 * __this, ChannelPacket_t1682596885  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2769789226(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1868382577 *, ChannelPacket_t1682596885 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2769789226_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3662368844_gshared (ReadOnlyCollection_1_t1868382577 * __this, ChannelPacketU5BU5D_t3883591672* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3662368844(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1868382577 *, ChannelPacketU5BU5D_t3883591672*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3662368844_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m978525783_gshared (ReadOnlyCollection_1_t1868382577 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m978525783(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1868382577 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m978525783_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2199501722_gshared (ReadOnlyCollection_1_t1868382577 * __this, ChannelPacket_t1682596885  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2199501722(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1868382577 *, ChannelPacket_t1682596885 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2199501722_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2073601383_gshared (ReadOnlyCollection_1_t1868382577 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2073601383(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1868382577 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2073601383_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>::get_Item(System.Int32)
extern "C"  ChannelPacket_t1682596885  ReadOnlyCollection_1_get_Item_m2815235757_gshared (ReadOnlyCollection_1_t1868382577 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2815235757(__this, ___index0, method) ((  ChannelPacket_t1682596885  (*) (ReadOnlyCollection_1_t1868382577 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2815235757_gshared)(__this, ___index0, method)
