﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayFaceCamSettings
struct EveryplayFaceCamSettings_t438190306;

#include "codegen/il2cpp-codegen.h"

// System.Void EveryplayFaceCamSettings::.ctor()
extern "C"  void EveryplayFaceCamSettings__ctor_m3661674959 (EveryplayFaceCamSettings_t438190306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayFaceCamSettings::Start()
extern "C"  void EveryplayFaceCamSettings_Start_m1854962875 (EveryplayFaceCamSettings_t438190306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
