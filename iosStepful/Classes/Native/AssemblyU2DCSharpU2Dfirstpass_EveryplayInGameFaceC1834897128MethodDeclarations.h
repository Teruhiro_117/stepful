﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayInGameFaceCam
struct EveryplayInGameFaceCam_t1834897128;

#include "codegen/il2cpp-codegen.h"

// System.Void EveryplayInGameFaceCam::.ctor()
extern "C"  void EveryplayInGameFaceCam__ctor_m3920189967 (EveryplayInGameFaceCam_t1834897128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayInGameFaceCam::Awake()
extern "C"  void EveryplayInGameFaceCam_Awake_m2034936186 (EveryplayInGameFaceCam_t1834897128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayInGameFaceCam::OnSessionStart()
extern "C"  void EveryplayInGameFaceCam_OnSessionStart_m2138866836 (EveryplayInGameFaceCam_t1834897128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayInGameFaceCam::OnSessionStop()
extern "C"  void EveryplayInGameFaceCam_OnSessionStop_m2949283982 (EveryplayInGameFaceCam_t1834897128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayInGameFaceCam::OnDestroy()
extern "C"  void EveryplayInGameFaceCam_OnDestroy_m2012015128 (EveryplayInGameFaceCam_t1834897128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
