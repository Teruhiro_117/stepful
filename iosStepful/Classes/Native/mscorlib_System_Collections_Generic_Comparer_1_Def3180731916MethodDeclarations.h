﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct DefaultComparer_t3180731916;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo396494387.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.ctor()
extern "C"  void DefaultComparer__ctor_m4205971365_gshared (DefaultComparer_t3180731916 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4205971365(__this, method) ((  void (*) (DefaultComparer_t3180731916 *, const MethodInfo*))DefaultComparer__ctor_m4205971365_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3610470744_gshared (DefaultComparer_t3180731916 * __this, PeerInfoPlayer_t396494387  ___x0, PeerInfoPlayer_t396494387  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3610470744(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3180731916 *, PeerInfoPlayer_t396494387 , PeerInfoPlayer_t396494387 , const MethodInfo*))DefaultComparer_Compare_m3610470744_gshared)(__this, ___x0, ___y1, method)
