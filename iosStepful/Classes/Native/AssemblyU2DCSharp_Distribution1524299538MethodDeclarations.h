﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Distribution
struct Distribution_t1524299538;

#include "codegen/il2cpp-codegen.h"

// System.Void Distribution::.ctor()
extern "C"  void Distribution__ctor_m664235853 (Distribution_t1524299538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Distribution::Start()
extern "C"  void Distribution_Start_m3147594557 (Distribution_t1524299538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Distribution::Update()
extern "C"  void Distribution_Update_m1453560724 (Distribution_t1524299538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
