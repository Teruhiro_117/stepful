﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct List_1_t4188176625;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3722906299.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo524088197.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3505010156_gshared (Enumerator_t3722906299 * __this, List_1_t4188176625 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3505010156(__this, ___l0, method) ((  void (*) (Enumerator_t3722906299 *, List_1_t4188176625 *, const MethodInfo*))Enumerator__ctor_m3505010156_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2582989766_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2582989766(__this, method) ((  void (*) (Enumerator_t3722906299 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2582989766_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4023888536_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4023888536(__this, method) ((  Il2CppObject * (*) (Enumerator_t3722906299 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4023888536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Dispose()
extern "C"  void Enumerator_Dispose_m3211405525_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3211405525(__this, method) ((  void (*) (Enumerator_t3722906299 *, const MethodInfo*))Enumerator_Dispose_m3211405525_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::VerifyState()
extern "C"  void Enumerator_VerifyState_m453828750_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m453828750(__this, method) ((  void (*) (Enumerator_t3722906299 *, const MethodInfo*))Enumerator_VerifyState_m453828750_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m24086994_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m24086994(__this, method) ((  bool (*) (Enumerator_t3722906299 *, const MethodInfo*))Enumerator_MoveNext_m24086994_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::get_Current()
extern "C"  CRCMessageEntry_t524088197  Enumerator_get_Current_m2486588269_gshared (Enumerator_t3722906299 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2486588269(__this, method) ((  CRCMessageEntry_t524088197  (*) (Enumerator_t3722906299 *, const MethodInfo*))Enumerator_get_Current_m2486588269_gshared)(__this, method)
