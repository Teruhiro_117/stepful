﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StableAspect
struct StableAspect_t3385526103;

#include "codegen/il2cpp-codegen.h"

// System.Void StableAspect::.ctor()
extern "C"  void StableAspect__ctor_m227441362 (StableAspect_t3385526103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StableAspect::Awake()
extern "C"  void StableAspect_Awake_m3893144231 (StableAspect_t3385526103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
