﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkSystem.OverrideTransformMessage
struct OverrideTransformMessage_t3795345151;
// UnityEngine.Networking.NetworkReader
struct NetworkReader_t3187690923;
// UnityEngine.Networking.NetworkWriter
struct NetworkWriter_t560143343;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3187690923.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo560143343.h"

// System.Void UnityEngine.Networking.NetworkSystem.OverrideTransformMessage::.ctor()
extern "C"  void OverrideTransformMessage__ctor_m114836555 (OverrideTransformMessage_t3795345151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkSystem.OverrideTransformMessage::Deserialize(UnityEngine.Networking.NetworkReader)
extern "C"  void OverrideTransformMessage_Deserialize_m3976552446 (OverrideTransformMessage_t3795345151 * __this, NetworkReader_t3187690923 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkSystem.OverrideTransformMessage::Serialize(UnityEngine.Networking.NetworkWriter)
extern "C"  void OverrideTransformMessage_Serialize_m183450191 (OverrideTransformMessage_t3795345151 * __this, NetworkWriter_t560143343 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
