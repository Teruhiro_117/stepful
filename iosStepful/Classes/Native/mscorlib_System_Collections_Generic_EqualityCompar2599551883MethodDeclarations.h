﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.LocalClient/InternalMsg>
struct DefaultComparer_t2599551883;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor()
extern "C"  void DefaultComparer__ctor_m3126446482_gshared (DefaultComparer_t2599551883 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3126446482(__this, method) ((  void (*) (DefaultComparer_t2599551883 *, const MethodInfo*))DefaultComparer__ctor_m3126446482_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.LocalClient/InternalMsg>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m531940521_gshared (DefaultComparer_t2599551883 * __this, InternalMsg_t977621722  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m531940521(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2599551883 *, InternalMsg_t977621722 , const MethodInfo*))DefaultComparer_GetHashCode_m531940521_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.LocalClient/InternalMsg>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3588738037_gshared (DefaultComparer_t2599551883 * __this, InternalMsg_t977621722  ___x0, InternalMsg_t977621722  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3588738037(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2599551883 *, InternalMsg_t977621722 , InternalMsg_t977621722 , const MethodInfo*))DefaultComparer_Equals_m3588738037_gshared)(__this, ___x0, ___y1, method)
