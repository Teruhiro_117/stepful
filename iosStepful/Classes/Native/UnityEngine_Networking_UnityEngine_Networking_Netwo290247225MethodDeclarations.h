﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkSettingsAttribute
struct NetworkSettingsAttribute_t290247225;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.NetworkSettingsAttribute::.ctor()
extern "C"  void NetworkSettingsAttribute__ctor_m2935799676 (NetworkSettingsAttribute_t290247225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
