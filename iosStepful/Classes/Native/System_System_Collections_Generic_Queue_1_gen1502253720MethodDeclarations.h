﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>
struct Queue_1_t1502253720;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.ChannelPacket>
struct IEnumerator_1_t3453088008;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Networking.ChannelPacket[]
struct ChannelPacketU5BU5D_t3883591672;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan1682596885.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat2012316800.h"

// System.Void System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::.ctor()
extern "C"  void Queue_1__ctor_m1650990715_gshared (Queue_1_t1502253720 * __this, const MethodInfo* method);
#define Queue_1__ctor_m1650990715(__this, method) ((  void (*) (Queue_1_t1502253720 *, const MethodInfo*))Queue_1__ctor_m1650990715_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Queue_1_System_Collections_ICollection_CopyTo_m104232323_gshared (Queue_1_t1502253720 * __this, Il2CppArray * ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m104232323(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1502253720 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m104232323_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m3676553867_gshared (Queue_1_t1502253720 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m3676553867(__this, method) ((  bool (*) (Queue_1_t1502253720 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m3676553867_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Queue_1_System_Collections_ICollection_get_SyncRoot_m2445958931_gshared (Queue_1_t1502253720 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m2445958931(__this, method) ((  Il2CppObject * (*) (Queue_1_t1502253720 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m2445958931_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2855509775_gshared (Queue_1_t1502253720 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2855509775(__this, method) ((  Il2CppObject* (*) (Queue_1_t1502253720 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2855509775_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Queue_1_System_Collections_IEnumerable_GetEnumerator_m4130762806_gshared (Queue_1_t1502253720 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m4130762806(__this, method) ((  Il2CppObject * (*) (Queue_1_t1502253720 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m4130762806_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::Clear()
extern "C"  void Queue_1_Clear_m1372223400_gshared (Queue_1_t1502253720 * __this, const MethodInfo* method);
#define Queue_1_Clear_m1372223400(__this, method) ((  void (*) (Queue_1_t1502253720 *, const MethodInfo*))Queue_1_Clear_m1372223400_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::CopyTo(T[],System.Int32)
extern "C"  void Queue_1_CopyTo_m1384465012_gshared (Queue_1_t1502253720 * __this, ChannelPacketU5BU5D_t3883591672* ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_CopyTo_m1384465012(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t1502253720 *, ChannelPacketU5BU5D_t3883591672*, int32_t, const MethodInfo*))Queue_1_CopyTo_m1384465012_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::Dequeue()
extern "C"  ChannelPacket_t1682596885  Queue_1_Dequeue_m60102049_gshared (Queue_1_t1502253720 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m60102049(__this, method) ((  ChannelPacket_t1682596885  (*) (Queue_1_t1502253720 *, const MethodInfo*))Queue_1_Dequeue_m60102049_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::Peek()
extern "C"  ChannelPacket_t1682596885  Queue_1_Peek_m2444738515_gshared (Queue_1_t1502253720 * __this, const MethodInfo* method);
#define Queue_1_Peek_m2444738515(__this, method) ((  ChannelPacket_t1682596885  (*) (Queue_1_t1502253720 *, const MethodInfo*))Queue_1_Peek_m2444738515_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::Enqueue(T)
extern "C"  void Queue_1_Enqueue_m2118525456_gshared (Queue_1_t1502253720 * __this, ChannelPacket_t1682596885  ___item0, const MethodInfo* method);
#define Queue_1_Enqueue_m2118525456(__this, ___item0, method) ((  void (*) (Queue_1_t1502253720 *, ChannelPacket_t1682596885 , const MethodInfo*))Queue_1_Enqueue_m2118525456_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::SetCapacity(System.Int32)
extern "C"  void Queue_1_SetCapacity_m4287235452_gshared (Queue_1_t1502253720 * __this, int32_t ___new_size0, const MethodInfo* method);
#define Queue_1_SetCapacity_m4287235452(__this, ___new_size0, method) ((  void (*) (Queue_1_t1502253720 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m4287235452_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m4141623983_gshared (Queue_1_t1502253720 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m4141623983(__this, method) ((  int32_t (*) (Queue_1_t1502253720 *, const MethodInfo*))Queue_1_get_Count_m4141623983_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<UnityEngine.Networking.ChannelPacket>::GetEnumerator()
extern "C"  Enumerator_t2012316800  Queue_1_GetEnumerator_m2194637624_gshared (Queue_1_t1502253720 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m2194637624(__this, method) ((  Enumerator_t2012316800  (*) (Queue_1_t1502253720 *, const MethodInfo*))Queue_1_GetEnumerator_m2194637624_gshared)(__this, method)
