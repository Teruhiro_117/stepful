﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Everyplay
struct Everyplay_t1799077027;
// Everyplay/WasClosedDelegate
struct WasClosedDelegate_t946300984;
// System.Object
struct Il2CppObject;
// Everyplay/ReadyForRecordingDelegate
struct ReadyForRecordingDelegate_t1593758596;
// Everyplay/RecordingStartedDelegate
struct RecordingStartedDelegate_t5060419;
// Everyplay/RecordingStoppedDelegate
struct RecordingStoppedDelegate_t3008025639;
// Everyplay/FaceCamSessionStartedDelegate
struct FaceCamSessionStartedDelegate_t1733547424;
// Everyplay/FaceCamRecordingPermissionDelegate
struct FaceCamRecordingPermissionDelegate_t1670731619;
// Everyplay/FaceCamSessionStoppedDelegate
struct FaceCamSessionStoppedDelegate_t1894731428;
// Everyplay/ThumbnailReadyAtTextureIdDelegate
struct ThumbnailReadyAtTextureIdDelegate_t3853410271;
// Everyplay/ThumbnailTextureReadyDelegate
struct ThumbnailTextureReadyDelegate_t2948235259;
// Everyplay/UploadDidStartDelegate
struct UploadDidStartDelegate_t1871027361;
// Everyplay/UploadDidProgressDelegate
struct UploadDidProgressDelegate_t2069570344;
// Everyplay/UploadDidCompleteDelegate
struct UploadDidCompleteDelegate_t1564565876;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// Everyplay/RequestReadyDelegate
struct RequestReadyDelegate_t4013578405;
// Everyplay/RequestFailedDelegate
struct RequestFailedDelegate_t3135434785;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// EveryplayRecButtons
struct EveryplayRecButtons_t2003903990;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// Everyplay/<MakeRequestEnumerator>c__Iterator0
struct U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// EveryplayAnimatedThumbnail
struct EveryplayAnimatedThumbnail_t3478222492;
// UnityEngine.Renderer
struct Renderer_t257310565;
// EveryplayAnimatedThumbnail/<CrossfadeTransition>c__Iterator0
struct U3CCrossfadeTransitionU3Ec__Iterator0_t120047739;
// EveryplayAnimatedThumbnailOnGUI
struct EveryplayAnimatedThumbnailOnGUI_t1420574672;
// EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0
struct U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193;
// EveryplayEarlyInitializer
struct EveryplayEarlyInitializer_t4267781536;
// EveryplayFaceCamSettings
struct EveryplayFaceCamSettings_t438190306;
// EveryplayFaceCamTest
struct EveryplayFaceCamTest_t2314249671;
// UnityEngine.GUIText
struct GUIText_t2411476300;
// EveryplayHudCamera
struct EveryplayHudCamera_t847528103;
// EveryplayInGameFaceCam
struct EveryplayInGameFaceCam_t1834897128;
// EveryplayMiniJSON.Json/Parser
struct Parser_t2541283378;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// EveryplayMiniJSON.Json/Serializer
struct Serializer_t3671902477;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.IList
struct IList_t3321498491;
// EveryplayRecButtons/Button
struct Button_t3262243951;
// EveryplayRecButtons/TextureAtlasSrc
struct TextureAtlasSrc_t2048635151;
// EveryplayRecButtons/ButtonTapped
struct ButtonTapped_t3122824015;
// EveryplayRecButtons/ToggleButton
struct ToggleButton_t2881484729;
// EveryplaySettings
struct EveryplaySettings_t83776108;
// EveryplayTest
struct EveryplayTest_t137848885;
// EveryplayTest/<ResetUploadStatusAfterDelay>c__Iterator0
struct U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340;
// EveryplayThumbnailPool
struct EveryplayThumbnailPool_t101914191;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2724090252;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay1799077027.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay1799077027MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_WasClosedDe946300984.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_ReadyForRe1593758596.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RecordingStar5060419.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RecordingS3008025639.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamSes1733547424.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamRec1670731619.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamSes1894731428.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_ThumbnailR3853410271.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_ThumbnailT2948235259.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_UploadDidS1871027361.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_UploadDidP2069570344.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_UploadDidC1564565876.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplaySettings83776108MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplaySettings83776108.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_DllNotFoundException3636280042.h"
#include "mscorlib_System_EntryPointNotFoundException3956266210.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayMiniJSON_Js1761959520MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RequestRea4013578405.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RequestFai3135434785.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamRec3536210470.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamPre2262691368.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons2003903990.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_U3CMakeReq4090265924MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_U3CMakeReq4090265924.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_WasClosedDe946300984MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayDictionaryE2740111869MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_ReadyForRe1593758596MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayDictionaryE2740111869.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RecordingStar5060419MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RecordingS3008025639MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamSes1733547424MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamRec1670731619MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamSes1894731428MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_ThumbnailR3853410271MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_ThumbnailT2948235259MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_UploadDidS1871027361MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_UploadDidP2069570344MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_UploadDidC1564565876MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RequestFai3135434785MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_RequestRea4013578405MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamPre2262691368MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_FaceCamRec3536210470MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_UserInterfa934015732.h"
#include "AssemblyU2DCSharpU2Dfirstpass_Everyplay_UserInterfa934015732MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayAnimatedThu3478222492.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayAnimatedThu3478222492MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayThumbnailPoo101914191.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayAnimatedThum120047739MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayAnimatedThum120047739.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayThumbnailPoo101914191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayAnimatedThu1420574672.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayAnimatedThu1420574672MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayAnimatedThu4284962193MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayAnimatedThu4284962193.h"
#include "UnityEngine_UnityEngine_Event3028476042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventType3919834026.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Event3028476042.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayEarlyInitia4267781536.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayEarlyInitia4267781536MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayFaceCamSetti438190306.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayFaceCamSetti438190306MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayFaceCamTest2314249671.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayFaceCamTest2314249671MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIText2411476300MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIText2411476300.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAlignment1418134952.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayHudCamera847528103.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayHudCamera847528103MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemInfo2353426895MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_GL1765937205MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayInGameFaceC1834897128.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayInGameFaceC1834897128MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureFormat1386130234.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureWrapMode3683976566.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayMiniJSON_Js1761959520.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayMiniJSON_Js2541283378MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayMiniJSON_Js3671902477MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayMiniJSON_Js2541283378.h"
#include "mscorlib_System_IO_StringReader1480123486MethodDeclarations.h"
#include "mscorlib_System_IO_StringReader1480123486.h"
#include "mscorlib_System_IO_TextReader1561828458MethodDeclarations.h"
#include "mscorlib_System_IO_TextReader1561828458.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayMiniJSON_Js2035824124.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "mscorlib_System_Double4078015681MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayMiniJSON_Js2035824124MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayMiniJSON_Js3671902477.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Decimal724701077.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons2003903990MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_2048635151MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_3122824015MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_3262243951MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_2881484729MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2631365083MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_2048635151.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_3122824015.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_3262243951.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_2881484729.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2631365083.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2166094757.h"
#include "UnityEngine_UnityEngine_Touch407273883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2166094757MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_B591708518.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayRecButtons_B591708518MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayTest137848885.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayTest137848885MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayTest_U3CRes3422975340MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayTest_U3CRes3422975340.h"
#include "UnityEngine_UnityEngine_NPOTSupport2717356124.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"

// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/WasClosedDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisWasClosedDelegate_t946300984_m389186345(__this /* static, unused */, p0, p1, p2, method) ((  WasClosedDelegate_t946300984 * (*) (Il2CppObject * /* static, unused */, WasClosedDelegate_t946300984 **, WasClosedDelegate_t946300984 *, WasClosedDelegate_t946300984 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/ReadyForRecordingDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisReadyForRecordingDelegate_t1593758596_m4019323123(__this /* static, unused */, p0, p1, p2, method) ((  ReadyForRecordingDelegate_t1593758596 * (*) (Il2CppObject * /* static, unused */, ReadyForRecordingDelegate_t1593758596 **, ReadyForRecordingDelegate_t1593758596 *, ReadyForRecordingDelegate_t1593758596 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/RecordingStartedDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisRecordingStartedDelegate_t5060419_m2621168744(__this /* static, unused */, p0, p1, p2, method) ((  RecordingStartedDelegate_t5060419 * (*) (Il2CppObject * /* static, unused */, RecordingStartedDelegate_t5060419 **, RecordingStartedDelegate_t5060419 *, RecordingStartedDelegate_t5060419 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/RecordingStoppedDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisRecordingStoppedDelegate_t3008025639_m2231249096(__this /* static, unused */, p0, p1, p2, method) ((  RecordingStoppedDelegate_t3008025639 * (*) (Il2CppObject * /* static, unused */, RecordingStoppedDelegate_t3008025639 **, RecordingStoppedDelegate_t3008025639 *, RecordingStoppedDelegate_t3008025639 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/FaceCamSessionStartedDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisFaceCamSessionStartedDelegate_t1733547424_m1972887271(__this /* static, unused */, p0, p1, p2, method) ((  FaceCamSessionStartedDelegate_t1733547424 * (*) (Il2CppObject * /* static, unused */, FaceCamSessionStartedDelegate_t1733547424 **, FaceCamSessionStartedDelegate_t1733547424 *, FaceCamSessionStartedDelegate_t1733547424 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/FaceCamRecordingPermissionDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisFaceCamRecordingPermissionDelegate_t1670731619_m4073967244(__this /* static, unused */, p0, p1, p2, method) ((  FaceCamRecordingPermissionDelegate_t1670731619 * (*) (Il2CppObject * /* static, unused */, FaceCamRecordingPermissionDelegate_t1670731619 **, FaceCamRecordingPermissionDelegate_t1670731619 *, FaceCamRecordingPermissionDelegate_t1670731619 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/FaceCamSessionStoppedDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisFaceCamSessionStoppedDelegate_t1894731428_m1930528791(__this /* static, unused */, p0, p1, p2, method) ((  FaceCamSessionStoppedDelegate_t1894731428 * (*) (Il2CppObject * /* static, unused */, FaceCamSessionStoppedDelegate_t1894731428 **, FaceCamSessionStoppedDelegate_t1894731428 *, FaceCamSessionStoppedDelegate_t1894731428 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/ThumbnailReadyAtTextureIdDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisThumbnailReadyAtTextureIdDelegate_t3853410271_m2745179428(__this /* static, unused */, p0, p1, p2, method) ((  ThumbnailReadyAtTextureIdDelegate_t3853410271 * (*) (Il2CppObject * /* static, unused */, ThumbnailReadyAtTextureIdDelegate_t3853410271 **, ThumbnailReadyAtTextureIdDelegate_t3853410271 *, ThumbnailReadyAtTextureIdDelegate_t3853410271 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/ThumbnailTextureReadyDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisThumbnailTextureReadyDelegate_t2948235259_m3386589622(__this /* static, unused */, p0, p1, p2, method) ((  ThumbnailTextureReadyDelegate_t2948235259 * (*) (Il2CppObject * /* static, unused */, ThumbnailTextureReadyDelegate_t2948235259 **, ThumbnailTextureReadyDelegate_t2948235259 *, ThumbnailTextureReadyDelegate_t2948235259 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/UploadDidStartDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUploadDidStartDelegate_t1871027361_m2064771274(__this /* static, unused */, p0, p1, p2, method) ((  UploadDidStartDelegate_t1871027361 * (*) (Il2CppObject * /* static, unused */, UploadDidStartDelegate_t1871027361 **, UploadDidStartDelegate_t1871027361 *, UploadDidStartDelegate_t1871027361 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/UploadDidProgressDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUploadDidProgressDelegate_t2069570344_m497588097(__this /* static, unused */, p0, p1, p2, method) ((  UploadDidProgressDelegate_t2069570344 * (*) (Il2CppObject * /* static, unused */, UploadDidProgressDelegate_t2069570344 **, UploadDidProgressDelegate_t2069570344 *, UploadDidProgressDelegate_t2069570344 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<Everyplay/UploadDidCompleteDelegate>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUploadDidCompleteDelegate_t1564565876_m1785791917(__this /* static, unused */, p0, p1, p2, method) ((  UploadDidCompleteDelegate_t1564565876 * (*) (Il2CppObject * /* static, unused */, UploadDidCompleteDelegate_t1564565876 **, UploadDidCompleteDelegate_t1564565876 *, UploadDidCompleteDelegate_t1564565876 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3813873105(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Everyplay>()
#define GameObject_AddComponent_TisEveryplay_t1799077027_m501018835(__this, method) ((  Everyplay_t1799077027 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<EveryplayRecButtons>()
#define GameObject_AddComponent_TisEveryplayRecButtons_t2003903990_m4110002744(__this, method) ((  EveryplayRecButtons_t2003903990 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// System.Boolean EveryplayDictionaryExtensions::TryGetValue<System.Boolean>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String,T&)
extern "C"  bool EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dict0, String_t* ___key1, bool* ___value2, const MethodInfo* method);
#define EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406(__this /* static, unused */, ___dict0, ___key1, ___value2, method) ((  bool (*) (Il2CppObject * /* static, unused */, Dictionary_2_t309261261 *, String_t*, bool*, const MethodInfo*))EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406_gshared)(__this /* static, unused */, ___dict0, ___key1, ___value2, method)
// System.Boolean EveryplayDictionaryExtensions::TryGetValue<System.Int32>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String,T&)
extern "C"  bool EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dict0, String_t* ___key1, int32_t* ___value2, const MethodInfo* method);
#define EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134(__this /* static, unused */, ___dict0, ___key1, ___value2, method) ((  bool (*) (Il2CppObject * /* static, unused */, Dictionary_2_t309261261 *, String_t*, int32_t*, const MethodInfo*))EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134_gshared)(__this /* static, unused */, ___dict0, ___key1, ___value2, method)
// System.Boolean EveryplayDictionaryExtensions::TryGetValue<System.Int64>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String,T&)
extern "C"  bool EveryplayDictionaryExtensions_TryGetValue_TisInt64_t909078037_m16980239_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dict0, String_t* ___key1, int64_t* ___value2, const MethodInfo* method);
#define EveryplayDictionaryExtensions_TryGetValue_TisInt64_t909078037_m16980239(__this /* static, unused */, ___dict0, ___key1, ___value2, method) ((  bool (*) (Il2CppObject * /* static, unused */, Dictionary_2_t309261261 *, String_t*, int64_t*, const MethodInfo*))EveryplayDictionaryExtensions_TryGetValue_TisInt64_t909078037_m16980239_gshared)(__this /* static, unused */, ___dict0, ___key1, ___value2, method)
// System.Boolean EveryplayDictionaryExtensions::TryGetValue<System.Double>(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String,T&)
extern "C"  bool EveryplayDictionaryExtensions_TryGetValue_TisDouble_t4078015681_m3826697939_gshared (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dict0, String_t* ___key1, double* ___value2, const MethodInfo* method);
#define EveryplayDictionaryExtensions_TryGetValue_TisDouble_t4078015681_m3826697939(__this /* static, unused */, ___dict0, ___key1, ___value2, method) ((  bool (*) (Il2CppObject * /* static, unused */, Dictionary_2_t309261261 *, String_t*, double*, const MethodInfo*))EveryplayDictionaryExtensions_TryGetValue_TisDouble_t4078015681_m3826697939_gshared)(__this /* static, unused */, ___dict0, ___key1, ___value2, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t257310565_m141370815(__this, method) ((  Renderer_t257310565 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.GUIText>()
#define GameObject_GetComponent_TisGUIText_t2411476300_m3222579698(__this, method) ((  GUIText_t2411476300 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Everyplay::.ctor()
extern "C"  void Everyplay__ctor_m1083411806 (Everyplay_t1799077027 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Everyplay::add_WasClosed(Everyplay/WasClosedDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* WasClosedDelegate_t946300984_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_WasClosed_m3659009735_MetadataUsageId;
extern "C"  void Everyplay_add_WasClosed_m3659009735 (Il2CppObject * __this /* static, unused */, WasClosedDelegate_t946300984 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_WasClosed_m3659009735_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WasClosedDelegate_t946300984 * V_0 = NULL;
	WasClosedDelegate_t946300984 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		WasClosedDelegate_t946300984 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_WasClosed_2();
		V_0 = L_0;
	}

IL_0006:
	{
		WasClosedDelegate_t946300984 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		WasClosedDelegate_t946300984 * L_2 = V_1;
		WasClosedDelegate_t946300984 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		WasClosedDelegate_t946300984 * L_5 = V_0;
		WasClosedDelegate_t946300984 * L_6 = InterlockedCompareExchangeImpl<WasClosedDelegate_t946300984 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_WasClosed_2()), ((WasClosedDelegate_t946300984 *)CastclassSealed(L_4, WasClosedDelegate_t946300984_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		WasClosedDelegate_t946300984 * L_7 = V_0;
		WasClosedDelegate_t946300984 * L_8 = V_1;
		if ((!(((Il2CppObject*)(WasClosedDelegate_t946300984 *)L_7) == ((Il2CppObject*)(WasClosedDelegate_t946300984 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_WasClosed(Everyplay/WasClosedDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* WasClosedDelegate_t946300984_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_WasClosed_m483262024_MetadataUsageId;
extern "C"  void Everyplay_remove_WasClosed_m483262024 (Il2CppObject * __this /* static, unused */, WasClosedDelegate_t946300984 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_WasClosed_m483262024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WasClosedDelegate_t946300984 * V_0 = NULL;
	WasClosedDelegate_t946300984 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		WasClosedDelegate_t946300984 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_WasClosed_2();
		V_0 = L_0;
	}

IL_0006:
	{
		WasClosedDelegate_t946300984 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		WasClosedDelegate_t946300984 * L_2 = V_1;
		WasClosedDelegate_t946300984 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		WasClosedDelegate_t946300984 * L_5 = V_0;
		WasClosedDelegate_t946300984 * L_6 = InterlockedCompareExchangeImpl<WasClosedDelegate_t946300984 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_WasClosed_2()), ((WasClosedDelegate_t946300984 *)CastclassSealed(L_4, WasClosedDelegate_t946300984_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		WasClosedDelegate_t946300984 * L_7 = V_0;
		WasClosedDelegate_t946300984 * L_8 = V_1;
		if ((!(((Il2CppObject*)(WasClosedDelegate_t946300984 *)L_7) == ((Il2CppObject*)(WasClosedDelegate_t946300984 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::add_ReadyForRecording(Everyplay/ReadyForRecordingDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_ReadyForRecording_m3347212935_MetadataUsageId;
extern "C"  void Everyplay_add_ReadyForRecording_m3347212935 (Il2CppObject * __this /* static, unused */, ReadyForRecordingDelegate_t1593758596 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_ReadyForRecording_m3347212935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReadyForRecordingDelegate_t1593758596 * V_0 = NULL;
	ReadyForRecordingDelegate_t1593758596 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ReadyForRecordingDelegate_t1593758596 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ReadyForRecording_3();
		V_0 = L_0;
	}

IL_0006:
	{
		ReadyForRecordingDelegate_t1593758596 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ReadyForRecordingDelegate_t1593758596 * L_2 = V_1;
		ReadyForRecordingDelegate_t1593758596 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReadyForRecordingDelegate_t1593758596 * L_5 = V_0;
		ReadyForRecordingDelegate_t1593758596 * L_6 = InterlockedCompareExchangeImpl<ReadyForRecordingDelegate_t1593758596 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_ReadyForRecording_3()), ((ReadyForRecordingDelegate_t1593758596 *)CastclassSealed(L_4, ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReadyForRecordingDelegate_t1593758596 * L_7 = V_0;
		ReadyForRecordingDelegate_t1593758596 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ReadyForRecordingDelegate_t1593758596 *)L_7) == ((Il2CppObject*)(ReadyForRecordingDelegate_t1593758596 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_ReadyForRecording(Everyplay/ReadyForRecordingDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_ReadyForRecording_m557751688_MetadataUsageId;
extern "C"  void Everyplay_remove_ReadyForRecording_m557751688 (Il2CppObject * __this /* static, unused */, ReadyForRecordingDelegate_t1593758596 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_ReadyForRecording_m557751688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReadyForRecordingDelegate_t1593758596 * V_0 = NULL;
	ReadyForRecordingDelegate_t1593758596 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ReadyForRecordingDelegate_t1593758596 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ReadyForRecording_3();
		V_0 = L_0;
	}

IL_0006:
	{
		ReadyForRecordingDelegate_t1593758596 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ReadyForRecordingDelegate_t1593758596 * L_2 = V_1;
		ReadyForRecordingDelegate_t1593758596 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReadyForRecordingDelegate_t1593758596 * L_5 = V_0;
		ReadyForRecordingDelegate_t1593758596 * L_6 = InterlockedCompareExchangeImpl<ReadyForRecordingDelegate_t1593758596 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_ReadyForRecording_3()), ((ReadyForRecordingDelegate_t1593758596 *)CastclassSealed(L_4, ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReadyForRecordingDelegate_t1593758596 * L_7 = V_0;
		ReadyForRecordingDelegate_t1593758596 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ReadyForRecordingDelegate_t1593758596 *)L_7) == ((Il2CppObject*)(ReadyForRecordingDelegate_t1593758596 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::add_RecordingStarted(Everyplay/RecordingStartedDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_RecordingStarted_m1975802831_MetadataUsageId;
extern "C"  void Everyplay_add_RecordingStarted_m1975802831 (Il2CppObject * __this /* static, unused */, RecordingStartedDelegate_t5060419 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_RecordingStarted_m1975802831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RecordingStartedDelegate_t5060419 * V_0 = NULL;
	RecordingStartedDelegate_t5060419 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStartedDelegate_t5060419 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_RecordingStarted_4();
		V_0 = L_0;
	}

IL_0006:
	{
		RecordingStartedDelegate_t5060419 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStartedDelegate_t5060419 * L_2 = V_1;
		RecordingStartedDelegate_t5060419 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		RecordingStartedDelegate_t5060419 * L_5 = V_0;
		RecordingStartedDelegate_t5060419 * L_6 = InterlockedCompareExchangeImpl<RecordingStartedDelegate_t5060419 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_RecordingStarted_4()), ((RecordingStartedDelegate_t5060419 *)CastclassSealed(L_4, RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		RecordingStartedDelegate_t5060419 * L_7 = V_0;
		RecordingStartedDelegate_t5060419 * L_8 = V_1;
		if ((!(((Il2CppObject*)(RecordingStartedDelegate_t5060419 *)L_7) == ((Il2CppObject*)(RecordingStartedDelegate_t5060419 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_RecordingStarted(Everyplay/RecordingStartedDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_RecordingStarted_m1408178652_MetadataUsageId;
extern "C"  void Everyplay_remove_RecordingStarted_m1408178652 (Il2CppObject * __this /* static, unused */, RecordingStartedDelegate_t5060419 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_RecordingStarted_m1408178652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RecordingStartedDelegate_t5060419 * V_0 = NULL;
	RecordingStartedDelegate_t5060419 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStartedDelegate_t5060419 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_RecordingStarted_4();
		V_0 = L_0;
	}

IL_0006:
	{
		RecordingStartedDelegate_t5060419 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStartedDelegate_t5060419 * L_2 = V_1;
		RecordingStartedDelegate_t5060419 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		RecordingStartedDelegate_t5060419 * L_5 = V_0;
		RecordingStartedDelegate_t5060419 * L_6 = InterlockedCompareExchangeImpl<RecordingStartedDelegate_t5060419 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_RecordingStarted_4()), ((RecordingStartedDelegate_t5060419 *)CastclassSealed(L_4, RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		RecordingStartedDelegate_t5060419 * L_7 = V_0;
		RecordingStartedDelegate_t5060419 * L_8 = V_1;
		if ((!(((Il2CppObject*)(RecordingStartedDelegate_t5060419 *)L_7) == ((Il2CppObject*)(RecordingStartedDelegate_t5060419 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::add_RecordingStopped(Everyplay/RecordingStoppedDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_RecordingStopped_m2601012783_MetadataUsageId;
extern "C"  void Everyplay_add_RecordingStopped_m2601012783 (Il2CppObject * __this /* static, unused */, RecordingStoppedDelegate_t3008025639 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_RecordingStopped_m2601012783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RecordingStoppedDelegate_t3008025639 * V_0 = NULL;
	RecordingStoppedDelegate_t3008025639 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStoppedDelegate_t3008025639 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_RecordingStopped_5();
		V_0 = L_0;
	}

IL_0006:
	{
		RecordingStoppedDelegate_t3008025639 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStoppedDelegate_t3008025639 * L_2 = V_1;
		RecordingStoppedDelegate_t3008025639 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		RecordingStoppedDelegate_t3008025639 * L_5 = V_0;
		RecordingStoppedDelegate_t3008025639 * L_6 = InterlockedCompareExchangeImpl<RecordingStoppedDelegate_t3008025639 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_RecordingStopped_5()), ((RecordingStoppedDelegate_t3008025639 *)CastclassSealed(L_4, RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		RecordingStoppedDelegate_t3008025639 * L_7 = V_0;
		RecordingStoppedDelegate_t3008025639 * L_8 = V_1;
		if ((!(((Il2CppObject*)(RecordingStoppedDelegate_t3008025639 *)L_7) == ((Il2CppObject*)(RecordingStoppedDelegate_t3008025639 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_RecordingStopped(Everyplay/RecordingStoppedDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_RecordingStopped_m550227836_MetadataUsageId;
extern "C"  void Everyplay_remove_RecordingStopped_m550227836 (Il2CppObject * __this /* static, unused */, RecordingStoppedDelegate_t3008025639 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_RecordingStopped_m550227836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RecordingStoppedDelegate_t3008025639 * V_0 = NULL;
	RecordingStoppedDelegate_t3008025639 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStoppedDelegate_t3008025639 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_RecordingStopped_5();
		V_0 = L_0;
	}

IL_0006:
	{
		RecordingStoppedDelegate_t3008025639 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStoppedDelegate_t3008025639 * L_2 = V_1;
		RecordingStoppedDelegate_t3008025639 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		RecordingStoppedDelegate_t3008025639 * L_5 = V_0;
		RecordingStoppedDelegate_t3008025639 * L_6 = InterlockedCompareExchangeImpl<RecordingStoppedDelegate_t3008025639 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_RecordingStopped_5()), ((RecordingStoppedDelegate_t3008025639 *)CastclassSealed(L_4, RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		RecordingStoppedDelegate_t3008025639 * L_7 = V_0;
		RecordingStoppedDelegate_t3008025639 * L_8 = V_1;
		if ((!(((Il2CppObject*)(RecordingStoppedDelegate_t3008025639 *)L_7) == ((Il2CppObject*)(RecordingStoppedDelegate_t3008025639 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::add_FaceCamSessionStarted(Everyplay/FaceCamSessionStartedDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* FaceCamSessionStartedDelegate_t1733547424_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_FaceCamSessionStarted_m3896409671_MetadataUsageId;
extern "C"  void Everyplay_add_FaceCamSessionStarted_m3896409671 (Il2CppObject * __this /* static, unused */, FaceCamSessionStartedDelegate_t1733547424 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_FaceCamSessionStarted_m3896409671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FaceCamSessionStartedDelegate_t1733547424 * V_0 = NULL;
	FaceCamSessionStartedDelegate_t1733547424 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStartedDelegate_t1733547424 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamSessionStarted_6();
		V_0 = L_0;
	}

IL_0006:
	{
		FaceCamSessionStartedDelegate_t1733547424 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStartedDelegate_t1733547424 * L_2 = V_1;
		FaceCamSessionStartedDelegate_t1733547424 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		FaceCamSessionStartedDelegate_t1733547424 * L_5 = V_0;
		FaceCamSessionStartedDelegate_t1733547424 * L_6 = InterlockedCompareExchangeImpl<FaceCamSessionStartedDelegate_t1733547424 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_FaceCamSessionStarted_6()), ((FaceCamSessionStartedDelegate_t1733547424 *)CastclassSealed(L_4, FaceCamSessionStartedDelegate_t1733547424_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		FaceCamSessionStartedDelegate_t1733547424 * L_7 = V_0;
		FaceCamSessionStartedDelegate_t1733547424 * L_8 = V_1;
		if ((!(((Il2CppObject*)(FaceCamSessionStartedDelegate_t1733547424 *)L_7) == ((Il2CppObject*)(FaceCamSessionStartedDelegate_t1733547424 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_FaceCamSessionStarted(Everyplay/FaceCamSessionStartedDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* FaceCamSessionStartedDelegate_t1733547424_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_FaceCamSessionStarted_m2342726600_MetadataUsageId;
extern "C"  void Everyplay_remove_FaceCamSessionStarted_m2342726600 (Il2CppObject * __this /* static, unused */, FaceCamSessionStartedDelegate_t1733547424 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_FaceCamSessionStarted_m2342726600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FaceCamSessionStartedDelegate_t1733547424 * V_0 = NULL;
	FaceCamSessionStartedDelegate_t1733547424 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStartedDelegate_t1733547424 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamSessionStarted_6();
		V_0 = L_0;
	}

IL_0006:
	{
		FaceCamSessionStartedDelegate_t1733547424 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStartedDelegate_t1733547424 * L_2 = V_1;
		FaceCamSessionStartedDelegate_t1733547424 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		FaceCamSessionStartedDelegate_t1733547424 * L_5 = V_0;
		FaceCamSessionStartedDelegate_t1733547424 * L_6 = InterlockedCompareExchangeImpl<FaceCamSessionStartedDelegate_t1733547424 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_FaceCamSessionStarted_6()), ((FaceCamSessionStartedDelegate_t1733547424 *)CastclassSealed(L_4, FaceCamSessionStartedDelegate_t1733547424_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		FaceCamSessionStartedDelegate_t1733547424 * L_7 = V_0;
		FaceCamSessionStartedDelegate_t1733547424 * L_8 = V_1;
		if ((!(((Il2CppObject*)(FaceCamSessionStartedDelegate_t1733547424 *)L_7) == ((Il2CppObject*)(FaceCamSessionStartedDelegate_t1733547424 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::add_FaceCamRecordingPermission(Everyplay/FaceCamRecordingPermissionDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_FaceCamRecordingPermission_m574143351_MetadataUsageId;
extern "C"  void Everyplay_add_FaceCamRecordingPermission_m574143351 (Il2CppObject * __this /* static, unused */, FaceCamRecordingPermissionDelegate_t1670731619 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_FaceCamRecordingPermission_m574143351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FaceCamRecordingPermissionDelegate_t1670731619 * V_0 = NULL;
	FaceCamRecordingPermissionDelegate_t1670731619 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamRecordingPermission_7();
		V_0 = L_0;
	}

IL_0006:
	{
		FaceCamRecordingPermissionDelegate_t1670731619 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_2 = V_1;
		FaceCamRecordingPermissionDelegate_t1670731619 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_5 = V_0;
		FaceCamRecordingPermissionDelegate_t1670731619 * L_6 = InterlockedCompareExchangeImpl<FaceCamRecordingPermissionDelegate_t1670731619 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_FaceCamRecordingPermission_7()), ((FaceCamRecordingPermissionDelegate_t1670731619 *)CastclassSealed(L_4, FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		FaceCamRecordingPermissionDelegate_t1670731619 * L_7 = V_0;
		FaceCamRecordingPermissionDelegate_t1670731619 * L_8 = V_1;
		if ((!(((Il2CppObject*)(FaceCamRecordingPermissionDelegate_t1670731619 *)L_7) == ((Il2CppObject*)(FaceCamRecordingPermissionDelegate_t1670731619 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_FaceCamRecordingPermission(Everyplay/FaceCamRecordingPermissionDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_FaceCamRecordingPermission_m4283361868_MetadataUsageId;
extern "C"  void Everyplay_remove_FaceCamRecordingPermission_m4283361868 (Il2CppObject * __this /* static, unused */, FaceCamRecordingPermissionDelegate_t1670731619 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_FaceCamRecordingPermission_m4283361868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FaceCamRecordingPermissionDelegate_t1670731619 * V_0 = NULL;
	FaceCamRecordingPermissionDelegate_t1670731619 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamRecordingPermission_7();
		V_0 = L_0;
	}

IL_0006:
	{
		FaceCamRecordingPermissionDelegate_t1670731619 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_2 = V_1;
		FaceCamRecordingPermissionDelegate_t1670731619 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_5 = V_0;
		FaceCamRecordingPermissionDelegate_t1670731619 * L_6 = InterlockedCompareExchangeImpl<FaceCamRecordingPermissionDelegate_t1670731619 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_FaceCamRecordingPermission_7()), ((FaceCamRecordingPermissionDelegate_t1670731619 *)CastclassSealed(L_4, FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		FaceCamRecordingPermissionDelegate_t1670731619 * L_7 = V_0;
		FaceCamRecordingPermissionDelegate_t1670731619 * L_8 = V_1;
		if ((!(((Il2CppObject*)(FaceCamRecordingPermissionDelegate_t1670731619 *)L_7) == ((Il2CppObject*)(FaceCamRecordingPermissionDelegate_t1670731619 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::add_FaceCamSessionStopped(Everyplay/FaceCamSessionStoppedDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* FaceCamSessionStoppedDelegate_t1894731428_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_FaceCamSessionStopped_m2491956679_MetadataUsageId;
extern "C"  void Everyplay_add_FaceCamSessionStopped_m2491956679 (Il2CppObject * __this /* static, unused */, FaceCamSessionStoppedDelegate_t1894731428 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_FaceCamSessionStopped_m2491956679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FaceCamSessionStoppedDelegate_t1894731428 * V_0 = NULL;
	FaceCamSessionStoppedDelegate_t1894731428 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStoppedDelegate_t1894731428 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamSessionStopped_8();
		V_0 = L_0;
	}

IL_0006:
	{
		FaceCamSessionStoppedDelegate_t1894731428 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStoppedDelegate_t1894731428 * L_2 = V_1;
		FaceCamSessionStoppedDelegate_t1894731428 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		FaceCamSessionStoppedDelegate_t1894731428 * L_5 = V_0;
		FaceCamSessionStoppedDelegate_t1894731428 * L_6 = InterlockedCompareExchangeImpl<FaceCamSessionStoppedDelegate_t1894731428 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_FaceCamSessionStopped_8()), ((FaceCamSessionStoppedDelegate_t1894731428 *)CastclassSealed(L_4, FaceCamSessionStoppedDelegate_t1894731428_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		FaceCamSessionStoppedDelegate_t1894731428 * L_7 = V_0;
		FaceCamSessionStoppedDelegate_t1894731428 * L_8 = V_1;
		if ((!(((Il2CppObject*)(FaceCamSessionStoppedDelegate_t1894731428 *)L_7) == ((Il2CppObject*)(FaceCamSessionStoppedDelegate_t1894731428 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_FaceCamSessionStopped(Everyplay/FaceCamSessionStoppedDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* FaceCamSessionStoppedDelegate_t1894731428_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_FaceCamSessionStopped_m3063767112_MetadataUsageId;
extern "C"  void Everyplay_remove_FaceCamSessionStopped_m3063767112 (Il2CppObject * __this /* static, unused */, FaceCamSessionStoppedDelegate_t1894731428 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_FaceCamSessionStopped_m3063767112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FaceCamSessionStoppedDelegate_t1894731428 * V_0 = NULL;
	FaceCamSessionStoppedDelegate_t1894731428 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStoppedDelegate_t1894731428 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamSessionStopped_8();
		V_0 = L_0;
	}

IL_0006:
	{
		FaceCamSessionStoppedDelegate_t1894731428 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStoppedDelegate_t1894731428 * L_2 = V_1;
		FaceCamSessionStoppedDelegate_t1894731428 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		FaceCamSessionStoppedDelegate_t1894731428 * L_5 = V_0;
		FaceCamSessionStoppedDelegate_t1894731428 * L_6 = InterlockedCompareExchangeImpl<FaceCamSessionStoppedDelegate_t1894731428 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_FaceCamSessionStopped_8()), ((FaceCamSessionStoppedDelegate_t1894731428 *)CastclassSealed(L_4, FaceCamSessionStoppedDelegate_t1894731428_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		FaceCamSessionStoppedDelegate_t1894731428 * L_7 = V_0;
		FaceCamSessionStoppedDelegate_t1894731428 * L_8 = V_1;
		if ((!(((Il2CppObject*)(FaceCamSessionStoppedDelegate_t1894731428 *)L_7) == ((Il2CppObject*)(FaceCamSessionStoppedDelegate_t1894731428 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::add_ThumbnailReadyAtTextureId(Everyplay/ThumbnailReadyAtTextureIdDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* ThumbnailReadyAtTextureIdDelegate_t3853410271_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_ThumbnailReadyAtTextureId_m1102520263_MetadataUsageId;
extern "C"  void Everyplay_add_ThumbnailReadyAtTextureId_m1102520263 (Il2CppObject * __this /* static, unused */, ThumbnailReadyAtTextureIdDelegate_t3853410271 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_ThumbnailReadyAtTextureId_m1102520263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ThumbnailReadyAtTextureIdDelegate_t3853410271 * V_0 = NULL;
	ThumbnailReadyAtTextureIdDelegate_t3853410271 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailReadyAtTextureId_9();
		V_0 = L_0;
	}

IL_0006:
	{
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_2 = V_1;
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_5 = V_0;
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_6 = InterlockedCompareExchangeImpl<ThumbnailReadyAtTextureIdDelegate_t3853410271 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_ThumbnailReadyAtTextureId_9()), ((ThumbnailReadyAtTextureIdDelegate_t3853410271 *)CastclassSealed(L_4, ThumbnailReadyAtTextureIdDelegate_t3853410271_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_7 = V_0;
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ThumbnailReadyAtTextureIdDelegate_t3853410271 *)L_7) == ((Il2CppObject*)(ThumbnailReadyAtTextureIdDelegate_t3853410271 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_ThumbnailReadyAtTextureId(Everyplay/ThumbnailReadyAtTextureIdDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* ThumbnailReadyAtTextureIdDelegate_t3853410271_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_ThumbnailReadyAtTextureId_m2712126024_MetadataUsageId;
extern "C"  void Everyplay_remove_ThumbnailReadyAtTextureId_m2712126024 (Il2CppObject * __this /* static, unused */, ThumbnailReadyAtTextureIdDelegate_t3853410271 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_ThumbnailReadyAtTextureId_m2712126024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ThumbnailReadyAtTextureIdDelegate_t3853410271 * V_0 = NULL;
	ThumbnailReadyAtTextureIdDelegate_t3853410271 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailReadyAtTextureId_9();
		V_0 = L_0;
	}

IL_0006:
	{
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_2 = V_1;
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_5 = V_0;
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_6 = InterlockedCompareExchangeImpl<ThumbnailReadyAtTextureIdDelegate_t3853410271 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_ThumbnailReadyAtTextureId_9()), ((ThumbnailReadyAtTextureIdDelegate_t3853410271 *)CastclassSealed(L_4, ThumbnailReadyAtTextureIdDelegate_t3853410271_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_7 = V_0;
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ThumbnailReadyAtTextureIdDelegate_t3853410271 *)L_7) == ((Il2CppObject*)(ThumbnailReadyAtTextureIdDelegate_t3853410271 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::add_ThumbnailTextureReady(Everyplay/ThumbnailTextureReadyDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* ThumbnailTextureReadyDelegate_t2948235259_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_ThumbnailTextureReady_m1784642183_MetadataUsageId;
extern "C"  void Everyplay_add_ThumbnailTextureReady_m1784642183 (Il2CppObject * __this /* static, unused */, ThumbnailTextureReadyDelegate_t2948235259 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_ThumbnailTextureReady_m1784642183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ThumbnailTextureReadyDelegate_t2948235259 * V_0 = NULL;
	ThumbnailTextureReadyDelegate_t2948235259 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailTextureReadyDelegate_t2948235259 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailTextureReady_10();
		V_0 = L_0;
	}

IL_0006:
	{
		ThumbnailTextureReadyDelegate_t2948235259 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailTextureReadyDelegate_t2948235259 * L_2 = V_1;
		ThumbnailTextureReadyDelegate_t2948235259 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ThumbnailTextureReadyDelegate_t2948235259 * L_5 = V_0;
		ThumbnailTextureReadyDelegate_t2948235259 * L_6 = InterlockedCompareExchangeImpl<ThumbnailTextureReadyDelegate_t2948235259 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_ThumbnailTextureReady_10()), ((ThumbnailTextureReadyDelegate_t2948235259 *)CastclassSealed(L_4, ThumbnailTextureReadyDelegate_t2948235259_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ThumbnailTextureReadyDelegate_t2948235259 * L_7 = V_0;
		ThumbnailTextureReadyDelegate_t2948235259 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ThumbnailTextureReadyDelegate_t2948235259 *)L_7) == ((Il2CppObject*)(ThumbnailTextureReadyDelegate_t2948235259 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_ThumbnailTextureReady(Everyplay/ThumbnailTextureReadyDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* ThumbnailTextureReadyDelegate_t2948235259_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_ThumbnailTextureReady_m3139354760_MetadataUsageId;
extern "C"  void Everyplay_remove_ThumbnailTextureReady_m3139354760 (Il2CppObject * __this /* static, unused */, ThumbnailTextureReadyDelegate_t2948235259 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_ThumbnailTextureReady_m3139354760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ThumbnailTextureReadyDelegate_t2948235259 * V_0 = NULL;
	ThumbnailTextureReadyDelegate_t2948235259 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailTextureReadyDelegate_t2948235259 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailTextureReady_10();
		V_0 = L_0;
	}

IL_0006:
	{
		ThumbnailTextureReadyDelegate_t2948235259 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailTextureReadyDelegate_t2948235259 * L_2 = V_1;
		ThumbnailTextureReadyDelegate_t2948235259 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ThumbnailTextureReadyDelegate_t2948235259 * L_5 = V_0;
		ThumbnailTextureReadyDelegate_t2948235259 * L_6 = InterlockedCompareExchangeImpl<ThumbnailTextureReadyDelegate_t2948235259 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_ThumbnailTextureReady_10()), ((ThumbnailTextureReadyDelegate_t2948235259 *)CastclassSealed(L_4, ThumbnailTextureReadyDelegate_t2948235259_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ThumbnailTextureReadyDelegate_t2948235259 * L_7 = V_0;
		ThumbnailTextureReadyDelegate_t2948235259 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ThumbnailTextureReadyDelegate_t2948235259 *)L_7) == ((Il2CppObject*)(ThumbnailTextureReadyDelegate_t2948235259 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::add_UploadDidStart(Everyplay/UploadDidStartDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidStartDelegate_t1871027361_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_UploadDidStart_m2150585455_MetadataUsageId;
extern "C"  void Everyplay_add_UploadDidStart_m2150585455 (Il2CppObject * __this /* static, unused */, UploadDidStartDelegate_t1871027361 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_UploadDidStart_m2150585455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UploadDidStartDelegate_t1871027361 * V_0 = NULL;
	UploadDidStartDelegate_t1871027361 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidStartDelegate_t1871027361 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidStart_11();
		V_0 = L_0;
	}

IL_0006:
	{
		UploadDidStartDelegate_t1871027361 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidStartDelegate_t1871027361 * L_2 = V_1;
		UploadDidStartDelegate_t1871027361 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		UploadDidStartDelegate_t1871027361 * L_5 = V_0;
		UploadDidStartDelegate_t1871027361 * L_6 = InterlockedCompareExchangeImpl<UploadDidStartDelegate_t1871027361 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_UploadDidStart_11()), ((UploadDidStartDelegate_t1871027361 *)CastclassSealed(L_4, UploadDidStartDelegate_t1871027361_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		UploadDidStartDelegate_t1871027361 * L_7 = V_0;
		UploadDidStartDelegate_t1871027361 * L_8 = V_1;
		if ((!(((Il2CppObject*)(UploadDidStartDelegate_t1871027361 *)L_7) == ((Il2CppObject*)(UploadDidStartDelegate_t1871027361 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_UploadDidStart(Everyplay/UploadDidStartDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidStartDelegate_t1871027361_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_UploadDidStart_m1799453144_MetadataUsageId;
extern "C"  void Everyplay_remove_UploadDidStart_m1799453144 (Il2CppObject * __this /* static, unused */, UploadDidStartDelegate_t1871027361 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_UploadDidStart_m1799453144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UploadDidStartDelegate_t1871027361 * V_0 = NULL;
	UploadDidStartDelegate_t1871027361 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidStartDelegate_t1871027361 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidStart_11();
		V_0 = L_0;
	}

IL_0006:
	{
		UploadDidStartDelegate_t1871027361 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidStartDelegate_t1871027361 * L_2 = V_1;
		UploadDidStartDelegate_t1871027361 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		UploadDidStartDelegate_t1871027361 * L_5 = V_0;
		UploadDidStartDelegate_t1871027361 * L_6 = InterlockedCompareExchangeImpl<UploadDidStartDelegate_t1871027361 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_UploadDidStart_11()), ((UploadDidStartDelegate_t1871027361 *)CastclassSealed(L_4, UploadDidStartDelegate_t1871027361_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		UploadDidStartDelegate_t1871027361 * L_7 = V_0;
		UploadDidStartDelegate_t1871027361 * L_8 = V_1;
		if ((!(((Il2CppObject*)(UploadDidStartDelegate_t1871027361 *)L_7) == ((Il2CppObject*)(UploadDidStartDelegate_t1871027361 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::add_UploadDidProgress(Everyplay/UploadDidProgressDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidProgressDelegate_t2069570344_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_UploadDidProgress_m1561866631_MetadataUsageId;
extern "C"  void Everyplay_add_UploadDidProgress_m1561866631 (Il2CppObject * __this /* static, unused */, UploadDidProgressDelegate_t2069570344 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_UploadDidProgress_m1561866631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UploadDidProgressDelegate_t2069570344 * V_0 = NULL;
	UploadDidProgressDelegate_t2069570344 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidProgressDelegate_t2069570344 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidProgress_12();
		V_0 = L_0;
	}

IL_0006:
	{
		UploadDidProgressDelegate_t2069570344 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidProgressDelegate_t2069570344 * L_2 = V_1;
		UploadDidProgressDelegate_t2069570344 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		UploadDidProgressDelegate_t2069570344 * L_5 = V_0;
		UploadDidProgressDelegate_t2069570344 * L_6 = InterlockedCompareExchangeImpl<UploadDidProgressDelegate_t2069570344 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_UploadDidProgress_12()), ((UploadDidProgressDelegate_t2069570344 *)CastclassSealed(L_4, UploadDidProgressDelegate_t2069570344_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		UploadDidProgressDelegate_t2069570344 * L_7 = V_0;
		UploadDidProgressDelegate_t2069570344 * L_8 = V_1;
		if ((!(((Il2CppObject*)(UploadDidProgressDelegate_t2069570344 *)L_7) == ((Il2CppObject*)(UploadDidProgressDelegate_t2069570344 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_UploadDidProgress(Everyplay/UploadDidProgressDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidProgressDelegate_t2069570344_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_UploadDidProgress_m4200454792_MetadataUsageId;
extern "C"  void Everyplay_remove_UploadDidProgress_m4200454792 (Il2CppObject * __this /* static, unused */, UploadDidProgressDelegate_t2069570344 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_UploadDidProgress_m4200454792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UploadDidProgressDelegate_t2069570344 * V_0 = NULL;
	UploadDidProgressDelegate_t2069570344 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidProgressDelegate_t2069570344 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidProgress_12();
		V_0 = L_0;
	}

IL_0006:
	{
		UploadDidProgressDelegate_t2069570344 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidProgressDelegate_t2069570344 * L_2 = V_1;
		UploadDidProgressDelegate_t2069570344 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		UploadDidProgressDelegate_t2069570344 * L_5 = V_0;
		UploadDidProgressDelegate_t2069570344 * L_6 = InterlockedCompareExchangeImpl<UploadDidProgressDelegate_t2069570344 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_UploadDidProgress_12()), ((UploadDidProgressDelegate_t2069570344 *)CastclassSealed(L_4, UploadDidProgressDelegate_t2069570344_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		UploadDidProgressDelegate_t2069570344 * L_7 = V_0;
		UploadDidProgressDelegate_t2069570344 * L_8 = V_1;
		if ((!(((Il2CppObject*)(UploadDidProgressDelegate_t2069570344 *)L_7) == ((Il2CppObject*)(UploadDidProgressDelegate_t2069570344 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::add_UploadDidComplete(Everyplay/UploadDidCompleteDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidCompleteDelegate_t1564565876_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_add_UploadDidComplete_m176217159_MetadataUsageId;
extern "C"  void Everyplay_add_UploadDidComplete_m176217159 (Il2CppObject * __this /* static, unused */, UploadDidCompleteDelegate_t1564565876 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_add_UploadDidComplete_m176217159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UploadDidCompleteDelegate_t1564565876 * V_0 = NULL;
	UploadDidCompleteDelegate_t1564565876 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidCompleteDelegate_t1564565876 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidComplete_13();
		V_0 = L_0;
	}

IL_0006:
	{
		UploadDidCompleteDelegate_t1564565876 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidCompleteDelegate_t1564565876 * L_2 = V_1;
		UploadDidCompleteDelegate_t1564565876 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		UploadDidCompleteDelegate_t1564565876 * L_5 = V_0;
		UploadDidCompleteDelegate_t1564565876 * L_6 = InterlockedCompareExchangeImpl<UploadDidCompleteDelegate_t1564565876 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_UploadDidComplete_13()), ((UploadDidCompleteDelegate_t1564565876 *)CastclassSealed(L_4, UploadDidCompleteDelegate_t1564565876_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		UploadDidCompleteDelegate_t1564565876 * L_7 = V_0;
		UploadDidCompleteDelegate_t1564565876 * L_8 = V_1;
		if ((!(((Il2CppObject*)(UploadDidCompleteDelegate_t1564565876 *)L_7) == ((Il2CppObject*)(UploadDidCompleteDelegate_t1564565876 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void Everyplay::remove_UploadDidComplete(Everyplay/UploadDidCompleteDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidCompleteDelegate_t1564565876_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_remove_UploadDidComplete_m3377286600_MetadataUsageId;
extern "C"  void Everyplay_remove_UploadDidComplete_m3377286600 (Il2CppObject * __this /* static, unused */, UploadDidCompleteDelegate_t1564565876 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_remove_UploadDidComplete_m3377286600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UploadDidCompleteDelegate_t1564565876 * V_0 = NULL;
	UploadDidCompleteDelegate_t1564565876 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidCompleteDelegate_t1564565876 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidComplete_13();
		V_0 = L_0;
	}

IL_0006:
	{
		UploadDidCompleteDelegate_t1564565876 * L_1 = V_0;
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidCompleteDelegate_t1564565876 * L_2 = V_1;
		UploadDidCompleteDelegate_t1564565876 * L_3 = ___value0;
		Delegate_t3022476291 * L_4 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		UploadDidCompleteDelegate_t1564565876 * L_5 = V_0;
		UploadDidCompleteDelegate_t1564565876 * L_6 = InterlockedCompareExchangeImpl<UploadDidCompleteDelegate_t1564565876 *>((((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_address_of_UploadDidComplete_13()), ((UploadDidCompleteDelegate_t1564565876 *)CastclassSealed(L_4, UploadDidCompleteDelegate_t1564565876_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		UploadDidCompleteDelegate_t1564565876 * L_7 = V_0;
		UploadDidCompleteDelegate_t1564565876 * L_8 = V_1;
		if ((!(((Il2CppObject*)(UploadDidCompleteDelegate_t1564565876 *)L_7) == ((Il2CppObject*)(UploadDidCompleteDelegate_t1564565876 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// Everyplay Everyplay::get_EveryplayInstance()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* EveryplaySettings_t83776108_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DllNotFoundException_t3636280042_il2cpp_TypeInfo_var;
extern Il2CppClass* EntryPointNotFoundException_t3956266210_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisEveryplay_t1799077027_m501018835_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral811854626;
extern Il2CppCodeGenString* _stringLiteral2527155545;
extern const uint32_t Everyplay_get_EveryplayInstance_m3310382687_MetadataUsageId;
extern "C"  Everyplay_t1799077027 * Everyplay_get_EveryplayInstance_m3310382687 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_get_EveryplayInstance_m3310382687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EveryplaySettings_t83776108 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	Everyplay_t1799077027 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_everyplayInstance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0119;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_appIsClosing_15();
		if (L_2)
		{
			goto IL_0119;
		}
	}
	{
		Object_t1021602117 * L_3 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral811854626, /*hidden argument*/NULL);
		V_0 = ((EveryplaySettings_t83776108 *)CastclassClass(L_3, EveryplaySettings_t83776108_il2cpp_TypeInfo_var));
		EveryplaySettings_t83776108 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0119;
		}
	}
	{
		EveryplaySettings_t83776108 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = EveryplaySettings_get_IsEnabled_m4208345999(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0119;
		}
	}
	{
		GameObject_t1756533147 * L_8 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_8, _stringLiteral2527155545, /*hidden argument*/NULL);
		V_1 = L_8;
		GameObject_t1756533147 * L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0119;
		}
	}
	{
		GameObject_t1756533147 * L_11 = V_1;
		GameObject_t1756533147 * L_12 = V_1;
		NullCheck(L_12);
		String_t* L_13 = Object_get_name_m2079638459(L_12, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_14 = V_1;
		NullCheck(L_14);
		int32_t L_15 = Object_GetInstanceID_m1920497914(L_14, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		Il2CppObject * L_17 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m56707527(NULL /*static, unused*/, L_13, L_17, /*hidden argument*/NULL);
		NullCheck(L_11);
		Object_set_name_m4157836998(L_11, L_18, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_19 = V_1;
		NullCheck(L_19);
		Everyplay_t1799077027 * L_20 = GameObject_AddComponent_TisEveryplay_t1799077027_m501018835(L_19, /*hidden argument*/GameObject_AddComponent_TisEveryplay_t1799077027_m501018835_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_everyplayInstance_20(L_20);
		Everyplay_t1799077027 * L_21 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_everyplayInstance_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_21, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0119;
		}
	}
	{
		EveryplaySettings_t83776108 * L_23 = V_0;
		NullCheck(L_23);
		String_t* L_24 = L_23->get_clientId_2();
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_clientId_14(L_24);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_hasMethods_16((bool)1);
	}

IL_00a0:
	try
	{ // begin try (depth: 1)
		EveryplaySettings_t83776108 * L_25 = V_0;
		NullCheck(L_25);
		String_t* L_26 = L_25->get_clientId_2();
		EveryplaySettings_t83776108 * L_27 = V_0;
		NullCheck(L_27);
		String_t* L_28 = L_27->get_clientSecret_3();
		EveryplaySettings_t83776108 * L_29 = V_0;
		NullCheck(L_29);
		String_t* L_30 = L_29->get_redirectURI_4();
		GameObject_t1756533147 * L_31 = V_1;
		NullCheck(L_31);
		String_t* L_32 = Object_get_name_m2079638459(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_InitEveryplay_m927127947(NULL /*static, unused*/, L_26, L_28, L_30, L_32, /*hidden argument*/NULL);
		goto IL_00f2;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (DllNotFoundException_t3636280042_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00c2;
		if(il2cpp_codegen_class_is_assignable_from (EntryPointNotFoundException_t3956266210_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00da;
		throw e;
	}

CATCH_00c2:
	{ // begin catch(System.DllNotFoundException)
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_hasMethods_16((bool)0);
		Everyplay_t1799077027 * L_33 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_everyplayInstance_20();
		NullCheck(L_33);
		Everyplay_OnApplicationQuit_m2101183012(L_33, /*hidden argument*/NULL);
		V_2 = (Everyplay_t1799077027 *)NULL;
		goto IL_011f;
	} // end catch (depth: 1)

CATCH_00da:
	{ // begin catch(System.EntryPointNotFoundException)
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_hasMethods_16((bool)0);
		Everyplay_t1799077027 * L_34 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_everyplayInstance_20();
		NullCheck(L_34);
		Everyplay_OnApplicationQuit_m2101183012(L_34, /*hidden argument*/NULL);
		V_2 = (Everyplay_t1799077027 *)NULL;
		goto IL_011f;
	} // end catch (depth: 1)

IL_00f2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_35 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_seenInitialization_17();
		if (L_35)
		{
			goto IL_00fc;
		}
	}

IL_00fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_seenInitialization_17((bool)1);
		EveryplaySettings_t83776108 * L_36 = V_0;
		NullCheck(L_36);
		bool L_37 = L_36->get_testButtonsEnabled_9();
		if (!L_37)
		{
			goto IL_0113;
		}
	}
	{
		GameObject_t1756533147 * L_38 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_AddTestButtons_m1954086306(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
	}

IL_0113:
	{
		GameObject_t1756533147 * L_39 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
	}

IL_0119:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_40 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_everyplayInstance_20();
		return L_40;
	}

IL_011f:
	{
		Everyplay_t1799077027 * L_41 = V_2;
		return L_41;
	}
}
// System.Void Everyplay::Initialize()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2582654310;
extern const uint32_t Everyplay_Initialize_m1735770310_MetadataUsageId;
extern "C"  void Everyplay_Initialize_m1735770310 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_Initialize_m1735770310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2582654310, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return;
	}
}
// System.Void Everyplay::Show()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_Show_m681744495_MetadataUsageId;
extern "C"  void Everyplay_Show_m681744495 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_Show_m681744495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayShow_m946367502(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Everyplay::ShowWithPath(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_ShowWithPath_m3146621956_MetadataUsageId;
extern "C"  void Everyplay_ShowWithPath_m3146621956 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_ShowWithPath_m3146621956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_3 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayShowWithPath_m1436735135(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::PlayVideoWithURL(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_PlayVideoWithURL_m504561604_MetadataUsageId;
extern "C"  void Everyplay_PlayVideoWithURL_m504561604 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_PlayVideoWithURL_m504561604_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_3 = ___url0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayPlayVideoWithURL_m1833305119(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::PlayVideoWithDictionary(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_PlayVideoWithDictionary_m2012575572_MetadataUsageId;
extern "C"  void Everyplay_PlayVideoWithDictionary_m2012575572 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dict0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_PlayVideoWithDictionary_m2012575572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		Dictionary_2_t309261261 * L_3 = ___dict0;
		String_t* L_4 = Json_Serialize_m1349440280(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayPlayVideoWithDictionary_m1579715712(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void Everyplay::MakeRequest(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,Everyplay/RequestReadyDelegate,Everyplay/RequestFailedDelegate)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_MakeRequest_m2840565148_MetadataUsageId;
extern "C"  void Everyplay_MakeRequest_m2840565148 (Il2CppObject * __this /* static, unused */, String_t* ___method0, String_t* ___url1, Dictionary_2_t309261261 * ___data2, RequestReadyDelegate_t4013578405 * ___readyDelegate3, RequestFailedDelegate_t3135434785 * ___failedDelegate4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_MakeRequest_m2840565148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_3 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = ___method0;
		String_t* L_5 = ___url1;
		Dictionary_2_t309261261 * L_6 = ___data2;
		RequestReadyDelegate_t4013578405 * L_7 = ___readyDelegate3;
		RequestFailedDelegate_t3135434785 * L_8 = ___failedDelegate4;
		NullCheck(L_3);
		Everyplay_AsyncMakeRequest_m1154701314(L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
// System.String Everyplay::AccessToken()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_AccessToken_m3705224328_MetadataUsageId;
extern "C"  String_t* Everyplay_AccessToken_m3705224328 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_AccessToken_m3705224328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		String_t* L_3 = Everyplay_EveryplayAccountAccessToken_m848432640(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (String_t*)NULL;
	}
}
// System.Void Everyplay::ShowSharingModal()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_ShowSharingModal_m2400588728_MetadataUsageId;
extern "C"  void Everyplay_ShowSharingModal_m2400588728 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_ShowSharingModal_m2400588728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayShowSharingModal_m2705975637(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Everyplay::PlayLastRecording()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_PlayLastRecording_m123861831_MetadataUsageId;
extern "C"  void Everyplay_PlayLastRecording_m123861831 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_PlayLastRecording_m123861831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayPlayLastRecording_m3548469286(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Everyplay::StartRecording()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_StartRecording_m3582706357_MetadataUsageId;
extern "C"  void Everyplay_StartRecording_m3582706357 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_StartRecording_m3582706357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayStartRecording_m9627532(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Everyplay::StopRecording()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_StopRecording_m3761179305_MetadataUsageId;
extern "C"  void Everyplay_StopRecording_m3761179305 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_StopRecording_m3761179305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayStopRecording_m1776955136(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Everyplay::PauseRecording()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_PauseRecording_m3227906851_MetadataUsageId;
extern "C"  void Everyplay_PauseRecording_m3227906851 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_PauseRecording_m3227906851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayPauseRecording_m998977122(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Everyplay::ResumeRecording()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_ResumeRecording_m2915910952_MetadataUsageId;
extern "C"  void Everyplay_ResumeRecording_m2915910952 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_ResumeRecording_m2915910952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayResumeRecording_m248112421(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Boolean Everyplay::IsRecording()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_IsRecording_m568570791_MetadataUsageId;
extern "C"  bool Everyplay_IsRecording_m568570791 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_IsRecording_m568570791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_EveryplayIsRecording_m3853108226(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Boolean Everyplay::IsRecordingSupported()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_IsRecordingSupported_m144924673_MetadataUsageId;
extern "C"  bool Everyplay_IsRecordingSupported_m144924673 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_IsRecordingSupported_m144924673_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_EveryplayIsRecordingSupported_m1388371902(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Boolean Everyplay::IsPaused()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_IsPaused_m3073713592_MetadataUsageId;
extern "C"  bool Everyplay_IsPaused_m3073713592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_IsPaused_m3073713592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_EveryplayIsPaused_m3761251281(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Boolean Everyplay::SnapshotRenderbuffer()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SnapshotRenderbuffer_m3941835888_MetadataUsageId;
extern "C"  bool Everyplay_SnapshotRenderbuffer_m3941835888 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SnapshotRenderbuffer_m3941835888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_EveryplaySnapshotRenderbuffer_m1557380991(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Boolean Everyplay::IsSupported()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_IsSupported_m2107210238_MetadataUsageId;
extern "C"  bool Everyplay_IsSupported_m2107210238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_IsSupported_m2107210238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_EveryplayIsSupported_m2495406309(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Boolean Everyplay::IsSingleCoreDevice()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_IsSingleCoreDevice_m2390517655_MetadataUsageId;
extern "C"  bool Everyplay_IsSingleCoreDevice_m2390517655 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_IsSingleCoreDevice_m2390517655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_EveryplayIsSingleCoreDevice_m2355195836(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Int32 Everyplay::GetUserInterfaceIdiom()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_GetUserInterfaceIdiom_m2004127790_MetadataUsageId;
extern "C"  int32_t Everyplay_GetUserInterfaceIdiom_m2004127790 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_GetUserInterfaceIdiom_m2004127790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		int32_t L_3 = Everyplay_EveryplayGetUserInterfaceIdiom_m298146343(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return 0;
	}
}
// System.Void Everyplay::SetMetadata(System.String,System.Object)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3188644741_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1128763565_MethodInfo_var;
extern const uint32_t Everyplay_SetMetadata_m2665767207_MetadataUsageId;
extern "C"  void Everyplay_SetMetadata_m2665767207 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Il2CppObject * ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetMetadata_m2665767207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_3 = ___key0;
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		Il2CppObject * L_4 = ___val1;
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		Dictionary_2_t309261261 * L_5 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3188644741(L_5, /*hidden argument*/Dictionary_2__ctor_m3188644741_MethodInfo_var);
		V_0 = L_5;
		Dictionary_2_t309261261 * L_6 = V_0;
		String_t* L_7 = ___key0;
		Il2CppObject * L_8 = ___val1;
		NullCheck(L_6);
		Dictionary_2_Add_m1128763565(L_6, L_7, L_8, /*hidden argument*/Dictionary_2_Add_m1128763565_MethodInfo_var);
		Dictionary_2_t309261261 * L_9 = V_0;
		String_t* L_10 = Json_Serialize_m1349440280(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetMetadata_m2926221680(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void Everyplay::SetMetadata(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m1801589293_MethodInfo_var;
extern const uint32_t Everyplay_SetMetadata_m2574869324_MetadataUsageId;
extern "C"  void Everyplay_SetMetadata_m2574869324 (Il2CppObject * __this /* static, unused */, Dictionary_2_t309261261 * ___dict0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetMetadata_m2574869324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		Dictionary_2_t309261261 * L_3 = ___dict0;
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		Dictionary_2_t309261261 * L_4 = ___dict0;
		NullCheck(L_4);
		int32_t L_5 = Dictionary_2_get_Count_m1801589293(L_4, /*hidden argument*/Dictionary_2_get_Count_m1801589293_MethodInfo_var);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		Dictionary_2_t309261261 * L_6 = ___dict0;
		String_t* L_7 = Json_Serialize_m1349440280(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetMetadata_m2926221680(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void Everyplay::SetTargetFPS(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SetTargetFPS_m2278303051_MetadataUsageId;
extern "C"  void Everyplay_SetTargetFPS_m2278303051 (Il2CppObject * __this /* static, unused */, int32_t ___fps0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetTargetFPS_m2278303051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___fps0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetTargetFPS_m410279722(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::SetMotionFactor(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SetMotionFactor_m680416294_MetadataUsageId;
extern "C"  void Everyplay_SetMotionFactor_m680416294 (Il2CppObject * __this /* static, unused */, int32_t ___factor0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetMotionFactor_m680416294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___factor0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetMotionFactor_m1496152793(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::SetAudioResamplerQuality(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SetAudioResamplerQuality_m4023239051_MetadataUsageId;
extern "C"  void Everyplay_SetAudioResamplerQuality_m4023239051 (Il2CppObject * __this /* static, unused */, int32_t ___quality0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetAudioResamplerQuality_m4023239051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001a;
		}
	}

IL_001a:
	{
		return;
	}
}
// System.Void Everyplay::SetMaxRecordingMinutesLength(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SetMaxRecordingMinutesLength_m1251936971_MetadataUsageId;
extern "C"  void Everyplay_SetMaxRecordingMinutesLength_m1251936971 (Il2CppObject * __this /* static, unused */, int32_t ___minutes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetMaxRecordingMinutesLength_m1251936971_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___minutes0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetMaxRecordingMinutesLength_m3939274140(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::SetMaxRecordingSecondsLength(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SetMaxRecordingSecondsLength_m3278065353_MetadataUsageId;
extern "C"  void Everyplay_SetMaxRecordingSecondsLength_m3278065353 (Il2CppObject * __this /* static, unused */, int32_t ___seconds0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetMaxRecordingSecondsLength_m3278065353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___seconds0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetMaxRecordingSecondsLength_m1274558896(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::SetLowMemoryDevice(System.Boolean)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SetLowMemoryDevice_m2440438802_MetadataUsageId;
extern "C"  void Everyplay_SetLowMemoryDevice_m2440438802 (Il2CppObject * __this /* static, unused */, bool ___state0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetLowMemoryDevice_m2440438802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		bool L_3 = ___state0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetLowMemoryDevice_m155866533(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::SetDisableSingleCoreDevices(System.Boolean)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SetDisableSingleCoreDevices_m3853165939_MetadataUsageId;
extern "C"  void Everyplay_SetDisableSingleCoreDevices_m3853165939 (Il2CppObject * __this /* static, unused */, bool ___state0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetDisableSingleCoreDevices_m3853165939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		bool L_3 = ___state0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetDisableSingleCoreDevices_m1396293428(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Boolean Everyplay::FaceCamIsVideoRecordingSupported()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamIsVideoRecordingSupported_m299330274_MetadataUsageId;
extern "C"  bool Everyplay_FaceCamIsVideoRecordingSupported_m299330274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamIsVideoRecordingSupported_m299330274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_EveryplayFaceCamIsVideoRecordingSupported_m1933775331(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Boolean Everyplay::FaceCamIsAudioRecordingSupported()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamIsAudioRecordingSupported_m818624605_MetadataUsageId;
extern "C"  bool Everyplay_FaceCamIsAudioRecordingSupported_m818624605 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamIsAudioRecordingSupported_m818624605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_EveryplayFaceCamIsAudioRecordingSupported_m622408608(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Boolean Everyplay::FaceCamIsHeadphonesPluggedIn()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamIsHeadphonesPluggedIn_m1073951028_MetadataUsageId;
extern "C"  bool Everyplay_FaceCamIsHeadphonesPluggedIn_m1073951028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamIsHeadphonesPluggedIn_m1073951028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_EveryplayFaceCamIsHeadphonesPluggedIn_m2030515277(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Boolean Everyplay::FaceCamIsSessionRunning()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamIsSessionRunning_m3953733397_MetadataUsageId;
extern "C"  bool Everyplay_FaceCamIsSessionRunning_m3953733397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamIsSessionRunning_m3953733397_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_EveryplayFaceCamIsSessionRunning_m3581958402(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Boolean Everyplay::FaceCamIsRecordingPermissionGranted()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamIsRecordingPermissionGranted_m3102847209_MetadataUsageId;
extern "C"  bool Everyplay_FaceCamIsRecordingPermissionGranted_m3102847209 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamIsRecordingPermissionGranted_m3102847209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_EveryplayFaceCamIsRecordingPermissionGranted_m3961460134(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Single Everyplay::FaceCamAudioPeakLevel()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamAudioPeakLevel_m1600399543_MetadataUsageId;
extern "C"  float Everyplay_FaceCamAudioPeakLevel_m1600399543 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamAudioPeakLevel_m1600399543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		float L_3 = Everyplay_EveryplayFaceCamAudioPeakLevel_m199553448(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (0.0f);
	}
}
// System.Single Everyplay::FaceCamAudioPowerLevel()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamAudioPowerLevel_m3465111755_MetadataUsageId;
extern "C"  float Everyplay_FaceCamAudioPowerLevel_m3465111755 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamAudioPowerLevel_m3465111755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		float L_3 = Everyplay_EveryplayFaceCamAudioPowerLevel_m3853690490(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0020:
	{
		return (0.0f);
	}
}
// System.Void Everyplay::FaceCamSetMonitorAudioLevels(System.Boolean)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetMonitorAudioLevels_m156233898_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetMonitorAudioLevels_m156233898 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetMonitorAudioLevels_m156233898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		bool L_3 = ___enabled0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetMonitorAudioLevels_m127210877(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetRecordingMode(Everyplay/FaceCamRecordingMode)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetRecordingMode_m2388711916_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetRecordingMode_m2388711916 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetRecordingMode_m2388711916_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___mode0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetRecordingMode_m1780428062(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetAudioOnly(System.Boolean)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetAudioOnly_m4118761357_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetAudioOnly_m4118761357 (Il2CppObject * __this /* static, unused */, bool ___audioOnly0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetAudioOnly_m4118761357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		bool L_3 = ___audioOnly0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetAudioOnly_m3301962502(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetPreviewVisible(System.Boolean)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetPreviewVisible_m3505686335_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetPreviewVisible_m3505686335 (Il2CppObject * __this /* static, unused */, bool ___visible0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetPreviewVisible_m3505686335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		bool L_3 = ___visible0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetPreviewVisible_m275566432(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetPreviewScaleRetina(System.Boolean)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetPreviewScaleRetina_m564042752_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetPreviewScaleRetina_m564042752 (Il2CppObject * __this /* static, unused */, bool ___autoScale0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetPreviewScaleRetina_m564042752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		bool L_3 = ___autoScale0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetPreviewScaleRetina_m2186195837(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetPreviewSideWidth(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetPreviewSideWidth_m2582819180_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetPreviewSideWidth_m2582819180 (Il2CppObject * __this /* static, unused */, int32_t ___width0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetPreviewSideWidth_m2582819180_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___width0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetPreviewSideWidth_m2664091223(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetPreviewBorderWidth(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetPreviewBorderWidth_m4144496013_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetPreviewBorderWidth_m4144496013 (Il2CppObject * __this /* static, unused */, int32_t ___width0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetPreviewBorderWidth_m4144496013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___width0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetPreviewBorderWidth_m3607310900(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetPreviewPositionX(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetPreviewPositionX_m1625010144_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetPreviewPositionX_m1625010144 (Il2CppObject * __this /* static, unused */, int32_t ___x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetPreviewPositionX_m1625010144_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetPreviewPositionX_m2443346075(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetPreviewPositionY(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetPreviewPositionY_m906599039_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetPreviewPositionY_m906599039 (Il2CppObject * __this /* static, unused */, int32_t ___y0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetPreviewPositionY_m906599039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___y0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetPreviewPositionY_m3939587552(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetPreviewBorderColor(System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetPreviewBorderColor_m2502318011_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetPreviewBorderColor_m2502318011 (Il2CppObject * __this /* static, unused */, float ___r0, float ___g1, float ___b2, float ___a3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetPreviewBorderColor_m2502318011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		float L_3 = ___r0;
		float L_4 = ___g1;
		float L_5 = ___b2;
		float L_6 = ___a3;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetPreviewBorderColor_m3525118556(NULL /*static, unused*/, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetPreviewOrigin(Everyplay/FaceCamPreviewOrigin)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetPreviewOrigin_m1150375596_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetPreviewOrigin_m1150375596 (Il2CppObject * __this /* static, unused */, int32_t ___origin0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetPreviewOrigin_m1150375596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___origin0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetPreviewOrigin_m397951628(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetTargetTexture(UnityEngine.Texture2D)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetTargetTexture_m2609324012_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetTargetTexture_m2609324012 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetTargetTexture_m2609324012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0056;
		}
	}
	{
		Texture2D_t3542995729 * L_3 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004c;
		}
	}
	{
		Texture2D_t3542995729 * L_5 = ___texture0;
		NullCheck(L_5);
		IntPtr_t L_6 = Texture_GetNativeTexturePtr_m292373493(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetTargetTexture_m3120082225(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_7 = ___texture0;
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_7);
		Everyplay_EveryplayFaceCamSetTargetTextureWidth_m539522378(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_9 = ___texture0;
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_9);
		Everyplay_EveryplayFaceCamSetTargetTextureHeight_m450171829(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_004c:
	{
		IntPtr_t L_11 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetTargetTexture_m3120082225(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetTargetTextureId(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetTargetTextureId_m3672435410_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetTargetTextureId_m3672435410 (Il2CppObject * __this /* static, unused */, int32_t ___textureId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetTargetTextureId_m3672435410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___textureId0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetTargetTextureId_m614786215(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetTargetTextureWidth(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetTargetTextureWidth_m3178038257_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetTargetTextureWidth_m3178038257 (Il2CppObject * __this /* static, unused */, int32_t ___textureWidth0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetTargetTextureWidth_m3178038257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___textureWidth0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetTargetTextureWidth_m539522378(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamSetTargetTextureHeight(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamSetTargetTextureHeight_m1571811864_MetadataUsageId;
extern "C"  void Everyplay_FaceCamSetTargetTextureHeight_m1571811864 (Il2CppObject * __this /* static, unused */, int32_t ___textureHeight0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamSetTargetTextureHeight_m1571811864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___textureHeight0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamSetTargetTextureHeight_m450171829(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamStartSession()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamStartSession_m2600985758_MetadataUsageId;
extern "C"  void Everyplay_FaceCamStartSession_m2600985758 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamStartSession_m2600985758_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamStartSession_m563630289(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamRequestRecordingPermission()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamRequestRecordingPermission_m3770143701_MetadataUsageId;
extern "C"  void Everyplay_FaceCamRequestRecordingPermission_m3770143701 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamRequestRecordingPermission_m3770143701_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamRequestRecordingPermission_m3269278668(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Everyplay::FaceCamStopSession()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_FaceCamStopSession_m1826517360_MetadataUsageId;
extern "C"  void Everyplay_FaceCamStopSession_m1826517360 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_FaceCamStopSession_m1826517360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayFaceCamStopSession_m2774136573(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Everyplay::SetThumbnailTargetTexture(UnityEngine.Texture2D)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SetThumbnailTargetTexture_m2121221160_MetadataUsageId;
extern "C"  void Everyplay_SetThumbnailTargetTexture_m2121221160 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetThumbnailTargetTexture_m2121221160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0068;
		}
	}
	{
		Texture2D_t3542995729 * L_3 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_currentThumbnailTargetTexture_21(L_3);
		Texture2D_t3542995729 * L_4 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_6 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_currentThumbnailTargetTexture_21();
		NullCheck(L_6);
		IntPtr_t L_7 = Texture_GetNativeTexturePtr_m292373493(L_6, /*hidden argument*/NULL);
		Everyplay_EveryplaySetThumbnailTargetTexture_m1799038923(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_8 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_currentThumbnailTargetTexture_21();
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_8);
		Everyplay_EveryplaySetThumbnailTargetTextureWidth_m379731554(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_10 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_currentThumbnailTargetTexture_21();
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_10);
		Everyplay_EveryplaySetThumbnailTargetTextureHeight_m3729298403(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		goto IL_0068;
	}

IL_005e:
	{
		IntPtr_t L_12 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetThumbnailTargetTexture_m1799038923(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void Everyplay::SetThumbnailTargetTextureId(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SetThumbnailTargetTextureId_m1453661614_MetadataUsageId;
extern "C"  void Everyplay_SetThumbnailTargetTextureId_m1453661614 (Il2CppObject * __this /* static, unused */, int32_t ___textureId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetThumbnailTargetTextureId_m1453661614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___textureId0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetThumbnailTargetTextureId_m3960249425(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::SetThumbnailTargetTextureWidth(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SetThumbnailTargetTextureWidth_m1053357747_MetadataUsageId;
extern "C"  void Everyplay_SetThumbnailTargetTextureWidth_m1053357747 (Il2CppObject * __this /* static, unused */, int32_t ___textureWidth0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetThumbnailTargetTextureWidth_m1053357747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___textureWidth0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetThumbnailTargetTextureWidth_m379731554(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::SetThumbnailTargetTextureHeight(System.Int32)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_SetThumbnailTargetTextureHeight_m1830783752_MetadataUsageId;
extern "C"  void Everyplay_SetThumbnailTargetTextureHeight_m1830783752 (Il2CppObject * __this /* static, unused */, int32_t ___textureHeight0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_SetThumbnailTargetTextureHeight_m1830783752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_3 = ___textureHeight0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplaySetThumbnailTargetTextureHeight_m3729298403(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Everyplay::TakeThumbnail()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_TakeThumbnail_m1670072747_MetadataUsageId;
extern "C"  void Everyplay_TakeThumbnail_m1670072747 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_TakeThumbnail_m1670072747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_EveryplayTakeThumbnail_m1202186698(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Boolean Everyplay::IsReadyForRecording()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_IsReadyForRecording_m4208154795_MetadataUsageId;
extern "C"  bool Everyplay_IsReadyForRecording_m4208154795 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_IsReadyForRecording_m4208154795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_t1799077027 * L_0 = Everyplay_get_EveryplayInstance_m3310382687(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_hasMethods_16();
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_readyForRecording_18();
		return L_3;
	}

IL_0020:
	{
		return (bool)0;
	}
}
// System.Void Everyplay::RemoveAllEventHandlers()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_RemoveAllEventHandlers_m3444867980_MetadataUsageId;
extern "C"  void Everyplay_RemoveAllEventHandlers_m3444867980 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_RemoveAllEventHandlers_m3444867980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_WasClosed_2((WasClosedDelegate_t946300984 *)NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_ReadyForRecording_3((ReadyForRecordingDelegate_t1593758596 *)NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_RecordingStarted_4((RecordingStartedDelegate_t5060419 *)NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_RecordingStopped_5((RecordingStoppedDelegate_t3008025639 *)NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_FaceCamSessionStarted_6((FaceCamSessionStartedDelegate_t1733547424 *)NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_FaceCamRecordingPermission_7((FaceCamRecordingPermissionDelegate_t1670731619 *)NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_FaceCamSessionStopped_8((FaceCamSessionStoppedDelegate_t1894731428 *)NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_ThumbnailReadyAtTextureId_9((ThumbnailReadyAtTextureIdDelegate_t3853410271 *)NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_ThumbnailTextureReady_10((ThumbnailTextureReadyDelegate_t2948235259 *)NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_UploadDidStart_11((UploadDidStartDelegate_t1871027361 *)NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_UploadDidProgress_12((UploadDidProgressDelegate_t2069570344 *)NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_UploadDidComplete_13((UploadDidCompleteDelegate_t1564565876 *)NULL);
		return;
	}
}
// System.Void Everyplay::Reset()
extern "C"  void Everyplay_Reset_m1986807329 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Everyplay::AddTestButtons(UnityEngine.GameObject)
extern const Il2CppType* Texture2D_t3542995729_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisEveryplayRecButtons_t2003903990_m4110002744_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4131885322;
extern const uint32_t Everyplay_AddTestButtons_m1954086306_MetadataUsageId;
extern "C"  void Everyplay_AddTestButtons_m1954086306 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_AddTestButtons_m1954086306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_t3542995729 * V_0 = NULL;
	EveryplayRecButtons_t2003903990 * V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Texture2D_t3542995729_0_0_0_var), /*hidden argument*/NULL);
		Object_t1021602117 * L_1 = Resources_Load_m243305716(NULL /*static, unused*/, _stringLiteral4131885322, L_0, /*hidden argument*/NULL);
		V_0 = ((Texture2D_t3542995729 *)CastclassSealed(L_1, Texture2D_t3542995729_il2cpp_TypeInfo_var));
		Texture2D_t3542995729 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0040;
		}
	}
	{
		GameObject_t1756533147 * L_4 = ___gameObject0;
		NullCheck(L_4);
		EveryplayRecButtons_t2003903990 * L_5 = GameObject_AddComponent_TisEveryplayRecButtons_t2003903990_m4110002744(L_4, /*hidden argument*/GameObject_AddComponent_TisEveryplayRecButtons_t2003903990_m4110002744_MethodInfo_var);
		V_1 = L_5;
		EveryplayRecButtons_t2003903990 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0040;
		}
	}
	{
		EveryplayRecButtons_t2003903990 * L_8 = V_1;
		Texture2D_t3542995729 * L_9 = V_0;
		NullCheck(L_8);
		L_8->set_atlasTexture_2(L_9);
	}

IL_0040:
	{
		return;
	}
}
// System.Void Everyplay::AsyncMakeRequest(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,Everyplay/RequestReadyDelegate,Everyplay/RequestFailedDelegate)
extern "C"  void Everyplay_AsyncMakeRequest_m1154701314 (Everyplay_t1799077027 * __this, String_t* ___method0, String_t* ___url1, Dictionary_2_t309261261 * ___data2, RequestReadyDelegate_t4013578405 * ___readyDelegate3, RequestFailedDelegate_t3135434785 * ___failedDelegate4, const MethodInfo* method)
{
	{
		String_t* L_0 = ___method0;
		String_t* L_1 = ___url1;
		Dictionary_2_t309261261 * L_2 = ___data2;
		RequestReadyDelegate_t4013578405 * L_3 = ___readyDelegate3;
		RequestFailedDelegate_t3135434785 * L_4 = ___failedDelegate4;
		Il2CppObject * L_5 = Everyplay_MakeRequestEnumerator_m1853830506(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Everyplay::MakeRequestEnumerator(System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>,Everyplay/RequestReadyDelegate,Everyplay/RequestFailedDelegate)
extern Il2CppClass* U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_MakeRequestEnumerator_m1853830506_MetadataUsageId;
extern "C"  Il2CppObject * Everyplay_MakeRequestEnumerator_m1853830506 (Everyplay_t1799077027 * __this, String_t* ___method0, String_t* ___url1, Dictionary_2_t309261261 * ___data2, RequestReadyDelegate_t4013578405 * ___readyDelegate3, RequestFailedDelegate_t3135434785 * ___failedDelegate4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_MakeRequestEnumerator_m1853830506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * V_0 = NULL;
	{
		U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * L_0 = (U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 *)il2cpp_codegen_object_new(U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924_il2cpp_TypeInfo_var);
		U3CMakeRequestEnumeratorU3Ec__Iterator0__ctor_m1954205247(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * L_1 = V_0;
		Dictionary_2_t309261261 * L_2 = ___data2;
		NullCheck(L_1);
		L_1->set_data_0(L_2);
		U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * L_3 = V_0;
		String_t* L_4 = ___url1;
		NullCheck(L_3);
		L_3->set_url_1(L_4);
		U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * L_5 = V_0;
		String_t* L_6 = ___method0;
		NullCheck(L_5);
		L_5->set_method_2(L_6);
		U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * L_7 = V_0;
		RequestFailedDelegate_t3135434785 * L_8 = ___failedDelegate4;
		NullCheck(L_7);
		L_7->set_failedDelegate_8(L_8);
		U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * L_9 = V_0;
		RequestReadyDelegate_t4013578405 * L_10 = ___readyDelegate3;
		NullCheck(L_9);
		L_9->set_readyDelegate_9(L_10);
		U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * L_11 = V_0;
		return L_11;
	}
}
// System.Void Everyplay::OnApplicationQuit()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_OnApplicationQuit_m2101183012_MetadataUsageId;
extern "C"  void Everyplay_OnApplicationQuit_m2101183012 (Everyplay_t1799077027 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_OnApplicationQuit_m2101183012_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_Reset_m1986807329(NULL /*static, unused*/, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_currentThumbnailTargetTexture_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_SetThumbnailTargetTexture_m2121221160(NULL /*static, unused*/, (Texture2D_t3542995729 *)NULL, /*hidden argument*/NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_currentThumbnailTargetTexture_21((Texture2D_t3542995729 *)NULL);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_RemoveAllEventHandlers_m3444867980(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_appIsClosing_15((bool)1);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_everyplayInstance_20((Everyplay_t1799077027 *)NULL);
		return;
	}
}
// System.Void Everyplay::EveryplayHidden(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_EveryplayHidden_m1197865333_MetadataUsageId;
extern "C"  void Everyplay_EveryplayHidden_m1197865333 (Everyplay_t1799077027 * __this, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayHidden_m1197865333_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		WasClosedDelegate_t946300984 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_WasClosed_2();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		WasClosedDelegate_t946300984 * L_1 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_WasClosed_2();
		NullCheck(L_1);
		WasClosedDelegate_Invoke_m2969330763(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Everyplay::EveryplayReadyForRecording(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral633313253;
extern const uint32_t Everyplay_EveryplayReadyForRecording_m3456378644_MetadataUsageId;
extern "C"  void Everyplay_EveryplayReadyForRecording_m3456378644 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayReadyForRecording_m3456378644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	bool V_1 = false;
	{
		String_t* L_0 = ___jsonMsg0;
		Dictionary_2_t309261261 * L_1 = EveryplayDictionaryExtensions_JsonToDictionary_m1861006415(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Dictionary_2_t309261261 * L_2 = V_0;
		bool L_3 = EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406(NULL /*static, unused*/, L_2, _stringLiteral633313253, (&V_1), /*hidden argument*/EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406_MethodInfo_var);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		bool L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_readyForRecording_18(L_4);
		ReadyForRecordingDelegate_t1593758596 * L_5 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ReadyForRecording_3();
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ReadyForRecordingDelegate_t1593758596 * L_6 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ReadyForRecording_3();
		bool L_7 = V_1;
		NullCheck(L_6);
		ReadyForRecordingDelegate_Invoke_m1966730172(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void Everyplay::EveryplayRecordingStarted(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_EveryplayRecordingStarted_m3414975027_MetadataUsageId;
extern "C"  void Everyplay_EveryplayRecordingStarted_m3414975027 (Everyplay_t1799077027 * __this, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayRecordingStarted_m3414975027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStartedDelegate_t5060419 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_RecordingStarted_4();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStartedDelegate_t5060419 * L_1 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_RecordingStarted_4();
		NullCheck(L_1);
		RecordingStartedDelegate_Invoke_m3318412280(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Everyplay::EveryplayRecordingStopped(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_EveryplayRecordingStopped_m3585386003_MetadataUsageId;
extern "C"  void Everyplay_EveryplayRecordingStopped_m3585386003 (Everyplay_t1799077027 * __this, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayRecordingStopped_m3585386003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStoppedDelegate_t3008025639 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_RecordingStopped_5();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		RecordingStoppedDelegate_t3008025639 * L_1 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_RecordingStopped_5();
		NullCheck(L_1);
		RecordingStoppedDelegate_Invoke_m2827616860(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Everyplay::EveryplayFaceCamSessionStarted(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_EveryplayFaceCamSessionStarted_m1477758232_MetadataUsageId;
extern "C"  void Everyplay_EveryplayFaceCamSessionStarted_m1477758232 (Everyplay_t1799077027 * __this, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayFaceCamSessionStarted_m1477758232_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStartedDelegate_t1733547424 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamSessionStarted_6();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStartedDelegate_t1733547424 * L_1 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamSessionStarted_6();
		NullCheck(L_1);
		FaceCamSessionStartedDelegate_Invoke_m1284990449(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Everyplay::EveryplayFaceCamRecordingPermission(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1263950463;
extern const uint32_t Everyplay_EveryplayFaceCamRecordingPermission_m3565058039_MetadataUsageId;
extern "C"  void Everyplay_EveryplayFaceCamRecordingPermission_m3565058039 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayFaceCamRecordingPermission_m3565058039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamRecordingPermission_7();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_1 = ___jsonMsg0;
		Dictionary_2_t309261261 * L_2 = EveryplayDictionaryExtensions_JsonToDictionary_m1861006415(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Dictionary_2_t309261261 * L_3 = V_0;
		bool L_4 = EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406(NULL /*static, unused*/, L_3, _stringLiteral1263950463, (&V_1), /*hidden argument*/EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406_MethodInfo_var);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_5 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamRecordingPermission_7();
		bool L_6 = V_1;
		NullCheck(L_5);
		FaceCamRecordingPermissionDelegate_Invoke_m1744694023(L_5, L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void Everyplay::EveryplayFaceCamSessionStopped(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay_EveryplayFaceCamSessionStopped_m3917869784_MetadataUsageId;
extern "C"  void Everyplay_EveryplayFaceCamSessionStopped_m3917869784 (Everyplay_t1799077027 * __this, String_t* ___msg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayFaceCamSessionStopped_m3917869784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStoppedDelegate_t1894731428 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamSessionStopped_8();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		FaceCamSessionStoppedDelegate_t1894731428 * L_1 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_FaceCamSessionStopped_8();
		NullCheck(L_1);
		FaceCamSessionStoppedDelegate_Invoke_m3720472693(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void Everyplay::EveryplayThumbnailReadyAtTextureId(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134_MethodInfo_var;
extern const MethodInfo* EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3735046248;
extern Il2CppCodeGenString* _stringLiteral1352397043;
extern const uint32_t Everyplay_EveryplayThumbnailReadyAtTextureId_m4253971571_MetadataUsageId;
extern "C"  void Everyplay_EveryplayThumbnailReadyAtTextureId_m4253971571 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayThumbnailReadyAtTextureId_m4253971571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailReadyAtTextureId_9();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailTextureReadyDelegate_t2948235259 * L_1 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailTextureReady_10();
		if (!L_1)
		{
			goto IL_008f;
		}
	}

IL_0014:
	{
		String_t* L_2 = ___jsonMsg0;
		Dictionary_2_t309261261 * L_3 = EveryplayDictionaryExtensions_JsonToDictionary_m1861006415(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Dictionary_2_t309261261 * L_4 = V_0;
		bool L_5 = EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134(NULL /*static, unused*/, L_4, _stringLiteral3735046248, (&V_1), /*hidden argument*/EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134_MethodInfo_var);
		if (!L_5)
		{
			goto IL_008f;
		}
	}
	{
		Dictionary_2_t309261261 * L_6 = V_0;
		bool L_7 = EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406(NULL /*static, unused*/, L_6, _stringLiteral1352397043, (&V_2), /*hidden argument*/EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406_MethodInfo_var);
		if (!L_7)
		{
			goto IL_008f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_8 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailReadyAtTextureId_9();
		if (!L_8)
		{
			goto IL_0055;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailReadyAtTextureIdDelegate_t3853410271 * L_9 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailReadyAtTextureId_9();
		int32_t L_10 = V_1;
		bool L_11 = V_2;
		NullCheck(L_9);
		ThumbnailReadyAtTextureIdDelegate_Invoke_m419038684(L_9, L_10, L_11, /*hidden argument*/NULL);
	}

IL_0055:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailTextureReadyDelegate_t2948235259 * L_12 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailTextureReady_10();
		if (!L_12)
		{
			goto IL_008f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_13 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_currentThumbnailTargetTexture_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_13, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_008f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_15 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_currentThumbnailTargetTexture_21();
		NullCheck(L_15);
		int32_t L_16 = Texture_GetNativeTextureID_m1317239321(L_15, /*hidden argument*/NULL);
		int32_t L_17 = V_1;
		if ((!(((uint32_t)L_16) == ((uint32_t)L_17))))
		{
			goto IL_008f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailTextureReadyDelegate_t2948235259 * L_18 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailTextureReady_10();
		Texture2D_t3542995729 * L_19 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_currentThumbnailTargetTexture_21();
		bool L_20 = V_2;
		NullCheck(L_18);
		ThumbnailTextureReadyDelegate_Invoke_m3492481785(L_18, L_19, L_20, /*hidden argument*/NULL);
	}

IL_008f:
	{
		return;
	}
}
// System.Void Everyplay::EveryplayThumbnailTextureReady(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayDictionaryExtensions_TryGetValue_TisInt64_t909078037_m16980239_MethodInfo_var;
extern const MethodInfo* EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral36253345;
extern Il2CppCodeGenString* _stringLiteral1352397043;
extern const uint32_t Everyplay_EveryplayThumbnailTextureReady_m3447023487_MetadataUsageId;
extern "C"  void Everyplay_EveryplayThumbnailTextureReady_m3447023487 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayThumbnailTextureReady_m3447023487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	int64_t V_1 = 0;
	bool V_2 = false;
	int64_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailTextureReadyDelegate_t2948235259 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailTextureReady_10();
		if (!L_0)
		{
			goto IL_006c;
		}
	}
	{
		String_t* L_1 = ___jsonMsg0;
		Dictionary_2_t309261261 * L_2 = EveryplayDictionaryExtensions_JsonToDictionary_m1861006415(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_3 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_currentThumbnailTargetTexture_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006c;
		}
	}
	{
		Dictionary_2_t309261261 * L_5 = V_0;
		bool L_6 = EveryplayDictionaryExtensions_TryGetValue_TisInt64_t909078037_m16980239(NULL /*static, unused*/, L_5, _stringLiteral36253345, (&V_1), /*hidden argument*/EveryplayDictionaryExtensions_TryGetValue_TisInt64_t909078037_m16980239_MethodInfo_var);
		if (!L_6)
		{
			goto IL_006c;
		}
	}
	{
		Dictionary_2_t309261261 * L_7 = V_0;
		bool L_8 = EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406(NULL /*static, unused*/, L_7, _stringLiteral1352397043, (&V_2), /*hidden argument*/EveryplayDictionaryExtensions_TryGetValue_TisBoolean_t3825574718_m1856492406_MethodInfo_var);
		if (!L_8)
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_9 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_currentThumbnailTargetTexture_21();
		NullCheck(L_9);
		IntPtr_t L_10 = Texture_GetNativeTexturePtr_m292373493(L_9, /*hidden argument*/NULL);
		int64_t L_11 = IntPtr_op_Explicit_m4212329003(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		int64_t L_12 = V_3;
		int64_t L_13 = V_1;
		if ((!(((uint64_t)L_12) == ((uint64_t)L_13))))
		{
			goto IL_006c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		ThumbnailTextureReadyDelegate_t2948235259 * L_14 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_ThumbnailTextureReady_10();
		Texture2D_t3542995729 * L_15 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_currentThumbnailTargetTexture_21();
		bool L_16 = V_2;
		NullCheck(L_14);
		ThumbnailTextureReadyDelegate_Invoke_m3492481785(L_14, L_15, L_16, /*hidden argument*/NULL);
	}

IL_006c:
	{
		return;
	}
}
// System.Void Everyplay::EveryplayUploadDidStart(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1668745788;
extern const uint32_t Everyplay_EveryplayUploadDidStart_m2462853237_MetadataUsageId;
extern "C"  void Everyplay_EveryplayUploadDidStart_m2462853237 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayUploadDidStart_m2462853237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidStartDelegate_t1871027361 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidStart_11();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_1 = ___jsonMsg0;
		Dictionary_2_t309261261 * L_2 = EveryplayDictionaryExtensions_JsonToDictionary_m1861006415(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Dictionary_2_t309261261 * L_3 = V_0;
		bool L_4 = EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134(NULL /*static, unused*/, L_3, _stringLiteral1668745788, (&V_1), /*hidden argument*/EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134_MethodInfo_var);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidStartDelegate_t1871027361 * L_5 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidStart_11();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		UploadDidStartDelegate_Invoke_m3051571839(L_5, L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// System.Void Everyplay::EveryplayUploadDidProgress(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134_MethodInfo_var;
extern const MethodInfo* EveryplayDictionaryExtensions_TryGetValue_TisDouble_t4078015681_m3826697939_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1668745788;
extern Il2CppCodeGenString* _stringLiteral1836469849;
extern const uint32_t Everyplay_EveryplayUploadDidProgress_m488542120_MetadataUsageId;
extern "C"  void Everyplay_EveryplayUploadDidProgress_m488542120 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayUploadDidProgress_m488542120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	int32_t V_1 = 0;
	double V_2 = 0.0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidProgressDelegate_t2069570344 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidProgress_12();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_1 = ___jsonMsg0;
		Dictionary_2_t309261261 * L_2 = EveryplayDictionaryExtensions_JsonToDictionary_m1861006415(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Dictionary_2_t309261261 * L_3 = V_0;
		bool L_4 = EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134(NULL /*static, unused*/, L_3, _stringLiteral1668745788, (&V_1), /*hidden argument*/EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		Dictionary_2_t309261261 * L_5 = V_0;
		bool L_6 = EveryplayDictionaryExtensions_TryGetValue_TisDouble_t4078015681_m3826697939(NULL /*static, unused*/, L_5, _stringLiteral1836469849, (&V_2), /*hidden argument*/EveryplayDictionaryExtensions_TryGetValue_TisDouble_t4078015681_m3826697939_MethodInfo_var);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidProgressDelegate_t2069570344 * L_7 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidProgress_12();
		int32_t L_8 = V_1;
		double L_9 = V_2;
		NullCheck(L_7);
		UploadDidProgressDelegate_Invoke_m511099547(L_7, L_8, (((float)((float)L_9))), /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void Everyplay::EveryplayUploadDidComplete(System.String)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1668745788;
extern const uint32_t Everyplay_EveryplayUploadDidComplete_m580978396_MetadataUsageId;
extern "C"  void Everyplay_EveryplayUploadDidComplete_m580978396 (Everyplay_t1799077027 * __this, String_t* ___jsonMsg0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay_EveryplayUploadDidComplete_m580978396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidCompleteDelegate_t1564565876 * L_0 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidComplete_13();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_1 = ___jsonMsg0;
		Dictionary_2_t309261261 * L_2 = EveryplayDictionaryExtensions_JsonToDictionary_m1861006415(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Dictionary_2_t309261261 * L_3 = V_0;
		bool L_4 = EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134(NULL /*static, unused*/, L_3, _stringLiteral1668745788, (&V_1), /*hidden argument*/EveryplayDictionaryExtensions_TryGetValue_TisInt32_t2071877448_m1431455134_MethodInfo_var);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		UploadDidCompleteDelegate_t1564565876 * L_5 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_UploadDidComplete_13();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		UploadDidCompleteDelegate_Invoke_m3224334404(L_5, L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL InitEveryplay(char*, char*, char*, char*);
// System.Void Everyplay::InitEveryplay(System.String,System.String,System.String,System.String)
extern "C"  void Everyplay_InitEveryplay_m927127947 (Il2CppObject * __this /* static, unused */, String_t* ___clientId0, String_t* ___clientSecret1, String_t* ___redirectURI2, String_t* ___gameObjectName3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, char*);

	// Marshaling of parameter '___clientId0' to native representation
	char* ____clientId0_marshaled = NULL;
	____clientId0_marshaled = il2cpp_codegen_marshal_string(___clientId0);

	// Marshaling of parameter '___clientSecret1' to native representation
	char* ____clientSecret1_marshaled = NULL;
	____clientSecret1_marshaled = il2cpp_codegen_marshal_string(___clientSecret1);

	// Marshaling of parameter '___redirectURI2' to native representation
	char* ____redirectURI2_marshaled = NULL;
	____redirectURI2_marshaled = il2cpp_codegen_marshal_string(___redirectURI2);

	// Marshaling of parameter '___gameObjectName3' to native representation
	char* ____gameObjectName3_marshaled = NULL;
	____gameObjectName3_marshaled = il2cpp_codegen_marshal_string(___gameObjectName3);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(InitEveryplay)(____clientId0_marshaled, ____clientSecret1_marshaled, ____redirectURI2_marshaled, ____gameObjectName3_marshaled);

	// Marshaling cleanup of parameter '___clientId0' native representation
	il2cpp_codegen_marshal_free(____clientId0_marshaled);
	____clientId0_marshaled = NULL;

	// Marshaling cleanup of parameter '___clientSecret1' native representation
	il2cpp_codegen_marshal_free(____clientSecret1_marshaled);
	____clientSecret1_marshaled = NULL;

	// Marshaling cleanup of parameter '___redirectURI2' native representation
	il2cpp_codegen_marshal_free(____redirectURI2_marshaled);
	____redirectURI2_marshaled = NULL;

	// Marshaling cleanup of parameter '___gameObjectName3' native representation
	il2cpp_codegen_marshal_free(____gameObjectName3_marshaled);
	____gameObjectName3_marshaled = NULL;

}
extern "C" void DEFAULT_CALL EveryplayShow();
// System.Void Everyplay::EveryplayShow()
extern "C"  void Everyplay_EveryplayShow_m946367502 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayShow)();

}
extern "C" void DEFAULT_CALL EveryplayShowWithPath(char*);
// System.Void Everyplay::EveryplayShowWithPath(System.String)
extern "C"  void Everyplay_EveryplayShowWithPath_m1436735135 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayShowWithPath)(____path0_marshaled);

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL EveryplayPlayVideoWithURL(char*);
// System.Void Everyplay::EveryplayPlayVideoWithURL(System.String)
extern "C"  void Everyplay_EveryplayPlayVideoWithURL_m1833305119 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___url0' to native representation
	char* ____url0_marshaled = NULL;
	____url0_marshaled = il2cpp_codegen_marshal_string(___url0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayPlayVideoWithURL)(____url0_marshaled);

	// Marshaling cleanup of parameter '___url0' native representation
	il2cpp_codegen_marshal_free(____url0_marshaled);
	____url0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL EveryplayPlayVideoWithDictionary(char*);
// System.Void Everyplay::EveryplayPlayVideoWithDictionary(System.String)
extern "C"  void Everyplay_EveryplayPlayVideoWithDictionary_m1579715712 (Il2CppObject * __this /* static, unused */, String_t* ___dic0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___dic0' to native representation
	char* ____dic0_marshaled = NULL;
	____dic0_marshaled = il2cpp_codegen_marshal_string(___dic0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayPlayVideoWithDictionary)(____dic0_marshaled);

	// Marshaling cleanup of parameter '___dic0' native representation
	il2cpp_codegen_marshal_free(____dic0_marshaled);
	____dic0_marshaled = NULL;

}
extern "C" char* DEFAULT_CALL EveryplayAccountAccessToken();
// System.String Everyplay::EveryplayAccountAccessToken()
extern "C"  String_t* Everyplay_EveryplayAccountAccessToken_m848432640 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(EveryplayAccountAccessToken)();

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL EveryplayShowSharingModal();
// System.Void Everyplay::EveryplayShowSharingModal()
extern "C"  void Everyplay_EveryplayShowSharingModal_m2705975637 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayShowSharingModal)();

}
extern "C" void DEFAULT_CALL EveryplayPlayLastRecording();
// System.Void Everyplay::EveryplayPlayLastRecording()
extern "C"  void Everyplay_EveryplayPlayLastRecording_m3548469286 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayPlayLastRecording)();

}
extern "C" void DEFAULT_CALL EveryplayStartRecording();
// System.Void Everyplay::EveryplayStartRecording()
extern "C"  void Everyplay_EveryplayStartRecording_m9627532 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayStartRecording)();

}
extern "C" void DEFAULT_CALL EveryplayStopRecording();
// System.Void Everyplay::EveryplayStopRecording()
extern "C"  void Everyplay_EveryplayStopRecording_m1776955136 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayStopRecording)();

}
extern "C" void DEFAULT_CALL EveryplayPauseRecording();
// System.Void Everyplay::EveryplayPauseRecording()
extern "C"  void Everyplay_EveryplayPauseRecording_m998977122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayPauseRecording)();

}
extern "C" void DEFAULT_CALL EveryplayResumeRecording();
// System.Void Everyplay::EveryplayResumeRecording()
extern "C"  void Everyplay_EveryplayResumeRecording_m248112421 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayResumeRecording)();

}
extern "C" int32_t DEFAULT_CALL EveryplayIsRecording();
// System.Boolean Everyplay::EveryplayIsRecording()
extern "C"  bool Everyplay_EveryplayIsRecording_m3853108226 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayIsRecording)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL EveryplayIsRecordingSupported();
// System.Boolean Everyplay::EveryplayIsRecordingSupported()
extern "C"  bool Everyplay_EveryplayIsRecordingSupported_m1388371902 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayIsRecordingSupported)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL EveryplayIsPaused();
// System.Boolean Everyplay::EveryplayIsPaused()
extern "C"  bool Everyplay_EveryplayIsPaused_m3761251281 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayIsPaused)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL EveryplaySnapshotRenderbuffer();
// System.Boolean Everyplay::EveryplaySnapshotRenderbuffer()
extern "C"  bool Everyplay_EveryplaySnapshotRenderbuffer_m1557380991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplaySnapshotRenderbuffer)();

	return static_cast<bool>(returnValue);
}
extern "C" void DEFAULT_CALL EveryplaySetMetadata(char*);
// System.Void Everyplay::EveryplaySetMetadata(System.String)
extern "C"  void Everyplay_EveryplaySetMetadata_m2926221680 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___json0' to native representation
	char* ____json0_marshaled = NULL;
	____json0_marshaled = il2cpp_codegen_marshal_string(___json0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplaySetMetadata)(____json0_marshaled);

	// Marshaling cleanup of parameter '___json0' native representation
	il2cpp_codegen_marshal_free(____json0_marshaled);
	____json0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL EveryplaySetTargetFPS(int32_t);
// System.Void Everyplay::EveryplaySetTargetFPS(System.Int32)
extern "C"  void Everyplay_EveryplaySetTargetFPS_m410279722 (Il2CppObject * __this /* static, unused */, int32_t ___fps0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplaySetTargetFPS)(___fps0);

}
extern "C" void DEFAULT_CALL EveryplaySetMotionFactor(int32_t);
// System.Void Everyplay::EveryplaySetMotionFactor(System.Int32)
extern "C"  void Everyplay_EveryplaySetMotionFactor_m1496152793 (Il2CppObject * __this /* static, unused */, int32_t ___factor0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplaySetMotionFactor)(___factor0);

}
extern "C" void DEFAULT_CALL EveryplaySetMaxRecordingMinutesLength(int32_t);
// System.Void Everyplay::EveryplaySetMaxRecordingMinutesLength(System.Int32)
extern "C"  void Everyplay_EveryplaySetMaxRecordingMinutesLength_m3939274140 (Il2CppObject * __this /* static, unused */, int32_t ___minutes0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplaySetMaxRecordingMinutesLength)(___minutes0);

}
extern "C" void DEFAULT_CALL EveryplaySetMaxRecordingSecondsLength(int32_t);
// System.Void Everyplay::EveryplaySetMaxRecordingSecondsLength(System.Int32)
extern "C"  void Everyplay_EveryplaySetMaxRecordingSecondsLength_m1274558896 (Il2CppObject * __this /* static, unused */, int32_t ___seconds0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplaySetMaxRecordingSecondsLength)(___seconds0);

}
extern "C" void DEFAULT_CALL EveryplaySetLowMemoryDevice(int32_t);
// System.Void Everyplay::EveryplaySetLowMemoryDevice(System.Boolean)
extern "C"  void Everyplay_EveryplaySetLowMemoryDevice_m155866533 (Il2CppObject * __this /* static, unused */, bool ___state0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplaySetLowMemoryDevice)(static_cast<int32_t>(___state0));

}
extern "C" void DEFAULT_CALL EveryplaySetDisableSingleCoreDevices(int32_t);
// System.Void Everyplay::EveryplaySetDisableSingleCoreDevices(System.Boolean)
extern "C"  void Everyplay_EveryplaySetDisableSingleCoreDevices_m1396293428 (Il2CppObject * __this /* static, unused */, bool ___state0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplaySetDisableSingleCoreDevices)(static_cast<int32_t>(___state0));

}
extern "C" int32_t DEFAULT_CALL EveryplayIsSupported();
// System.Boolean Everyplay::EveryplayIsSupported()
extern "C"  bool Everyplay_EveryplayIsSupported_m2495406309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayIsSupported)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL EveryplayIsSingleCoreDevice();
// System.Boolean Everyplay::EveryplayIsSingleCoreDevice()
extern "C"  bool Everyplay_EveryplayIsSingleCoreDevice_m2355195836 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayIsSingleCoreDevice)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL EveryplayGetUserInterfaceIdiom();
// System.Int32 Everyplay::EveryplayGetUserInterfaceIdiom()
extern "C"  int32_t Everyplay_EveryplayGetUserInterfaceIdiom_m298146343 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayGetUserInterfaceIdiom)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL EveryplayFaceCamIsVideoRecordingSupported();
// System.Boolean Everyplay::EveryplayFaceCamIsVideoRecordingSupported()
extern "C"  bool Everyplay_EveryplayFaceCamIsVideoRecordingSupported_m1933775331 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayFaceCamIsVideoRecordingSupported)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL EveryplayFaceCamIsAudioRecordingSupported();
// System.Boolean Everyplay::EveryplayFaceCamIsAudioRecordingSupported()
extern "C"  bool Everyplay_EveryplayFaceCamIsAudioRecordingSupported_m622408608 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayFaceCamIsAudioRecordingSupported)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL EveryplayFaceCamIsHeadphonesPluggedIn();
// System.Boolean Everyplay::EveryplayFaceCamIsHeadphonesPluggedIn()
extern "C"  bool Everyplay_EveryplayFaceCamIsHeadphonesPluggedIn_m2030515277 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayFaceCamIsHeadphonesPluggedIn)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL EveryplayFaceCamIsSessionRunning();
// System.Boolean Everyplay::EveryplayFaceCamIsSessionRunning()
extern "C"  bool Everyplay_EveryplayFaceCamIsSessionRunning_m3581958402 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayFaceCamIsSessionRunning)();

	return static_cast<bool>(returnValue);
}
extern "C" int32_t DEFAULT_CALL EveryplayFaceCamIsRecordingPermissionGranted();
// System.Boolean Everyplay::EveryplayFaceCamIsRecordingPermissionGranted()
extern "C"  bool Everyplay_EveryplayFaceCamIsRecordingPermissionGranted_m3961460134 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayFaceCamIsRecordingPermissionGranted)();

	return static_cast<bool>(returnValue);
}
extern "C" float DEFAULT_CALL EveryplayFaceCamAudioPeakLevel();
// System.Single Everyplay::EveryplayFaceCamAudioPeakLevel()
extern "C"  float Everyplay_EveryplayFaceCamAudioPeakLevel_m199553448 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(EveryplayFaceCamAudioPeakLevel)();

	return returnValue;
}
extern "C" float DEFAULT_CALL EveryplayFaceCamAudioPowerLevel();
// System.Single Everyplay::EveryplayFaceCamAudioPowerLevel()
extern "C"  float Everyplay_EveryplayFaceCamAudioPowerLevel_m3853690490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(EveryplayFaceCamAudioPowerLevel)();

	return returnValue;
}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetMonitorAudioLevels(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetMonitorAudioLevels(System.Boolean)
extern "C"  void Everyplay_EveryplayFaceCamSetMonitorAudioLevels_m127210877 (Il2CppObject * __this /* static, unused */, bool ___enabled0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetMonitorAudioLevels)(static_cast<int32_t>(___enabled0));

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetRecordingMode(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetRecordingMode(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetRecordingMode_m1780428062 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetRecordingMode)(___mode0);

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetAudioOnly(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetAudioOnly(System.Boolean)
extern "C"  void Everyplay_EveryplayFaceCamSetAudioOnly_m3301962502 (Il2CppObject * __this /* static, unused */, bool ___audioOnly0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetAudioOnly)(static_cast<int32_t>(___audioOnly0));

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetPreviewVisible(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetPreviewVisible(System.Boolean)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewVisible_m275566432 (Il2CppObject * __this /* static, unused */, bool ___visible0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetPreviewVisible)(static_cast<int32_t>(___visible0));

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetPreviewScaleRetina(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetPreviewScaleRetina(System.Boolean)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewScaleRetina_m2186195837 (Il2CppObject * __this /* static, unused */, bool ___autoScale0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetPreviewScaleRetina)(static_cast<int32_t>(___autoScale0));

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetPreviewSideWidth(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetPreviewSideWidth(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewSideWidth_m2664091223 (Il2CppObject * __this /* static, unused */, int32_t ___width0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetPreviewSideWidth)(___width0);

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetPreviewBorderWidth(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetPreviewBorderWidth(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewBorderWidth_m3607310900 (Il2CppObject * __this /* static, unused */, int32_t ___width0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetPreviewBorderWidth)(___width0);

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetPreviewPositionX(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetPreviewPositionX(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewPositionX_m2443346075 (Il2CppObject * __this /* static, unused */, int32_t ___x0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetPreviewPositionX)(___x0);

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetPreviewPositionY(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetPreviewPositionY(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewPositionY_m3939587552 (Il2CppObject * __this /* static, unused */, int32_t ___y0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetPreviewPositionY)(___y0);

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetPreviewBorderColor(float, float, float, float);
// System.Void Everyplay::EveryplayFaceCamSetPreviewBorderColor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewBorderColor_m3525118556 (Il2CppObject * __this /* static, unused */, float ___r0, float ___g1, float ___b2, float ___a3, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float, float, float, float);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetPreviewBorderColor)(___r0, ___g1, ___b2, ___a3);

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetPreviewOrigin(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetPreviewOrigin(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetPreviewOrigin_m397951628 (Il2CppObject * __this /* static, unused */, int32_t ___origin0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetPreviewOrigin)(___origin0);

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetTargetTexture(intptr_t);
// System.Void Everyplay::EveryplayFaceCamSetTargetTexture(System.IntPtr)
extern "C"  void Everyplay_EveryplayFaceCamSetTargetTexture_m3120082225 (Il2CppObject * __this /* static, unused */, IntPtr_t ___texturePtr0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetTargetTexture)(reinterpret_cast<intptr_t>((___texturePtr0).get_m_value_0()));

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetTargetTextureId(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetTargetTextureId(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetTargetTextureId_m614786215 (Il2CppObject * __this /* static, unused */, int32_t ___textureId0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetTargetTextureId)(___textureId0);

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetTargetTextureWidth(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetTargetTextureWidth(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetTargetTextureWidth_m539522378 (Il2CppObject * __this /* static, unused */, int32_t ___textureWidth0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetTargetTextureWidth)(___textureWidth0);

}
extern "C" void DEFAULT_CALL EveryplayFaceCamSetTargetTextureHeight(int32_t);
// System.Void Everyplay::EveryplayFaceCamSetTargetTextureHeight(System.Int32)
extern "C"  void Everyplay_EveryplayFaceCamSetTargetTextureHeight_m450171829 (Il2CppObject * __this /* static, unused */, int32_t ___textureHeight0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamSetTargetTextureHeight)(___textureHeight0);

}
extern "C" void DEFAULT_CALL EveryplayFaceCamStartSession();
// System.Void Everyplay::EveryplayFaceCamStartSession()
extern "C"  void Everyplay_EveryplayFaceCamStartSession_m563630289 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamStartSession)();

}
extern "C" void DEFAULT_CALL EveryplayFaceCamRequestRecordingPermission();
// System.Void Everyplay::EveryplayFaceCamRequestRecordingPermission()
extern "C"  void Everyplay_EveryplayFaceCamRequestRecordingPermission_m3269278668 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamRequestRecordingPermission)();

}
extern "C" void DEFAULT_CALL EveryplayFaceCamStopSession();
// System.Void Everyplay::EveryplayFaceCamStopSession()
extern "C"  void Everyplay_EveryplayFaceCamStopSession_m2774136573 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayFaceCamStopSession)();

}
extern "C" void DEFAULT_CALL EveryplaySetThumbnailTargetTexture(intptr_t);
// System.Void Everyplay::EveryplaySetThumbnailTargetTexture(System.IntPtr)
extern "C"  void Everyplay_EveryplaySetThumbnailTargetTexture_m1799038923 (Il2CppObject * __this /* static, unused */, IntPtr_t ___texturePtr0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplaySetThumbnailTargetTexture)(reinterpret_cast<intptr_t>((___texturePtr0).get_m_value_0()));

}
extern "C" void DEFAULT_CALL EveryplaySetThumbnailTargetTextureId(int32_t);
// System.Void Everyplay::EveryplaySetThumbnailTargetTextureId(System.Int32)
extern "C"  void Everyplay_EveryplaySetThumbnailTargetTextureId_m3960249425 (Il2CppObject * __this /* static, unused */, int32_t ___textureId0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplaySetThumbnailTargetTextureId)(___textureId0);

}
extern "C" void DEFAULT_CALL EveryplaySetThumbnailTargetTextureWidth(int32_t);
// System.Void Everyplay::EveryplaySetThumbnailTargetTextureWidth(System.Int32)
extern "C"  void Everyplay_EveryplaySetThumbnailTargetTextureWidth_m379731554 (Il2CppObject * __this /* static, unused */, int32_t ___textureWidth0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplaySetThumbnailTargetTextureWidth)(___textureWidth0);

}
extern "C" void DEFAULT_CALL EveryplaySetThumbnailTargetTextureHeight(int32_t);
// System.Void Everyplay::EveryplaySetThumbnailTargetTextureHeight(System.Int32)
extern "C"  void Everyplay_EveryplaySetThumbnailTargetTextureHeight_m3729298403 (Il2CppObject * __this /* static, unused */, int32_t ___textureHeight0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplaySetThumbnailTargetTextureHeight)(___textureHeight0);

}
extern "C" void DEFAULT_CALL EveryplayTakeThumbnail();
// System.Void Everyplay::EveryplayTakeThumbnail()
extern "C"  void Everyplay_EveryplayTakeThumbnail_m1202186698 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(EveryplayTakeThumbnail)();

}
// System.Void Everyplay::.cctor()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t Everyplay__cctor_m2517547459_MetadataUsageId;
extern "C"  void Everyplay__cctor_m2517547459 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Everyplay__cctor_m2517547459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->set_hasMethods_16((bool)1);
		return;
	}
}
// System.Void Everyplay/<MakeRequestEnumerator>c__Iterator0::.ctor()
extern "C"  void U3CMakeRequestEnumeratorU3Ec__Iterator0__ctor_m1954205247 (U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Everyplay/<MakeRequestEnumerator>c__Iterator0::MoveNext()
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3188644741_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m760167321_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m4244870320_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1128763565_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3833952254;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern Il2CppCodeGenString* _stringLiteral715456741;
extern Il2CppCodeGenString* _stringLiteral2313763943;
extern Il2CppCodeGenString* _stringLiteral488122563;
extern Il2CppCodeGenString* _stringLiteral3980222703;
extern Il2CppCodeGenString* _stringLiteral372029331;
extern Il2CppCodeGenString* _stringLiteral372029308;
extern Il2CppCodeGenString* _stringLiteral3313422838;
extern Il2CppCodeGenString* _stringLiteral3483507730;
extern Il2CppCodeGenString* _stringLiteral3819387978;
extern Il2CppCodeGenString* _stringLiteral1391431453;
extern Il2CppCodeGenString* _stringLiteral1048821954;
extern Il2CppCodeGenString* _stringLiteral1511988009;
extern Il2CppCodeGenString* _stringLiteral2704910744;
extern Il2CppCodeGenString* _stringLiteral677017058;
extern Il2CppCodeGenString* _stringLiteral2979633503;
extern const uint32_t U3CMakeRequestEnumeratorU3Ec__Iterator0_MoveNext_m3593835773_MetadataUsageId;
extern "C"  bool U3CMakeRequestEnumeratorU3Ec__Iterator0_MoveNext_m3593835773 (U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeRequestEnumeratorU3Ec__Iterator0_MoveNext_m3593835773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U24PC_12();
		V_0 = L_0;
		__this->set_U24PC_12((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_023b;
		}
	}
	{
		goto IL_02bd;
	}

IL_0021:
	{
		Dictionary_2_t309261261 * L_2 = __this->get_data_0();
		if (L_2)
		{
			goto IL_0037;
		}
	}
	{
		Dictionary_2_t309261261 * L_3 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3188644741(L_3, /*hidden argument*/Dictionary_2__ctor_m3188644741_MethodInfo_var);
		__this->set_data_0(L_3);
	}

IL_0037:
	{
		String_t* L_4 = __this->get_url_1();
		NullCheck(L_4);
		int32_t L_5 = String_IndexOf_m4251815737(L_4, _stringLiteral3833952254, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_6 = __this->get_url_1();
		NullCheck(L_6);
		int32_t L_7 = String_IndexOf_m4251815737(L_6, _stringLiteral372029315, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0077;
		}
	}
	{
		String_t* L_8 = __this->get_url_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029315, L_8, /*hidden argument*/NULL);
		__this->set_url_1(L_9);
	}

IL_0077:
	{
		String_t* L_10 = __this->get_url_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral715456741, L_10, /*hidden argument*/NULL);
		__this->set_url_1(L_11);
	}

IL_008d:
	{
		String_t* L_12 = __this->get_method_2();
		NullCheck(L_12);
		String_t* L_13 = String_ToLower_m2994460523(L_12, /*hidden argument*/NULL);
		__this->set_method_2(L_13);
		Dictionary_2_t3943999495 * L_14 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m760167321(L_14, /*hidden argument*/Dictionary_2__ctor_m760167321_MethodInfo_var);
		__this->set_U3CheadersU3E__0_3(L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		String_t* L_15 = Everyplay_AccessToken_m3705224328(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CaccessTokenU3E__1_4(L_15);
		String_t* L_16 = __this->get_U3CaccessTokenU3E__1_4();
		if (!L_16)
		{
			goto IL_00e4;
		}
	}
	{
		Dictionary_2_t3943999495 * L_17 = __this->get_U3CheadersU3E__0_3();
		String_t* L_18 = __this->get_U3CaccessTokenU3E__1_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral488122563, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Dictionary_2_set_Item_m4244870320(L_17, _stringLiteral2313763943, L_19, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
		goto IL_015c;
	}

IL_00e4:
	{
		String_t* L_20 = __this->get_url_1();
		NullCheck(L_20);
		int32_t L_21 = String_IndexOf_m4251815737(L_20, _stringLiteral3980222703, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_21) == ((uint32_t)(-1)))))
		{
			goto IL_015c;
		}
	}
	{
		String_t* L_22 = __this->get_url_1();
		NullCheck(L_22);
		int32_t L_23 = String_IndexOf_m4251815737(L_22, _stringLiteral372029331, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_23) == ((uint32_t)(-1)))))
		{
			goto IL_012b;
		}
	}
	{
		String_t* L_24 = __this->get_url_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m2596409543(NULL /*static, unused*/, L_24, _stringLiteral372029331, /*hidden argument*/NULL);
		__this->set_url_1(L_25);
		goto IL_0141;
	}

IL_012b:
	{
		String_t* L_26 = __this->get_url_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m2596409543(NULL /*static, unused*/, L_26, _stringLiteral372029308, /*hidden argument*/NULL);
		__this->set_url_1(L_27);
	}

IL_0141:
	{
		String_t* L_28 = __this->get_url_1();
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		String_t* L_29 = ((Everyplay_t1799077027_StaticFields*)Everyplay_t1799077027_il2cpp_TypeInfo_var->static_fields)->get_clientId_14();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m612901809(NULL /*static, unused*/, L_28, _stringLiteral3313422838, L_29, /*hidden argument*/NULL);
		__this->set_url_1(L_30);
	}

IL_015c:
	{
		Dictionary_2_t309261261 * L_31 = __this->get_data_0();
		String_t* L_32 = __this->get_method_2();
		NullCheck(L_31);
		Dictionary_2_Add_m1128763565(L_31, _stringLiteral3483507730, L_32, /*hidden argument*/Dictionary_2_Add_m1128763565_MethodInfo_var);
		Dictionary_2_t309261261 * L_33 = __this->get_data_0();
		String_t* L_34 = Json_Serialize_m1349440280(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		__this->set_U3CdataStringU3E__2_5(L_34);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_35 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_36 = __this->get_U3CdataStringU3E__2_5();
		NullCheck(L_35);
		ByteU5BU5D_t3397334013* L_37 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_35, L_36);
		__this->set_U3CdataArrayU3E__3_6(L_37);
		Dictionary_2_t3943999495 * L_38 = __this->get_U3CheadersU3E__0_3();
		NullCheck(L_38);
		Dictionary_2_set_Item_m4244870320(L_38, _stringLiteral3819387978, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
		Dictionary_2_t3943999495 * L_39 = __this->get_U3CheadersU3E__0_3();
		NullCheck(L_39);
		Dictionary_2_set_Item_m4244870320(L_39, _stringLiteral1048821954, _stringLiteral1391431453, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
		Dictionary_2_t3943999495 * L_40 = __this->get_U3CheadersU3E__0_3();
		NullCheck(L_40);
		Dictionary_2_set_Item_m4244870320(L_40, _stringLiteral1511988009, _stringLiteral2704910744, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
		Dictionary_2_t3943999495 * L_41 = __this->get_U3CheadersU3E__0_3();
		ByteU5BU5D_t3397334013* L_42 = __this->get_U3CdataArrayU3E__3_6();
		NullCheck(L_42);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length))));
		String_t* L_43 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		NullCheck(L_41);
		Dictionary_2_set_Item_m4244870320(L_41, _stringLiteral677017058, L_43, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
		String_t* L_44 = __this->get_url_1();
		ByteU5BU5D_t3397334013* L_45 = __this->get_U3CdataArrayU3E__3_6();
		Dictionary_2_t3943999495 * L_46 = __this->get_U3CheadersU3E__0_3();
		WWW_t2919945039 * L_47 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m1577105574(L_47, L_44, L_45, L_46, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__4_7(L_47);
		WWW_t2919945039 * L_48 = __this->get_U3CwwwU3E__4_7();
		__this->set_U24current_10(L_48);
		bool L_49 = __this->get_U24disposing_11();
		if (L_49)
		{
			goto IL_0236;
		}
	}
	{
		__this->set_U24PC_12(1);
	}

IL_0236:
	{
		goto IL_02bf;
	}

IL_023b:
	{
		WWW_t2919945039 * L_50 = __this->get_U3CwwwU3E__4_7();
		NullCheck(L_50);
		String_t* L_51 = WWW_get_error_m3092701216(L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		if (L_52)
		{
			goto IL_0280;
		}
	}
	{
		RequestFailedDelegate_t3135434785 * L_53 = __this->get_failedDelegate_8();
		if (!L_53)
		{
			goto IL_0280;
		}
	}
	{
		RequestFailedDelegate_t3135434785 * L_54 = __this->get_failedDelegate_8();
		WWW_t2919945039 * L_55 = __this->get_U3CwwwU3E__4_7();
		NullCheck(L_55);
		String_t* L_56 = WWW_get_error_m3092701216(L_55, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_57 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2979633503, L_56, /*hidden argument*/NULL);
		NullCheck(L_54);
		RequestFailedDelegate_Invoke_m3522845546(L_54, L_57, /*hidden argument*/NULL);
		goto IL_02b6;
	}

IL_0280:
	{
		WWW_t2919945039 * L_58 = __this->get_U3CwwwU3E__4_7();
		NullCheck(L_58);
		String_t* L_59 = WWW_get_error_m3092701216(L_58, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_60 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_02b6;
		}
	}
	{
		RequestReadyDelegate_t4013578405 * L_61 = __this->get_readyDelegate_9();
		if (!L_61)
		{
			goto IL_02b6;
		}
	}
	{
		RequestReadyDelegate_t4013578405 * L_62 = __this->get_readyDelegate_9();
		WWW_t2919945039 * L_63 = __this->get_U3CwwwU3E__4_7();
		NullCheck(L_63);
		String_t* L_64 = WWW_get_text_m1558985139(L_63, /*hidden argument*/NULL);
		NullCheck(L_62);
		RequestReadyDelegate_Invoke_m2023212226(L_62, L_64, /*hidden argument*/NULL);
	}

IL_02b6:
	{
		__this->set_U24PC_12((-1));
	}

IL_02bd:
	{
		return (bool)0;
	}

IL_02bf:
	{
		return (bool)1;
	}
}
// System.Object Everyplay/<MakeRequestEnumerator>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeRequestEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1936361649 (U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_10();
		return L_0;
	}
}
// System.Object Everyplay/<MakeRequestEnumerator>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeRequestEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2285939673 (U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_10();
		return L_0;
	}
}
// System.Void Everyplay/<MakeRequestEnumerator>c__Iterator0::Dispose()
extern "C"  void U3CMakeRequestEnumeratorU3Ec__Iterator0_Dispose_m1056559392 (U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_11((bool)1);
		__this->set_U24PC_12((-1));
		return;
	}
}
// System.Void Everyplay/<MakeRequestEnumerator>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMakeRequestEnumeratorU3Ec__Iterator0_Reset_m706398818_MetadataUsageId;
extern "C"  void U3CMakeRequestEnumeratorU3Ec__Iterator0_Reset_m706398818 (U3CMakeRequestEnumeratorU3Ec__Iterator0_t4090265924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeRequestEnumeratorU3Ec__Iterator0_Reset_m706398818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Everyplay/FaceCamRecordingPermissionDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void FaceCamRecordingPermissionDelegate__ctor_m539527746 (FaceCamRecordingPermissionDelegate_t1670731619 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/FaceCamRecordingPermissionDelegate::Invoke(System.Boolean)
extern "C"  void FaceCamRecordingPermissionDelegate_Invoke_m1744694023 (FaceCamRecordingPermissionDelegate_t1670731619 * __this, bool ___granted0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FaceCamRecordingPermissionDelegate_Invoke_m1744694023((FaceCamRecordingPermissionDelegate_t1670731619 *)__this->get_prev_9(),___granted0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___granted0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___granted0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___granted0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___granted0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FaceCamRecordingPermissionDelegate_t1670731619 (FaceCamRecordingPermissionDelegate_t1670731619 * __this, bool ___granted0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___granted0));

}
// System.IAsyncResult Everyplay/FaceCamRecordingPermissionDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t FaceCamRecordingPermissionDelegate_BeginInvoke_m3142418516_MetadataUsageId;
extern "C"  Il2CppObject * FaceCamRecordingPermissionDelegate_BeginInvoke_m3142418516 (FaceCamRecordingPermissionDelegate_t1670731619 * __this, bool ___granted0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FaceCamRecordingPermissionDelegate_BeginInvoke_m3142418516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___granted0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Everyplay/FaceCamRecordingPermissionDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void FaceCamRecordingPermissionDelegate_EndInvoke_m3467057676 (FaceCamRecordingPermissionDelegate_t1670731619 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/FaceCamSessionStartedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void FaceCamSessionStartedDelegate__ctor_m1763728449 (FaceCamSessionStartedDelegate_t1733547424 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/FaceCamSessionStartedDelegate::Invoke()
extern "C"  void FaceCamSessionStartedDelegate_Invoke_m1284990449 (FaceCamSessionStartedDelegate_t1733547424 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FaceCamSessionStartedDelegate_Invoke_m1284990449((FaceCamSessionStartedDelegate_t1733547424 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FaceCamSessionStartedDelegate_t1733547424 (FaceCamSessionStartedDelegate_t1733547424 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult Everyplay/FaceCamSessionStartedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FaceCamSessionStartedDelegate_BeginInvoke_m4215735148 (FaceCamSessionStartedDelegate_t1733547424 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Everyplay/FaceCamSessionStartedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void FaceCamSessionStartedDelegate_EndInvoke_m2537038431 (FaceCamSessionStartedDelegate_t1733547424 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/FaceCamSessionStoppedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void FaceCamSessionStoppedDelegate__ctor_m4186510893 (FaceCamSessionStoppedDelegate_t1894731428 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/FaceCamSessionStoppedDelegate::Invoke()
extern "C"  void FaceCamSessionStoppedDelegate_Invoke_m3720472693 (FaceCamSessionStoppedDelegate_t1894731428 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FaceCamSessionStoppedDelegate_Invoke_m3720472693((FaceCamSessionStoppedDelegate_t1894731428 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_FaceCamSessionStoppedDelegate_t1894731428 (FaceCamSessionStoppedDelegate_t1894731428 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult Everyplay/FaceCamSessionStoppedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FaceCamSessionStoppedDelegate_BeginInvoke_m498308524 (FaceCamSessionStoppedDelegate_t1894731428 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Everyplay/FaceCamSessionStoppedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void FaceCamSessionStoppedDelegate_EndInvoke_m3919409903 (FaceCamSessionStoppedDelegate_t1894731428 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/ReadyForRecordingDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ReadyForRecordingDelegate__ctor_m4085498349 (ReadyForRecordingDelegate_t1593758596 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/ReadyForRecordingDelegate::Invoke(System.Boolean)
extern "C"  void ReadyForRecordingDelegate_Invoke_m1966730172 (ReadyForRecordingDelegate_t1593758596 * __this, bool ___enabled0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReadyForRecordingDelegate_Invoke_m1966730172((ReadyForRecordingDelegate_t1593758596 *)__this->get_prev_9(),___enabled0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___enabled0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___enabled0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___enabled0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___enabled0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ReadyForRecordingDelegate_t1593758596 (ReadyForRecordingDelegate_t1593758596 * __this, bool ___enabled0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___enabled0));

}
// System.IAsyncResult Everyplay/ReadyForRecordingDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t ReadyForRecordingDelegate_BeginInvoke_m1277641157_MetadataUsageId;
extern "C"  Il2CppObject * ReadyForRecordingDelegate_BeginInvoke_m1277641157 (ReadyForRecordingDelegate_t1593758596 * __this, bool ___enabled0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadyForRecordingDelegate_BeginInvoke_m1277641157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___enabled0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Everyplay/ReadyForRecordingDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ReadyForRecordingDelegate_EndInvoke_m4071120819 (ReadyForRecordingDelegate_t1593758596 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/RecordingStartedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void RecordingStartedDelegate__ctor_m1828386050 (RecordingStartedDelegate_t5060419 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/RecordingStartedDelegate::Invoke()
extern "C"  void RecordingStartedDelegate_Invoke_m3318412280 (RecordingStartedDelegate_t5060419 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RecordingStartedDelegate_Invoke_m3318412280((RecordingStartedDelegate_t5060419 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_RecordingStartedDelegate_t5060419 (RecordingStartedDelegate_t5060419 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult Everyplay/RecordingStartedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RecordingStartedDelegate_BeginInvoke_m1131019807 (RecordingStartedDelegate_t5060419 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Everyplay/RecordingStartedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void RecordingStartedDelegate_EndInvoke_m2512124704 (RecordingStartedDelegate_t5060419 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/RecordingStoppedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void RecordingStoppedDelegate__ctor_m1537694926 (RecordingStoppedDelegate_t3008025639 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/RecordingStoppedDelegate::Invoke()
extern "C"  void RecordingStoppedDelegate_Invoke_m2827616860 (RecordingStoppedDelegate_t3008025639 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RecordingStoppedDelegate_Invoke_m2827616860((RecordingStoppedDelegate_t3008025639 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_RecordingStoppedDelegate_t3008025639 (RecordingStoppedDelegate_t3008025639 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult Everyplay/RecordingStoppedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RecordingStoppedDelegate_BeginInvoke_m1915962495 (RecordingStoppedDelegate_t3008025639 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Everyplay/RecordingStoppedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void RecordingStoppedDelegate_EndInvoke_m1411260736 (RecordingStoppedDelegate_t3008025639 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/RequestFailedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestFailedDelegate__ctor_m844497070 (RequestFailedDelegate_t3135434785 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/RequestFailedDelegate::Invoke(System.String)
extern "C"  void RequestFailedDelegate_Invoke_m3522845546 (RequestFailedDelegate_t3135434785 * __this, String_t* ___error0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RequestFailedDelegate_Invoke_m3522845546((RequestFailedDelegate_t3135434785 *)__this->get_prev_9(),___error0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___error0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___error0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___error0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___error0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___error0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_RequestFailedDelegate_t3135434785 (RequestFailedDelegate_t3135434785 * __this, String_t* ___error0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___error0' to native representation
	char* ____error0_marshaled = NULL;
	____error0_marshaled = il2cpp_codegen_marshal_string(___error0);

	// Native function invocation
	il2cppPInvokeFunc(____error0_marshaled);

	// Marshaling cleanup of parameter '___error0' native representation
	il2cpp_codegen_marshal_free(____error0_marshaled);
	____error0_marshaled = NULL;

}
// System.IAsyncResult Everyplay/RequestFailedDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RequestFailedDelegate_BeginInvoke_m3384874647 (RequestFailedDelegate_t3135434785 * __this, String_t* ___error0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___error0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Everyplay/RequestFailedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void RequestFailedDelegate_EndInvoke_m3568189980 (RequestFailedDelegate_t3135434785 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/RequestReadyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestReadyDelegate__ctor_m3227343646 (RequestReadyDelegate_t4013578405 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/RequestReadyDelegate::Invoke(System.String)
extern "C"  void RequestReadyDelegate_Invoke_m2023212226 (RequestReadyDelegate_t4013578405 * __this, String_t* ___response0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RequestReadyDelegate_Invoke_m2023212226((RequestReadyDelegate_t4013578405 *)__this->get_prev_9(),___response0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___response0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___response0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___response0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___response0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___response0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_RequestReadyDelegate_t4013578405 (RequestReadyDelegate_t4013578405 * __this, String_t* ___response0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___response0' to native representation
	char* ____response0_marshaled = NULL;
	____response0_marshaled = il2cpp_codegen_marshal_string(___response0);

	// Native function invocation
	il2cppPInvokeFunc(____response0_marshaled);

	// Marshaling cleanup of parameter '___response0' native representation
	il2cpp_codegen_marshal_free(____response0_marshaled);
	____response0_marshaled = NULL;

}
// System.IAsyncResult Everyplay/RequestReadyDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * RequestReadyDelegate_BeginInvoke_m4250386803 (RequestReadyDelegate_t4013578405 * __this, String_t* ___response0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___response0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Everyplay/RequestReadyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void RequestReadyDelegate_EndInvoke_m1528410900 (RequestReadyDelegate_t4013578405 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/ThumbnailReadyAtTextureIdDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ThumbnailReadyAtTextureIdDelegate__ctor_m818508762 (ThumbnailReadyAtTextureIdDelegate_t3853410271 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/ThumbnailReadyAtTextureIdDelegate::Invoke(System.Int32,System.Boolean)
extern "C"  void ThumbnailReadyAtTextureIdDelegate_Invoke_m419038684 (ThumbnailReadyAtTextureIdDelegate_t3853410271 * __this, int32_t ___textureId0, bool ___portrait1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ThumbnailReadyAtTextureIdDelegate_Invoke_m419038684((ThumbnailReadyAtTextureIdDelegate_t3853410271 *)__this->get_prev_9(),___textureId0, ___portrait1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___textureId0, bool ___portrait1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___textureId0, ___portrait1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___textureId0, bool ___portrait1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___textureId0, ___portrait1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ThumbnailReadyAtTextureIdDelegate_t3853410271 (ThumbnailReadyAtTextureIdDelegate_t3853410271 * __this, int32_t ___textureId0, bool ___portrait1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___textureId0, static_cast<int32_t>(___portrait1));

}
// System.IAsyncResult Everyplay/ThumbnailReadyAtTextureIdDelegate::BeginInvoke(System.Int32,System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t ThumbnailReadyAtTextureIdDelegate_BeginInvoke_m1723076673_MetadataUsageId;
extern "C"  Il2CppObject * ThumbnailReadyAtTextureIdDelegate_BeginInvoke_m1723076673 (ThumbnailReadyAtTextureIdDelegate_t3853410271 * __this, int32_t ___textureId0, bool ___portrait1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThumbnailReadyAtTextureIdDelegate_BeginInvoke_m1723076673_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___textureId0);
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___portrait1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Everyplay/ThumbnailReadyAtTextureIdDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ThumbnailReadyAtTextureIdDelegate_EndInvoke_m2924983148 (ThumbnailReadyAtTextureIdDelegate_t3853410271 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/ThumbnailTextureReadyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ThumbnailTextureReadyDelegate__ctor_m3605697724 (ThumbnailTextureReadyDelegate_t2948235259 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/ThumbnailTextureReadyDelegate::Invoke(UnityEngine.Texture2D,System.Boolean)
extern "C"  void ThumbnailTextureReadyDelegate_Invoke_m3492481785 (ThumbnailTextureReadyDelegate_t2948235259 * __this, Texture2D_t3542995729 * ___texture0, bool ___portrait1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ThumbnailTextureReadyDelegate_Invoke_m3492481785((ThumbnailTextureReadyDelegate_t2948235259 *)__this->get_prev_9(),___texture0, ___portrait1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Texture2D_t3542995729 * ___texture0, bool ___portrait1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___texture0, ___portrait1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Texture2D_t3542995729 * ___texture0, bool ___portrait1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___texture0, ___portrait1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___portrait1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___texture0, ___portrait1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Everyplay/ThumbnailTextureReadyDelegate::BeginInvoke(UnityEngine.Texture2D,System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t ThumbnailTextureReadyDelegate_BeginInvoke_m3479227342_MetadataUsageId;
extern "C"  Il2CppObject * ThumbnailTextureReadyDelegate_BeginInvoke_m3479227342 (ThumbnailTextureReadyDelegate_t2948235259 * __this, Texture2D_t3542995729 * ___texture0, bool ___portrait1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThumbnailTextureReadyDelegate_BeginInvoke_m3479227342_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___texture0;
	__d_args[1] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___portrait1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Everyplay/ThumbnailTextureReadyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ThumbnailTextureReadyDelegate_EndInvoke_m125693966 (ThumbnailTextureReadyDelegate_t2948235259 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/UploadDidCompleteDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void UploadDidCompleteDelegate__ctor_m3967701987 (UploadDidCompleteDelegate_t1564565876 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/UploadDidCompleteDelegate::Invoke(System.Int32)
extern "C"  void UploadDidCompleteDelegate_Invoke_m3224334404 (UploadDidCompleteDelegate_t1564565876 * __this, int32_t ___videoId0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UploadDidCompleteDelegate_Invoke_m3224334404((UploadDidCompleteDelegate_t1564565876 *)__this->get_prev_9(),___videoId0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___videoId0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___videoId0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___videoId0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___videoId0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_UploadDidCompleteDelegate_t1564565876 (UploadDidCompleteDelegate_t1564565876 * __this, int32_t ___videoId0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___videoId0);

}
// System.IAsyncResult Everyplay/UploadDidCompleteDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t UploadDidCompleteDelegate_BeginInvoke_m3582390865_MetadataUsageId;
extern "C"  Il2CppObject * UploadDidCompleteDelegate_BeginInvoke_m3582390865 (UploadDidCompleteDelegate_t1564565876 * __this, int32_t ___videoId0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadDidCompleteDelegate_BeginInvoke_m3582390865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___videoId0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Everyplay/UploadDidCompleteDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void UploadDidCompleteDelegate_EndInvoke_m1332152417 (UploadDidCompleteDelegate_t1564565876 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/UploadDidProgressDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void UploadDidProgressDelegate__ctor_m1182991975 (UploadDidProgressDelegate_t2069570344 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/UploadDidProgressDelegate::Invoke(System.Int32,System.Single)
extern "C"  void UploadDidProgressDelegate_Invoke_m511099547 (UploadDidProgressDelegate_t2069570344 * __this, int32_t ___videoId0, float ___progress1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UploadDidProgressDelegate_Invoke_m511099547((UploadDidProgressDelegate_t2069570344 *)__this->get_prev_9(),___videoId0, ___progress1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___videoId0, float ___progress1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___videoId0, ___progress1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___videoId0, float ___progress1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___videoId0, ___progress1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_UploadDidProgressDelegate_t2069570344 (UploadDidProgressDelegate_t2069570344 * __this, int32_t ___videoId0, float ___progress1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t, float);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___videoId0, ___progress1);

}
// System.IAsyncResult Everyplay/UploadDidProgressDelegate::BeginInvoke(System.Int32,System.Single,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t UploadDidProgressDelegate_BeginInvoke_m2925747644_MetadataUsageId;
extern "C"  Il2CppObject * UploadDidProgressDelegate_BeginInvoke_m2925747644 (UploadDidProgressDelegate_t2069570344 * __this, int32_t ___videoId0, float ___progress1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadDidProgressDelegate_BeginInvoke_m2925747644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___videoId0);
	__d_args[1] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___progress1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void Everyplay/UploadDidProgressDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void UploadDidProgressDelegate_EndInvoke_m4023123501 (UploadDidProgressDelegate_t2069570344 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/UploadDidStartDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void UploadDidStartDelegate__ctor_m2080745860 (UploadDidStartDelegate_t1871027361 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/UploadDidStartDelegate::Invoke(System.Int32)
extern "C"  void UploadDidStartDelegate_Invoke_m3051571839 (UploadDidStartDelegate_t1871027361 * __this, int32_t ___videoId0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UploadDidStartDelegate_Invoke_m3051571839((UploadDidStartDelegate_t1871027361 *)__this->get_prev_9(),___videoId0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___videoId0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___videoId0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___videoId0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___videoId0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_UploadDidStartDelegate_t1871027361 (UploadDidStartDelegate_t1871027361 * __this, int32_t ___videoId0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___videoId0);

}
// System.IAsyncResult Everyplay/UploadDidStartDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t UploadDidStartDelegate_BeginInvoke_m362396062_MetadataUsageId;
extern "C"  Il2CppObject * UploadDidStartDelegate_BeginInvoke_m362396062 (UploadDidStartDelegate_t1871027361 * __this, int32_t ___videoId0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UploadDidStartDelegate_BeginInvoke_m362396062_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___videoId0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void Everyplay/UploadDidStartDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void UploadDidStartDelegate_EndInvoke_m3859536394 (UploadDidStartDelegate_t1871027361 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Everyplay/WasClosedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void WasClosedDelegate__ctor_m1406105615 (WasClosedDelegate_t946300984 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Everyplay/WasClosedDelegate::Invoke()
extern "C"  void WasClosedDelegate_Invoke_m2969330763 (WasClosedDelegate_t946300984 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WasClosedDelegate_Invoke_m2969330763((WasClosedDelegate_t946300984 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_WasClosedDelegate_t946300984 (WasClosedDelegate_t946300984 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult Everyplay/WasClosedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WasClosedDelegate_BeginInvoke_m3121665708 (WasClosedDelegate_t946300984 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Everyplay/WasClosedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void WasClosedDelegate_EndInvoke_m1646449925 (WasClosedDelegate_t946300984 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void EveryplayAnimatedThumbnail::.ctor()
extern "C"  void EveryplayAnimatedThumbnail__ctor_m1963933355 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayAnimatedThumbnail::Awake()
extern const MethodInfo* Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var;
extern const uint32_t EveryplayAnimatedThumbnail_Awake_m78205726_MetadataUsageId;
extern "C"  void EveryplayAnimatedThumbnail_Awake_m78205726 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayAnimatedThumbnail_Awake_m78205726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Renderer_t257310565 * L_0 = Component_GetComponent_TisRenderer_t257310565_m141370815(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m141370815_MethodInfo_var);
		__this->set_mainRenderer_3(L_0);
		return;
	}
}
// System.Void EveryplayAnimatedThumbnail::Start()
extern const Il2CppType* EveryplayThumbnailPool_t101914191_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* EveryplayThumbnailPool_t101914191_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3241710594;
extern const uint32_t EveryplayAnimatedThumbnail_Start_m2007437215_MetadataUsageId;
extern "C"  void EveryplayAnimatedThumbnail_Start_m2007437215 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayAnimatedThumbnail_Start_m2007437215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(EveryplayThumbnailPool_t101914191_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_1 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_thumbnailPool_2(((EveryplayThumbnailPool_t101914191 *)CastclassClass(L_1, EveryplayThumbnailPool_t101914191_il2cpp_TypeInfo_var)));
		EveryplayThumbnailPool_t101914191 * L_2 = __this->get_thumbnailPool_2();
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		Renderer_t257310565 * L_4 = __this->get_mainRenderer_3();
		NullCheck(L_4);
		Material_t193706927 * L_5 = Renderer_get_material_m2553789785(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Texture_t2243626319 * L_6 = Material_get_mainTexture_m432794412(L_5, /*hidden argument*/NULL);
		__this->set_defaultTexture_4(L_6);
		EveryplayAnimatedThumbnail_ResetThumbnail_m3893133820(__this, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3241710594, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void EveryplayAnimatedThumbnail::OnDestroy()
extern "C"  void EveryplayAnimatedThumbnail_OnDestroy_m1984512408 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method)
{
	{
		EveryplayAnimatedThumbnail_StopTransitions_m473791811(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayAnimatedThumbnail::OnDisable()
extern "C"  void EveryplayAnimatedThumbnail_OnDisable_m2113261882 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method)
{
	{
		EveryplayAnimatedThumbnail_StopTransitions_m473791811(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayAnimatedThumbnail::ResetThumbnail()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1022637398;
extern const uint32_t EveryplayAnimatedThumbnail_ResetThumbnail_m3893133820_MetadataUsageId;
extern "C"  void EveryplayAnimatedThumbnail_ResetThumbnail_m3893133820 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayAnimatedThumbnail_ResetThumbnail_m3893133820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_currentIndex_5((-1));
		EveryplayAnimatedThumbnail_StopTransitions_m473791811(__this, /*hidden argument*/NULL);
		__this->set_blend_7((0.0f));
		Renderer_t257310565 * L_0 = __this->get_mainRenderer_3();
		NullCheck(L_0);
		Material_t193706927 * L_1 = Renderer_get_material_m2553789785(L_0, /*hidden argument*/NULL);
		float L_2 = __this->get_blend_7();
		NullCheck(L_1);
		Material_SetFloat_m1926275467(L_1, _stringLiteral1022637398, L_2, /*hidden argument*/NULL);
		Renderer_t257310565 * L_3 = __this->get_mainRenderer_3();
		NullCheck(L_3);
		Material_t193706927 * L_4 = Renderer_get_material_m2553789785(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Texture_t2243626319 * L_5 = Material_get_mainTexture_m432794412(L_4, /*hidden argument*/NULL);
		Texture_t2243626319 * L_6 = __this->get_defaultTexture_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007e;
		}
	}
	{
		Renderer_t257310565 * L_8 = __this->get_mainRenderer_3();
		NullCheck(L_8);
		Material_t193706927 * L_9 = Renderer_get_material_m2553789785(L_8, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_set_mainTextureScale_m723074403(L_9, L_10, /*hidden argument*/NULL);
		Renderer_t257310565 * L_11 = __this->get_mainRenderer_3();
		NullCheck(L_11);
		Material_t193706927 * L_12 = Renderer_get_material_m2553789785(L_11, /*hidden argument*/NULL);
		Texture_t2243626319 * L_13 = __this->get_defaultTexture_4();
		NullCheck(L_12);
		Material_set_mainTexture_m3584203343(L_12, L_13, /*hidden argument*/NULL);
	}

IL_007e:
	{
		return;
	}
}
// System.Collections.IEnumerator EveryplayAnimatedThumbnail::CrossfadeTransition()
extern Il2CppClass* U3CCrossfadeTransitionU3Ec__Iterator0_t120047739_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayAnimatedThumbnail_CrossfadeTransition_m3430637328_MetadataUsageId;
extern "C"  Il2CppObject * EveryplayAnimatedThumbnail_CrossfadeTransition_m3430637328 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayAnimatedThumbnail_CrossfadeTransition_m3430637328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CCrossfadeTransitionU3Ec__Iterator0_t120047739 * V_0 = NULL;
	{
		U3CCrossfadeTransitionU3Ec__Iterator0_t120047739 * L_0 = (U3CCrossfadeTransitionU3Ec__Iterator0_t120047739 *)il2cpp_codegen_object_new(U3CCrossfadeTransitionU3Ec__Iterator0_t120047739_il2cpp_TypeInfo_var);
		U3CCrossfadeTransitionU3Ec__Iterator0__ctor_m3234585754(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCrossfadeTransitionU3Ec__Iterator0_t120047739 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CCrossfadeTransitionU3Ec__Iterator0_t120047739 * L_2 = V_0;
		return L_2;
	}
}
// System.Void EveryplayAnimatedThumbnail::StopTransitions()
extern "C"  void EveryplayAnimatedThumbnail_StopTransitions_m473791811 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method)
{
	{
		__this->set_transitionInProgress_6((bool)0);
		MonoBehaviour_StopAllCoroutines_m1675795839(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayAnimatedThumbnail::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2172956323;
extern Il2CppCodeGenString* _stringLiteral2887705779;
extern const uint32_t EveryplayAnimatedThumbnail_Update_m3294450140_MetadataUsageId;
extern "C"  void EveryplayAnimatedThumbnail_Update_m3294450140 (EveryplayAnimatedThumbnail_t3478222492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayAnimatedThumbnail_Update_m3294450140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EveryplayThumbnailPool_t101914191 * L_0 = __this->get_thumbnailPool_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_013b;
		}
	}
	{
		bool L_2 = __this->get_transitionInProgress_6();
		if (L_2)
		{
			goto IL_013b;
		}
	}
	{
		EveryplayThumbnailPool_t101914191 * L_3 = __this->get_thumbnailPool_2();
		NullCheck(L_3);
		int32_t L_4 = EveryplayThumbnailPool_get_availableThumbnailCount_m4126025113(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0129;
		}
	}
	{
		int32_t L_5 = __this->get_currentIndex_5();
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_0081;
		}
	}
	{
		__this->set_currentIndex_5(0);
		Renderer_t257310565 * L_6 = __this->get_mainRenderer_3();
		NullCheck(L_6);
		Material_t193706927 * L_7 = Renderer_get_material_m2553789785(L_6, /*hidden argument*/NULL);
		EveryplayThumbnailPool_t101914191 * L_8 = __this->get_thumbnailPool_2();
		NullCheck(L_8);
		Vector2_t2243707579  L_9 = EveryplayThumbnailPool_get_thumbnailScale_m1960597216(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_set_mainTextureScale_m723074403(L_7, L_9, /*hidden argument*/NULL);
		Renderer_t257310565 * L_10 = __this->get_mainRenderer_3();
		NullCheck(L_10);
		Material_t193706927 * L_11 = Renderer_get_material_m2553789785(L_10, /*hidden argument*/NULL);
		EveryplayThumbnailPool_t101914191 * L_12 = __this->get_thumbnailPool_2();
		NullCheck(L_12);
		Texture2DU5BU5D_t2724090252* L_13 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(L_12, /*hidden argument*/NULL);
		int32_t L_14 = __this->get_currentIndex_5();
		NullCheck(L_13);
		int32_t L_15 = L_14;
		Texture2D_t3542995729 * L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_11);
		Material_set_mainTexture_m3584203343(L_11, L_16, /*hidden argument*/NULL);
		goto IL_0124;
	}

IL_0081:
	{
		EveryplayThumbnailPool_t101914191 * L_17 = __this->get_thumbnailPool_2();
		NullCheck(L_17);
		int32_t L_18 = EveryplayThumbnailPool_get_availableThumbnailCount_m4126025113(L_17, /*hidden argument*/NULL);
		if ((((int32_t)L_18) <= ((int32_t)1)))
		{
			goto IL_0124;
		}
	}
	{
		int32_t L_19 = Time_get_frameCount_m1198768813(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (((int32_t)((int32_t)L_19%(int32_t)((int32_t)50))))
		{
			goto IL_0124;
		}
	}
	{
		int32_t L_20 = __this->get_currentIndex_5();
		__this->set_currentIndex_5(((int32_t)((int32_t)L_20+(int32_t)1)));
		int32_t L_21 = __this->get_currentIndex_5();
		EveryplayThumbnailPool_t101914191 * L_22 = __this->get_thumbnailPool_2();
		NullCheck(L_22);
		int32_t L_23 = EveryplayThumbnailPool_get_availableThumbnailCount_m4126025113(L_22, /*hidden argument*/NULL);
		if ((((int32_t)L_21) < ((int32_t)L_23)))
		{
			goto IL_00ca;
		}
	}
	{
		__this->set_currentIndex_5(0);
	}

IL_00ca:
	{
		Renderer_t257310565 * L_24 = __this->get_mainRenderer_3();
		NullCheck(L_24);
		Material_t193706927 * L_25 = Renderer_get_material_m2553789785(L_24, /*hidden argument*/NULL);
		EveryplayThumbnailPool_t101914191 * L_26 = __this->get_thumbnailPool_2();
		NullCheck(L_26);
		Vector2_t2243707579  L_27 = EveryplayThumbnailPool_get_thumbnailScale_m1960597216(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Material_SetTextureScale_m1622979841(L_25, _stringLiteral2172956323, L_27, /*hidden argument*/NULL);
		Renderer_t257310565 * L_28 = __this->get_mainRenderer_3();
		NullCheck(L_28);
		Material_t193706927 * L_29 = Renderer_get_material_m2553789785(L_28, /*hidden argument*/NULL);
		EveryplayThumbnailPool_t101914191 * L_30 = __this->get_thumbnailPool_2();
		NullCheck(L_30);
		Texture2DU5BU5D_t2724090252* L_31 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(L_30, /*hidden argument*/NULL);
		int32_t L_32 = __this->get_currentIndex_5();
		NullCheck(L_31);
		int32_t L_33 = L_32;
		Texture2D_t3542995729 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_29);
		Material_SetTexture_m141095205(L_29, _stringLiteral2172956323, L_34, /*hidden argument*/NULL);
		__this->set_transitionInProgress_6((bool)1);
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral2887705779, /*hidden argument*/NULL);
	}

IL_0124:
	{
		goto IL_013b;
	}

IL_0129:
	{
		int32_t L_35 = __this->get_currentIndex_5();
		if ((((int32_t)L_35) < ((int32_t)0)))
		{
			goto IL_013b;
		}
	}
	{
		EveryplayAnimatedThumbnail_ResetThumbnail_m3893133820(__this, /*hidden argument*/NULL);
	}

IL_013b:
	{
		return;
	}
}
// System.Void EveryplayAnimatedThumbnail/<CrossfadeTransition>c__Iterator0::.ctor()
extern "C"  void U3CCrossfadeTransitionU3Ec__Iterator0__ctor_m3234585754 (U3CCrossfadeTransitionU3Ec__Iterator0_t120047739 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean EveryplayAnimatedThumbnail/<CrossfadeTransition>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1022637398;
extern Il2CppCodeGenString* _stringLiteral2172956323;
extern const uint32_t U3CCrossfadeTransitionU3Ec__Iterator0_MoveNext_m2798257218_MetadataUsageId;
extern "C"  bool U3CCrossfadeTransitionU3Ec__Iterator0_MoveNext_m2798257218 (U3CCrossfadeTransitionU3Ec__Iterator0_t120047739 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCrossfadeTransitionU3Ec__Iterator0_MoveNext_m2798257218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0086;
		}
	}
	{
		goto IL_0151;
	}

IL_0021:
	{
		goto IL_0086;
	}

IL_0026:
	{
		EveryplayAnimatedThumbnail_t3478222492 * L_2 = __this->get_U24this_0();
		EveryplayAnimatedThumbnail_t3478222492 * L_3 = L_2;
		NullCheck(L_3);
		float L_4 = L_3->get_blend_7();
		NullCheck(L_3);
		L_3->set_blend_7(((float)((float)L_4+(float)(0.1f))));
		EveryplayAnimatedThumbnail_t3478222492 * L_5 = __this->get_U24this_0();
		NullCheck(L_5);
		Renderer_t257310565 * L_6 = L_5->get_mainRenderer_3();
		NullCheck(L_6);
		Material_t193706927 * L_7 = Renderer_get_material_m2553789785(L_6, /*hidden argument*/NULL);
		EveryplayAnimatedThumbnail_t3478222492 * L_8 = __this->get_U24this_0();
		NullCheck(L_8);
		float L_9 = L_8->get_blend_7();
		NullCheck(L_7);
		Material_SetFloat_m1926275467(L_7, _stringLiteral1022637398, L_9, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_10 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_10, (0.025f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_10);
		bool L_11 = __this->get_U24disposing_2();
		if (L_11)
		{
			goto IL_0081;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0081:
	{
		goto IL_0153;
	}

IL_0086:
	{
		EveryplayAnimatedThumbnail_t3478222492 * L_12 = __this->get_U24this_0();
		NullCheck(L_12);
		float L_13 = L_12->get_blend_7();
		if ((!(((float)L_13) < ((float)(1.0f)))))
		{
			goto IL_00ab;
		}
	}
	{
		EveryplayAnimatedThumbnail_t3478222492 * L_14 = __this->get_U24this_0();
		NullCheck(L_14);
		bool L_15 = L_14->get_transitionInProgress_6();
		if (L_15)
		{
			goto IL_0026;
		}
	}

IL_00ab:
	{
		EveryplayAnimatedThumbnail_t3478222492 * L_16 = __this->get_U24this_0();
		NullCheck(L_16);
		Renderer_t257310565 * L_17 = L_16->get_mainRenderer_3();
		NullCheck(L_17);
		Material_t193706927 * L_18 = Renderer_get_material_m2553789785(L_17, /*hidden argument*/NULL);
		EveryplayAnimatedThumbnail_t3478222492 * L_19 = __this->get_U24this_0();
		NullCheck(L_19);
		Renderer_t257310565 * L_20 = L_19->get_mainRenderer_3();
		NullCheck(L_20);
		Material_t193706927 * L_21 = Renderer_get_material_m2553789785(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Texture_t2243626319 * L_22 = Material_GetTexture_m1257877102(L_21, _stringLiteral2172956323, /*hidden argument*/NULL);
		NullCheck(L_18);
		Material_set_mainTexture_m3584203343(L_18, L_22, /*hidden argument*/NULL);
		EveryplayAnimatedThumbnail_t3478222492 * L_23 = __this->get_U24this_0();
		NullCheck(L_23);
		Renderer_t257310565 * L_24 = L_23->get_mainRenderer_3();
		NullCheck(L_24);
		Material_t193706927 * L_25 = Renderer_get_material_m2553789785(L_24, /*hidden argument*/NULL);
		EveryplayAnimatedThumbnail_t3478222492 * L_26 = __this->get_U24this_0();
		NullCheck(L_26);
		Renderer_t257310565 * L_27 = L_26->get_mainRenderer_3();
		NullCheck(L_27);
		Material_t193706927 * L_28 = Renderer_get_material_m2553789785(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector2_t2243707579  L_29 = Material_GetTextureScale_m1359469258(L_28, _stringLiteral2172956323, /*hidden argument*/NULL);
		NullCheck(L_25);
		Material_set_mainTextureScale_m723074403(L_25, L_29, /*hidden argument*/NULL);
		EveryplayAnimatedThumbnail_t3478222492 * L_30 = __this->get_U24this_0();
		NullCheck(L_30);
		L_30->set_blend_7((0.0f));
		EveryplayAnimatedThumbnail_t3478222492 * L_31 = __this->get_U24this_0();
		NullCheck(L_31);
		Renderer_t257310565 * L_32 = L_31->get_mainRenderer_3();
		NullCheck(L_32);
		Material_t193706927 * L_33 = Renderer_get_material_m2553789785(L_32, /*hidden argument*/NULL);
		EveryplayAnimatedThumbnail_t3478222492 * L_34 = __this->get_U24this_0();
		NullCheck(L_34);
		float L_35 = L_34->get_blend_7();
		NullCheck(L_33);
		Material_SetFloat_m1926275467(L_33, _stringLiteral1022637398, L_35, /*hidden argument*/NULL);
		EveryplayAnimatedThumbnail_t3478222492 * L_36 = __this->get_U24this_0();
		NullCheck(L_36);
		L_36->set_transitionInProgress_6((bool)0);
		__this->set_U24PC_3((-1));
	}

IL_0151:
	{
		return (bool)0;
	}

IL_0153:
	{
		return (bool)1;
	}
}
// System.Object EveryplayAnimatedThumbnail/<CrossfadeTransition>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCrossfadeTransitionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1374832118 (U3CCrossfadeTransitionU3Ec__Iterator0_t120047739 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object EveryplayAnimatedThumbnail/<CrossfadeTransition>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCrossfadeTransitionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m298300158 (U3CCrossfadeTransitionU3Ec__Iterator0_t120047739 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void EveryplayAnimatedThumbnail/<CrossfadeTransition>c__Iterator0::Dispose()
extern "C"  void U3CCrossfadeTransitionU3Ec__Iterator0_Dispose_m3557423431 (U3CCrossfadeTransitionU3Ec__Iterator0_t120047739 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void EveryplayAnimatedThumbnail/<CrossfadeTransition>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCrossfadeTransitionU3Ec__Iterator0_Reset_m662167969_MetadataUsageId;
extern "C"  void U3CCrossfadeTransitionU3Ec__Iterator0_Reset_m662167969 (U3CCrossfadeTransitionU3Ec__Iterator0_t120047739 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCrossfadeTransitionU3Ec__Iterator0_Reset_m662167969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI::.ctor()
extern "C"  void EveryplayAnimatedThumbnailOnGUI__ctor_m4073471057 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method)
{
	{
		Rect_t3681755626  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m1220545469(&L_0, (10.0f), (10.0f), (256.0f), (196.0f), /*hidden argument*/NULL);
		__this->set_pixelInset_3(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI::Awake()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_Awake_m972992546 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method)
{
	{
		Texture_t2243626319 * L_0 = __this->get_defaultTexture_2();
		__this->set_bottomTexture_8(L_0);
		return;
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI::Start()
extern const Il2CppType* EveryplayThumbnailPool_t101914191_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* EveryplayThumbnailPool_t101914191_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3241710594;
extern const uint32_t EveryplayAnimatedThumbnailOnGUI_Start_m570778561_MetadataUsageId;
extern "C"  void EveryplayAnimatedThumbnailOnGUI_Start_m570778561 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayAnimatedThumbnailOnGUI_Start_m570778561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(EveryplayThumbnailPool_t101914191_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_1 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_thumbnailPool_4(((EveryplayThumbnailPool_t101914191 *)CastclassClass(L_1, EveryplayThumbnailPool_t101914191_il2cpp_TypeInfo_var)));
		EveryplayThumbnailPool_t101914191 * L_2 = __this->get_thumbnailPool_4();
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		EveryplayAnimatedThumbnailOnGUI_ResetThumbnail_m494468424(__this, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3241710594, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI::OnDestroy()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_OnDestroy_m3821570548 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method)
{
	{
		EveryplayAnimatedThumbnailOnGUI_StopTransitions_m2114846637(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI::OnDisable()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_OnDisable_m2671812778 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method)
{
	{
		EveryplayAnimatedThumbnailOnGUI_StopTransitions_m2114846637(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI::ResetThumbnail()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_ResetThumbnail_m494468424 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method)
{
	{
		__this->set_currentIndex_5((-1));
		EveryplayAnimatedThumbnailOnGUI_StopTransitions_m2114846637(__this, /*hidden argument*/NULL);
		__this->set_blend_7((0.0f));
		Vector2_t2243707579  L_0 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_bottomTextureScale_9(L_0);
		Texture_t2243626319 * L_1 = __this->get_defaultTexture_2();
		__this->set_bottomTexture_8(L_1);
		return;
	}
}
// System.Collections.IEnumerator EveryplayAnimatedThumbnailOnGUI::CrossfadeTransition()
extern Il2CppClass* U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayAnimatedThumbnailOnGUI_CrossfadeTransition_m2248282768_MetadataUsageId;
extern "C"  Il2CppObject * EveryplayAnimatedThumbnailOnGUI_CrossfadeTransition_m2248282768 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayAnimatedThumbnailOnGUI_CrossfadeTransition_m2248282768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * V_0 = NULL;
	{
		U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * L_0 = (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 *)il2cpp_codegen_object_new(U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193_il2cpp_TypeInfo_var);
		U3CCrossfadeTransitionU3Ec__Iterator0__ctor_m2088035682(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * L_2 = V_0;
		return L_2;
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI::StopTransitions()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_StopTransitions_m2114846637 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method)
{
	{
		__this->set_transitionInProgress_6((bool)0);
		MonoBehaviour_StopAllCoroutines_m1675795839(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI::Update()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2887705779;
extern const uint32_t EveryplayAnimatedThumbnailOnGUI_Update_m1307573924_MetadataUsageId;
extern "C"  void EveryplayAnimatedThumbnailOnGUI_Update_m1307573924 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayAnimatedThumbnailOnGUI_Update_m1307573924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EveryplayThumbnailPool_t101914191 * L_0 = __this->get_thumbnailPool_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0109;
		}
	}
	{
		bool L_2 = __this->get_transitionInProgress_6();
		if (L_2)
		{
			goto IL_0109;
		}
	}
	{
		EveryplayThumbnailPool_t101914191 * L_3 = __this->get_thumbnailPool_4();
		NullCheck(L_3);
		int32_t L_4 = EveryplayThumbnailPool_get_availableThumbnailCount_m4126025113(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_00f7;
		}
	}
	{
		int32_t L_5 = __this->get_currentIndex_5();
		if ((((int32_t)L_5) >= ((int32_t)0)))
		{
			goto IL_006d;
		}
	}
	{
		__this->set_currentIndex_5(0);
		EveryplayThumbnailPool_t101914191 * L_6 = __this->get_thumbnailPool_4();
		NullCheck(L_6);
		Vector2_t2243707579  L_7 = EveryplayThumbnailPool_get_thumbnailScale_m1960597216(L_6, /*hidden argument*/NULL);
		__this->set_bottomTextureScale_9(L_7);
		EveryplayThumbnailPool_t101914191 * L_8 = __this->get_thumbnailPool_4();
		NullCheck(L_8);
		Texture2DU5BU5D_t2724090252* L_9 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(L_8, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_currentIndex_5();
		NullCheck(L_9);
		int32_t L_11 = L_10;
		Texture2D_t3542995729 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		__this->set_bottomTexture_8(L_12);
		goto IL_00f2;
	}

IL_006d:
	{
		EveryplayThumbnailPool_t101914191 * L_13 = __this->get_thumbnailPool_4();
		NullCheck(L_13);
		int32_t L_14 = EveryplayThumbnailPool_get_availableThumbnailCount_m4126025113(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_14) <= ((int32_t)1)))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_15 = Time_get_frameCount_m1198768813(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (((int32_t)((int32_t)L_15%(int32_t)((int32_t)50))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_16 = __this->get_currentIndex_5();
		__this->set_currentIndex_5(((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = __this->get_currentIndex_5();
		EveryplayThumbnailPool_t101914191 * L_18 = __this->get_thumbnailPool_4();
		NullCheck(L_18);
		int32_t L_19 = EveryplayThumbnailPool_get_availableThumbnailCount_m4126025113(L_18, /*hidden argument*/NULL);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_00b6;
		}
	}
	{
		__this->set_currentIndex_5(0);
	}

IL_00b6:
	{
		EveryplayThumbnailPool_t101914191 * L_20 = __this->get_thumbnailPool_4();
		NullCheck(L_20);
		Vector2_t2243707579  L_21 = EveryplayThumbnailPool_get_thumbnailScale_m1960597216(L_20, /*hidden argument*/NULL);
		__this->set_topTextureScale_10(L_21);
		EveryplayThumbnailPool_t101914191 * L_22 = __this->get_thumbnailPool_4();
		NullCheck(L_22);
		Texture2DU5BU5D_t2724090252* L_23 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(L_22, /*hidden argument*/NULL);
		int32_t L_24 = __this->get_currentIndex_5();
		NullCheck(L_23);
		int32_t L_25 = L_24;
		Texture2D_t3542995729 * L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		__this->set_topTexture_11(L_26);
		__this->set_transitionInProgress_6((bool)1);
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral2887705779, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		goto IL_0109;
	}

IL_00f7:
	{
		int32_t L_27 = __this->get_currentIndex_5();
		if ((((int32_t)L_27) < ((int32_t)0)))
		{
			goto IL_0109;
		}
	}
	{
		EveryplayAnimatedThumbnailOnGUI_ResetThumbnail_m494468424(__this, /*hidden argument*/NULL);
	}

IL_0109:
	{
		return;
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI::OnGUI()
extern Il2CppClass* EventType_t3919834026_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayAnimatedThumbnailOnGUI_OnGUI_m2082476659_MetadataUsageId;
extern "C"  void EveryplayAnimatedThumbnailOnGUI_OnGUI_m2082476659 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayAnimatedThumbnailOnGUI_OnGUI_m2082476659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Event_t3028476042 * L_0 = Event_get_current_m2901774193(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m2426033198(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ((int32_t)7);
		Il2CppObject * L_3 = Box(EventType_t3919834026_il2cpp_TypeInfo_var, &L_2);
		Il2CppObject * L_4 = Box(EventType_t3919834026_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_4, L_3);
		if (!L_5)
		{
			goto IL_0146;
		}
	}
	{
		Texture_t2243626319 * L_6 = __this->get_bottomTexture_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0094;
		}
	}
	{
		Rect_t3681755626 * L_8 = __this->get_address_of_pixelInset_3();
		float L_9 = Rect_get_x_m1393582490(L_8, /*hidden argument*/NULL);
		Rect_t3681755626 * L_10 = __this->get_address_of_pixelInset_3();
		float L_11 = Rect_get_y_m1393582395(L_10, /*hidden argument*/NULL);
		Rect_t3681755626 * L_12 = __this->get_address_of_pixelInset_3();
		float L_13 = Rect_get_width_m1138015702(L_12, /*hidden argument*/NULL);
		Rect_t3681755626 * L_14 = __this->get_address_of_pixelInset_3();
		float L_15 = Rect_get_height_m3128694305(L_14, /*hidden argument*/NULL);
		Rect_t3681755626  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Rect__ctor_m1220545469(&L_16, L_9, L_11, L_13, L_15, /*hidden argument*/NULL);
		Texture_t2243626319 * L_17 = __this->get_bottomTexture_8();
		Vector2_t2243707579 * L_18 = __this->get_address_of_bottomTextureScale_9();
		float L_19 = L_18->get_x_0();
		Vector2_t2243707579 * L_20 = __this->get_address_of_bottomTextureScale_9();
		float L_21 = L_20->get_y_1();
		Rect_t3681755626  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Rect__ctor_m1220545469(&L_22, (0.0f), (0.0f), L_19, L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_DrawTextureWithTexCoords_m621549064(NULL /*static, unused*/, L_16, L_17, L_22, /*hidden argument*/NULL);
	}

IL_0094:
	{
		Texture_t2243626319 * L_23 = __this->get_topTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0146;
		}
	}
	{
		float L_25 = __this->get_blend_7();
		if ((!(((float)L_25) > ((float)(0.0f)))))
		{
			goto IL_0146;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Color_t2020392075  L_26 = GUI_get_color_m1234367343(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_26;
		float L_27 = (&V_1)->get_r_0();
		float L_28 = (&V_1)->get_g_1();
		float L_29 = (&V_1)->get_b_2();
		float L_30 = __this->get_blend_7();
		Color_t2020392075  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Color__ctor_m1909920690(&L_31, L_27, L_28, L_29, L_30, /*hidden argument*/NULL);
		GUI_set_color_m3547334264(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		Rect_t3681755626 * L_32 = __this->get_address_of_pixelInset_3();
		float L_33 = Rect_get_x_m1393582490(L_32, /*hidden argument*/NULL);
		Rect_t3681755626 * L_34 = __this->get_address_of_pixelInset_3();
		float L_35 = Rect_get_y_m1393582395(L_34, /*hidden argument*/NULL);
		Rect_t3681755626 * L_36 = __this->get_address_of_pixelInset_3();
		float L_37 = Rect_get_width_m1138015702(L_36, /*hidden argument*/NULL);
		Rect_t3681755626 * L_38 = __this->get_address_of_pixelInset_3();
		float L_39 = Rect_get_height_m3128694305(L_38, /*hidden argument*/NULL);
		Rect_t3681755626  L_40;
		memset(&L_40, 0, sizeof(L_40));
		Rect__ctor_m1220545469(&L_40, L_33, L_35, L_37, L_39, /*hidden argument*/NULL);
		Texture_t2243626319 * L_41 = __this->get_topTexture_11();
		Vector2_t2243707579 * L_42 = __this->get_address_of_topTextureScale_10();
		float L_43 = L_42->get_x_0();
		Vector2_t2243707579 * L_44 = __this->get_address_of_topTextureScale_10();
		float L_45 = L_44->get_y_1();
		Rect_t3681755626  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Rect__ctor_m1220545469(&L_46, (0.0f), (0.0f), L_43, L_45, /*hidden argument*/NULL);
		GUI_DrawTextureWithTexCoords_m621549064(NULL /*static, unused*/, L_40, L_41, L_46, /*hidden argument*/NULL);
		Color_t2020392075  L_47 = V_1;
		GUI_set_color_m3547334264(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
	}

IL_0146:
	{
		return;
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::.ctor()
extern "C"  void U3CCrossfadeTransitionU3Ec__Iterator0__ctor_m2088035682 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CCrossfadeTransitionU3Ec__Iterator0_MoveNext_m1072767658_MetadataUsageId;
extern "C"  bool U3CCrossfadeTransitionU3Ec__Iterator0_MoveNext_m1072767658 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCrossfadeTransitionU3Ec__Iterator0_MoveNext_m1072767658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0061;
		}
	}
	{
		goto IL_00d5;
	}

IL_0021:
	{
		goto IL_0061;
	}

IL_0026:
	{
		EveryplayAnimatedThumbnailOnGUI_t1420574672 * L_2 = __this->get_U24this_0();
		EveryplayAnimatedThumbnailOnGUI_t1420574672 * L_3 = L_2;
		NullCheck(L_3);
		float L_4 = L_3->get_blend_7();
		NullCheck(L_3);
		L_3->set_blend_7(((float)((float)L_4+(float)(0.1f))));
		WaitForSeconds_t3839502067 * L_5 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_5, (0.025f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_5);
		bool L_6 = __this->get_U24disposing_2();
		if (L_6)
		{
			goto IL_005c;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_005c:
	{
		goto IL_00d7;
	}

IL_0061:
	{
		EveryplayAnimatedThumbnailOnGUI_t1420574672 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		float L_8 = L_7->get_blend_7();
		if ((!(((float)L_8) < ((float)(1.0f)))))
		{
			goto IL_0086;
		}
	}
	{
		EveryplayAnimatedThumbnailOnGUI_t1420574672 * L_9 = __this->get_U24this_0();
		NullCheck(L_9);
		bool L_10 = L_9->get_transitionInProgress_6();
		if (L_10)
		{
			goto IL_0026;
		}
	}

IL_0086:
	{
		EveryplayAnimatedThumbnailOnGUI_t1420574672 * L_11 = __this->get_U24this_0();
		EveryplayAnimatedThumbnailOnGUI_t1420574672 * L_12 = __this->get_U24this_0();
		NullCheck(L_12);
		Texture_t2243626319 * L_13 = L_12->get_topTexture_11();
		NullCheck(L_11);
		L_11->set_bottomTexture_8(L_13);
		EveryplayAnimatedThumbnailOnGUI_t1420574672 * L_14 = __this->get_U24this_0();
		EveryplayAnimatedThumbnailOnGUI_t1420574672 * L_15 = __this->get_U24this_0();
		NullCheck(L_15);
		Vector2_t2243707579  L_16 = L_15->get_topTextureScale_10();
		NullCheck(L_14);
		L_14->set_bottomTextureScale_9(L_16);
		EveryplayAnimatedThumbnailOnGUI_t1420574672 * L_17 = __this->get_U24this_0();
		NullCheck(L_17);
		L_17->set_blend_7((0.0f));
		EveryplayAnimatedThumbnailOnGUI_t1420574672 * L_18 = __this->get_U24this_0();
		NullCheck(L_18);
		L_18->set_transitionInProgress_6((bool)0);
		__this->set_U24PC_3((-1));
	}

IL_00d5:
	{
		return (bool)0;
	}

IL_00d7:
	{
		return (bool)1;
	}
}
// System.Object EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCrossfadeTransitionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2847969304 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCrossfadeTransitionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2048901056 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::Dispose()
extern "C"  void U3CCrossfadeTransitionU3Ec__Iterator0_Dispose_m573461969 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void EveryplayAnimatedThumbnailOnGUI/<CrossfadeTransition>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCrossfadeTransitionU3Ec__Iterator0_Reset_m249625775_MetadataUsageId;
extern "C"  void U3CCrossfadeTransitionU3Ec__Iterator0_Reset_m249625775 (U3CCrossfadeTransitionU3Ec__Iterator0_t4284962193 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCrossfadeTransitionU3Ec__Iterator0_Reset_m249625775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> EveryplayDictionaryExtensions::JsonToDictionary(System.String)
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayDictionaryExtensions_JsonToDictionary_m1861006415_MetadataUsageId;
extern "C"  Dictionary_2_t309261261 * EveryplayDictionaryExtensions_JsonToDictionary_m1861006415 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayDictionaryExtensions_JsonToDictionary_m1861006415_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___json0;
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		String_t* L_1 = ___json0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_001e;
		}
	}
	{
		String_t* L_3 = ___json0;
		Il2CppObject * L_4 = Json_Deserialize_m2304917621(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return ((Dictionary_2_t309261261 *)IsInstClass(L_4, Dictionary_2_t309261261_il2cpp_TypeInfo_var));
	}

IL_001e:
	{
		return (Dictionary_2_t309261261 *)NULL;
	}
}
// System.Void EveryplayEarlyInitializer::.ctor()
extern "C"  void EveryplayEarlyInitializer__ctor_m1892662465 (EveryplayEarlyInitializer_t4267781536 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayEarlyInitializer::InitializeEveryplayOnStartup()
extern Il2CppClass* EveryplaySettings_t83776108_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral811854626;
extern const uint32_t EveryplayEarlyInitializer_InitializeEveryplayOnStartup_m1465447962_MetadataUsageId;
extern "C"  void EveryplayEarlyInitializer_InitializeEveryplayOnStartup_m1465447962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayEarlyInitializer_InitializeEveryplayOnStartup_m1465447962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EveryplaySettings_t83776108 * V_0 = NULL;
	{
		Object_t1021602117 * L_0 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral811854626, /*hidden argument*/NULL);
		V_0 = ((EveryplaySettings_t83776108 *)CastclassClass(L_0, EveryplaySettings_t83776108_il2cpp_TypeInfo_var));
		EveryplaySettings_t83776108 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0042;
		}
	}
	{
		EveryplaySettings_t83776108 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = L_3->get_earlyInitializerEnabled_10();
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		EveryplaySettings_t83776108 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = EveryplaySettings_get_IsEnabled_m4208345999(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		EveryplaySettings_t83776108 * L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = EveryplaySettings_get_IsValid_m586503010(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_Initialize_m1735770310(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void EveryplayFaceCamSettings::.ctor()
extern "C"  void EveryplayFaceCamSettings__ctor_m3661674959 (EveryplayFaceCamSettings_t438190306 * __this, const MethodInfo* method)
{
	{
		__this->set_previewVisible_2((bool)1);
		__this->set_iPhonePreviewSideWidth_3(((int32_t)64));
		__this->set_iPhonePreviewPositionX_4(((int32_t)16));
		__this->set_iPhonePreviewPositionY_5(((int32_t)16));
		__this->set_iPhonePreviewBorderWidth_6(2);
		__this->set_iPadPreviewSideWidth_7(((int32_t)96));
		__this->set_iPadPreviewPositionX_8(((int32_t)24));
		__this->set_iPadPreviewPositionY_9(((int32_t)24));
		__this->set_iPadPreviewBorderWidth_10(2);
		Color_t2020392075  L_0 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_previewBorderColor_11(L_0);
		__this->set_previewOrigin_12(3);
		__this->set_previewScaleRetina_13((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayFaceCamSettings::Start()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayFaceCamSettings_Start_m1854962875_MetadataUsageId;
extern "C"  void EveryplayFaceCamSettings_Start_m1854962875 (EveryplayFaceCamSettings_t438190306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayFaceCamSettings_Start_m1854962875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		int32_t L_0 = Everyplay_GetUserInterfaceIdiom_m2004127790(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_1 = __this->get_iPadPreviewSideWidth_7();
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_FaceCamSetPreviewSideWidth_m2582819180(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_iPadPreviewBorderWidth_10();
		Everyplay_FaceCamSetPreviewBorderWidth_m4144496013(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_iPadPreviewPositionX_8();
		Everyplay_FaceCamSetPreviewPositionX_m1625010144(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_iPadPreviewPositionY_9();
		Everyplay_FaceCamSetPreviewPositionY_m906599039(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0068;
	}

IL_003c:
	{
		int32_t L_5 = __this->get_iPhonePreviewSideWidth_3();
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_FaceCamSetPreviewSideWidth_m2582819180(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_iPhonePreviewBorderWidth_6();
		Everyplay_FaceCamSetPreviewBorderWidth_m4144496013(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_iPhonePreviewPositionX_4();
		Everyplay_FaceCamSetPreviewPositionX_m1625010144(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		int32_t L_8 = __this->get_iPhonePreviewPositionY_5();
		Everyplay_FaceCamSetPreviewPositionY_m906599039(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0068:
	{
		Color_t2020392075 * L_9 = __this->get_address_of_previewBorderColor_11();
		float L_10 = L_9->get_r_0();
		Color_t2020392075 * L_11 = __this->get_address_of_previewBorderColor_11();
		float L_12 = L_11->get_g_1();
		Color_t2020392075 * L_13 = __this->get_address_of_previewBorderColor_11();
		float L_14 = L_13->get_b_2();
		Color_t2020392075 * L_15 = __this->get_address_of_previewBorderColor_11();
		float L_16 = L_15->get_a_3();
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_FaceCamSetPreviewBorderColor_m2502318011(NULL /*static, unused*/, L_10, L_12, L_14, L_16, /*hidden argument*/NULL);
		int32_t L_17 = __this->get_previewOrigin_12();
		Everyplay_FaceCamSetPreviewOrigin_m1150375596(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		bool L_18 = __this->get_previewScaleRetina_13();
		Everyplay_FaceCamSetPreviewScaleRetina_m564042752(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		bool L_19 = __this->get_previewVisible_2();
		Everyplay_FaceCamSetPreviewVisible_m3505686335(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		bool L_20 = __this->get_audioOnly_14();
		Everyplay_FaceCamSetAudioOnly_m4118761357(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayFaceCamTest::.ctor()
extern "C"  void EveryplayFaceCamTest__ctor_m1308166180 (EveryplayFaceCamTest_t2314249671 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayFaceCamTest::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayFaceCamTest_Awake_m674056825_MetadataUsageId;
extern "C"  void EveryplayFaceCamTest_Awake_m674056825 (EveryplayFaceCamTest_t2314249671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayFaceCamTest_Awake_m674056825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayFaceCamTest::Start()
extern Il2CppClass* FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayFaceCamTest_CheckFaceCamRecordingPermission_m221574661_MethodInfo_var;
extern const uint32_t EveryplayFaceCamTest_Start_m2030955940_MetadataUsageId;
extern "C"  void EveryplayFaceCamTest_Start_m2030955940 (EveryplayFaceCamTest_t2314249671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayFaceCamTest_Start_m2030955940_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)EveryplayFaceCamTest_CheckFaceCamRecordingPermission_m221574661_MethodInfo_var);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_1 = (FaceCamRecordingPermissionDelegate_t1670731619 *)il2cpp_codegen_object_new(FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var);
		FaceCamRecordingPermissionDelegate__ctor_m539527746(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_add_FaceCamRecordingPermission_m574143351(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayFaceCamTest::Destroy()
extern Il2CppClass* FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayFaceCamTest_CheckFaceCamRecordingPermission_m221574661_MethodInfo_var;
extern const uint32_t EveryplayFaceCamTest_Destroy_m1427295446_MetadataUsageId;
extern "C"  void EveryplayFaceCamTest_Destroy_m1427295446 (EveryplayFaceCamTest_t2314249671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayFaceCamTest_Destroy_m1427295446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)EveryplayFaceCamTest_CheckFaceCamRecordingPermission_m221574661_MethodInfo_var);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_1 = (FaceCamRecordingPermissionDelegate_t1670731619 *)il2cpp_codegen_object_new(FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var);
		FaceCamRecordingPermissionDelegate__ctor_m539527746(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_remove_FaceCamRecordingPermission_m4283361868(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayFaceCamTest::CheckFaceCamRecordingPermission(System.Boolean)
extern const Il2CppType* GUIText_t2411476300_0_0_0_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGUIText_t2411476300_m3222579698_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1989962612;
extern Il2CppCodeGenString* _stringLiteral1805968459;
extern const uint32_t EveryplayFaceCamTest_CheckFaceCamRecordingPermission_m221574661_MetadataUsageId;
extern "C"  void EveryplayFaceCamTest_CheckFaceCamRecordingPermission_m221574661 (EveryplayFaceCamTest_t2314249671 * __this, bool ___granted0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayFaceCamTest_CheckFaceCamRecordingPermission_m221574661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GUIText_t2411476300 * V_0 = NULL;
	{
		bool L_0 = ___granted0;
		__this->set_recordingPermissionGranted_2(L_0);
		bool L_1 = ___granted0;
		if (L_1)
		{
			goto IL_00a5;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_debugMessage_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_00a5;
		}
	}
	{
		TypeU5BU5D_t1664964607* L_4 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(GUIText_t2411476300_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		GameObject_t1756533147 * L_6 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m1633632305(L_6, _stringLiteral1989962612, L_4, /*hidden argument*/NULL);
		__this->set_debugMessage_3(L_6);
		GameObject_t1756533147 * L_7 = __this->get_debugMessage_3();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, (0.5f), (0.5f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_position_m2469242620(L_8, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = __this->get_debugMessage_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00a5;
		}
	}
	{
		GameObject_t1756533147 * L_12 = __this->get_debugMessage_3();
		NullCheck(L_12);
		GUIText_t2411476300 * L_13 = GameObject_GetComponent_TisGUIText_t2411476300_m3222579698(L_12, /*hidden argument*/GameObject_GetComponent_TisGUIText_t2411476300_m3222579698_MethodInfo_var);
		V_0 = L_13;
		GUIText_t2411476300 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00a5;
		}
	}
	{
		GUIText_t2411476300 * L_16 = V_0;
		NullCheck(L_16);
		GUIText_set_text_m3758377277(L_16, _stringLiteral1805968459, /*hidden argument*/NULL);
		GUIText_t2411476300 * L_17 = V_0;
		NullCheck(L_17);
		GUIText_set_alignment_m2615389432(L_17, 1, /*hidden argument*/NULL);
		GUIText_t2411476300 * L_18 = V_0;
		NullCheck(L_18);
		GUIText_set_anchor_m2234105042(L_18, 4, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		return;
	}
}
// System.Void EveryplayFaceCamTest::OnGUI()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3789345094;
extern Il2CppCodeGenString* _stringLiteral2314668778;
extern Il2CppCodeGenString* _stringLiteral3128382796;
extern const uint32_t EveryplayFaceCamTest_OnGUI_m1118772452_MetadataUsageId;
extern "C"  void EveryplayFaceCamTest_OnGUI_m1118772452 (EveryplayFaceCamTest_t2314249671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayFaceCamTest_OnGUI_m1118772452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t3681755626  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	Rect_t3681755626  G_B2_0;
	memset(&G_B2_0, 0, sizeof(G_B2_0));
	String_t* G_B4_0 = NULL;
	Rect_t3681755626  G_B4_1;
	memset(&G_B4_1, 0, sizeof(G_B4_1));
	{
		bool L_0 = __this->get_recordingPermissionGranted_2();
		if (!L_0)
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m1220545469(&L_2, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_1-(int32_t)((int32_t)10)))-(int32_t)((int32_t)158)))))), (10.0f), (158.0f), (48.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_3 = Everyplay_FaceCamIsSessionRunning_m3953733397(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B2_0 = L_2;
		if (!L_3)
		{
			G_B3_0 = L_2;
			goto IL_0042;
		}
	}
	{
		G_B4_0 = _stringLiteral3789345094;
		G_B4_1 = G_B2_0;
		goto IL_0047;
	}

IL_0042:
	{
		G_B4_0 = _stringLiteral2314668778;
		G_B4_1 = G_B3_0;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_4 = GUI_Button_m3054448581(NULL /*static, unused*/, G_B4_1, G_B4_0, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_5 = Everyplay_FaceCamIsSessionRunning_m3953733397(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0065;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_FaceCamStopSession_m1826517360(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_0065:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_FaceCamStartSession_m2600985758(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_006a:
	{
		goto IL_00a6;
	}

IL_006f:
	{
		int32_t L_6 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Rect__ctor_m1220545469(&L_7, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)((int32_t)10)))-(int32_t)((int32_t)158)))))), (10.0f), (158.0f), (48.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_8 = GUI_Button_m3054448581(NULL /*static, unused*/, L_7, _stringLiteral3128382796, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00a6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_FaceCamRequestRecordingPermission_m3770143701(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		return;
	}
}
// System.Void EveryplayHudCamera::.ctor()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayHudCamera__ctor_m1447002134_MetadataUsageId;
extern "C"  void EveryplayHudCamera__ctor_m1447002134 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayHudCamera__ctor_m1447002134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_renderEventPtr_5(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayHudCamera::Awake()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3920523765;
extern Il2CppCodeGenString* _stringLiteral3111071515;
extern const uint32_t EveryplayHudCamera_Awake_m3859285561_MetadataUsageId;
extern "C"  void EveryplayHudCamera_Awake_m3859285561 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayHudCamera_Awake_m3859285561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EveryplayHudCamera_t847528103 * G_B4_0 = NULL;
	EveryplayHudCamera_t847528103 * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	EveryplayHudCamera_t847528103 * G_B5_1 = NULL;
	{
		EveryplayHudCamera_Subscribe_m1644800095(__this, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_0 = Everyplay_IsReadyForRecording_m4208154795(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_readyForRecording_4(L_0);
		bool L_1 = __this->get_readyForRecording_4();
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		IntPtr_t L_2 = EveryplayHudCamera_EveryplayGetUnityRenderEventPtr_m59318425(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_renderEventPtr_5(L_2);
	}

IL_0028:
	{
		String_t* L_3 = SystemInfo_get_graphicsDeviceVersion_m2838077281(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = String_Contains_m4017059963(L_3, _stringLiteral3920523765, /*hidden argument*/NULL);
		G_B3_0 = __this;
		if (!L_4)
		{
			G_B4_0 = __this;
			goto IL_0051;
		}
	}
	{
		String_t* L_5 = SystemInfo_get_graphicsDeviceVersion_m2838077281(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = String_Contains_m4017059963(L_5, _stringLiteral3111071515, /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		G_B5_1 = G_B3_0;
		goto IL_0052;
	}

IL_0051:
	{
		G_B5_0 = 0;
		G_B5_1 = G_B4_0;
	}

IL_0052:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_isMetalDevice_6((bool)G_B5_0);
		int32_t L_7 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_isAndroidDevice_7((bool)((((int32_t)L_7) == ((int32_t)((int32_t)11)))? 1 : 0));
		return;
	}
}
// System.Void EveryplayHudCamera::OnDestroy()
extern "C"  void EveryplayHudCamera_OnDestroy_m3602730979 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method)
{
	{
		EveryplayHudCamera_Subscribe_m1644800095(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayHudCamera::OnEnable()
extern "C"  void EveryplayHudCamera_OnEnable_m2236775166 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method)
{
	{
		EveryplayHudCamera_Subscribe_m1644800095(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayHudCamera::OnDisable()
extern "C"  void EveryplayHudCamera_OnDisable_m1865417241 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method)
{
	{
		EveryplayHudCamera_Subscribe_m1644800095(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayHudCamera::Subscribe(System.Boolean)
extern Il2CppClass* ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayHudCamera_ReadyForRecording_m4131802134_MethodInfo_var;
extern const uint32_t EveryplayHudCamera_Subscribe_m1644800095_MetadataUsageId;
extern "C"  void EveryplayHudCamera_Subscribe_m1644800095 (EveryplayHudCamera_t847528103 * __this, bool ___subscribe0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayHudCamera_Subscribe_m1644800095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_subscribed_3();
		if (L_0)
		{
			goto IL_0027;
		}
	}
	{
		bool L_1 = ___subscribe0;
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)EveryplayHudCamera_ReadyForRecording_m4131802134_MethodInfo_var);
		ReadyForRecordingDelegate_t1593758596 * L_3 = (ReadyForRecordingDelegate_t1593758596 *)il2cpp_codegen_object_new(ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var);
		ReadyForRecordingDelegate__ctor_m4085498349(L_3, __this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_add_ReadyForRecording_m3347212935(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_0049;
	}

IL_0027:
	{
		bool L_4 = __this->get_subscribed_3();
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		bool L_5 = ___subscribe0;
		if (L_5)
		{
			goto IL_0049;
		}
	}
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)EveryplayHudCamera_ReadyForRecording_m4131802134_MethodInfo_var);
		ReadyForRecordingDelegate_t1593758596 * L_7 = (ReadyForRecordingDelegate_t1593758596 *)il2cpp_codegen_object_new(ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var);
		ReadyForRecordingDelegate__ctor_m4085498349(L_7, __this, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_remove_ReadyForRecording_m557751688(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0049:
	{
		bool L_8 = ___subscribe0;
		__this->set_subscribed_3(L_8);
		return;
	}
}
// System.Void EveryplayHudCamera::ReadyForRecording(System.Boolean)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayHudCamera_ReadyForRecording_m4131802134_MetadataUsageId;
extern "C"  void EveryplayHudCamera_ReadyForRecording_m4131802134 (EveryplayHudCamera_t847528103 * __this, bool ___ready0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayHudCamera_ReadyForRecording_m4131802134_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___ready0;
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		IntPtr_t L_1 = __this->get_renderEventPtr_5();
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		IntPtr_t L_4 = EveryplayHudCamera_EveryplayGetUnityRenderEventPtr_m59318425(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_renderEventPtr_5(L_4);
	}

IL_0026:
	{
		bool L_5 = ___ready0;
		__this->set_readyForRecording_4(L_5);
		return;
	}
}
// System.Void EveryplayHudCamera::OnPreRender()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayHudCamera_OnPreRender_m2946483768_MetadataUsageId;
extern "C"  void EveryplayHudCamera_OnPreRender_m2946483768 (EveryplayHudCamera_t847528103 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayHudCamera_OnPreRender_m2946483768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_readyForRecording_4();
		if (!L_0)
		{
			goto IL_0051;
		}
	}
	{
		IntPtr_t L_1 = __this->get_renderEventPtr_5();
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0051;
		}
	}
	{
		bool L_4 = __this->get_isMetalDevice_6();
		if (L_4)
		{
			goto IL_0036;
		}
	}
	{
		bool L_5 = __this->get_isAndroidDevice_7();
		if (!L_5)
		{
			goto IL_004b;
		}
	}

IL_0036:
	{
		IntPtr_t L_6 = __this->get_renderEventPtr_5();
		GL_IssuePluginEvent_m2498560141(NULL /*static, unused*/, L_6, ((int32_t)1162892114), /*hidden argument*/NULL);
		goto IL_0051;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_SnapshotRenderbuffer_m3941835888(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0051:
	{
		return;
	}
}
extern "C" intptr_t DEFAULT_CALL EveryplayGetUnityRenderEventPtr();
// System.IntPtr EveryplayHudCamera::EveryplayGetUnityRenderEventPtr()
extern "C"  IntPtr_t EveryplayHudCamera_EveryplayGetUnityRenderEventPtr_m59318425 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(EveryplayGetUnityRenderEventPtr)();

	// Marshaling of return value back from native representation
	IntPtr_t _returnValue_unmarshaled;
	_returnValue_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)(returnValue)));

	return _returnValue_unmarshaled;
}
// System.Void EveryplayInGameFaceCam::.ctor()
extern "C"  void EveryplayInGameFaceCam__ctor_m3920189967 (EveryplayInGameFaceCam_t1834897128 * __this, const MethodInfo* method)
{
	{
		__this->set_textureSideWidth_3(((int32_t)128));
		__this->set_textureFormat_4(4);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayInGameFaceCam::Awake()
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* FaceCamSessionStartedDelegate_t1733547424_il2cpp_TypeInfo_var;
extern Il2CppClass* FaceCamSessionStoppedDelegate_t1894731428_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayInGameFaceCam_OnSessionStart_m2138866836_MethodInfo_var;
extern const MethodInfo* EveryplayInGameFaceCam_OnSessionStop_m2949283982_MethodInfo_var;
extern const uint32_t EveryplayInGameFaceCam_Awake_m2034936186_MetadataUsageId;
extern "C"  void EveryplayInGameFaceCam_Awake_m2034936186 (EveryplayInGameFaceCam_t1834897128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayInGameFaceCam_Awake_m2034936186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_textureSideWidth_3();
		int32_t L_1 = __this->get_textureSideWidth_3();
		int32_t L_2 = __this->get_textureFormat_4();
		Texture2D_t3542995729 * L_3 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_3, L_0, L_1, L_2, (bool)0, /*hidden argument*/NULL);
		__this->set_targetTexture_7(L_3);
		Texture2D_t3542995729 * L_4 = __this->get_targetTexture_7();
		int32_t L_5 = __this->get_textureWrapMode_5();
		NullCheck(L_4);
		Texture_set_wrapMode_m333956747(L_4, L_5, /*hidden argument*/NULL);
		Material_t193706927 * L_6 = __this->get_targetMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_008d;
		}
	}
	{
		Texture2D_t3542995729 * L_8 = __this->get_targetTexture_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_008d;
		}
	}
	{
		Material_t193706927 * L_10 = __this->get_targetMaterial_2();
		NullCheck(L_10);
		Texture_t2243626319 * L_11 = Material_get_mainTexture_m432794412(L_10, /*hidden argument*/NULL);
		__this->set_defaultTexture_6(L_11);
		Texture2D_t3542995729 * L_12 = __this->get_targetTexture_7();
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_FaceCamSetTargetTexture_m2609324012(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IntPtr_t L_13;
		L_13.set_m_value_0((void*)(void*)EveryplayInGameFaceCam_OnSessionStart_m2138866836_MethodInfo_var);
		FaceCamSessionStartedDelegate_t1733547424 * L_14 = (FaceCamSessionStartedDelegate_t1733547424 *)il2cpp_codegen_object_new(FaceCamSessionStartedDelegate_t1733547424_il2cpp_TypeInfo_var);
		FaceCamSessionStartedDelegate__ctor_m1763728449(L_14, __this, L_13, /*hidden argument*/NULL);
		Everyplay_add_FaceCamSessionStarted_m3896409671(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)EveryplayInGameFaceCam_OnSessionStop_m2949283982_MethodInfo_var);
		FaceCamSessionStoppedDelegate_t1894731428 * L_16 = (FaceCamSessionStoppedDelegate_t1894731428 *)il2cpp_codegen_object_new(FaceCamSessionStoppedDelegate_t1894731428_il2cpp_TypeInfo_var);
		FaceCamSessionStoppedDelegate__ctor_m4186510893(L_16, __this, L_15, /*hidden argument*/NULL);
		Everyplay_add_FaceCamSessionStopped_m2491956679(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_008d:
	{
		return;
	}
}
// System.Void EveryplayInGameFaceCam::OnSessionStart()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayInGameFaceCam_OnSessionStart_m2138866836_MetadataUsageId;
extern "C"  void EveryplayInGameFaceCam_OnSessionStart_m2138866836 (EveryplayInGameFaceCam_t1834897128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayInGameFaceCam_OnSessionStart_m2138866836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_targetMaterial_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		Texture2D_t3542995729 * L_2 = __this->get_targetTexture_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		Material_t193706927 * L_4 = __this->get_targetMaterial_2();
		Texture2D_t3542995729 * L_5 = __this->get_targetTexture_7();
		NullCheck(L_4);
		Material_set_mainTexture_m3584203343(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void EveryplayInGameFaceCam::OnSessionStop()
extern "C"  void EveryplayInGameFaceCam_OnSessionStop_m2949283982 (EveryplayInGameFaceCam_t1834897128 * __this, const MethodInfo* method)
{
	{
		Material_t193706927 * L_0 = __this->get_targetMaterial_2();
		Texture_t2243626319 * L_1 = __this->get_defaultTexture_6();
		NullCheck(L_0);
		Material_set_mainTexture_m3584203343(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayInGameFaceCam::OnDestroy()
extern Il2CppClass* FaceCamSessionStartedDelegate_t1733547424_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* FaceCamSessionStoppedDelegate_t1894731428_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayInGameFaceCam_OnSessionStart_m2138866836_MethodInfo_var;
extern const MethodInfo* EveryplayInGameFaceCam_OnSessionStop_m2949283982_MethodInfo_var;
extern const uint32_t EveryplayInGameFaceCam_OnDestroy_m2012015128_MetadataUsageId;
extern "C"  void EveryplayInGameFaceCam_OnDestroy_m2012015128 (EveryplayInGameFaceCam_t1834897128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayInGameFaceCam_OnDestroy_m2012015128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)EveryplayInGameFaceCam_OnSessionStart_m2138866836_MethodInfo_var);
		FaceCamSessionStartedDelegate_t1733547424 * L_1 = (FaceCamSessionStartedDelegate_t1733547424 *)il2cpp_codegen_object_new(FaceCamSessionStartedDelegate_t1733547424_il2cpp_TypeInfo_var);
		FaceCamSessionStartedDelegate__ctor_m1763728449(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_remove_FaceCamSessionStarted_m2342726600(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)EveryplayInGameFaceCam_OnSessionStop_m2949283982_MethodInfo_var);
		FaceCamSessionStoppedDelegate_t1894731428 * L_3 = (FaceCamSessionStoppedDelegate_t1894731428 *)il2cpp_codegen_object_new(FaceCamSessionStoppedDelegate_t1894731428_il2cpp_TypeInfo_var);
		FaceCamSessionStoppedDelegate__ctor_m4186510893(L_3, __this, L_2, /*hidden argument*/NULL);
		Everyplay_remove_FaceCamSessionStopped_m3063767112(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Object EveryplayMiniJSON.Json::Deserialize(System.String)
extern "C"  Il2CppObject * Json_Deserialize_m2304917621 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___json0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return NULL;
	}

IL_0008:
	{
		String_t* L_1 = ___json0;
		Il2CppObject * L_2 = Parser_Parse_m1759762959(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String EveryplayMiniJSON.Json::Serialize(System.Object)
extern "C"  String_t* Json_Serialize_m1349440280 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		String_t* L_1 = Serializer_Serialize_m3517769903(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void EveryplayMiniJSON.Json/Parser::.ctor(System.String)
extern Il2CppClass* StringReader_t1480123486_il2cpp_TypeInfo_var;
extern const uint32_t Parser__ctor_m1858534703_MetadataUsageId;
extern "C"  void Parser__ctor_m1858534703 (Parser_t2541283378 * __this, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser__ctor_m1858534703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___jsonString0;
		StringReader_t1480123486 * L_1 = (StringReader_t1480123486 *)il2cpp_codegen_object_new(StringReader_t1480123486_il2cpp_TypeInfo_var);
		StringReader__ctor_m643998729(L_1, L_0, /*hidden argument*/NULL);
		__this->set_json_2(L_1);
		return;
	}
}
// System.Object EveryplayMiniJSON.Json/Parser::Parse(System.String)
extern Il2CppClass* Parser_t2541283378_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Parser_Parse_m1759762959_MetadataUsageId;
extern "C"  Il2CppObject * Parser_Parse_m1759762959 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_Parse_m1759762959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Parser_t2541283378 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___jsonString0;
		Parser_t2541283378 * L_1 = (Parser_t2541283378 *)il2cpp_codegen_object_new(Parser_t2541283378_il2cpp_TypeInfo_var);
		Parser__ctor_m1858534703(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		Parser_t2541283378 * L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject * L_3 = Parser_ParseValue_m2923126038(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_LEAVE(0x20, FINALLY_0013);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0013;
	}

FINALLY_0013:
	{ // begin finally (depth: 1)
		{
			Parser_t2541283378 * L_4 = V_0;
			if (!L_4)
			{
				goto IL_001f;
			}
		}

IL_0019:
		{
			Parser_t2541283378 * L_5 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_5);
		}

IL_001f:
		{
			IL2CPP_END_FINALLY(19)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(19)
	{
		IL2CPP_JUMP_TBL(0x20, IL_0020)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0020:
	{
		Il2CppObject * L_6 = V_1;
		return L_6;
	}
}
// System.Void EveryplayMiniJSON.Json/Parser::Dispose()
extern "C"  void Parser_Dispose_m3460752774 (Parser_t2541283378 * __this, const MethodInfo* method)
{
	{
		StringReader_t1480123486 * L_0 = __this->get_json_2();
		NullCheck(L_0);
		TextReader_Dispose_m4077464570(L_0, /*hidden argument*/NULL);
		__this->set_json_2((StringReader_t1480123486 *)NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> EveryplayMiniJSON.Json/Parser::ParseObject()
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3188644741_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m4132139590_MethodInfo_var;
extern const uint32_t Parser_ParseObject_m4045311529_MetadataUsageId;
extern "C"  Dictionary_2_t309261261 * Parser_ParseObject_m4045311529 (Parser_t2541283378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseObject_m4045311529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t309261261 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		Dictionary_2_t309261261 * L_0 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3188644741(L_0, /*hidden argument*/Dictionary_2__ctor_m3188644741_MethodInfo_var);
		V_0 = L_0;
		StringReader_t1480123486 * L_1 = __this->get_json_2();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_1);
	}

IL_0012:
	{
		int32_t L_2 = Parser_get_NextToken_m3643776035(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (L_3 == 0)
		{
			goto IL_0037;
		}
		if (L_3 == 1)
		{
			goto IL_002b;
		}
		if (L_3 == 2)
		{
			goto IL_003e;
		}
	}

IL_002b:
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)6)))
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0040;
	}

IL_0037:
	{
		return (Dictionary_2_t309261261 *)NULL;
	}

IL_0039:
	{
		goto IL_0012;
	}

IL_003e:
	{
		Dictionary_2_t309261261 * L_5 = V_0;
		return L_5;
	}

IL_0040:
	{
		String_t* L_6 = Parser_ParseString_m3675037596(__this, /*hidden argument*/NULL);
		V_2 = L_6;
		String_t* L_7 = V_2;
		if (L_7)
		{
			goto IL_004f;
		}
	}
	{
		return (Dictionary_2_t309261261 *)NULL;
	}

IL_004f:
	{
		int32_t L_8 = Parser_get_NextToken_m3643776035(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)5)))
		{
			goto IL_005d;
		}
	}
	{
		return (Dictionary_2_t309261261 *)NULL;
	}

IL_005d:
	{
		StringReader_t1480123486 * L_9 = __this->get_json_2();
		NullCheck(L_9);
		VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_9);
		Dictionary_2_t309261261 * L_10 = V_0;
		String_t* L_11 = V_2;
		Il2CppObject * L_12 = Parser_ParseValue_m2923126038(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Dictionary_2_set_Item_m4132139590(L_10, L_11, L_12, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
		goto IL_007b;
	}

IL_007b:
	{
		goto IL_0012;
	}
}
// System.Collections.Generic.List`1<System.Object> EveryplayMiniJSON.Json/Parser::ParseArray()
extern Il2CppClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m365405030_MethodInfo_var;
extern const MethodInfo* List_1_Add_m567051994_MethodInfo_var;
extern const uint32_t Parser_ParseArray_m3092052064_MetadataUsageId;
extern "C"  List_1_t2058570427 * Parser_ParseArray_m3092052064 (Parser_t2541283378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseArray_m3092052064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2058570427 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)il2cpp_codegen_object_new(List_1_t2058570427_il2cpp_TypeInfo_var);
		List_1__ctor_m365405030(L_0, /*hidden argument*/List_1__ctor_m365405030_MethodInfo_var);
		V_0 = L_0;
		StringReader_t1480123486 * L_1 = __this->get_json_2();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_1);
		V_1 = (bool)1;
		goto IL_0061;
	}

IL_0019:
	{
		int32_t L_2 = Parser_get_NextToken_m3643776035(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_2;
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 0)
		{
			goto IL_0046;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 1)
		{
			goto IL_0034;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)4)) == 2)
		{
			goto IL_0041;
		}
	}

IL_0034:
	{
		int32_t L_4 = V_2;
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		goto IL_004d;
	}

IL_003f:
	{
		return (List_1_t2058570427 *)NULL;
	}

IL_0041:
	{
		goto IL_0061;
	}

IL_0046:
	{
		V_1 = (bool)0;
		goto IL_0061;
	}

IL_004d:
	{
		int32_t L_5 = V_2;
		Il2CppObject * L_6 = Parser_ParseByToken_m2171510005(__this, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		List_1_t2058570427 * L_7 = V_0;
		Il2CppObject * L_8 = V_3;
		NullCheck(L_7);
		List_1_Add_m567051994(L_7, L_8, /*hidden argument*/List_1_Add_m567051994_MethodInfo_var);
		goto IL_0061;
	}

IL_0061:
	{
		bool L_9 = V_1;
		if (L_9)
		{
			goto IL_0019;
		}
	}
	{
		List_1_t2058570427 * L_10 = V_0;
		return L_10;
	}
}
// System.Object EveryplayMiniJSON.Json/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m2923126038 (Parser_t2541283378 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Parser_get_NextToken_m3643776035(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		Il2CppObject * L_2 = Parser_ParseByToken_m2171510005(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Object EveryplayMiniJSON.Json/Parser::ParseByToken(EveryplayMiniJSON.Json/Parser/TOKEN)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseByToken_m2171510005_MetadataUsageId;
extern "C"  Il2CppObject * Parser_ParseByToken_m2171510005 (Parser_t2541283378 * __this, int32_t ___token0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseByToken_m2171510005_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___token0;
		if (((int32_t)((int32_t)L_0-(int32_t)7)) == 0)
		{
			goto IL_0035;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)7)) == 1)
		{
			goto IL_003c;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)7)) == 2)
		{
			goto IL_0051;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)7)) == 3)
		{
			goto IL_0058;
		}
		if (((int32_t)((int32_t)L_0-(int32_t)7)) == 4)
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_1 = ___token0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0043;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0061;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_004a;
		}
	}
	{
		goto IL_0061;
	}

IL_0035:
	{
		String_t* L_2 = Parser_ParseString_m3675037596(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_003c:
	{
		Il2CppObject * L_3 = Parser_ParseNumber_m3048319054(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0043:
	{
		Dictionary_2_t309261261 * L_4 = Parser_ParseObject_m4045311529(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_004a:
	{
		List_1_t2058570427 * L_5 = Parser_ParseArray_m3092052064(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0051:
	{
		bool L_6 = ((bool)1);
		Il2CppObject * L_7 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_6);
		return L_7;
	}

IL_0058:
	{
		bool L_8 = ((bool)0);
		Il2CppObject * L_9 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_8);
		return L_9;
	}

IL_005f:
	{
		return NULL;
	}

IL_0061:
	{
		return NULL;
	}
}
// System.String EveryplayMiniJSON.Json/Parser::ParseString()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseString_m3675037596_MetadataUsageId;
extern "C"  String_t* Parser_ParseString_m3675037596 (Parser_t2541283378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseString_m3675037596_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	bool V_2 = false;
	StringBuilder_t1221177846 * V_3 = NULL;
	int32_t V_4 = 0;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		StringReader_t1480123486 * L_1 = __this->get_json_2();
		NullCheck(L_1);
		VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_1);
		V_2 = (bool)1;
		goto IL_0168;
	}

IL_0019:
	{
		StringReader_t1480123486 * L_2 = __this->get_json_2();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0031;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_016e;
	}

IL_0031:
	{
		Il2CppChar L_4 = Parser_get_NextChar_m3068679379(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		Il2CppChar L_5 = V_1;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)34))))
		{
			goto IL_004d;
		}
	}
	{
		Il2CppChar L_6 = V_1;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)92))))
		{
			goto IL_0054;
		}
	}
	{
		goto IL_015b;
	}

IL_004d:
	{
		V_2 = (bool)0;
		goto IL_0168;
	}

IL_0054:
	{
		StringReader_t1480123486 * L_7 = __this->get_json_2();
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_7);
		if ((!(((uint32_t)L_8) == ((uint32_t)(-1)))))
		{
			goto IL_006c;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_0168;
	}

IL_006c:
	{
		Il2CppChar L_9 = Parser_get_NextChar_m3068679379(__this, /*hidden argument*/NULL);
		V_1 = L_9;
		Il2CppChar L_10 = V_1;
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)114))) == 0)
		{
			goto IL_00f7;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)114))) == 1)
		{
			goto IL_008c;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)114))) == 2)
		{
			goto IL_0105;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)114))) == 3)
		{
			goto IL_0113;
		}
	}

IL_008c:
	{
		Il2CppChar L_11 = V_1;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)34))))
		{
			goto IL_00c1;
		}
	}
	{
		Il2CppChar L_12 = V_1;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)47))))
		{
			goto IL_00c1;
		}
	}
	{
		Il2CppChar L_13 = V_1;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)92))))
		{
			goto IL_00c1;
		}
	}
	{
		Il2CppChar L_14 = V_1;
		if ((((int32_t)L_14) == ((int32_t)((int32_t)98))))
		{
			goto IL_00ce;
		}
	}
	{
		Il2CppChar L_15 = V_1;
		if ((((int32_t)L_15) == ((int32_t)((int32_t)102))))
		{
			goto IL_00db;
		}
	}
	{
		Il2CppChar L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)((int32_t)110))))
		{
			goto IL_00e9;
		}
	}
	{
		goto IL_0156;
	}

IL_00c1:
	{
		StringBuilder_t1221177846 * L_17 = V_0;
		Il2CppChar L_18 = V_1;
		NullCheck(L_17);
		StringBuilder_Append_m3618697540(L_17, L_18, /*hidden argument*/NULL);
		goto IL_0156;
	}

IL_00ce:
	{
		StringBuilder_t1221177846 * L_19 = V_0;
		NullCheck(L_19);
		StringBuilder_Append_m3618697540(L_19, 8, /*hidden argument*/NULL);
		goto IL_0156;
	}

IL_00db:
	{
		StringBuilder_t1221177846 * L_20 = V_0;
		NullCheck(L_20);
		StringBuilder_Append_m3618697540(L_20, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_0156;
	}

IL_00e9:
	{
		StringBuilder_t1221177846 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m3618697540(L_21, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_0156;
	}

IL_00f7:
	{
		StringBuilder_t1221177846 * L_22 = V_0;
		NullCheck(L_22);
		StringBuilder_Append_m3618697540(L_22, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_0156;
	}

IL_0105:
	{
		StringBuilder_t1221177846 * L_23 = V_0;
		NullCheck(L_23);
		StringBuilder_Append_m3618697540(L_23, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0156;
	}

IL_0113:
	{
		StringBuilder_t1221177846 * L_24 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_24, /*hidden argument*/NULL);
		V_3 = L_24;
		V_4 = 0;
		goto IL_0134;
	}

IL_0121:
	{
		StringBuilder_t1221177846 * L_25 = V_3;
		Il2CppChar L_26 = Parser_get_NextChar_m3068679379(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		StringBuilder_Append_m3618697540(L_25, L_26, /*hidden argument*/NULL);
		int32_t L_27 = V_4;
		V_4 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0134:
	{
		int32_t L_28 = V_4;
		if ((((int32_t)L_28) < ((int32_t)4)))
		{
			goto IL_0121;
		}
	}
	{
		StringBuilder_t1221177846 * L_29 = V_0;
		StringBuilder_t1221177846 * L_30 = V_3;
		NullCheck(L_30);
		String_t* L_31 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_32 = Convert_ToInt32_m3262696010(NULL /*static, unused*/, L_31, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_29);
		StringBuilder_Append_m3618697540(L_29, (((int32_t)((uint16_t)L_32))), /*hidden argument*/NULL);
		goto IL_0156;
	}

IL_0156:
	{
		goto IL_0168;
	}

IL_015b:
	{
		StringBuilder_t1221177846 * L_33 = V_0;
		Il2CppChar L_34 = V_1;
		NullCheck(L_33);
		StringBuilder_Append_m3618697540(L_33, L_34, /*hidden argument*/NULL);
		goto IL_0168;
	}

IL_0168:
	{
		bool L_35 = V_2;
		if (L_35)
		{
			goto IL_0019;
		}
	}

IL_016e:
	{
		StringBuilder_t1221177846 * L_36 = V_0;
		NullCheck(L_36);
		String_t* L_37 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_36);
		return L_37;
	}
}
// System.Object EveryplayMiniJSON.Json/Parser::ParseNumber()
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t Parser_ParseNumber_m3048319054_MetadataUsageId;
extern "C"  Il2CppObject * Parser_ParseNumber_m3048319054 (Parser_t2541283378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_ParseNumber_m3048319054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int64_t V_1 = 0;
	double V_2 = 0.0;
	{
		String_t* L_0 = Parser_get_NextWord_m1808783620(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = String_IndexOf_m2358239236(L_1, ((int32_t)46), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_3 = V_0;
		Int64_TryParse_m948922810(NULL /*static, unused*/, L_3, (&V_1), /*hidden argument*/NULL);
		int64_t L_4 = V_1;
		int64_t L_5 = L_4;
		Il2CppObject * L_6 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_5);
		return L_6;
	}

IL_0025:
	{
		String_t* L_7 = V_0;
		Double_TryParse_m3252018994(NULL /*static, unused*/, L_7, (&V_2), /*hidden argument*/NULL);
		double L_8 = V_2;
		double L_9 = L_8;
		Il2CppObject * L_10 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_9);
		return L_10;
	}
}
// System.Void EveryplayMiniJSON.Json/Parser::EatWhitespace()
extern Il2CppCodeGenString* _stringLiteral2166592820;
extern const uint32_t Parser_EatWhitespace_m2728703728_MetadataUsageId;
extern "C"  void Parser_EatWhitespace_m2728703728 (Parser_t2541283378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_EatWhitespace_m2728703728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		goto IL_0027;
	}

IL_0005:
	{
		StringReader_t1480123486 * L_0 = __this->get_json_2();
		NullCheck(L_0);
		VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_0);
		StringReader_t1480123486 * L_1 = __this->get_json_2();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0027;
		}
	}
	{
		goto IL_003d;
	}

IL_0027:
	{
		Il2CppChar L_3 = Parser_get_PeekChar_m1526822795(__this, /*hidden argument*/NULL);
		NullCheck(_stringLiteral2166592820);
		int32_t L_4 = String_IndexOf_m2358239236(_stringLiteral2166592820, L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0005;
		}
	}

IL_003d:
	{
		return;
	}
}
// System.Char EveryplayMiniJSON.Json/Parser::get_PeekChar()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_PeekChar_m1526822795_MetadataUsageId;
extern "C"  Il2CppChar Parser_get_PeekChar_m1526822795 (Parser_t2541283378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_PeekChar_m1526822795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringReader_t1480123486 * L_0 = __this->get_json_2();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppChar L_2 = Convert_ToChar_m3827339132(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Char EveryplayMiniJSON.Json/Parser::get_NextChar()
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t Parser_get_NextChar_m3068679379_MetadataUsageId;
extern "C"  Il2CppChar Parser_get_NextChar_m3068679379 (Parser_t2541283378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextChar_m3068679379_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringReader_t1480123486 * L_0 = __this->get_json_2();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppChar L_2 = Convert_ToChar_m3827339132(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String EveryplayMiniJSON.Json/Parser::get_NextWord()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral227230824;
extern const uint32_t Parser_get_NextWord_m1808783620_MetadataUsageId;
extern "C"  String_t* Parser_get_NextWord_m1808783620 (Parser_t2541283378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextWord_m1808783620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_002e;
	}

IL_000b:
	{
		StringBuilder_t1221177846 * L_1 = V_0;
		Il2CppChar L_2 = Parser_get_NextChar_m3068679379(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m3618697540(L_1, L_2, /*hidden argument*/NULL);
		StringReader_t1480123486 * L_3 = __this->get_json_2();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_3);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_002e;
		}
	}
	{
		goto IL_0044;
	}

IL_002e:
	{
		Il2CppChar L_5 = Parser_get_PeekChar_m1526822795(__this, /*hidden argument*/NULL);
		NullCheck(_stringLiteral227230824);
		int32_t L_6 = String_IndexOf_m2358239236(_stringLiteral227230824, L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)(-1))))
		{
			goto IL_000b;
		}
	}

IL_0044:
	{
		StringBuilder_t1221177846 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		return L_8;
	}
}
// EveryplayMiniJSON.Json/Parser/TOKEN EveryplayMiniJSON.Json/Parser::get_NextToken()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern const uint32_t Parser_get_NextToken_m3643776035_MetadataUsageId;
extern "C"  int32_t Parser_get_NextToken_m3643776035 (Parser_t2541283378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Parser_get_NextToken_m3643776035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	String_t* V_1 = NULL;
	{
		Parser_EatWhitespace_m2728703728(__this, /*hidden argument*/NULL);
		StringReader_t1480123486 * L_0 = __this->get_json_2();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(9 /* System.Int32 System.IO.TextReader::Peek() */, L_0);
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_0019;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0019:
	{
		Il2CppChar L_2 = Parser_get_PeekChar_m1526822795(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		Il2CppChar L_3 = V_0;
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 0)
		{
			goto IL_00bc;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 1)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 2)
		{
			goto IL_0065;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 3)
		{
			goto IL_0065;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 4)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 5)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 6)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 7)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 8)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 9)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 10)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 11)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 12)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 13)
		{
			goto IL_00ce;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)((int32_t)44))) == 14)
		{
			goto IL_00cc;
		}
	}

IL_0065:
	{
		Il2CppChar L_4 = V_0;
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_00ac;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_007a;
		}
		if (((int32_t)((int32_t)L_4-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_00ae;
		}
	}

IL_007a:
	{
		Il2CppChar L_5 = V_0;
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_009c;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_008f;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_009e;
		}
	}

IL_008f:
	{
		Il2CppChar L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)34))))
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_00d0;
	}

IL_009c:
	{
		return (int32_t)(1);
	}

IL_009e:
	{
		StringReader_t1480123486 * L_7 = __this->get_json_2();
		NullCheck(L_7);
		VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_7);
		return (int32_t)(2);
	}

IL_00ac:
	{
		return (int32_t)(3);
	}

IL_00ae:
	{
		StringReader_t1480123486 * L_8 = __this->get_json_2();
		NullCheck(L_8);
		VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_8);
		return (int32_t)(4);
	}

IL_00bc:
	{
		StringReader_t1480123486 * L_9 = __this->get_json_2();
		NullCheck(L_9);
		VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.IO.TextReader::Read() */, L_9);
		return (int32_t)(6);
	}

IL_00ca:
	{
		return (int32_t)(7);
	}

IL_00cc:
	{
		return (int32_t)(5);
	}

IL_00ce:
	{
		return (int32_t)(8);
	}

IL_00d0:
	{
		String_t* L_10 = Parser_get_NextWord_m1808783620(__this, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t* L_11 = V_1;
		if (!L_11)
		{
			goto IL_011b;
		}
	}
	{
		String_t* L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_12, _stringLiteral2609877245, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0112;
		}
	}
	{
		String_t* L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_14, _stringLiteral3323263070, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0115;
		}
	}
	{
		String_t* L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_16, _stringLiteral1743624307, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0118;
		}
	}
	{
		goto IL_011b;
	}

IL_0112:
	{
		return (int32_t)(((int32_t)10));
	}

IL_0115:
	{
		return (int32_t)(((int32_t)9));
	}

IL_0118:
	{
		return (int32_t)(((int32_t)11));
	}

IL_011b:
	{
		return (int32_t)(0);
	}
}
// System.Void EveryplayMiniJSON.Json/Serializer::.ctor()
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern const uint32_t Serializer__ctor_m1683379102_MetadataUsageId;
extern "C"  void Serializer__ctor_m1683379102 (Serializer_t3671902477 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer__ctor_m1683379102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_0, /*hidden argument*/NULL);
		__this->set_builder_0(L_0);
		return;
	}
}
// System.String EveryplayMiniJSON.Json/Serializer::Serialize(System.Object)
extern Il2CppClass* Serializer_t3671902477_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_Serialize_m3517769903_MetadataUsageId;
extern "C"  String_t* Serializer_Serialize_m3517769903 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_Serialize_m3517769903_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Serializer_t3671902477 * V_0 = NULL;
	{
		Serializer_t3671902477 * L_0 = (Serializer_t3671902477 *)il2cpp_codegen_object_new(Serializer_t3671902477_il2cpp_TypeInfo_var);
		Serializer__ctor_m1683379102(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Serializer_t3671902477 * L_1 = V_0;
		Il2CppObject * L_2 = ___obj0;
		NullCheck(L_1);
		Serializer_SerializeValue_m4288457565(L_1, L_2, /*hidden argument*/NULL);
		Serializer_t3671902477 * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_t1221177846 * L_4 = L_3->get_builder_0();
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		return L_5;
	}
}
// System.Void EveryplayMiniJSON.Json/Serializer::SerializeValue(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_t3321498491_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern const uint32_t Serializer_SerializeValue_m4288457565_MetadataUsageId;
extern "C"  void Serializer_SerializeValue_m4288457565 (Serializer_t3671902477 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeValue_m4288457565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	String_t* V_2 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		StringBuilder_t1221177846 * L_1 = __this->get_builder_0();
		NullCheck(L_1);
		StringBuilder_Append_m3636508479(L_1, _stringLiteral1743624307, /*hidden argument*/NULL);
		goto IL_00b1;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___value0;
		String_t* L_3 = ((String_t*)IsInstSealed(L_2, String_t_il2cpp_TypeInfo_var));
		V_2 = L_3;
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_4 = V_2;
		Serializer_SerializeString_m2042610399(__this, L_4, /*hidden argument*/NULL);
		goto IL_00b1;
	}

IL_0035:
	{
		Il2CppObject * L_5 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_5, Boolean_t3825574718_il2cpp_TypeInfo_var)))
		{
			goto IL_005c;
		}
	}
	{
		StringBuilder_t1221177846 * L_6 = __this->get_builder_0();
		Il2CppObject * L_7 = ___value0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		NullCheck(L_8);
		String_t* L_9 = String_ToLower_m2994460523(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringBuilder_Append_m3636508479(L_6, L_9, /*hidden argument*/NULL);
		goto IL_00b1;
	}

IL_005c:
	{
		Il2CppObject * L_10 = ___value0;
		Il2CppObject * L_11 = ((Il2CppObject *)IsInst(L_10, IList_t3321498491_il2cpp_TypeInfo_var));
		V_0 = L_11;
		if (!L_11)
		{
			goto IL_0075;
		}
	}
	{
		Il2CppObject * L_12 = V_0;
		Serializer_SerializeArray_m2532734174(__this, L_12, /*hidden argument*/NULL);
		goto IL_00b1;
	}

IL_0075:
	{
		Il2CppObject * L_13 = ___value0;
		Il2CppObject * L_14 = ((Il2CppObject *)IsInst(L_13, IDictionary_t596158605_il2cpp_TypeInfo_var));
		V_1 = L_14;
		if (!L_14)
		{
			goto IL_008e;
		}
	}
	{
		Il2CppObject * L_15 = V_1;
		Serializer_SerializeObject_m488985586(__this, L_15, /*hidden argument*/NULL);
		goto IL_00b1;
	}

IL_008e:
	{
		Il2CppObject * L_16 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_16, Char_t3454481338_il2cpp_TypeInfo_var)))
		{
			goto IL_00aa;
		}
	}
	{
		Il2CppObject * L_17 = ___value0;
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		Serializer_SerializeString_m2042610399(__this, L_18, /*hidden argument*/NULL);
		goto IL_00b1;
	}

IL_00aa:
	{
		Il2CppObject * L_19 = ___value0;
		Serializer_SerializeOther_m3004225876(__this, L_19, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		return;
	}
}
// System.Void EveryplayMiniJSON.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern Il2CppClass* IDictionary_t596158605_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeObject_m488985586_MetadataUsageId;
extern "C"  void Serializer_SerializeObject_m488985586 (Serializer_t3671902477 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeObject_m488985586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (bool)1;
		StringBuilder_t1221177846 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m3618697540(L_0, ((int32_t)123), /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___obj0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(2 /* System.Collections.ICollection System.Collections.IDictionary::get_Keys() */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0065;
		}

IL_0021:
		{
			Il2CppObject * L_4 = V_2;
			NullCheck(L_4);
			Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			bool L_6 = V_0;
			if (L_6)
			{
				goto IL_003c;
			}
		}

IL_002e:
		{
			StringBuilder_t1221177846 * L_7 = __this->get_builder_0();
			NullCheck(L_7);
			StringBuilder_Append_m3618697540(L_7, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_003c:
		{
			Il2CppObject * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
			Serializer_SerializeString_m2042610399(__this, L_9, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_10 = __this->get_builder_0();
			NullCheck(L_10);
			StringBuilder_Append_m3618697540(L_10, ((int32_t)58), /*hidden argument*/NULL);
			Il2CppObject * L_11 = ___obj0;
			Il2CppObject * L_12 = V_1;
			NullCheck(L_11);
			Il2CppObject * L_13 = InterfaceFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IDictionary::get_Item(System.Object) */, IDictionary_t596158605_il2cpp_TypeInfo_var, L_11, L_12);
			Serializer_SerializeValue_m4288457565(__this, L_13, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_0065:
		{
			Il2CppObject * L_14 = V_2;
			NullCheck(L_14);
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_0021;
			}
		}

IL_0070:
		{
			IL2CPP_LEAVE(0x89, FINALLY_0075);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0075;
	}

FINALLY_0075:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_16 = V_2;
			Il2CppObject * L_17 = ((Il2CppObject *)IsInst(L_16, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_3 = L_17;
			if (!L_17)
			{
				goto IL_0088;
			}
		}

IL_0082:
		{
			Il2CppObject * L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_18);
		}

IL_0088:
		{
			IL2CPP_END_FINALLY(117)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(117)
	{
		IL2CPP_JUMP_TBL(0x89, IL_0089)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0089:
	{
		StringBuilder_t1221177846 * L_19 = __this->get_builder_0();
		NullCheck(L_19);
		StringBuilder_Append_m3618697540(L_19, ((int32_t)125), /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayMiniJSON.Json/Serializer::SerializeArray(System.Collections.IList)
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeArray_m2532734174_MetadataUsageId;
extern "C"  void Serializer_SerializeArray_m2532734174 (Serializer_t3671902477 * __this, Il2CppObject * ___anArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeArray_m2532734174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1221177846 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m3618697540(L_0, ((int32_t)91), /*hidden argument*/NULL);
		V_0 = (bool)1;
		Il2CppObject * L_1 = ___anArray0;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_1);
		V_2 = L_2;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0040;
		}

IL_001c:
		{
			Il2CppObject * L_3 = V_2;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			bool L_5 = V_0;
			if (L_5)
			{
				goto IL_0037;
			}
		}

IL_0029:
		{
			StringBuilder_t1221177846 * L_6 = __this->get_builder_0();
			NullCheck(L_6);
			StringBuilder_Append_m3618697540(L_6, ((int32_t)44), /*hidden argument*/NULL);
		}

IL_0037:
		{
			Il2CppObject * L_7 = V_1;
			Serializer_SerializeValue_m4288457565(__this, L_7, /*hidden argument*/NULL);
			V_0 = (bool)0;
		}

IL_0040:
		{
			Il2CppObject * L_8 = V_2;
			NullCheck(L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_001c;
			}
		}

IL_004b:
		{
			IL2CPP_LEAVE(0x64, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_10 = V_2;
			Il2CppObject * L_11 = ((Il2CppObject *)IsInst(L_10, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_3 = L_11;
			if (!L_11)
			{
				goto IL_0063;
			}
		}

IL_005d:
		{
			Il2CppObject * L_12 = V_3;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_12);
		}

IL_0063:
		{
			IL2CPP_END_FINALLY(80)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x64, IL_0064)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0064:
	{
		StringBuilder_t1221177846 * L_13 = __this->get_builder_0();
		NullCheck(L_13);
		StringBuilder_Append_m3618697540(L_13, ((int32_t)93), /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayMiniJSON.Json/Serializer::SerializeString(System.String)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3943473468;
extern Il2CppCodeGenString* _stringLiteral2088416310;
extern Il2CppCodeGenString* _stringLiteral1093630588;
extern Il2CppCodeGenString* _stringLiteral3419229416;
extern Il2CppCodeGenString* _stringLiteral3062999056;
extern Il2CppCodeGenString* _stringLiteral381169868;
extern Il2CppCodeGenString* _stringLiteral3869568110;
extern Il2CppCodeGenString* _stringLiteral2303484169;
extern const uint32_t Serializer_SerializeString_m2042610399_MetadataUsageId;
extern "C"  void Serializer_SerializeString_m2042610399 (Serializer_t3671902477 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeString_m2042610399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t1328083999* V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	CharU5BU5D_t1328083999* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		StringBuilder_t1221177846 * L_0 = __this->get_builder_0();
		NullCheck(L_0);
		StringBuilder_Append_m3618697540(L_0, ((int32_t)34), /*hidden argument*/NULL);
		String_t* L_1 = ___str0;
		NullCheck(L_1);
		CharU5BU5D_t1328083999* L_2 = String_ToCharArray_m870309954(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		CharU5BU5D_t1328083999* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_014d;
	}

IL_001e:
	{
		CharU5BU5D_t1328083999* L_4 = V_2;
		int32_t L_5 = V_3;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		uint16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		Il2CppChar L_8 = V_1;
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 0)
		{
			goto IL_0083;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 1)
		{
			goto IL_00db;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 2)
		{
			goto IL_00af;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 3)
		{
			goto IL_0042;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 4)
		{
			goto IL_0099;
		}
		if (((int32_t)((int32_t)L_8-(int32_t)8)) == 5)
		{
			goto IL_00c5;
		}
	}

IL_0042:
	{
		Il2CppChar L_9 = V_1;
		if ((((int32_t)L_9) == ((int32_t)((int32_t)34))))
		{
			goto IL_0057;
		}
	}
	{
		Il2CppChar L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)((int32_t)92))))
		{
			goto IL_006d;
		}
	}
	{
		goto IL_00f1;
	}

IL_0057:
	{
		StringBuilder_t1221177846 * L_11 = __this->get_builder_0();
		NullCheck(L_11);
		StringBuilder_Append_m3636508479(L_11, _stringLiteral3943473468, /*hidden argument*/NULL);
		goto IL_0149;
	}

IL_006d:
	{
		StringBuilder_t1221177846 * L_12 = __this->get_builder_0();
		NullCheck(L_12);
		StringBuilder_Append_m3636508479(L_12, _stringLiteral2088416310, /*hidden argument*/NULL);
		goto IL_0149;
	}

IL_0083:
	{
		StringBuilder_t1221177846 * L_13 = __this->get_builder_0();
		NullCheck(L_13);
		StringBuilder_Append_m3636508479(L_13, _stringLiteral1093630588, /*hidden argument*/NULL);
		goto IL_0149;
	}

IL_0099:
	{
		StringBuilder_t1221177846 * L_14 = __this->get_builder_0();
		NullCheck(L_14);
		StringBuilder_Append_m3636508479(L_14, _stringLiteral3419229416, /*hidden argument*/NULL);
		goto IL_0149;
	}

IL_00af:
	{
		StringBuilder_t1221177846 * L_15 = __this->get_builder_0();
		NullCheck(L_15);
		StringBuilder_Append_m3636508479(L_15, _stringLiteral3062999056, /*hidden argument*/NULL);
		goto IL_0149;
	}

IL_00c5:
	{
		StringBuilder_t1221177846 * L_16 = __this->get_builder_0();
		NullCheck(L_16);
		StringBuilder_Append_m3636508479(L_16, _stringLiteral381169868, /*hidden argument*/NULL);
		goto IL_0149;
	}

IL_00db:
	{
		StringBuilder_t1221177846 * L_17 = __this->get_builder_0();
		NullCheck(L_17);
		StringBuilder_Append_m3636508479(L_17, _stringLiteral3869568110, /*hidden argument*/NULL);
		goto IL_0149;
	}

IL_00f1:
	{
		Il2CppChar L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		int32_t L_19 = Convert_ToInt32_m3683486440(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		int32_t L_20 = V_4;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)32))))
		{
			goto IL_011d;
		}
	}
	{
		int32_t L_21 = V_4;
		if ((((int32_t)L_21) > ((int32_t)((int32_t)126))))
		{
			goto IL_011d;
		}
	}
	{
		StringBuilder_t1221177846 * L_22 = __this->get_builder_0();
		Il2CppChar L_23 = V_1;
		NullCheck(L_22);
		StringBuilder_Append_m3618697540(L_22, L_23, /*hidden argument*/NULL);
		goto IL_0144;
	}

IL_011d:
	{
		StringBuilder_t1221177846 * L_24 = __this->get_builder_0();
		int32_t L_25 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_26 = Convert_ToString_m3058299767(NULL /*static, unused*/, L_25, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27 = String_PadLeft_m1726975163(L_26, 4, ((int32_t)48), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2303484169, L_27, /*hidden argument*/NULL);
		NullCheck(L_24);
		StringBuilder_Append_m3636508479(L_24, L_28, /*hidden argument*/NULL);
	}

IL_0144:
	{
		goto IL_0149;
	}

IL_0149:
	{
		int32_t L_29 = V_3;
		V_3 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_014d:
	{
		int32_t L_30 = V_3;
		CharU5BU5D_t1328083999* L_31 = V_2;
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		StringBuilder_t1221177846 * L_32 = __this->get_builder_0();
		NullCheck(L_32);
		StringBuilder_Append_m3618697540(L_32, ((int32_t)34), /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayMiniJSON.Json/Serializer::SerializeOther(System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* SByte_t454417549_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern const uint32_t Serializer_SerializeOther_m3004225876_MetadataUsageId;
extern "C"  void Serializer_SerializeOther_m3004225876 (Serializer_t3671902477 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Serializer_SerializeOther_m3004225876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_0, Single_t2076509932_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_1, Int32_t2071877448_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_2 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_2, UInt32_t2149682021_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_3 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_3, Int64_t909078037_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_4 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_4, Double_t4078015681_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_5 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_5, SByte_t454417549_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_6 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_6, Byte_t3683104436_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_7 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_7, Int16_t4041245914_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_8 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_8, UInt16_t986882611_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_9 = ___value0;
		if (((Il2CppObject *)IsInstSealed(L_9, UInt64_t2909196914_il2cpp_TypeInfo_var)))
		{
			goto IL_0079;
		}
	}
	{
		Il2CppObject * L_10 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_10, Decimal_t724701077_il2cpp_TypeInfo_var)))
		{
			goto IL_0090;
		}
	}

IL_0079:
	{
		StringBuilder_t1221177846 * L_11 = __this->get_builder_0();
		Il2CppObject * L_12 = ___value0;
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_12);
		NullCheck(L_11);
		StringBuilder_Append_m3636508479(L_11, L_13, /*hidden argument*/NULL);
		goto IL_009c;
	}

IL_0090:
	{
		Il2CppObject * L_14 = ___value0;
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		Serializer_SerializeString_m2042610399(__this, L_15, /*hidden argument*/NULL);
	}

IL_009c:
	{
		return;
	}
}
// System.Void EveryplayRecButtons::.ctor()
extern "C"  void EveryplayRecButtons__ctor_m553216977 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (16.0f), (16.0f), /*hidden argument*/NULL);
		__this->set_containerMargin_4(L_0);
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_containerOffset_5(L_1);
		__this->set_containerScaling_6((1.0f));
		__this->set_buttonTitleHorizontalMargin_10(((int32_t)16));
		__this->set_buttonTitleVerticalMargin_11(((int32_t)20));
		__this->set_buttonMargin_12(8);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::Awake()
extern Il2CppClass* TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* ButtonTapped_t3122824015_il2cpp_TypeInfo_var;
extern Il2CppClass* Button_t3262243951_il2cpp_TypeInfo_var;
extern Il2CppClass* ToggleButton_t2881484729_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2631365083_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var;
extern Il2CppClass* FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayRecButtons_ShareVideo_m1973277741_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_EditVideo_m3846525876_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_OpenEveryplay_m4060651158_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_StartRecording_m1425756496_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_StopRecording_m1817812284_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_FaceCamToggle_m1400819525_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2532183734_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_RecordingStarted_m2508165025_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_RecordingStopped_m2345808949_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_ReadyForRecording_m1360139799_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_FaceCamRecordingPermission_m3188918626_MethodInfo_var;
extern const uint32_t EveryplayRecButtons_Awake_m3555848656_MetadataUsageId;
extern "C"  void EveryplayRecButtons_Awake_m3555848656 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_Awake_m3555848656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = EveryplayRecButtons_GetScalingByResolution_m2106876889(__this, /*hidden argument*/NULL);
		__this->set_containerScaling_6(L_0);
		float L_1 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_2 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_2, ((int32_t)112), ((int32_t)19), 0, 0, L_1, /*hidden argument*/NULL);
		__this->set_editVideoAtlasSrc_15(L_2);
		float L_3 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_4 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_4, ((int32_t)103), ((int32_t)19), ((int32_t)116), 0, L_3, /*hidden argument*/NULL);
		__this->set_faceCamAtlasSrc_16(L_4);
		float L_5 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_6 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_6, ((int32_t)178), ((int32_t)23), 0, ((int32_t)23), L_5, /*hidden argument*/NULL);
		__this->set_openEveryplayAtlasSrc_17(L_6);
		float L_7 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_8 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_8, ((int32_t)134), ((int32_t)19), 0, ((int32_t)50), L_7, /*hidden argument*/NULL);
		__this->set_shareVideoAtlasSrc_18(L_8);
		float L_9 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_10 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_10, ((int32_t)171), ((int32_t)23), 0, ((int32_t)73), L_9, /*hidden argument*/NULL);
		__this->set_startRecordingAtlasSrc_19(L_10);
		float L_11 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_12 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_12, ((int32_t)169), ((int32_t)23), 0, ((int32_t)100), L_11, /*hidden argument*/NULL);
		__this->set_stopRecordingAtlasSrc_20(L_12);
		float L_13 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_14 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_14, ((int32_t)101), ((int32_t)42), 0, ((int32_t)127), L_13, /*hidden argument*/NULL);
		__this->set_facecamToggleOnAtlasSrc_21(L_14);
		float L_15 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_16 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_16, ((int32_t)101), ((int32_t)42), ((int32_t)101), ((int32_t)127), L_15, /*hidden argument*/NULL);
		__this->set_facecamToggleOffAtlasSrc_22(L_16);
		float L_17 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_18 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_18, ((int32_t)256), ((int32_t)9), 0, ((int32_t)169), L_17, /*hidden argument*/NULL);
		__this->set_bgHeaderAtlasSrc_23(L_18);
		float L_19 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_20 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_20, ((int32_t)256), ((int32_t)9), 0, ((int32_t)169), L_19, /*hidden argument*/NULL);
		__this->set_bgFooterAtlasSrc_24(L_20);
		float L_21 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_22 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_22, ((int32_t)256), 6, 0, ((int32_t)178), L_21, /*hidden argument*/NULL);
		__this->set_bgAtlasSrc_25(L_22);
		float L_23 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_24 = (TextureAtlasSrc_t2048635151 *)il2cpp_codegen_object_new(TextureAtlasSrc_t2048635151_il2cpp_TypeInfo_var);
		TextureAtlasSrc__ctor_m2103365045(L_24, ((int32_t)220), ((int32_t)64), 0, ((int32_t)190), L_23, /*hidden argument*/NULL);
		__this->set_buttonAtlasSrc_26(L_24);
		int32_t L_25 = __this->get_buttonTitleHorizontalMargin_10();
		float L_26 = __this->get_containerScaling_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_27 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)(((float)((float)L_25)))*(float)L_26)), /*hidden argument*/NULL);
		__this->set_buttonTitleHorizontalMargin_10(L_27);
		int32_t L_28 = __this->get_buttonTitleVerticalMargin_11();
		float L_29 = __this->get_containerScaling_6();
		int32_t L_30 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)(((float)((float)L_28)))*(float)L_29)), /*hidden argument*/NULL);
		__this->set_buttonTitleVerticalMargin_11(L_30);
		int32_t L_31 = __this->get_buttonMargin_12();
		float L_32 = __this->get_containerScaling_6();
		int32_t L_33 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)(((float)((float)L_31)))*(float)L_32)), /*hidden argument*/NULL);
		__this->set_buttonMargin_12(L_33);
		TextureAtlasSrc_t2048635151 * L_34 = __this->get_buttonAtlasSrc_26();
		TextureAtlasSrc_t2048635151 * L_35 = __this->get_shareVideoAtlasSrc_18();
		IntPtr_t L_36;
		L_36.set_m_value_0((void*)(void*)EveryplayRecButtons_ShareVideo_m1973277741_MethodInfo_var);
		ButtonTapped_t3122824015 * L_37 = (ButtonTapped_t3122824015 *)il2cpp_codegen_object_new(ButtonTapped_t3122824015_il2cpp_TypeInfo_var);
		ButtonTapped__ctor_m207266678(L_37, __this, L_36, /*hidden argument*/NULL);
		Button_t3262243951 * L_38 = (Button_t3262243951 *)il2cpp_codegen_object_new(Button_t3262243951_il2cpp_TypeInfo_var);
		Button__ctor_m345536917(L_38, L_34, L_35, L_37, /*hidden argument*/NULL);
		__this->set_shareVideoButton_27(L_38);
		TextureAtlasSrc_t2048635151 * L_39 = __this->get_buttonAtlasSrc_26();
		TextureAtlasSrc_t2048635151 * L_40 = __this->get_editVideoAtlasSrc_15();
		IntPtr_t L_41;
		L_41.set_m_value_0((void*)(void*)EveryplayRecButtons_EditVideo_m3846525876_MethodInfo_var);
		ButtonTapped_t3122824015 * L_42 = (ButtonTapped_t3122824015 *)il2cpp_codegen_object_new(ButtonTapped_t3122824015_il2cpp_TypeInfo_var);
		ButtonTapped__ctor_m207266678(L_42, __this, L_41, /*hidden argument*/NULL);
		Button_t3262243951 * L_43 = (Button_t3262243951 *)il2cpp_codegen_object_new(Button_t3262243951_il2cpp_TypeInfo_var);
		Button__ctor_m345536917(L_43, L_39, L_40, L_42, /*hidden argument*/NULL);
		__this->set_editVideoButton_28(L_43);
		TextureAtlasSrc_t2048635151 * L_44 = __this->get_buttonAtlasSrc_26();
		TextureAtlasSrc_t2048635151 * L_45 = __this->get_openEveryplayAtlasSrc_17();
		IntPtr_t L_46;
		L_46.set_m_value_0((void*)(void*)EveryplayRecButtons_OpenEveryplay_m4060651158_MethodInfo_var);
		ButtonTapped_t3122824015 * L_47 = (ButtonTapped_t3122824015 *)il2cpp_codegen_object_new(ButtonTapped_t3122824015_il2cpp_TypeInfo_var);
		ButtonTapped__ctor_m207266678(L_47, __this, L_46, /*hidden argument*/NULL);
		Button_t3262243951 * L_48 = (Button_t3262243951 *)il2cpp_codegen_object_new(Button_t3262243951_il2cpp_TypeInfo_var);
		Button__ctor_m345536917(L_48, L_44, L_45, L_47, /*hidden argument*/NULL);
		__this->set_openEveryplayButton_29(L_48);
		TextureAtlasSrc_t2048635151 * L_49 = __this->get_buttonAtlasSrc_26();
		TextureAtlasSrc_t2048635151 * L_50 = __this->get_startRecordingAtlasSrc_19();
		IntPtr_t L_51;
		L_51.set_m_value_0((void*)(void*)EveryplayRecButtons_StartRecording_m1425756496_MethodInfo_var);
		ButtonTapped_t3122824015 * L_52 = (ButtonTapped_t3122824015 *)il2cpp_codegen_object_new(ButtonTapped_t3122824015_il2cpp_TypeInfo_var);
		ButtonTapped__ctor_m207266678(L_52, __this, L_51, /*hidden argument*/NULL);
		Button_t3262243951 * L_53 = (Button_t3262243951 *)il2cpp_codegen_object_new(Button_t3262243951_il2cpp_TypeInfo_var);
		Button__ctor_m345536917(L_53, L_49, L_50, L_52, /*hidden argument*/NULL);
		__this->set_startRecordingButton_30(L_53);
		TextureAtlasSrc_t2048635151 * L_54 = __this->get_buttonAtlasSrc_26();
		TextureAtlasSrc_t2048635151 * L_55 = __this->get_stopRecordingAtlasSrc_20();
		IntPtr_t L_56;
		L_56.set_m_value_0((void*)(void*)EveryplayRecButtons_StopRecording_m1817812284_MethodInfo_var);
		ButtonTapped_t3122824015 * L_57 = (ButtonTapped_t3122824015 *)il2cpp_codegen_object_new(ButtonTapped_t3122824015_il2cpp_TypeInfo_var);
		ButtonTapped__ctor_m207266678(L_57, __this, L_56, /*hidden argument*/NULL);
		Button_t3262243951 * L_58 = (Button_t3262243951 *)il2cpp_codegen_object_new(Button_t3262243951_il2cpp_TypeInfo_var);
		Button__ctor_m345536917(L_58, L_54, L_55, L_57, /*hidden argument*/NULL);
		__this->set_stopRecordingButton_31(L_58);
		TextureAtlasSrc_t2048635151 * L_59 = __this->get_buttonAtlasSrc_26();
		TextureAtlasSrc_t2048635151 * L_60 = __this->get_faceCamAtlasSrc_16();
		IntPtr_t L_61;
		L_61.set_m_value_0((void*)(void*)EveryplayRecButtons_FaceCamToggle_m1400819525_MethodInfo_var);
		ButtonTapped_t3122824015 * L_62 = (ButtonTapped_t3122824015 *)il2cpp_codegen_object_new(ButtonTapped_t3122824015_il2cpp_TypeInfo_var);
		ButtonTapped__ctor_m207266678(L_62, __this, L_61, /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_63 = __this->get_facecamToggleOnAtlasSrc_21();
		TextureAtlasSrc_t2048635151 * L_64 = __this->get_facecamToggleOffAtlasSrc_22();
		ToggleButton_t2881484729 * L_65 = (ToggleButton_t2881484729 *)il2cpp_codegen_object_new(ToggleButton_t2881484729_il2cpp_TypeInfo_var);
		ToggleButton__ctor_m801500299(L_65, L_59, L_60, L_62, L_63, L_64, /*hidden argument*/NULL);
		__this->set_faceCamToggleButton_32(L_65);
		List_1_t2631365083 * L_66 = (List_1_t2631365083 *)il2cpp_codegen_object_new(List_1_t2631365083_il2cpp_TypeInfo_var);
		List_1__ctor_m2532183734(L_66, /*hidden argument*/List_1__ctor_m2532183734_MethodInfo_var);
		__this->set_visibleButtons_34(L_66);
		TextureAtlasSrc_t2048635151 * L_67 = __this->get_bgFooterAtlasSrc_24();
		NullCheck(L_67);
		Rect_t3681755626 * L_68 = L_67->get_address_of_normalizedAtlasRect_1();
		TextureAtlasSrc_t2048635151 * L_69 = __this->get_bgFooterAtlasSrc_24();
		NullCheck(L_69);
		Rect_t3681755626 * L_70 = L_69->get_address_of_normalizedAtlasRect_1();
		float L_71 = Rect_get_y_m1393582395(L_70, /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_72 = __this->get_bgFooterAtlasSrc_24();
		NullCheck(L_72);
		Rect_t3681755626 * L_73 = L_72->get_address_of_normalizedAtlasRect_1();
		float L_74 = Rect_get_height_m3128694305(L_73, /*hidden argument*/NULL);
		Rect_set_y_m4294916608(L_68, ((float)((float)L_71+(float)L_74)), /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_75 = __this->get_bgFooterAtlasSrc_24();
		NullCheck(L_75);
		Rect_t3681755626 * L_76 = L_75->get_address_of_normalizedAtlasRect_1();
		TextureAtlasSrc_t2048635151 * L_77 = __this->get_bgFooterAtlasSrc_24();
		NullCheck(L_77);
		Rect_t3681755626 * L_78 = L_77->get_address_of_normalizedAtlasRect_1();
		float L_79 = Rect_get_height_m3128694305(L_78, /*hidden argument*/NULL);
		Rect_set_height_m2019122814(L_76, ((-L_79)), /*hidden argument*/NULL);
		Button_t3262243951 * L_80 = __this->get_startRecordingButton_30();
		EveryplayRecButtons_SetButtonVisible_m3014996681(__this, L_80, (bool)1, /*hidden argument*/NULL);
		Button_t3262243951 * L_81 = __this->get_openEveryplayButton_29();
		EveryplayRecButtons_SetButtonVisible_m3014996681(__this, L_81, (bool)1, /*hidden argument*/NULL);
		ToggleButton_t2881484729 * L_82 = __this->get_faceCamToggleButton_32();
		EveryplayRecButtons_SetButtonVisible_m3014996681(__this, L_82, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_83 = Everyplay_IsRecordingSupported_m144924673(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_83)
		{
			goto IL_031e;
		}
	}
	{
		Button_t3262243951 * L_84 = __this->get_startRecordingButton_30();
		NullCheck(L_84);
		L_84->set_enabled_0((bool)0);
		Button_t3262243951 * L_85 = __this->get_stopRecordingButton_31();
		NullCheck(L_85);
		L_85->set_enabled_0((bool)0);
	}

IL_031e:
	{
		IntPtr_t L_86;
		L_86.set_m_value_0((void*)(void*)EveryplayRecButtons_RecordingStarted_m2508165025_MethodInfo_var);
		RecordingStartedDelegate_t5060419 * L_87 = (RecordingStartedDelegate_t5060419 *)il2cpp_codegen_object_new(RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var);
		RecordingStartedDelegate__ctor_m1828386050(L_87, __this, L_86, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_add_RecordingStarted_m1975802831(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
		IntPtr_t L_88;
		L_88.set_m_value_0((void*)(void*)EveryplayRecButtons_RecordingStopped_m2345808949_MethodInfo_var);
		RecordingStoppedDelegate_t3008025639 * L_89 = (RecordingStoppedDelegate_t3008025639 *)il2cpp_codegen_object_new(RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var);
		RecordingStoppedDelegate__ctor_m1537694926(L_89, __this, L_88, /*hidden argument*/NULL);
		Everyplay_add_RecordingStopped_m2601012783(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
		IntPtr_t L_90;
		L_90.set_m_value_0((void*)(void*)EveryplayRecButtons_ReadyForRecording_m1360139799_MethodInfo_var);
		ReadyForRecordingDelegate_t1593758596 * L_91 = (ReadyForRecordingDelegate_t1593758596 *)il2cpp_codegen_object_new(ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var);
		ReadyForRecordingDelegate__ctor_m4085498349(L_91, __this, L_90, /*hidden argument*/NULL);
		Everyplay_add_ReadyForRecording_m3347212935(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		IntPtr_t L_92;
		L_92.set_m_value_0((void*)(void*)EveryplayRecButtons_FaceCamRecordingPermission_m3188918626_MethodInfo_var);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_93 = (FaceCamRecordingPermissionDelegate_t1670731619 *)il2cpp_codegen_object_new(FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var);
		FaceCamRecordingPermissionDelegate__ctor_m539527746(L_93, __this, L_92, /*hidden argument*/NULL);
		Everyplay_add_FaceCamRecordingPermission_m574143351(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::Destroy()
extern Il2CppClass* RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var;
extern Il2CppClass* FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayRecButtons_RecordingStarted_m2508165025_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_RecordingStopped_m2345808949_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_ReadyForRecording_m1360139799_MethodInfo_var;
extern const MethodInfo* EveryplayRecButtons_FaceCamRecordingPermission_m3188918626_MethodInfo_var;
extern const uint32_t EveryplayRecButtons_Destroy_m1427959579_MetadataUsageId;
extern "C"  void EveryplayRecButtons_Destroy_m1427959579 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_Destroy_m1427959579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)EveryplayRecButtons_RecordingStarted_m2508165025_MethodInfo_var);
		RecordingStartedDelegate_t5060419 * L_1 = (RecordingStartedDelegate_t5060419 *)il2cpp_codegen_object_new(RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var);
		RecordingStartedDelegate__ctor_m1828386050(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_remove_RecordingStarted_m1408178652(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)EveryplayRecButtons_RecordingStopped_m2345808949_MethodInfo_var);
		RecordingStoppedDelegate_t3008025639 * L_3 = (RecordingStoppedDelegate_t3008025639 *)il2cpp_codegen_object_new(RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var);
		RecordingStoppedDelegate__ctor_m1537694926(L_3, __this, L_2, /*hidden argument*/NULL);
		Everyplay_remove_RecordingStopped_m550227836(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)EveryplayRecButtons_ReadyForRecording_m1360139799_MethodInfo_var);
		ReadyForRecordingDelegate_t1593758596 * L_5 = (ReadyForRecordingDelegate_t1593758596 *)il2cpp_codegen_object_new(ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var);
		ReadyForRecordingDelegate__ctor_m4085498349(L_5, __this, L_4, /*hidden argument*/NULL);
		Everyplay_remove_ReadyForRecording_m557751688(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)EveryplayRecButtons_FaceCamRecordingPermission_m3188918626_MethodInfo_var);
		FaceCamRecordingPermissionDelegate_t1670731619 * L_7 = (FaceCamRecordingPermissionDelegate_t1670731619 *)il2cpp_codegen_object_new(FaceCamRecordingPermissionDelegate_t1670731619_il2cpp_TypeInfo_var);
		FaceCamRecordingPermissionDelegate__ctor_m539527746(L_7, __this, L_6, /*hidden argument*/NULL);
		Everyplay_remove_FaceCamRecordingPermission_m4283361868(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::SetButtonVisible(EveryplayRecButtons/Button,System.Boolean)
extern const MethodInfo* List_1_Contains_m3190811608_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m2073967865_MethodInfo_var;
extern const MethodInfo* List_1_Add_m424405106_MethodInfo_var;
extern const uint32_t EveryplayRecButtons_SetButtonVisible_m3014996681_MetadataUsageId;
extern "C"  void EveryplayRecButtons_SetButtonVisible_m3014996681 (EveryplayRecButtons_t2003903990 * __this, Button_t3262243951 * ___button0, bool ___visible1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_SetButtonVisible_m3014996681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2631365083 * L_0 = __this->get_visibleButtons_34();
		Button_t3262243951 * L_1 = ___button0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m3190811608(L_0, L_1, /*hidden argument*/List_1_Contains_m3190811608_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		bool L_3 = ___visible1;
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		List_1_t2631365083 * L_4 = __this->get_visibleButtons_34();
		Button_t3262243951 * L_5 = ___button0;
		NullCheck(L_4);
		List_1_Remove_m2073967865(L_4, L_5, /*hidden argument*/List_1_Remove_m2073967865_MethodInfo_var);
	}

IL_0024:
	{
		goto IL_003b;
	}

IL_0029:
	{
		bool L_6 = ___visible1;
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		List_1_t2631365083 * L_7 = __this->get_visibleButtons_34();
		Button_t3262243951 * L_8 = ___button0;
		NullCheck(L_7);
		List_1_Add_m424405106(L_7, L_8, /*hidden argument*/List_1_Add_m424405106_MethodInfo_var);
	}

IL_003b:
	{
		return;
	}
}
// System.Void EveryplayRecButtons::ReplaceVisibleButton(EveryplayRecButtons/Button,EveryplayRecButtons/Button)
extern const MethodInfo* List_1_IndexOf_m3720380274_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m4240420148_MethodInfo_var;
extern const uint32_t EveryplayRecButtons_ReplaceVisibleButton_m2850053193_MetadataUsageId;
extern "C"  void EveryplayRecButtons_ReplaceVisibleButton_m2850053193 (EveryplayRecButtons_t2003903990 * __this, Button_t3262243951 * ___button0, Button_t3262243951 * ___replacementButton1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_ReplaceVisibleButton_m2850053193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t2631365083 * L_0 = __this->get_visibleButtons_34();
		Button_t3262243951 * L_1 = ___button0;
		NullCheck(L_0);
		int32_t L_2 = List_1_IndexOf_m3720380274(L_0, L_1, /*hidden argument*/List_1_IndexOf_m3720380274_MethodInfo_var);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0021;
		}
	}
	{
		List_1_t2631365083 * L_4 = __this->get_visibleButtons_34();
		int32_t L_5 = V_0;
		Button_t3262243951 * L_6 = ___replacementButton1;
		NullCheck(L_4);
		List_1_set_Item_m4240420148(L_4, L_5, L_6, /*hidden argument*/List_1_set_Item_m4240420148_MethodInfo_var);
	}

IL_0021:
	{
		return;
	}
}
// System.Void EveryplayRecButtons::StartRecording()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayRecButtons_StartRecording_m1425756496_MetadataUsageId;
extern "C"  void EveryplayRecButtons_StartRecording_m1425756496 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_StartRecording_m1425756496_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_StartRecording_m3582706357(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::StopRecording()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayRecButtons_StopRecording_m1817812284_MetadataUsageId;
extern "C"  void EveryplayRecButtons_StopRecording_m1817812284 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_StopRecording_m1817812284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_StopRecording_m3761179305(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::RecordingStarted()
extern "C"  void EveryplayRecButtons_RecordingStarted_m2508165025 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	{
		Button_t3262243951 * L_0 = __this->get_startRecordingButton_30();
		Button_t3262243951 * L_1 = __this->get_stopRecordingButton_31();
		EveryplayRecButtons_ReplaceVisibleButton_m2850053193(__this, L_0, L_1, /*hidden argument*/NULL);
		Button_t3262243951 * L_2 = __this->get_shareVideoButton_27();
		EveryplayRecButtons_SetButtonVisible_m3014996681(__this, L_2, (bool)0, /*hidden argument*/NULL);
		Button_t3262243951 * L_3 = __this->get_editVideoButton_28();
		EveryplayRecButtons_SetButtonVisible_m3014996681(__this, L_3, (bool)0, /*hidden argument*/NULL);
		ToggleButton_t2881484729 * L_4 = __this->get_faceCamToggleButton_32();
		EveryplayRecButtons_SetButtonVisible_m3014996681(__this, L_4, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::RecordingStopped()
extern "C"  void EveryplayRecButtons_RecordingStopped_m2345808949 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	{
		Button_t3262243951 * L_0 = __this->get_stopRecordingButton_31();
		Button_t3262243951 * L_1 = __this->get_startRecordingButton_30();
		EveryplayRecButtons_ReplaceVisibleButton_m2850053193(__this, L_0, L_1, /*hidden argument*/NULL);
		Button_t3262243951 * L_2 = __this->get_shareVideoButton_27();
		EveryplayRecButtons_SetButtonVisible_m3014996681(__this, L_2, (bool)1, /*hidden argument*/NULL);
		Button_t3262243951 * L_3 = __this->get_editVideoButton_28();
		EveryplayRecButtons_SetButtonVisible_m3014996681(__this, L_3, (bool)1, /*hidden argument*/NULL);
		ToggleButton_t2881484729 * L_4 = __this->get_faceCamToggleButton_32();
		EveryplayRecButtons_SetButtonVisible_m3014996681(__this, L_4, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::ReadyForRecording(System.Boolean)
extern "C"  void EveryplayRecButtons_ReadyForRecording_m1360139799 (EveryplayRecButtons_t2003903990 * __this, bool ___enabled0, const MethodInfo* method)
{
	{
		Button_t3262243951 * L_0 = __this->get_startRecordingButton_30();
		bool L_1 = ___enabled0;
		NullCheck(L_0);
		L_0->set_enabled_0(L_1);
		Button_t3262243951 * L_2 = __this->get_stopRecordingButton_31();
		bool L_3 = ___enabled0;
		NullCheck(L_2);
		L_2->set_enabled_0(L_3);
		return;
	}
}
// System.Void EveryplayRecButtons::FaceCamRecordingPermission(System.Boolean)
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayRecButtons_FaceCamRecordingPermission_m3188918626_MetadataUsageId;
extern "C"  void EveryplayRecButtons_FaceCamRecordingPermission_m3188918626 (EveryplayRecButtons_t2003903990 * __this, bool ___granted0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_FaceCamRecordingPermission_m3188918626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___granted0;
		__this->set_faceCamPermissionGranted_13(L_0);
		bool L_1 = __this->get_startFaceCamWhenPermissionGranted_14();
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		ToggleButton_t2881484729 * L_2 = __this->get_faceCamToggleButton_32();
		bool L_3 = ___granted0;
		NullCheck(L_2);
		L_2->set_toggled_7(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_FaceCamStartSession_m2600985758(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = Everyplay_FaceCamIsSessionRunning_m3953733397(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		__this->set_startFaceCamWhenPermissionGranted_14((bool)0);
	}

IL_0034:
	{
		return;
	}
}
// System.Void EveryplayRecButtons::FaceCamToggle()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayRecButtons_FaceCamToggle_m1400819525_MetadataUsageId;
extern "C"  void EveryplayRecButtons_FaceCamToggle_m1400819525 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_FaceCamToggle_m1400819525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_faceCamPermissionGranted_13();
		if (!L_0)
		{
			goto IL_005c;
		}
	}
	{
		ToggleButton_t2881484729 * L_1 = __this->get_faceCamToggleButton_32();
		ToggleButton_t2881484729 * L_2 = __this->get_faceCamToggleButton_32();
		NullCheck(L_2);
		bool L_3 = L_2->get_toggled_7();
		NullCheck(L_1);
		L_1->set_toggled_7((bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0));
		ToggleButton_t2881484729 * L_4 = __this->get_faceCamToggleButton_32();
		NullCheck(L_4);
		bool L_5 = L_4->get_toggled_7();
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_6 = Everyplay_FaceCamIsSessionRunning_m3953733397(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_FaceCamStartSession_m2600985758(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0043:
	{
		goto IL_0057;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_7 = Everyplay_FaceCamIsSessionRunning_m3953733397(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_FaceCamStopSession_m1826517360(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0057:
	{
		goto IL_0068;
	}

IL_005c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_FaceCamRequestRecordingPermission_m3770143701(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_startFaceCamWhenPermissionGranted_14((bool)1);
	}

IL_0068:
	{
		return;
	}
}
// System.Void EveryplayRecButtons::OpenEveryplay()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayRecButtons_OpenEveryplay_m4060651158_MetadataUsageId;
extern "C"  void EveryplayRecButtons_OpenEveryplay_m4060651158 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_OpenEveryplay_m4060651158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_Show_m681744495(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::EditVideo()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayRecButtons_EditVideo_m3846525876_MetadataUsageId;
extern "C"  void EveryplayRecButtons_EditVideo_m3846525876 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_EditVideo_m3846525876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_PlayLastRecording_m123861831(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::ShareVideo()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayRecButtons_ShareVideo_m1973277741_MetadataUsageId;
extern "C"  void EveryplayRecButtons_ShareVideo_m1973277741 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_ShareVideo_m1973277741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_ShowSharingModal_m2400588728(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2017216959_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3398575995_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3703810831_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m463829143_MethodInfo_var;
extern const uint32_t EveryplayRecButtons_Update_m1198130542_MetadataUsageId;
extern "C"  void EveryplayRecButtons_Update_m1198130542 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_Update_m1198130542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t407273883  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TouchU5BU5D_t3887265178* V_1 = NULL;
	int32_t V_2 = 0;
	Button_t3262243951 * V_3 = NULL;
	Enumerator_t2166094757  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Button_t3262243951 * V_7 = NULL;
	Enumerator_t2166094757  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector2_t2243707579  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		TouchU5BU5D_t3887265178* L_0 = Input_get_touches_m388011594(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_0;
		V_2 = 0;
		goto IL_0197;
	}

IL_000d:
	{
		TouchU5BU5D_t3887265178* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		V_0 = (*(Touch_t407273883 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		int32_t L_3 = Touch_get_phase_m196706494((&V_0), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_00bf;
		}
	}
	{
		List_1_t2631365083 * L_4 = __this->get_visibleButtons_34();
		NullCheck(L_4);
		Enumerator_t2166094757  L_5 = List_1_GetEnumerator_m2017216959(L_4, /*hidden argument*/List_1_GetEnumerator_m2017216959_MethodInfo_var);
		V_4 = L_5;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		{
			goto IL_009b;
		}

IL_0038:
		{
			Button_t3262243951 * L_6 = Enumerator_get_Current_m3398575995((&V_4), /*hidden argument*/Enumerator_get_Current_m3398575995_MethodInfo_var);
			V_3 = L_6;
			Button_t3262243951 * L_7 = V_3;
			NullCheck(L_7);
			Rect_t3681755626 * L_8 = L_7->get_address_of_screenRect_1();
			Vector2_t2243707579  L_9 = Touch_get_position_m2079703643((&V_0), /*hidden argument*/NULL);
			V_5 = L_9;
			float L_10 = (&V_5)->get_x_0();
			Vector2_t2243707579 * L_11 = __this->get_address_of_containerOffset_5();
			float L_12 = L_11->get_x_0();
			int32_t L_13 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
			Vector2_t2243707579  L_14 = Touch_get_position_m2079703643((&V_0), /*hidden argument*/NULL);
			V_6 = L_14;
			float L_15 = (&V_6)->get_y_1();
			Vector2_t2243707579 * L_16 = __this->get_address_of_containerOffset_5();
			float L_17 = L_16->get_y_1();
			Vector2_t2243707579  L_18;
			memset(&L_18, 0, sizeof(L_18));
			Vector2__ctor_m3067419446(&L_18, ((float)((float)L_10-(float)L_12)), ((float)((float)((float)((float)(((float)((float)L_13)))-(float)L_15))-(float)L_17)), /*hidden argument*/NULL);
			bool L_19 = Rect_Contains_m1334685290(L_8, L_18, /*hidden argument*/NULL);
			if (!L_19)
			{
				goto IL_009b;
			}
		}

IL_0094:
		{
			Button_t3262243951 * L_20 = V_3;
			__this->set_tappedButton_33(L_20);
		}

IL_009b:
		{
			bool L_21 = Enumerator_MoveNext_m3703810831((&V_4), /*hidden argument*/Enumerator_MoveNext_m3703810831_MethodInfo_var);
			if (L_21)
			{
				goto IL_0038;
			}
		}

IL_00a7:
		{
			IL2CPP_LEAVE(0xBA, FINALLY_00ac);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ac;
	}

FINALLY_00ac:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m463829143((&V_4), /*hidden argument*/Enumerator_Dispose_m463829143_MethodInfo_var);
		IL2CPP_END_FINALLY(172)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(172)
	{
		IL2CPP_JUMP_TBL(0xBA, IL_00ba)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ba:
	{
		goto IL_0193;
	}

IL_00bf:
	{
		int32_t L_22 = Touch_get_phase_m196706494((&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_22) == ((uint32_t)3))))
		{
			goto IL_017f;
		}
	}
	{
		List_1_t2631365083 * L_23 = __this->get_visibleButtons_34();
		NullCheck(L_23);
		Enumerator_t2166094757  L_24 = List_1_GetEnumerator_m2017216959(L_23, /*hidden argument*/List_1_GetEnumerator_m2017216959_MethodInfo_var);
		V_8 = L_24;
	}

IL_00d9:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0154;
		}

IL_00de:
		{
			Button_t3262243951 * L_25 = Enumerator_get_Current_m3398575995((&V_8), /*hidden argument*/Enumerator_get_Current_m3398575995_MethodInfo_var);
			V_7 = L_25;
			Button_t3262243951 * L_26 = V_7;
			NullCheck(L_26);
			Rect_t3681755626 * L_27 = L_26->get_address_of_screenRect_1();
			Vector2_t2243707579  L_28 = Touch_get_position_m2079703643((&V_0), /*hidden argument*/NULL);
			V_9 = L_28;
			float L_29 = (&V_9)->get_x_0();
			Vector2_t2243707579 * L_30 = __this->get_address_of_containerOffset_5();
			float L_31 = L_30->get_x_0();
			int32_t L_32 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
			Vector2_t2243707579  L_33 = Touch_get_position_m2079703643((&V_0), /*hidden argument*/NULL);
			V_10 = L_33;
			float L_34 = (&V_10)->get_y_1();
			Vector2_t2243707579 * L_35 = __this->get_address_of_containerOffset_5();
			float L_36 = L_35->get_y_1();
			Vector2_t2243707579  L_37;
			memset(&L_37, 0, sizeof(L_37));
			Vector2__ctor_m3067419446(&L_37, ((float)((float)L_29-(float)L_31)), ((float)((float)((float)((float)(((float)((float)L_32)))-(float)L_34))-(float)L_36)), /*hidden argument*/NULL);
			bool L_38 = Rect_Contains_m1334685290(L_27, L_37, /*hidden argument*/NULL);
			if (!L_38)
			{
				goto IL_0154;
			}
		}

IL_013c:
		{
			Button_t3262243951 * L_39 = V_7;
			NullCheck(L_39);
			ButtonTapped_t3122824015 * L_40 = L_39->get_onTap_4();
			if (!L_40)
			{
				goto IL_0154;
			}
		}

IL_0148:
		{
			Button_t3262243951 * L_41 = V_7;
			NullCheck(L_41);
			ButtonTapped_t3122824015 * L_42 = L_41->get_onTap_4();
			NullCheck(L_42);
			ButtonTapped_Invoke_m1052892676(L_42, /*hidden argument*/NULL);
		}

IL_0154:
		{
			bool L_43 = Enumerator_MoveNext_m3703810831((&V_8), /*hidden argument*/Enumerator_MoveNext_m3703810831_MethodInfo_var);
			if (L_43)
			{
				goto IL_00de;
			}
		}

IL_0160:
		{
			IL2CPP_LEAVE(0x173, FINALLY_0165);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0165;
	}

FINALLY_0165:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m463829143((&V_8), /*hidden argument*/Enumerator_Dispose_m463829143_MethodInfo_var);
		IL2CPP_END_FINALLY(357)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(357)
	{
		IL2CPP_JUMP_TBL(0x173, IL_0173)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0173:
	{
		__this->set_tappedButton_33((Button_t3262243951 *)NULL);
		goto IL_0193;
	}

IL_017f:
	{
		int32_t L_44 = Touch_get_phase_m196706494((&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_44) == ((uint32_t)4))))
		{
			goto IL_0193;
		}
	}
	{
		__this->set_tappedButton_33((Button_t3262243951 *)NULL);
	}

IL_0193:
	{
		int32_t L_45 = V_2;
		V_2 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_0197:
	{
		int32_t L_46 = V_2;
		TouchU5BU5D_t3887265178* L_47 = V_1;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}
}
// System.Void EveryplayRecButtons::OnGUI()
extern Il2CppClass* EventType_t3919834026_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayRecButtons_OnGUI_m410492671_MetadataUsageId;
extern "C"  void EveryplayRecButtons_OnGUI_m410492671 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_OnGUI_m410492671_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Event_t3028476042 * L_0 = Event_get_current_m2901774193(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m2426033198(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ((int32_t)7);
		Il2CppObject * L_3 = Box(EventType_t3919834026_il2cpp_TypeInfo_var, &L_2);
		Il2CppObject * L_4 = Box(EventType_t3919834026_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_4);
		bool L_5 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_4, L_3);
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_6 = EveryplayRecButtons_CalculateContainerHeight_m449657747(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = V_1;
		EveryplayRecButtons_UpdateContainerOffset_m1773314275(__this, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		EveryplayRecButtons_DrawBackround_m2870053337(__this, L_8, /*hidden argument*/NULL);
		EveryplayRecButtons_DrawButtons_m2512355848(__this, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void EveryplayRecButtons::DrawTexture(System.Single,System.Single,System.Single,System.Single,UnityEngine.Texture2D,UnityEngine.Rect)
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayRecButtons_DrawTexture_m925030179_MetadataUsageId;
extern "C"  void EveryplayRecButtons_DrawTexture_m925030179 (EveryplayRecButtons_t2003903990 * __this, float ___x0, float ___y1, float ___width2, float ___height3, Texture2D_t3542995729 * ___texture4, Rect_t3681755626  ___uvRect5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_DrawTexture_m925030179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___x0;
		Vector2_t2243707579 * L_1 = __this->get_address_of_containerOffset_5();
		float L_2 = L_1->get_x_0();
		___x0 = ((float)((float)L_0+(float)L_2));
		float L_3 = ___y1;
		Vector2_t2243707579 * L_4 = __this->get_address_of_containerOffset_5();
		float L_5 = L_4->get_y_1();
		___y1 = ((float)((float)L_3+(float)L_5));
		float L_6 = ___x0;
		float L_7 = ___y1;
		float L_8 = ___width2;
		float L_9 = ___height3;
		Rect_t3681755626  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Rect__ctor_m1220545469(&L_10, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_11 = ___texture4;
		Rect_t3681755626  L_12 = ___uvRect5;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_DrawTextureWithTexCoords_m2900470915(NULL /*static, unused*/, L_10, L_11, L_12, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::DrawButtons()
extern const MethodInfo* List_1_GetEnumerator_m2017216959_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3398575995_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3703810831_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m463829143_MethodInfo_var;
extern const uint32_t EveryplayRecButtons_DrawButtons_m2512355848_MetadataUsageId;
extern "C"  void EveryplayRecButtons_DrawButtons_m2512355848 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_DrawButtons_m2512355848_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Button_t3262243951 * V_0 = NULL;
	Enumerator_t2166094757  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Button_t3262243951 * G_B5_0 = NULL;
	EveryplayRecButtons_t2003903990 * G_B5_1 = NULL;
	Button_t3262243951 * G_B4_0 = NULL;
	EveryplayRecButtons_t2003903990 * G_B4_1 = NULL;
	Color_t2020392075  G_B6_0;
	memset(&G_B6_0, 0, sizeof(G_B6_0));
	Button_t3262243951 * G_B6_1 = NULL;
	EveryplayRecButtons_t2003903990 * G_B6_2 = NULL;
	{
		List_1_t2631365083 * L_0 = __this->get_visibleButtons_34();
		NullCheck(L_0);
		Enumerator_t2166094757  L_1 = List_1_GetEnumerator_m2017216959(L_0, /*hidden argument*/List_1_GetEnumerator_m2017216959_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006b;
		}

IL_0011:
		{
			Button_t3262243951 * L_2 = Enumerator_get_Current_m3398575995((&V_1), /*hidden argument*/Enumerator_get_Current_m3398575995_MethodInfo_var);
			V_0 = L_2;
			Button_t3262243951 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = L_3->get_enabled_0();
			if (!L_4)
			{
				goto IL_004b;
			}
		}

IL_0024:
		{
			Button_t3262243951 * L_5 = V_0;
			Button_t3262243951 * L_6 = __this->get_tappedButton_33();
			Button_t3262243951 * L_7 = V_0;
			G_B4_0 = L_5;
			G_B4_1 = __this;
			if ((!(((Il2CppObject*)(Button_t3262243951 *)L_6) == ((Il2CppObject*)(Button_t3262243951 *)L_7))))
			{
				G_B5_0 = L_5;
				G_B5_1 = __this;
				goto IL_003c;
			}
		}

IL_0032:
		{
			Color_t2020392075  L_8 = Color_get_gray_m1396712533(NULL /*static, unused*/, /*hidden argument*/NULL);
			G_B6_0 = L_8;
			G_B6_1 = G_B4_0;
			G_B6_2 = G_B4_1;
			goto IL_0041;
		}

IL_003c:
		{
			Color_t2020392075  L_9 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
			G_B6_0 = L_9;
			G_B6_1 = G_B5_0;
			G_B6_2 = G_B5_1;
		}

IL_0041:
		{
			NullCheck(G_B6_2);
			EveryplayRecButtons_DrawButton_m4200336054(G_B6_2, G_B6_1, G_B6_0, /*hidden argument*/NULL);
			goto IL_006b;
		}

IL_004b:
		{
			Button_t3262243951 * L_10 = V_0;
			Color_t2020392075  L_11;
			memset(&L_11, 0, sizeof(L_11));
			Color__ctor_m1909920690(&L_11, (0.5f), (0.5f), (0.5f), (0.3f), /*hidden argument*/NULL);
			EveryplayRecButtons_DrawButton_m4200336054(__this, L_10, L_11, /*hidden argument*/NULL);
		}

IL_006b:
		{
			bool L_12 = Enumerator_MoveNext_m3703810831((&V_1), /*hidden argument*/Enumerator_MoveNext_m3703810831_MethodInfo_var);
			if (L_12)
			{
				goto IL_0011;
			}
		}

IL_0077:
		{
			IL2CPP_LEAVE(0x8A, FINALLY_007c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007c;
	}

FINALLY_007c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m463829143((&V_1), /*hidden argument*/Enumerator_Dispose_m463829143_MethodInfo_var);
		IL2CPP_END_FINALLY(124)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(124)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008a:
	{
		return;
	}
}
// System.Void EveryplayRecButtons::DrawBackround(System.Int32)
extern "C"  void EveryplayRecButtons_DrawBackround_m2870053337 (EveryplayRecButtons_t2003903990 * __this, int32_t ___containerHeight0, const MethodInfo* method)
{
	{
		TextureAtlasSrc_t2048635151 * L_0 = __this->get_bgHeaderAtlasSrc_23();
		NullCheck(L_0);
		Rect_t3681755626 * L_1 = L_0->get_address_of_atlasRect_0();
		float L_2 = Rect_get_width_m1138015702(L_1, /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_3 = __this->get_bgHeaderAtlasSrc_23();
		NullCheck(L_3);
		Rect_t3681755626 * L_4 = L_3->get_address_of_atlasRect_0();
		float L_5 = Rect_get_height_m3128694305(L_4, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_6 = __this->get_atlasTexture_2();
		TextureAtlasSrc_t2048635151 * L_7 = __this->get_bgHeaderAtlasSrc_23();
		NullCheck(L_7);
		Rect_t3681755626  L_8 = L_7->get_normalizedAtlasRect_1();
		EveryplayRecButtons_DrawTexture_m925030179(__this, (0.0f), (0.0f), L_2, L_5, L_6, L_8, /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_9 = __this->get_bgHeaderAtlasSrc_23();
		NullCheck(L_9);
		Rect_t3681755626 * L_10 = L_9->get_address_of_atlasRect_0();
		float L_11 = Rect_get_height_m3128694305(L_10, /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_12 = __this->get_bgAtlasSrc_25();
		NullCheck(L_12);
		Rect_t3681755626 * L_13 = L_12->get_address_of_atlasRect_0();
		float L_14 = Rect_get_width_m1138015702(L_13, /*hidden argument*/NULL);
		int32_t L_15 = ___containerHeight0;
		TextureAtlasSrc_t2048635151 * L_16 = __this->get_bgHeaderAtlasSrc_23();
		NullCheck(L_16);
		Rect_t3681755626 * L_17 = L_16->get_address_of_atlasRect_0();
		float L_18 = Rect_get_height_m3128694305(L_17, /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_19 = __this->get_bgFooterAtlasSrc_24();
		NullCheck(L_19);
		Rect_t3681755626 * L_20 = L_19->get_address_of_atlasRect_0();
		float L_21 = Rect_get_height_m3128694305(L_20, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_22 = __this->get_atlasTexture_2();
		TextureAtlasSrc_t2048635151 * L_23 = __this->get_bgAtlasSrc_25();
		NullCheck(L_23);
		Rect_t3681755626  L_24 = L_23->get_normalizedAtlasRect_1();
		EveryplayRecButtons_DrawTexture_m925030179(__this, (0.0f), L_11, L_14, ((float)((float)((float)((float)(((float)((float)L_15)))-(float)L_18))-(float)L_21)), L_22, L_24, /*hidden argument*/NULL);
		int32_t L_25 = ___containerHeight0;
		TextureAtlasSrc_t2048635151 * L_26 = __this->get_bgFooterAtlasSrc_24();
		NullCheck(L_26);
		Rect_t3681755626 * L_27 = L_26->get_address_of_atlasRect_0();
		float L_28 = Rect_get_height_m3128694305(L_27, /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_29 = __this->get_bgFooterAtlasSrc_24();
		NullCheck(L_29);
		Rect_t3681755626 * L_30 = L_29->get_address_of_atlasRect_0();
		float L_31 = Rect_get_width_m1138015702(L_30, /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_32 = __this->get_bgFooterAtlasSrc_24();
		NullCheck(L_32);
		Rect_t3681755626 * L_33 = L_32->get_address_of_atlasRect_0();
		float L_34 = Rect_get_height_m3128694305(L_33, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_35 = __this->get_atlasTexture_2();
		TextureAtlasSrc_t2048635151 * L_36 = __this->get_bgFooterAtlasSrc_24();
		NullCheck(L_36);
		Rect_t3681755626  L_37 = L_36->get_normalizedAtlasRect_1();
		EveryplayRecButtons_DrawTexture_m925030179(__this, (0.0f), ((float)((float)(((float)((float)L_25)))-(float)L_28)), L_31, L_34, L_35, L_37, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons::DrawButton(EveryplayRecButtons/Button,UnityEngine.Color)
extern const Il2CppType* ToggleButton_t2881484729_0_0_0_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ToggleButton_t2881484729_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayRecButtons_DrawButton_m4200336054_MetadataUsageId;
extern "C"  void EveryplayRecButtons_DrawButton_m4200336054 (EveryplayRecButtons_t2003903990 * __this, Button_t3262243951 * ___button0, Color_t2020392075  ___tintColor1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_DrawButton_m4200336054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	ToggleButton_t2881484729 * V_2 = NULL;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	TextureAtlasSrc_t2048635151 * V_5 = NULL;
	float V_6 = 0.0f;
	TextureAtlasSrc_t2048635151 * G_B5_0 = NULL;
	int32_t G_B11_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Color_t2020392075  L_0 = GUI_get_color_m1234367343(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ToggleButton_t2881484729_0_0_0_var), /*hidden argument*/NULL);
		Button_t3262243951 * L_2 = ___button0;
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m191970594(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_4 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_1, L_3);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_00ed;
		}
	}
	{
		Button_t3262243951 * L_6 = ___button0;
		V_2 = ((ToggleButton_t2881484729 *)CastclassClass(L_6, ToggleButton_t2881484729_il2cpp_TypeInfo_var));
		Button_t3262243951 * L_7 = ___button0;
		if (!L_7)
		{
			goto IL_00e8;
		}
	}
	{
		Button_t3262243951 * L_8 = ___button0;
		NullCheck(L_8);
		Rect_t3681755626 * L_9 = L_8->get_address_of_screenRect_1();
		float L_10 = Rect_get_x_m1393582490(L_9, /*hidden argument*/NULL);
		Button_t3262243951 * L_11 = ___button0;
		NullCheck(L_11);
		Rect_t3681755626 * L_12 = L_11->get_address_of_screenRect_1();
		float L_13 = Rect_get_width_m1138015702(L_12, /*hidden argument*/NULL);
		ToggleButton_t2881484729 * L_14 = V_2;
		NullCheck(L_14);
		TextureAtlasSrc_t2048635151 * L_15 = L_14->get_toggleOn_5();
		NullCheck(L_15);
		Rect_t3681755626 * L_16 = L_15->get_address_of_atlasRect_0();
		float L_17 = Rect_get_width_m1138015702(L_16, /*hidden argument*/NULL);
		V_3 = ((float)((float)((float)((float)L_10+(float)L_13))-(float)L_17));
		Button_t3262243951 * L_18 = ___button0;
		NullCheck(L_18);
		Rect_t3681755626 * L_19 = L_18->get_address_of_screenRect_1();
		float L_20 = Rect_get_y_m1393582395(L_19, /*hidden argument*/NULL);
		Button_t3262243951 * L_21 = ___button0;
		NullCheck(L_21);
		Rect_t3681755626 * L_22 = L_21->get_address_of_screenRect_1();
		float L_23 = Rect_get_height_m3128694305(L_22, /*hidden argument*/NULL);
		ToggleButton_t2881484729 * L_24 = V_2;
		NullCheck(L_24);
		TextureAtlasSrc_t2048635151 * L_25 = L_24->get_toggleOn_5();
		NullCheck(L_25);
		Rect_t3681755626 * L_26 = L_25->get_address_of_atlasRect_0();
		float L_27 = Rect_get_height_m3128694305(L_26, /*hidden argument*/NULL);
		V_4 = ((float)((float)((float)((float)L_20+(float)((float)((float)L_23/(float)(2.0f)))))-(float)((float)((float)L_27/(float)(2.0f)))));
		ToggleButton_t2881484729 * L_28 = V_2;
		NullCheck(L_28);
		bool L_29 = L_28->get_toggled_7();
		if (!L_29)
		{
			goto IL_00a6;
		}
	}
	{
		ToggleButton_t2881484729 * L_30 = V_2;
		NullCheck(L_30);
		TextureAtlasSrc_t2048635151 * L_31 = L_30->get_toggleOn_5();
		G_B5_0 = L_31;
		goto IL_00ac;
	}

IL_00a6:
	{
		ToggleButton_t2881484729 * L_32 = V_2;
		NullCheck(L_32);
		TextureAtlasSrc_t2048635151 * L_33 = L_32->get_toggleOff_6();
		G_B5_0 = L_33;
	}

IL_00ac:
	{
		V_5 = G_B5_0;
		Color_t2020392075  L_34 = ___tintColor1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_color_m3547334264(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		float L_35 = V_3;
		float L_36 = V_4;
		TextureAtlasSrc_t2048635151 * L_37 = V_5;
		NullCheck(L_37);
		Rect_t3681755626 * L_38 = L_37->get_address_of_atlasRect_0();
		float L_39 = Rect_get_width_m1138015702(L_38, /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_40 = V_5;
		NullCheck(L_40);
		Rect_t3681755626 * L_41 = L_40->get_address_of_atlasRect_0();
		float L_42 = Rect_get_height_m3128694305(L_41, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_43 = __this->get_atlasTexture_2();
		TextureAtlasSrc_t2048635151 * L_44 = V_5;
		NullCheck(L_44);
		Rect_t3681755626  L_45 = L_44->get_normalizedAtlasRect_1();
		EveryplayRecButtons_DrawTexture_m925030179(__this, L_35, L_36, L_39, L_42, L_43, L_45, /*hidden argument*/NULL);
		Color_t2020392075  L_46 = V_0;
		GUI_set_color_m3547334264(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		goto IL_0146;
	}

IL_00ed:
	{
		Color_t2020392075  L_47 = ___tintColor1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_color_m3547334264(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		Button_t3262243951 * L_48 = ___button0;
		NullCheck(L_48);
		Rect_t3681755626 * L_49 = L_48->get_address_of_screenRect_1();
		float L_50 = Rect_get_x_m1393582490(L_49, /*hidden argument*/NULL);
		Button_t3262243951 * L_51 = ___button0;
		NullCheck(L_51);
		Rect_t3681755626 * L_52 = L_51->get_address_of_screenRect_1();
		float L_53 = Rect_get_y_m1393582395(L_52, /*hidden argument*/NULL);
		Button_t3262243951 * L_54 = ___button0;
		NullCheck(L_54);
		TextureAtlasSrc_t2048635151 * L_55 = L_54->get_bg_2();
		NullCheck(L_55);
		Rect_t3681755626 * L_56 = L_55->get_address_of_atlasRect_0();
		float L_57 = Rect_get_width_m1138015702(L_56, /*hidden argument*/NULL);
		Button_t3262243951 * L_58 = ___button0;
		NullCheck(L_58);
		TextureAtlasSrc_t2048635151 * L_59 = L_58->get_bg_2();
		NullCheck(L_59);
		Rect_t3681755626 * L_60 = L_59->get_address_of_atlasRect_0();
		float L_61 = Rect_get_height_m3128694305(L_60, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_62 = __this->get_atlasTexture_2();
		Button_t3262243951 * L_63 = ___button0;
		NullCheck(L_63);
		TextureAtlasSrc_t2048635151 * L_64 = L_63->get_bg_2();
		NullCheck(L_64);
		Rect_t3681755626  L_65 = L_64->get_normalizedAtlasRect_1();
		EveryplayRecButtons_DrawTexture_m925030179(__this, L_50, L_53, L_57, L_61, L_62, L_65, /*hidden argument*/NULL);
		Color_t2020392075  L_66 = V_0;
		GUI_set_color_m3547334264(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
	}

IL_0146:
	{
		bool L_67 = V_1;
		if (!L_67)
		{
			goto IL_0152;
		}
	}
	{
		G_B11_0 = 0;
		goto IL_0158;
	}

IL_0152:
	{
		int32_t L_68 = __this->get_buttonTitleHorizontalMargin_10();
		G_B11_0 = L_68;
	}

IL_0158:
	{
		V_6 = (((float)((float)G_B11_0)));
		Button_t3262243951 * L_69 = ___button0;
		NullCheck(L_69);
		bool L_70 = L_69->get_enabled_0();
		if (L_70)
		{
			goto IL_016c;
		}
	}
	{
		Color_t2020392075  L_71 = ___tintColor1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_color_m3547334264(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
	}

IL_016c:
	{
		Button_t3262243951 * L_72 = ___button0;
		NullCheck(L_72);
		Rect_t3681755626 * L_73 = L_72->get_address_of_screenRect_1();
		float L_74 = Rect_get_x_m1393582490(L_73, /*hidden argument*/NULL);
		float L_75 = V_6;
		Button_t3262243951 * L_76 = ___button0;
		NullCheck(L_76);
		Rect_t3681755626 * L_77 = L_76->get_address_of_screenRect_1();
		float L_78 = Rect_get_y_m1393582395(L_77, /*hidden argument*/NULL);
		int32_t L_79 = __this->get_buttonTitleVerticalMargin_11();
		Button_t3262243951 * L_80 = ___button0;
		NullCheck(L_80);
		TextureAtlasSrc_t2048635151 * L_81 = L_80->get_title_3();
		NullCheck(L_81);
		Rect_t3681755626 * L_82 = L_81->get_address_of_atlasRect_0();
		float L_83 = Rect_get_width_m1138015702(L_82, /*hidden argument*/NULL);
		Button_t3262243951 * L_84 = ___button0;
		NullCheck(L_84);
		TextureAtlasSrc_t2048635151 * L_85 = L_84->get_title_3();
		NullCheck(L_85);
		Rect_t3681755626 * L_86 = L_85->get_address_of_atlasRect_0();
		float L_87 = Rect_get_height_m3128694305(L_86, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_88 = __this->get_atlasTexture_2();
		Button_t3262243951 * L_89 = ___button0;
		NullCheck(L_89);
		TextureAtlasSrc_t2048635151 * L_90 = L_89->get_title_3();
		NullCheck(L_90);
		Rect_t3681755626  L_91 = L_90->get_normalizedAtlasRect_1();
		EveryplayRecButtons_DrawTexture_m925030179(__this, ((float)((float)L_74+(float)L_75)), ((float)((float)L_78+(float)(((float)((float)L_79))))), L_83, L_87, L_88, L_91, /*hidden argument*/NULL);
		Button_t3262243951 * L_92 = ___button0;
		NullCheck(L_92);
		bool L_93 = L_92->get_enabled_0();
		if (L_93)
		{
			goto IL_01d5;
		}
	}
	{
		Color_t2020392075  L_94 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_color_m3547334264(NULL /*static, unused*/, L_94, /*hidden argument*/NULL);
	}

IL_01d5:
	{
		return;
	}
}
// System.Int32 EveryplayRecButtons::CalculateContainerHeight()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2017216959_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3398575995_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3703810831_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m463829143_MethodInfo_var;
extern const uint32_t EveryplayRecButtons_CalculateContainerHeight_m449657747_MetadataUsageId;
extern "C"  int32_t EveryplayRecButtons_CalculateContainerHeight_m449657747 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_CalculateContainerHeight_m449657747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Button_t3262243951 * V_2 = NULL;
	Enumerator_t2166094757  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (0.0f);
		TextureAtlasSrc_t2048635151 * L_0 = __this->get_bgHeaderAtlasSrc_23();
		NullCheck(L_0);
		Rect_t3681755626 * L_1 = L_0->get_address_of_atlasRect_0();
		float L_2 = Rect_get_height_m3128694305(L_1, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_buttonMargin_12();
		TextureAtlasSrc_t2048635151 * L_4 = __this->get_bgHeaderAtlasSrc_23();
		NullCheck(L_4);
		Rect_t3681755626 * L_5 = L_4->get_address_of_atlasRect_0();
		float L_6 = Rect_get_height_m3128694305(L_5, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2+(float)((float)((float)(((float)((float)((int32_t)((int32_t)L_3*(int32_t)2)))))-(float)L_6))));
		List_1_t2631365083 * L_7 = __this->get_visibleButtons_34();
		NullCheck(L_7);
		Enumerator_t2166094757  L_8 = List_1_GetEnumerator_m2017216959(L_7, /*hidden argument*/List_1_GetEnumerator_m2017216959_MethodInfo_var);
		V_3 = L_8;
	}

IL_003e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b0;
		}

IL_0043:
		{
			Button_t3262243951 * L_9 = Enumerator_get_Current_m3398575995((&V_3), /*hidden argument*/Enumerator_get_Current_m3398575995_MethodInfo_var);
			V_2 = L_9;
			Button_t3262243951 * L_10 = V_2;
			NullCheck(L_10);
			Rect_t3681755626 * L_11 = L_10->get_address_of_screenRect_1();
			TextureAtlasSrc_t2048635151 * L_12 = __this->get_bgAtlasSrc_25();
			NullCheck(L_12);
			Rect_t3681755626 * L_13 = L_12->get_address_of_atlasRect_0();
			float L_14 = Rect_get_width_m1138015702(L_13, /*hidden argument*/NULL);
			Button_t3262243951 * L_15 = V_2;
			NullCheck(L_15);
			Rect_t3681755626 * L_16 = L_15->get_address_of_screenRect_1();
			float L_17 = Rect_get_width_m1138015702(L_16, /*hidden argument*/NULL);
			Rect_set_x_m3783700513(L_11, ((float)((float)((float)((float)L_14-(float)L_17))/(float)(2.0f))), /*hidden argument*/NULL);
			Button_t3262243951 * L_18 = V_2;
			NullCheck(L_18);
			Rect_t3681755626 * L_19 = L_18->get_address_of_screenRect_1();
			float L_20 = V_1;
			Rect_set_y_m4294916608(L_19, L_20, /*hidden argument*/NULL);
			float L_21 = V_1;
			int32_t L_22 = __this->get_buttonMargin_12();
			Button_t3262243951 * L_23 = V_2;
			NullCheck(L_23);
			Rect_t3681755626 * L_24 = L_23->get_address_of_screenRect_1();
			float L_25 = Rect_get_height_m3128694305(L_24, /*hidden argument*/NULL);
			V_1 = ((float)((float)L_21+(float)((float)((float)(((float)((float)L_22)))+(float)L_25))));
			float L_26 = V_0;
			int32_t L_27 = __this->get_buttonMargin_12();
			Button_t3262243951 * L_28 = V_2;
			NullCheck(L_28);
			Rect_t3681755626 * L_29 = L_28->get_address_of_screenRect_1();
			float L_30 = Rect_get_height_m3128694305(L_29, /*hidden argument*/NULL);
			V_0 = ((float)((float)L_26+(float)((float)((float)(((float)((float)L_27)))+(float)L_30))));
		}

IL_00b0:
		{
			bool L_31 = Enumerator_MoveNext_m3703810831((&V_3), /*hidden argument*/Enumerator_MoveNext_m3703810831_MethodInfo_var);
			if (L_31)
			{
				goto IL_0043;
			}
		}

IL_00bc:
		{
			IL2CPP_LEAVE(0xCF, FINALLY_00c1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00c1;
	}

FINALLY_00c1:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m463829143((&V_3), /*hidden argument*/Enumerator_Dispose_m463829143_MethodInfo_var);
		IL2CPP_END_FINALLY(193)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(193)
	{
		IL2CPP_JUMP_TBL(0xCF, IL_00cf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00cf:
	{
		float L_32 = V_0;
		TextureAtlasSrc_t2048635151 * L_33 = __this->get_bgHeaderAtlasSrc_23();
		NullCheck(L_33);
		Rect_t3681755626 * L_34 = L_33->get_address_of_atlasRect_0();
		float L_35 = Rect_get_height_m3128694305(L_34, /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_36 = __this->get_bgFooterAtlasSrc_24();
		NullCheck(L_36);
		Rect_t3681755626 * L_37 = L_36->get_address_of_atlasRect_0();
		float L_38 = Rect_get_height_m3128694305(L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_39 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)((float)((float)L_32+(float)L_35))+(float)L_38)), /*hidden argument*/NULL);
		V_4 = L_39;
		int32_t L_40 = V_4;
		return L_40;
	}
}
// System.Void EveryplayRecButtons::UpdateContainerOffset(System.Int32)
extern "C"  void EveryplayRecButtons_UpdateContainerOffset_m1773314275 (EveryplayRecButtons_t2003903990 * __this, int32_t ___containerHeight0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_origin_3();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0063;
		}
	}
	{
		Vector2_t2243707579 * L_1 = __this->get_address_of_containerOffset_5();
		int32_t L_2 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_3 = __this->get_address_of_containerMargin_4();
		float L_4 = L_3->get_x_0();
		float L_5 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_6 = __this->get_bgAtlasSrc_25();
		NullCheck(L_6);
		Rect_t3681755626 * L_7 = L_6->get_address_of_atlasRect_0();
		float L_8 = Rect_get_width_m1138015702(L_7, /*hidden argument*/NULL);
		L_1->set_x_0(((float)((float)((float)((float)(((float)((float)L_2)))-(float)((float)((float)L_4*(float)L_5))))-(float)L_8)));
		Vector2_t2243707579 * L_9 = __this->get_address_of_containerOffset_5();
		Vector2_t2243707579 * L_10 = __this->get_address_of_containerMargin_4();
		float L_11 = L_10->get_y_1();
		float L_12 = __this->get_containerScaling_6();
		L_9->set_y_1(((float)((float)L_11*(float)L_12)));
		goto IL_015f;
	}

IL_0063:
	{
		int32_t L_13 = __this->get_origin_3();
		if ((!(((uint32_t)L_13) == ((uint32_t)2))))
		{
			goto IL_00b8;
		}
	}
	{
		Vector2_t2243707579 * L_14 = __this->get_address_of_containerOffset_5();
		Vector2_t2243707579 * L_15 = __this->get_address_of_containerMargin_4();
		float L_16 = L_15->get_x_0();
		float L_17 = __this->get_containerScaling_6();
		L_14->set_x_0(((float)((float)L_16*(float)L_17)));
		Vector2_t2243707579 * L_18 = __this->get_address_of_containerOffset_5();
		int32_t L_19 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_20 = __this->get_address_of_containerMargin_4();
		float L_21 = L_20->get_y_1();
		float L_22 = __this->get_containerScaling_6();
		int32_t L_23 = ___containerHeight0;
		L_18->set_y_1(((float)((float)((float)((float)(((float)((float)L_19)))-(float)((float)((float)L_21*(float)L_22))))-(float)(((float)((float)L_23))))));
		goto IL_015f;
	}

IL_00b8:
	{
		int32_t L_24 = __this->get_origin_3();
		if ((!(((uint32_t)L_24) == ((uint32_t)3))))
		{
			goto IL_0125;
		}
	}
	{
		Vector2_t2243707579 * L_25 = __this->get_address_of_containerOffset_5();
		int32_t L_26 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_27 = __this->get_address_of_containerMargin_4();
		float L_28 = L_27->get_x_0();
		float L_29 = __this->get_containerScaling_6();
		TextureAtlasSrc_t2048635151 * L_30 = __this->get_bgAtlasSrc_25();
		NullCheck(L_30);
		Rect_t3681755626 * L_31 = L_30->get_address_of_atlasRect_0();
		float L_32 = Rect_get_width_m1138015702(L_31, /*hidden argument*/NULL);
		L_25->set_x_0(((float)((float)((float)((float)(((float)((float)L_26)))-(float)((float)((float)L_28*(float)L_29))))-(float)L_32)));
		Vector2_t2243707579 * L_33 = __this->get_address_of_containerOffset_5();
		int32_t L_34 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_35 = __this->get_address_of_containerMargin_4();
		float L_36 = L_35->get_y_1();
		float L_37 = __this->get_containerScaling_6();
		int32_t L_38 = ___containerHeight0;
		L_33->set_y_1(((float)((float)((float)((float)(((float)((float)L_34)))-(float)((float)((float)L_36*(float)L_37))))-(float)(((float)((float)L_38))))));
		goto IL_015f;
	}

IL_0125:
	{
		Vector2_t2243707579 * L_39 = __this->get_address_of_containerOffset_5();
		Vector2_t2243707579 * L_40 = __this->get_address_of_containerMargin_4();
		float L_41 = L_40->get_x_0();
		float L_42 = __this->get_containerScaling_6();
		L_39->set_x_0(((float)((float)L_41*(float)L_42)));
		Vector2_t2243707579 * L_43 = __this->get_address_of_containerOffset_5();
		Vector2_t2243707579 * L_44 = __this->get_address_of_containerMargin_4();
		float L_45 = L_44->get_y_1();
		float L_46 = __this->get_containerScaling_6();
		L_43->set_y_1(((float)((float)L_45*(float)L_46)));
	}

IL_015f:
	{
		return;
	}
}
// System.Single EveryplayRecButtons::GetScalingByResolution()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayRecButtons_GetScalingByResolution_m2106876889_MetadataUsageId;
extern "C"  float EveryplayRecButtons_GetScalingByResolution_m2106876889 (EveryplayRecButtons_t2003903990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayRecButtons_GetScalingByResolution_m2106876889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m1875893177(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Mathf_Min_m2906823867(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)640))))
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)1024)))))
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = V_1;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)768)))))
		{
			goto IL_0047;
		}
	}

IL_0041:
	{
		return (0.5f);
	}

IL_0047:
	{
		return (1.0f);
	}
}
// System.Void EveryplayRecButtons/Button::.ctor(EveryplayRecButtons/TextureAtlasSrc,EveryplayRecButtons/TextureAtlasSrc,EveryplayRecButtons/ButtonTapped)
extern "C"  void Button__ctor_m345536917 (Button_t3262243951 * __this, TextureAtlasSrc_t2048635151 * ___bg0, TextureAtlasSrc_t2048635151 * ___title1, ButtonTapped_t3122824015 * ___buttonTapped2, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_enabled_0((bool)1);
		TextureAtlasSrc_t2048635151 * L_0 = ___bg0;
		__this->set_bg_2(L_0);
		TextureAtlasSrc_t2048635151 * L_1 = ___title1;
		__this->set_title_3(L_1);
		Rect_t3681755626 * L_2 = __this->get_address_of_screenRect_1();
		TextureAtlasSrc_t2048635151 * L_3 = ___bg0;
		NullCheck(L_3);
		Rect_t3681755626 * L_4 = L_3->get_address_of_atlasRect_0();
		float L_5 = Rect_get_width_m1138015702(L_4, /*hidden argument*/NULL);
		Rect_set_width_m1921257731(L_2, L_5, /*hidden argument*/NULL);
		Rect_t3681755626 * L_6 = __this->get_address_of_screenRect_1();
		TextureAtlasSrc_t2048635151 * L_7 = ___bg0;
		NullCheck(L_7);
		Rect_t3681755626 * L_8 = L_7->get_address_of_atlasRect_0();
		float L_9 = Rect_get_height_m3128694305(L_8, /*hidden argument*/NULL);
		Rect_set_height_m2019122814(L_6, L_9, /*hidden argument*/NULL);
		ButtonTapped_t3122824015 * L_10 = ___buttonTapped2;
		__this->set_onTap_4(L_10);
		return;
	}
}
// System.Void EveryplayRecButtons/ButtonTapped::.ctor(System.Object,System.IntPtr)
extern "C"  void ButtonTapped__ctor_m207266678 (ButtonTapped_t3122824015 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void EveryplayRecButtons/ButtonTapped::Invoke()
extern "C"  void ButtonTapped_Invoke_m1052892676 (ButtonTapped_t3122824015 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ButtonTapped_Invoke_m1052892676((ButtonTapped_t3122824015 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_ButtonTapped_t3122824015 (ButtonTapped_t3122824015 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult EveryplayRecButtons/ButtonTapped::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ButtonTapped_BeginInvoke_m331452055 (ButtonTapped_t3122824015 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void EveryplayRecButtons/ButtonTapped::EndInvoke(System.IAsyncResult)
extern "C"  void ButtonTapped_EndInvoke_m1076891736 (ButtonTapped_t3122824015 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void EveryplayRecButtons/TextureAtlasSrc::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Single)
extern "C"  void TextureAtlasSrc__ctor_m2103365045 (TextureAtlasSrc_t2048635151 * __this, int32_t ___width0, int32_t ___height1, int32_t ___x2, int32_t ___y3, float ___scale4, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Rect_t3681755626 * L_0 = __this->get_address_of_atlasRect_0();
		int32_t L_1 = ___x2;
		Rect_set_x_m3783700513(L_0, (((float)((float)((int32_t)((int32_t)L_1+(int32_t)2))))), /*hidden argument*/NULL);
		Rect_t3681755626 * L_2 = __this->get_address_of_atlasRect_0();
		int32_t L_3 = ___y3;
		Rect_set_y_m4294916608(L_2, (((float)((float)((int32_t)((int32_t)L_3+(int32_t)2))))), /*hidden argument*/NULL);
		Rect_t3681755626 * L_4 = __this->get_address_of_atlasRect_0();
		int32_t L_5 = ___width0;
		float L_6 = ___scale4;
		Rect_set_width_m1921257731(L_4, ((float)((float)(((float)((float)L_5)))*(float)L_6)), /*hidden argument*/NULL);
		Rect_t3681755626 * L_7 = __this->get_address_of_atlasRect_0();
		int32_t L_8 = ___height1;
		float L_9 = ___scale4;
		Rect_set_height_m2019122814(L_7, ((float)((float)(((float)((float)L_8)))*(float)L_9)), /*hidden argument*/NULL);
		Rect_t3681755626 * L_10 = __this->get_address_of_normalizedAtlasRect_1();
		int32_t L_11 = ___width0;
		Rect_set_width_m1921257731(L_10, ((float)((float)(((float)((float)L_11)))/(float)(256.0f))), /*hidden argument*/NULL);
		Rect_t3681755626 * L_12 = __this->get_address_of_normalizedAtlasRect_1();
		int32_t L_13 = ___height1;
		Rect_set_height_m2019122814(L_12, ((float)((float)(((float)((float)L_13)))/(float)(256.0f))), /*hidden argument*/NULL);
		Rect_t3681755626 * L_14 = __this->get_address_of_normalizedAtlasRect_1();
		Rect_t3681755626 * L_15 = __this->get_address_of_atlasRect_0();
		float L_16 = Rect_get_x_m1393582490(L_15, /*hidden argument*/NULL);
		Rect_set_x_m3783700513(L_14, ((float)((float)L_16/(float)(256.0f))), /*hidden argument*/NULL);
		Rect_t3681755626 * L_17 = __this->get_address_of_normalizedAtlasRect_1();
		Rect_t3681755626 * L_18 = __this->get_address_of_atlasRect_0();
		float L_19 = Rect_get_y_m1393582395(L_18, /*hidden argument*/NULL);
		int32_t L_20 = ___height1;
		Rect_set_y_m4294916608(L_17, ((float)((float)(1.0f)-(float)((float)((float)((float)((float)L_19+(float)(((float)((float)L_20)))))/(float)(256.0f))))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayRecButtons/ToggleButton::.ctor(EveryplayRecButtons/TextureAtlasSrc,EveryplayRecButtons/TextureAtlasSrc,EveryplayRecButtons/ButtonTapped,EveryplayRecButtons/TextureAtlasSrc,EveryplayRecButtons/TextureAtlasSrc)
extern "C"  void ToggleButton__ctor_m801500299 (ToggleButton_t2881484729 * __this, TextureAtlasSrc_t2048635151 * ___bg0, TextureAtlasSrc_t2048635151 * ___title1, ButtonTapped_t3122824015 * ___buttonTapped2, TextureAtlasSrc_t2048635151 * ___toggleOn3, TextureAtlasSrc_t2048635151 * ___toggleOff4, const MethodInfo* method)
{
	{
		TextureAtlasSrc_t2048635151 * L_0 = ___bg0;
		TextureAtlasSrc_t2048635151 * L_1 = ___title1;
		ButtonTapped_t3122824015 * L_2 = ___buttonTapped2;
		Button__ctor_m345536917(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		TextureAtlasSrc_t2048635151 * L_3 = ___toggleOn3;
		__this->set_toggleOn_5(L_3);
		TextureAtlasSrc_t2048635151 * L_4 = ___toggleOff4;
		__this->set_toggleOff_6(L_4);
		return;
	}
}
// System.Void EveryplaySettings::.ctor()
extern Il2CppCodeGenString* _stringLiteral1329738731;
extern const uint32_t EveryplaySettings__ctor_m1650251901_MetadataUsageId;
extern "C"  void EveryplaySettings__ctor_m1650251901 (EveryplaySettings_t83776108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplaySettings__ctor_m1650251901_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_redirectURI_4(_stringLiteral1329738731);
		__this->set_earlyInitializerEnabled_10((bool)1);
		ScriptableObject__ctor_m2671490429(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean EveryplaySettings::get_IsEnabled()
extern "C"  bool EveryplaySettings_get_IsEnabled_m4208345999 (EveryplaySettings_t83776108 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_iosSupportEnabled_5();
		return L_0;
	}
}
// System.Boolean EveryplaySettings::get_IsValid()
extern "C"  bool EveryplaySettings_get_IsValid_m586503010 (EveryplaySettings_t83776108 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_clientId_2();
		if (!L_0)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_1 = __this->get_clientSecret_3();
		if (!L_1)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_2 = __this->get_redirectURI_4();
		if (!L_2)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_3 = __this->get_clientId_2();
		NullCheck(L_3);
		String_t* L_4 = String_Trim_m2668767713(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1606060069(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_6 = __this->get_clientSecret_3();
		NullCheck(L_6);
		String_t* L_7 = String_Trim_m2668767713(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m1606060069(L_7, /*hidden argument*/NULL);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_9 = __this->get_redirectURI_4();
		NullCheck(L_9);
		String_t* L_10 = String_Trim_m2668767713(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m1606060069(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0065;
		}
	}
	{
		return (bool)1;
	}

IL_0065:
	{
		return (bool)0;
	}
}
// System.Void EveryplayTest::.ctor()
extern "C"  void EveryplayTest__ctor_m127066598 (EveryplayTest_t137848885 * __this, const MethodInfo* method)
{
	{
		__this->set_showUploadStatus_2((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayTest::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayTest_Awake_m2134326271_MetadataUsageId;
extern "C"  void EveryplayTest_Awake_m2134326271 (EveryplayTest_t137848885 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayTest_Awake_m2134326271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		bool L_1 = __this->get_showUploadStatus_2();
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		EveryplayTest_CreateUploadStatusLabel_m41225027(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayTest::Start()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidStartDelegate_t1871027361_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidProgressDelegate_t2069570344_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidCompleteDelegate_t1564565876_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayTest_UploadDidStart_m1379149981_MethodInfo_var;
extern const MethodInfo* EveryplayTest_UploadDidProgress_m2604112157_MethodInfo_var;
extern const MethodInfo* EveryplayTest_UploadDidComplete_m399990778_MethodInfo_var;
extern const MethodInfo* EveryplayTest_RecordingStarted_m1058971452_MethodInfo_var;
extern const MethodInfo* EveryplayTest_RecordingStopped_m897046588_MethodInfo_var;
extern const uint32_t EveryplayTest_Start_m849491018_MetadataUsageId;
extern "C"  void EveryplayTest_Start_m849491018 (EveryplayTest_t137848885 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayTest_Start_m849491018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIText_t2411476300 * L_0 = __this->get_uploadStatusLabel_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0044;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)EveryplayTest_UploadDidStart_m1379149981_MethodInfo_var);
		UploadDidStartDelegate_t1871027361 * L_3 = (UploadDidStartDelegate_t1871027361 *)il2cpp_codegen_object_new(UploadDidStartDelegate_t1871027361_il2cpp_TypeInfo_var);
		UploadDidStartDelegate__ctor_m2080745860(L_3, __this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_add_UploadDidStart_m2150585455(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)EveryplayTest_UploadDidProgress_m2604112157_MethodInfo_var);
		UploadDidProgressDelegate_t2069570344 * L_5 = (UploadDidProgressDelegate_t2069570344 *)il2cpp_codegen_object_new(UploadDidProgressDelegate_t2069570344_il2cpp_TypeInfo_var);
		UploadDidProgressDelegate__ctor_m1182991975(L_5, __this, L_4, /*hidden argument*/NULL);
		Everyplay_add_UploadDidProgress_m1561866631(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)EveryplayTest_UploadDidComplete_m399990778_MethodInfo_var);
		UploadDidCompleteDelegate_t1564565876 * L_7 = (UploadDidCompleteDelegate_t1564565876 *)il2cpp_codegen_object_new(UploadDidCompleteDelegate_t1564565876_il2cpp_TypeInfo_var);
		UploadDidCompleteDelegate__ctor_m3967701987(L_7, __this, L_6, /*hidden argument*/NULL);
		Everyplay_add_UploadDidComplete_m176217159(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0044:
	{
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)EveryplayTest_RecordingStarted_m1058971452_MethodInfo_var);
		RecordingStartedDelegate_t5060419 * L_9 = (RecordingStartedDelegate_t5060419 *)il2cpp_codegen_object_new(RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var);
		RecordingStartedDelegate__ctor_m1828386050(L_9, __this, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_add_RecordingStarted_m1975802831(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)EveryplayTest_RecordingStopped_m897046588_MethodInfo_var);
		RecordingStoppedDelegate_t3008025639 * L_11 = (RecordingStoppedDelegate_t3008025639 *)il2cpp_codegen_object_new(RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var);
		RecordingStoppedDelegate__ctor_m1537694926(L_11, __this, L_10, /*hidden argument*/NULL);
		Everyplay_add_RecordingStopped_m2601012783(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayTest::Destroy()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidStartDelegate_t1871027361_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidProgressDelegate_t2069570344_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadDidCompleteDelegate_t1564565876_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayTest_UploadDidStart_m1379149981_MethodInfo_var;
extern const MethodInfo* EveryplayTest_UploadDidProgress_m2604112157_MethodInfo_var;
extern const MethodInfo* EveryplayTest_UploadDidComplete_m399990778_MethodInfo_var;
extern const MethodInfo* EveryplayTest_RecordingStarted_m1058971452_MethodInfo_var;
extern const MethodInfo* EveryplayTest_RecordingStopped_m897046588_MethodInfo_var;
extern const uint32_t EveryplayTest_Destroy_m3770768148_MetadataUsageId;
extern "C"  void EveryplayTest_Destroy_m3770768148 (EveryplayTest_t137848885 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayTest_Destroy_m3770768148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIText_t2411476300 * L_0 = __this->get_uploadStatusLabel_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0044;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)EveryplayTest_UploadDidStart_m1379149981_MethodInfo_var);
		UploadDidStartDelegate_t1871027361 * L_3 = (UploadDidStartDelegate_t1871027361 *)il2cpp_codegen_object_new(UploadDidStartDelegate_t1871027361_il2cpp_TypeInfo_var);
		UploadDidStartDelegate__ctor_m2080745860(L_3, __this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_remove_UploadDidStart_m1799453144(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)EveryplayTest_UploadDidProgress_m2604112157_MethodInfo_var);
		UploadDidProgressDelegate_t2069570344 * L_5 = (UploadDidProgressDelegate_t2069570344 *)il2cpp_codegen_object_new(UploadDidProgressDelegate_t2069570344_il2cpp_TypeInfo_var);
		UploadDidProgressDelegate__ctor_m1182991975(L_5, __this, L_4, /*hidden argument*/NULL);
		Everyplay_remove_UploadDidProgress_m4200454792(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)EveryplayTest_UploadDidComplete_m399990778_MethodInfo_var);
		UploadDidCompleteDelegate_t1564565876 * L_7 = (UploadDidCompleteDelegate_t1564565876 *)il2cpp_codegen_object_new(UploadDidCompleteDelegate_t1564565876_il2cpp_TypeInfo_var);
		UploadDidCompleteDelegate__ctor_m3967701987(L_7, __this, L_6, /*hidden argument*/NULL);
		Everyplay_remove_UploadDidComplete_m3377286600(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0044:
	{
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)EveryplayTest_RecordingStarted_m1058971452_MethodInfo_var);
		RecordingStartedDelegate_t5060419 * L_9 = (RecordingStartedDelegate_t5060419 *)il2cpp_codegen_object_new(RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var);
		RecordingStartedDelegate__ctor_m1828386050(L_9, __this, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_remove_RecordingStarted_m1408178652(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)EveryplayTest_RecordingStopped_m897046588_MethodInfo_var);
		RecordingStoppedDelegate_t3008025639 * L_11 = (RecordingStoppedDelegate_t3008025639 *)il2cpp_codegen_object_new(RecordingStoppedDelegate_t3008025639_il2cpp_TypeInfo_var);
		RecordingStoppedDelegate__ctor_m1537694926(L_11, __this, L_10, /*hidden argument*/NULL);
		Everyplay_remove_RecordingStopped_m550227836(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayTest::RecordingStarted()
extern "C"  void EveryplayTest_RecordingStarted_m1058971452 (EveryplayTest_t137848885 * __this, const MethodInfo* method)
{
	{
		__this->set_isRecording_3((bool)1);
		__this->set_isPaused_4((bool)0);
		__this->set_isRecordingFinished_5((bool)0);
		return;
	}
}
// System.Void EveryplayTest::RecordingStopped()
extern "C"  void EveryplayTest_RecordingStopped_m897046588 (EveryplayTest_t137848885 * __this, const MethodInfo* method)
{
	{
		__this->set_isRecording_3((bool)0);
		__this->set_isRecordingFinished_5((bool)1);
		return;
	}
}
// System.Void EveryplayTest::CreateUploadStatusLabel()
extern const Il2CppType* GUIText_t2411476300_0_0_0_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGUIText_t2411476300_m3222579698_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral944132699;
extern Il2CppCodeGenString* _stringLiteral1276040036;
extern const uint32_t EveryplayTest_CreateUploadStatusLabel_m41225027_MetadataUsageId;
extern "C"  void EveryplayTest_CreateUploadStatusLabel_m41225027 (EveryplayTest_t137848885 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayTest_CreateUploadStatusLabel_m41225027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		TypeU5BU5D_t1664964607* L_0 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(GUIText_t2411476300_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		GameObject_t1756533147 * L_2 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m1633632305(L_2, _stringLiteral944132699, L_0, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007f;
		}
	}
	{
		GameObject_t1756533147 * L_5 = V_0;
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = GameObject_get_transform_m909382139(L_5, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_parent_m3281327839(L_6, L_7, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = V_0;
		NullCheck(L_8);
		GUIText_t2411476300 * L_9 = GameObject_GetComponent_TisGUIText_t2411476300_m3222579698(L_8, /*hidden argument*/GameObject_GetComponent_TisGUIText_t2411476300_m3222579698_MethodInfo_var);
		__this->set_uploadStatusLabel_6(L_9);
		GUIText_t2411476300 * L_10 = __this->get_uploadStatusLabel_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_10, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007f;
		}
	}
	{
		GUIText_t2411476300 * L_12 = __this->get_uploadStatusLabel_6();
		NullCheck(L_12);
		GUIText_set_anchor_m2234105042(L_12, 6, /*hidden argument*/NULL);
		GUIText_t2411476300 * L_13 = __this->get_uploadStatusLabel_6();
		NullCheck(L_13);
		GUIText_set_alignment_m2615389432(L_13, 0, /*hidden argument*/NULL);
		GUIText_t2411476300 * L_14 = __this->get_uploadStatusLabel_6();
		NullCheck(L_14);
		GUIText_set_text_m3758377277(L_14, _stringLiteral1276040036, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void EveryplayTest::UploadDidStart(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3084384435;
extern Il2CppCodeGenString* _stringLiteral185555595;
extern const uint32_t EveryplayTest_UploadDidStart_m1379149981_MetadataUsageId;
extern "C"  void EveryplayTest_UploadDidStart_m1379149981 (EveryplayTest_t137848885 * __this, int32_t ___videoId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayTest_UploadDidStart_m1379149981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIText_t2411476300 * L_0 = __this->get_uploadStatusLabel_6();
		int32_t L_1 = ___videoId0;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral3084384435, L_3, _stringLiteral185555595, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIText_set_text_m3758377277(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayTest::UploadDidProgress(System.Int32,System.Single)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3084384435;
extern Il2CppCodeGenString* _stringLiteral1010958322;
extern Il2CppCodeGenString* _stringLiteral3808702230;
extern const uint32_t EveryplayTest_UploadDidProgress_m2604112157_MetadataUsageId;
extern "C"  void EveryplayTest_UploadDidProgress_m2604112157 (EveryplayTest_t137848885 * __this, int32_t ___videoId0, float ___progress1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayTest_UploadDidProgress_m2604112157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIText_t2411476300 * L_0 = __this->get_uploadStatusLabel_6();
		ObjectU5BU5D_t3614634134* L_1 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral3084384435);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3084384435);
		ObjectU5BU5D_t3614634134* L_2 = L_1;
		int32_t L_3 = ___videoId0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_5);
		ObjectU5BU5D_t3614634134* L_6 = L_2;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral1010958322);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1010958322);
		ObjectU5BU5D_t3614634134* L_7 = L_6;
		float L_8 = ___progress1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_9 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, ((float)((float)(((float)((float)L_8)))*(float)(100.0f))), /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_11);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_7;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3808702230);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3808702230);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m3881798623(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIText_set_text_m3758377277(L_0, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EveryplayTest::UploadDidComplete(System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3084384435;
extern Il2CppCodeGenString* _stringLiteral3282880231;
extern const uint32_t EveryplayTest_UploadDidComplete_m399990778_MetadataUsageId;
extern "C"  void EveryplayTest_UploadDidComplete_m399990778 (EveryplayTest_t137848885 * __this, int32_t ___videoId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayTest_UploadDidComplete_m399990778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIText_t2411476300 * L_0 = __this->get_uploadStatusLabel_6();
		int32_t L_1 = ___videoId0;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral3084384435, L_3, _stringLiteral3282880231, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIText_set_text_m3758377277(L_0, L_4, /*hidden argument*/NULL);
		Il2CppObject * L_5 = EveryplayTest_ResetUploadStatusAfterDelay_m1239864648(__this, (2.0f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator EveryplayTest::ResetUploadStatusAfterDelay(System.Single)
extern Il2CppClass* U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayTest_ResetUploadStatusAfterDelay_m1239864648_MetadataUsageId;
extern "C"  Il2CppObject * EveryplayTest_ResetUploadStatusAfterDelay_m1239864648 (EveryplayTest_t137848885 * __this, float ___time0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayTest_ResetUploadStatusAfterDelay_m1239864648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 * V_0 = NULL;
	{
		U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 * L_0 = (U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 *)il2cpp_codegen_object_new(U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340_il2cpp_TypeInfo_var);
		U3CResetUploadStatusAfterDelayU3Ec__Iterator0__ctor_m964882061(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 * L_1 = V_0;
		float L_2 = ___time0;
		NullCheck(L_1);
		L_1->set_time_0(L_2);
		U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_1(__this);
		U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 * L_4 = V_0;
		return L_4;
	}
}
// System.Void EveryplayTest::OnGUI()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2527155545;
extern Il2CppCodeGenString* _stringLiteral907864917;
extern Il2CppCodeGenString* _stringLiteral3913185469;
extern Il2CppCodeGenString* _stringLiteral2755959291;
extern Il2CppCodeGenString* _stringLiteral878782202;
extern Il2CppCodeGenString* _stringLiteral3211873399;
extern Il2CppCodeGenString* _stringLiteral1423781827;
extern Il2CppCodeGenString* _stringLiteral2906427122;
extern const uint32_t EveryplayTest_OnGUI_m1024825530_MetadataUsageId;
extern "C"  void EveryplayTest_OnGUI_m1024825530 (EveryplayTest_t137848885 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayTest_OnGUI_m1024825530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rect_t3681755626  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m1220545469(&L_0, (10.0f), (10.0f), (138.0f), (48.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_1 = GUI_Button_m3054448581(NULL /*static, unused*/, L_0, _stringLiteral2527155545, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_Show_m681744495(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_002d:
	{
		bool L_2 = __this->get_isRecording_3();
		if (!L_2)
		{
			goto IL_006a;
		}
	}
	{
		Rect_t3681755626  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m1220545469(&L_3, (10.0f), (64.0f), (138.0f), (48.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_4 = GUI_Button_m3054448581(NULL /*static, unused*/, L_3, _stringLiteral907864917, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_StopRecording_m3761179305(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_00a2;
	}

IL_006a:
	{
		bool L_5 = __this->get_isRecording_3();
		if (L_5)
		{
			goto IL_00a2;
		}
	}
	{
		Rect_t3681755626  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m1220545469(&L_6, (10.0f), (64.0f), (138.0f), (48.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_7 = GUI_Button_m3054448581(NULL /*static, unused*/, L_6, _stringLiteral3913185469, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00a2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_StartRecording_m3582706357(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_00a2:
	{
		bool L_8 = __this->get_isRecording_3();
		if (!L_8)
		{
			goto IL_0130;
		}
	}
	{
		bool L_9 = __this->get_isPaused_4();
		if (L_9)
		{
			goto IL_00f1;
		}
	}
	{
		Rect_t3681755626  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Rect__ctor_m1220545469(&L_10, (160.0f), (64.0f), (138.0f), (48.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_11 = GUI_Button_m3054448581(NULL /*static, unused*/, L_10, _stringLiteral2755959291, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00f1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_PauseRecording_m3227906851(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_isPaused_4((bool)1);
		goto IL_0130;
	}

IL_00f1:
	{
		bool L_12 = __this->get_isPaused_4();
		if (!L_12)
		{
			goto IL_0130;
		}
	}
	{
		Rect_t3681755626  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Rect__ctor_m1220545469(&L_13, (160.0f), (64.0f), (138.0f), (48.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_14 = GUI_Button_m3054448581(NULL /*static, unused*/, L_13, _stringLiteral878782202, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0130;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_ResumeRecording_m2915910952(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_isPaused_4((bool)0);
	}

IL_0130:
	{
		bool L_15 = __this->get_isRecordingFinished_5();
		if (!L_15)
		{
			goto IL_0168;
		}
	}
	{
		Rect_t3681755626  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Rect__ctor_m1220545469(&L_16, (10.0f), (118.0f), (138.0f), (48.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_17 = GUI_Button_m3054448581(NULL /*static, unused*/, L_16, _stringLiteral3211873399, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0168;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_PlayLastRecording_m123861831(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0168:
	{
		bool L_18 = __this->get_isRecording_3();
		if (!L_18)
		{
			goto IL_01a0;
		}
	}
	{
		Rect_t3681755626  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Rect__ctor_m1220545469(&L_19, (10.0f), (118.0f), (138.0f), (48.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_20 = GUI_Button_m3054448581(NULL /*static, unused*/, L_19, _stringLiteral1423781827, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_01a0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_TakeThumbnail_m1670072747(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_01a0:
	{
		bool L_21 = __this->get_isRecordingFinished_5();
		if (!L_21)
		{
			goto IL_01d8;
		}
	}
	{
		Rect_t3681755626  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Rect__ctor_m1220545469(&L_22, (10.0f), (172.0f), (138.0f), (48.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_23 = GUI_Button_m3054448581(NULL /*static, unused*/, L_22, _stringLiteral2906427122, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_01d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_ShowSharingModal_m2400588728(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_01d8:
	{
		return;
	}
}
// System.Void EveryplayTest/<ResetUploadStatusAfterDelay>c__Iterator0::.ctor()
extern "C"  void U3CResetUploadStatusAfterDelayU3Ec__Iterator0__ctor_m964882061 (U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean EveryplayTest/<ResetUploadStatusAfterDelay>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1276040036;
extern const uint32_t U3CResetUploadStatusAfterDelayU3Ec__Iterator0_MoveNext_m2175068115_MetadataUsageId;
extern "C"  bool U3CResetUploadStatusAfterDelayU3Ec__Iterator0_MoveNext_m2175068115 (U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CResetUploadStatusAfterDelayU3Ec__Iterator0_MoveNext_m2175068115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0046;
		}
	}
	{
		goto IL_0062;
	}

IL_0021:
	{
		float L_2 = __this->get_time_0();
		WaitForSeconds_t3839502067 * L_3 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U24current_2(L_3);
		bool L_4 = __this->get_U24disposing_3();
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0041:
	{
		goto IL_0064;
	}

IL_0046:
	{
		EveryplayTest_t137848885 * L_5 = __this->get_U24this_1();
		NullCheck(L_5);
		GUIText_t2411476300 * L_6 = L_5->get_uploadStatusLabel_6();
		NullCheck(L_6);
		GUIText_set_text_m3758377277(L_6, _stringLiteral1276040036, /*hidden argument*/NULL);
		__this->set_U24PC_4((-1));
	}

IL_0062:
	{
		return (bool)0;
	}

IL_0064:
	{
		return (bool)1;
	}
}
// System.Object EveryplayTest/<ResetUploadStatusAfterDelay>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CResetUploadStatusAfterDelayU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1933022383 (U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object EveryplayTest/<ResetUploadStatusAfterDelay>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CResetUploadStatusAfterDelayU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2203120199 (U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void EveryplayTest/<ResetUploadStatusAfterDelay>c__Iterator0::Dispose()
extern "C"  void U3CResetUploadStatusAfterDelayU3Ec__Iterator0_Dispose_m3789921272 (U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void EveryplayTest/<ResetUploadStatusAfterDelay>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CResetUploadStatusAfterDelayU3Ec__Iterator0_Reset_m1501353290_MetadataUsageId;
extern "C"  void U3CResetUploadStatusAfterDelayU3Ec__Iterator0_Reset_m1501353290 (U3CResetUploadStatusAfterDelayU3Ec__Iterator0_t3422975340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CResetUploadStatusAfterDelayU3Ec__Iterator0_Reset_m1501353290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EveryplayThumbnailPool::.ctor()
extern "C"  void EveryplayThumbnailPool__ctor_m3938529262 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	{
		__this->set_thumbnailCount_2(4);
		__this->set_thumbnailWidth_3(((int32_t)128));
		__this->set_takeRandomShots_5((bool)1);
		__this->set_textureFormat_6(4);
		__this->set_dontDestroyOnLoad_7((bool)1);
		__this->set_allowOneInstanceOnly_8((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture2D[] EveryplayThumbnailPool::get_thumbnailTextures()
extern "C"  Texture2DU5BU5D_t2724090252* EveryplayThumbnailPool_get_thumbnailTextures_m3919877250 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	{
		Texture2DU5BU5D_t2724090252* L_0 = __this->get_U3CthumbnailTexturesU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void EveryplayThumbnailPool::set_thumbnailTextures(UnityEngine.Texture2D[])
extern "C"  void EveryplayThumbnailPool_set_thumbnailTextures_m3824254701 (EveryplayThumbnailPool_t101914191 * __this, Texture2DU5BU5D_t2724090252* ___value0, const MethodInfo* method)
{
	{
		Texture2DU5BU5D_t2724090252* L_0 = ___value0;
		__this->set_U3CthumbnailTexturesU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Int32 EveryplayThumbnailPool::get_availableThumbnailCount()
extern "C"  int32_t EveryplayThumbnailPool_get_availableThumbnailCount_m4126025113 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CavailableThumbnailCountU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void EveryplayThumbnailPool::set_availableThumbnailCount(System.Int32)
extern "C"  void EveryplayThumbnailPool_set_availableThumbnailCount_m196460788 (EveryplayThumbnailPool_t101914191 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CavailableThumbnailCountU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Single EveryplayThumbnailPool::get_aspectRatio()
extern "C"  float EveryplayThumbnailPool_get_aspectRatio_m1000773640 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_U3CaspectRatioU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void EveryplayThumbnailPool::set_aspectRatio(System.Single)
extern "C"  void EveryplayThumbnailPool_set_aspectRatio_m3386986733 (EveryplayThumbnailPool_t101914191 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CaspectRatioU3Ek__BackingField_11(L_0);
		return;
	}
}
// UnityEngine.Vector2 EveryplayThumbnailPool::get_thumbnailScale()
extern "C"  Vector2_t2243707579  EveryplayThumbnailPool_get_thumbnailScale_m1960597216 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_U3CthumbnailScaleU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void EveryplayThumbnailPool::set_thumbnailScale(UnityEngine.Vector2)
extern "C"  void EveryplayThumbnailPool_set_thumbnailScale_m79783341 (EveryplayThumbnailPool_t101914191 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = ___value0;
		__this->set_U3CthumbnailScaleU3Ek__BackingField_12(L_0);
		return;
	}
}
// System.Void EveryplayThumbnailPool::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayThumbnailPool_OnReadyForRecording_m2992974271_MethodInfo_var;
extern const uint32_t EveryplayThumbnailPool_Awake_m277671193_MetadataUsageId;
extern "C"  void EveryplayThumbnailPool_Awake_m277671193 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayThumbnailPool_Awake_m277671193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_allowOneInstanceOnly_8();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Type_t * L_1 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_2 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)1)))
		{
			goto IL_002e;
		}
	}
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_002e:
	{
		bool L_4 = __this->get_dontDestroyOnLoad_7();
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0044:
	{
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)EveryplayThumbnailPool_OnReadyForRecording_m2992974271_MethodInfo_var);
		ReadyForRecordingDelegate_t1593758596 * L_7 = (ReadyForRecordingDelegate_t1593758596 *)il2cpp_codegen_object_new(ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var);
		ReadyForRecordingDelegate__ctor_m4085498349(L_7, __this, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_add_ReadyForRecording_m3347212935(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void EveryplayThumbnailPool::Start()
extern "C"  void EveryplayThumbnailPool_Start_m3827185342 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		EveryplayThumbnailPool_Initialize_m1620953478(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void EveryplayThumbnailPool::OnReadyForRecording(System.Boolean)
extern "C"  void EveryplayThumbnailPool_OnReadyForRecording_m2992974271 (EveryplayThumbnailPool_t101914191 * __this, bool ___ready0, const MethodInfo* method)
{
	{
		bool L_0 = ___ready0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		EveryplayThumbnailPool_Initialize_m1620953478(__this, /*hidden argument*/NULL);
	}

IL_000c:
	{
		return;
	}
}
// System.Void EveryplayThumbnailPool::Initialize()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2DU5BU5D_t2724090252_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* ThumbnailTextureReadyDelegate_t2948235259_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayThumbnailPool_OnThumbnailReady_m1873378191_MethodInfo_var;
extern const MethodInfo* EveryplayThumbnailPool_OnRecordingStarted_m1912222785_MethodInfo_var;
extern const uint32_t EveryplayThumbnailPool_Initialize_m1620953478_MetadataUsageId;
extern "C"  void EveryplayThumbnailPool_Initialize_m1620953478 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayThumbnailPool_Initialize_m1620953478_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t G_B5_0 = 0;
	Texture2DU5BU5D_t2724090252* G_B5_1 = NULL;
	int32_t G_B4_0 = 0;
	Texture2DU5BU5D_t2724090252* G_B4_1 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	Texture2DU5BU5D_t2724090252* G_B6_2 = NULL;
	int32_t G_B8_0 = 0;
	int32_t G_B8_1 = 0;
	Texture2DU5BU5D_t2724090252* G_B8_2 = NULL;
	int32_t G_B7_0 = 0;
	int32_t G_B7_1 = 0;
	Texture2DU5BU5D_t2724090252* G_B7_2 = NULL;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	int32_t G_B9_2 = 0;
	Texture2DU5BU5D_t2724090252* G_B9_3 = NULL;
	{
		bool L_0 = __this->get_initialized_14();
		if (L_0)
		{
			goto IL_0159;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_1 = Everyplay_IsRecordingSupported_m144924673(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0159;
		}
	}
	{
		int32_t L_2 = __this->get_thumbnailWidth_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_3 = Mathf_Clamp_m3542052159(NULL /*static, unused*/, L_2, ((int32_t)32), ((int32_t)2048), /*hidden argument*/NULL);
		__this->set_thumbnailWidth_3(L_3);
		int32_t L_4 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = Mathf_Min_m2906823867(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		int32_t L_7 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = Mathf_Max_m1875893177(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		EveryplayThumbnailPool_set_aspectRatio_m3386986733(__this, ((float)((float)(((float)((float)L_6)))/(float)(((float)((float)L_9))))), /*hidden argument*/NULL);
		int32_t L_10 = __this->get_thumbnailWidth_3();
		float L_11 = EveryplayThumbnailPool_get_aspectRatio_m1000773640(__this, /*hidden argument*/NULL);
		__this->set_thumbnailHeight_17((((int32_t)((int32_t)((float)((float)(((float)((float)L_10)))*(float)L_11))))));
		__this->set_npotSupported_13((bool)0);
		int32_t L_12 = SystemInfo_get_npotSupport_m748569251(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_npotSupported_13((bool)((((int32_t)((((int32_t)L_12) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0));
		int32_t L_13 = __this->get_thumbnailWidth_3();
		int32_t L_14 = Mathf_NextPowerOfTwo_m489009601(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		int32_t L_15 = __this->get_thumbnailHeight_17();
		int32_t L_16 = Mathf_NextPowerOfTwo_m489009601(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		int32_t L_17 = __this->get_thumbnailCount_2();
		EveryplayThumbnailPool_set_thumbnailTextures_m3824254701(__this, ((Texture2DU5BU5D_t2724090252*)SZArrayNew(Texture2DU5BU5D_t2724090252_il2cpp_TypeInfo_var, (uint32_t)L_17)), /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_0105;
	}

IL_00b1:
	{
		Texture2DU5BU5D_t2724090252* L_18 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(__this, /*hidden argument*/NULL);
		int32_t L_19 = V_2;
		bool L_20 = __this->get_npotSupported_13();
		G_B4_0 = L_19;
		G_B4_1 = L_18;
		if (!L_20)
		{
			G_B5_0 = L_19;
			G_B5_1 = L_18;
			goto IL_00ce;
		}
	}
	{
		int32_t L_21 = __this->get_thumbnailWidth_3();
		G_B6_0 = L_21;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_00cf;
	}

IL_00ce:
	{
		int32_t L_22 = V_0;
		G_B6_0 = L_22;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_00cf:
	{
		bool L_23 = __this->get_npotSupported_13();
		G_B7_0 = G_B6_0;
		G_B7_1 = G_B6_1;
		G_B7_2 = G_B6_2;
		if (!L_23)
		{
			G_B8_0 = G_B6_0;
			G_B8_1 = G_B6_1;
			G_B8_2 = G_B6_2;
			goto IL_00e5;
		}
	}
	{
		int32_t L_24 = __this->get_thumbnailHeight_17();
		G_B9_0 = L_24;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_00e6;
	}

IL_00e5:
	{
		int32_t L_25 = V_1;
		G_B9_0 = L_25;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_00e6:
	{
		int32_t L_26 = __this->get_textureFormat_6();
		Texture2D_t3542995729 * L_27 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m1873923924(L_27, G_B9_1, G_B9_0, L_26, (bool)0, /*hidden argument*/NULL);
		NullCheck(G_B9_3);
		ArrayElementTypeCheck (G_B9_3, L_27);
		(G_B9_3)->SetAt(static_cast<il2cpp_array_size_t>(G_B9_2), (Texture2D_t3542995729 *)L_27);
		Texture2DU5BU5D_t2724090252* L_28 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(__this, /*hidden argument*/NULL);
		int32_t L_29 = V_2;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		Texture2D_t3542995729 * L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_31);
		Texture_set_wrapMode_m333956747(L_31, 1, /*hidden argument*/NULL);
		int32_t L_32 = V_2;
		V_2 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_0105:
	{
		int32_t L_33 = V_2;
		int32_t L_34 = __this->get_thumbnailCount_2();
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_00b1;
		}
	}
	{
		__this->set_currentThumbnailTextureIndex_15(0);
		Texture2DU5BU5D_t2724090252* L_35 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(__this, /*hidden argument*/NULL);
		int32_t L_36 = __this->get_currentThumbnailTextureIndex_15();
		NullCheck(L_35);
		int32_t L_37 = L_36;
		Texture2D_t3542995729 * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_SetThumbnailTargetTexture_m2121221160(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		EveryplayThumbnailPool_SetThumbnailTargetSize_m642502636(__this, /*hidden argument*/NULL);
		IntPtr_t L_39;
		L_39.set_m_value_0((void*)(void*)EveryplayThumbnailPool_OnThumbnailReady_m1873378191_MethodInfo_var);
		ThumbnailTextureReadyDelegate_t2948235259 * L_40 = (ThumbnailTextureReadyDelegate_t2948235259 *)il2cpp_codegen_object_new(ThumbnailTextureReadyDelegate_t2948235259_il2cpp_TypeInfo_var);
		ThumbnailTextureReadyDelegate__ctor_m3605697724(L_40, __this, L_39, /*hidden argument*/NULL);
		Everyplay_add_ThumbnailTextureReady_m1784642183(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		IntPtr_t L_41;
		L_41.set_m_value_0((void*)(void*)EveryplayThumbnailPool_OnRecordingStarted_m1912222785_MethodInfo_var);
		RecordingStartedDelegate_t5060419 * L_42 = (RecordingStartedDelegate_t5060419 *)il2cpp_codegen_object_new(RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var);
		RecordingStartedDelegate__ctor_m1828386050(L_42, __this, L_41, /*hidden argument*/NULL);
		Everyplay_add_RecordingStarted_m1975802831(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		__this->set_initialized_14((bool)1);
	}

IL_0159:
	{
		return;
	}
}
// System.Void EveryplayThumbnailPool::OnRecordingStarted()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayThumbnailPool_OnRecordingStarted_m1912222785_MetadataUsageId;
extern "C"  void EveryplayThumbnailPool_OnRecordingStarted_m1912222785 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayThumbnailPool_OnRecordingStarted_m1912222785_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EveryplayThumbnailPool_set_availableThumbnailCount_m196460788(__this, 0, /*hidden argument*/NULL);
		__this->set_currentThumbnailTextureIndex_15(0);
		Texture2DU5BU5D_t2724090252* L_0 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_currentThumbnailTextureIndex_15();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Texture2D_t3542995729 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_SetThumbnailTargetTexture_m2121221160(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		EveryplayThumbnailPool_SetThumbnailTargetSize_m642502636(__this, /*hidden argument*/NULL);
		bool L_4 = __this->get_takeRandomShots_5();
		if (!L_4)
		{
			goto IL_0051;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_TakeThumbnail_m1670072747(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = Random_Range_m2884721203(NULL /*static, unused*/, (3.0f), (15.0f), /*hidden argument*/NULL);
		__this->set_nextRandomShotTime_16(((float)((float)L_5+(float)L_6)));
	}

IL_0051:
	{
		return;
	}
}
// System.Void EveryplayThumbnailPool::Update()
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayThumbnailPool_Update_m2223714429_MetadataUsageId;
extern "C"  void EveryplayThumbnailPool_Update_m2223714429 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayThumbnailPool_Update_m2223714429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_takeRandomShots_5();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_1 = Everyplay_IsRecording_m568570791(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		bool L_2 = Everyplay_IsPaused_m3073713592(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_004f;
		}
	}
	{
		float L_3 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_nextRandomShotTime_16();
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_TakeThumbnail_m1670072747(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = Random_Range_m2884721203(NULL /*static, unused*/, (3.0f), (15.0f), /*hidden argument*/NULL);
		__this->set_nextRandomShotTime_16(((float)((float)L_5+(float)L_6)));
	}

IL_004f:
	{
		return;
	}
}
// System.Void EveryplayThumbnailPool::OnThumbnailReady(UnityEngine.Texture2D,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayThumbnailPool_OnThumbnailReady_m1873378191_MetadataUsageId;
extern "C"  void EveryplayThumbnailPool_OnThumbnailReady_m1873378191 (EveryplayThumbnailPool_t101914191 * __this, Texture2D_t3542995729 * ___texture0, bool ___portrait1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayThumbnailPool_OnThumbnailReady_m1873378191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture2DU5BU5D_t2724090252* L_0 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_currentThumbnailTextureIndex_15();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Texture2D_t3542995729 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		Texture2D_t3542995729 * L_4 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0079;
		}
	}
	{
		int32_t L_6 = __this->get_currentThumbnailTextureIndex_15();
		__this->set_currentThumbnailTextureIndex_15(((int32_t)((int32_t)L_6+(int32_t)1)));
		int32_t L_7 = __this->get_currentThumbnailTextureIndex_15();
		Texture2DU5BU5D_t2724090252* L_8 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		__this->set_currentThumbnailTextureIndex_15(0);
	}

IL_0040:
	{
		int32_t L_9 = EveryplayThumbnailPool_get_availableThumbnailCount_m4126025113(__this, /*hidden argument*/NULL);
		Texture2DU5BU5D_t2724090252* L_10 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		if ((((int32_t)L_9) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_11 = EveryplayThumbnailPool_get_availableThumbnailCount_m4126025113(__this, /*hidden argument*/NULL);
		EveryplayThumbnailPool_set_availableThumbnailCount_m196460788(__this, ((int32_t)((int32_t)L_11+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0061:
	{
		Texture2DU5BU5D_t2724090252* L_12 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(__this, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_currentThumbnailTextureIndex_15();
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Texture2D_t3542995729 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_SetThumbnailTargetTexture_m2121221160(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		EveryplayThumbnailPool_SetThumbnailTargetSize_m642502636(__this, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void EveryplayThumbnailPool::SetThumbnailTargetSize()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern const uint32_t EveryplayThumbnailPool_SetThumbnailTargetSize_m642502636_MetadataUsageId;
extern "C"  void EveryplayThumbnailPool_SetThumbnailTargetSize_m642502636 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayThumbnailPool_SetThumbnailTargetSize_m642502636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_thumbnailWidth_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_1 = Mathf_NextPowerOfTwo_m489009601(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_thumbnailHeight_17();
		int32_t L_3 = Mathf_NextPowerOfTwo_m489009601(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		bool L_4 = __this->get_npotSupported_13();
		if (!L_4)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_5 = __this->get_thumbnailWidth_3();
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_SetThumbnailTargetTextureWidth_m1053357747(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_thumbnailHeight_17();
		Everyplay_SetThumbnailTargetTextureHeight_m1830783752(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector2__ctor_m3067419446(&L_7, (1.0f), (1.0f), /*hidden argument*/NULL);
		EveryplayThumbnailPool_set_thumbnailScale_m79783341(__this, L_7, /*hidden argument*/NULL);
		goto IL_00b9;
	}

IL_0053:
	{
		bool L_8 = __this->get_pixelPerfect_4();
		if (!L_8)
		{
			goto IL_0098;
		}
	}
	{
		int32_t L_9 = __this->get_thumbnailWidth_3();
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_SetThumbnailTargetTextureWidth_m1053357747(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_thumbnailHeight_17();
		Everyplay_SetThumbnailTargetTextureHeight_m1830783752(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_thumbnailWidth_3();
		int32_t L_12 = V_0;
		int32_t L_13 = __this->get_thumbnailHeight_17();
		int32_t L_14 = V_1;
		Vector2_t2243707579  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector2__ctor_m3067419446(&L_15, ((float)((float)(((float)((float)L_11)))/(float)(((float)((float)L_12))))), ((float)((float)(((float)((float)L_13)))/(float)(((float)((float)L_14))))), /*hidden argument*/NULL);
		EveryplayThumbnailPool_set_thumbnailScale_m79783341(__this, L_15, /*hidden argument*/NULL);
		goto IL_00b9;
	}

IL_0098:
	{
		int32_t L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_SetThumbnailTargetTextureWidth_m1053357747(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		int32_t L_17 = V_1;
		Everyplay_SetThumbnailTargetTextureHeight_m1830783752(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Vector2_t2243707579  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector2__ctor_m3067419446(&L_18, (1.0f), (1.0f), /*hidden argument*/NULL);
		EveryplayThumbnailPool_set_thumbnailScale_m79783341(__this, L_18, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		return;
	}
}
// System.Void EveryplayThumbnailPool::OnDestroy()
extern Il2CppClass* ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var;
extern Il2CppClass* Everyplay_t1799077027_il2cpp_TypeInfo_var;
extern Il2CppClass* RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var;
extern Il2CppClass* ThumbnailTextureReadyDelegate_t2948235259_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* EveryplayThumbnailPool_OnReadyForRecording_m2992974271_MethodInfo_var;
extern const MethodInfo* EveryplayThumbnailPool_OnRecordingStarted_m1912222785_MethodInfo_var;
extern const MethodInfo* EveryplayThumbnailPool_OnThumbnailReady_m1873378191_MethodInfo_var;
extern const uint32_t EveryplayThumbnailPool_OnDestroy_m3028613635_MetadataUsageId;
extern "C"  void EveryplayThumbnailPool_OnDestroy_m3028613635 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EveryplayThumbnailPool_OnDestroy_m3028613635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2D_t3542995729 * V_0 = NULL;
	Texture2DU5BU5D_t2724090252* V_1 = NULL;
	int32_t V_2 = 0;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)EveryplayThumbnailPool_OnReadyForRecording_m2992974271_MethodInfo_var);
		ReadyForRecordingDelegate_t1593758596 * L_1 = (ReadyForRecordingDelegate_t1593758596 *)il2cpp_codegen_object_new(ReadyForRecordingDelegate_t1593758596_il2cpp_TypeInfo_var);
		ReadyForRecordingDelegate__ctor_m4085498349(L_1, __this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_remove_ReadyForRecording_m557751688(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_2 = __this->get_initialized_14();
		if (!L_2)
		{
			goto IL_0083;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Everyplay_t1799077027_il2cpp_TypeInfo_var);
		Everyplay_SetThumbnailTargetTexture_m2121221160(NULL /*static, unused*/, (Texture2D_t3542995729 *)NULL, /*hidden argument*/NULL);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)EveryplayThumbnailPool_OnRecordingStarted_m1912222785_MethodInfo_var);
		RecordingStartedDelegate_t5060419 * L_4 = (RecordingStartedDelegate_t5060419 *)il2cpp_codegen_object_new(RecordingStartedDelegate_t5060419_il2cpp_TypeInfo_var);
		RecordingStartedDelegate__ctor_m1828386050(L_4, __this, L_3, /*hidden argument*/NULL);
		Everyplay_remove_RecordingStarted_m1408178652(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)EveryplayThumbnailPool_OnThumbnailReady_m1873378191_MethodInfo_var);
		ThumbnailTextureReadyDelegate_t2948235259 * L_6 = (ThumbnailTextureReadyDelegate_t2948235259 *)il2cpp_codegen_object_new(ThumbnailTextureReadyDelegate_t2948235259_il2cpp_TypeInfo_var);
		ThumbnailTextureReadyDelegate__ctor_m3605697724(L_6, __this, L_5, /*hidden argument*/NULL);
		Everyplay_remove_ThumbnailTextureReady_m3139354760(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Texture2DU5BU5D_t2724090252* L_7 = EveryplayThumbnailPool_get_thumbnailTextures_m3919877250(__this, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_006c;
	}

IL_0052:
	{
		Texture2DU5BU5D_t2724090252* L_8 = V_1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Texture2D_t3542995729 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_0 = L_11;
		Texture2D_t3542995729 * L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_12, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0068;
		}
	}
	{
		Texture2D_t3542995729 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0068:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_006c:
	{
		int32_t L_16 = V_2;
		Texture2DU5BU5D_t2724090252* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		EveryplayThumbnailPool_set_thumbnailTextures_m3824254701(__this, (Texture2DU5BU5D_t2724090252*)(Texture2DU5BU5D_t2724090252*)NULL, /*hidden argument*/NULL);
		__this->set_initialized_14((bool)0);
	}

IL_0083:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
