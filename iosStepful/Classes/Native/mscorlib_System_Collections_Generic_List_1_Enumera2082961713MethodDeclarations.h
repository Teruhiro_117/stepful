﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchInfoSnapshot>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1796843816(__this, ___l0, method) ((  void (*) (Enumerator_t2082961713 *, List_1_t2548232039 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchInfoSnapshot>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m54798018(__this, method) ((  void (*) (Enumerator_t2082961713 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchInfoSnapshot>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1013177364(__this, method) ((  Il2CppObject * (*) (Enumerator_t2082961713 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchInfoSnapshot>::Dispose()
#define Enumerator_Dispose_m1544291089(__this, method) ((  void (*) (Enumerator_t2082961713 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchInfoSnapshot>::VerifyState()
#define Enumerator_VerifyState_m4285324906(__this, method) ((  void (*) (Enumerator_t2082961713 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchInfoSnapshot>::MoveNext()
#define Enumerator_MoveNext_m1496862918(__this, method) ((  bool (*) (Enumerator_t2082961713 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchInfoSnapshot>::get_Current()
#define Enumerator_get_Current_m2371762953(__this, method) ((  MatchInfoSnapshot_t3179110907 * (*) (Enumerator_t2082961713 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
