﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Futi
struct Futi_t612262014;

#include "codegen/il2cpp-codegen.h"

// System.Void Futi::.ctor()
extern "C"  void Futi__ctor_m1883418413 (Futi_t612262014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Futi::Start()
extern "C"  void Futi_Start_m1837758333 (Futi_t612262014 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
