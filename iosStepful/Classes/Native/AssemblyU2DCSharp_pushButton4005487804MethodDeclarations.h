﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// pushButton
struct pushButton_t4005487804;

#include "codegen/il2cpp-codegen.h"

// System.Void pushButton::.ctor()
extern "C"  void pushButton__ctor_m292811691 (pushButton_t4005487804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void pushButton::Start()
extern "C"  void pushButton_Start_m3144148455 (pushButton_t4005487804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void pushButton::Update()
extern "C"  void pushButton_Update_m44191066 (pushButton_t4005487804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void pushButton::OnClick()
extern "C"  void pushButton_OnClick_m1687122768 (pushButton_t4005487804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void pushButton::OnPointerEnter()
extern "C"  void pushButton_OnPointerEnter_m3031795593 (pushButton_t4005487804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void pushButton::OnPointerExit()
extern "C"  void pushButton_OnPointerExit_m2966859661 (pushButton_t4005487804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
