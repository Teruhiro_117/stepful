﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.GlobalConfig
struct GlobalConfig_t2977893073;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_ReactorModel14967895.h"

// System.Void UnityEngine.Networking.GlobalConfig::.ctor()
extern "C"  void GlobalConfig__ctor_m3202036438 (GlobalConfig_t2977893073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.Networking.GlobalConfig::get_ThreadAwakeTimeout()
extern "C"  uint32_t GlobalConfig_get_ThreadAwakeTimeout_m619243016 (GlobalConfig_t2977893073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.GlobalConfig::set_ThreadAwakeTimeout(System.UInt32)
extern "C"  void GlobalConfig_set_ThreadAwakeTimeout_m1912232805 (GlobalConfig_t2977893073 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.ReactorModel UnityEngine.Networking.GlobalConfig::get_ReactorModel()
extern "C"  int32_t GlobalConfig_get_ReactorModel_m2456742585 (GlobalConfig_t2977893073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 UnityEngine.Networking.GlobalConfig::get_ReactorMaximumReceivedMessages()
extern "C"  uint16_t GlobalConfig_get_ReactorMaximumReceivedMessages_m1438844271 (GlobalConfig_t2977893073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 UnityEngine.Networking.GlobalConfig::get_ReactorMaximumSentMessages()
extern "C"  uint16_t GlobalConfig_get_ReactorMaximumSentMessages_m2100377038 (GlobalConfig_t2977893073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 UnityEngine.Networking.GlobalConfig::get_MaxPacketSize()
extern "C"  uint16_t GlobalConfig_get_MaxPacketSize_m1551167873 (GlobalConfig_t2977893073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
