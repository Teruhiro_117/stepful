﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayEarlyInitializer
struct EveryplayEarlyInitializer_t4267781536;

#include "codegen/il2cpp-codegen.h"

// System.Void EveryplayEarlyInitializer::.ctor()
extern "C"  void EveryplayEarlyInitializer__ctor_m1892662465 (EveryplayEarlyInitializer_t4267781536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayEarlyInitializer::InitializeEveryplayOnStartup()
extern "C"  void EveryplayEarlyInitializer_InitializeEveryplayOnStartup_m1465447962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
