﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.Match.CreateMatchResponse
struct CreateMatchResponse_t1656355148;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID348058649.h"
#include "UnityEngine_UnityEngine_Networking_Types_NodeID3569487935.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityEngine.Networking.Match.CreateMatchResponse::.ctor()
extern "C"  void CreateMatchResponse__ctor_m578071542 (CreateMatchResponse_t1656355148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.CreateMatchResponse::get_address()
extern "C"  String_t* CreateMatchResponse_get_address_m2086994754 (CreateMatchResponse_t1656355148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_address(System.String)
extern "C"  void CreateMatchResponse_set_address_m1597518889 (CreateMatchResponse_t1656355148 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.Match.CreateMatchResponse::get_port()
extern "C"  int32_t CreateMatchResponse_get_port_m373618154 (CreateMatchResponse_t1656355148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_port(System.Int32)
extern "C"  void CreateMatchResponse_set_port_m175521497 (CreateMatchResponse_t1656355148 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.Match.CreateMatchResponse::get_domain()
extern "C"  int32_t CreateMatchResponse_get_domain_m3318180827 (CreateMatchResponse_t1656355148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.CreateMatchResponse::get_networkId()
extern "C"  uint64_t CreateMatchResponse_get_networkId_m3393814418 (CreateMatchResponse_t1656355148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C"  void CreateMatchResponse_set_networkId_m2830929839 (CreateMatchResponse_t1656355148 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.CreateMatchResponse::get_accessTokenString()
extern "C"  String_t* CreateMatchResponse_get_accessTokenString_m104816140 (CreateMatchResponse_t1656355148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_accessTokenString(System.String)
extern "C"  void CreateMatchResponse_set_accessTokenString_m3943204009 (CreateMatchResponse_t1656355148 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.CreateMatchResponse::get_nodeId()
extern "C"  uint16_t CreateMatchResponse_get_nodeId_m2565202086 (CreateMatchResponse_t1656355148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern "C"  void CreateMatchResponse_set_nodeId_m3897359275 (CreateMatchResponse_t1656355148 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.Match.CreateMatchResponse::get_usingRelay()
extern "C"  bool CreateMatchResponse_get_usingRelay_m2528292636 (CreateMatchResponse_t1656355148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_usingRelay(System.Boolean)
extern "C"  void CreateMatchResponse_set_usingRelay_m1234781999 (CreateMatchResponse_t1656355148 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.CreateMatchResponse::ToString()
extern "C"  String_t* CreateMatchResponse_ToString_m2052400895 (CreateMatchResponse_t1656355148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::Parse(System.Object)
extern "C"  void CreateMatchResponse_Parse_m1388008455 (CreateMatchResponse_t1656355148 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
