﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::.ctor()
#define List_1__ctor_m2532183734(__this, method) ((  void (*) (List_1_t2631365083 *, const MethodInfo*))List_1__ctor_m365405030_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m272141894(__this, ___collection0, method) ((  void (*) (List_1_t2631365083 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m454375187_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::.ctor(System.Int32)
#define List_1__ctor_m3541045500(__this, ___capacity0, method) ((  void (*) (List_1_t2631365083 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::.cctor()
#define List_1__cctor_m1426332708(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1840792583(__this, method) ((  Il2CppObject* (*) (List_1_t2631365083 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3726622043(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2631365083 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m773040178(__this, method) ((  Il2CppObject * (*) (List_1_t2631365083 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2094358803(__this, ___item0, method) ((  int32_t (*) (List_1_t2631365083 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2288492819(__this, ___item0, method) ((  bool (*) (List_1_t2631365083 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1877602557(__this, ___item0, method) ((  int32_t (*) (List_1_t2631365083 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3311923952(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2631365083 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3301049550(__this, ___item0, method) ((  void (*) (List_1_t2631365083 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2654911606(__this, method) ((  bool (*) (List_1_t2631365083 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1234049147(__this, method) ((  bool (*) (List_1_t2631365083 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m4001246415(__this, method) ((  Il2CppObject * (*) (List_1_t2631365083 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3839279324(__this, method) ((  bool (*) (List_1_t2631365083 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2516324687(__this, method) ((  bool (*) (List_1_t2631365083 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1897609410(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2631365083 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2972472337(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2631365083 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::Add(T)
#define List_1_Add_m424405106(__this, ___item0, method) ((  void (*) (List_1_t2631365083 *, Button_t3262243951 *, const MethodInfo*))List_1_Add_m567051994_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m3698472871(__this, ___newCount0, method) ((  void (*) (List_1_t2631365083 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1176148751(__this, ___collection0, method) ((  void (*) (List_1_t2631365083 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3663470687(__this, ___enumerable0, method) ((  void (*) (List_1_t2631365083 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m910277802(__this, ___collection0, method) ((  void (*) (List_1_t2631365083 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<EveryplayRecButtons/Button>::AsReadOnly()
#define List_1_AsReadOnly_m642004159(__this, method) ((  ReadOnlyCollection_1_t3448029643 * (*) (List_1_t2631365083 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::Clear()
#define List_1_Clear_m1887608872(__this, method) ((  void (*) (List_1_t2631365083 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<EveryplayRecButtons/Button>::Contains(T)
#define List_1_Contains_m3190811608(__this, ___item0, method) ((  bool (*) (List_1_t2631365083 *, Button_t3262243951 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m560826256(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2631365083 *, ButtonU5BU5D_t2724374774*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<EveryplayRecButtons/Button>::Find(System.Predicate`1<T>)
#define List_1_Find_m3733811694(__this, ___match0, method) ((  Button_t3262243951 * (*) (List_1_t2631365083 *, Predicate_1_t1705214066 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m486015627(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1705214066 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<EveryplayRecButtons/Button>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m249888360(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2631365083 *, int32_t, int32_t, Predicate_1_t1705214066 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<EveryplayRecButtons/Button>::GetEnumerator()
#define List_1_GetEnumerator_m2017216959(__this, method) ((  Enumerator_t2166094757  (*) (List_1_t2631365083 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<EveryplayRecButtons/Button>::IndexOf(T)
#define List_1_IndexOf_m3720380274(__this, ___item0, method) ((  int32_t (*) (List_1_t2631365083 *, Button_t3262243951 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1015826295(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2631365083 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m645805544(__this, ___index0, method) ((  void (*) (List_1_t2631365083 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::Insert(System.Int32,T)
#define List_1_Insert_m2177491317(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2631365083 *, int32_t, Button_t3262243951 *, const MethodInfo*))List_1_Insert_m11735664_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1411845294(__this, ___collection0, method) ((  void (*) (List_1_t2631365083 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<EveryplayRecButtons/Button>::Remove(T)
#define List_1_Remove_m2073967865(__this, ___item0, method) ((  bool (*) (List_1_t2631365083 *, Button_t3262243951 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<EveryplayRecButtons/Button>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3337353483(__this, ___match0, method) ((  int32_t (*) (List_1_t2631365083 *, Predicate_1_t1705214066 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1488976049(__this, ___index0, method) ((  void (*) (List_1_t2631365083 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::Reverse()
#define List_1_Reverse_m2514215903(__this, method) ((  void (*) (List_1_t2631365083 *, const MethodInfo*))List_1_Reverse_m4038478200_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::Sort()
#define List_1_Sort_m2866502069(__this, method) ((  void (*) (List_1_t2631365083 *, const MethodInfo*))List_1_Sort_m554162636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m1034144073(__this, ___comparer0, method) ((  void (*) (List_1_t2631365083 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1776685136_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3172987884(__this, ___comparison0, method) ((  void (*) (List_1_t2631365083 *, Comparison_1_t229015506 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<EveryplayRecButtons/Button>::ToArray()
#define List_1_ToArray_m362427186(__this, method) ((  ButtonU5BU5D_t2724374774* (*) (List_1_t2631365083 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::TrimExcess()
#define List_1_TrimExcess_m1899176650(__this, method) ((  void (*) (List_1_t2631365083 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<EveryplayRecButtons/Button>::get_Capacity()
#define List_1_get_Capacity_m4111100140(__this, method) ((  int32_t (*) (List_1_t2631365083 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m351909679(__this, ___value0, method) ((  void (*) (List_1_t2631365083 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<EveryplayRecButtons/Button>::get_Count()
#define List_1_get_Count_m2739392739(__this, method) ((  int32_t (*) (List_1_t2631365083 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// T System.Collections.Generic.List`1<EveryplayRecButtons/Button>::get_Item(System.Int32)
#define List_1_get_Item_m135004381(__this, ___index0, method) ((  Button_t3262243951 * (*) (List_1_t2631365083 *, int32_t, const MethodInfo*))List_1_get_Item_m1354830498_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<EveryplayRecButtons/Button>::set_Item(System.Int32,T)
#define List_1_set_Item_m4240420148(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2631365083 *, int32_t, Button_t3262243951 *, const MethodInfo*))List_1_set_Item_m4128108021_gshared)(__this, ___index0, ___value1, method)
