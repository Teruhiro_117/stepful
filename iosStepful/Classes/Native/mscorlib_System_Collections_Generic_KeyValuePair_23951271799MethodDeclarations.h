﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23709506243MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m482250351(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3951271799 *, uint64_t, NetworkAccessToken_t2931214851 *, const MethodInfo*))KeyValuePair_2__ctor_m1665737714_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Key()
#define KeyValuePair_2_get_Key_m3239181093(__this, method) ((  uint64_t (*) (KeyValuePair_2_t3951271799 *, const MethodInfo*))KeyValuePair_2_get_Key_m3078650232_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2329745362(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3951271799 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Key_m62205575_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Value()
#define KeyValuePair_2_get_Value_m2897841165(__this, method) ((  NetworkAccessToken_t2931214851 * (*) (KeyValuePair_2_t3951271799 *, const MethodInfo*))KeyValuePair_2_get_Value_m549329768_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m132472746(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3951271799 *, NetworkAccessToken_t2931214851 *, const MethodInfo*))KeyValuePair_2_set_Value_m223828967_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::ToString()
#define KeyValuePair_2_ToString_m1062244928(__this, method) ((  String_t* (*) (KeyValuePair_2_t3951271799 *, const MethodInfo*))KeyValuePair_2_ToString_m1791447377_gshared)(__this, method)
