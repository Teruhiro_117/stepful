﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.TargetRpcAttribute
struct TargetRpcAttribute_t3571055858;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.TargetRpcAttribute::.ctor()
extern "C"  void TargetRpcAttribute__ctor_m1193475865 (TargetRpcAttribute_t3571055858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
