﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.ChannelQOS
struct ChannelQOS_t1324696500;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_QosType2003892483.h"
#include "UnityEngine_UnityEngine_Networking_ChannelQOS1324696500.h"

// System.Void UnityEngine.Networking.ChannelQOS::.ctor(UnityEngine.Networking.QosType)
extern "C"  void ChannelQOS__ctor_m1757418857 (ChannelQOS_t1324696500 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ChannelQOS::.ctor()
extern "C"  void ChannelQOS__ctor_m4227055211 (ChannelQOS_t1324696500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.ChannelQOS::.ctor(UnityEngine.Networking.ChannelQOS)
extern "C"  void ChannelQOS__ctor_m1654424486 (ChannelQOS_t1324696500 * __this, ChannelQOS_t1324696500 * ___channel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.QosType UnityEngine.Networking.ChannelQOS::get_QOS()
extern "C"  int32_t ChannelQOS_get_QOS_m3202585600 (ChannelQOS_t1324696500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
