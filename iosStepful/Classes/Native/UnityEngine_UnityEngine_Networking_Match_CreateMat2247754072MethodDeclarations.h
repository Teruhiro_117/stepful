﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.Match.CreateMatchRequest
struct CreateMatchRequest_t2247754072;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t2823857299;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Networking.Match.CreateMatchRequest::.ctor()
extern "C"  void CreateMatchRequest__ctor_m1891194952 (CreateMatchRequest_t2247754072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.CreateMatchRequest::get_name()
extern "C"  String_t* CreateMatchRequest_get_name_m3791044889 (CreateMatchRequest_t2247754072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_name(System.String)
extern "C"  void CreateMatchRequest_set_name_m2699303562 (CreateMatchRequest_t2247754072 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.Networking.Match.CreateMatchRequest::get_size()
extern "C"  uint32_t CreateMatchRequest_get_size_m3495445829 (CreateMatchRequest_t2247754072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_size(System.UInt32)
extern "C"  void CreateMatchRequest_set_size_m1749631240 (CreateMatchRequest_t2247754072 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.CreateMatchRequest::get_publicAddress()
extern "C"  String_t* CreateMatchRequest_get_publicAddress_m2527240613 (CreateMatchRequest_t2247754072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_publicAddress(System.String)
extern "C"  void CreateMatchRequest_set_publicAddress_m2792934722 (CreateMatchRequest_t2247754072 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.CreateMatchRequest::get_privateAddress()
extern "C"  String_t* CreateMatchRequest_get_privateAddress_m53299421 (CreateMatchRequest_t2247754072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_privateAddress(System.String)
extern "C"  void CreateMatchRequest_set_privateAddress_m2915606332 (CreateMatchRequest_t2247754072 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.Match.CreateMatchRequest::get_eloScore()
extern "C"  int32_t CreateMatchRequest_get_eloScore_m4063322599 (CreateMatchRequest_t2247754072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_eloScore(System.Int32)
extern "C"  void CreateMatchRequest_set_eloScore_m3795205574 (CreateMatchRequest_t2247754072 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.Match.CreateMatchRequest::get_advertise()
extern "C"  bool CreateMatchRequest_get_advertise_m2222950006 (CreateMatchRequest_t2247754072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_advertise(System.Boolean)
extern "C"  void CreateMatchRequest_set_advertise_m3413724155 (CreateMatchRequest_t2247754072 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.CreateMatchRequest::get_password()
extern "C"  String_t* CreateMatchRequest_get_password_m2168556861 (CreateMatchRequest_t2247754072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_password(System.String)
extern "C"  void CreateMatchRequest_set_password_m2867634368 (CreateMatchRequest_t2247754072 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64> UnityEngine.Networking.Match.CreateMatchRequest::get_matchAttributes()
extern "C"  Dictionary_2_t2823857299 * CreateMatchRequest_get_matchAttributes_m1208770647 (CreateMatchRequest_t2247754072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.CreateMatchRequest::ToString()
extern "C"  String_t* CreateMatchRequest_ToString_m2936767673 (CreateMatchRequest_t2247754072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
