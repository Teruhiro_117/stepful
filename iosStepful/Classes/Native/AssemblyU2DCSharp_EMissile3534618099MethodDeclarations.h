﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EMissile
struct EMissile_t3534618099;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void EMissile::.ctor()
extern "C"  void EMissile__ctor_m3541219220 (EMissile_t3534618099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EMissile::Init(System.String)
extern "C"  void EMissile_Init_m3737596326 (EMissile_t3534618099 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EMissile::Move(System.Int32,System.Int32)
extern "C"  void EMissile_Move_m827381263 (EMissile_t3534618099 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EMissile::Action(System.Int32)
extern "C"  void EMissile_Action_m2403800549 (EMissile_t3534618099 * __this, int32_t ___resource0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
