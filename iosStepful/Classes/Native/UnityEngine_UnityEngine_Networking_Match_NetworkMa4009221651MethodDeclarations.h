﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMa3073640404MethodDeclarations.h"

// System.Void UnityEngine.Networking.Match.NetworkMatch/InternalResponseDelegate`2<UnityEngine.Networking.Match.JoinMatchResponse,UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<UnityEngine.Networking.Match.MatchInfo>>::.ctor(System.Object,System.IntPtr)
#define InternalResponseDelegate_2__ctor_m3796834453(__this, ___object0, ___method1, method) ((  void (*) (InternalResponseDelegate_2_t4009221651 *, Il2CppObject *, IntPtr_t, const MethodInfo*))InternalResponseDelegate_2__ctor_m3764815497_gshared)(__this, ___object0, ___method1, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/InternalResponseDelegate`2<UnityEngine.Networking.Match.JoinMatchResponse,UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<UnityEngine.Networking.Match.MatchInfo>>::Invoke(T,U)
#define InternalResponseDelegate_2_Invoke_m669845510(__this, ___response0, ___userCallback1, method) ((  void (*) (InternalResponseDelegate_2_t4009221651 *, JoinMatchResponse_t1256242636 *, DataResponseDelegate_1_t3220115103 *, const MethodInfo*))InternalResponseDelegate_2_Invoke_m3415191514_gshared)(__this, ___response0, ___userCallback1, method)
// System.IAsyncResult UnityEngine.Networking.Match.NetworkMatch/InternalResponseDelegate`2<UnityEngine.Networking.Match.JoinMatchResponse,UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<UnityEngine.Networking.Match.MatchInfo>>::BeginInvoke(T,U,System.AsyncCallback,System.Object)
#define InternalResponseDelegate_2_BeginInvoke_m386398763(__this, ___response0, ___userCallback1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (InternalResponseDelegate_2_t4009221651 *, JoinMatchResponse_t1256242636 *, DataResponseDelegate_1_t3220115103 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))InternalResponseDelegate_2_BeginInvoke_m2722812275_gshared)(__this, ___response0, ___userCallback1, ___callback2, ___object3, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/InternalResponseDelegate`2<UnityEngine.Networking.Match.JoinMatchResponse,UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<UnityEngine.Networking.Match.MatchInfo>>::EndInvoke(System.IAsyncResult)
#define InternalResponseDelegate_2_EndInvoke_m931048483(__this, ___result0, method) ((  void (*) (InternalResponseDelegate_2_t4009221651 *, Il2CppObject *, const MethodInfo*))InternalResponseDelegate_2_EndInvoke_m769238843_gshared)(__this, ___result0, method)
