﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// pushButton
struct  pushButton_t4005487804  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject pushButton::refObj
	GameObject_t1756533147 * ___refObj_2;
	// System.Int32 pushButton::line
	int32_t ___line_3;
	// System.Int32 pushButton::row
	int32_t ___row_4;

public:
	inline static int32_t get_offset_of_refObj_2() { return static_cast<int32_t>(offsetof(pushButton_t4005487804, ___refObj_2)); }
	inline GameObject_t1756533147 * get_refObj_2() const { return ___refObj_2; }
	inline GameObject_t1756533147 ** get_address_of_refObj_2() { return &___refObj_2; }
	inline void set_refObj_2(GameObject_t1756533147 * value)
	{
		___refObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___refObj_2, value);
	}

	inline static int32_t get_offset_of_line_3() { return static_cast<int32_t>(offsetof(pushButton_t4005487804, ___line_3)); }
	inline int32_t get_line_3() const { return ___line_3; }
	inline int32_t* get_address_of_line_3() { return &___line_3; }
	inline void set_line_3(int32_t value)
	{
		___line_3 = value;
	}

	inline static int32_t get_offset_of_row_4() { return static_cast<int32_t>(offsetof(pushButton_t4005487804, ___row_4)); }
	inline int32_t get_row_4() const { return ___row_4; }
	inline int32_t* get_address_of_row_4() { return &___row_4; }
	inline void set_row_4(int32_t value)
	{
		___row_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
