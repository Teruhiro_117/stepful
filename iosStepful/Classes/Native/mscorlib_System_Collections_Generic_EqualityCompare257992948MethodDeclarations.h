﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkSceneId>
struct DefaultComparer_t257992948;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2931030083.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkSceneId>::.ctor()
extern "C"  void DefaultComparer__ctor_m2742022600_gshared (DefaultComparer_t257992948 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2742022600(__this, method) ((  void (*) (DefaultComparer_t257992948 *, const MethodInfo*))DefaultComparer__ctor_m2742022600_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkSceneId>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2669850325_gshared (DefaultComparer_t257992948 * __this, NetworkSceneId_t2931030083  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2669850325(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t257992948 *, NetworkSceneId_t2931030083 , const MethodInfo*))DefaultComparer_GetHashCode_m2669850325_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkSceneId>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2139402781_gshared (DefaultComparer_t257992948 * __this, NetworkSceneId_t2931030083  ___x0, NetworkSceneId_t2931030083  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2139402781(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t257992948 *, NetworkSceneId_t2931030083 , NetworkSceneId_t2931030083 , const MethodInfo*))DefaultComparer_Equals_m2139402781_gshared)(__this, ___x0, ___y1, method)
