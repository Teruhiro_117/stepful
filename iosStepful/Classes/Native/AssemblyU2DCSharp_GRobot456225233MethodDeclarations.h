﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GRobot
struct GRobot_t456225233;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void GRobot::.ctor()
extern "C"  void GRobot__ctor_m1963847546 (GRobot_t456225233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GRobot::Init(System.String)
extern "C"  void GRobot_Init_m3680419648 (GRobot_t456225233 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GRobot::Move(System.Int32,System.Int32)
extern "C"  void GRobot_Move_m656884969 (GRobot_t456225233 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GRobot::Draw(System.Int32,System.Int32)
extern "C"  void GRobot_Draw_m3014666978 (GRobot_t456225233 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GRobot::Erase(System.Int32,System.Int32)
extern "C"  void GRobot_Erase_m3020905980 (GRobot_t456225233 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
