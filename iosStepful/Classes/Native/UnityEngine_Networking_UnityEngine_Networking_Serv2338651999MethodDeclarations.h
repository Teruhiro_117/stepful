﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.ServerAttribute
struct ServerAttribute_t2338651999;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.ServerAttribute::.ctor()
extern "C"  void ServerAttribute__ctor_m648388184 (ServerAttribute_t2338651999 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
