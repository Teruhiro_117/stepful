﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct Collection_1_t4233206437;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.NetworkSystem.PeerInfoPlayer[]
struct PeerInfoPlayerU5BU5D_t1632998306;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct IEnumerator_1_t2166985510;
// System.Collections.Generic.IList`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct IList_1_t937434988;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo396494387.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.ctor()
extern "C"  void Collection_1__ctor_m3579058316_gshared (Collection_1_t4233206437 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3579058316(__this, method) ((  void (*) (Collection_1_t4233206437 *, const MethodInfo*))Collection_1__ctor_m3579058316_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2840907205_gshared (Collection_1_t4233206437 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2840907205(__this, method) ((  bool (*) (Collection_1_t4233206437 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2840907205_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2651366252_gshared (Collection_1_t4233206437 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2651366252(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t4233206437 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2651366252_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1240757473_gshared (Collection_1_t4233206437 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1240757473(__this, method) ((  Il2CppObject * (*) (Collection_1_t4233206437 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1240757473_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m60629188_gshared (Collection_1_t4233206437 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m60629188(__this, ___value0, method) ((  int32_t (*) (Collection_1_t4233206437 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m60629188_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2161547698_gshared (Collection_1_t4233206437 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2161547698(__this, ___value0, method) ((  bool (*) (Collection_1_t4233206437 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2161547698_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m841273866_gshared (Collection_1_t4233206437 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m841273866(__this, ___value0, method) ((  int32_t (*) (Collection_1_t4233206437 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m841273866_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2005902121_gshared (Collection_1_t4233206437 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2005902121(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4233206437 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2005902121_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2169918985_gshared (Collection_1_t4233206437 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2169918985(__this, ___value0, method) ((  void (*) (Collection_1_t4233206437 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2169918985_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1975752964_gshared (Collection_1_t4233206437 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1975752964(__this, method) ((  bool (*) (Collection_1_t4233206437 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1975752964_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m484002444_gshared (Collection_1_t4233206437 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m484002444(__this, method) ((  Il2CppObject * (*) (Collection_1_t4233206437 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m484002444_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2378566301_gshared (Collection_1_t4233206437 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2378566301(__this, method) ((  bool (*) (Collection_1_t4233206437 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2378566301_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3129287920_gshared (Collection_1_t4233206437 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3129287920(__this, method) ((  bool (*) (Collection_1_t4233206437 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3129287920_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m3778496325_gshared (Collection_1_t4233206437 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m3778496325(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t4233206437 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m3778496325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2210109854_gshared (Collection_1_t4233206437 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2210109854(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4233206437 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2210109854_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Add(T)
extern "C"  void Collection_1_Add_m3112815741_gshared (Collection_1_t4233206437 * __this, PeerInfoPlayer_t396494387  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3112815741(__this, ___item0, method) ((  void (*) (Collection_1_t4233206437 *, PeerInfoPlayer_t396494387 , const MethodInfo*))Collection_1_Add_m3112815741_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Clear()
extern "C"  void Collection_1_Clear_m1837831089_gshared (Collection_1_t4233206437 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1837831089(__this, method) ((  void (*) (Collection_1_t4233206437 *, const MethodInfo*))Collection_1_Clear_m1837831089_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1576842855_gshared (Collection_1_t4233206437 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1576842855(__this, method) ((  void (*) (Collection_1_t4233206437 *, const MethodInfo*))Collection_1_ClearItems_m1576842855_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Contains(T)
extern "C"  bool Collection_1_Contains_m3467530355_gshared (Collection_1_t4233206437 * __this, PeerInfoPlayer_t396494387  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3467530355(__this, ___item0, method) ((  bool (*) (Collection_1_t4233206437 *, PeerInfoPlayer_t396494387 , const MethodInfo*))Collection_1_Contains_m3467530355_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2180367565_gshared (Collection_1_t4233206437 * __this, PeerInfoPlayerU5BU5D_t1632998306* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2180367565(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t4233206437 *, PeerInfoPlayerU5BU5D_t1632998306*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2180367565_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3261286616_gshared (Collection_1_t4233206437 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3261286616(__this, method) ((  Il2CppObject* (*) (Collection_1_t4233206437 *, const MethodInfo*))Collection_1_GetEnumerator_m3261286616_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2631617449_gshared (Collection_1_t4233206437 * __this, PeerInfoPlayer_t396494387  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2631617449(__this, ___item0, method) ((  int32_t (*) (Collection_1_t4233206437 *, PeerInfoPlayer_t396494387 , const MethodInfo*))Collection_1_IndexOf_m2631617449_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2157089094_gshared (Collection_1_t4233206437 * __this, int32_t ___index0, PeerInfoPlayer_t396494387  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2157089094(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4233206437 *, int32_t, PeerInfoPlayer_t396494387 , const MethodInfo*))Collection_1_Insert_m2157089094_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m435630659_gshared (Collection_1_t4233206437 * __this, int32_t ___index0, PeerInfoPlayer_t396494387  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m435630659(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4233206437 *, int32_t, PeerInfoPlayer_t396494387 , const MethodInfo*))Collection_1_InsertItem_m435630659_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Remove(T)
extern "C"  bool Collection_1_Remove_m1957541544_gshared (Collection_1_t4233206437 * __this, PeerInfoPlayer_t396494387  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m1957541544(__this, ___item0, method) ((  bool (*) (Collection_1_t4233206437 *, PeerInfoPlayer_t396494387 , const MethodInfo*))Collection_1_Remove_m1957541544_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3659587562_gshared (Collection_1_t4233206437 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3659587562(__this, ___index0, method) ((  void (*) (Collection_1_t4233206437 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3659587562_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2238045688_gshared (Collection_1_t4233206437 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2238045688(__this, ___index0, method) ((  void (*) (Collection_1_t4233206437 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2238045688_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1571565092_gshared (Collection_1_t4233206437 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1571565092(__this, method) ((  int32_t (*) (Collection_1_t4233206437 *, const MethodInfo*))Collection_1_get_Count_m1571565092_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::get_Item(System.Int32)
extern "C"  PeerInfoPlayer_t396494387  Collection_1_get_Item_m480816620_gshared (Collection_1_t4233206437 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m480816620(__this, ___index0, method) ((  PeerInfoPlayer_t396494387  (*) (Collection_1_t4233206437 *, int32_t, const MethodInfo*))Collection_1_get_Item_m480816620_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1799086205_gshared (Collection_1_t4233206437 * __this, int32_t ___index0, PeerInfoPlayer_t396494387  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1799086205(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t4233206437 *, int32_t, PeerInfoPlayer_t396494387 , const MethodInfo*))Collection_1_set_Item_m1799086205_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m776497582_gshared (Collection_1_t4233206437 * __this, int32_t ___index0, PeerInfoPlayer_t396494387  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m776497582(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t4233206437 *, int32_t, PeerInfoPlayer_t396494387 , const MethodInfo*))Collection_1_SetItem_m776497582_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2422313167_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2422313167(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2422313167_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::ConvertItem(System.Object)
extern "C"  PeerInfoPlayer_t396494387  Collection_1_ConvertItem_m3012946207_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3012946207(__this /* static, unused */, ___item0, method) ((  PeerInfoPlayer_t396494387  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3012946207_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1126456931_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1126456931(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1126456931_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2160962299_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2160962299(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2160962299_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2545469034_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2545469034(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2545469034_gshared)(__this /* static, unused */, ___list0, method)
