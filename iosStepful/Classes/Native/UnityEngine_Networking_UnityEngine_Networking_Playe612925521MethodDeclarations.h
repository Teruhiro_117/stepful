﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.PlayerController
struct PlayerController_t612925521;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void UnityEngine.Networking.PlayerController::.ctor()
extern "C"  void PlayerController__ctor_m794476596 (PlayerController_t612925521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.PlayerController::.ctor(UnityEngine.GameObject,System.Int16)
extern "C"  void PlayerController__ctor_m603789449 (PlayerController_t612925521 * __this, GameObject_t1756533147 * ___go0, int16_t ___playerControllerId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.PlayerController::get_IsValid()
extern "C"  bool PlayerController_get_IsValid_m3079811 (PlayerController_t612925521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.PlayerController::ToString()
extern "C"  String_t* PlayerController_ToString_m4001351317 (PlayerController_t612925521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
