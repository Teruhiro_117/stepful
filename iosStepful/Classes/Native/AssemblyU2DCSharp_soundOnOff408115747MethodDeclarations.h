﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// soundOnOff
struct soundOnOff_t408115747;

#include "codegen/il2cpp-codegen.h"

// System.Void soundOnOff::.ctor()
extern "C"  void soundOnOff__ctor_m2820888082 (soundOnOff_t408115747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void soundOnOff::Start()
extern "C"  void soundOnOff_Start_m3976287054 (soundOnOff_t408115747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void soundOnOff::Update()
extern "C"  void soundOnOff_Update_m3653385803 (soundOnOff_t408115747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void soundOnOff::OnClick()
extern "C"  void soundOnOff_OnClick_m1428459721 (soundOnOff_t408115747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
