﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkSystem.LobbyReadyToBeginMessage
struct LobbyReadyToBeginMessage_t3495336496;
// UnityEngine.Networking.NetworkReader
struct NetworkReader_t3187690923;
// UnityEngine.Networking.NetworkWriter
struct NetworkWriter_t560143343;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3187690923.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo560143343.h"

// System.Void UnityEngine.Networking.NetworkSystem.LobbyReadyToBeginMessage::.ctor()
extern "C"  void LobbyReadyToBeginMessage__ctor_m372685288 (LobbyReadyToBeginMessage_t3495336496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkSystem.LobbyReadyToBeginMessage::Deserialize(UnityEngine.Networking.NetworkReader)
extern "C"  void LobbyReadyToBeginMessage_Deserialize_m3246315961 (LobbyReadyToBeginMessage_t3495336496 * __this, NetworkReader_t3187690923 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkSystem.LobbyReadyToBeginMessage::Serialize(UnityEngine.Networking.NetworkWriter)
extern "C"  void LobbyReadyToBeginMessage_Serialize_m1539163564 (LobbyReadyToBeginMessage_t3495336496 * __this, NetworkWriter_t560143343 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
