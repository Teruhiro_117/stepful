﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct DefaultComparer_t3675306282;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2053376121.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m1922441911_gshared (DefaultComparer_t3675306282 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1922441911(__this, method) ((  void (*) (DefaultComparer_t3675306282 *, const MethodInfo*))DefaultComparer__ctor_m1922441911_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1566575408_gshared (DefaultComparer_t3675306282 * __this, PendingPlayerInfo_t2053376121  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1566575408(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3675306282 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))DefaultComparer_GetHashCode_m1566575408_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m334525628_gshared (DefaultComparer_t3675306282 * __this, PendingPlayerInfo_t2053376121  ___x0, PendingPlayerInfo_t2053376121  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m334525628(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3675306282 *, PendingPlayerInfo_t2053376121 , PendingPlayerInfo_t2053376121 , const MethodInfo*))DefaultComparer_Equals_m334525628_gshared)(__this, ___x0, ___y1, method)
