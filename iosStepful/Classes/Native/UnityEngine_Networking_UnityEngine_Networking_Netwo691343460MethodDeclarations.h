﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkConnection/PacketStat
struct PacketStat_t691343460;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo691343460.h"

// System.Void UnityEngine.Networking.NetworkConnection/PacketStat::.ctor()
extern "C"  void PacketStat__ctor_m3378605432 (PacketStat_t691343460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkConnection/PacketStat::.ctor(UnityEngine.Networking.NetworkConnection/PacketStat)
extern "C"  void PacketStat__ctor_m2198563430 (PacketStat_t691343460 * __this, PacketStat_t691343460 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.NetworkConnection/PacketStat::ToString()
extern "C"  String_t* PacketStat_ToString_m2956564915 (PacketStat_t691343460 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
