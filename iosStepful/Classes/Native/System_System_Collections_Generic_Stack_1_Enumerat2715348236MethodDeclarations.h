﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct Stack_1_t2065349876;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat2715348236.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C"  void Enumerator__ctor_m1076400127_gshared (Enumerator_t2715348236 * __this, Stack_1_t2065349876 * ___t0, const MethodInfo* method);
#define Enumerator__ctor_m1076400127(__this, ___t0, method) ((  void (*) (Enumerator_t2715348236 *, Stack_1_t2065349876 *, const MethodInfo*))Enumerator__ctor_m1076400127_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2875874647_gshared (Enumerator_t2715348236 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2875874647(__this, method) ((  void (*) (Enumerator_t2715348236 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2875874647_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m177899195_gshared (Enumerator_t2715348236 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m177899195(__this, method) ((  Il2CppObject * (*) (Enumerator_t2715348236 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m177899195_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::Dispose()
extern "C"  void Enumerator_Dispose_m3091971352_gshared (Enumerator_t2715348236 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3091971352(__this, method) ((  void (*) (Enumerator_t2715348236 *, const MethodInfo*))Enumerator_Dispose_m3091971352_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2067282091_gshared (Enumerator_t2715348236 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2067282091(__this, method) ((  bool (*) (Enumerator_t2715348236 *, const MethodInfo*))Enumerator_MoveNext_m2067282091_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<UnityEngine.Networking.LocalClient/InternalMsg>::get_Current()
extern "C"  InternalMsg_t977621722  Enumerator_get_Current_m1999086838_gshared (Enumerator_t2715348236 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1999086838(__this, method) ((  InternalMsg_t977621722  (*) (Enumerator_t2715348236 *, const MethodInfo*))Enumerator_get_Current_m1999086838_gshared)(__this, method)
