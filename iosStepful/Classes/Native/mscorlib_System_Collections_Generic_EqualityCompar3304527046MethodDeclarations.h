﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.ChannelPacket>
struct DefaultComparer_t3304527046;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan1682596885.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.ChannelPacket>::.ctor()
extern "C"  void DefaultComparer__ctor_m1269791100_gshared (DefaultComparer_t3304527046 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1269791100(__this, method) ((  void (*) (DefaultComparer_t3304527046 *, const MethodInfo*))DefaultComparer__ctor_m1269791100_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.ChannelPacket>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2282099347_gshared (DefaultComparer_t3304527046 * __this, ChannelPacket_t1682596885  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2282099347(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3304527046 *, ChannelPacket_t1682596885 , const MethodInfo*))DefaultComparer_GetHashCode_m2282099347_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.ChannelPacket>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m125061731_gshared (DefaultComparer_t3304527046 * __this, ChannelPacket_t1682596885  ___x0, ChannelPacket_t1682596885  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m125061731(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3304527046 *, ChannelPacket_t1682596885 , ChannelPacket_t1682596885 , const MethodInfo*))DefaultComparer_Equals_m125061731_gshared)(__this, ___x0, ___y1, method)
