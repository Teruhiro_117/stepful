﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.ClientScene/PendingOwner>
struct DefaultComparer_t2499749080;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clien877818919.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.ClientScene/PendingOwner>::.ctor()
extern "C"  void DefaultComparer__ctor_m2325464965_gshared (DefaultComparer_t2499749080 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2325464965(__this, method) ((  void (*) (DefaultComparer_t2499749080 *, const MethodInfo*))DefaultComparer__ctor_m2325464965_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.ClientScene/PendingOwner>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3895583276_gshared (DefaultComparer_t2499749080 * __this, PendingOwner_t877818919  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3895583276(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2499749080 *, PendingOwner_t877818919 , const MethodInfo*))DefaultComparer_GetHashCode_m3895583276_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Networking.ClientScene/PendingOwner>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m960809140_gshared (DefaultComparer_t2499749080 * __this, PendingOwner_t877818919  ___x0, PendingOwner_t877818919  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m960809140(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2499749080 *, PendingOwner_t877818919 , PendingOwner_t877818919 , const MethodInfo*))DefaultComparer_Equals_m960809140_gshared)(__this, ___x0, ___y1, method)
