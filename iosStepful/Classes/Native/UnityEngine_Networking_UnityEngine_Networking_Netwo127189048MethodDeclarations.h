﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkSystem.ReadyMessage
struct ReadyMessage_t127189048;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.NetworkSystem.ReadyMessage::.ctor()
extern "C"  void ReadyMessage__ctor_m440230484 (ReadyMessage_t127189048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
