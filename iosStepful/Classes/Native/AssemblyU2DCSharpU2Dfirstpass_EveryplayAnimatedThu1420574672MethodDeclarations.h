﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayAnimatedThumbnailOnGUI
struct EveryplayAnimatedThumbnailOnGUI_t1420574672;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void EveryplayAnimatedThumbnailOnGUI::.ctor()
extern "C"  void EveryplayAnimatedThumbnailOnGUI__ctor_m4073471057 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnailOnGUI::Awake()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_Awake_m972992546 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnailOnGUI::Start()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_Start_m570778561 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnailOnGUI::OnDestroy()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_OnDestroy_m3821570548 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnailOnGUI::OnDisable()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_OnDisable_m2671812778 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnailOnGUI::ResetThumbnail()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_ResetThumbnail_m494468424 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EveryplayAnimatedThumbnailOnGUI::CrossfadeTransition()
extern "C"  Il2CppObject * EveryplayAnimatedThumbnailOnGUI_CrossfadeTransition_m2248282768 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnailOnGUI::StopTransitions()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_StopTransitions_m2114846637 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnailOnGUI::Update()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_Update_m1307573924 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayAnimatedThumbnailOnGUI::OnGUI()
extern "C"  void EveryplayAnimatedThumbnailOnGUI_OnGUI_m2082476659 (EveryplayAnimatedThumbnailOnGUI_t1420574672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
