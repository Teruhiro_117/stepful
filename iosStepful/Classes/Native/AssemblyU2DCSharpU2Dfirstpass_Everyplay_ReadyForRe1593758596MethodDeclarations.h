﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/ReadyForRecordingDelegate
struct ReadyForRecordingDelegate_t1593758596;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/ReadyForRecordingDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ReadyForRecordingDelegate__ctor_m4085498349 (ReadyForRecordingDelegate_t1593758596 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/ReadyForRecordingDelegate::Invoke(System.Boolean)
extern "C"  void ReadyForRecordingDelegate_Invoke_m1966730172 (ReadyForRecordingDelegate_t1593758596 * __this, bool ___enabled0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/ReadyForRecordingDelegate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReadyForRecordingDelegate_BeginInvoke_m1277641157 (ReadyForRecordingDelegate_t1593758596 * __this, bool ___enabled0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/ReadyForRecordingDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void ReadyForRecordingDelegate_EndInvoke_m4071120819 (ReadyForRecordingDelegate_t1593758596 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
