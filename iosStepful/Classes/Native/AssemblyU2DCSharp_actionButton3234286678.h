﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// actionButton
struct  actionButton_t3234286678  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject actionButton::refObj
	GameObject_t1756533147 * ___refObj_2;
	// System.Int32 actionButton::actionNum
	int32_t ___actionNum_3;

public:
	inline static int32_t get_offset_of_refObj_2() { return static_cast<int32_t>(offsetof(actionButton_t3234286678, ___refObj_2)); }
	inline GameObject_t1756533147 * get_refObj_2() const { return ___refObj_2; }
	inline GameObject_t1756533147 ** get_address_of_refObj_2() { return &___refObj_2; }
	inline void set_refObj_2(GameObject_t1756533147 * value)
	{
		___refObj_2 = value;
		Il2CppCodeGenWriteBarrier(&___refObj_2, value);
	}

	inline static int32_t get_offset_of_actionNum_3() { return static_cast<int32_t>(offsetof(actionButton_t3234286678, ___actionNum_3)); }
	inline int32_t get_actionNum_3() const { return ___actionNum_3; }
	inline int32_t* get_address_of_actionNum_3() { return &___actionNum_3; }
	inline void set_actionNum_3(int32_t value)
	{
		___actionNum_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
