﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo
struct PendingPlayerInfo_t2053376121;
struct PendingPlayerInfo_t2053376121_marshaled_pinvoke;
struct PendingPlayerInfo_t2053376121_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct PendingPlayerInfo_t2053376121;
struct PendingPlayerInfo_t2053376121_marshaled_pinvoke;

extern "C" void PendingPlayerInfo_t2053376121_marshal_pinvoke(const PendingPlayerInfo_t2053376121& unmarshaled, PendingPlayerInfo_t2053376121_marshaled_pinvoke& marshaled);
extern "C" void PendingPlayerInfo_t2053376121_marshal_pinvoke_back(const PendingPlayerInfo_t2053376121_marshaled_pinvoke& marshaled, PendingPlayerInfo_t2053376121& unmarshaled);
extern "C" void PendingPlayerInfo_t2053376121_marshal_pinvoke_cleanup(PendingPlayerInfo_t2053376121_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct PendingPlayerInfo_t2053376121;
struct PendingPlayerInfo_t2053376121_marshaled_com;

extern "C" void PendingPlayerInfo_t2053376121_marshal_com(const PendingPlayerInfo_t2053376121& unmarshaled, PendingPlayerInfo_t2053376121_marshaled_com& marshaled);
extern "C" void PendingPlayerInfo_t2053376121_marshal_com_back(const PendingPlayerInfo_t2053376121_marshaled_com& marshaled, PendingPlayerInfo_t2053376121& unmarshaled);
extern "C" void PendingPlayerInfo_t2053376121_marshal_com_cleanup(PendingPlayerInfo_t2053376121_marshaled_com& marshaled);
