﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.Int16>
struct GenericEqualityComparer_1_t3025128050;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int16>::.ctor()
extern "C"  void GenericEqualityComparer_1__ctor_m39386843_gshared (GenericEqualityComparer_1_t3025128050 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m39386843(__this, method) ((  void (*) (GenericEqualityComparer_1_t3025128050 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m39386843_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int16>::GetHashCode(T)
extern "C"  int32_t GenericEqualityComparer_1_GetHashCode_m709743494_gshared (GenericEqualityComparer_1_t3025128050 * __this, int16_t ___obj0, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m709743494(__this, ___obj0, method) ((  int32_t (*) (GenericEqualityComparer_1_t3025128050 *, int16_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m709743494_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int16>::Equals(T,T)
extern "C"  bool GenericEqualityComparer_1_Equals_m847891558_gshared (GenericEqualityComparer_1_t3025128050 * __this, int16_t ___x0, int16_t ___y1, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m847891558(__this, ___x0, ___y1, method) ((  bool (*) (GenericEqualityComparer_1_t3025128050 *, int16_t, int16_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m847891558_gshared)(__this, ___x0, ___y1, method)
