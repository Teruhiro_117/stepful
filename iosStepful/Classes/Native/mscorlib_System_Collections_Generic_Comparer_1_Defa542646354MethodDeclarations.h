﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct DefaultComparer_t542646354;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2053376121.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m1611761391_gshared (DefaultComparer_t542646354 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1611761391(__this, method) ((  void (*) (DefaultComparer_t542646354 *, const MethodInfo*))DefaultComparer__ctor_m1611761391_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1031732650_gshared (DefaultComparer_t542646354 * __this, PendingPlayerInfo_t2053376121  ___x0, PendingPlayerInfo_t2053376121  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1031732650(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t542646354 *, PendingPlayerInfo_t2053376121 , PendingPlayerInfo_t2053376121 , const MethodInfo*))DefaultComparer_Compare_m1031732650_gshared)(__this, ___x0, ___y1, method)
