﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.NetworkTransformVisualizer
struct NetworkTransformVisualizer_t1115473132;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void UnityEngine.Networking.NetworkTransformVisualizer::.ctor()
extern "C"  void NetworkTransformVisualizer__ctor_m2397009347 (NetworkTransformVisualizer_t1115473132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Networking.NetworkTransformVisualizer::get_visualizerPrefab()
extern "C"  GameObject_t1756533147 * NetworkTransformVisualizer_get_visualizerPrefab_m3103396379 (NetworkTransformVisualizer_t1115473132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkTransformVisualizer::set_visualizerPrefab(UnityEngine.GameObject)
extern "C"  void NetworkTransformVisualizer_set_visualizerPrefab_m3675337304 (NetworkTransformVisualizer_t1115473132 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkTransformVisualizer::OnStartClient()
extern "C"  void NetworkTransformVisualizer_OnStartClient_m185839103 (NetworkTransformVisualizer_t1115473132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkTransformVisualizer::OnStartLocalPlayer()
extern "C"  void NetworkTransformVisualizer_OnStartLocalPlayer_m4291523358 (NetworkTransformVisualizer_t1115473132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkTransformVisualizer::OnDestroy()
extern "C"  void NetworkTransformVisualizer_OnDestroy_m3564218532 (NetworkTransformVisualizer_t1115473132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkTransformVisualizer::FixedUpdate()
extern "C"  void NetworkTransformVisualizer_FixedUpdate_m3810713470 (NetworkTransformVisualizer_t1115473132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkTransformVisualizer::OnRenderObject()
extern "C"  void NetworkTransformVisualizer_OnRenderObject_m3441662473 (NetworkTransformVisualizer_t1115473132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkTransformVisualizer::DrawRotationInterpolation()
extern "C"  void NetworkTransformVisualizer_DrawRotationInterpolation_m2176870673 (NetworkTransformVisualizer_t1115473132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.NetworkTransformVisualizer::CreateLineMaterial()
extern "C"  void NetworkTransformVisualizer_CreateLineMaterial_m2147135550 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
