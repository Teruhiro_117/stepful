﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// selectKoma
struct selectKoma_t1760479012;

#include "codegen/il2cpp-codegen.h"

// System.Void selectKoma::.ctor()
extern "C"  void selectKoma__ctor_m3523864131 (selectKoma_t1760479012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void selectKoma::Start()
extern "C"  void selectKoma_Start_m913817551 (selectKoma_t1760479012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void selectKoma::Update()
extern "C"  void selectKoma_Update_m48568258 (selectKoma_t1760479012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void selectKoma::OnClick()
extern "C"  void selectKoma_OnClick_m1450290360 (selectKoma_t1760479012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
