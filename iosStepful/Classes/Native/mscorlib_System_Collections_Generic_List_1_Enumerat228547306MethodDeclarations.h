﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelQOS>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3762008444(__this, ___l0, method) ((  void (*) (Enumerator_t228547306 *, List_1_t693817632 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelQOS>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3520178094(__this, method) ((  void (*) (Enumerator_t228547306 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelQOS>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m645188242(__this, method) ((  Il2CppObject * (*) (Enumerator_t228547306 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelQOS>::Dispose()
#define Enumerator_Dispose_m3060121695(__this, method) ((  void (*) (Enumerator_t228547306 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelQOS>::VerifyState()
#define Enumerator_VerifyState_m3211101478(__this, method) ((  void (*) (Enumerator_t228547306 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelQOS>::MoveNext()
#define Enumerator_MoveNext_m2398468422(__this, method) ((  bool (*) (Enumerator_t228547306 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.ChannelQOS>::get_Current()
#define Enumerator_get_Current_m2446322684(__this, method) ((  ChannelQOS_t1324696500 * (*) (Enumerator_t228547306 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
