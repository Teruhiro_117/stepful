﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SaveScript
struct  SaveScript_t1729063510  : public MonoBehaviour_t1158329972
{
public:
	// System.String SaveScript::str
	String_t* ___str_2;
	// UnityEngine.UI.InputField SaveScript::inputField
	InputField_t1631627530 * ___inputField_3;
	// UnityEngine.UI.Text SaveScript::text
	Text_t356221433 * ___text_4;

public:
	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(SaveScript_t1729063510, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier(&___str_2, value);
	}

	inline static int32_t get_offset_of_inputField_3() { return static_cast<int32_t>(offsetof(SaveScript_t1729063510, ___inputField_3)); }
	inline InputField_t1631627530 * get_inputField_3() const { return ___inputField_3; }
	inline InputField_t1631627530 ** get_address_of_inputField_3() { return &___inputField_3; }
	inline void set_inputField_3(InputField_t1631627530 * value)
	{
		___inputField_3 = value;
		Il2CppCodeGenWriteBarrier(&___inputField_3, value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(SaveScript_t1729063510, ___text_4)); }
	inline Text_t356221433 * get_text_4() const { return ___text_4; }
	inline Text_t356221433 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t356221433 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier(&___text_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
