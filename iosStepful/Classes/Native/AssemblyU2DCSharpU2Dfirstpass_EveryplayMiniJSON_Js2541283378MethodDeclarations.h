﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayMiniJSON.Json/Parser
struct Parser_t2541283378;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_EveryplayMiniJSON_Js2035824124.h"

// System.Void EveryplayMiniJSON.Json/Parser::.ctor(System.String)
extern "C"  void Parser__ctor_m1858534703 (Parser_t2541283378 * __this, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EveryplayMiniJSON.Json/Parser::Parse(System.String)
extern "C"  Il2CppObject * Parser_Parse_m1759762959 (Il2CppObject * __this /* static, unused */, String_t* ___jsonString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayMiniJSON.Json/Parser::Dispose()
extern "C"  void Parser_Dispose_m3460752774 (Parser_t2541283378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> EveryplayMiniJSON.Json/Parser::ParseObject()
extern "C"  Dictionary_2_t309261261 * Parser_ParseObject_m4045311529 (Parser_t2541283378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> EveryplayMiniJSON.Json/Parser::ParseArray()
extern "C"  List_1_t2058570427 * Parser_ParseArray_m3092052064 (Parser_t2541283378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EveryplayMiniJSON.Json/Parser::ParseValue()
extern "C"  Il2CppObject * Parser_ParseValue_m2923126038 (Parser_t2541283378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EveryplayMiniJSON.Json/Parser::ParseByToken(EveryplayMiniJSON.Json/Parser/TOKEN)
extern "C"  Il2CppObject * Parser_ParseByToken_m2171510005 (Parser_t2541283378 * __this, int32_t ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EveryplayMiniJSON.Json/Parser::ParseString()
extern "C"  String_t* Parser_ParseString_m3675037596 (Parser_t2541283378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EveryplayMiniJSON.Json/Parser::ParseNumber()
extern "C"  Il2CppObject * Parser_ParseNumber_m3048319054 (Parser_t2541283378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayMiniJSON.Json/Parser::EatWhitespace()
extern "C"  void Parser_EatWhitespace_m2728703728 (Parser_t2541283378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char EveryplayMiniJSON.Json/Parser::get_PeekChar()
extern "C"  Il2CppChar Parser_get_PeekChar_m1526822795 (Parser_t2541283378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char EveryplayMiniJSON.Json/Parser::get_NextChar()
extern "C"  Il2CppChar Parser_get_NextChar_m3068679379 (Parser_t2541283378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EveryplayMiniJSON.Json/Parser::get_NextWord()
extern "C"  String_t* Parser_get_NextWord_m1808783620 (Parser_t2541283378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EveryplayMiniJSON.Json/Parser/TOKEN EveryplayMiniJSON.Json/Parser::get_NextToken()
extern "C"  int32_t Parser_get_NextToken_m3643776035 (Parser_t2541283378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
