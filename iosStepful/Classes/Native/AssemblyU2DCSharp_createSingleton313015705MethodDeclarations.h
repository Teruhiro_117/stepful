﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// createSingleton
struct createSingleton_t313015705;

#include "codegen/il2cpp-codegen.h"

// System.Void createSingleton::.ctor()
extern "C"  void createSingleton__ctor_m736849240 (createSingleton_t313015705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void createSingleton::Start()
extern "C"  void createSingleton_Start_m3605344032 (createSingleton_t313015705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void createSingleton::Update()
extern "C"  void createSingleton_Update_m4196438813 (createSingleton_t313015705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
