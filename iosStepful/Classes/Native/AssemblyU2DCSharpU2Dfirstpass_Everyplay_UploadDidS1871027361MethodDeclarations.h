﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/UploadDidStartDelegate
struct UploadDidStartDelegate_t1871027361;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/UploadDidStartDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void UploadDidStartDelegate__ctor_m2080745860 (UploadDidStartDelegate_t1871027361 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/UploadDidStartDelegate::Invoke(System.Int32)
extern "C"  void UploadDidStartDelegate_Invoke_m3051571839 (UploadDidStartDelegate_t1871027361 * __this, int32_t ___videoId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/UploadDidStartDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UploadDidStartDelegate_BeginInvoke_m362396062 (UploadDidStartDelegate_t1871027361 * __this, int32_t ___videoId0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/UploadDidStartDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void UploadDidStartDelegate_EndInvoke_m3859536394 (UploadDidStartDelegate_t1871027361 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
