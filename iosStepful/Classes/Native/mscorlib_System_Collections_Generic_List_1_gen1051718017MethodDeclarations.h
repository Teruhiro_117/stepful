﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>
struct List_1_t1051718017;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.ChannelPacket>
struct IEnumerable_1_t1974723930;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.ChannelPacket>
struct IEnumerator_1_t3453088008;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.ChannelPacket>
struct ICollection_1_t2634672190;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.ChannelPacket>
struct ReadOnlyCollection_1_t1868382577;
// UnityEngine.Networking.ChannelPacket[]
struct ChannelPacketU5BU5D_t3883591672;
// System.Predicate`1<UnityEngine.Networking.ChannelPacket>
struct Predicate_1_t125567000;
// System.Collections.Generic.IComparer`1<UnityEngine.Networking.ChannelPacket>
struct IComparer_1_t3932027303;
// System.Comparison`1<UnityEngine.Networking.ChannelPacket>
struct Comparison_1_t2944335736;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan1682596885.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat586447691.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::.ctor()
extern "C"  void List_1__ctor_m1696857552_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1__ctor_m1696857552(__this, method) ((  void (*) (List_1_t1051718017 *, const MethodInfo*))List_1__ctor_m1696857552_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3483554605_gshared (List_1_t1051718017 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3483554605(__this, ___collection0, method) ((  void (*) (List_1_t1051718017 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3483554605_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m4177893335_gshared (List_1_t1051718017 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m4177893335(__this, ___capacity0, method) ((  void (*) (List_1_t1051718017 *, int32_t, const MethodInfo*))List_1__ctor_m4177893335_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::.cctor()
extern "C"  void List_1__cctor_m3890115053_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3890115053(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3890115053_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3867686156_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3867686156(__this, method) ((  Il2CppObject* (*) (List_1_t1051718017 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3867686156_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3531697262_gshared (List_1_t1051718017 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3531697262(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1051718017 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3531697262_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1802695915_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1802695915(__this, method) ((  Il2CppObject * (*) (List_1_t1051718017 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1802695915_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m367903394_gshared (List_1_t1051718017 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m367903394(__this, ___item0, method) ((  int32_t (*) (List_1_t1051718017 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m367903394_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m237386508_gshared (List_1_t1051718017 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m237386508(__this, ___item0, method) ((  bool (*) (List_1_t1051718017 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m237386508_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1018413496_gshared (List_1_t1051718017 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1018413496(__this, ___item0, method) ((  int32_t (*) (List_1_t1051718017 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1018413496_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m4020996635_gshared (List_1_t1051718017 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m4020996635(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1051718017 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m4020996635_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3228906555_gshared (List_1_t1051718017 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3228906555(__this, ___item0, method) ((  void (*) (List_1_t1051718017 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3228906555_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2875584767_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2875584767(__this, method) ((  bool (*) (List_1_t1051718017 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2875584767_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3892459130_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3892459130(__this, method) ((  bool (*) (List_1_t1051718017 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3892459130_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3608237252_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3608237252(__this, method) ((  Il2CppObject * (*) (List_1_t1051718017 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3608237252_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m651521579_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m651521579(__this, method) ((  bool (*) (List_1_t1051718017 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m651521579_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m3448936982_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m3448936982(__this, method) ((  bool (*) (List_1_t1051718017 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m3448936982_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1798727863_gshared (List_1_t1051718017 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1798727863(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1051718017 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1798727863_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1518352520_gshared (List_1_t1051718017 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1518352520(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1051718017 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1518352520_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::Add(T)
extern "C"  void List_1_Add_m2435247380_gshared (List_1_t1051718017 * __this, ChannelPacket_t1682596885  ___item0, const MethodInfo* method);
#define List_1_Add_m2435247380(__this, ___item0, method) ((  void (*) (List_1_t1051718017 *, ChannelPacket_t1682596885 , const MethodInfo*))List_1_Add_m2435247380_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1202650964_gshared (List_1_t1051718017 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1202650964(__this, ___newCount0, method) ((  void (*) (List_1_t1051718017 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1202650964_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2739269044_gshared (List_1_t1051718017 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2739269044(__this, ___collection0, method) ((  void (*) (List_1_t1051718017 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2739269044_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1624128420_gshared (List_1_t1051718017 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1624128420(__this, ___enumerable0, method) ((  void (*) (List_1_t1051718017 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1624128420_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1415943399_gshared (List_1_t1051718017 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1415943399(__this, ___collection0, method) ((  void (*) (List_1_t1051718017 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1415943399_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1868382577 * List_1_AsReadOnly_m3010415998_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3010415998(__this, method) ((  ReadOnlyCollection_1_t1868382577 * (*) (List_1_t1051718017 *, const MethodInfo*))List_1_AsReadOnly_m3010415998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::Clear()
extern "C"  void List_1_Clear_m1543732971_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_Clear_m1543732971(__this, method) ((  void (*) (List_1_t1051718017 *, const MethodInfo*))List_1_Clear_m1543732971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::Contains(T)
extern "C"  bool List_1_Contains_m540182293_gshared (List_1_t1051718017 * __this, ChannelPacket_t1682596885  ___item0, const MethodInfo* method);
#define List_1_Contains_m540182293(__this, ___item0, method) ((  bool (*) (List_1_t1051718017 *, ChannelPacket_t1682596885 , const MethodInfo*))List_1_Contains_m540182293_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m4165313987_gshared (List_1_t1051718017 * __this, ChannelPacketU5BU5D_t3883591672* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m4165313987(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1051718017 *, ChannelPacketU5BU5D_t3883591672*, int32_t, const MethodInfo*))List_1_CopyTo_m4165313987_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::Find(System.Predicate`1<T>)
extern "C"  ChannelPacket_t1682596885  List_1_Find_m251592093_gshared (List_1_t1051718017 * __this, Predicate_1_t125567000 * ___match0, const MethodInfo* method);
#define List_1_Find_m251592093(__this, ___match0, method) ((  ChannelPacket_t1682596885  (*) (List_1_t1051718017 *, Predicate_1_t125567000 *, const MethodInfo*))List_1_Find_m251592093_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m3299324518_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t125567000 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m3299324518(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t125567000 *, const MethodInfo*))List_1_CheckMatch_m3299324518_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m4096487405_gshared (List_1_t1051718017 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t125567000 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m4096487405(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1051718017 *, int32_t, int32_t, Predicate_1_t125567000 *, const MethodInfo*))List_1_GetIndex_m4096487405_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::GetEnumerator()
extern "C"  Enumerator_t586447691  List_1_GetEnumerator_m817785466_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m817785466(__this, method) ((  Enumerator_t586447691  (*) (List_1_t1051718017 *, const MethodInfo*))List_1_GetEnumerator_m817785466_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m4085186367_gshared (List_1_t1051718017 * __this, ChannelPacket_t1682596885  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m4085186367(__this, ___item0, method) ((  int32_t (*) (List_1_t1051718017 *, ChannelPacket_t1682596885 , const MethodInfo*))List_1_IndexOf_m4085186367_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3916033738_gshared (List_1_t1051718017 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3916033738(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1051718017 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3916033738_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3850463379_gshared (List_1_t1051718017 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3850463379(__this, ___index0, method) ((  void (*) (List_1_t1051718017 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3850463379_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2399338460_gshared (List_1_t1051718017 * __this, int32_t ___index0, ChannelPacket_t1682596885  ___item1, const MethodInfo* method);
#define List_1_Insert_m2399338460(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1051718017 *, int32_t, ChannelPacket_t1682596885 , const MethodInfo*))List_1_Insert_m2399338460_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2542101285_gshared (List_1_t1051718017 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2542101285(__this, ___collection0, method) ((  void (*) (List_1_t1051718017 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2542101285_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::Remove(T)
extern "C"  bool List_1_Remove_m1692528282_gshared (List_1_t1051718017 * __this, ChannelPacket_t1682596885  ___item0, const MethodInfo* method);
#define List_1_Remove_m1692528282(__this, ___item0, method) ((  bool (*) (List_1_t1051718017 *, ChannelPacket_t1682596885 , const MethodInfo*))List_1_Remove_m1692528282_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2356943746_gshared (List_1_t1051718017 * __this, Predicate_1_t125567000 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2356943746(__this, ___match0, method) ((  int32_t (*) (List_1_t1051718017 *, Predicate_1_t125567000 *, const MethodInfo*))List_1_RemoveAll_m2356943746_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m4176387594_gshared (List_1_t1051718017 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m4176387594(__this, ___index0, method) ((  void (*) (List_1_t1051718017 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4176387594_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::Reverse()
extern "C"  void List_1_Reverse_m985023740_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_Reverse_m985023740(__this, method) ((  void (*) (List_1_t1051718017 *, const MethodInfo*))List_1_Reverse_m985023740_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::Sort()
extern "C"  void List_1_Sort_m940519628_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_Sort_m940519628(__this, method) ((  void (*) (List_1_t1051718017 *, const MethodInfo*))List_1_Sort_m940519628_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3799226520_gshared (List_1_t1051718017 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3799226520(__this, ___comparer0, method) ((  void (*) (List_1_t1051718017 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3799226520_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1654400373_gshared (List_1_t1051718017 * __this, Comparison_1_t2944335736 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1654400373(__this, ___comparison0, method) ((  void (*) (List_1_t1051718017 *, Comparison_1_t2944335736 *, const MethodInfo*))List_1_Sort_m1654400373_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::ToArray()
extern "C"  ChannelPacketU5BU5D_t3883591672* List_1_ToArray_m354304473_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_ToArray_m354304473(__this, method) ((  ChannelPacketU5BU5D_t3883591672* (*) (List_1_t1051718017 *, const MethodInfo*))List_1_ToArray_m354304473_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2279180759_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2279180759(__this, method) ((  void (*) (List_1_t1051718017 *, const MethodInfo*))List_1_TrimExcess_m2279180759_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m890926865_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m890926865(__this, method) ((  int32_t (*) (List_1_t1051718017 *, const MethodInfo*))List_1_get_Capacity_m890926865_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3595839860_gshared (List_1_t1051718017 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3595839860(__this, ___value0, method) ((  void (*) (List_1_t1051718017 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3595839860_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::get_Count()
extern "C"  int32_t List_1_get_Count_m2157271048_gshared (List_1_t1051718017 * __this, const MethodInfo* method);
#define List_1_get_Count_m2157271048(__this, method) ((  int32_t (*) (List_1_t1051718017 *, const MethodInfo*))List_1_get_Count_m2157271048_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::get_Item(System.Int32)
extern "C"  ChannelPacket_t1682596885  List_1_get_Item_m324350671_gshared (List_1_t1051718017 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m324350671(__this, ___index0, method) ((  ChannelPacket_t1682596885  (*) (List_1_t1051718017 *, int32_t, const MethodInfo*))List_1_get_Item_m324350671_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.ChannelPacket>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1232827943_gshared (List_1_t1051718017 * __this, int32_t ___index0, ChannelPacket_t1682596885  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1232827943(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1051718017 *, int32_t, ChannelPacket_t1682596885 , const MethodInfo*))List_1_set_Item_m1232827943_gshared)(__this, ___index0, ___value1, method)
