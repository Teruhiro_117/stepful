﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayRecButtons/TextureAtlasSrc
struct TextureAtlasSrc_t2048635151;

#include "codegen/il2cpp-codegen.h"

// System.Void EveryplayRecButtons/TextureAtlasSrc::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Single)
extern "C"  void TextureAtlasSrc__ctor_m2103365045 (TextureAtlasSrc_t2048635151 * __this, int32_t ___width0, int32_t ___height1, int32_t ___x2, int32_t ___y3, float ___scale4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
