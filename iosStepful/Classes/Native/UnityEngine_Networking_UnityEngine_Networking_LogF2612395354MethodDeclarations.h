﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.LogFilter
struct LogFilter_t2612395354;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.LogFilter::.ctor()
extern "C"  void LogFilter__ctor_m532696793 (LogFilter_t2612395354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.LogFilter::get_currentLogLevel()
extern "C"  int32_t LogFilter_get_currentLogLevel_m3710832137 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.LogFilter::set_currentLogLevel(System.Int32)
extern "C"  void LogFilter_set_currentLogLevel_m932644292 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.LogFilter::get_logDev()
extern "C"  bool LogFilter_get_logDev_m1227657219 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.LogFilter::get_logDebug()
extern "C"  bool LogFilter_get_logDebug_m1803179661 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.LogFilter::get_logInfo()
extern "C"  bool LogFilter_get_logInfo_m1178750562 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.LogFilter::get_logWarn()
extern "C"  bool LogFilter_get_logWarn_m3896982046 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.LogFilter::get_logError()
extern "C"  bool LogFilter_get_logError_m3315740406 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.LogFilter::get_logFatal()
extern "C"  bool LogFilter_get_logFatal_m2275092914 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.LogFilter::.cctor()
extern "C"  void LogFilter__cctor_m1501302990 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
