﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.LocalClient/InternalMsg>
struct DefaultComparer_t3761859251;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor()
extern "C"  void DefaultComparer__ctor_m1830279754_gshared (DefaultComparer_t3761859251 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1830279754(__this, method) ((  void (*) (DefaultComparer_t3761859251 *, const MethodInfo*))DefaultComparer__ctor_m1830279754_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Networking.LocalClient/InternalMsg>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m775814683_gshared (DefaultComparer_t3761859251 * __this, InternalMsg_t977621722  ___x0, InternalMsg_t977621722  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m775814683(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3761859251 *, InternalMsg_t977621722 , InternalMsg_t977621722 , const MethodInfo*))DefaultComparer_Compare_m775814683_gshared)(__this, ___x0, ___y1, method)
