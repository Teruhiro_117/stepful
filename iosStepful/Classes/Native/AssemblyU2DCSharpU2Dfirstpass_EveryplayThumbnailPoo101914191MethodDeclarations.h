﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EveryplayThumbnailPool
struct EveryplayThumbnailPool_t101914191;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2724090252;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"

// System.Void EveryplayThumbnailPool::.ctor()
extern "C"  void EveryplayThumbnailPool__ctor_m3938529262 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D[] EveryplayThumbnailPool::get_thumbnailTextures()
extern "C"  Texture2DU5BU5D_t2724090252* EveryplayThumbnailPool_get_thumbnailTextures_m3919877250 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::set_thumbnailTextures(UnityEngine.Texture2D[])
extern "C"  void EveryplayThumbnailPool_set_thumbnailTextures_m3824254701 (EveryplayThumbnailPool_t101914191 * __this, Texture2DU5BU5D_t2724090252* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EveryplayThumbnailPool::get_availableThumbnailCount()
extern "C"  int32_t EveryplayThumbnailPool_get_availableThumbnailCount_m4126025113 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::set_availableThumbnailCount(System.Int32)
extern "C"  void EveryplayThumbnailPool_set_availableThumbnailCount_m196460788 (EveryplayThumbnailPool_t101914191 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single EveryplayThumbnailPool::get_aspectRatio()
extern "C"  float EveryplayThumbnailPool_get_aspectRatio_m1000773640 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::set_aspectRatio(System.Single)
extern "C"  void EveryplayThumbnailPool_set_aspectRatio_m3386986733 (EveryplayThumbnailPool_t101914191 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 EveryplayThumbnailPool::get_thumbnailScale()
extern "C"  Vector2_t2243707579  EveryplayThumbnailPool_get_thumbnailScale_m1960597216 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::set_thumbnailScale(UnityEngine.Vector2)
extern "C"  void EveryplayThumbnailPool_set_thumbnailScale_m79783341 (EveryplayThumbnailPool_t101914191 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::Awake()
extern "C"  void EveryplayThumbnailPool_Awake_m277671193 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::Start()
extern "C"  void EveryplayThumbnailPool_Start_m3827185342 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::OnReadyForRecording(System.Boolean)
extern "C"  void EveryplayThumbnailPool_OnReadyForRecording_m2992974271 (EveryplayThumbnailPool_t101914191 * __this, bool ___ready0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::Initialize()
extern "C"  void EveryplayThumbnailPool_Initialize_m1620953478 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::OnRecordingStarted()
extern "C"  void EveryplayThumbnailPool_OnRecordingStarted_m1912222785 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::Update()
extern "C"  void EveryplayThumbnailPool_Update_m2223714429 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::OnThumbnailReady(UnityEngine.Texture2D,System.Boolean)
extern "C"  void EveryplayThumbnailPool_OnThumbnailReady_m1873378191 (EveryplayThumbnailPool_t101914191 * __this, Texture2D_t3542995729 * ___texture0, bool ___portrait1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::SetThumbnailTargetSize()
extern "C"  void EveryplayThumbnailPool_SetThumbnailTargetSize_m642502636 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EveryplayThumbnailPool::OnDestroy()
extern "C"  void EveryplayThumbnailPool_OnDestroy_m3028613635 (EveryplayThumbnailPool_t101914191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
