﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct List_1_t346742854;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct IEnumerable_1_t1269748767;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct IEnumerator_1_t2748112845;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct ICollection_1_t1929697027;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct ReadOnlyCollection_1_t1163407414;
// UnityEngine.Networking.LocalClient/InternalMsg[]
struct InternalMsgU5BU5D_t2023740543;
// System.Predicate`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct Predicate_1_t3715559133;
// System.Collections.Generic.IComparer`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct IComparer_1_t3227052140;
// System.Comparison`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct Comparison_1_t2239360573;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4176439824.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor()
extern "C"  void List_1__ctor_m2559816066_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1__ctor_m2559816066(__this, method) ((  void (*) (List_1_t346742854 *, const MethodInfo*))List_1__ctor_m2559816066_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3329718119_gshared (List_1_t346742854 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3329718119(__this, ___collection0, method) ((  void (*) (List_1_t346742854 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3329718119_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m528453285_gshared (List_1_t346742854 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m528453285(__this, ___capacity0, method) ((  void (*) (List_1_t346742854 *, int32_t, const MethodInfo*))List_1__ctor_m528453285_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::.cctor()
extern "C"  void List_1__cctor_m3631973679_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3631973679(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3631973679_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1497946486_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1497946486(__this, method) ((  Il2CppObject* (*) (List_1_t346742854 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1497946486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3690151660_gshared (List_1_t346742854 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3690151660(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t346742854 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3690151660_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1895272129_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1895272129(__this, method) ((  Il2CppObject * (*) (List_1_t346742854 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1895272129_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m385824720_gshared (List_1_t346742854 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m385824720(__this, ___item0, method) ((  int32_t (*) (List_1_t346742854 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m385824720_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m883330130_gshared (List_1_t346742854 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m883330130(__this, ___item0, method) ((  bool (*) (List_1_t346742854 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m883330130_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m816598370_gshared (List_1_t346742854 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m816598370(__this, ___item0, method) ((  int32_t (*) (List_1_t346742854 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m816598370_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3425582157_gshared (List_1_t346742854 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3425582157(__this, ___index0, ___item1, method) ((  void (*) (List_1_t346742854 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3425582157_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1261020469_gshared (List_1_t346742854 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1261020469(__this, ___item0, method) ((  void (*) (List_1_t346742854 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1261020469_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1075942009_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1075942009(__this, method) ((  bool (*) (List_1_t346742854 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1075942009_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3889806516_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3889806516(__this, method) ((  bool (*) (List_1_t346742854 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3889806516_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3981841010_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3981841010(__this, method) ((  Il2CppObject * (*) (List_1_t346742854 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3981841010_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3066096601_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3066096601(__this, method) ((  bool (*) (List_1_t346742854 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3066096601_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1970239552_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1970239552(__this, method) ((  bool (*) (List_1_t346742854 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1970239552_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1207774805_gshared (List_1_t346742854 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1207774805(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t346742854 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1207774805_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m945227654_gshared (List_1_t346742854 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m945227654(__this, ___index0, ___value1, method) ((  void (*) (List_1_t346742854 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m945227654_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::Add(T)
extern "C"  void List_1_Add_m1694919374_gshared (List_1_t346742854 * __this, InternalMsg_t977621722  ___item0, const MethodInfo* method);
#define List_1_Add_m1694919374(__this, ___item0, method) ((  void (*) (List_1_t346742854 *, InternalMsg_t977621722 , const MethodInfo*))List_1_Add_m1694919374_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m462144278_gshared (List_1_t346742854 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m462144278(__this, ___newCount0, method) ((  void (*) (List_1_t346742854 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m462144278_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2491716574_gshared (List_1_t346742854 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2491716574(__this, ___collection0, method) ((  void (*) (List_1_t346742854 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2491716574_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3637538718_gshared (List_1_t346742854 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3637538718(__this, ___enumerable0, method) ((  void (*) (List_1_t346742854 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3637538718_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m4010362477_gshared (List_1_t346742854 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m4010362477(__this, ___collection0, method) ((  void (*) (List_1_t346742854 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m4010362477_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t1163407414 * List_1_AsReadOnly_m2120805968_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2120805968(__this, method) ((  ReadOnlyCollection_1_t1163407414 * (*) (List_1_t346742854 *, const MethodInfo*))List_1_AsReadOnly_m2120805968_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::Clear()
extern "C"  void List_1_Clear_m4258161739_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_Clear_m4258161739(__this, method) ((  void (*) (List_1_t346742854 *, const MethodInfo*))List_1_Clear_m4258161739_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::Contains(T)
extern "C"  bool List_1_Contains_m1327413143_gshared (List_1_t346742854 * __this, InternalMsg_t977621722  ___item0, const MethodInfo* method);
#define List_1_Contains_m1327413143(__this, ___item0, method) ((  bool (*) (List_1_t346742854 *, InternalMsg_t977621722 , const MethodInfo*))List_1_Contains_m1327413143_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m4227469329_gshared (List_1_t346742854 * __this, InternalMsgU5BU5D_t2023740543* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m4227469329(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t346742854 *, InternalMsgU5BU5D_t2023740543*, int32_t, const MethodInfo*))List_1_CopyTo_m4227469329_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::Find(System.Predicate`1<T>)
extern "C"  InternalMsg_t977621722  List_1_Find_m2027437487_gshared (List_1_t346742854 * __this, Predicate_1_t3715559133 * ___match0, const MethodInfo* method);
#define List_1_Find_m2027437487(__this, ___match0, method) ((  InternalMsg_t977621722  (*) (List_1_t346742854 *, Predicate_1_t3715559133 *, const MethodInfo*))List_1_Find_m2027437487_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1057617900_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3715559133 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1057617900(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3715559133 *, const MethodInfo*))List_1_CheckMatch_m1057617900_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m331475863_gshared (List_1_t346742854 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3715559133 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m331475863(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t346742854 *, int32_t, int32_t, Predicate_1_t3715559133 *, const MethodInfo*))List_1_GetIndex_m331475863_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::GetEnumerator()
extern "C"  Enumerator_t4176439824  List_1_GetEnumerator_m101143168_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m101143168(__this, method) ((  Enumerator_t4176439824  (*) (List_1_t346742854 *, const MethodInfo*))List_1_GetEnumerator_m101143168_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m911245073_gshared (List_1_t346742854 * __this, InternalMsg_t977621722  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m911245073(__this, ___item0, method) ((  int32_t (*) (List_1_t346742854 *, InternalMsg_t977621722 , const MethodInfo*))List_1_IndexOf_m911245073_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3469022864_gshared (List_1_t346742854 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3469022864(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t346742854 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3469022864_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m1242106885_gshared (List_1_t346742854 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m1242106885(__this, ___index0, method) ((  void (*) (List_1_t346742854 *, int32_t, const MethodInfo*))List_1_CheckIndex_m1242106885_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2902547166_gshared (List_1_t346742854 * __this, int32_t ___index0, InternalMsg_t977621722  ___item1, const MethodInfo* method);
#define List_1_Insert_m2902547166(__this, ___index0, ___item1, method) ((  void (*) (List_1_t346742854 *, int32_t, InternalMsg_t977621722 , const MethodInfo*))List_1_Insert_m2902547166_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m451974835_gshared (List_1_t346742854 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m451974835(__this, ___collection0, method) ((  void (*) (List_1_t346742854 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m451974835_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::Remove(T)
extern "C"  bool List_1_Remove_m388263088_gshared (List_1_t346742854 * __this, InternalMsg_t977621722  ___item0, const MethodInfo* method);
#define List_1_Remove_m388263088(__this, ___item0, method) ((  bool (*) (List_1_t346742854 *, InternalMsg_t977621722 , const MethodInfo*))List_1_Remove_m388263088_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3659619464_gshared (List_1_t346742854 * __this, Predicate_1_t3715559133 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3659619464(__this, ___match0, method) ((  int32_t (*) (List_1_t346742854 *, Predicate_1_t3715559133 *, const MethodInfo*))List_1_RemoveAll_m3659619464_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3200000450_gshared (List_1_t346742854 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m3200000450(__this, ___index0, method) ((  void (*) (List_1_t346742854 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3200000450_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::Reverse()
extern "C"  void List_1_Reverse_m3133049938_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_Reverse_m3133049938(__this, method) ((  void (*) (List_1_t346742854 *, const MethodInfo*))List_1_Reverse_m3133049938_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::Sort()
extern "C"  void List_1_Sort_m4244214290_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_Sort_m4244214290(__this, method) ((  void (*) (List_1_t346742854 *, const MethodInfo*))List_1_Sort_m4244214290_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1504315038_gshared (List_1_t346742854 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1504315038(__this, ___comparer0, method) ((  void (*) (List_1_t346742854 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1504315038_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m680405559_gshared (List_1_t346742854 * __this, Comparison_1_t2239360573 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m680405559(__this, ___comparison0, method) ((  void (*) (List_1_t346742854 *, Comparison_1_t2239360573 *, const MethodInfo*))List_1_Sort_m680405559_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::ToArray()
extern "C"  InternalMsgU5BU5D_t2023740543* List_1_ToArray_m3745665659_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_ToArray_m3745665659(__this, method) ((  InternalMsgU5BU5D_t2023740543* (*) (List_1_t346742854 *, const MethodInfo*))List_1_ToArray_m3745665659_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::TrimExcess()
extern "C"  void List_1_TrimExcess_m4261499033_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m4261499033(__this, method) ((  void (*) (List_1_t346742854 *, const MethodInfo*))List_1_TrimExcess_m4261499033_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1945099111_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1945099111(__this, method) ((  int32_t (*) (List_1_t346742854 *, const MethodInfo*))List_1_get_Capacity_m1945099111_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2900819422_gshared (List_1_t346742854 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2900819422(__this, ___value0, method) ((  void (*) (List_1_t346742854 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2900819422_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::get_Count()
extern "C"  int32_t List_1_get_Count_m569721562_gshared (List_1_t346742854 * __this, const MethodInfo* method);
#define List_1_get_Count_m569721562(__this, method) ((  int32_t (*) (List_1_t346742854 *, const MethodInfo*))List_1_get_Count_m569721562_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::get_Item(System.Int32)
extern "C"  InternalMsg_t977621722  List_1_get_Item_m1551656265_gshared (List_1_t346742854 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m1551656265(__this, ___index0, method) ((  InternalMsg_t977621722  (*) (List_1_t346742854 *, int32_t, const MethodInfo*))List_1_get_Item_m1551656265_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.LocalClient/InternalMsg>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2100516825_gshared (List_1_t346742854 * __this, int32_t ___index0, InternalMsg_t977621722  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2100516825(__this, ___index0, ___value1, method) ((  void (*) (List_1_t346742854 *, int32_t, InternalMsg_t977621722 , const MethodInfo*))List_1_set_Item_m2100516825_gshared)(__this, ___index0, ___value1, method)
