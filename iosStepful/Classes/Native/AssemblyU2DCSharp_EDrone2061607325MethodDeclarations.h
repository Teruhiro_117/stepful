﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EDrone
struct EDrone_t2061607325;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void EDrone::.ctor()
extern "C"  void EDrone__ctor_m3222050530 (EDrone_t2061607325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDrone::Init(System.String)
extern "C"  void EDrone_Init_m3295800248 (EDrone_t2061607325 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDrone::Move(System.Int32,System.Int32)
extern "C"  void EDrone_Move_m1270797897 (EDrone_t2061607325 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
