﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Skytree
struct Skytree_t850646749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Skytree::.ctor()
extern "C"  void Skytree__ctor_m1984419468 (Skytree_t850646749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Skytree::Init(System.String)
extern "C"  void Skytree_Init_m2468843630 (Skytree_t850646749 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
