﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct Comparer_1_t3709064612;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.ctor()
extern "C"  void Comparer_1__ctor_m1013408590_gshared (Comparer_1_t3709064612 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m1013408590(__this, method) ((  void (*) (Comparer_1_t3709064612 *, const MethodInfo*))Comparer_1__ctor_m1013408590_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.cctor()
extern "C"  void Comparer_1__cctor_m281655745_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m281655745(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m281655745_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1167227933_gshared (Comparer_1_t3709064612 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m1167227933(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t3709064612 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m1167227933_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::get_Default()
extern "C"  Comparer_1_t3709064612 * Comparer_1_get_Default_m3158327170_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m3158327170(__this /* static, unused */, method) ((  Comparer_1_t3709064612 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m3158327170_gshared)(__this /* static, unused */, method)
