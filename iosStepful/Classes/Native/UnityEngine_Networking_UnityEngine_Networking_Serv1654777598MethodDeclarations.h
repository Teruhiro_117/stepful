﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.ServerCallbackAttribute
struct ServerCallbackAttribute_t1654777598;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.ServerCallbackAttribute::.ctor()
extern "C"  void ServerCallbackAttribute__ctor_m1002166089 (ServerCallbackAttribute_t1654777598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
