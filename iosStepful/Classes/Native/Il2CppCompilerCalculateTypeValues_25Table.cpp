﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "UnityEngine_Networking_U3CModuleU3E3783534214.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan3774159139.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan1682596885.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clie3650307179.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clien877818919.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Conn2445648839.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo290247225.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync2509757400.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Comm3380140335.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clie2297656918.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Targ3571055858.h"
#include "UnityEngine_Networking_UnityEngine_Networking_SyncE996312527.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Serv2338651999.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Serv1654777598.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clie2502255907.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clie2024078602.h"
#include "UnityEngine_Networking_UnityEngine_Networking_DotN4023688896.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Loca4139140194.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"
#include "UnityEngine_Networking_UnityEngine_Networking_ULoca815699498.h"
#include "UnityEngine_Networking_UnityEngine_Networking_ULoc2986899766.h"
#include "UnityEngine_Networking_UnityEngine_Networking_LogF2612395354.h"
#include "UnityEngine_Networking_UnityEngine_Networking_LogF2308008460.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Mess2552428296.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3906797298.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2422550789.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3203285442.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo775412683.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo127189048.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo745889821.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw4197260945.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3520841950.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1205003694.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo396494387.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw4070066639.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo297244123.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3062411074.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo320416085.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3027639263.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3823788215.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3106274748.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo225159112.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo648218179.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3795345151.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3934203181.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo349763583.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo949407937.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3495336496.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo524088197.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1549987785.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo579711437.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3873055601.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2573314724.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw4096515284.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3239134783.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3859202811.h"
#include "UnityEngine_Networking_UnityEngine_Networking_NetB3875182795.h"
#include "UnityEngine_Networking_UnityEngine_Networking_UInt1624724448.h"
#include "UnityEngine_Networking_UnityEngine_Networking_UInt1232155165.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Floa4266763898.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo696867603.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2530234696.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo107267906.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo691343460.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3393861890.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo646317982.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2507[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2508[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2514[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2519[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2527[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2531[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (ChannelBuffer_t3774159139), -1, sizeof(ChannelBuffer_t3774159139_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2535[30] = 
{
	ChannelBuffer_t3774159139::get_offset_of_m_Connection_0(),
	ChannelBuffer_t3774159139::get_offset_of_m_CurrentPacket_1(),
	ChannelBuffer_t3774159139::get_offset_of_m_LastFlushTime_2(),
	ChannelBuffer_t3774159139::get_offset_of_m_ChannelId_3(),
	ChannelBuffer_t3774159139::get_offset_of_m_MaxPacketSize_4(),
	ChannelBuffer_t3774159139::get_offset_of_m_IsReliable_5(),
	ChannelBuffer_t3774159139::get_offset_of_m_AllowFragmentation_6(),
	ChannelBuffer_t3774159139::get_offset_of_m_IsBroken_7(),
	ChannelBuffer_t3774159139::get_offset_of_m_MaxPendingPacketCount_8(),
	0,
	0,
	0,
	ChannelBuffer_t3774159139::get_offset_of_m_PendingPackets_12(),
	ChannelBuffer_t3774159139_StaticFields::get_offset_of_s_FreePackets_13(),
	ChannelBuffer_t3774159139_StaticFields::get_offset_of_pendingPacketCount_14(),
	ChannelBuffer_t3774159139::get_offset_of_maxDelay_15(),
	ChannelBuffer_t3774159139::get_offset_of_m_LastBufferedMessageCountTimer_16(),
	ChannelBuffer_t3774159139::get_offset_of_U3CnumMsgsOutU3Ek__BackingField_17(),
	ChannelBuffer_t3774159139::get_offset_of_U3CnumBufferedMsgsOutU3Ek__BackingField_18(),
	ChannelBuffer_t3774159139::get_offset_of_U3CnumBytesOutU3Ek__BackingField_19(),
	ChannelBuffer_t3774159139::get_offset_of_U3CnumMsgsInU3Ek__BackingField_20(),
	ChannelBuffer_t3774159139::get_offset_of_U3CnumBytesInU3Ek__BackingField_21(),
	ChannelBuffer_t3774159139::get_offset_of_U3CnumBufferedPerSecondU3Ek__BackingField_22(),
	ChannelBuffer_t3774159139::get_offset_of_U3ClastBufferedPerSecondU3Ek__BackingField_23(),
	ChannelBuffer_t3774159139_StaticFields::get_offset_of_s_SendWriter_24(),
	ChannelBuffer_t3774159139_StaticFields::get_offset_of_s_FragmentWriter_25(),
	0,
	ChannelBuffer_t3774159139::get_offset_of_m_Disposed_27(),
	ChannelBuffer_t3774159139::get_offset_of_fragmentBuffer_28(),
	ChannelBuffer_t3774159139::get_offset_of_readingFragment_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (ChannelPacket_t1682596885)+ sizeof (Il2CppObject), sizeof(ChannelPacket_t1682596885_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2536[3] = 
{
	ChannelPacket_t1682596885::get_offset_of_m_Position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ChannelPacket_t1682596885::get_offset_of_m_Buffer_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ChannelPacket_t1682596885::get_offset_of_m_IsReliable_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (ClientScene_t3650307179), -1, sizeof(ClientScene_t3650307179_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2537[36] = 
{
	ClientScene_t3650307179_StaticFields::get_offset_of_s_LocalPlayers_0(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_ReadyConnection_1(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_SpawnableObjects_2(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_IsReady_3(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_IsSpawnFinished_4(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_NetworkScene_5(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_ObjectSpawnSceneMessage_6(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_ObjectSpawnFinishedMessage_7(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_ObjectDestroyMessage_8(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_ObjectSpawnMessage_9(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_OwnerMessage_10(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_ClientAuthorityMessage_11(),
	0,
	0,
	ClientScene_t3650307179_StaticFields::get_offset_of_s_ReconnectId_14(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_Peers_15(),
	ClientScene_t3650307179_StaticFields::get_offset_of_s_PendingOwnerIds_16(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_17(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_18(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_19(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_20(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_21(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_22(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_23(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_24(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_25(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_26(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_27(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_28(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_29(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_30(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_31(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_32(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_33(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache11_34(),
	ClientScene_t3650307179_StaticFields::get_offset_of_U3CU3Ef__mgU24cache12_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (PendingOwner_t877818919)+ sizeof (Il2CppObject), sizeof(PendingOwner_t877818919 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2538[2] = 
{
	PendingOwner_t877818919::get_offset_of_netId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PendingOwner_t877818919::get_offset_of_playerControllerId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (ConnectionArray_t2445648839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[2] = 
{
	ConnectionArray_t2445648839::get_offset_of_m_LocalConnections_0(),
	ConnectionArray_t2445648839::get_offset_of_m_Connections_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (NetworkSettingsAttribute_t290247225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[2] = 
{
	NetworkSettingsAttribute_t290247225::get_offset_of_channel_0(),
	NetworkSettingsAttribute_t290247225::get_offset_of_sendInterval_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (SyncVarAttribute_t2509757400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[1] = 
{
	SyncVarAttribute_t2509757400::get_offset_of_hook_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (CommandAttribute_t3380140335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[1] = 
{
	CommandAttribute_t3380140335::get_offset_of_channel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (ClientRpcAttribute_t2297656918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[1] = 
{
	ClientRpcAttribute_t2297656918::get_offset_of_channel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (TargetRpcAttribute_t3571055858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[1] = 
{
	TargetRpcAttribute_t3571055858::get_offset_of_channel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (SyncEventAttribute_t996312527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[1] = 
{
	SyncEventAttribute_t996312527::get_offset_of_channel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (ServerAttribute_t2338651999), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (ServerCallbackAttribute_t1654777598), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (ClientAttribute_t2502255907), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (ClientCallbackAttribute_t2024078602), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (DotNetCompatibility_t4023688896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (LocalClient_t4139140194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[7] = 
{
	0,
	LocalClient_t4139140194::get_offset_of_m_InternalMsgs_24(),
	LocalClient_t4139140194::get_offset_of_m_InternalMsgs2_25(),
	LocalClient_t4139140194::get_offset_of_m_FreeMessages_26(),
	LocalClient_t4139140194::get_offset_of_m_LocalServer_27(),
	LocalClient_t4139140194::get_offset_of_m_Connected_28(),
	LocalClient_t4139140194::get_offset_of_s_InternalMessage_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (InternalMsg_t977621722)+ sizeof (Il2CppObject), sizeof(InternalMsg_t977621722_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2552[2] = 
{
	InternalMsg_t977621722::get_offset_of_buffer_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalMsg_t977621722::get_offset_of_channelId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (ULocalConnectionToClient_t815699498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[1] = 
{
	ULocalConnectionToClient_t815699498::get_offset_of_m_LocalClient_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (ULocalConnectionToServer_t2986899766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[1] = 
{
	ULocalConnectionToServer_t2986899766::get_offset_of_m_LocalServer_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (LogFilter_t2612395354), -1, sizeof(LogFilter_t2612395354_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2555[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	LogFilter_t2612395354_StaticFields::get_offset_of_current_6(),
	LogFilter_t2612395354_StaticFields::get_offset_of_s_CurrentLogLevel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (FilterLevel_t2308008460)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2556[7] = 
{
	FilterLevel_t2308008460::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (MessageBase_t2552428296), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (StringMessage_t3906797298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[1] = 
{
	StringMessage_t3906797298::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (IntegerMessage_t2422550789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[1] = 
{
	IntegerMessage_t2422550789::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (EmptyMessage_t3203285442), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (ErrorMessage_t775412683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[1] = 
{
	ErrorMessage_t775412683::get_offset_of_errorCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (ReadyMessage_t127189048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (NotReadyMessage_t745889821), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (AddPlayerMessage_t4197260945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[3] = 
{
	AddPlayerMessage_t4197260945::get_offset_of_playerControllerId_0(),
	AddPlayerMessage_t4197260945::get_offset_of_msgSize_1(),
	AddPlayerMessage_t4197260945::get_offset_of_msgData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (RemovePlayerMessage_t3520841950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[1] = 
{
	RemovePlayerMessage_t3520841950::get_offset_of_playerControllerId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (PeerAuthorityMessage_t1205003694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[3] = 
{
	PeerAuthorityMessage_t1205003694::get_offset_of_connectionId_0(),
	PeerAuthorityMessage_t1205003694::get_offset_of_netId_1(),
	PeerAuthorityMessage_t1205003694::get_offset_of_authorityState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (PeerInfoPlayer_t396494387)+ sizeof (Il2CppObject), sizeof(PeerInfoPlayer_t396494387 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2567[2] = 
{
	PeerInfoPlayer_t396494387::get_offset_of_netId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PeerInfoPlayer_t396494387::get_offset_of_playerControllerId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (PeerInfoMessage_t4070066639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[6] = 
{
	PeerInfoMessage_t4070066639::get_offset_of_connectionId_0(),
	PeerInfoMessage_t4070066639::get_offset_of_address_1(),
	PeerInfoMessage_t4070066639::get_offset_of_port_2(),
	PeerInfoMessage_t4070066639::get_offset_of_isHost_3(),
	PeerInfoMessage_t4070066639::get_offset_of_isYou_4(),
	PeerInfoMessage_t4070066639::get_offset_of_playerIds_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (PeerListMessage_t297244123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2569[2] = 
{
	PeerListMessage_t297244123::get_offset_of_peers_0(),
	PeerListMessage_t297244123::get_offset_of_oldServerConnectionId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (ReconnectMessage_t3062411074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[5] = 
{
	ReconnectMessage_t3062411074::get_offset_of_oldConnectionId_0(),
	ReconnectMessage_t3062411074::get_offset_of_playerControllerId_1(),
	ReconnectMessage_t3062411074::get_offset_of_netId_2(),
	ReconnectMessage_t3062411074::get_offset_of_msgSize_3(),
	ReconnectMessage_t3062411074::get_offset_of_msgData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (ObjectSpawnMessage_t320416085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[5] = 
{
	ObjectSpawnMessage_t320416085::get_offset_of_netId_0(),
	ObjectSpawnMessage_t320416085::get_offset_of_assetId_1(),
	ObjectSpawnMessage_t320416085::get_offset_of_position_2(),
	ObjectSpawnMessage_t320416085::get_offset_of_payload_3(),
	ObjectSpawnMessage_t320416085::get_offset_of_rotation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (ObjectSpawnSceneMessage_t3027639263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[4] = 
{
	ObjectSpawnSceneMessage_t3027639263::get_offset_of_netId_0(),
	ObjectSpawnSceneMessage_t3027639263::get_offset_of_sceneId_1(),
	ObjectSpawnSceneMessage_t3027639263::get_offset_of_position_2(),
	ObjectSpawnSceneMessage_t3027639263::get_offset_of_payload_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (ObjectSpawnFinishedMessage_t3823788215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[1] = 
{
	ObjectSpawnFinishedMessage_t3823788215::get_offset_of_state_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (ObjectDestroyMessage_t3106274748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[1] = 
{
	ObjectDestroyMessage_t3106274748::get_offset_of_netId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (OwnerMessage_t225159112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[2] = 
{
	OwnerMessage_t225159112::get_offset_of_netId_0(),
	OwnerMessage_t225159112::get_offset_of_playerControllerId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (ClientAuthorityMessage_t648218179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[2] = 
{
	ClientAuthorityMessage_t648218179::get_offset_of_netId_0(),
	ClientAuthorityMessage_t648218179::get_offset_of_authority_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (OverrideTransformMessage_t3795345151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[4] = 
{
	OverrideTransformMessage_t3795345151::get_offset_of_netId_0(),
	OverrideTransformMessage_t3795345151::get_offset_of_payload_1(),
	OverrideTransformMessage_t3795345151::get_offset_of_teleport_2(),
	OverrideTransformMessage_t3795345151::get_offset_of_time_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (AnimationMessage_t3934203181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[4] = 
{
	AnimationMessage_t3934203181::get_offset_of_netId_0(),
	AnimationMessage_t3934203181::get_offset_of_stateHash_1(),
	AnimationMessage_t3934203181::get_offset_of_normalizedTime_2(),
	AnimationMessage_t3934203181::get_offset_of_parameters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (AnimationParametersMessage_t349763583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[2] = 
{
	AnimationParametersMessage_t349763583::get_offset_of_netId_0(),
	AnimationParametersMessage_t349763583::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (AnimationTriggerMessage_t949407937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[2] = 
{
	AnimationTriggerMessage_t949407937::get_offset_of_netId_0(),
	AnimationTriggerMessage_t949407937::get_offset_of_hash_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (LobbyReadyToBeginMessage_t3495336496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[2] = 
{
	LobbyReadyToBeginMessage_t3495336496::get_offset_of_slotId_0(),
	LobbyReadyToBeginMessage_t3495336496::get_offset_of_readyState_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (CRCMessageEntry_t524088197)+ sizeof (Il2CppObject), sizeof(CRCMessageEntry_t524088197_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2582[2] = 
{
	CRCMessageEntry_t524088197::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CRCMessageEntry_t524088197::get_offset_of_channel_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (CRCMessage_t1549987785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[1] = 
{
	CRCMessage_t1549987785::get_offset_of_scripts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (NetworkAnimator_t579711437), -1, sizeof(NetworkAnimator_t579711437_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2584[15] = 
{
	NetworkAnimator_t579711437::get_offset_of_m_Animator_8(),
	NetworkAnimator_t579711437::get_offset_of_m_ParameterSendBits_9(),
	NetworkAnimator_t579711437_StaticFields::get_offset_of_s_AnimationMessage_10(),
	NetworkAnimator_t579711437_StaticFields::get_offset_of_s_AnimationParametersMessage_11(),
	NetworkAnimator_t579711437_StaticFields::get_offset_of_s_AnimationTriggerMessage_12(),
	NetworkAnimator_t579711437::get_offset_of_m_AnimationHash_13(),
	NetworkAnimator_t579711437::get_offset_of_m_TransitionHash_14(),
	NetworkAnimator_t579711437::get_offset_of_m_ParameterWriter_15(),
	NetworkAnimator_t579711437::get_offset_of_m_SendTimer_16(),
	NetworkAnimator_t579711437::get_offset_of_param0_17(),
	NetworkAnimator_t579711437::get_offset_of_param1_18(),
	NetworkAnimator_t579711437::get_offset_of_param2_19(),
	NetworkAnimator_t579711437::get_offset_of_param3_20(),
	NetworkAnimator_t579711437::get_offset_of_param4_21(),
	NetworkAnimator_t579711437::get_offset_of_param5_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (NetworkBehaviour_t3873055601), -1, sizeof(NetworkBehaviour_t3873055601_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2585[6] = 
{
	NetworkBehaviour_t3873055601::get_offset_of_m_SyncVarDirtyBits_2(),
	NetworkBehaviour_t3873055601::get_offset_of_m_LastSendTime_3(),
	NetworkBehaviour_t3873055601::get_offset_of_m_SyncVarGuard_4(),
	0,
	NetworkBehaviour_t3873055601::get_offset_of_m_MyView_6(),
	NetworkBehaviour_t3873055601_StaticFields::get_offset_of_s_CmdHandlerDelegates_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (CmdDelegate_t2573314724), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (EventDelegate_t4096515284), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (UNetInvokeType_t3239134783)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2588[5] = 
{
	UNetInvokeType_t3239134783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (Invoker_t3859202811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[3] = 
{
	Invoker_t3859202811::get_offset_of_invokeType_0(),
	Invoker_t3859202811::get_offset_of_invokeClass_1(),
	Invoker_t3859202811::get_offset_of_invokeFunction_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (NetBuffer_t3875182795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[5] = 
{
	NetBuffer_t3875182795::get_offset_of_m_Buffer_0(),
	NetBuffer_t3875182795::get_offset_of_m_Pos_1(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (UIntFloat_t1624724448)+ sizeof (Il2CppObject), sizeof(UIntFloat_t1624724448 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2591[4] = 
{
	UIntFloat_t1624724448::get_offset_of_floatValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIntFloat_t1624724448::get_offset_of_intValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIntFloat_t1624724448::get_offset_of_doubleValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIntFloat_t1624724448::get_offset_of_longValue_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (UIntDecimal_t1232155165)+ sizeof (Il2CppObject), sizeof(UIntDecimal_t1232155165 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2592[3] = 
{
	UIntDecimal_t1232155165::get_offset_of_longValue1_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIntDecimal_t1232155165::get_offset_of_longValue2_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIntDecimal_t1232155165::get_offset_of_decimalValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (FloatConversion_t4266763898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (NetworkClient_t696867603), -1, sizeof(NetworkClient_t696867603_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2594[23] = 
{
	NetworkClient_t696867603::get_offset_of_m_NetworkConnectionClass_0(),
	0,
	NetworkClient_t696867603_StaticFields::get_offset_of_s_Clients_2(),
	NetworkClient_t696867603_StaticFields::get_offset_of_s_IsActive_3(),
	NetworkClient_t696867603::get_offset_of_m_HostTopology_4(),
	NetworkClient_t696867603::get_offset_of_m_UseSimulator_5(),
	NetworkClient_t696867603::get_offset_of_m_SimulatedLatency_6(),
	NetworkClient_t696867603::get_offset_of_m_PacketLoss_7(),
	NetworkClient_t696867603::get_offset_of_m_ServerIp_8(),
	NetworkClient_t696867603::get_offset_of_m_ServerPort_9(),
	NetworkClient_t696867603::get_offset_of_m_ClientId_10(),
	NetworkClient_t696867603::get_offset_of_m_ClientConnectionId_11(),
	NetworkClient_t696867603::get_offset_of_m_StatResetTime_12(),
	NetworkClient_t696867603::get_offset_of_m_RemoteEndPoint_13(),
	NetworkClient_t696867603_StaticFields::get_offset_of_s_CRCMessage_14(),
	NetworkClient_t696867603::get_offset_of_m_MessageHandlers_15(),
	NetworkClient_t696867603::get_offset_of_m_Connection_16(),
	NetworkClient_t696867603::get_offset_of_m_MsgBuffer_17(),
	NetworkClient_t696867603::get_offset_of_m_MsgReader_18(),
	NetworkClient_t696867603::get_offset_of_m_AsyncConnect_19(),
	NetworkClient_t696867603::get_offset_of_m_RequestedServerHost_20(),
	NetworkClient_t696867603_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_21(),
	NetworkClient_t696867603_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (ConnectState_t2530234696)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2595[8] = 
{
	ConnectState_t2530234696::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (NetworkConnection_t107267906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[19] = 
{
	NetworkConnection_t107267906::get_offset_of_m_Channels_0(),
	NetworkConnection_t107267906::get_offset_of_m_PlayerControllers_1(),
	NetworkConnection_t107267906::get_offset_of_m_NetMsg_2(),
	NetworkConnection_t107267906::get_offset_of_m_VisList_3(),
	NetworkConnection_t107267906::get_offset_of_m_Writer_4(),
	NetworkConnection_t107267906::get_offset_of_m_MessageHandlersDict_5(),
	NetworkConnection_t107267906::get_offset_of_m_MessageHandlers_6(),
	NetworkConnection_t107267906::get_offset_of_m_ClientOwnedObjects_7(),
	NetworkConnection_t107267906::get_offset_of_m_MessageInfo_8(),
	0,
	NetworkConnection_t107267906::get_offset_of_error_10(),
	NetworkConnection_t107267906::get_offset_of_hostId_11(),
	NetworkConnection_t107267906::get_offset_of_connectionId_12(),
	NetworkConnection_t107267906::get_offset_of_isReady_13(),
	NetworkConnection_t107267906::get_offset_of_address_14(),
	NetworkConnection_t107267906::get_offset_of_lastMessageTime_15(),
	NetworkConnection_t107267906::get_offset_of_logNetworkMessages_16(),
	NetworkConnection_t107267906::get_offset_of_m_PacketStats_17(),
	NetworkConnection_t107267906::get_offset_of_m_Disposed_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (PacketStat_t691343460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[3] = 
{
	PacketStat_t691343460::get_offset_of_msgType_0(),
	PacketStat_t691343460::get_offset_of_count_1(),
	PacketStat_t691343460::get_offset_of_bytes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (NetworkCRC_t3393861890), -1, sizeof(NetworkCRC_t3393861890_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2598[3] = 
{
	NetworkCRC_t3393861890_StaticFields::get_offset_of_s_Singleton_0(),
	NetworkCRC_t3393861890::get_offset_of_m_Scripts_1(),
	NetworkCRC_t3393861890::get_offset_of_m_ScriptCRCCheck_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (NetworkBroadcastResult_t646317982)+ sizeof (Il2CppObject), sizeof(NetworkBroadcastResult_t646317982_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2599[2] = 
{
	NetworkBroadcastResult_t646317982::get_offset_of_serverAddress_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	NetworkBroadcastResult_t646317982::get_offset_of_broadcastData_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
