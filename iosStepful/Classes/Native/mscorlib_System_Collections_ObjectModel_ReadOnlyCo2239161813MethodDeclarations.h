﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct ReadOnlyCollection_1_t2239161813;
// System.Collections.Generic.IList`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct IList_1_t2594316722;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo[]
struct PendingPlayerInfoU5BU5D_t2199507332;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct IEnumerator_1_t3823867244;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2053376121.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2129761435_gshared (ReadOnlyCollection_1_t2239161813 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2129761435(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2129761435_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3201530145_gshared (ReadOnlyCollection_1_t2239161813 * __this, PendingPlayerInfo_t2053376121  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3201530145(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3201530145_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m498150557_gshared (ReadOnlyCollection_1_t2239161813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m498150557(__this, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m498150557_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3120250896_gshared (ReadOnlyCollection_1_t2239161813 * __this, int32_t ___index0, PendingPlayerInfo_t2053376121  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3120250896(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, int32_t, PendingPlayerInfo_t2053376121 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3120250896_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2959776354_gshared (ReadOnlyCollection_1_t2239161813 * __this, PendingPlayerInfo_t2053376121  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2959776354(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2239161813 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2959776354_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m384646804_gshared (ReadOnlyCollection_1_t2239161813 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m384646804(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m384646804_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  PendingPlayerInfo_t2053376121  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m310907072_gshared (ReadOnlyCollection_1_t2239161813 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m310907072(__this, ___index0, method) ((  PendingPlayerInfo_t2053376121  (*) (ReadOnlyCollection_1_t2239161813 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m310907072_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m518997537_gshared (ReadOnlyCollection_1_t2239161813 * __this, int32_t ___index0, PendingPlayerInfo_t2053376121  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m518997537(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, int32_t, PendingPlayerInfo_t2053376121 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m518997537_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m463330885_gshared (ReadOnlyCollection_1_t2239161813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m463330885(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2239161813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m463330885_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1469593954_gshared (ReadOnlyCollection_1_t2239161813 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1469593954(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1469593954_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1212204909_gshared (ReadOnlyCollection_1_t2239161813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1212204909(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2239161813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1212204909_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m601312038_gshared (ReadOnlyCollection_1_t2239161813 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m601312038(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2239161813 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m601312038_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m2132186768_gshared (ReadOnlyCollection_1_t2239161813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m2132186768(__this, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m2132186768_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m931462044_gshared (ReadOnlyCollection_1_t2239161813 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m931462044(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2239161813 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m931462044_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2721952024_gshared (ReadOnlyCollection_1_t2239161813 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2721952024(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2239161813 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2721952024_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2483413945_gshared (ReadOnlyCollection_1_t2239161813 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2483413945(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2483413945_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3202526377_gshared (ReadOnlyCollection_1_t2239161813 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3202526377(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3202526377_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2430647127_gshared (ReadOnlyCollection_1_t2239161813 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2430647127(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2430647127_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2510644554_gshared (ReadOnlyCollection_1_t2239161813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2510644554(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2239161813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m2510644554_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m154790044_gshared (ReadOnlyCollection_1_t2239161813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m154790044(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2239161813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m154790044_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3459954797_gshared (ReadOnlyCollection_1_t2239161813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3459954797(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2239161813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3459954797_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2305779446_gshared (ReadOnlyCollection_1_t2239161813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2305779446(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2239161813 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2305779446_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1040971049_gshared (ReadOnlyCollection_1_t2239161813 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1040971049(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2239161813 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1040971049_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m797289212_gshared (ReadOnlyCollection_1_t2239161813 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m797289212(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m797289212_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m687445259_gshared (ReadOnlyCollection_1_t2239161813 * __this, PendingPlayerInfo_t2053376121  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m687445259(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2239161813 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))ReadOnlyCollection_1_Contains_m687445259_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m580326365_gshared (ReadOnlyCollection_1_t2239161813 * __this, PendingPlayerInfoU5BU5D_t2199507332* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m580326365(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2239161813 *, PendingPlayerInfoU5BU5D_t2199507332*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m580326365_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1789210682_gshared (ReadOnlyCollection_1_t2239161813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1789210682(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2239161813 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1789210682_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3348757245_gshared (ReadOnlyCollection_1_t2239161813 * __this, PendingPlayerInfo_t2053376121  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3348757245(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2239161813 *, PendingPlayerInfo_t2053376121 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3348757245_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m742740742_gshared (ReadOnlyCollection_1_t2239161813 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m742740742(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2239161813 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m742740742_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::get_Item(System.Int32)
extern "C"  PendingPlayerInfo_t2053376121  ReadOnlyCollection_1_get_Item_m1764556540_gshared (ReadOnlyCollection_1_t2239161813 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1764556540(__this, ___index0, method) ((  PendingPlayerInfo_t2053376121  (*) (ReadOnlyCollection_1_t2239161813 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1764556540_gshared)(__this, ___index0, method)
