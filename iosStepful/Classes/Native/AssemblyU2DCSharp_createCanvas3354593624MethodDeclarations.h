﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// createCanvas
struct createCanvas_t3354593624;

#include "codegen/il2cpp-codegen.h"

// System.Void createCanvas::.ctor()
extern "C"  void createCanvas__ctor_m1636400783 (createCanvas_t3354593624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void createCanvas::Start()
extern "C"  void createCanvas_Start_m3083297603 (createCanvas_t3354593624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void createCanvas::Update()
extern "C"  void createCanvas_Update_m656525814 (createCanvas_t3354593624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
