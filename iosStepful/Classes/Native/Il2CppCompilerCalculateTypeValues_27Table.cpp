﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Data3569509720.h"
#include "AssemblyU2DCSharp_Distribution1524299538.h"
#include "AssemblyU2DCSharp_SaveScript1729063510.h"
#include "AssemblyU2DCSharp_arrangeKoma3423810832.h"
#include "AssemblyU2DCSharp_arrangeKoma_KOMAINF446687068.h"
#include "AssemblyU2DCSharp_selectKoma1760479012.h"
#include "AssemblyU2DCSharp_selectPlace742894733.h"
#include "AssemblyU2DCSharp_inputName1068137479.h"
#include "AssemblyU2DCSharp_Koma3011766596.h"
#include "AssemblyU2DCSharp_Empty4012560223.h"
#include "AssemblyU2DCSharp_Human1156088493.h"
#include "AssemblyU2DCSharp_GRobot456225233.h"
#include "AssemblyU2DCSharp_SRobot456246757.h"
#include "AssemblyU2DCSharp_Missile813944928.h"
#include "AssemblyU2DCSharp_Factory931158954.h"
#include "AssemblyU2DCSharp_Drone860454962.h"
#include "AssemblyU2DCSharp_Skytree850646749.h"
#include "AssemblyU2DCSharp_EHuman715499684.h"
#include "AssemblyU2DCSharp_EGRobot1059295608.h"
#include "AssemblyU2DCSharp_ESRobot2546702684.h"
#include "AssemblyU2DCSharp_EMissile3534618099.h"
#include "AssemblyU2DCSharp_EFactory2050856767.h"
#include "AssemblyU2DCSharp_EDrone2061607325.h"
#include "AssemblyU2DCSharp_ESkytree2245705948.h"
#include "AssemblyU2DCSharp_Futi612262014.h"
#include "AssemblyU2DCSharp_actionButton3234286678.h"
#include "AssemblyU2DCSharp_banmen2510752313.h"
#include "AssemblyU2DCSharp_banmen_KOMAINF1054450899.h"
#include "AssemblyU2DCSharp_baseMethods2705876163.h"
#include "AssemblyU2DCSharp_pushButton4005487804.h"
#include "AssemblyU2DCSharp_soundOnOff408115747.h"
#include "AssemblyU2DCSharp_OpeningButton2416718932.h"
#include "AssemblyU2DCSharp_Singleton1770047133.h"
#include "AssemblyU2DCSharp_StableAspect3385526103.h"
#include "AssemblyU2DCSharp_createCanvas3354593624.h"
#include "AssemblyU2DCSharp_createSingleton313015705.h"
#include "AssemblyU2DCSharp_showData3631251573.h"
#include "AssemblyU2DCSharp_startToggle123871076.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E3783534214.h"
#include "AssemblyU2DUnityScript_ColorPicker_Material1107773901.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (Data_t3569509720), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { sizeof (Distribution_t1524299538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[5] = 
{
	Distribution_t1524299538::get_offset_of_prefab_2(),
	Distribution_t1524299538::get_offset_of_prefabResource_3(),
	Distribution_t1524299538::get_offset_of_prefabEResource_4(),
	Distribution_t1524299538::get_offset_of_prefabBackGround_5(),
	Distribution_t1524299538::get_offset_of_prefabRefresh_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (SaveScript_t1729063510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[3] = 
{
	SaveScript_t1729063510::get_offset_of_str_2(),
	SaveScript_t1729063510::get_offset_of_inputField_3(),
	SaveScript_t1729063510::get_offset_of_text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (arrangeKoma_t3423810832), -1, sizeof(arrangeKoma_t3423810832_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2703[18] = 
{
	arrangeKoma_t3423810832_StaticFields::get_offset_of_startBoard_2(),
	arrangeKoma_t3423810832::get_offset_of_canTouchPlace_3(),
	arrangeKoma_t3423810832::get_offset_of_human_4(),
	arrangeKoma_t3423810832::get_offset_of_resources_5(),
	arrangeKoma_t3423810832::get_offset_of_koma_6(),
	arrangeKoma_t3423810832::get_offset_of_komaNum_7(),
	arrangeKoma_t3423810832::get_offset_of_img_8(),
	arrangeKoma_t3423810832::get_offset_of_text_9(),
	arrangeKoma_t3423810832::get_offset_of_line_10(),
	arrangeKoma_t3423810832::get_offset_of_row_11(),
	arrangeKoma_t3423810832::get_offset_of_changeRed_12(),
	arrangeKoma_t3423810832::get_offset_of_changeGreen_13(),
	arrangeKoma_t3423810832::get_offset_of_changeBlue_14(),
	arrangeKoma_t3423810832::get_offset_of_changeAlpha_15(),
	arrangeKoma_t3423810832_StaticFields::get_offset_of_barrier_16(),
	arrangeKoma_t3423810832_StaticFields::get_offset_of_factoryLocation_17(),
	arrangeKoma_t3423810832::get_offset_of_humanPosition_18(),
	arrangeKoma_t3423810832_StaticFields::get_offset_of_myResources_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (KOMAINF_t446687068)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2704[17] = 
{
	KOMAINF_t446687068::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (selectKoma_t1760479012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[2] = 
{
	selectKoma_t1760479012::get_offset_of_refObj_2(),
	selectKoma_t1760479012::get_offset_of_selectNum_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (selectPlace_t742894733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[3] = 
{
	selectPlace_t742894733::get_offset_of_refObj_2(),
	selectPlace_t742894733::get_offset_of_line_3(),
	selectPlace_t742894733::get_offset_of_row_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (inputName_t1068137479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[6] = 
{
	inputName_t1068137479::get_offset_of_obj_2(),
	inputName_t1068137479::get_offset_of_canvasObject_3(),
	inputName_t1068137479::get_offset_of_prefabResource_4(),
	inputName_t1068137479::get_offset_of_text_5(),
	inputName_t1068137479::get_offset_of_str_6(),
	inputName_t1068137479::get_offset_of_inputField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (Koma_t3011766596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[6] = 
{
	Koma_t3011766596::get_offset_of_ban_0(),
	Koma_t3011766596::get_offset_of_komaView_1(),
	Koma_t3011766596::get_offset_of_komaNumber_2(),
	Koma_t3011766596::get_offset_of_resources_3(),
	Koma_t3011766596::get_offset_of_brokeKomaView_4(),
	Koma_t3011766596::get_offset_of_komaMaxTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (Empty_t4012560223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (Human_t1156088493), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (GRobot_t456225233), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (SRobot_t456246757), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (Missile_t813944928), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (Factory_t931158954), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (Drone_t860454962), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (Skytree_t850646749), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (EHuman_t715499684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (EGRobot_t1059295608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (ESRobot_t2546702684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (EMissile_t3534618099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (EFactory_t2050856767), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (EDrone_t2061607325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (ESkytree_t2245705948), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (Futi_t612262014), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (actionButton_t3234286678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[2] = 
{
	actionButton_t3234286678::get_offset_of_refObj_2(),
	actionButton_t3234286678::get_offset_of_actionNum_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (banmen_t2510752313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2726[55] = 
{
	banmen_t2510752313::get_offset_of_baseM_2(),
	banmen_t2510752313::get_offset_of_Board_3(),
	banmen_t2510752313::get_offset_of_canTouchPlace_4(),
	banmen_t2510752313::get_offset_of_broken_5(),
	banmen_t2510752313::get_offset_of_barrier_6(),
	banmen_t2510752313::get_offset_of_factoryLocation_7(),
	banmen_t2510752313::get_offset_of_humanPosition_8(),
	banmen_t2510752313::get_offset_of_komaTime_9(),
	banmen_t2510752313::get_offset_of_komaSlider_10(),
	banmen_t2510752313::get_offset_of_EkomaTime_11(),
	banmen_t2510752313::get_offset_of_EkomaSlider_12(),
	banmen_t2510752313::get_offset_of_arrangeMode_13(),
	banmen_t2510752313::get_offset_of_arrange1_14(),
	banmen_t2510752313::get_offset_of_arrange2_15(),
	banmen_t2510752313::get_offset_of_humanExist_16(),
	banmen_t2510752313::get_offset_of_winLose_17(),
	banmen_t2510752313::get_offset_of_img_18(),
	banmen_t2510752313::get_offset_of_text_19(),
	banmen_t2510752313::get_offset_of_line_20(),
	banmen_t2510752313::get_offset_of_row_21(),
	banmen_t2510752313::get_offset_of_halfOfKoma_22(),
	banmen_t2510752313::get_offset_of_bl_23(),
	banmen_t2510752313::get_offset_of_br_24(),
	banmen_t2510752313::get_offset_of_al_25(),
	banmen_t2510752313::get_offset_of_ar_26(),
	banmen_t2510752313::get_offset_of_changeRed_27(),
	banmen_t2510752313::get_offset_of_changeGreen_28(),
	banmen_t2510752313::get_offset_of_changeBlue_29(),
	banmen_t2510752313::get_offset_of_changeAlpha_30(),
	banmen_t2510752313::get_offset_of_enemy_31(),
	banmen_t2510752313::get_offset_of_wall_32(),
	banmen_t2510752313::get_offset_of_satelite_33(),
	banmen_t2510752313::get_offset_of_Esatelite_34(),
	banmen_t2510752313::get_offset_of_kariKoma_35(),
	banmen_t2510752313::get_offset_of_kariKomaNum_36(),
	banmen_t2510752313::get_offset_of_myFactoryTime_37(),
	banmen_t2510752313::get_offset_of_enemyFactoryTime_38(),
	banmen_t2510752313::get_offset_of_myResources_39(),
	banmen_t2510752313::get_offset_of_enemyResources_40(),
	banmen_t2510752313::get_offset_of_empty_41(),
	banmen_t2510752313::get_offset_of_human_42(),
	banmen_t2510752313::get_offset_of_gRobot_43(),
	banmen_t2510752313::get_offset_of_sRobot_44(),
	banmen_t2510752313::get_offset_of_missile_45(),
	banmen_t2510752313::get_offset_of_factory_46(),
	banmen_t2510752313::get_offset_of_drone_47(),
	banmen_t2510752313::get_offset_of_skytree_48(),
	banmen_t2510752313::get_offset_of_Ehuman_49(),
	banmen_t2510752313::get_offset_of_EgRobot_50(),
	banmen_t2510752313::get_offset_of_EsRobot_51(),
	banmen_t2510752313::get_offset_of_Emissile_52(),
	banmen_t2510752313::get_offset_of_Efactory_53(),
	banmen_t2510752313::get_offset_of_Edrone_54(),
	banmen_t2510752313::get_offset_of_Eskytree_55(),
	banmen_t2510752313::get_offset_of_futi_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (KOMAINF_t1054450899)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2727[17] = 
{
	KOMAINF_t1054450899::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (baseMethods_t2705876163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[5] = 
{
	baseMethods_t2705876163::get_offset_of_ban_0(),
	baseMethods_t2705876163::get_offset_of_refObj_1(),
	baseMethods_t2705876163::get_offset_of_singleton_2(),
	baseMethods_t2705876163::get_offset_of_prefab_3(),
	baseMethods_t2705876163::get_offset_of_winLoseObj_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (pushButton_t4005487804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[3] = 
{
	pushButton_t4005487804::get_offset_of_refObj_2(),
	pushButton_t4005487804::get_offset_of_line_3(),
	pushButton_t4005487804::get_offset_of_row_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (soundOnOff_t408115747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[2] = 
{
	soundOnOff_t408115747::get_offset_of_obj_2(),
	soundOnOff_t408115747::get_offset_of_onOff_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (OpeningButton_t2416718932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[1] = 
{
	OpeningButton_t2416718932::get_offset_of_stageNum_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (Singleton_t1770047133), -1, sizeof(Singleton_t1770047133_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2732[1] = 
{
	Singleton_t1770047133_StaticFields::get_offset_of_existsInstance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (StableAspect_t3385526103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[4] = 
{
	StableAspect_t3385526103::get_offset_of_cam_2(),
	StableAspect_t3385526103::get_offset_of_width_3(),
	StableAspect_t3385526103::get_offset_of_height_4(),
	StableAspect_t3385526103::get_offset_of_pixelPerUnit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (createCanvas_t3354593624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[2] = 
{
	createCanvas_t3354593624::get_offset_of_canvasNum_2(),
	createCanvas_t3354593624::get_offset_of_prefab_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (createSingleton_t313015705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (showData_t3631251573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[4] = 
{
	showData_t3631251573::get_offset_of_text_2(),
	showData_t3631251573::get_offset_of_refObj_3(),
	showData_t3631251573::get_offset_of_singleton_4(),
	showData_t3631251573::get_offset_of_average_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (startToggle_t123871076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[1] = 
{
	startToggle_t123871076::get_offset_of_toggle_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (U3CModuleU3E_t3783534228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (ColorPicker_Material_t1107773901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[6] = 
{
	ColorPicker_Material_t1107773901::get_offset_of_targetSharedMaterial_2(),
	ColorPicker_Material_t1107773901::get_offset_of_colorPickerTexture_3(),
	ColorPicker_Material_t1107773901::get_offset_of_guiTextureX_4(),
	ColorPicker_Material_t1107773901::get_offset_of_guiTextureY_5(),
	ColorPicker_Material_t1107773901::get_offset_of_guiTextureWidth_6(),
	ColorPicker_Material_t1107773901::get_offset_of_guiTextureHeight_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
