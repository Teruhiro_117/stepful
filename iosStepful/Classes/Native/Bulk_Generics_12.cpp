﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.String
struct String_t;
// System.Predicate`1<System.Boolean>
struct Predicate_1_t2268544833;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Predicate_1_t2776792056;
// System.Predicate`1<System.Int32>
struct Predicate_1_t514847563;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t2832094954;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t4236135325;
// System.Predicate`1<System.Single>
struct Predicate_1_t519480047;
// System.Predicate`1<System.UInt32>
struct Predicate_1_t592652136;
// System.Predicate`1<UnityEngine.Color32>
struct Predicate_1_t3612454929;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2759123787;
// System.Predicate`1<UnityEngine.Experimental.Director.Playable>
struct Predicate_1_t2110515663;
// System.Predicate`1<UnityEngine.Networking.ChannelPacket>
struct Predicate_1_t125567000;
// System.Predicate`1<UnityEngine.Networking.ClientScene/PendingOwner>
struct Predicate_1_t3615756330;
// System.Predicate`1<UnityEngine.Networking.LocalClient/InternalMsg>
struct Predicate_1_t3715559133;
// System.Predicate`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct Predicate_1_t4255032428;
// System.Predicate`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>
struct Predicate_1_t496346236;
// System.Predicate`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>
struct Predicate_1_t3262025608;
// System.Predicate`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>
struct Predicate_1_t3134431798;
// System.Predicate`1<UnityEngine.Networking.QosType>
struct Predicate_1_t446862598;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t1499606915;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t2064247989;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t3942196229;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t686677694;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t686677695;
// System.Predicate`1<UnityEngine.Vector4>
struct Predicate_1_t686677696;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t4179406139;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t1095697167;
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t2619124609;
// UnityEngine.Object
struct Object_t1021602117;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t865427339;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t1482999186;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t870059823;
// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t2019901575;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t897193173;
// UnityEngine.Events.InvokableCall`1<System.Int32>
struct InvokableCall_1_t266204305;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t3438463199;
// UnityEngine.Events.InvokableCall`1<System.Object>
struct InvokableCall_1_t883776152;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t4056035046;
// UnityEngine.Events.InvokableCall`1<System.Single>
struct InvokableCall_1_t270836789;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t3443095683;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
struct InvokableCall_1_t214718932;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t3386977826;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
struct InvokableCall_1_t438034436;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t3610293330;
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct InvokableCall_2_t3799696166;
// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
struct InvokableCall_3_t2191335654;
// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
struct InvokableCall_4_t2955480072;
// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct UnityAction_1_t3051495417;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t3784905282;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t1903595547;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t606618774;
// UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>
struct UnityAction_3_t3482433968;
// UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>
struct UnityAction_4_t1666603240;
// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct UnityEvent_1_t3863924733;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t2110227463;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t2727799310;
// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t2114859947;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct UnityEvent_1_t2058742090;
// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct UnityEvent_1_t2282057594;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t1372135904;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct UnityEvent_3_t3149477088;
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct UnityEvent_4_t2935245934;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t1186599945;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2681005625;
// UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>
struct U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007;
// UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<System.Object>
struct DataResponseDelegate_1_t945754175;
// UnityEngine.Networking.Match.NetworkMatch/InternalResponseDelegate`2<System.Object,System.Object>
struct InternalResponseDelegate_2_t3073640404;
// UnityEngine.Networking.SyncList`1/SyncListChanged<System.Boolean>
struct SyncListChanged_t2191845168;
// UnityEngine.Networking.SyncList`1/SyncListChanged<System.Int32>
struct SyncListChanged_t438147898;
// UnityEngine.Networking.SyncList`1/SyncListChanged<System.Object>
struct SyncListChanged_t1055719745;
// UnityEngine.Networking.SyncList`1/SyncListChanged<System.Single>
struct SyncListChanged_t442780382;
// UnityEngine.Networking.SyncList`1/SyncListChanged<System.UInt32>
struct SyncListChanged_t515952471;
// UnityEngine.Networking.SyncList`1<System.Boolean>
struct SyncList_1_t486689021;
// UnityEngine.Networking.NetworkBehaviour
struct NetworkBehaviour_t3873055601;
// UnityEngine.Networking.NetworkIdentity
struct NetworkIdentity_t1766639790;
// UnityEngine.Networking.NetworkReader
struct NetworkReader_t3187690923;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_t1301098545;
// UnityEngine.Networking.SyncList`1<System.Int32>
struct SyncList_1_t3027959047;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3842368571;
// UnityEngine.Networking.SyncList`1<System.Object>
struct SyncList_1_t3645530894;
// UnityEngine.Networking.SyncList`1<System.Single>
struct SyncList_1_t3032591531;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t3847001055;
// UnityEngine.Networking.SyncList`1<System.UInt32>
struct SyncList_1_t3105763620;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t3920173144;
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t549597370;
// System.Comparison`1<System.Object>
struct Comparison_1_t3951188146;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t2989619467;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct U3CStartU3Ec__Iterator0_t2537691210;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t2725162992;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t243638650;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t573379950;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI4121738870.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI4121738870MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "System_Core_System_Func_2_gen3961629604.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_UInt322149682021.h"
#include "System_Core_System_Func_2_gen3961629604MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Nullable_1_gen1693325264.h"
#include "mscorlib_System_Nullable_1_gen1693325264MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_TimeSpan3430258949MethodDeclarations.h"
#include "mscorlib_System_ValueType3507792607MethodDeclarations.h"
#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2268544833.h"
#include "mscorlib_System_Predicate_1_gen2268544833MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "mscorlib_System_Predicate_1_gen2776792056.h"
#include "mscorlib_System_Predicate_1_gen2776792056MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Predicate_1_gen514847563.h"
#include "mscorlib_System_Predicate_1_gen514847563MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen1132419410.h"
#include "mscorlib_System_Predicate_1_gen1132419410MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen2832094954.h"
#include "mscorlib_System_Predicate_1_gen2832094954MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Predicate_1_gen4236135325.h"
#include "mscorlib_System_Predicate_1_gen4236135325MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Predicate_1_gen519480047.h"
#include "mscorlib_System_Predicate_1_gen519480047MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Predicate_1_gen592652136.h"
#include "mscorlib_System_Predicate_1_gen592652136MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3612454929.h"
#include "mscorlib_System_Predicate_1_gen3612454929MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Predicate_1_gen2759123787.h"
#include "mscorlib_System_Predicate_1_gen2759123787MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "mscorlib_System_Predicate_1_gen2110515663.h"
#include "mscorlib_System_Predicate_1_gen2110515663MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "mscorlib_System_Predicate_1_gen125567000.h"
#include "mscorlib_System_Predicate_1_gen125567000MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Chan1682596885.h"
#include "mscorlib_System_Predicate_1_gen3615756330.h"
#include "mscorlib_System_Predicate_1_gen3615756330MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Clien877818919.h"
#include "mscorlib_System_Predicate_1_gen3715559133.h"
#include "mscorlib_System_Predicate_1_gen3715559133MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Local977621722.h"
#include "mscorlib_System_Predicate_1_gen4255032428.h"
#include "mscorlib_System_Predicate_1_gen4255032428MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1517095017.h"
#include "mscorlib_System_Predicate_1_gen496346236.h"
#include "mscorlib_System_Predicate_1_gen496346236MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw2053376121.h"
#include "mscorlib_System_Predicate_1_gen3262025608.h"
#include "mscorlib_System_Predicate_1_gen3262025608MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo524088197.h"
#include "mscorlib_System_Predicate_1_gen3134431798.h"
#include "mscorlib_System_Predicate_1_gen3134431798MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo396494387.h"
#include "mscorlib_System_Predicate_1_gen446862598.h"
#include "mscorlib_System_Predicate_1_gen446862598MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_QosType2003892483.h"
#include "mscorlib_System_Predicate_1_gen1499606915.h"
#include "mscorlib_System_Predicate_1_gen1499606915MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Predicate_1_gen2064247989.h"
#include "mscorlib_System_Predicate_1_gen2064247989MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Predicate_1_gen3942196229.h"
#include "mscorlib_System_Predicate_1_gen3942196229MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Predicate_1_gen686677694.h"
#include "mscorlib_System_Predicate_1_gen686677694MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Predicate_1_gen686677695.h"
#include "mscorlib_System_Predicate_1_gen686677695MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Predicate_1_gen686677696.h"
#include "mscorlib_System_Predicate_1_gen686677696MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g4179406139.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g4179406139MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGett1095697167.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGett1095697167MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen3207297272.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen3207297272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall2619124609.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall2619124609MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2019901575MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen2019901575.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_865427339.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_865427339MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen266204305MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen266204305.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall1482999186.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall1482999186MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen883776152MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen883776152.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_870059823.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_870059823MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen270836789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen270836789.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3438463199MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4056035046.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4056035046MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3443095683.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3443095683MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen214718932.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen214718932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3386977826.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3386977826MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen438034436.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen438034436MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3610293330.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3610293330MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3799696166.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen3799696166MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3784905282.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen3784905282MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen2191335654.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen2191335654MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen3482433968.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_3_gen3482433968MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen2955480072.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen2955480072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_4_gen1666603240.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_4_gen1666603240MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3051495417.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3051495417MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1903595547.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1903595547MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen606618774.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen606618774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3863924733.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3863924733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2110227463.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2110227463MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2727799310.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2727799310MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2114859947.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2114859947MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2058742090.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2058742090MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2282057594.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2282057594MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen1372135904.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen1372135904MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen3149477088.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen3149477088MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen2935245934.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen2935245934MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1186599945.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1186599945MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMat106232007.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMat106232007MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "UnityEngine_SimpleJson_SimpleJson3569903358MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityString276356480MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_Response999782913MethodDeclarations.h"
#include "mscorlib_System_FormatException2948921286.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "mscorlib_System_Activator1850728717MethodDeclarations.h"
#include "mscorlib_System_Activator1850728717.h"
#include "UnityEngine_UnityEngine_Networking_Match_ResponseB1952642056.h"
#include "UnityEngine_UnityEngine_Networking_Match_ResponseB1952642056MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_Response999782913.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMa3073640404.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMa3073640404MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMat945754175.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMat945754175MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync3559304062.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync3559304062MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync1805606792.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync1805606792MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync2423178639.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync2423178639MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync1810239276.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync1810239276MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync1883411365.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync1883411365MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync2191845168.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync2191845168MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_SyncL438147898.h"
#include "UnityEngine_Networking_UnityEngine_Networking_SyncL438147898MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync1055719745.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync1055719745MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_SyncL442780382.h"
#include "UnityEngine_Networking_UnityEngine_Networking_SyncL442780382MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_SyncL515952471.h"
#include "UnityEngine_Networking_UnityEngine_Networking_SyncL515952471MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_SyncL486689021.h"
#include "UnityEngine_Networking_UnityEngine_Networking_SyncL486689021MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3194695850.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3194695850MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3873055601.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_LogF2612395354MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1766639790MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo560143343MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3779449791MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3873055601MethodDeclarations.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1766639790.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netwo560143343.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Int164041245914.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Networ33998832.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3187690923.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw3187690923MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2729425524.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync3027959047.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync3027959047MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync3645530894.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync3645530894MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync3032591531.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync3032591531MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064MethodDeclarations.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat980360738.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync3105763620.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Sync3105763620MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1518803153.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1518803153MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1053532827.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSe549597370.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSe549597370MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1663937576MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "mscorlib_System_Comparison_1_gen3951188146.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween2989619467.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween2989619467MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3438117476.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3438117476MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween2537691210.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween2537691210MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2986189219.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2986189219MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween3177091249.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween3177091249MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween2725162992.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Tween2725162992MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen924852264.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen924852264MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen4282372027.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen4282372027MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen2807584331.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen2807584331MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen1542424111.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen1542424111MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen604976578.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen604976578MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3425156178.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3425156178MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen4022459630.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen4022459630MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen243638650.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen3085012097.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen3085012097MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1610224401.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1610224401MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen243638650MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen57233634.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen57233634MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen573379950.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen3414753397.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen3414753397MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1939965701.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen1939965701MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen573379950MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen1096682395.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen1096682395MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen159234862.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen159234862MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen2979414462.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen2979414462MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen1096682396.h"
#include "UnityEngine_UI_UnityEngine_UI_ListPool_1_gen1096682396MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen159234863.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen159234863MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen2979414463.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen2979414463MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712MethodDeclarations.h"

// !!0 System.Threading.Interlocked::CompareExchange<System.Object>(!!0&,!!0,!!0)
extern "C"  Il2CppObject * Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject ** p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method);
#define Interlocked_CompareExchange_TisIl2CppObject_m2145889806(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject **, Il2CppObject *, Il2CppObject *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<System.Boolean>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t897193173_m226081743(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t897193173 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t897193173 **, UnityAction_1_t897193173 *, UnityAction_1_t897193173 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Boolean>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t3825574718_m3557881725_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t3825574718_m3557881725(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisBoolean_t3825574718_m3557881725_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<System.Int32>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t3438463199_m4157363705(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t3438463199 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3438463199 **, UnityAction_1_t3438463199 *, UnityAction_1_t3438463199 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Int32>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t2071877448_m4010682571_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t2071877448_m4010682571(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisInt32_t2071877448_m4010682571_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<System.Object>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t4056035046_m3323700736(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t4056035046 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t4056035046 **, UnityAction_1_t4056035046 *, UnityAction_1_t4056035046 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1349548392_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1349548392(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisIl2CppObject_m1349548392_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<System.Single>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t3443095683_m3615079253(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t3443095683 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3443095683 **, UnityAction_1_t3443095683 *, UnityAction_1_t3443095683 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Single>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t2076509932_m3470174535_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t2076509932_m3470174535(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisSingle_t2076509932_m3470174535_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<UnityEngine.Color>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t3386977826_m3959013804(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t3386977826 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3386977826 **, UnityAction_1_t3386977826 *, UnityAction_1_t3386977826 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Color>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisColor_t2020392075_m85849056_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisColor_t2020392075_m85849056(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisColor_t2020392075_m85849056_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Threading.Interlocked::CompareExchange<UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>>(!!0&,!!0,!!0)
#define Interlocked_CompareExchange_TisUnityAction_1_t3610293330_m1518298976(__this /* static, unused */, p0, p1, p2, method) ((  UnityAction_1_t3610293330 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3610293330 **, UnityAction_1_t3610293330 *, UnityAction_1_t3610293330 *, const MethodInfo*))Interlocked_CompareExchange_TisIl2CppObject_m2145889806_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Vector2>(System.Object)
extern "C"  void BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t2243707579_m3249535332_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, const MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t2243707579_m3249535332(__this /* static, unused */, ___arg0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))BaseInvokableCall_ThrowOnInvalidArg_TisVector2_t2243707579_m3249535332_gshared)(__this /* static, unused */, ___arg0, method)
// !!0 System.Activator::CreateInstance<System.Object>()
extern "C"  Il2CppObject * Activator_CreateInstance_TisIl2CppObject_m1935044620_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisIl2CppObject_m1935044620(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisIl2CppObject_m1935044620_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m4109961936(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Networking.NetworkIdentity>()
#define Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169(__this, method) ((  NetworkIdentity_t1766639790 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m1958283157_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3602665650_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m269113779_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3279674866_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2682676065_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_5 = V_0;
		Func_2_t3961629604 * L_6 = (Func_2_t3961629604 *)__this->get_U3CU24U3Epredicate_7();
		NullCheck(L_5);
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_MetadataUsageId;
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		NullCheck((Il2CppObject*)L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_110U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0089;
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject*)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3961629604 * L_7 = (Func_2_t3961629604 *)__this->get_predicate_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			NullCheck((Func_2_t3961629604 *)L_7);
			bool L_9 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3961629604 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003b;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_110U3E__0_1();
			NullCheck((Il2CppObject *)L_3);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_MetadataUsageId;
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m796575255_gshared (Nullable_1_t1693325264 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		TimeSpan_t3430258949  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m796575255_AdjustorThunk (Il2CppObject * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m796575255(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3663286555_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3663286555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3663286555(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m1743067844_MetadataUsageId;
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1743067844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		TimeSpan_t3430258949  L_2 = (TimeSpan_t3430258949 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t3430258949  _returnValue = Nullable_1_get_Value_m1743067844(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3860982732_gshared (Nullable_1_t1693325264 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1693325264 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = Nullable_1_Equals_m1889119397((Nullable_1_t1693325264 *)__this, (Nullable_1_t1693325264 )((*(Nullable_1_t1693325264 *)((Nullable_1_t1693325264 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3860982732_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3860982732(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1889119397_gshared (Nullable_1_t1693325264 * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		TimeSpan_t3430258949 * L_3 = (TimeSpan_t3430258949 *)(&___other0)->get_address_of_value_0();
		TimeSpan_t3430258949  L_4 = (TimeSpan_t3430258949 )__this->get_value_0();
		TimeSpan_t3430258949  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m4102942751((TimeSpan_t3430258949 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1889119397_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1889119397(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		int32_t L_2 = TimeSpan_GetHashCode_m550404245((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1791015856(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m1238126148_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m1238126148_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1238126148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		String_t* L_2 = TimeSpan_ToString_m2947282901((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1238126148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1238126148(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Predicate`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3379886338_gshared (Predicate_1_t2268544833 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Boolean>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3492771294_gshared (Predicate_1_t2268544833 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3492771294((Predicate_1_t2268544833 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1973834093_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1973834093_gshared (Predicate_1_t2268544833 * __this, bool ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1973834093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m43128384_gshared (Predicate_1_t2268544833 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2359416570_gshared (Predicate_1_t2776792056 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m332691618_gshared (Predicate_1_t2776792056 * __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m332691618((Predicate_1_t2776792056 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, KeyValuePair_2_t38854645  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* KeyValuePair_2_t38854645_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3157529563_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3157529563_gshared (Predicate_1_t2776792056 * __this, KeyValuePair_2_t38854645  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3157529563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(KeyValuePair_2_t38854645_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m548603360_gshared (Predicate_1_t2776792056 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2826800414_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m695569038_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m695569038((Predicate_1_t514847563 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2559992383_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2559992383_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2559992383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1202813828_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2289454599_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4047721271_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4047721271((Predicate_1_t1132419410 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3556950370_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3656575065_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1767993638_gshared (Predicate_1_t2832094954 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m527131606_gshared (Predicate_1_t2832094954 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m527131606((Predicate_1_t2832094954 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1448216027_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1448216027_gshared (Predicate_1_t2832094954 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1448216027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m215026240_gshared (Predicate_1_t2832094954 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1292402863_gshared (Predicate_1_t4236135325 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2060780095_gshared (Predicate_1_t4236135325 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2060780095((Predicate_1_t4236135325 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1856151290_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1856151290_gshared (Predicate_1_t4236135325 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1856151290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m259774785_gshared (Predicate_1_t4236135325 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3886687440_gshared (Predicate_1_t519480047 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Single>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2581621416_gshared (Predicate_1_t519480047 * __this, float ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2581621416((Predicate_1_t519480047 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, float ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, float ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3791856215_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3791856215_gshared (Predicate_1_t519480047 * __this, float ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3791856215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3081660750_gshared (Predicate_1_t519480047 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3732669481_gshared (Predicate_1_t592652136 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.UInt32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m539473317_gshared (Predicate_1_t592652136 * __this, uint32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m539473317((Predicate_1_t592652136 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, uint32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.UInt32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m739040720_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m739040720_gshared (Predicate_1_t592652136 * __this, uint32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m739040720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3585710307_gshared (Predicate_1_t592652136 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3811123782_gshared (Predicate_1_t3612454929 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m122788314_gshared (Predicate_1_t3612454929 * __this, Color32_t874517518  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m122788314((Predicate_1_t3612454929 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Color32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color32_t874517518_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2959352225_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2959352225_gshared (Predicate_1_t3612454929 * __this, Color32_t874517518  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2959352225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color32_t874517518_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m924884444_gshared (Predicate_1_t3612454929 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1567825400_gshared (Predicate_1_t2759123787 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3860206640_gshared (Predicate_1_t2759123787 * __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3860206640((Predicate_1_t2759123787 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* RaycastResult_t21186376_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4068629879_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4068629879_gshared (Predicate_1_t2759123787 * __this, RaycastResult_t21186376  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4068629879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RaycastResult_t21186376_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m973058386_gshared (Predicate_1_t2759123787 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Experimental.Director.Playable>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3105937642_gshared (Predicate_1_t2110515663 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Experimental.Director.Playable>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1617267354_gshared (Predicate_1_t2110515663 * __this, Playable_t3667545548  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1617267354((Predicate_1_t2110515663 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Playable_t3667545548  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Playable_t3667545548  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Experimental.Director.Playable>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Playable_t3667545548_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3423161611_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3423161611_gshared (Predicate_1_t2110515663 * __this, Playable_t3667545548  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3423161611_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Playable_t3667545548_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Experimental.Director.Playable>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2453294608_gshared (Predicate_1_t2110515663 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Networking.ChannelPacket>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m258867289_gshared (Predicate_1_t125567000 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.ChannelPacket>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2955039645_gshared (Predicate_1_t125567000 * __this, ChannelPacket_t1682596885  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2955039645((Predicate_1_t125567000 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, ChannelPacket_t1682596885  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, ChannelPacket_t1682596885  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Networking.ChannelPacket>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* ChannelPacket_t1682596885_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1159308984_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1159308984_gshared (Predicate_1_t125567000 * __this, ChannelPacket_t1682596885  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1159308984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(ChannelPacket_t1682596885_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.ChannelPacket>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3320739175_gshared (Predicate_1_t125567000 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Networking.ClientScene/PendingOwner>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2897080396_gshared (Predicate_1_t3615756330 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.ClientScene/PendingOwner>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1784621660_gshared (Predicate_1_t3615756330 * __this, PendingOwner_t877818919  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1784621660((Predicate_1_t3615756330 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, PendingOwner_t877818919  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, PendingOwner_t877818919  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Networking.ClientScene/PendingOwner>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PendingOwner_t877818919_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m215257159_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m215257159_gshared (Predicate_1_t3615756330 * __this, PendingOwner_t877818919  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m215257159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PendingOwner_t877818919_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.ClientScene/PendingOwner>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3198754270_gshared (Predicate_1_t3615756330 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Networking.LocalClient/InternalMsg>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1095411391_gshared (Predicate_1_t3715559133 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.LocalClient/InternalMsg>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1138509055_gshared (Predicate_1_t3715559133 * __this, InternalMsg_t977621722  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1138509055((Predicate_1_t3715559133 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, InternalMsg_t977621722  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, InternalMsg_t977621722  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Networking.LocalClient/InternalMsg>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* InternalMsg_t977621722_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4213413790_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4213413790_gshared (Predicate_1_t3715559133 * __this, InternalMsg_t977621722  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4213413790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(InternalMsg_t977621722_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.LocalClient/InternalMsg>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3997902805_gshared (Predicate_1_t3715559133 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m4138988622_gshared (Predicate_1_t4255032428 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3393660506_gshared (Predicate_1_t4255032428 * __this, PendingPlayer_t1517095017  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3393660506((Predicate_1_t4255032428 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, PendingPlayer_t1517095017  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, PendingPlayer_t1517095017  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PendingPlayer_t1517095017_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m101869037_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m101869037_gshared (Predicate_1_t4255032428 * __this, PendingPlayer_t1517095017  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m101869037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PendingPlayer_t1517095017_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m685968816_gshared (Predicate_1_t4255032428 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2299132662_gshared (Predicate_1_t496346236 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1253789082_gshared (Predicate_1_t496346236 * __this, PendingPlayerInfo_t2053376121  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1253789082((Predicate_1_t496346236 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, PendingPlayerInfo_t2053376121  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, PendingPlayerInfo_t2053376121  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PendingPlayerInfo_t2053376121_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1221277109_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1221277109_gshared (Predicate_1_t496346236 * __this, PendingPlayerInfo_t2053376121  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1221277109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PendingPlayerInfo_t2053376121_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.NetworkMigrationManager/PendingPlayerInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2335506840_gshared (Predicate_1_t496346236 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3446176832_gshared (Predicate_1_t3262025608 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m879360452_gshared (Predicate_1_t3262025608 * __this, CRCMessageEntry_t524088197  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m879360452((Predicate_1_t3262025608 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CRCMessageEntry_t524088197  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CRCMessageEntry_t524088197  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* CRCMessageEntry_t524088197_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2990581205_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2990581205_gshared (Predicate_1_t3262025608 * __this, CRCMessageEntry_t524088197  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2990581205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CRCMessageEntry_t524088197_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.NetworkSystem.CRCMessageEntry>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m675108450_gshared (Predicate_1_t3262025608 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1473976206_gshared (Predicate_1_t3134431798 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1926252046_gshared (Predicate_1_t3134431798 * __this, PeerInfoPlayer_t396494387  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1926252046((Predicate_1_t3134431798 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, PeerInfoPlayer_t396494387  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, PeerInfoPlayer_t396494387  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* PeerInfoPlayer_t396494387_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2513518087_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2513518087_gshared (Predicate_1_t3134431798 * __this, PeerInfoPlayer_t396494387  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2513518087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(PeerInfoPlayer_t396494387_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.NetworkSystem.PeerInfoPlayer>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m719382320_gshared (Predicate_1_t3134431798 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Networking.QosType>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3234515295_gshared (Predicate_1_t446862598 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.QosType>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1748618967_gshared (Predicate_1_t446862598 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1748618967((Predicate_1_t446862598 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Networking.QosType>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* QosType_t2003892483_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m134486622_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m134486622_gshared (Predicate_1_t446862598 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m134486622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(QosType_t2003892483_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Networking.QosType>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m691498773_gshared (Predicate_1_t446862598 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1020292372_gshared (Predicate_1_t1499606915 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3539717340_gshared (Predicate_1_t1499606915 * __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3539717340((Predicate_1_t1499606915 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UICharInfo_t3056636800_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3056726495_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3056726495_gshared (Predicate_1_t1499606915 * __this, UICharInfo_t3056636800  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3056726495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UICharInfo_t3056636800_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2354180346_gshared (Predicate_1_t1499606915 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m784266182_gshared (Predicate_1_t2064247989 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m577088274_gshared (Predicate_1_t2064247989 * __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m577088274((Predicate_1_t2064247989 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UILineInfo_t3621277874_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2329589669_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2329589669_gshared (Predicate_1_t2064247989 * __this, UILineInfo_t3621277874  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2329589669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UILineInfo_t3621277874_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3442731496_gshared (Predicate_1_t2064247989 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m549279630_gshared (Predicate_1_t3942196229 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2883675618_gshared (Predicate_1_t3942196229 * __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2883675618((Predicate_1_t3942196229 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UIVertex>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UIVertex_t1204258818_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3926587117_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3926587117_gshared (Predicate_1_t3942196229 * __this, UIVertex_t1204258818  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3926587117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIVertex_t1204258818_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m337889472_gshared (Predicate_1_t3942196229 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2863314033_gshared (Predicate_1_t686677694 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3001657933_gshared (Predicate_1_t686677694 * __this, Vector2_t2243707579  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3001657933((Predicate_1_t686677694 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m866207434_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m866207434_gshared (Predicate_1_t686677694 * __this, Vector2_t2243707579  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m866207434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3406729927_gshared (Predicate_1_t686677694 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3243601712_gshared (Predicate_1_t686677695 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2775223656_gshared (Predicate_1_t686677695 * __this, Vector3_t2243707580  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2775223656((Predicate_1_t686677695 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1764756107_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1764756107_gshared (Predicate_1_t686677695 * __this, Vector3_t2243707580  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1764756107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1035116514_gshared (Predicate_1_t686677695 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2995226103_gshared (Predicate_1_t686677696 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2407726575_gshared (Predicate_1_t686677696 * __this, Vector4_t2243707581  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2407726575((Predicate_1_t686677696 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2425667920_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2425667920_gshared (Predicate_1_t686677696 * __this, Vector4_t2243707581  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2425667920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2420144145_gshared (Predicate_1_t686677696 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Getter_2__ctor_m653998582_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Getter_2_Invoke_m3338489829_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ____this0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Getter_2_Invoke_m3338489829((Getter_2_t4179406139 *)__this->get_prev_9(),____this0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Getter_2_BeginInvoke_m2080015031_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ____this0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Getter_2_EndInvoke_m977999903_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void StaticGetter_1__ctor_m1290492285_gshared (StaticGetter_1_t1095697167 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * StaticGetter_1_Invoke_m1348877692_gshared (StaticGetter_1_t1095697167 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StaticGetter_1_Invoke_m1348877692((StaticGetter_1_t1095697167 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StaticGetter_1_BeginInvoke_m2732579814_gshared (StaticGetter_1_t1095697167 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * StaticGetter_1_EndInvoke_m44757160_gshared (StaticGetter_1_t1095697167 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m2563320212_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m2563320212_gshared (CachedInvokableCall_1_t2619124609 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, bool ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m2563320212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t2019901575 *)__this);
		((  void (*) (InvokableCall_1_t2019901575 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t2019901575 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		bool L_3 = ___argument2;
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m3247299909_gshared (CachedInvokableCall_1_t2619124609 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t2019901575 *)__this);
		((  void (*) (InvokableCall_1_t2019901575 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t2019901575 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m127496184_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m127496184_gshared (CachedInvokableCall_1_t865427339 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, int32_t ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m127496184_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t266204305 *)__this);
		((  void (*) (InvokableCall_1_t266204305 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t266204305 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		int32_t L_3 = ___argument2;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m2815073919_gshared (CachedInvokableCall_1_t865427339 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t266204305 *)__this);
		((  void (*) (InvokableCall_1_t266204305 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t266204305 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m79259589_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m79259589_gshared (CachedInvokableCall_1_t1482999186 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, Il2CppObject * ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m79259589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t883776152 *)__this);
		((  void (*) (InvokableCall_1_t883776152 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t883776152 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		Il2CppObject * L_3 = ___argument2;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m2401236944_gshared (CachedInvokableCall_1_t1482999186 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t883776152 *)__this);
		((  void (*) (InvokableCall_1_t883776152 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t883776152 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t CachedInvokableCall_1__ctor_m3238306320_MetadataUsageId;
extern "C"  void CachedInvokableCall_1__ctor_m3238306320_gshared (CachedInvokableCall_1_t870059823 * __this, Object_t1021602117 * ___target0, MethodInfo_t * ___theFunction1, float ___argument2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CachedInvokableCall_1__ctor_m3238306320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Arg1_1(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		Object_t1021602117 * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((InvokableCall_1_t270836789 *)__this);
		((  void (*) (InvokableCall_1_t270836789 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((InvokableCall_1_t270836789 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		float L_3 = ___argument2;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2), &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::Invoke(System.Object[])
extern "C"  void CachedInvokableCall_1_Invoke_m4097553971_gshared (CachedInvokableCall_1_t870059823 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_Arg1_1();
		NullCheck((InvokableCall_1_t270836789 *)__this);
		((  void (*) (InvokableCall_1_t270836789 *, ObjectU5BU5D_t3614634134*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((InvokableCall_1_t270836789 *)__this, (ObjectU5BU5D_t3614634134*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m874046876_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m874046876_gshared (InvokableCall_1_t2019901575 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m874046876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t2019901575 *)__this);
		((  void (*) (InvokableCall_1_t2019901575 *, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t2019901575 *)__this, (UnityAction_1_t897193173 *)((UnityAction_1_t897193173 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m2693793190_gshared (InvokableCall_1_t2019901575 * __this, UnityAction_1_t897193173 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t897193173 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t2019901575 *)__this);
		((  void (*) (InvokableCall_1_t2019901575 *, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t2019901575 *)__this, (UnityAction_1_t897193173 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m3048312905_gshared (InvokableCall_1_t2019901575 * __this, UnityAction_1_t897193173 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t897193173 * V_0 = NULL;
	UnityAction_1_t897193173 * V_1 = NULL;
	{
		UnityAction_1_t897193173 * L_0 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t897193173 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t897193173 * L_1 = V_0;
		V_1 = (UnityAction_1_t897193173 *)L_1;
		UnityAction_1_t897193173 ** L_2 = (UnityAction_1_t897193173 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t897193173 * L_3 = V_1;
		UnityAction_1_t897193173 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t897193173 * L_6 = V_0;
		UnityAction_1_t897193173 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t897193173 *>((UnityAction_1_t897193173 **)L_2, (UnityAction_1_t897193173 *)((UnityAction_1_t897193173 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t897193173 *)L_6);
		V_0 = (UnityAction_1_t897193173 *)L_7;
		UnityAction_1_t897193173 * L_8 = V_0;
		UnityAction_1_t897193173 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t897193173 *)L_8) == ((Il2CppObject*)(UnityAction_1_t897193173 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1481038152_gshared (InvokableCall_1_t2019901575 * __this, UnityAction_1_t897193173 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t897193173 * V_0 = NULL;
	UnityAction_1_t897193173 * V_1 = NULL;
	{
		UnityAction_1_t897193173 * L_0 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t897193173 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t897193173 * L_1 = V_0;
		V_1 = (UnityAction_1_t897193173 *)L_1;
		UnityAction_1_t897193173 ** L_2 = (UnityAction_1_t897193173 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t897193173 * L_3 = V_1;
		UnityAction_1_t897193173 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t897193173 * L_6 = V_0;
		UnityAction_1_t897193173 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t897193173 *>((UnityAction_1_t897193173 **)L_2, (UnityAction_1_t897193173 *)((UnityAction_1_t897193173 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t897193173 *)L_6);
		V_0 = (UnityAction_1_t897193173 *)L_7;
		UnityAction_1_t897193173 * L_8 = V_0;
		UnityAction_1_t897193173 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t897193173 *)L_8) == ((Il2CppObject*)(UnityAction_1_t897193173 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m769918017_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m769918017_gshared (InvokableCall_1_t2019901575 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m769918017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t897193173 * L_5 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t897193173 * L_7 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t897193173 *)L_7);
		((  void (*) (UnityAction_1_t897193173 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t897193173 *)L_7, (bool)((*(bool*)((bool*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m951110817_gshared (InvokableCall_1_t2019901575 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t897193173 * L_0 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t897193173 * L_3 = (UnityAction_1_t897193173 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m231935020_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m231935020_gshared (InvokableCall_1_t266204305 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m231935020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t266204305 *)__this);
		((  void (*) (InvokableCall_1_t266204305 *, UnityAction_1_t3438463199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t266204305 *)__this, (UnityAction_1_t3438463199 *)((UnityAction_1_t3438463199 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m563785030_gshared (InvokableCall_1_t266204305 * __this, UnityAction_1_t3438463199 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3438463199 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t266204305 *)__this);
		((  void (*) (InvokableCall_1_t266204305 *, UnityAction_1_t3438463199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t266204305 *)__this, (UnityAction_1_t3438463199 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m3068046591_gshared (InvokableCall_1_t266204305 * __this, UnityAction_1_t3438463199 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3438463199 * V_0 = NULL;
	UnityAction_1_t3438463199 * V_1 = NULL;
	{
		UnityAction_1_t3438463199 * L_0 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3438463199 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3438463199 * L_1 = V_0;
		V_1 = (UnityAction_1_t3438463199 *)L_1;
		UnityAction_1_t3438463199 ** L_2 = (UnityAction_1_t3438463199 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3438463199 * L_3 = V_1;
		UnityAction_1_t3438463199 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3438463199 * L_6 = V_0;
		UnityAction_1_t3438463199 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3438463199 *>((UnityAction_1_t3438463199 **)L_2, (UnityAction_1_t3438463199 *)((UnityAction_1_t3438463199 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3438463199 *)L_6);
		V_0 = (UnityAction_1_t3438463199 *)L_7;
		UnityAction_1_t3438463199 * L_8 = V_0;
		UnityAction_1_t3438463199 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3438463199 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3438463199 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3070410248_gshared (InvokableCall_1_t266204305 * __this, UnityAction_1_t3438463199 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3438463199 * V_0 = NULL;
	UnityAction_1_t3438463199 * V_1 = NULL;
	{
		UnityAction_1_t3438463199 * L_0 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3438463199 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3438463199 * L_1 = V_0;
		V_1 = (UnityAction_1_t3438463199 *)L_1;
		UnityAction_1_t3438463199 ** L_2 = (UnityAction_1_t3438463199 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3438463199 * L_3 = V_1;
		UnityAction_1_t3438463199 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3438463199 * L_6 = V_0;
		UnityAction_1_t3438463199 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3438463199 *>((UnityAction_1_t3438463199 **)L_2, (UnityAction_1_t3438463199 *)((UnityAction_1_t3438463199 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3438463199 *)L_6);
		V_0 = (UnityAction_1_t3438463199 *)L_7;
		UnityAction_1_t3438463199 * L_8 = V_0;
		UnityAction_1_t3438463199 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3438463199 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3438463199 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m428957899_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m428957899_gshared (InvokableCall_1_t266204305 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m428957899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3438463199 * L_5 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3438463199 * L_7 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3438463199 *)L_7);
		((  void (*) (UnityAction_1_t3438463199 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3438463199 *)L_7, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Int32>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m2775216619_gshared (InvokableCall_1_t266204305 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3438463199 * L_0 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3438463199 * L_3 = (UnityAction_1_t3438463199 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m54675381_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m54675381_gshared (InvokableCall_1_t883776152 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m54675381_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t883776152 *)__this);
		((  void (*) (InvokableCall_1_t883776152 *, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t883776152 *)__this, (UnityAction_1_t4056035046 *)((UnityAction_1_t4056035046 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m833213021_gshared (InvokableCall_1_t883776152 * __this, UnityAction_1_t4056035046 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t4056035046 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t883776152 *)__this);
		((  void (*) (InvokableCall_1_t883776152 *, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t883776152 *)__this, (UnityAction_1_t4056035046 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m4009721884_gshared (InvokableCall_1_t883776152 * __this, UnityAction_1_t4056035046 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t4056035046 * V_0 = NULL;
	UnityAction_1_t4056035046 * V_1 = NULL;
	{
		UnityAction_1_t4056035046 * L_0 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t4056035046 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t4056035046 * L_1 = V_0;
		V_1 = (UnityAction_1_t4056035046 *)L_1;
		UnityAction_1_t4056035046 ** L_2 = (UnityAction_1_t4056035046 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t4056035046 * L_3 = V_1;
		UnityAction_1_t4056035046 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t4056035046 * L_6 = V_0;
		UnityAction_1_t4056035046 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t4056035046 *>((UnityAction_1_t4056035046 **)L_2, (UnityAction_1_t4056035046 *)((UnityAction_1_t4056035046 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t4056035046 *)L_6);
		V_0 = (UnityAction_1_t4056035046 *)L_7;
		UnityAction_1_t4056035046 * L_8 = V_0;
		UnityAction_1_t4056035046 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t4056035046 *)L_8) == ((Il2CppObject*)(UnityAction_1_t4056035046 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m527482931_gshared (InvokableCall_1_t883776152 * __this, UnityAction_1_t4056035046 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t4056035046 * V_0 = NULL;
	UnityAction_1_t4056035046 * V_1 = NULL;
	{
		UnityAction_1_t4056035046 * L_0 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t4056035046 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t4056035046 * L_1 = V_0;
		V_1 = (UnityAction_1_t4056035046 *)L_1;
		UnityAction_1_t4056035046 ** L_2 = (UnityAction_1_t4056035046 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t4056035046 * L_3 = V_1;
		UnityAction_1_t4056035046 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t4056035046 * L_6 = V_0;
		UnityAction_1_t4056035046 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t4056035046 *>((UnityAction_1_t4056035046 **)L_2, (UnityAction_1_t4056035046 *)((UnityAction_1_t4056035046 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t4056035046 *)L_6);
		V_0 = (UnityAction_1_t4056035046 *)L_7;
		UnityAction_1_t4056035046 * L_8 = V_0;
		UnityAction_1_t4056035046 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t4056035046 *)L_8) == ((Il2CppObject*)(UnityAction_1_t4056035046 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m1715547918_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m1715547918_gshared (InvokableCall_1_t883776152 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m1715547918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t4056035046 * L_5 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t4056035046 * L_7 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t4056035046 *)L_7);
		((  void (*) (UnityAction_1_t4056035046 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t4056035046 *)L_7, (Il2CppObject *)((Il2CppObject *)Castclass(L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m1325295794_gshared (InvokableCall_1_t883776152 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t4056035046 * L_0 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t4056035046 * L_3 = (UnityAction_1_t4056035046 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m4078762228_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m4078762228_gshared (InvokableCall_1_t270836789 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m4078762228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t270836789 *)__this);
		((  void (*) (InvokableCall_1_t270836789 *, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t270836789 *)__this, (UnityAction_1_t3443095683 *)((UnityAction_1_t3443095683 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m121193486_gshared (InvokableCall_1_t270836789 * __this, UnityAction_1_t3443095683 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3443095683 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t270836789 *)__this);
		((  void (*) (InvokableCall_1_t270836789 *, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t270836789 *)__this, (UnityAction_1_t3443095683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m3251799843_gshared (InvokableCall_1_t270836789 * __this, UnityAction_1_t3443095683 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3443095683 * V_0 = NULL;
	UnityAction_1_t3443095683 * V_1 = NULL;
	{
		UnityAction_1_t3443095683 * L_0 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3443095683 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3443095683 * L_1 = V_0;
		V_1 = (UnityAction_1_t3443095683 *)L_1;
		UnityAction_1_t3443095683 ** L_2 = (UnityAction_1_t3443095683 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3443095683 * L_3 = V_1;
		UnityAction_1_t3443095683 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3443095683 * L_6 = V_0;
		UnityAction_1_t3443095683 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3443095683 *>((UnityAction_1_t3443095683 **)L_2, (UnityAction_1_t3443095683 *)((UnityAction_1_t3443095683 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3443095683 *)L_6);
		V_0 = (UnityAction_1_t3443095683 *)L_7;
		UnityAction_1_t3443095683 * L_8 = V_0;
		UnityAction_1_t3443095683 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3443095683 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3443095683 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m1744559252_gshared (InvokableCall_1_t270836789 * __this, UnityAction_1_t3443095683 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3443095683 * V_0 = NULL;
	UnityAction_1_t3443095683 * V_1 = NULL;
	{
		UnityAction_1_t3443095683 * L_0 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3443095683 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3443095683 * L_1 = V_0;
		V_1 = (UnityAction_1_t3443095683 *)L_1;
		UnityAction_1_t3443095683 ** L_2 = (UnityAction_1_t3443095683 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3443095683 * L_3 = V_1;
		UnityAction_1_t3443095683 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3443095683 * L_6 = V_0;
		UnityAction_1_t3443095683 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3443095683 *>((UnityAction_1_t3443095683 **)L_2, (UnityAction_1_t3443095683 *)((UnityAction_1_t3443095683 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3443095683 *)L_6);
		V_0 = (UnityAction_1_t3443095683 *)L_7;
		UnityAction_1_t3443095683 * L_8 = V_0;
		UnityAction_1_t3443095683 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3443095683 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3443095683 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Single>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m4090512311_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m4090512311_gshared (InvokableCall_1_t270836789 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m4090512311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3443095683 * L_5 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3443095683 * L_7 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3443095683 *)L_7);
		((  void (*) (UnityAction_1_t3443095683 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3443095683 *)L_7, (float)((*(float*)((float*)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Single>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m678413071_gshared (InvokableCall_1_t270836789 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3443095683 * L_0 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3443095683 * L_3 = (UnityAction_1_t3443095683 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m983088749_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m983088749_gshared (InvokableCall_1_t214718932 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m983088749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t214718932 *)__this);
		((  void (*) (InvokableCall_1_t214718932 *, UnityAction_1_t3386977826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t214718932 *)__this, (UnityAction_1_t3386977826 *)((UnityAction_1_t3386977826 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m3755016325_gshared (InvokableCall_1_t214718932 * __this, UnityAction_1_t3386977826 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3386977826 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t214718932 *)__this);
		((  void (*) (InvokableCall_1_t214718932 *, UnityAction_1_t3386977826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t214718932 *)__this, (UnityAction_1_t3386977826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m705395724_gshared (InvokableCall_1_t214718932 * __this, UnityAction_1_t3386977826 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3386977826 * V_0 = NULL;
	UnityAction_1_t3386977826 * V_1 = NULL;
	{
		UnityAction_1_t3386977826 * L_0 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3386977826 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3386977826 * L_1 = V_0;
		V_1 = (UnityAction_1_t3386977826 *)L_1;
		UnityAction_1_t3386977826 ** L_2 = (UnityAction_1_t3386977826 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3386977826 * L_3 = V_1;
		UnityAction_1_t3386977826 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3386977826 * L_6 = V_0;
		UnityAction_1_t3386977826 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3386977826 *>((UnityAction_1_t3386977826 **)L_2, (UnityAction_1_t3386977826 *)((UnityAction_1_t3386977826 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3386977826 *)L_6);
		V_0 = (UnityAction_1_t3386977826 *)L_7;
		UnityAction_1_t3386977826 * L_8 = V_0;
		UnityAction_1_t3386977826 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3386977826 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3386977826 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3576859071_gshared (InvokableCall_1_t214718932 * __this, UnityAction_1_t3386977826 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3386977826 * V_0 = NULL;
	UnityAction_1_t3386977826 * V_1 = NULL;
	{
		UnityAction_1_t3386977826 * L_0 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3386977826 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3386977826 * L_1 = V_0;
		V_1 = (UnityAction_1_t3386977826 *)L_1;
		UnityAction_1_t3386977826 ** L_2 = (UnityAction_1_t3386977826 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3386977826 * L_3 = V_1;
		UnityAction_1_t3386977826 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3386977826 * L_6 = V_0;
		UnityAction_1_t3386977826 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3386977826 *>((UnityAction_1_t3386977826 **)L_2, (UnityAction_1_t3386977826 *)((UnityAction_1_t3386977826 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3386977826 *)L_6);
		V_0 = (UnityAction_1_t3386977826 *)L_7;
		UnityAction_1_t3386977826 * L_8 = V_0;
		UnityAction_1_t3386977826 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3386977826 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3386977826 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m2424028974_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m2424028974_gshared (InvokableCall_1_t214718932 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m2424028974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3386977826 * L_5 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3386977826 * L_7 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3386977826 *)L_7);
		((  void (*) (UnityAction_1_t3386977826 *, Color_t2020392075 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3386977826 *)L_7, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m1941574338_gshared (InvokableCall_1_t214718932 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3386977826 * L_0 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3386977826 * L_3 = (UnityAction_1_t3386977826 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_1__ctor_m2837611051_MetadataUsageId;
extern "C"  void InvokableCall_1__ctor_m2837611051_gshared (InvokableCall_1_t438034436 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1__ctor_m2837611051_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		NullCheck((InvokableCall_1_t438034436 *)__this);
		((  void (*) (InvokableCall_1_t438034436 *, UnityAction_1_t3610293330 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t438034436 *)__this, (UnityAction_1_t3610293330 *)((UnityAction_1_t3610293330 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1__ctor_m866952903_gshared (InvokableCall_1_t438034436 * __this, UnityAction_1_t3610293330 * ___action0, const MethodInfo* method)
{
	{
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m1107507914((BaseInvokableCall_t2229564840 *)__this, /*hidden argument*/NULL);
		UnityAction_1_t3610293330 * L_0 = ___action0;
		NullCheck((InvokableCall_1_t438034436 *)__this);
		((  void (*) (InvokableCall_1_t438034436 *, UnityAction_1_t3610293330 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InvokableCall_1_t438034436 *)__this, (UnityAction_1_t3610293330 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_add_Delegate_m1013059220_gshared (InvokableCall_1_t438034436 * __this, UnityAction_1_t3610293330 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3610293330 * V_0 = NULL;
	UnityAction_1_t3610293330 * V_1 = NULL;
	{
		UnityAction_1_t3610293330 * L_0 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3610293330 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3610293330 * L_1 = V_0;
		V_1 = (UnityAction_1_t3610293330 *)L_1;
		UnityAction_1_t3610293330 ** L_2 = (UnityAction_1_t3610293330 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3610293330 * L_3 = V_1;
		UnityAction_1_t3610293330 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3610293330 * L_6 = V_0;
		UnityAction_1_t3610293330 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3610293330 *>((UnityAction_1_t3610293330 **)L_2, (UnityAction_1_t3610293330 *)((UnityAction_1_t3610293330 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3610293330 *)L_6);
		V_0 = (UnityAction_1_t3610293330 *)L_7;
		UnityAction_1_t3610293330 * L_8 = V_0;
		UnityAction_1_t3610293330 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3610293330 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3610293330 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
extern "C"  void InvokableCall_1_remove_Delegate_m3619329377_gshared (InvokableCall_1_t438034436 * __this, UnityAction_1_t3610293330 * ___value0, const MethodInfo* method)
{
	UnityAction_1_t3610293330 * V_0 = NULL;
	UnityAction_1_t3610293330 * V_1 = NULL;
	{
		UnityAction_1_t3610293330 * L_0 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		V_0 = (UnityAction_1_t3610293330 *)L_0;
	}

IL_0007:
	{
		UnityAction_1_t3610293330 * L_1 = V_0;
		V_1 = (UnityAction_1_t3610293330 *)L_1;
		UnityAction_1_t3610293330 ** L_2 = (UnityAction_1_t3610293330 **)__this->get_address_of_Delegate_0();
		UnityAction_1_t3610293330 * L_3 = V_1;
		UnityAction_1_t3610293330 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, (Delegate_t3022476291 *)L_4, /*hidden argument*/NULL);
		UnityAction_1_t3610293330 * L_6 = V_0;
		UnityAction_1_t3610293330 * L_7 = InterlockedCompareExchangeImpl<UnityAction_1_t3610293330 *>((UnityAction_1_t3610293330 **)L_2, (UnityAction_1_t3610293330 *)((UnityAction_1_t3610293330 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))), (UnityAction_1_t3610293330 *)L_6);
		V_0 = (UnityAction_1_t3610293330 *)L_7;
		UnityAction_1_t3610293330 * L_8 = V_0;
		UnityAction_1_t3610293330 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_1_t3610293330 *)L_8) == ((Il2CppObject*)(UnityAction_1_t3610293330 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_1_Invoke_m3239892614_MetadataUsageId;
extern "C"  void InvokableCall_1_Invoke_m3239892614_gshared (InvokableCall_1_t438034436 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_1_Invoke_m3239892614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_1_t3610293330 * L_5 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		bool L_6 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		UnityAction_1_t3610293330 * L_7 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((UnityAction_1_t3610293330 *)L_7);
		((  void (*) (UnityAction_1_t3610293330 *, Vector2_t2243707579 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_1_t3610293330 *)L_7, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_10, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0040:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_1_Find_m4182726010_gshared (InvokableCall_1_t438034436 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t3610293330 * L_0 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_1_t3610293330 * L_3 = (UnityAction_1_t3610293330 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_2__ctor_m974169948_MetadataUsageId;
extern "C"  void InvokableCall_2__ctor_m974169948_gshared (InvokableCall_2_t3799696166 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2__ctor_m974169948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_2_t3784905282 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_2_Invoke_m1071013389_MetadataUsageId;
extern "C"  void InvokableCall_2_Invoke_m1071013389_gshared (InvokableCall_2_t3799696166 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_2_Invoke_m1071013389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		UnityAction_2_t3784905282 * L_8 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		bool L_9 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		UnityAction_2_t3784905282 * L_10 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 0;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 1;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck((UnityAction_2_t3784905282 *)L_10);
		((  void (*) (UnityAction_2_t3784905282 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((UnityAction_2_t3784905282 *)L_10, (Il2CppObject *)((Il2CppObject *)Castclass(L_13, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4))), (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
	}

IL_0050:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_2_Find_m1763382885_gshared (InvokableCall_2_t3799696166 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_2_t3784905282 * L_0 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_2_t3784905282 * L_3 = (UnityAction_2_t3784905282 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_3__ctor_m3141607487_MetadataUsageId;
extern "C"  void InvokableCall_3__ctor_m3141607487_gshared (InvokableCall_3_t2191335654 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3__ctor_m3141607487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_3_t3482433968 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_3_Invoke_m74557124_MetadataUsageId;
extern "C"  void InvokableCall_3_Invoke_m74557124_gshared (InvokableCall_3_t2191335654 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_3_Invoke_m74557124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		UnityAction_3_t3482433968 * L_11 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		bool L_12 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0060;
		}
	}
	{
		UnityAction_3_t3482433968 * L_13 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_14 = ___args0;
		NullCheck(L_14);
		int32_t L_15 = 0;
		Il2CppObject * L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		ObjectU5BU5D_t3614634134* L_17 = ___args0;
		NullCheck(L_17);
		int32_t L_18 = 1;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t3614634134* L_20 = ___args0;
		NullCheck(L_20);
		int32_t L_21 = 2;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck((UnityAction_3_t3482433968 *)L_13);
		((  void (*) (UnityAction_3_t3482433968 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((UnityAction_3_t3482433968 *)L_13, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5))), (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
	}

IL_0060:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_3_Find_m3470456112_gshared (InvokableCall_3_t2191335654 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_3_t3482433968 * L_0 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_3_t3482433968 * L_3 = (UnityAction_3_t3482433968 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall_4__ctor_m1096399974_MetadataUsageId;
extern "C"  void InvokableCall_4__ctor_m1096399974_gshared (InvokableCall_4_t2955480072 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4__ctor_m1096399974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		NullCheck((BaseInvokableCall_t2229564840 *)__this);
		BaseInvokableCall__ctor_m2877580597((BaseInvokableCall_t2229564840 *)__this, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t3022476291 * L_5 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, (MethodInfo_t *)L_2, (Type_t *)L_3, (Il2CppObject *)L_4, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_4_t1666603240 *)Castclass(L_5, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1))));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Invoke(System.Object[])
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3025533088;
extern const uint32_t InvokableCall_4_Invoke_m1555001411_MetadataUsageId;
extern "C"  void InvokableCall_4_Invoke_m1555001411_gshared (InvokableCall_4_t2955480072 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_4_Invoke_m1555001411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ___args0;
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) == ((int32_t)4)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, (String_t*)_stringLiteral3025533088, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		ObjectU5BU5D_t3614634134* L_2 = ___args0;
		NullCheck(L_2);
		int32_t L_3 = 0;
		Il2CppObject * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		ObjectU5BU5D_t3614634134* L_5 = ___args0;
		NullCheck(L_5);
		int32_t L_6 = 1;
		Il2CppObject * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		ObjectU5BU5D_t3614634134* L_8 = ___args0;
		NullCheck(L_8);
		int32_t L_9 = 2;
		Il2CppObject * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		ObjectU5BU5D_t3614634134* L_11 = ___args0;
		NullCheck(L_11);
		int32_t L_12 = 3;
		Il2CppObject * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		UnityAction_4_t1666603240 * L_14 = (UnityAction_4_t1666603240 *)__this->get_Delegate_0();
		bool L_15 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, (Delegate_t3022476291 *)L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0070;
		}
	}
	{
		UnityAction_4_t1666603240 * L_16 = (UnityAction_4_t1666603240 *)__this->get_Delegate_0();
		ObjectU5BU5D_t3614634134* L_17 = ___args0;
		NullCheck(L_17);
		int32_t L_18 = 0;
		Il2CppObject * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		ObjectU5BU5D_t3614634134* L_20 = ___args0;
		NullCheck(L_20);
		int32_t L_21 = 1;
		Il2CppObject * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		ObjectU5BU5D_t3614634134* L_23 = ___args0;
		NullCheck(L_23);
		int32_t L_24 = 2;
		Il2CppObject * L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		ObjectU5BU5D_t3614634134* L_26 = ___args0;
		NullCheck(L_26);
		int32_t L_27 = 3;
		Il2CppObject * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck((UnityAction_4_t1666603240 *)L_16);
		((  void (*) (UnityAction_4_t1666603240 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((UnityAction_4_t1666603240 *)L_16, (Il2CppObject *)((Il2CppObject *)Castclass(L_19, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_22, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7))), (Il2CppObject *)((Il2CppObject *)Castclass(L_25, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 8))), (Il2CppObject *)((Il2CppObject *)Castclass(L_28, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 9))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_0070:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_4_Find_m1467690987_gshared (InvokableCall_4_t2955480072 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_4_t1666603240 * L_0 = (UnityAction_4_t1666603240 *)__this->get_Delegate_0();
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_4_t1666603240 * L_3 = (UnityAction_4_t1666603240 *)__this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck((Il2CppObject *)L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_4, (Il2CppObject *)L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m1968084291_gshared (UnityAction_1_t897193173 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3523417209_gshared (UnityAction_1_t897193173 * __this, bool ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3523417209((UnityAction_1_t897193173 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Boolean>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2512011642_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2512011642_gshared (UnityAction_1_t897193173 * __this, bool ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2512011642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3317901367_gshared (UnityAction_1_t897193173 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m25541871_gshared (UnityAction_1_t3438463199 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2563101999_gshared (UnityAction_1_t3438463199 * __this, int32_t ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2563101999((UnityAction_1_t3438463199 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Int32>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m530778538_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m530778538_gshared (UnityAction_1_t3438463199 * __this, int32_t ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m530778538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m1662218393_gshared (UnityAction_1_t3438463199 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2836997866_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m1279804060_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m1279804060((UnityAction_1_t4056035046 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m3462722079_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg00;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m2822290096_gshared (UnityAction_1_t4056035046 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2172708761_gshared (UnityAction_1_t3443095683 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2563206587_gshared (UnityAction_1_t3443095683 * __this, float ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2563206587((UnityAction_1_t3443095683 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Single>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m4162767106_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m4162767106_gshared (UnityAction_1_t3443095683 * __this, float ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m4162767106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3175338521_gshared (UnityAction_1_t3443095683 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m3329809356_gshared (UnityAction_1_t3386977826 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2771701188_gshared (UnityAction_1_t3386977826 * __this, Color_t2020392075  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2771701188((UnityAction_1_t3386977826 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2020392075  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2020392075  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Color>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2192647899_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2192647899_gshared (UnityAction_1_t3386977826 * __this, Color_t2020392075  ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2192647899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m2603848420_gshared (UnityAction_1_t3386977826 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m2627946124_gshared (UnityAction_1_t3051495417 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3061904506_gshared (UnityAction_1_t3051495417 * __this, Scene_t1684909666  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m3061904506((UnityAction_1_t3051495417 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Scene_t1684909666_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m2974933271_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m2974933271_gshared (UnityAction_1_t3051495417 * __this, Scene_t1684909666  ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m2974933271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m3641222126_gshared (UnityAction_1_t3051495417 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_1__ctor_m1266646666_gshared (UnityAction_1_t3610293330 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m2702242020_gshared (UnityAction_1_t3610293330 * __this, Vector2_t2243707579  ___arg00, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_1_Invoke_m2702242020((UnityAction_1_t3610293330 *)__this->get_prev_9(),___arg00, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___arg00, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_1_BeginInvoke_m4083379797_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_1_BeginInvoke_m4083379797_gshared (UnityAction_1_t3610293330 * __this, Vector2_t2243707579  ___arg00, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_1_BeginInvoke_m4083379797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___arg00);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_1_EndInvoke_m539982532_gshared (UnityAction_1_t3610293330 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m622153369_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1994351568_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m1994351568((UnityAction_2_t3784905282 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<System.Object,System.Object>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m3203769083_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m4199296611_gshared (UnityAction_2_t3784905282 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m420633936_gshared (UnityAction_2_t1903595547 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1528820797_gshared (UnityAction_2_t1903595547 * __this, Scene_t1684909666  ___arg00, int32_t ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m1528820797((UnityAction_2_t1903595547 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, int32_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, int32_t ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* Scene_t1684909666_il2cpp_TypeInfo_var;
extern Il2CppClass* LoadSceneMode_t2981886439_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m2528278652_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2528278652_gshared (UnityAction_2_t1903595547 * __this, Scene_t1684909666  ___arg00, int32_t ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2528278652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(LoadSceneMode_t2981886439_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m1593881300_gshared (UnityAction_2_t1903595547 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_2__ctor_m2892452633_gshared (UnityAction_2_t606618774 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m670567184_gshared (UnityAction_2_t606618774 * __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_2_Invoke_m670567184((UnityAction_2_t606618774 *)__this->get_prev_9(),___arg00, ___arg11, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern Il2CppClass* Scene_t1684909666_il2cpp_TypeInfo_var;
extern const uint32_t UnityAction_2_BeginInvoke_m2733450299_MetadataUsageId;
extern "C"  Il2CppObject * UnityAction_2_BeginInvoke_m2733450299_gshared (UnityAction_2_t606618774 * __this, Scene_t1684909666  ___arg00, Scene_t1684909666  ___arg11, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityAction_2_BeginInvoke_m2733450299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg00);
	__d_args[1] = Box(Scene_t1684909666_il2cpp_TypeInfo_var, &___arg11);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_2_EndInvoke_m234106915_gshared (UnityAction_2_t606618774 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_3__ctor_m3783439840_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::Invoke(T0,T1,T2)
extern "C"  void UnityAction_3_Invoke_m1498227613_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_3_Invoke_m1498227613((UnityAction_3_t3482433968 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_3_BeginInvoke_m160302482_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Events.UnityAction`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_3_EndInvoke_m1279075386_gshared (UnityAction_3_t3482433968 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction_4__ctor_m2053485839_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T0,T1,T2,T3)
extern "C"  void UnityAction_4_Invoke_m3312096275_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_4_Invoke_m3312096275((UnityAction_4_t1666603240 *)__this->get_prev_9(),___arg00, ___arg11, ___arg22, ___arg33, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg00, ___arg11, ___arg22, ___arg33,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_4_BeginInvoke_m3427746322_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___arg00, Il2CppObject * ___arg11, Il2CppObject * ___arg22, Il2CppObject * ___arg33, AsyncCallback_t163412349 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method)
{
	void *__d_args[5] = {0};
	__d_args[0] = ___arg00;
	__d_args[1] = ___arg11;
	__d_args[2] = ___arg22;
	__d_args[3] = ___arg33;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback4, (Il2CppObject*)___object5);
}
// System.Void UnityEngine.Events.UnityAction`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_4_EndInvoke_m3887055469_gshared (UnityAction_4_t1666603240 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m4051141261_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m4051141261_gshared (UnityEvent_1_t3863924733 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m4051141261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m1708363187_gshared (UnityEvent_1_t3863924733 * __this, UnityAction_1_t897193173 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t897193173 * L_0 = ___call0;
		BaseInvokableCall_t2229564840 * L_1 = ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t897193173 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_AddCall_m1842680419((UnityEventBase_t828812576 *)__this, (BaseInvokableCall_t2229564840 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m670609979_gshared (UnityEvent_1_t3863924733 * __this, UnityAction_1_t897193173 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t897193173 * L_0 = ___call0;
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t897193173 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_RemoveListener_m2645959491((UnityEventBase_t828812576 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Boolean>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m3743240374_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m3743240374_gshared (UnityEvent_1_t3863924733 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m3743240374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m2856963016_gshared (UnityEvent_1_t3863924733 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t2019901575 * L_2 = (InvokableCall_1_t2019901575 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2019901575 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Boolean>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m1528404507_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t897193173 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		UnityAction_1_t897193173 * L_0 = ___action0;
		InvokableCall_1_t2019901575 * L_1 = (InvokableCall_1_t2019901575 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t2019901575 *, UnityAction_1_t897193173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t897193173 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2229564840 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Boolean>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m667974834_gshared (UnityEvent_1_t3863924733 * __this, bool ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		bool L_1 = ___arg00;
		bool L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m3244234683_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m3244234683_gshared (UnityEvent_1_t2110227463 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m3244234683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m846589010_gshared (UnityEvent_1_t2110227463 * __this, UnityAction_1_t3438463199 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3438463199 * L_0 = ___call0;
		BaseInvokableCall_t2229564840 * L_1 = ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3438463199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t3438463199 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_AddCall_m1842680419((UnityEventBase_t828812576 *)__this, (BaseInvokableCall_t2229564840 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m2851793905_gshared (UnityEvent_1_t2110227463 * __this, UnityAction_1_t3438463199 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3438463199 * L_0 = ___call0;
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t3438463199 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_RemoveListener_m2645959491((UnityEventBase_t828812576 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Int32>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m4083384818_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m4083384818_gshared (UnityEvent_1_t2110227463 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m4083384818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m3311025800_gshared (UnityEvent_1_t2110227463 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t266204305 * L_2 = (InvokableCall_1_t266204305 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t266204305 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Int32>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m3475403017_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t3438463199 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		UnityAction_1_t3438463199 * L_0 = ___action0;
		InvokableCall_1_t266204305 * L_1 = (InvokableCall_1_t266204305 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t266204305 *, UnityAction_1_t3438463199 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t3438463199 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2229564840 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1805498302_gshared (UnityEvent_1_t2110227463 * __this, int32_t ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		int32_t L_1 = ___arg00;
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m2073978020_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m2073978020_gshared (UnityEvent_1_t2727799310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m2073978020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m22503421_gshared (UnityEvent_1_t2727799310 * __this, UnityAction_1_t4056035046 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4056035046 * L_0 = ___call0;
		BaseInvokableCall_t2229564840 * L_1 = ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t4056035046 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_AddCall_m1842680419((UnityEventBase_t828812576 *)__this, (BaseInvokableCall_t2229564840 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m4278264272_gshared (UnityEvent_1_t2727799310 * __this, UnityAction_1_t4056035046 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t4056035046 * L_0 = ___call0;
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t4056035046 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_RemoveListener_m2645959491((UnityEventBase_t828812576 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Object>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m2223850067_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2223850067_gshared (UnityEvent_1_t2727799310 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m2223850067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m669290055_gshared (UnityEvent_1_t2727799310 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t883776152 * L_2 = (InvokableCall_1_t883776152 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t883776152 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Object>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m3098147632_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t4056035046 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		UnityAction_1_t4056035046 * L_0 = ___action0;
		InvokableCall_1_t883776152 * L_1 = (InvokableCall_1_t883776152 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t883776152 *, UnityAction_1_t4056035046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t4056035046 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2229564840 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m838874366_gshared (UnityEvent_1_t2727799310 * __this, Il2CppObject * ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		Il2CppObject * L_1 = ___arg00;
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t3614634134* L_2 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m29611311_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m29611311_gshared (UnityEvent_1_t2114859947 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m29611311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m2377847221_gshared (UnityEvent_1_t2114859947 * __this, UnityAction_1_t3443095683 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3443095683 * L_0 = ___call0;
		BaseInvokableCall_t2229564840 * L_1 = ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t3443095683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_AddCall_m1842680419((UnityEventBase_t828812576 *)__this, (BaseInvokableCall_t2229564840 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m2564825698_gshared (UnityEvent_1_t2114859947 * __this, UnityAction_1_t3443095683 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3443095683 * L_0 = ___call0;
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t3443095683 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_RemoveListener_m2645959491((UnityEventBase_t828812576 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Single>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m3813546_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m3813546_gshared (UnityEvent_1_t2114859947 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m3813546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m2566156550_gshared (UnityEvent_1_t2114859947 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t270836789 * L_2 = (InvokableCall_1_t270836789 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t270836789 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m4062537313_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t3443095683 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		UnityAction_1_t3443095683 * L_0 = ___action0;
		InvokableCall_1_t270836789 * L_1 = (InvokableCall_1_t270836789 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t270836789 *, UnityAction_1_t3443095683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t3443095683 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2229564840 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1298892870_gshared (UnityEvent_1_t2114859947 * __this, float ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		float L_1 = ___arg00;
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m117795578_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m117795578_gshared (UnityEvent_1_t2058742090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m117795578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m903508446_gshared (UnityEvent_1_t2058742090 * __this, UnityAction_1_t3386977826 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3386977826 * L_0 = ___call0;
		BaseInvokableCall_t2229564840 * L_1 = ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3386977826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t3386977826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_AddCall_m1842680419((UnityEventBase_t828812576 *)__this, (BaseInvokableCall_t2229564840 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m219620396_gshared (UnityEvent_1_t2058742090 * __this, UnityAction_1_t3386977826 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3386977826 * L_0 = ___call0;
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t3386977826 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_RemoveListener_m2645959491((UnityEventBase_t828812576 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m1178377679_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m1178377679_gshared (UnityEvent_1_t2058742090 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m1178377679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m2720691419_gshared (UnityEvent_1_t2058742090 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t214718932 * L_2 = (InvokableCall_1_t214718932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t214718932 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m1805145148_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t3386977826 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		UnityAction_1_t3386977826 * L_0 = ___action0;
		InvokableCall_1_t214718932 * L_1 = (InvokableCall_1_t214718932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t214718932 *, UnityAction_1_t3386977826 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t3386977826 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2229564840 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m2213115825_gshared (UnityEvent_1_t2058742090 * __this, Color_t2020392075  ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		Color_t2020392075  L_1 = ___arg00;
		Color_t2020392075  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m3317039790_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m3317039790_gshared (UnityEvent_1_t2282057594 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m3317039790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_AddListener_m525228415_gshared (UnityEvent_1_t2282057594 * __this, UnityAction_1_t3610293330 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3610293330 * L_0 = ___call0;
		BaseInvokableCall_t2229564840 * L_1 = ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t3610293330 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, (UnityAction_1_t3610293330 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_AddCall_m1842680419((UnityEventBase_t828812576 *)__this, (BaseInvokableCall_t2229564840 *)L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  void UnityEvent_1_RemoveListener_m4000386396_gshared (UnityEvent_1_t2282057594 * __this, UnityAction_1_t3610293330 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_1_t3610293330 * L_0 = ___call0;
		NullCheck((Delegate_t3022476291 *)L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953((Delegate_t3022476291 *)L_0, /*hidden argument*/NULL);
		UnityAction_1_t3610293330 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, (Delegate_t3022476291 *)L_2, /*hidden argument*/NULL);
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_RemoveListener_m2645959491((UnityEventBase_t828812576 *)__this, (Il2CppObject *)L_1, (MethodInfo_t *)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1_FindMethod_Impl_m2323626861_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2323626861_gshared (UnityEvent_1_t2282057594 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1_FindMethod_Impl_m2323626861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		MethodInfo_t * L_4 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_2, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_4;
		goto IL_0021;
	}

IL_0021:
	{
		MethodInfo_t * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m820458489_gshared (UnityEvent_1_t2282057594 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_1_t438034436 * L_2 = (InvokableCall_1_t438034436 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t438034436 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_1_GetDelegate_m66964436_gshared (Il2CppObject * __this /* static, unused */, UnityAction_1_t3610293330 * ___action0, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		UnityAction_1_t3610293330 * L_0 = ___action0;
		InvokableCall_1_t438034436 * L_1 = (InvokableCall_1_t438034436 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		((  void (*) (InvokableCall_1_t438034436 *, UnityAction_1_t3610293330 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (UnityAction_1_t3610293330 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2229564840 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>::Invoke(T0)
extern "C"  void UnityEvent_1_Invoke_m1533100983_gshared (UnityEvent_1_t2282057594 * __this, Vector2_t2243707579  ___arg00, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		Vector2_t2243707579  L_1 = ___arg00;
		Vector2_t2243707579  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 6), &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = (ObjectU5BU5D_t3614634134*)__this->get_m_InvokeArray_4();
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase_Invoke_m2706435282((UnityEventBase_t828812576 *)__this, (ObjectU5BU5D_t3614634134*)L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_2__ctor_m3717034779_MetadataUsageId;
extern "C"  void UnityEvent_2__ctor_m3717034779_gshared (UnityEvent_2_t1372135904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2__ctor_m3717034779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_2_FindMethod_Impl_m2783251718_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_2_FindMethod_Impl_m2783251718_gshared (UnityEvent_2_t1372135904 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2_FindMethod_Impl_m2783251718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		MethodInfo_t * L_6 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_4, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_6;
		goto IL_002e;
	}

IL_002e:
	{
		MethodInfo_t * L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_2_GetDelegate_m2147273130_gshared (UnityEvent_2_t1372135904 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_2_t3799696166 * L_2 = (InvokableCall_2_t3799696166 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (InvokableCall_2_t3799696166 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_3__ctor_m3502631330_MetadataUsageId;
extern "C"  void UnityEvent_3__ctor_m3502631330_gshared (UnityEvent_3_t3149477088 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3__ctor_m3502631330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_3_FindMethod_Impl_m1889846153_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_3_FindMethod_Impl_m1889846153_gshared (UnityEvent_3_t3149477088 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3_FindMethod_Impl_m1889846153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		TypeU5BU5D_t1664964607* L_6 = (TypeU5BU5D_t1664964607*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_7);
		MethodInfo_t * L_8 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_6, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_8;
		goto IL_003b;
	}

IL_003b:
	{
		MethodInfo_t * L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_3_GetDelegate_m338681277_gshared (UnityEvent_3_t3149477088 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_3_t2191335654 * L_2 = (InvokableCall_3_t2191335654 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3));
		((  void (*) (InvokableCall_3_t2191335654 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_4__ctor_m3102731553_MetadataUsageId;
extern "C"  void UnityEvent_4__ctor_m3102731553_gshared (UnityEvent_4_t2935245934 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4__ctor_m3102731553_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4)));
		NullCheck((UnityEventBase_t828812576 *)__this);
		UnityEventBase__ctor_m4062111756((UnityEventBase_t828812576 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_4_FindMethod_Impl_m4079512420_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_4_FindMethod_Impl_m4079512420_gshared (UnityEvent_4_t2935245934 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4_FindMethod_Impl_m4079512420_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		TypeU5BU5D_t1664964607* L_2 = (TypeU5BU5D_t1664964607*)((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)4));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_3);
		TypeU5BU5D_t1664964607* L_4 = (TypeU5BU5D_t1664964607*)L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_5);
		TypeU5BU5D_t1664964607* L_6 = (TypeU5BU5D_t1664964607*)L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_7);
		TypeU5BU5D_t1664964607* L_8 = (TypeU5BU5D_t1664964607*)L_6;
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Type_t *)L_9);
		MethodInfo_t * L_10 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)L_1, (TypeU5BU5D_t1664964607*)L_8, /*hidden argument*/NULL);
		V_0 = (MethodInfo_t *)L_10;
		goto IL_0048;
	}

IL_0048:
	{
		MethodInfo_t * L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_4_GetDelegate_m2704961864_gshared (UnityEvent_4_t2935245934 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	BaseInvokableCall_t2229564840 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_4_t2955480072 * L_2 = (InvokableCall_4_t2955480072 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (InvokableCall_4_t2955480072 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_2, (Il2CppObject *)L_0, (MethodInfo_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (BaseInvokableCall_t2229564840 *)L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2229564840 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void EventFunction_1__ctor_m814090495_gshared (EventFunction_1_t1186599945 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
extern "C"  void EventFunction_1_Invoke_m2378823590_gshared (EventFunction_1_t1186599945 * __this, Il2CppObject * ___handler0, BaseEventData_t2681005625 * ___eventData1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		EventFunction_1_Invoke_m2378823590((EventFunction_1_t1186599945 *)__this->get_prev_9(),___handler0, ___eventData1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___handler0, BaseEventData_t2681005625 * ___eventData1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___handler0, ___eventData1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___handler0, BaseEventData_t2681005625 * ___eventData1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___handler0, ___eventData1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, BaseEventData_t2681005625 * ___eventData1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___handler0, ___eventData1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::BeginInvoke(T1,UnityEngine.EventSystems.BaseEventData,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * EventFunction_1_BeginInvoke_m3064802067_gshared (EventFunction_1_t1186599945 * __this, Il2CppObject * ___handler0, BaseEventData_t2681005625 * ___eventData1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___handler0;
	__d_args[1] = ___eventData1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void EventFunction_1_EndInvoke_m1238672169_gshared (EventFunction_1_t1186599945 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CProcessMatchResponseU3Ec__Iterator0_2__ctor_m1399652640_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2603311978_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t2948921286_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2071365188;
extern Il2CppCodeGenString* _stringLiteral3776533995;
extern const uint32_t U3CProcessMatchResponseU3Ec__Iterator0_2_MoveNext_m295428290_MetadataUsageId;
extern "C"  bool U3CProcessMatchResponseU3Ec__Iterator0_2_MoveNext_m295428290_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CProcessMatchResponseU3Ec__Iterator0_2_MoveNext_m295428290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	FormatException_t2948921286 * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_6();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0042;
		}
	}
	{
		goto IL_0144;
	}

IL_0021:
	{
		WWW_t2919945039 * L_2 = (WWW_t2919945039 *)__this->get_client_0();
		__this->set_U24current_4(L_2);
		bool L_3 = (bool)__this->get_U24disposing_5();
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_003d:
	{
		goto IL_0146;
	}

IL_0042:
	{
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		__this->set_U3CjsonInterfaceU3E__0_1(L_4);
		WWW_t2919945039 * L_5 = (WWW_t2919945039 *)__this->get_client_0();
		NullCheck((WWW_t2919945039 *)L_5);
		String_t* L_6 = WWW_get_error_m3092701216((WWW_t2919945039 *)L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, (String_t*)L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00dc;
		}
	}
	{
		WWW_t2919945039 * L_8 = (WWW_t2919945039 *)__this->get_client_0();
		NullCheck((WWW_t2919945039 *)L_8);
		String_t* L_9 = WWW_get_text_m1558985139((WWW_t2919945039 *)L_8, /*hidden argument*/NULL);
		bool L_10 = SimpleJson_TryDeserializeObject_m2024713247(NULL /*static, unused*/, (String_t*)L_9, (Il2CppObject **)(&V_1), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00d6;
		}
	}
	{
		Il2CppObject * L_11 = V_1;
		V_2 = (Il2CppObject*)((Il2CppObject*)IsInst(L_11, IDictionary_2_t2603311978_il2cpp_TypeInfo_var));
		Il2CppObject* L_12 = V_2;
		if (!L_12)
		{
			goto IL_00d5;
		}
	}
	{
	}

IL_0089:
	try
	{ // begin try (depth: 1)
		Il2CppObject ** L_13 = (Il2CppObject **)__this->get_address_of_U3CjsonInterfaceU3E__0_1();
		Il2CppObject * L_14 = V_1;
		NullCheck((ResponseBase_t1952642056 *)(*L_13));
		VirtActionInvoker1< Il2CppObject * >::Invoke(4 /* System.Void UnityEngine.Networking.Match.ResponseBase::Parse(System.Object) */, (ResponseBase_t1952642056 *)(*L_13), (Il2CppObject *)L_14);
		goto IL_00d4;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t2948921286_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00a2;
		throw e;
	}

CATCH_00a2:
	{ // begin catch(System.FormatException)
		V_3 = (FormatException_t2948921286 *)((FormatException_t2948921286 *)__exception_local);
		Il2CppObject ** L_15 = (Il2CppObject **)__this->get_address_of_U3CjsonInterfaceU3E__0_1();
		ObjectU5BU5D_t3614634134* L_16 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		FormatException_t2948921286 * L_17 = V_3;
		NullCheck((Il2CppObject *)L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_17);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_18);
		String_t* L_19 = UnityString_Format_m2949645127(NULL /*static, unused*/, (String_t*)_stringLiteral2071365188, (ObjectU5BU5D_t3614634134*)L_16, /*hidden argument*/NULL);
		NullCheck((Response_t999782913 *)(*L_15));
		Response_SetFailure_m3586177683((Response_t999782913 *)(*L_15), (String_t*)L_19, /*hidden argument*/NULL);
		goto IL_00d4;
	} // end catch (depth: 1)

IL_00d4:
	{
	}

IL_00d5:
	{
	}

IL_00d6:
	{
		goto IL_011b;
	}

IL_00dc:
	{
		Il2CppObject ** L_20 = (Il2CppObject **)__this->get_address_of_U3CjsonInterfaceU3E__0_1();
		ObjectU5BU5D_t3614634134* L_21 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		WWW_t2919945039 * L_22 = (WWW_t2919945039 *)__this->get_client_0();
		NullCheck((WWW_t2919945039 *)L_22);
		String_t* L_23 = WWW_get_error_m3092701216((WWW_t2919945039 *)L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_23);
		ObjectU5BU5D_t3614634134* L_24 = (ObjectU5BU5D_t3614634134*)L_21;
		WWW_t2919945039 * L_25 = (WWW_t2919945039 *)__this->get_client_0();
		NullCheck((WWW_t2919945039 *)L_25);
		String_t* L_26 = WWW_get_text_m1558985139((WWW_t2919945039 *)L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_26);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_26);
		String_t* L_27 = UnityString_Format_m2949645127(NULL /*static, unused*/, (String_t*)_stringLiteral3776533995, (ObjectU5BU5D_t3614634134*)L_24, /*hidden argument*/NULL);
		NullCheck((Response_t999782913 *)(*L_20));
		Response_SetFailure_m3586177683((Response_t999782913 *)(*L_20), (String_t*)L_27, /*hidden argument*/NULL);
	}

IL_011b:
	{
		WWW_t2919945039 * L_28 = (WWW_t2919945039 *)__this->get_client_0();
		NullCheck((WWW_t2919945039 *)L_28);
		WWW_Dispose_m2554269413((WWW_t2919945039 *)L_28, /*hidden argument*/NULL);
		InternalResponseDelegate_2_t3073640404 * L_29 = (InternalResponseDelegate_2_t3073640404 *)__this->get_internalCallback_2();
		Il2CppObject * L_30 = (Il2CppObject *)__this->get_U3CjsonInterfaceU3E__0_1();
		Il2CppObject * L_31 = (Il2CppObject *)__this->get_userCallback_3();
		NullCheck((InternalResponseDelegate_2_t3073640404 *)L_29);
		((  void (*) (InternalResponseDelegate_2_t3073640404 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((InternalResponseDelegate_2_t3073640404 *)L_29, (Il2CppObject *)L_30, (Il2CppObject *)L_31, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		__this->set_U24PC_6((-1));
	}

IL_0144:
	{
		return (bool)0;
	}

IL_0146:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CProcessMatchResponseU3Ec__Iterator0_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2722760562_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		V_0 = (Il2CppObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CProcessMatchResponseU3Ec__Iterator0_2_System_Collections_IEnumerator_get_Current_m2214904858_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		V_0 = (Il2CppObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CProcessMatchResponseU3Ec__Iterator0_2_Dispose_m2975156865_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`2<System.Object,System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CProcessMatchResponseU3Ec__Iterator0_2_Reset_m307142371_MetadataUsageId;
extern "C"  void U3CProcessMatchResponseU3Ec__Iterator0_2_Reset_m307142371_gshared (U3CProcessMatchResponseU3Ec__Iterator0_2_t106232007 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CProcessMatchResponseU3Ec__Iterator0_2_Reset_m307142371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DataResponseDelegate_1__ctor_m2222754223_gshared (DataResponseDelegate_1_t945754175 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<System.Object>::Invoke(System.Boolean,System.String,T)
extern "C"  void DataResponseDelegate_1_Invoke_m2892839852_gshared (DataResponseDelegate_1_t945754175 * __this, bool ___success0, String_t* ___extendedInfo1, Il2CppObject * ___responseData2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DataResponseDelegate_1_Invoke_m2892839852((DataResponseDelegate_1_t945754175 *)__this->get_prev_9(),___success0, ___extendedInfo1, ___responseData2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___success0, String_t* ___extendedInfo1, Il2CppObject * ___responseData2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___success0, ___extendedInfo1, ___responseData2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___success0, String_t* ___extendedInfo1, Il2CppObject * ___responseData2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___success0, ___extendedInfo1, ___responseData2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<System.Object>::BeginInvoke(System.Boolean,System.String,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t DataResponseDelegate_1_BeginInvoke_m2356550577_MetadataUsageId;
extern "C"  Il2CppObject * DataResponseDelegate_1_BeginInvoke_m2356550577_gshared (DataResponseDelegate_1_t945754175 * __this, bool ___success0, String_t* ___extendedInfo1, Il2CppObject * ___responseData2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DataResponseDelegate_1_BeginInvoke_m2356550577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___success0);
	__d_args[1] = ___extendedInfo1;
	__d_args[2] = ___responseData2;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Networking.Match.NetworkMatch/DataResponseDelegate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void DataResponseDelegate_1_EndInvoke_m1252280981_gshared (DataResponseDelegate_1_t945754175 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Networking.Match.NetworkMatch/InternalResponseDelegate`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void InternalResponseDelegate_2__ctor_m3764815497_gshared (InternalResponseDelegate_2_t3073640404 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Networking.Match.NetworkMatch/InternalResponseDelegate`2<System.Object,System.Object>::Invoke(T,U)
extern "C"  void InternalResponseDelegate_2_Invoke_m3415191514_gshared (InternalResponseDelegate_2_t3073640404 * __this, Il2CppObject * ___response0, Il2CppObject * ___userCallback1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		InternalResponseDelegate_2_Invoke_m3415191514((InternalResponseDelegate_2_t3073640404 *)__this->get_prev_9(),___response0, ___userCallback1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___response0, Il2CppObject * ___userCallback1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___response0, ___userCallback1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___response0, Il2CppObject * ___userCallback1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___response0, ___userCallback1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___userCallback1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___response0, ___userCallback1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Networking.Match.NetworkMatch/InternalResponseDelegate`2<System.Object,System.Object>::BeginInvoke(T,U,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * InternalResponseDelegate_2_BeginInvoke_m2722812275_gshared (InternalResponseDelegate_2_t3073640404 * __this, Il2CppObject * ___response0, Il2CppObject * ___userCallback1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___response0;
	__d_args[1] = ___userCallback1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Networking.Match.NetworkMatch/InternalResponseDelegate`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void InternalResponseDelegate_2_EndInvoke_m769238843_gshared (InternalResponseDelegate_2_t3073640404 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void SyncListChanged__ctor_m769665116_gshared (SyncListChanged_t2191845168 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Boolean>::Invoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
extern "C"  void SyncListChanged_Invoke_m3033510366_gshared (SyncListChanged_t2191845168 * __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SyncListChanged_Invoke_m3033510366((SyncListChanged_t2191845168 *)__this->get_prev_9(),___op0, ___itemIndex1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___op0, ___itemIndex1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___op0, ___itemIndex1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Networking.SyncList`1/SyncListChanged<System.Boolean>::BeginInvoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Operation_t3559304062_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t SyncListChanged_BeginInvoke_m2345038185_MetadataUsageId;
extern "C"  Il2CppObject * SyncListChanged_BeginInvoke_m2345038185_gshared (SyncListChanged_t2191845168 * __this, int32_t ___op0, int32_t ___itemIndex1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncListChanged_BeginInvoke_m2345038185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Operation_t3559304062_il2cpp_TypeInfo_var, &___op0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___itemIndex1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void SyncListChanged_EndInvoke_m2948996758_gshared (SyncListChanged_t2191845168 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void SyncListChanged__ctor_m379823528_gshared (SyncListChanged_t438147898 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Int32>::Invoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
extern "C"  void SyncListChanged_Invoke_m334110062_gshared (SyncListChanged_t438147898 * __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SyncListChanged_Invoke_m334110062((SyncListChanged_t438147898 *)__this->get_prev_9(),___op0, ___itemIndex1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___op0, ___itemIndex1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___op0, ___itemIndex1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Networking.SyncList`1/SyncListChanged<System.Int32>::BeginInvoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Operation_t1805606792_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t SyncListChanged_BeginInvoke_m3362829979_MetadataUsageId;
extern "C"  Il2CppObject * SyncListChanged_BeginInvoke_m3362829979_gshared (SyncListChanged_t438147898 * __this, int32_t ___op0, int32_t ___itemIndex1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncListChanged_BeginInvoke_m3362829979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Operation_t1805606792_il2cpp_TypeInfo_var, &___op0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___itemIndex1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void SyncListChanged_EndInvoke_m3084555514_gshared (SyncListChanged_t438147898 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void SyncListChanged__ctor_m853520105_gshared (SyncListChanged_t1055719745 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Object>::Invoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
extern "C"  void SyncListChanged_Invoke_m931180591_gshared (SyncListChanged_t1055719745 * __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SyncListChanged_Invoke_m931180591((SyncListChanged_t1055719745 *)__this->get_prev_9(),___op0, ___itemIndex1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___op0, ___itemIndex1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___op0, ___itemIndex1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Networking.SyncList`1/SyncListChanged<System.Object>::BeginInvoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Operation_t2423178639_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t SyncListChanged_BeginInvoke_m1984733246_MetadataUsageId;
extern "C"  Il2CppObject * SyncListChanged_BeginInvoke_m1984733246_gshared (SyncListChanged_t1055719745 * __this, int32_t ___op0, int32_t ___itemIndex1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncListChanged_BeginInvoke_m1984733246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Operation_t2423178639_il2cpp_TypeInfo_var, &___op0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___itemIndex1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void SyncListChanged_EndInvoke_m523398343_gshared (SyncListChanged_t1055719745 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void SyncListChanged__ctor_m1363939818_gshared (SyncListChanged_t442780382 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Single>::Invoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
extern "C"  void SyncListChanged_Invoke_m1324996092_gshared (SyncListChanged_t442780382 * __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SyncListChanged_Invoke_m1324996092((SyncListChanged_t442780382 *)__this->get_prev_9(),___op0, ___itemIndex1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___op0, ___itemIndex1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___op0, ___itemIndex1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Networking.SyncList`1/SyncListChanged<System.Single>::BeginInvoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Operation_t1810239276_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t SyncListChanged_BeginInvoke_m3282563275_MetadataUsageId;
extern "C"  Il2CppObject * SyncListChanged_BeginInvoke_m3282563275_gshared (SyncListChanged_t442780382 * __this, int32_t ___op0, int32_t ___itemIndex1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncListChanged_BeginInvoke_m3282563275_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Operation_t1810239276_il2cpp_TypeInfo_var, &___op0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___itemIndex1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void SyncListChanged_EndInvoke_m2669966992_gshared (SyncListChanged_t442780382 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void SyncListChanged__ctor_m368823015_gshared (SyncListChanged_t515952471 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.UInt32>::Invoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
extern "C"  void SyncListChanged_Invoke_m1252266753_gshared (SyncListChanged_t515952471 * __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SyncListChanged_Invoke_m1252266753((SyncListChanged_t515952471 *)__this->get_prev_9(),___op0, ___itemIndex1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___op0, ___itemIndex1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___op0, ___itemIndex1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Networking.SyncList`1/SyncListChanged<System.UInt32>::BeginInvoke(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Operation_t1883411365_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t SyncListChanged_BeginInvoke_m488249864_MetadataUsageId;
extern "C"  Il2CppObject * SyncListChanged_BeginInvoke_m488249864_gshared (SyncListChanged_t515952471 * __this, int32_t ___op0, int32_t ___itemIndex1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncListChanged_BeginInvoke_m488249864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Operation_t1883411365_il2cpp_TypeInfo_var, &___op0);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___itemIndex1);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void UnityEngine.Networking.SyncList`1/SyncListChanged<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  void SyncListChanged_EndInvoke_m566727093_gshared (SyncListChanged_t515952471 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::.ctor()
extern "C"  void SyncList_1__ctor_m3883448086_gshared (SyncList_1_t486689021 * __this, const MethodInfo* method)
{
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t3194695850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_Objects_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Networking.SyncList`1<System.Boolean>::get_Count()
extern "C"  int32_t SyncList_1_get_Count_m729930006_gshared (SyncList_1_t486689021 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		NullCheck((List_1_t3194695850 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t3194695850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t3194695850 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Boolean>::get_IsReadOnly()
extern "C"  bool SyncList_1_get_IsReadOnly_m1309112529_gshared (SyncList_1_t486689021 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		goto IL_0008;
	}

IL_0008:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Networking.SyncList`1/SyncListChanged<T> UnityEngine.Networking.SyncList`1<System.Boolean>::get_Callback()
extern "C"  SyncListChanged_t2191845168 * SyncList_1_get_Callback_m1373482542_gshared (SyncList_1_t486689021 * __this, const MethodInfo* method)
{
	SyncListChanged_t2191845168 * V_0 = NULL;
	{
		SyncListChanged_t2191845168 * L_0 = (SyncListChanged_t2191845168 *)__this->get_m_Callback_3();
		V_0 = (SyncListChanged_t2191845168 *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		SyncListChanged_t2191845168 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::set_Callback(UnityEngine.Networking.SyncList`1/SyncListChanged<T>)
extern "C"  void SyncList_1_set_Callback_m1728077921_gshared (SyncList_1_t486689021 * __this, SyncListChanged_t2191845168 * ___value0, const MethodInfo* method)
{
	{
		SyncListChanged_t2191845168 * L_0 = ___value0;
		__this->set_m_Callback_3(L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::InitializeBehaviour(UnityEngine.Networking.NetworkBehaviour,System.Int32)
extern "C"  void SyncList_1_InitializeBehaviour_m3861922590_gshared (SyncList_1_t486689021 * __this, NetworkBehaviour_t3873055601 * ___beh0, int32_t ___cmdHash1, const MethodInfo* method)
{
	{
		NetworkBehaviour_t3873055601 * L_0 = ___beh0;
		__this->set_m_Behaviour_1(L_0);
		int32_t L_1 = ___cmdHash1;
		__this->set_m_CmdHash_2(L_1);
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,T)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* LogFilter_t2612395354_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkWriter_t560143343_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkServer_t3779449791_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2028122062;
extern Il2CppCodeGenString* _stringLiteral2863456972;
extern const uint32_t SyncList_1_SendMsg_m3803823555_MetadataUsageId;
extern "C"  void SyncList_1_SendMsg_m3803823555_gshared (SyncList_1_t486689021 * __this, int32_t ___op0, int32_t ___itemIndex1, bool ___item2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncList_1_SendMsg_m3803823555_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NetworkIdentity_t1766639790 * V_0 = NULL;
	NetworkWriter_t560143343 * V_1 = NULL;
	{
		NetworkBehaviour_t3873055601 * L_0 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogFilter_t2612395354_il2cpp_TypeInfo_var);
		bool L_2 = LogFilter_get_logError_m3315740406(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2028122062, /*hidden argument*/NULL);
	}

IL_0029:
	{
		goto IL_0107;
	}

IL_002e:
	{
		NetworkBehaviour_t3873055601 * L_3 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((Component_t3819376471 *)L_3);
		NetworkIdentity_t1766639790 * L_4 = Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169((Component_t3819376471 *)L_3, /*hidden argument*/Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169_MethodInfo_var);
		V_0 = (NetworkIdentity_t1766639790 *)L_4;
		NetworkIdentity_t1766639790 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogFilter_t2612395354_il2cpp_TypeInfo_var);
		bool L_7 = LogFilter_get_logError_m3315740406(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2863456972, /*hidden argument*/NULL);
	}

IL_005d:
	{
		goto IL_0107;
	}

IL_0062:
	{
		NetworkIdentity_t1766639790 * L_8 = V_0;
		NullCheck((NetworkIdentity_t1766639790 *)L_8);
		bool L_9 = NetworkIdentity_get_isServer_m1468412663((NetworkIdentity_t1766639790 *)L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0073;
		}
	}
	{
		goto IL_0107;
	}

IL_0073:
	{
		NetworkWriter_t560143343 * L_10 = (NetworkWriter_t560143343 *)il2cpp_codegen_object_new(NetworkWriter_t560143343_il2cpp_TypeInfo_var);
		NetworkWriter__ctor_m2809346428(L_10, /*hidden argument*/NULL);
		V_1 = (NetworkWriter_t560143343 *)L_10;
		NetworkWriter_t560143343 * L_11 = V_1;
		NullCheck((NetworkWriter_t560143343 *)L_11);
		NetworkWriter_StartMessage_m931021002((NetworkWriter_t560143343 *)L_11, (int16_t)((int32_t)9), /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_12 = V_1;
		NetworkIdentity_t1766639790 * L_13 = V_0;
		NullCheck((NetworkIdentity_t1766639790 *)L_13);
		NetworkInstanceId_t33998832  L_14 = NetworkIdentity_get_netId_m1164915858((NetworkIdentity_t1766639790 *)L_13, /*hidden argument*/NULL);
		NullCheck((NetworkWriter_t560143343 *)L_12);
		NetworkWriter_Write_m688344706((NetworkWriter_t560143343 *)L_12, (NetworkInstanceId_t33998832 )L_14, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_15 = V_1;
		int32_t L_16 = (int32_t)__this->get_m_CmdHash_2();
		NullCheck((NetworkWriter_t560143343 *)L_15);
		NetworkWriter_WritePackedUInt32_m2634533176((NetworkWriter_t560143343 *)L_15, (uint32_t)L_16, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_17 = V_1;
		int32_t L_18 = ___op0;
		NullCheck((NetworkWriter_t560143343 *)L_17);
		NetworkWriter_Write_m1305006738((NetworkWriter_t560143343 *)L_17, (uint8_t)(((int32_t)((uint8_t)L_18))), /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_19 = V_1;
		int32_t L_20 = ___itemIndex1;
		NullCheck((NetworkWriter_t560143343 *)L_19);
		NetworkWriter_WritePackedUInt32_m2634533176((NetworkWriter_t560143343 *)L_19, (uint32_t)L_20, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_21 = V_1;
		bool L_22 = ___item2;
		NullCheck((SyncList_1_t486689021 *)__this);
		VirtActionInvoker2< NetworkWriter_t560143343 *, bool >::Invoke(18 /* System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::SerializeItem(UnityEngine.Networking.NetworkWriter,T) */, (SyncList_1_t486689021 *)__this, (NetworkWriter_t560143343 *)L_21, (bool)L_22);
		NetworkWriter_t560143343 * L_23 = V_1;
		NullCheck((NetworkWriter_t560143343 *)L_23);
		NetworkWriter_FinishMessage_m3604322206((NetworkWriter_t560143343 *)L_23, /*hidden argument*/NULL);
		NetworkIdentity_t1766639790 * L_24 = V_0;
		NullCheck((Component_t3819376471 *)L_24);
		GameObject_t1756533147 * L_25 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_24, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_26 = V_1;
		NetworkBehaviour_t3873055601 * L_27 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 UnityEngine.Networking.NetworkBehaviour::GetNetworkChannel() */, (NetworkBehaviour_t3873055601 *)L_27);
		IL2CPP_RUNTIME_CLASS_INIT(NetworkServer_t3779449791_il2cpp_TypeInfo_var);
		NetworkServer_SendWriterToReady_m3464733118(NULL /*static, unused*/, (GameObject_t1756533147 *)L_25, (NetworkWriter_t560143343 *)L_26, (int32_t)L_28, /*hidden argument*/NULL);
		NetworkBehaviour_t3873055601 * L_29 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_29);
		bool L_30 = NetworkBehaviour_get_isServer_m3866369296((NetworkBehaviour_t3873055601 *)L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0107;
		}
	}
	{
		NetworkBehaviour_t3873055601 * L_31 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_31);
		bool L_32 = NetworkBehaviour_get_isClient_m3491347612((NetworkBehaviour_t3873055601 *)L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0107;
		}
	}
	{
		SyncListChanged_t2191845168 * L_33 = (SyncListChanged_t2191845168 *)__this->get_m_Callback_3();
		if (!L_33)
		{
			goto IL_0107;
		}
	}
	{
		SyncListChanged_t2191845168 * L_34 = (SyncListChanged_t2191845168 *)__this->get_m_Callback_3();
		int32_t L_35 = ___op0;
		int32_t L_36 = ___itemIndex1;
		NullCheck((SyncListChanged_t2191845168 *)L_34);
		((  void (*) (SyncListChanged_t2191845168 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((SyncListChanged_t2191845168 *)L_34, (int32_t)L_35, (int32_t)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_0107:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t SyncList_1_SendMsg_m3461705107_MetadataUsageId;
extern "C"  void SyncList_1_SendMsg_m3461705107_gshared (SyncList_1_t486689021 * __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncList_1_SendMsg_m3461705107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = ___op0;
		int32_t L_1 = ___itemIndex1;
		Initobj (Boolean_t3825574718_il2cpp_TypeInfo_var, (&V_0));
		bool L_2 = V_0;
		NullCheck((SyncList_1_t486689021 *)__this);
		((  void (*) (SyncList_1_t486689021 *, int32_t, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t486689021 *)__this, (int32_t)L_0, (int32_t)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::HandleMsg(UnityEngine.Networking.NetworkReader)
extern "C"  void SyncList_1_HandleMsg_m510169247_gshared (SyncList_1_t486689021 * __this, NetworkReader_t3187690923 * ___reader0, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	{
		NetworkReader_t3187690923 * L_0 = ___reader0;
		NullCheck((NetworkReader_t3187690923 *)L_0);
		uint8_t L_1 = NetworkReader_ReadByte_m3670356752((NetworkReader_t3187690923 *)L_0, /*hidden argument*/NULL);
		V_0 = (uint8_t)L_1;
		NetworkReader_t3187690923 * L_2 = ___reader0;
		NullCheck((NetworkReader_t3187690923 *)L_2);
		uint32_t L_3 = NetworkReader_ReadPackedUInt32_m3078633978((NetworkReader_t3187690923 *)L_2, /*hidden argument*/NULL);
		V_1 = (int32_t)L_3;
		NetworkReader_t3187690923 * L_4 = ___reader0;
		NullCheck((SyncList_1_t486689021 *)__this);
		bool L_5 = VirtFuncInvoker1< bool, NetworkReader_t3187690923 * >::Invoke(19 /* T UnityEngine.Networking.SyncList`1<System.Boolean>::DeserializeItem(UnityEngine.Networking.NetworkReader) */, (SyncList_1_t486689021 *)__this, (NetworkReader_t3187690923 *)L_4);
		V_2 = (bool)L_5;
		uint8_t L_6 = V_0;
		V_3 = (int32_t)L_6;
		int32_t L_7 = V_3;
		if (L_7 == 0)
		{
			goto IL_0040;
		}
		if (L_7 == 1)
		{
			goto IL_0051;
		}
		if (L_7 == 2)
		{
			goto IL_0061;
		}
		if (L_7 == 3)
		{
			goto IL_0073;
		}
		if (L_7 == 4)
		{
			goto IL_0085;
		}
		if (L_7 == 5)
		{
			goto IL_0096;
		}
		if (L_7 == 6)
		{
			goto IL_0096;
		}
	}
	{
		goto IL_00a8;
	}

IL_0040:
	{
		List_1_t3194695850 * L_8 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		bool L_9 = V_2;
		NullCheck((List_1_t3194695850 *)L_8);
		((  void (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3194695850 *)L_8, (bool)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		goto IL_00a8;
	}

IL_0051:
	{
		List_1_t3194695850 * L_10 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		NullCheck((List_1_t3194695850 *)L_10);
		((  void (*) (List_1_t3194695850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t3194695850 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		goto IL_00a8;
	}

IL_0061:
	{
		List_1_t3194695850 * L_11 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		int32_t L_12 = V_1;
		bool L_13 = V_2;
		NullCheck((List_1_t3194695850 *)L_11);
		((  void (*) (List_1_t3194695850 *, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t3194695850 *)L_11, (int32_t)L_12, (bool)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		goto IL_00a8;
	}

IL_0073:
	{
		List_1_t3194695850 * L_14 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		bool L_15 = V_2;
		NullCheck((List_1_t3194695850 *)L_14);
		((  bool (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t3194695850 *)L_14, (bool)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_00a8;
	}

IL_0085:
	{
		List_1_t3194695850 * L_16 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		int32_t L_17 = V_1;
		NullCheck((List_1_t3194695850 *)L_16);
		((  void (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t3194695850 *)L_16, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		goto IL_00a8;
	}

IL_0096:
	{
		List_1_t3194695850 * L_18 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		int32_t L_19 = V_1;
		bool L_20 = V_2;
		NullCheck((List_1_t3194695850 *)L_18);
		((  void (*) (List_1_t3194695850 *, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3194695850 *)L_18, (int32_t)L_19, (bool)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		goto IL_00a8;
	}

IL_00a8:
	{
		SyncListChanged_t2191845168 * L_21 = (SyncListChanged_t2191845168 *)__this->get_m_Callback_3();
		if (!L_21)
		{
			goto IL_00c2;
		}
	}
	{
		SyncListChanged_t2191845168 * L_22 = (SyncListChanged_t2191845168 *)__this->get_m_Callback_3();
		uint8_t L_23 = V_0;
		int32_t L_24 = V_1;
		NullCheck((SyncListChanged_t2191845168 *)L_22);
		((  void (*) (SyncListChanged_t2191845168 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((SyncListChanged_t2191845168 *)L_22, (int32_t)L_23, (int32_t)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_00c2:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::AddInternal(T)
extern "C"  void SyncList_1_AddInternal_m3400484086_gshared (SyncList_1_t486689021 * __this, bool ___item0, const MethodInfo* method)
{
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		bool L_1 = ___item0;
		NullCheck((List_1_t3194695850 *)L_0);
		((  void (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3194695850 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::Add(T)
extern "C"  void SyncList_1_Add_m1357060353_gshared (SyncList_1_t486689021 * __this, bool ___item0, const MethodInfo* method)
{
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		bool L_1 = ___item0;
		NullCheck((List_1_t3194695850 *)L_0);
		((  void (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t3194695850 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		List_1_t3194695850 * L_2 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		NullCheck((List_1_t3194695850 *)L_2);
		int32_t L_3 = ((  int32_t (*) (List_1_t3194695850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t3194695850 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		bool L_4 = ___item0;
		NullCheck((SyncList_1_t486689021 *)__this);
		((  void (*) (SyncList_1_t486689021 *, int32_t, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t486689021 *)__this, (int32_t)0, (int32_t)((int32_t)((int32_t)L_3-(int32_t)1)), (bool)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::Clear()
extern "C"  void SyncList_1_Clear_m3078903365_gshared (SyncList_1_t486689021 * __this, const MethodInfo* method)
{
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		NullCheck((List_1_t3194695850 *)L_0);
		((  void (*) (List_1_t3194695850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t3194695850 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((SyncList_1_t486689021 *)__this);
		((  void (*) (SyncList_1_t486689021 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((SyncList_1_t486689021 *)__this, (int32_t)1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Boolean>::Contains(T)
extern "C"  bool SyncList_1_Contains_m2034188887_gshared (SyncList_1_t486689021 * __this, bool ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		bool L_1 = ___item0;
		NullCheck((List_1_t3194695850 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t3194695850 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (bool)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::CopyTo(T[],System.Int32)
extern "C"  void SyncList_1_CopyTo_m1452428865_gshared (SyncList_1_t486689021 * __this, BooleanU5BU5D_t3568034315* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		BooleanU5BU5D_t3568034315* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((List_1_t3194695850 *)L_0);
		((  void (*) (List_1_t3194695850 *, BooleanU5BU5D_t3568034315*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t3194695850 *)L_0, (BooleanU5BU5D_t3568034315*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return;
	}
}
// System.Int32 UnityEngine.Networking.SyncList`1<System.Boolean>::IndexOf(T)
extern "C"  int32_t SyncList_1_IndexOf_m3878656989_gshared (SyncList_1_t486689021 * __this, bool ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		bool L_1 = ___item0;
		NullCheck((List_1_t3194695850 *)L_0);
		int32_t L_2 = ((  int32_t (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t3194695850 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (int32_t)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::Insert(System.Int32,T)
extern "C"  void SyncList_1_Insert_m636769576_gshared (SyncList_1_t486689021 * __this, int32_t ___index0, bool ___item1, const MethodInfo* method)
{
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		int32_t L_1 = ___index0;
		bool L_2 = ___item1;
		NullCheck((List_1_t3194695850 *)L_0);
		((  void (*) (List_1_t3194695850 *, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t3194695850 *)L_0, (int32_t)L_1, (bool)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		int32_t L_3 = ___index0;
		bool L_4 = ___item1;
		NullCheck((SyncList_1_t486689021 *)__this);
		((  void (*) (SyncList_1_t486689021 *, int32_t, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t486689021 *)__this, (int32_t)2, (int32_t)L_3, (bool)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Boolean>::Remove(T)
extern "C"  bool SyncList_1_Remove_m4241621430_gshared (SyncList_1_t486689021 * __this, bool ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		bool L_1 = ___item0;
		NullCheck((List_1_t3194695850 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t3194695850 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t3194695850 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_0 = (bool)L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		bool L_4 = ___item0;
		NullCheck((SyncList_1_t486689021 *)__this);
		((  void (*) (SyncList_1_t486689021 *, int32_t, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t486689021 *)__this, (int32_t)3, (int32_t)0, (bool)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_001f:
	{
		bool L_5 = V_0;
		V_1 = (bool)L_5;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::RemoveAt(System.Int32)
extern "C"  void SyncList_1_RemoveAt_m595967228_gshared (SyncList_1_t486689021 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t3194695850 *)L_0);
		((  void (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t3194695850 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		int32_t L_2 = ___index0;
		NullCheck((SyncList_1_t486689021 *)__this);
		((  void (*) (SyncList_1_t486689021 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((SyncList_1_t486689021 *)__this, (int32_t)4, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::Dirty(System.Int32)
extern "C"  void SyncList_1_Dirty_m2092426563_gshared (SyncList_1_t486689021 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		List_1_t3194695850 * L_1 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		int32_t L_2 = ___index0;
		NullCheck((List_1_t3194695850 *)L_1);
		bool L_3 = ((  bool (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3194695850 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		NullCheck((SyncList_1_t486689021 *)__this);
		((  void (*) (SyncList_1_t486689021 *, int32_t, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t486689021 *)__this, (int32_t)6, (int32_t)L_0, (bool)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// T UnityEngine.Networking.SyncList`1<System.Boolean>::get_Item(System.Int32)
extern "C"  bool SyncList_1_get_Item_m2490763992_gshared (SyncList_1_t486689021 * __this, int32_t ___i0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		int32_t L_1 = ___i0;
		NullCheck((List_1_t3194695850 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3194695850 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_0 = (bool)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Boolean>::set_Item(System.Int32,T)
extern "C"  void SyncList_1_set_Item_m3387238089_gshared (SyncList_1_t486689021 * __this, int32_t ___i0, bool ___value1, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		int32_t L_1 = ___i0;
		NullCheck((List_1_t3194695850 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t3194695850 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t3194695850 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_1 = (bool)L_2;
		bool L_3 = ___value1;
		bool L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18), &L_4);
		bool L_6 = Boolean_Equals_m2118901528((bool*)(&V_1), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		List_1_t3194695850 * L_7 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		int32_t L_8 = ___i0;
		bool L_9 = ___value1;
		NullCheck((List_1_t3194695850 *)L_7);
		((  void (*) (List_1_t3194695850 *, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t3194695850 *)L_7, (int32_t)L_8, (bool)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_11 = ___i0;
		bool L_12 = ___value1;
		NullCheck((SyncList_1_t486689021 *)__this);
		((  void (*) (SyncList_1_t486689021 *, int32_t, int32_t, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t486689021 *)__this, (int32_t)5, (int32_t)L_11, (bool)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0043:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.Networking.SyncList`1<System.Boolean>::GetEnumerator()
extern "C"  Il2CppObject* SyncList_1_GetEnumerator_m2887830198_gshared (SyncList_1_t486689021 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		List_1_t3194695850 * L_0 = (List_1_t3194695850 *)__this->get_m_Objects_0();
		NullCheck((List_1_t3194695850 *)L_0);
		Enumerator_t2729425524  L_1 = ((  Enumerator_t2729425524  (*) (List_1_t3194695850 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t3194695850 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		Enumerator_t2729425524  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), &L_2);
		V_0 = (Il2CppObject*)L_3;
		goto IL_0017;
	}

IL_0017:
	{
		Il2CppObject* L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator UnityEngine.Networking.SyncList`1<System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SyncList_1_System_Collections_IEnumerable_GetEnumerator_m102818917_gshared (SyncList_1_t486689021 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((SyncList_1_t486689021 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (SyncList_1_t486689021 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((SyncList_1_t486689021 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		V_0 = (Il2CppObject *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::.ctor()
extern "C"  void SyncList_1__ctor_m1617538870_gshared (SyncList_1_t3027959047 * __this, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_Objects_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Networking.SyncList`1<System.Int32>::get_Count()
extern "C"  int32_t SyncList_1_get_Count_m674961814_gshared (SyncList_1_t3027959047 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1440998580 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t1440998580 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Int32>::get_IsReadOnly()
extern "C"  bool SyncList_1_get_IsReadOnly_m1888710563_gshared (SyncList_1_t3027959047 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		goto IL_0008;
	}

IL_0008:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Networking.SyncList`1/SyncListChanged<T> UnityEngine.Networking.SyncList`1<System.Int32>::get_Callback()
extern "C"  SyncListChanged_t438147898 * SyncList_1_get_Callback_m1988320818_gshared (SyncList_1_t3027959047 * __this, const MethodInfo* method)
{
	SyncListChanged_t438147898 * V_0 = NULL;
	{
		SyncListChanged_t438147898 * L_0 = (SyncListChanged_t438147898 *)__this->get_m_Callback_3();
		V_0 = (SyncListChanged_t438147898 *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		SyncListChanged_t438147898 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::set_Callback(UnityEngine.Networking.SyncList`1/SyncListChanged<T>)
extern "C"  void SyncList_1_set_Callback_m2413288163_gshared (SyncList_1_t3027959047 * __this, SyncListChanged_t438147898 * ___value0, const MethodInfo* method)
{
	{
		SyncListChanged_t438147898 * L_0 = ___value0;
		__this->set_m_Callback_3(L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::InitializeBehaviour(UnityEngine.Networking.NetworkBehaviour,System.Int32)
extern "C"  void SyncList_1_InitializeBehaviour_m3248485582_gshared (SyncList_1_t3027959047 * __this, NetworkBehaviour_t3873055601 * ___beh0, int32_t ___cmdHash1, const MethodInfo* method)
{
	{
		NetworkBehaviour_t3873055601 * L_0 = ___beh0;
		__this->set_m_Behaviour_1(L_0);
		int32_t L_1 = ___cmdHash1;
		__this->set_m_CmdHash_2(L_1);
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,T)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* LogFilter_t2612395354_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkWriter_t560143343_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkServer_t3779449791_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2028122062;
extern Il2CppCodeGenString* _stringLiteral2863456972;
extern const uint32_t SyncList_1_SendMsg_m4213084229_MetadataUsageId;
extern "C"  void SyncList_1_SendMsg_m4213084229_gshared (SyncList_1_t3027959047 * __this, int32_t ___op0, int32_t ___itemIndex1, int32_t ___item2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncList_1_SendMsg_m4213084229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NetworkIdentity_t1766639790 * V_0 = NULL;
	NetworkWriter_t560143343 * V_1 = NULL;
	{
		NetworkBehaviour_t3873055601 * L_0 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogFilter_t2612395354_il2cpp_TypeInfo_var);
		bool L_2 = LogFilter_get_logError_m3315740406(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2028122062, /*hidden argument*/NULL);
	}

IL_0029:
	{
		goto IL_0107;
	}

IL_002e:
	{
		NetworkBehaviour_t3873055601 * L_3 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((Component_t3819376471 *)L_3);
		NetworkIdentity_t1766639790 * L_4 = Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169((Component_t3819376471 *)L_3, /*hidden argument*/Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169_MethodInfo_var);
		V_0 = (NetworkIdentity_t1766639790 *)L_4;
		NetworkIdentity_t1766639790 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogFilter_t2612395354_il2cpp_TypeInfo_var);
		bool L_7 = LogFilter_get_logError_m3315740406(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2863456972, /*hidden argument*/NULL);
	}

IL_005d:
	{
		goto IL_0107;
	}

IL_0062:
	{
		NetworkIdentity_t1766639790 * L_8 = V_0;
		NullCheck((NetworkIdentity_t1766639790 *)L_8);
		bool L_9 = NetworkIdentity_get_isServer_m1468412663((NetworkIdentity_t1766639790 *)L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0073;
		}
	}
	{
		goto IL_0107;
	}

IL_0073:
	{
		NetworkWriter_t560143343 * L_10 = (NetworkWriter_t560143343 *)il2cpp_codegen_object_new(NetworkWriter_t560143343_il2cpp_TypeInfo_var);
		NetworkWriter__ctor_m2809346428(L_10, /*hidden argument*/NULL);
		V_1 = (NetworkWriter_t560143343 *)L_10;
		NetworkWriter_t560143343 * L_11 = V_1;
		NullCheck((NetworkWriter_t560143343 *)L_11);
		NetworkWriter_StartMessage_m931021002((NetworkWriter_t560143343 *)L_11, (int16_t)((int32_t)9), /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_12 = V_1;
		NetworkIdentity_t1766639790 * L_13 = V_0;
		NullCheck((NetworkIdentity_t1766639790 *)L_13);
		NetworkInstanceId_t33998832  L_14 = NetworkIdentity_get_netId_m1164915858((NetworkIdentity_t1766639790 *)L_13, /*hidden argument*/NULL);
		NullCheck((NetworkWriter_t560143343 *)L_12);
		NetworkWriter_Write_m688344706((NetworkWriter_t560143343 *)L_12, (NetworkInstanceId_t33998832 )L_14, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_15 = V_1;
		int32_t L_16 = (int32_t)__this->get_m_CmdHash_2();
		NullCheck((NetworkWriter_t560143343 *)L_15);
		NetworkWriter_WritePackedUInt32_m2634533176((NetworkWriter_t560143343 *)L_15, (uint32_t)L_16, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_17 = V_1;
		int32_t L_18 = ___op0;
		NullCheck((NetworkWriter_t560143343 *)L_17);
		NetworkWriter_Write_m1305006738((NetworkWriter_t560143343 *)L_17, (uint8_t)(((int32_t)((uint8_t)L_18))), /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_19 = V_1;
		int32_t L_20 = ___itemIndex1;
		NullCheck((NetworkWriter_t560143343 *)L_19);
		NetworkWriter_WritePackedUInt32_m2634533176((NetworkWriter_t560143343 *)L_19, (uint32_t)L_20, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_21 = V_1;
		int32_t L_22 = ___item2;
		NullCheck((SyncList_1_t3027959047 *)__this);
		VirtActionInvoker2< NetworkWriter_t560143343 *, int32_t >::Invoke(18 /* System.Void UnityEngine.Networking.SyncList`1<System.Int32>::SerializeItem(UnityEngine.Networking.NetworkWriter,T) */, (SyncList_1_t3027959047 *)__this, (NetworkWriter_t560143343 *)L_21, (int32_t)L_22);
		NetworkWriter_t560143343 * L_23 = V_1;
		NullCheck((NetworkWriter_t560143343 *)L_23);
		NetworkWriter_FinishMessage_m3604322206((NetworkWriter_t560143343 *)L_23, /*hidden argument*/NULL);
		NetworkIdentity_t1766639790 * L_24 = V_0;
		NullCheck((Component_t3819376471 *)L_24);
		GameObject_t1756533147 * L_25 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_24, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_26 = V_1;
		NetworkBehaviour_t3873055601 * L_27 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 UnityEngine.Networking.NetworkBehaviour::GetNetworkChannel() */, (NetworkBehaviour_t3873055601 *)L_27);
		IL2CPP_RUNTIME_CLASS_INIT(NetworkServer_t3779449791_il2cpp_TypeInfo_var);
		NetworkServer_SendWriterToReady_m3464733118(NULL /*static, unused*/, (GameObject_t1756533147 *)L_25, (NetworkWriter_t560143343 *)L_26, (int32_t)L_28, /*hidden argument*/NULL);
		NetworkBehaviour_t3873055601 * L_29 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_29);
		bool L_30 = NetworkBehaviour_get_isServer_m3866369296((NetworkBehaviour_t3873055601 *)L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0107;
		}
	}
	{
		NetworkBehaviour_t3873055601 * L_31 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_31);
		bool L_32 = NetworkBehaviour_get_isClient_m3491347612((NetworkBehaviour_t3873055601 *)L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0107;
		}
	}
	{
		SyncListChanged_t438147898 * L_33 = (SyncListChanged_t438147898 *)__this->get_m_Callback_3();
		if (!L_33)
		{
			goto IL_0107;
		}
	}
	{
		SyncListChanged_t438147898 * L_34 = (SyncListChanged_t438147898 *)__this->get_m_Callback_3();
		int32_t L_35 = ___op0;
		int32_t L_36 = ___itemIndex1;
		NullCheck((SyncListChanged_t438147898 *)L_34);
		((  void (*) (SyncListChanged_t438147898 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((SyncListChanged_t438147898 *)L_34, (int32_t)L_35, (int32_t)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_0107:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t SyncList_1_SendMsg_m2507854557_MetadataUsageId;
extern "C"  void SyncList_1_SendMsg_m2507854557_gshared (SyncList_1_t3027959047 * __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncList_1_SendMsg_m2507854557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___op0;
		int32_t L_1 = ___itemIndex1;
		Initobj (Int32_t2071877448_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		NullCheck((SyncList_1_t3027959047 *)__this);
		((  void (*) (SyncList_1_t3027959047 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3027959047 *)__this, (int32_t)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::HandleMsg(UnityEngine.Networking.NetworkReader)
extern "C"  void SyncList_1_HandleMsg_m1811372009_gshared (SyncList_1_t3027959047 * __this, NetworkReader_t3187690923 * ___reader0, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		NetworkReader_t3187690923 * L_0 = ___reader0;
		NullCheck((NetworkReader_t3187690923 *)L_0);
		uint8_t L_1 = NetworkReader_ReadByte_m3670356752((NetworkReader_t3187690923 *)L_0, /*hidden argument*/NULL);
		V_0 = (uint8_t)L_1;
		NetworkReader_t3187690923 * L_2 = ___reader0;
		NullCheck((NetworkReader_t3187690923 *)L_2);
		uint32_t L_3 = NetworkReader_ReadPackedUInt32_m3078633978((NetworkReader_t3187690923 *)L_2, /*hidden argument*/NULL);
		V_1 = (int32_t)L_3;
		NetworkReader_t3187690923 * L_4 = ___reader0;
		NullCheck((SyncList_1_t3027959047 *)__this);
		int32_t L_5 = VirtFuncInvoker1< int32_t, NetworkReader_t3187690923 * >::Invoke(19 /* T UnityEngine.Networking.SyncList`1<System.Int32>::DeserializeItem(UnityEngine.Networking.NetworkReader) */, (SyncList_1_t3027959047 *)__this, (NetworkReader_t3187690923 *)L_4);
		V_2 = (int32_t)L_5;
		uint8_t L_6 = V_0;
		V_3 = (int32_t)L_6;
		int32_t L_7 = V_3;
		if (L_7 == 0)
		{
			goto IL_0040;
		}
		if (L_7 == 1)
		{
			goto IL_0051;
		}
		if (L_7 == 2)
		{
			goto IL_0061;
		}
		if (L_7 == 3)
		{
			goto IL_0073;
		}
		if (L_7 == 4)
		{
			goto IL_0085;
		}
		if (L_7 == 5)
		{
			goto IL_0096;
		}
		if (L_7 == 6)
		{
			goto IL_0096;
		}
	}
	{
		goto IL_00a8;
	}

IL_0040:
	{
		List_1_t1440998580 * L_8 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_9 = V_2;
		NullCheck((List_1_t1440998580 *)L_8);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1440998580 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		goto IL_00a8;
	}

IL_0051:
	{
		List_1_t1440998580 * L_10 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1440998580 *)L_10);
		((  void (*) (List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t1440998580 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		goto IL_00a8;
	}

IL_0061:
	{
		List_1_t1440998580 * L_11 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_12 = V_1;
		int32_t L_13 = V_2;
		NullCheck((List_1_t1440998580 *)L_11);
		((  void (*) (List_1_t1440998580 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1440998580 *)L_11, (int32_t)L_12, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		goto IL_00a8;
	}

IL_0073:
	{
		List_1_t1440998580 * L_14 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_15 = V_2;
		NullCheck((List_1_t1440998580 *)L_14);
		((  bool (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1440998580 *)L_14, (int32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_00a8;
	}

IL_0085:
	{
		List_1_t1440998580 * L_16 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_17 = V_1;
		NullCheck((List_1_t1440998580 *)L_16);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1440998580 *)L_16, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		goto IL_00a8;
	}

IL_0096:
	{
		List_1_t1440998580 * L_18 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_19 = V_1;
		int32_t L_20 = V_2;
		NullCheck((List_1_t1440998580 *)L_18);
		((  void (*) (List_1_t1440998580 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1440998580 *)L_18, (int32_t)L_19, (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		goto IL_00a8;
	}

IL_00a8:
	{
		SyncListChanged_t438147898 * L_21 = (SyncListChanged_t438147898 *)__this->get_m_Callback_3();
		if (!L_21)
		{
			goto IL_00c2;
		}
	}
	{
		SyncListChanged_t438147898 * L_22 = (SyncListChanged_t438147898 *)__this->get_m_Callback_3();
		uint8_t L_23 = V_0;
		int32_t L_24 = V_1;
		NullCheck((SyncListChanged_t438147898 *)L_22);
		((  void (*) (SyncListChanged_t438147898 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((SyncListChanged_t438147898 *)L_22, (int32_t)L_23, (int32_t)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_00c2:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::AddInternal(T)
extern "C"  void SyncList_1_AddInternal_m3520370070_gshared (SyncList_1_t3027959047 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_1 = ___item0;
		NullCheck((List_1_t1440998580 *)L_0);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1440998580 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::Add(T)
extern "C"  void SyncList_1_Add_m983006667_gshared (SyncList_1_t3027959047 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_1 = ___item0;
		NullCheck((List_1_t1440998580 *)L_0);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1440998580 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		List_1_t1440998580 * L_2 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1440998580 *)L_2);
		int32_t L_3 = ((  int32_t (*) (List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t1440998580 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		int32_t L_4 = ___item0;
		NullCheck((SyncList_1_t3027959047 *)__this);
		((  void (*) (SyncList_1_t3027959047 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3027959047 *)__this, (int32_t)0, (int32_t)((int32_t)((int32_t)L_3-(int32_t)1)), (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::Clear()
extern "C"  void SyncList_1_Clear_m4207226335_gshared (SyncList_1_t3027959047 * __this, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1440998580 *)L_0);
		((  void (*) (List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t1440998580 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((SyncList_1_t3027959047 *)__this);
		((  void (*) (SyncList_1_t3027959047 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((SyncList_1_t3027959047 *)__this, (int32_t)1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Int32>::Contains(T)
extern "C"  bool SyncList_1_Contains_m3476989245_gshared (SyncList_1_t3027959047 * __this, int32_t ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_1 = ___item0;
		NullCheck((List_1_t1440998580 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1440998580 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (bool)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C"  void SyncList_1_CopyTo_m2328597255_gshared (SyncList_1_t3027959047 * __this, Int32U5BU5D_t3030399641* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		Int32U5BU5D_t3030399641* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((List_1_t1440998580 *)L_0);
		((  void (*) (List_1_t1440998580 *, Int32U5BU5D_t3030399641*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t1440998580 *)L_0, (Int32U5BU5D_t3030399641*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return;
	}
}
// System.Int32 UnityEngine.Networking.SyncList`1<System.Int32>::IndexOf(T)
extern "C"  int32_t SyncList_1_IndexOf_m4188021131_gshared (SyncList_1_t3027959047 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_1 = ___item0;
		NullCheck((List_1_t1440998580 *)L_0);
		int32_t L_2 = ((  int32_t (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t1440998580 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (int32_t)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::Insert(System.Int32,T)
extern "C"  void SyncList_1_Insert_m669348072_gshared (SyncList_1_t3027959047 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_1 = ___index0;
		int32_t L_2 = ___item1;
		NullCheck((List_1_t1440998580 *)L_0);
		((  void (*) (List_1_t1440998580 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1440998580 *)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		int32_t L_3 = ___index0;
		int32_t L_4 = ___item1;
		NullCheck((SyncList_1_t3027959047 *)__this);
		((  void (*) (SyncList_1_t3027959047 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3027959047 *)__this, (int32_t)2, (int32_t)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Int32>::Remove(T)
extern "C"  bool SyncList_1_Remove_m3307296690_gshared (SyncList_1_t3027959047 * __this, int32_t ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_1 = ___item0;
		NullCheck((List_1_t1440998580 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1440998580 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_0 = (bool)L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_4 = ___item0;
		NullCheck((SyncList_1_t3027959047 *)__this);
		((  void (*) (SyncList_1_t3027959047 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3027959047 *)__this, (int32_t)3, (int32_t)0, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_001f:
	{
		bool L_5 = V_0;
		V_1 = (bool)L_5;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::RemoveAt(System.Int32)
extern "C"  void SyncList_1_RemoveAt_m3057230892_gshared (SyncList_1_t3027959047 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t1440998580 *)L_0);
		((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1440998580 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		int32_t L_2 = ___index0;
		NullCheck((SyncList_1_t3027959047 *)__this);
		((  void (*) (SyncList_1_t3027959047 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((SyncList_1_t3027959047 *)__this, (int32_t)4, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::Dirty(System.Int32)
extern "C"  void SyncList_1_Dirty_m3348978025_gshared (SyncList_1_t3027959047 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		List_1_t1440998580 * L_1 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_2 = ___index0;
		NullCheck((List_1_t1440998580 *)L_1);
		int32_t L_3 = ((  int32_t (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1440998580 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		NullCheck((SyncList_1_t3027959047 *)__this);
		((  void (*) (SyncList_1_t3027959047 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3027959047 *)__this, (int32_t)6, (int32_t)L_0, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// T UnityEngine.Networking.SyncList`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t SyncList_1_get_Item_m3721680368_gshared (SyncList_1_t3027959047 * __this, int32_t ___i0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_1 = ___i0;
		NullCheck((List_1_t1440998580 *)L_0);
		int32_t L_2 = ((  int32_t (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1440998580 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_0 = (int32_t)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Int32>::set_Item(System.Int32,T)
extern "C"  void SyncList_1_set_Item_m1912747907_gshared (SyncList_1_t3027959047 * __this, int32_t ___i0, int32_t ___value1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_1 = ___i0;
		NullCheck((List_1_t1440998580 *)L_0);
		int32_t L_2 = ((  int32_t (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1440998580 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_1 = (int32_t)L_2;
		int32_t L_3 = ___value1;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18), &L_4);
		bool L_6 = Int32_Equals_m753832628((int32_t*)(&V_1), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		List_1_t1440998580 * L_7 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		int32_t L_8 = ___i0;
		int32_t L_9 = ___value1;
		NullCheck((List_1_t1440998580 *)L_7);
		((  void (*) (List_1_t1440998580 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1440998580 *)L_7, (int32_t)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_11 = ___i0;
		int32_t L_12 = ___value1;
		NullCheck((SyncList_1_t3027959047 *)__this);
		((  void (*) (SyncList_1_t3027959047 *, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3027959047 *)__this, (int32_t)5, (int32_t)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0043:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.Networking.SyncList`1<System.Int32>::GetEnumerator()
extern "C"  Il2CppObject* SyncList_1_GetEnumerator_m3342809466_gshared (SyncList_1_t3027959047 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1440998580 *)L_0);
		Enumerator_t975728254  L_1 = ((  Enumerator_t975728254  (*) (List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t1440998580 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		Enumerator_t975728254  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), &L_2);
		V_0 = (Il2CppObject*)L_3;
		goto IL_0017;
	}

IL_0017:
	{
		Il2CppObject* L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator UnityEngine.Networking.SyncList`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SyncList_1_System_Collections_IEnumerable_GetEnumerator_m3585423471_gshared (SyncList_1_t3027959047 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((SyncList_1_t3027959047 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (SyncList_1_t3027959047 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((SyncList_1_t3027959047 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		V_0 = (Il2CppObject *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::.ctor()
extern "C"  void SyncList_1__ctor_m1979817233_gshared (SyncList_1_t3645530894 * __this, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_Objects_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Networking.SyncList`1<System.Object>::get_Count()
extern "C"  int32_t SyncList_1_get_Count_m3995922617_gshared (SyncList_1_t3645530894 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		NullCheck((List_1_t2058570427 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t2058570427 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Object>::get_IsReadOnly()
extern "C"  bool SyncList_1_get_IsReadOnly_m1739608972_gshared (SyncList_1_t3645530894 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		goto IL_0008;
	}

IL_0008:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Networking.SyncList`1/SyncListChanged<T> UnityEngine.Networking.SyncList`1<System.Object>::get_Callback()
extern "C"  SyncListChanged_t1055719745 * SyncList_1_get_Callback_m3756815995_gshared (SyncList_1_t3645530894 * __this, const MethodInfo* method)
{
	SyncListChanged_t1055719745 * V_0 = NULL;
	{
		SyncListChanged_t1055719745 * L_0 = (SyncListChanged_t1055719745 *)__this->get_m_Callback_3();
		V_0 = (SyncListChanged_t1055719745 *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		SyncListChanged_t1055719745 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::set_Callback(UnityEngine.Networking.SyncList`1/SyncListChanged<T>)
extern "C"  void SyncList_1_set_Callback_m3071308488_gshared (SyncList_1_t3645530894 * __this, SyncListChanged_t1055719745 * ___value0, const MethodInfo* method)
{
	{
		SyncListChanged_t1055719745 * L_0 = ___value0;
		__this->set_m_Callback_3(L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::InitializeBehaviour(UnityEngine.Networking.NetworkBehaviour,System.Int32)
extern "C"  void SyncList_1_InitializeBehaviour_m3472596089_gshared (SyncList_1_t3645530894 * __this, NetworkBehaviour_t3873055601 * ___beh0, int32_t ___cmdHash1, const MethodInfo* method)
{
	{
		NetworkBehaviour_t3873055601 * L_0 = ___beh0;
		__this->set_m_Behaviour_1(L_0);
		int32_t L_1 = ___cmdHash1;
		__this->set_m_CmdHash_2(L_1);
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,T)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* LogFilter_t2612395354_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkWriter_t560143343_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkServer_t3779449791_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2028122062;
extern Il2CppCodeGenString* _stringLiteral2863456972;
extern const uint32_t SyncList_1_SendMsg_m1310025432_MetadataUsageId;
extern "C"  void SyncList_1_SendMsg_m1310025432_gshared (SyncList_1_t3645530894 * __this, int32_t ___op0, int32_t ___itemIndex1, Il2CppObject * ___item2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncList_1_SendMsg_m1310025432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NetworkIdentity_t1766639790 * V_0 = NULL;
	NetworkWriter_t560143343 * V_1 = NULL;
	{
		NetworkBehaviour_t3873055601 * L_0 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogFilter_t2612395354_il2cpp_TypeInfo_var);
		bool L_2 = LogFilter_get_logError_m3315740406(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2028122062, /*hidden argument*/NULL);
	}

IL_0029:
	{
		goto IL_0107;
	}

IL_002e:
	{
		NetworkBehaviour_t3873055601 * L_3 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((Component_t3819376471 *)L_3);
		NetworkIdentity_t1766639790 * L_4 = Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169((Component_t3819376471 *)L_3, /*hidden argument*/Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169_MethodInfo_var);
		V_0 = (NetworkIdentity_t1766639790 *)L_4;
		NetworkIdentity_t1766639790 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogFilter_t2612395354_il2cpp_TypeInfo_var);
		bool L_7 = LogFilter_get_logError_m3315740406(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2863456972, /*hidden argument*/NULL);
	}

IL_005d:
	{
		goto IL_0107;
	}

IL_0062:
	{
		NetworkIdentity_t1766639790 * L_8 = V_0;
		NullCheck((NetworkIdentity_t1766639790 *)L_8);
		bool L_9 = NetworkIdentity_get_isServer_m1468412663((NetworkIdentity_t1766639790 *)L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0073;
		}
	}
	{
		goto IL_0107;
	}

IL_0073:
	{
		NetworkWriter_t560143343 * L_10 = (NetworkWriter_t560143343 *)il2cpp_codegen_object_new(NetworkWriter_t560143343_il2cpp_TypeInfo_var);
		NetworkWriter__ctor_m2809346428(L_10, /*hidden argument*/NULL);
		V_1 = (NetworkWriter_t560143343 *)L_10;
		NetworkWriter_t560143343 * L_11 = V_1;
		NullCheck((NetworkWriter_t560143343 *)L_11);
		NetworkWriter_StartMessage_m931021002((NetworkWriter_t560143343 *)L_11, (int16_t)((int32_t)9), /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_12 = V_1;
		NetworkIdentity_t1766639790 * L_13 = V_0;
		NullCheck((NetworkIdentity_t1766639790 *)L_13);
		NetworkInstanceId_t33998832  L_14 = NetworkIdentity_get_netId_m1164915858((NetworkIdentity_t1766639790 *)L_13, /*hidden argument*/NULL);
		NullCheck((NetworkWriter_t560143343 *)L_12);
		NetworkWriter_Write_m688344706((NetworkWriter_t560143343 *)L_12, (NetworkInstanceId_t33998832 )L_14, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_15 = V_1;
		int32_t L_16 = (int32_t)__this->get_m_CmdHash_2();
		NullCheck((NetworkWriter_t560143343 *)L_15);
		NetworkWriter_WritePackedUInt32_m2634533176((NetworkWriter_t560143343 *)L_15, (uint32_t)L_16, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_17 = V_1;
		int32_t L_18 = ___op0;
		NullCheck((NetworkWriter_t560143343 *)L_17);
		NetworkWriter_Write_m1305006738((NetworkWriter_t560143343 *)L_17, (uint8_t)(((int32_t)((uint8_t)L_18))), /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_19 = V_1;
		int32_t L_20 = ___itemIndex1;
		NullCheck((NetworkWriter_t560143343 *)L_19);
		NetworkWriter_WritePackedUInt32_m2634533176((NetworkWriter_t560143343 *)L_19, (uint32_t)L_20, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_21 = V_1;
		Il2CppObject * L_22 = ___item2;
		NullCheck((SyncList_1_t3645530894 *)__this);
		VirtActionInvoker2< NetworkWriter_t560143343 *, Il2CppObject * >::Invoke(18 /* System.Void UnityEngine.Networking.SyncList`1<System.Object>::SerializeItem(UnityEngine.Networking.NetworkWriter,T) */, (SyncList_1_t3645530894 *)__this, (NetworkWriter_t560143343 *)L_21, (Il2CppObject *)L_22);
		NetworkWriter_t560143343 * L_23 = V_1;
		NullCheck((NetworkWriter_t560143343 *)L_23);
		NetworkWriter_FinishMessage_m3604322206((NetworkWriter_t560143343 *)L_23, /*hidden argument*/NULL);
		NetworkIdentity_t1766639790 * L_24 = V_0;
		NullCheck((Component_t3819376471 *)L_24);
		GameObject_t1756533147 * L_25 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_24, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_26 = V_1;
		NetworkBehaviour_t3873055601 * L_27 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 UnityEngine.Networking.NetworkBehaviour::GetNetworkChannel() */, (NetworkBehaviour_t3873055601 *)L_27);
		IL2CPP_RUNTIME_CLASS_INIT(NetworkServer_t3779449791_il2cpp_TypeInfo_var);
		NetworkServer_SendWriterToReady_m3464733118(NULL /*static, unused*/, (GameObject_t1756533147 *)L_25, (NetworkWriter_t560143343 *)L_26, (int32_t)L_28, /*hidden argument*/NULL);
		NetworkBehaviour_t3873055601 * L_29 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_29);
		bool L_30 = NetworkBehaviour_get_isServer_m3866369296((NetworkBehaviour_t3873055601 *)L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0107;
		}
	}
	{
		NetworkBehaviour_t3873055601 * L_31 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_31);
		bool L_32 = NetworkBehaviour_get_isClient_m3491347612((NetworkBehaviour_t3873055601 *)L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0107;
		}
	}
	{
		SyncListChanged_t1055719745 * L_33 = (SyncListChanged_t1055719745 *)__this->get_m_Callback_3();
		if (!L_33)
		{
			goto IL_0107;
		}
	}
	{
		SyncListChanged_t1055719745 * L_34 = (SyncListChanged_t1055719745 *)__this->get_m_Callback_3();
		int32_t L_35 = ___op0;
		int32_t L_36 = ___itemIndex1;
		NullCheck((SyncListChanged_t1055719745 *)L_34);
		((  void (*) (SyncListChanged_t1055719745 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((SyncListChanged_t1055719745 *)L_34, (int32_t)L_35, (int32_t)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_0107:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t SyncList_1_SendMsg_m3027456264_MetadataUsageId;
extern "C"  void SyncList_1_SendMsg_m3027456264_gshared (SyncList_1_t3645530894 * __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncList_1_SendMsg_m3027456264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___op0;
		int32_t L_1 = ___itemIndex1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_2 = V_0;
		NullCheck((SyncList_1_t3645530894 *)__this);
		((  void (*) (SyncList_1_t3645530894 *, int32_t, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3645530894 *)__this, (int32_t)L_0, (int32_t)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::HandleMsg(UnityEngine.Networking.NetworkReader)
extern "C"  void SyncList_1_HandleMsg_m1047790890_gshared (SyncList_1_t3645530894 * __this, NetworkReader_t3187690923 * ___reader0, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	int32_t V_3 = 0;
	{
		NetworkReader_t3187690923 * L_0 = ___reader0;
		NullCheck((NetworkReader_t3187690923 *)L_0);
		uint8_t L_1 = NetworkReader_ReadByte_m3670356752((NetworkReader_t3187690923 *)L_0, /*hidden argument*/NULL);
		V_0 = (uint8_t)L_1;
		NetworkReader_t3187690923 * L_2 = ___reader0;
		NullCheck((NetworkReader_t3187690923 *)L_2);
		uint32_t L_3 = NetworkReader_ReadPackedUInt32_m3078633978((NetworkReader_t3187690923 *)L_2, /*hidden argument*/NULL);
		V_1 = (int32_t)L_3;
		NetworkReader_t3187690923 * L_4 = ___reader0;
		NullCheck((SyncList_1_t3645530894 *)__this);
		Il2CppObject * L_5 = VirtFuncInvoker1< Il2CppObject *, NetworkReader_t3187690923 * >::Invoke(19 /* T UnityEngine.Networking.SyncList`1<System.Object>::DeserializeItem(UnityEngine.Networking.NetworkReader) */, (SyncList_1_t3645530894 *)__this, (NetworkReader_t3187690923 *)L_4);
		V_2 = (Il2CppObject *)L_5;
		uint8_t L_6 = V_0;
		V_3 = (int32_t)L_6;
		int32_t L_7 = V_3;
		if (L_7 == 0)
		{
			goto IL_0040;
		}
		if (L_7 == 1)
		{
			goto IL_0051;
		}
		if (L_7 == 2)
		{
			goto IL_0061;
		}
		if (L_7 == 3)
		{
			goto IL_0073;
		}
		if (L_7 == 4)
		{
			goto IL_0085;
		}
		if (L_7 == 5)
		{
			goto IL_0096;
		}
		if (L_7 == 6)
		{
			goto IL_0096;
		}
	}
	{
		goto IL_00a8;
	}

IL_0040:
	{
		List_1_t2058570427 * L_8 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		Il2CppObject * L_9 = V_2;
		NullCheck((List_1_t2058570427 *)L_8);
		((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2058570427 *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		goto IL_00a8;
	}

IL_0051:
	{
		List_1_t2058570427 * L_10 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		NullCheck((List_1_t2058570427 *)L_10);
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t2058570427 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		goto IL_00a8;
	}

IL_0061:
	{
		List_1_t2058570427 * L_11 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		int32_t L_12 = V_1;
		Il2CppObject * L_13 = V_2;
		NullCheck((List_1_t2058570427 *)L_11);
		((  void (*) (List_1_t2058570427 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t2058570427 *)L_11, (int32_t)L_12, (Il2CppObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		goto IL_00a8;
	}

IL_0073:
	{
		List_1_t2058570427 * L_14 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		Il2CppObject * L_15 = V_2;
		NullCheck((List_1_t2058570427 *)L_14);
		((  bool (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t2058570427 *)L_14, (Il2CppObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_00a8;
	}

IL_0085:
	{
		List_1_t2058570427 * L_16 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		int32_t L_17 = V_1;
		NullCheck((List_1_t2058570427 *)L_16);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2058570427 *)L_16, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		goto IL_00a8;
	}

IL_0096:
	{
		List_1_t2058570427 * L_18 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		int32_t L_19 = V_1;
		Il2CppObject * L_20 = V_2;
		NullCheck((List_1_t2058570427 *)L_18);
		((  void (*) (List_1_t2058570427 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2058570427 *)L_18, (int32_t)L_19, (Il2CppObject *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		goto IL_00a8;
	}

IL_00a8:
	{
		SyncListChanged_t1055719745 * L_21 = (SyncListChanged_t1055719745 *)__this->get_m_Callback_3();
		if (!L_21)
		{
			goto IL_00c2;
		}
	}
	{
		SyncListChanged_t1055719745 * L_22 = (SyncListChanged_t1055719745 *)__this->get_m_Callback_3();
		uint8_t L_23 = V_0;
		int32_t L_24 = V_1;
		NullCheck((SyncListChanged_t1055719745 *)L_22);
		((  void (*) (SyncListChanged_t1055719745 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((SyncListChanged_t1055719745 *)L_22, (int32_t)L_23, (int32_t)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_00c2:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::AddInternal(T)
extern "C"  void SyncList_1_AddInternal_m1156564503_gshared (SyncList_1_t3645530894 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2058570427 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::Add(T)
extern "C"  void SyncList_1_Add_m903203842_gshared (SyncList_1_t3645530894 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t2058570427 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		NullCheck((List_1_t2058570427 *)L_2);
		int32_t L_3 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t2058570427 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		Il2CppObject * L_4 = ___item0;
		NullCheck((SyncList_1_t3645530894 *)__this);
		((  void (*) (SyncList_1_t3645530894 *, int32_t, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3645530894 *)__this, (int32_t)0, (int32_t)((int32_t)((int32_t)L_3-(int32_t)1)), (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::Clear()
extern "C"  void SyncList_1_Clear_m479214166_gshared (SyncList_1_t3645530894 * __this, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t2058570427 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((SyncList_1_t3645530894 *)__this);
		((  void (*) (SyncList_1_t3645530894 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((SyncList_1_t3645530894 *)__this, (int32_t)1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Object>::Contains(T)
extern "C"  bool SyncList_1_Contains_m2184498664_gshared (SyncList_1_t3645530894 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((List_1_t2058570427 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t2058570427 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (bool)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void SyncList_1_CopyTo_m1104313626_gshared (SyncList_1_t3645530894 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t2058570427 *)L_0, (ObjectU5BU5D_t3614634134*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return;
	}
}
// System.Int32 UnityEngine.Networking.SyncList`1<System.Object>::IndexOf(T)
extern "C"  int32_t SyncList_1_IndexOf_m2624123316_gshared (SyncList_1_t3645530894 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((List_1_t2058570427 *)L_0);
		int32_t L_2 = ((  int32_t (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t2058570427 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (int32_t)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void SyncList_1_Insert_m4109060535_gshared (SyncList_1_t3645530894 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		int32_t L_1 = ___index0;
		Il2CppObject * L_2 = ___item1;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t2058570427 *)L_0, (int32_t)L_1, (Il2CppObject *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		int32_t L_3 = ___index0;
		Il2CppObject * L_4 = ___item1;
		NullCheck((SyncList_1_t3645530894 *)__this);
		((  void (*) (SyncList_1_t3645530894 *, int32_t, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3645530894 *)__this, (int32_t)2, (int32_t)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Object>::Remove(T)
extern "C"  bool SyncList_1_Remove_m338076795_gshared (SyncList_1_t3645530894 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((List_1_t2058570427 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t2058570427 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_0 = (bool)L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_4 = ___item0;
		NullCheck((SyncList_1_t3645530894 *)__this);
		((  void (*) (SyncList_1_t3645530894 *, int32_t, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3645530894 *)__this, (int32_t)3, (int32_t)0, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_001f:
	{
		bool L_5 = V_0;
		V_1 = (bool)L_5;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void SyncList_1_RemoveAt_m20766587_gshared (SyncList_1_t3645530894 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2058570427 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		int32_t L_2 = ___index0;
		NullCheck((SyncList_1_t3645530894 *)__this);
		((  void (*) (SyncList_1_t3645530894 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((SyncList_1_t3645530894 *)__this, (int32_t)4, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::Dirty(System.Int32)
extern "C"  void SyncList_1_Dirty_m1369571806_gshared (SyncList_1_t3645530894 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		List_1_t2058570427 * L_1 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		int32_t L_2 = ___index0;
		NullCheck((List_1_t2058570427 *)L_1);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2058570427 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		NullCheck((SyncList_1_t3645530894 *)__this);
		((  void (*) (SyncList_1_t3645530894 *, int32_t, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3645530894 *)__this, (int32_t)6, (int32_t)L_0, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// T UnityEngine.Networking.SyncList`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * SyncList_1_get_Item_m172423547_gshared (SyncList_1_t3645530894 * __this, int32_t ___i0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		int32_t L_1 = ___i0;
		NullCheck((List_1_t2058570427 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2058570427 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_0 = (Il2CppObject *)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void SyncList_1_set_Item_m4107841040_gshared (SyncList_1_t3645530894 * __this, int32_t ___i0, Il2CppObject * ___value1, const MethodInfo* method)
{
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		int32_t L_1 = ___i0;
		NullCheck((List_1_t2058570427 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2058570427 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_1 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = ___value1;
		NullCheck((Il2CppObject *)(*(&V_1)));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&V_1)), (Il2CppObject *)L_3);
		V_0 = (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
		List_1_t2058570427 * L_5 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		int32_t L_6 = ___i0;
		Il2CppObject * L_7 = ___value1;
		NullCheck((List_1_t2058570427 *)L_5);
		((  void (*) (List_1_t2058570427 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t2058570427 *)L_5, (int32_t)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_9 = ___i0;
		Il2CppObject * L_10 = ___value1;
		NullCheck((SyncList_1_t3645530894 *)__this);
		((  void (*) (SyncList_1_t3645530894 *, int32_t, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3645530894 *)__this, (int32_t)5, (int32_t)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0043:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.Networking.SyncList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* SyncList_1_GetEnumerator_m1059025845_gshared (SyncList_1_t3645530894 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_Objects_0();
		NullCheck((List_1_t2058570427 *)L_0);
		Enumerator_t1593300101  L_1 = ((  Enumerator_t1593300101  (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t2058570427 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		Enumerator_t1593300101  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), &L_2);
		V_0 = (Il2CppObject*)L_3;
		goto IL_0017;
	}

IL_0017:
	{
		Il2CppObject* L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator UnityEngine.Networking.SyncList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SyncList_1_System_Collections_IEnumerable_GetEnumerator_m2736782484_gshared (SyncList_1_t3645530894 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((SyncList_1_t3645530894 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (SyncList_1_t3645530894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((SyncList_1_t3645530894 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		V_0 = (Il2CppObject *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::.ctor()
extern "C"  void SyncList_1__ctor_m939798964_gshared (SyncList_1_t3032591531 * __this, const MethodInfo* method)
{
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t1445631064 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_Objects_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Networking.SyncList`1<System.Single>::get_Count()
extern "C"  int32_t SyncList_1_get_Count_m1737387036_gshared (SyncList_1_t3032591531 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1445631064 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t1445631064 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t1445631064 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Single>::get_IsReadOnly()
extern "C"  bool SyncList_1_get_IsReadOnly_m2226582971_gshared (SyncList_1_t3032591531 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		goto IL_0008;
	}

IL_0008:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Networking.SyncList`1/SyncListChanged<T> UnityEngine.Networking.SyncList`1<System.Single>::get_Callback()
extern "C"  SyncListChanged_t442780382 * SyncList_1_get_Callback_m2721139684_gshared (SyncList_1_t3032591531 * __this, const MethodInfo* method)
{
	SyncListChanged_t442780382 * V_0 = NULL;
	{
		SyncListChanged_t442780382 * L_0 = (SyncListChanged_t442780382 *)__this->get_m_Callback_3();
		V_0 = (SyncListChanged_t442780382 *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		SyncListChanged_t442780382 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::set_Callback(UnityEngine.Networking.SyncList`1/SyncListChanged<T>)
extern "C"  void SyncList_1_set_Callback_m1813398075_gshared (SyncList_1_t3032591531 * __this, SyncListChanged_t442780382 * ___value0, const MethodInfo* method)
{
	{
		SyncListChanged_t442780382 * L_0 = ___value0;
		__this->set_m_Callback_3(L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::InitializeBehaviour(UnityEngine.Networking.NetworkBehaviour,System.Int32)
extern "C"  void SyncList_1_InitializeBehaviour_m1853065196_gshared (SyncList_1_t3032591531 * __this, NetworkBehaviour_t3873055601 * ___beh0, int32_t ___cmdHash1, const MethodInfo* method)
{
	{
		NetworkBehaviour_t3873055601 * L_0 = ___beh0;
		__this->set_m_Behaviour_1(L_0);
		int32_t L_1 = ___cmdHash1;
		__this->set_m_CmdHash_2(L_1);
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,T)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* LogFilter_t2612395354_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkWriter_t560143343_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkServer_t3779449791_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2028122062;
extern Il2CppCodeGenString* _stringLiteral2863456972;
extern const uint32_t SyncList_1_SendMsg_m1907044641_MetadataUsageId;
extern "C"  void SyncList_1_SendMsg_m1907044641_gshared (SyncList_1_t3032591531 * __this, int32_t ___op0, int32_t ___itemIndex1, float ___item2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncList_1_SendMsg_m1907044641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NetworkIdentity_t1766639790 * V_0 = NULL;
	NetworkWriter_t560143343 * V_1 = NULL;
	{
		NetworkBehaviour_t3873055601 * L_0 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogFilter_t2612395354_il2cpp_TypeInfo_var);
		bool L_2 = LogFilter_get_logError_m3315740406(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2028122062, /*hidden argument*/NULL);
	}

IL_0029:
	{
		goto IL_0107;
	}

IL_002e:
	{
		NetworkBehaviour_t3873055601 * L_3 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((Component_t3819376471 *)L_3);
		NetworkIdentity_t1766639790 * L_4 = Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169((Component_t3819376471 *)L_3, /*hidden argument*/Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169_MethodInfo_var);
		V_0 = (NetworkIdentity_t1766639790 *)L_4;
		NetworkIdentity_t1766639790 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogFilter_t2612395354_il2cpp_TypeInfo_var);
		bool L_7 = LogFilter_get_logError_m3315740406(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2863456972, /*hidden argument*/NULL);
	}

IL_005d:
	{
		goto IL_0107;
	}

IL_0062:
	{
		NetworkIdentity_t1766639790 * L_8 = V_0;
		NullCheck((NetworkIdentity_t1766639790 *)L_8);
		bool L_9 = NetworkIdentity_get_isServer_m1468412663((NetworkIdentity_t1766639790 *)L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0073;
		}
	}
	{
		goto IL_0107;
	}

IL_0073:
	{
		NetworkWriter_t560143343 * L_10 = (NetworkWriter_t560143343 *)il2cpp_codegen_object_new(NetworkWriter_t560143343_il2cpp_TypeInfo_var);
		NetworkWriter__ctor_m2809346428(L_10, /*hidden argument*/NULL);
		V_1 = (NetworkWriter_t560143343 *)L_10;
		NetworkWriter_t560143343 * L_11 = V_1;
		NullCheck((NetworkWriter_t560143343 *)L_11);
		NetworkWriter_StartMessage_m931021002((NetworkWriter_t560143343 *)L_11, (int16_t)((int32_t)9), /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_12 = V_1;
		NetworkIdentity_t1766639790 * L_13 = V_0;
		NullCheck((NetworkIdentity_t1766639790 *)L_13);
		NetworkInstanceId_t33998832  L_14 = NetworkIdentity_get_netId_m1164915858((NetworkIdentity_t1766639790 *)L_13, /*hidden argument*/NULL);
		NullCheck((NetworkWriter_t560143343 *)L_12);
		NetworkWriter_Write_m688344706((NetworkWriter_t560143343 *)L_12, (NetworkInstanceId_t33998832 )L_14, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_15 = V_1;
		int32_t L_16 = (int32_t)__this->get_m_CmdHash_2();
		NullCheck((NetworkWriter_t560143343 *)L_15);
		NetworkWriter_WritePackedUInt32_m2634533176((NetworkWriter_t560143343 *)L_15, (uint32_t)L_16, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_17 = V_1;
		int32_t L_18 = ___op0;
		NullCheck((NetworkWriter_t560143343 *)L_17);
		NetworkWriter_Write_m1305006738((NetworkWriter_t560143343 *)L_17, (uint8_t)(((int32_t)((uint8_t)L_18))), /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_19 = V_1;
		int32_t L_20 = ___itemIndex1;
		NullCheck((NetworkWriter_t560143343 *)L_19);
		NetworkWriter_WritePackedUInt32_m2634533176((NetworkWriter_t560143343 *)L_19, (uint32_t)L_20, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_21 = V_1;
		float L_22 = ___item2;
		NullCheck((SyncList_1_t3032591531 *)__this);
		VirtActionInvoker2< NetworkWriter_t560143343 *, float >::Invoke(18 /* System.Void UnityEngine.Networking.SyncList`1<System.Single>::SerializeItem(UnityEngine.Networking.NetworkWriter,T) */, (SyncList_1_t3032591531 *)__this, (NetworkWriter_t560143343 *)L_21, (float)L_22);
		NetworkWriter_t560143343 * L_23 = V_1;
		NullCheck((NetworkWriter_t560143343 *)L_23);
		NetworkWriter_FinishMessage_m3604322206((NetworkWriter_t560143343 *)L_23, /*hidden argument*/NULL);
		NetworkIdentity_t1766639790 * L_24 = V_0;
		NullCheck((Component_t3819376471 *)L_24);
		GameObject_t1756533147 * L_25 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_24, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_26 = V_1;
		NetworkBehaviour_t3873055601 * L_27 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 UnityEngine.Networking.NetworkBehaviour::GetNetworkChannel() */, (NetworkBehaviour_t3873055601 *)L_27);
		IL2CPP_RUNTIME_CLASS_INIT(NetworkServer_t3779449791_il2cpp_TypeInfo_var);
		NetworkServer_SendWriterToReady_m3464733118(NULL /*static, unused*/, (GameObject_t1756533147 *)L_25, (NetworkWriter_t560143343 *)L_26, (int32_t)L_28, /*hidden argument*/NULL);
		NetworkBehaviour_t3873055601 * L_29 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_29);
		bool L_30 = NetworkBehaviour_get_isServer_m3866369296((NetworkBehaviour_t3873055601 *)L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0107;
		}
	}
	{
		NetworkBehaviour_t3873055601 * L_31 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_31);
		bool L_32 = NetworkBehaviour_get_isClient_m3491347612((NetworkBehaviour_t3873055601 *)L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0107;
		}
	}
	{
		SyncListChanged_t442780382 * L_33 = (SyncListChanged_t442780382 *)__this->get_m_Callback_3();
		if (!L_33)
		{
			goto IL_0107;
		}
	}
	{
		SyncListChanged_t442780382 * L_34 = (SyncListChanged_t442780382 *)__this->get_m_Callback_3();
		int32_t L_35 = ___op0;
		int32_t L_36 = ___itemIndex1;
		NullCheck((SyncListChanged_t442780382 *)L_34);
		((  void (*) (SyncListChanged_t442780382 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((SyncListChanged_t442780382 *)L_34, (int32_t)L_35, (int32_t)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_0107:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t SyncList_1_SendMsg_m3349028233_MetadataUsageId;
extern "C"  void SyncList_1_SendMsg_m3349028233_gshared (SyncList_1_t3032591531 * __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncList_1_SendMsg_m3349028233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___op0;
		int32_t L_1 = ___itemIndex1;
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_2 = V_0;
		NullCheck((SyncList_1_t3032591531 *)__this);
		((  void (*) (SyncList_1_t3032591531 *, int32_t, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3032591531 *)__this, (int32_t)L_0, (int32_t)L_1, (float)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::HandleMsg(UnityEngine.Networking.NetworkReader)
extern "C"  void SyncList_1_HandleMsg_m4174949781_gshared (SyncList_1_t3032591531 * __this, NetworkReader_t3187690923 * ___reader0, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	{
		NetworkReader_t3187690923 * L_0 = ___reader0;
		NullCheck((NetworkReader_t3187690923 *)L_0);
		uint8_t L_1 = NetworkReader_ReadByte_m3670356752((NetworkReader_t3187690923 *)L_0, /*hidden argument*/NULL);
		V_0 = (uint8_t)L_1;
		NetworkReader_t3187690923 * L_2 = ___reader0;
		NullCheck((NetworkReader_t3187690923 *)L_2);
		uint32_t L_3 = NetworkReader_ReadPackedUInt32_m3078633978((NetworkReader_t3187690923 *)L_2, /*hidden argument*/NULL);
		V_1 = (int32_t)L_3;
		NetworkReader_t3187690923 * L_4 = ___reader0;
		NullCheck((SyncList_1_t3032591531 *)__this);
		float L_5 = VirtFuncInvoker1< float, NetworkReader_t3187690923 * >::Invoke(19 /* T UnityEngine.Networking.SyncList`1<System.Single>::DeserializeItem(UnityEngine.Networking.NetworkReader) */, (SyncList_1_t3032591531 *)__this, (NetworkReader_t3187690923 *)L_4);
		V_2 = (float)L_5;
		uint8_t L_6 = V_0;
		V_3 = (int32_t)L_6;
		int32_t L_7 = V_3;
		if (L_7 == 0)
		{
			goto IL_0040;
		}
		if (L_7 == 1)
		{
			goto IL_0051;
		}
		if (L_7 == 2)
		{
			goto IL_0061;
		}
		if (L_7 == 3)
		{
			goto IL_0073;
		}
		if (L_7 == 4)
		{
			goto IL_0085;
		}
		if (L_7 == 5)
		{
			goto IL_0096;
		}
		if (L_7 == 6)
		{
			goto IL_0096;
		}
	}
	{
		goto IL_00a8;
	}

IL_0040:
	{
		List_1_t1445631064 * L_8 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		float L_9 = V_2;
		NullCheck((List_1_t1445631064 *)L_8);
		((  void (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1445631064 *)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		goto IL_00a8;
	}

IL_0051:
	{
		List_1_t1445631064 * L_10 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1445631064 *)L_10);
		((  void (*) (List_1_t1445631064 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t1445631064 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		goto IL_00a8;
	}

IL_0061:
	{
		List_1_t1445631064 * L_11 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		int32_t L_12 = V_1;
		float L_13 = V_2;
		NullCheck((List_1_t1445631064 *)L_11);
		((  void (*) (List_1_t1445631064 *, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1445631064 *)L_11, (int32_t)L_12, (float)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		goto IL_00a8;
	}

IL_0073:
	{
		List_1_t1445631064 * L_14 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		float L_15 = V_2;
		NullCheck((List_1_t1445631064 *)L_14);
		((  bool (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1445631064 *)L_14, (float)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_00a8;
	}

IL_0085:
	{
		List_1_t1445631064 * L_16 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		int32_t L_17 = V_1;
		NullCheck((List_1_t1445631064 *)L_16);
		((  void (*) (List_1_t1445631064 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1445631064 *)L_16, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		goto IL_00a8;
	}

IL_0096:
	{
		List_1_t1445631064 * L_18 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		int32_t L_19 = V_1;
		float L_20 = V_2;
		NullCheck((List_1_t1445631064 *)L_18);
		((  void (*) (List_1_t1445631064 *, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1445631064 *)L_18, (int32_t)L_19, (float)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		goto IL_00a8;
	}

IL_00a8:
	{
		SyncListChanged_t442780382 * L_21 = (SyncListChanged_t442780382 *)__this->get_m_Callback_3();
		if (!L_21)
		{
			goto IL_00c2;
		}
	}
	{
		SyncListChanged_t442780382 * L_22 = (SyncListChanged_t442780382 *)__this->get_m_Callback_3();
		uint8_t L_23 = V_0;
		int32_t L_24 = V_1;
		NullCheck((SyncListChanged_t442780382 *)L_22);
		((  void (*) (SyncListChanged_t442780382 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((SyncListChanged_t442780382 *)L_22, (int32_t)L_23, (int32_t)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_00c2:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::AddInternal(T)
extern "C"  void SyncList_1_AddInternal_m673825172_gshared (SyncList_1_t3032591531 * __this, float ___item0, const MethodInfo* method)
{
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		float L_1 = ___item0;
		NullCheck((List_1_t1445631064 *)L_0);
		((  void (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1445631064 *)L_0, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::Add(T)
extern "C"  void SyncList_1_Add_m3093692931_gshared (SyncList_1_t3032591531 * __this, float ___item0, const MethodInfo* method)
{
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		float L_1 = ___item0;
		NullCheck((List_1_t1445631064 *)L_0);
		((  void (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1445631064 *)L_0, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		List_1_t1445631064 * L_2 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1445631064 *)L_2);
		int32_t L_3 = ((  int32_t (*) (List_1_t1445631064 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t1445631064 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		float L_4 = ___item0;
		NullCheck((SyncList_1_t3032591531 *)__this);
		((  void (*) (SyncList_1_t3032591531 *, int32_t, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3032591531 *)__this, (int32_t)0, (int32_t)((int32_t)((int32_t)L_3-(int32_t)1)), (float)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::Clear()
extern "C"  void SyncList_1_Clear_m1163979079_gshared (SyncList_1_t3032591531 * __this, const MethodInfo* method)
{
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1445631064 *)L_0);
		((  void (*) (List_1_t1445631064 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t1445631064 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((SyncList_1_t3032591531 *)__this);
		((  void (*) (SyncList_1_t3032591531 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((SyncList_1_t3032591531 *)__this, (int32_t)1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Single>::Contains(T)
extern "C"  bool SyncList_1_Contains_m2904753985_gshared (SyncList_1_t3032591531 * __this, float ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		float L_1 = ___item0;
		NullCheck((List_1_t1445631064 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1445631064 *)L_0, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (bool)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::CopyTo(T[],System.Int32)
extern "C"  void SyncList_1_CopyTo_m3053896959_gshared (SyncList_1_t3032591531 * __this, SingleU5BU5D_t577127397* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		SingleU5BU5D_t577127397* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((List_1_t1445631064 *)L_0);
		((  void (*) (List_1_t1445631064 *, SingleU5BU5D_t577127397*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t1445631064 *)L_0, (SingleU5BU5D_t577127397*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return;
	}
}
// System.Int32 UnityEngine.Networking.SyncList`1<System.Single>::IndexOf(T)
extern "C"  int32_t SyncList_1_IndexOf_m2442115867_gshared (SyncList_1_t3032591531 * __this, float ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		float L_1 = ___item0;
		NullCheck((List_1_t1445631064 *)L_0);
		int32_t L_2 = ((  int32_t (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t1445631064 *)L_0, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (int32_t)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::Insert(System.Int32,T)
extern "C"  void SyncList_1_Insert_m1383230378_gshared (SyncList_1_t3032591531 * __this, int32_t ___index0, float ___item1, const MethodInfo* method)
{
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		int32_t L_1 = ___index0;
		float L_2 = ___item1;
		NullCheck((List_1_t1445631064 *)L_0);
		((  void (*) (List_1_t1445631064 *, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1445631064 *)L_0, (int32_t)L_1, (float)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		int32_t L_3 = ___index0;
		float L_4 = ___item1;
		NullCheck((SyncList_1_t3032591531 *)__this);
		((  void (*) (SyncList_1_t3032591531 *, int32_t, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3032591531 *)__this, (int32_t)2, (int32_t)L_3, (float)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.Single>::Remove(T)
extern "C"  bool SyncList_1_Remove_m119719096_gshared (SyncList_1_t3032591531 * __this, float ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		float L_1 = ___item0;
		NullCheck((List_1_t1445631064 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t1445631064 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1445631064 *)L_0, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_0 = (bool)L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		float L_4 = ___item0;
		NullCheck((SyncList_1_t3032591531 *)__this);
		((  void (*) (SyncList_1_t3032591531 *, int32_t, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3032591531 *)__this, (int32_t)3, (int32_t)0, (float)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_001f:
	{
		bool L_5 = V_0;
		V_1 = (bool)L_5;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::RemoveAt(System.Int32)
extern "C"  void SyncList_1_RemoveAt_m3226634894_gshared (SyncList_1_t3032591531 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t1445631064 *)L_0);
		((  void (*) (List_1_t1445631064 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1445631064 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		int32_t L_2 = ___index0;
		NullCheck((SyncList_1_t3032591531 *)__this);
		((  void (*) (SyncList_1_t3032591531 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((SyncList_1_t3032591531 *)__this, (int32_t)4, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::Dirty(System.Int32)
extern "C"  void SyncList_1_Dirty_m1289160653_gshared (SyncList_1_t3032591531 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		List_1_t1445631064 * L_1 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		int32_t L_2 = ___index0;
		NullCheck((List_1_t1445631064 *)L_1);
		float L_3 = ((  float (*) (List_1_t1445631064 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1445631064 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		NullCheck((SyncList_1_t3032591531 *)__this);
		((  void (*) (SyncList_1_t3032591531 *, int32_t, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3032591531 *)__this, (int32_t)6, (int32_t)L_0, (float)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// T UnityEngine.Networking.SyncList`1<System.Single>::get_Item(System.Int32)
extern "C"  float SyncList_1_get_Item_m3322709784_gshared (SyncList_1_t3032591531 * __this, int32_t ___i0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		int32_t L_1 = ___i0;
		NullCheck((List_1_t1445631064 *)L_0);
		float L_2 = ((  float (*) (List_1_t1445631064 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1445631064 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_0 = (float)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.Single>::set_Item(System.Int32,T)
extern "C"  void SyncList_1_set_Item_m1801038539_gshared (SyncList_1_t3032591531 * __this, int32_t ___i0, float ___value1, const MethodInfo* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		int32_t L_1 = ___i0;
		NullCheck((List_1_t1445631064 *)L_0);
		float L_2 = ((  float (*) (List_1_t1445631064 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1445631064 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_1 = (float)L_2;
		float L_3 = ___value1;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18), &L_4);
		bool L_6 = Single_Equals_m3679433096((float*)(&V_1), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		List_1_t1445631064 * L_7 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		int32_t L_8 = ___i0;
		float L_9 = ___value1;
		NullCheck((List_1_t1445631064 *)L_7);
		((  void (*) (List_1_t1445631064 *, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1445631064 *)L_7, (int32_t)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_11 = ___i0;
		float L_12 = ___value1;
		NullCheck((SyncList_1_t3032591531 *)__this);
		((  void (*) (SyncList_1_t3032591531 *, int32_t, int32_t, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3032591531 *)__this, (int32_t)5, (int32_t)L_11, (float)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0043:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.Networking.SyncList`1<System.Single>::GetEnumerator()
extern "C"  Il2CppObject* SyncList_1_GetEnumerator_m4100478848_gshared (SyncList_1_t3032591531 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1445631064 *)L_0);
		Enumerator_t980360738  L_1 = ((  Enumerator_t980360738  (*) (List_1_t1445631064 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t1445631064 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		Enumerator_t980360738  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), &L_2);
		V_0 = (Il2CppObject*)L_3;
		goto IL_0017;
	}

IL_0017:
	{
		Il2CppObject* L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator UnityEngine.Networking.SyncList`1<System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SyncList_1_System_Collections_IEnumerable_GetEnumerator_m1520768143_gshared (SyncList_1_t3032591531 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((SyncList_1_t3032591531 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (SyncList_1_t3032591531 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((SyncList_1_t3032591531 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		V_0 = (Il2CppObject *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::.ctor()
extern "C"  void SyncList_1__ctor_m1474722247_gshared (SyncList_1_t3105763620 * __this, const MethodInfo* method)
{
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t1518803153 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_Objects_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Networking.SyncList`1<System.UInt32>::get_Count()
extern "C"  int32_t SyncList_1_get_Count_m502901323_gshared (SyncList_1_t3105763620 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1518803153 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t1518803153 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t1518803153 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.UInt32>::get_IsReadOnly()
extern "C"  bool SyncList_1_get_IsReadOnly_m3302912794_gshared (SyncList_1_t3105763620 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		goto IL_0008;
	}

IL_0008:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Networking.SyncList`1/SyncListChanged<T> UnityEngine.Networking.SyncList`1<System.UInt32>::get_Callback()
extern "C"  SyncListChanged_t515952471 * SyncList_1_get_Callback_m1916564565_gshared (SyncList_1_t3105763620 * __this, const MethodInfo* method)
{
	SyncListChanged_t515952471 * V_0 = NULL;
	{
		SyncListChanged_t515952471 * L_0 = (SyncListChanged_t515952471 *)__this->get_m_Callback_3();
		V_0 = (SyncListChanged_t515952471 *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		SyncListChanged_t515952471 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::set_Callback(UnityEngine.Networking.SyncList`1/SyncListChanged<T>)
extern "C"  void SyncList_1_set_Callback_m1287183110_gshared (SyncList_1_t3105763620 * __this, SyncListChanged_t515952471 * ___value0, const MethodInfo* method)
{
	{
		SyncListChanged_t515952471 * L_0 = ___value0;
		__this->set_m_Callback_3(L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::InitializeBehaviour(UnityEngine.Networking.NetworkBehaviour,System.Int32)
extern "C"  void SyncList_1_InitializeBehaviour_m867652159_gshared (SyncList_1_t3105763620 * __this, NetworkBehaviour_t3873055601 * ___beh0, int32_t ___cmdHash1, const MethodInfo* method)
{
	{
		NetworkBehaviour_t3873055601 * L_0 = ___beh0;
		__this->set_m_Behaviour_1(L_0);
		int32_t L_1 = ___cmdHash1;
		__this->set_m_CmdHash_2(L_1);
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32,T)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* LogFilter_t2612395354_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkWriter_t560143343_il2cpp_TypeInfo_var;
extern Il2CppClass* NetworkServer_t3779449791_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2028122062;
extern Il2CppCodeGenString* _stringLiteral2863456972;
extern const uint32_t SyncList_1_SendMsg_m957818222_MetadataUsageId;
extern "C"  void SyncList_1_SendMsg_m957818222_gshared (SyncList_1_t3105763620 * __this, int32_t ___op0, int32_t ___itemIndex1, uint32_t ___item2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncList_1_SendMsg_m957818222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NetworkIdentity_t1766639790 * V_0 = NULL;
	NetworkWriter_t560143343 * V_1 = NULL;
	{
		NetworkBehaviour_t3873055601 * L_0 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogFilter_t2612395354_il2cpp_TypeInfo_var);
		bool L_2 = LogFilter_get_logError_m3315740406(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2028122062, /*hidden argument*/NULL);
	}

IL_0029:
	{
		goto IL_0107;
	}

IL_002e:
	{
		NetworkBehaviour_t3873055601 * L_3 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((Component_t3819376471 *)L_3);
		NetworkIdentity_t1766639790 * L_4 = Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169((Component_t3819376471 *)L_3, /*hidden argument*/Component_GetComponent_TisNetworkIdentity_t1766639790_m2950944169_MethodInfo_var);
		V_0 = (NetworkIdentity_t1766639790 *)L_4;
		NetworkIdentity_t1766639790 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogFilter_t2612395354_il2cpp_TypeInfo_var);
		bool L_7 = LogFilter_get_logError_m3315740406(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2863456972, /*hidden argument*/NULL);
	}

IL_005d:
	{
		goto IL_0107;
	}

IL_0062:
	{
		NetworkIdentity_t1766639790 * L_8 = V_0;
		NullCheck((NetworkIdentity_t1766639790 *)L_8);
		bool L_9 = NetworkIdentity_get_isServer_m1468412663((NetworkIdentity_t1766639790 *)L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0073;
		}
	}
	{
		goto IL_0107;
	}

IL_0073:
	{
		NetworkWriter_t560143343 * L_10 = (NetworkWriter_t560143343 *)il2cpp_codegen_object_new(NetworkWriter_t560143343_il2cpp_TypeInfo_var);
		NetworkWriter__ctor_m2809346428(L_10, /*hidden argument*/NULL);
		V_1 = (NetworkWriter_t560143343 *)L_10;
		NetworkWriter_t560143343 * L_11 = V_1;
		NullCheck((NetworkWriter_t560143343 *)L_11);
		NetworkWriter_StartMessage_m931021002((NetworkWriter_t560143343 *)L_11, (int16_t)((int32_t)9), /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_12 = V_1;
		NetworkIdentity_t1766639790 * L_13 = V_0;
		NullCheck((NetworkIdentity_t1766639790 *)L_13);
		NetworkInstanceId_t33998832  L_14 = NetworkIdentity_get_netId_m1164915858((NetworkIdentity_t1766639790 *)L_13, /*hidden argument*/NULL);
		NullCheck((NetworkWriter_t560143343 *)L_12);
		NetworkWriter_Write_m688344706((NetworkWriter_t560143343 *)L_12, (NetworkInstanceId_t33998832 )L_14, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_15 = V_1;
		int32_t L_16 = (int32_t)__this->get_m_CmdHash_2();
		NullCheck((NetworkWriter_t560143343 *)L_15);
		NetworkWriter_WritePackedUInt32_m2634533176((NetworkWriter_t560143343 *)L_15, (uint32_t)L_16, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_17 = V_1;
		int32_t L_18 = ___op0;
		NullCheck((NetworkWriter_t560143343 *)L_17);
		NetworkWriter_Write_m1305006738((NetworkWriter_t560143343 *)L_17, (uint8_t)(((int32_t)((uint8_t)L_18))), /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_19 = V_1;
		int32_t L_20 = ___itemIndex1;
		NullCheck((NetworkWriter_t560143343 *)L_19);
		NetworkWriter_WritePackedUInt32_m2634533176((NetworkWriter_t560143343 *)L_19, (uint32_t)L_20, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_21 = V_1;
		uint32_t L_22 = ___item2;
		NullCheck((SyncList_1_t3105763620 *)__this);
		VirtActionInvoker2< NetworkWriter_t560143343 *, uint32_t >::Invoke(18 /* System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::SerializeItem(UnityEngine.Networking.NetworkWriter,T) */, (SyncList_1_t3105763620 *)__this, (NetworkWriter_t560143343 *)L_21, (uint32_t)L_22);
		NetworkWriter_t560143343 * L_23 = V_1;
		NullCheck((NetworkWriter_t560143343 *)L_23);
		NetworkWriter_FinishMessage_m3604322206((NetworkWriter_t560143343 *)L_23, /*hidden argument*/NULL);
		NetworkIdentity_t1766639790 * L_24 = V_0;
		NullCheck((Component_t3819376471 *)L_24);
		GameObject_t1756533147 * L_25 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_24, /*hidden argument*/NULL);
		NetworkWriter_t560143343 * L_26 = V_1;
		NetworkBehaviour_t3873055601 * L_27 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 UnityEngine.Networking.NetworkBehaviour::GetNetworkChannel() */, (NetworkBehaviour_t3873055601 *)L_27);
		IL2CPP_RUNTIME_CLASS_INIT(NetworkServer_t3779449791_il2cpp_TypeInfo_var);
		NetworkServer_SendWriterToReady_m3464733118(NULL /*static, unused*/, (GameObject_t1756533147 *)L_25, (NetworkWriter_t560143343 *)L_26, (int32_t)L_28, /*hidden argument*/NULL);
		NetworkBehaviour_t3873055601 * L_29 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_29);
		bool L_30 = NetworkBehaviour_get_isServer_m3866369296((NetworkBehaviour_t3873055601 *)L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0107;
		}
	}
	{
		NetworkBehaviour_t3873055601 * L_31 = (NetworkBehaviour_t3873055601 *)__this->get_m_Behaviour_1();
		NullCheck((NetworkBehaviour_t3873055601 *)L_31);
		bool L_32 = NetworkBehaviour_get_isClient_m3491347612((NetworkBehaviour_t3873055601 *)L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0107;
		}
	}
	{
		SyncListChanged_t515952471 * L_33 = (SyncListChanged_t515952471 *)__this->get_m_Callback_3();
		if (!L_33)
		{
			goto IL_0107;
		}
	}
	{
		SyncListChanged_t515952471 * L_34 = (SyncListChanged_t515952471 *)__this->get_m_Callback_3();
		int32_t L_35 = ___op0;
		int32_t L_36 = ___itemIndex1;
		NullCheck((SyncListChanged_t515952471 *)L_34);
		((  void (*) (SyncListChanged_t515952471 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((SyncListChanged_t515952471 *)L_34, (int32_t)L_35, (int32_t)L_36, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_0107:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::SendMsg(UnityEngine.Networking.SyncList`1/Operation<T>,System.Int32)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t SyncList_1_SendMsg_m2289854462_MetadataUsageId;
extern "C"  void SyncList_1_SendMsg_m2289854462_gshared (SyncList_1_t3105763620 * __this, int32_t ___op0, int32_t ___itemIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SyncList_1_SendMsg_m2289854462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = ___op0;
		int32_t L_1 = ___itemIndex1;
		Initobj (UInt32_t2149682021_il2cpp_TypeInfo_var, (&V_0));
		uint32_t L_2 = V_0;
		NullCheck((SyncList_1_t3105763620 *)__this);
		((  void (*) (SyncList_1_t3105763620 *, int32_t, int32_t, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3105763620 *)__this, (int32_t)L_0, (int32_t)L_1, (uint32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::HandleMsg(UnityEngine.Networking.NetworkReader)
extern "C"  void SyncList_1_HandleMsg_m4042333072_gshared (SyncList_1_t3105763620 * __this, NetworkReader_t3187690923 * ___reader0, const MethodInfo* method)
{
	uint8_t V_0 = 0x0;
	int32_t V_1 = 0;
	uint32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		NetworkReader_t3187690923 * L_0 = ___reader0;
		NullCheck((NetworkReader_t3187690923 *)L_0);
		uint8_t L_1 = NetworkReader_ReadByte_m3670356752((NetworkReader_t3187690923 *)L_0, /*hidden argument*/NULL);
		V_0 = (uint8_t)L_1;
		NetworkReader_t3187690923 * L_2 = ___reader0;
		NullCheck((NetworkReader_t3187690923 *)L_2);
		uint32_t L_3 = NetworkReader_ReadPackedUInt32_m3078633978((NetworkReader_t3187690923 *)L_2, /*hidden argument*/NULL);
		V_1 = (int32_t)L_3;
		NetworkReader_t3187690923 * L_4 = ___reader0;
		NullCheck((SyncList_1_t3105763620 *)__this);
		uint32_t L_5 = VirtFuncInvoker1< uint32_t, NetworkReader_t3187690923 * >::Invoke(19 /* T UnityEngine.Networking.SyncList`1<System.UInt32>::DeserializeItem(UnityEngine.Networking.NetworkReader) */, (SyncList_1_t3105763620 *)__this, (NetworkReader_t3187690923 *)L_4);
		V_2 = (uint32_t)L_5;
		uint8_t L_6 = V_0;
		V_3 = (int32_t)L_6;
		int32_t L_7 = V_3;
		if (L_7 == 0)
		{
			goto IL_0040;
		}
		if (L_7 == 1)
		{
			goto IL_0051;
		}
		if (L_7 == 2)
		{
			goto IL_0061;
		}
		if (L_7 == 3)
		{
			goto IL_0073;
		}
		if (L_7 == 4)
		{
			goto IL_0085;
		}
		if (L_7 == 5)
		{
			goto IL_0096;
		}
		if (L_7 == 6)
		{
			goto IL_0096;
		}
	}
	{
		goto IL_00a8;
	}

IL_0040:
	{
		List_1_t1518803153 * L_8 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		uint32_t L_9 = V_2;
		NullCheck((List_1_t1518803153 *)L_8);
		((  void (*) (List_1_t1518803153 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1518803153 *)L_8, (uint32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		goto IL_00a8;
	}

IL_0051:
	{
		List_1_t1518803153 * L_10 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1518803153 *)L_10);
		((  void (*) (List_1_t1518803153 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t1518803153 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		goto IL_00a8;
	}

IL_0061:
	{
		List_1_t1518803153 * L_11 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		int32_t L_12 = V_1;
		uint32_t L_13 = V_2;
		NullCheck((List_1_t1518803153 *)L_11);
		((  void (*) (List_1_t1518803153 *, int32_t, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1518803153 *)L_11, (int32_t)L_12, (uint32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		goto IL_00a8;
	}

IL_0073:
	{
		List_1_t1518803153 * L_14 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		uint32_t L_15 = V_2;
		NullCheck((List_1_t1518803153 *)L_14);
		((  bool (*) (List_1_t1518803153 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1518803153 *)L_14, (uint32_t)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		goto IL_00a8;
	}

IL_0085:
	{
		List_1_t1518803153 * L_16 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		int32_t L_17 = V_1;
		NullCheck((List_1_t1518803153 *)L_16);
		((  void (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1518803153 *)L_16, (int32_t)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		goto IL_00a8;
	}

IL_0096:
	{
		List_1_t1518803153 * L_18 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		int32_t L_19 = V_1;
		uint32_t L_20 = V_2;
		NullCheck((List_1_t1518803153 *)L_18);
		((  void (*) (List_1_t1518803153 *, int32_t, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1518803153 *)L_18, (int32_t)L_19, (uint32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		goto IL_00a8;
	}

IL_00a8:
	{
		SyncListChanged_t515952471 * L_21 = (SyncListChanged_t515952471 *)__this->get_m_Callback_3();
		if (!L_21)
		{
			goto IL_00c2;
		}
	}
	{
		SyncListChanged_t515952471 * L_22 = (SyncListChanged_t515952471 *)__this->get_m_Callback_3();
		uint8_t L_23 = V_0;
		int32_t L_24 = V_1;
		NullCheck((SyncListChanged_t515952471 *)L_22);
		((  void (*) (SyncListChanged_t515952471 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((SyncListChanged_t515952471 *)L_22, (int32_t)L_23, (int32_t)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
	}

IL_00c2:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::AddInternal(T)
extern "C"  void SyncList_1_AddInternal_m308919353_gshared (SyncList_1_t3105763620 * __this, uint32_t ___item0, const MethodInfo* method)
{
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		uint32_t L_1 = ___item0;
		NullCheck((List_1_t1518803153 *)L_0);
		((  void (*) (List_1_t1518803153 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1518803153 *)L_0, (uint32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::Add(T)
extern "C"  void SyncList_1_Add_m2636178648_gshared (SyncList_1_t3105763620 * __this, uint32_t ___item0, const MethodInfo* method)
{
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		uint32_t L_1 = ___item0;
		NullCheck((List_1_t1518803153 *)L_0);
		((  void (*) (List_1_t1518803153 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((List_1_t1518803153 *)L_0, (uint32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		List_1_t1518803153 * L_2 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1518803153 *)L_2);
		int32_t L_3 = ((  int32_t (*) (List_1_t1518803153 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((List_1_t1518803153 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		uint32_t L_4 = ___item0;
		NullCheck((SyncList_1_t3105763620 *)__this);
		((  void (*) (SyncList_1_t3105763620 *, int32_t, int32_t, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3105763620 *)__this, (int32_t)0, (int32_t)((int32_t)((int32_t)L_3-(int32_t)1)), (uint32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::Clear()
extern "C"  void SyncList_1_Clear_m1771215876_gshared (SyncList_1_t3105763620 * __this, const MethodInfo* method)
{
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1518803153 *)L_0);
		((  void (*) (List_1_t1518803153 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((List_1_t1518803153 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((SyncList_1_t3105763620 *)__this);
		((  void (*) (SyncList_1_t3105763620 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((SyncList_1_t3105763620 *)__this, (int32_t)1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.UInt32>::Contains(T)
extern "C"  bool SyncList_1_Contains_m1235229642_gshared (SyncList_1_t3105763620 * __this, uint32_t ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		uint32_t L_1 = ___item0;
		NullCheck((List_1_t1518803153 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t1518803153 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t1518803153 *)L_0, (uint32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (bool)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::CopyTo(T[],System.Int32)
extern "C"  void SyncList_1_CopyTo_m1942817004_gshared (SyncList_1_t3105763620 * __this, UInt32U5BU5D_t59386216* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		UInt32U5BU5D_t59386216* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((List_1_t1518803153 *)L_0);
		((  void (*) (List_1_t1518803153 *, UInt32U5BU5D_t59386216*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((List_1_t1518803153 *)L_0, (UInt32U5BU5D_t59386216*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		return;
	}
}
// System.Int32 UnityEngine.Networking.SyncList`1<System.UInt32>::IndexOf(T)
extern "C"  int32_t SyncList_1_IndexOf_m1415762930_gshared (SyncList_1_t3105763620 * __this, uint32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		uint32_t L_1 = ___item0;
		NullCheck((List_1_t1518803153 *)L_0);
		int32_t L_2 = ((  int32_t (*) (List_1_t1518803153 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t1518803153 *)L_0, (uint32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (int32_t)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::Insert(System.Int32,T)
extern "C"  void SyncList_1_Insert_m2365521229_gshared (SyncList_1_t3105763620 * __this, int32_t ___index0, uint32_t ___item1, const MethodInfo* method)
{
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		int32_t L_1 = ___index0;
		uint32_t L_2 = ___item1;
		NullCheck((List_1_t1518803153 *)L_0);
		((  void (*) (List_1_t1518803153 *, int32_t, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((List_1_t1518803153 *)L_0, (int32_t)L_1, (uint32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		int32_t L_3 = ___index0;
		uint32_t L_4 = ___item1;
		NullCheck((SyncList_1_t3105763620 *)__this);
		((  void (*) (SyncList_1_t3105763620 *, int32_t, int32_t, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3105763620 *)__this, (int32_t)2, (int32_t)L_3, (uint32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// System.Boolean UnityEngine.Networking.SyncList`1<System.UInt32>::Remove(T)
extern "C"  bool SyncList_1_Remove_m1746606321_gshared (SyncList_1_t3105763620 * __this, uint32_t ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		uint32_t L_1 = ___item0;
		NullCheck((List_1_t1518803153 *)L_0);
		bool L_2 = ((  bool (*) (List_1_t1518803153 *, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((List_1_t1518803153 *)L_0, (uint32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_0 = (bool)L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001f;
		}
	}
	{
		uint32_t L_4 = ___item0;
		NullCheck((SyncList_1_t3105763620 *)__this);
		((  void (*) (SyncList_1_t3105763620 *, int32_t, int32_t, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3105763620 *)__this, (int32_t)3, (int32_t)0, (uint32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_001f:
	{
		bool L_5 = V_0;
		V_1 = (bool)L_5;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::RemoveAt(System.Int32)
extern "C"  void SyncList_1_RemoveAt_m666144777_gshared (SyncList_1_t3105763620 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t1518803153 *)L_0);
		((  void (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t1518803153 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		int32_t L_2 = ___index0;
		NullCheck((SyncList_1_t3105763620 *)__this);
		((  void (*) (SyncList_1_t3105763620 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((SyncList_1_t3105763620 *)__this, (int32_t)4, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::Dirty(System.Int32)
extern "C"  void SyncList_1_Dirty_m2071038416_gshared (SyncList_1_t3105763620 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		List_1_t1518803153 * L_1 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		int32_t L_2 = ___index0;
		NullCheck((List_1_t1518803153 *)L_1);
		uint32_t L_3 = ((  uint32_t (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1518803153 *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		NullCheck((SyncList_1_t3105763620 *)__this);
		((  void (*) (SyncList_1_t3105763620 *, int32_t, int32_t, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3105763620 *)__this, (int32_t)6, (int32_t)L_0, (uint32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		return;
	}
}
// T UnityEngine.Networking.SyncList`1<System.UInt32>::get_Item(System.Int32)
extern "C"  uint32_t SyncList_1_get_Item_m1123482453_gshared (SyncList_1_t3105763620 * __this, int32_t ___i0, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		int32_t L_1 = ___i0;
		NullCheck((List_1_t1518803153 *)L_0);
		uint32_t L_2 = ((  uint32_t (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1518803153 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_0 = (uint32_t)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		uint32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Networking.SyncList`1<System.UInt32>::set_Item(System.Int32,T)
extern "C"  void SyncList_1_set_Item_m2428786142_gshared (SyncList_1_t3105763620 * __this, int32_t ___i0, uint32_t ___value1, const MethodInfo* method)
{
	bool V_0 = false;
	uint32_t V_1 = 0;
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		int32_t L_1 = ___i0;
		NullCheck((List_1_t1518803153 *)L_0);
		uint32_t L_2 = ((  uint32_t (*) (List_1_t1518803153 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t1518803153 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		V_1 = (uint32_t)L_2;
		uint32_t L_3 = ___value1;
		uint32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18), &L_4);
		bool L_6 = UInt32_Equals_m3998179817((uint32_t*)(&V_1), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		List_1_t1518803153 * L_7 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		int32_t L_8 = ___i0;
		uint32_t L_9 = ___value1;
		NullCheck((List_1_t1518803153 *)L_7);
		((  void (*) (List_1_t1518803153 *, int32_t, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((List_1_t1518803153 *)L_7, (int32_t)L_8, (uint32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_11 = ___i0;
		uint32_t L_12 = ___value1;
		NullCheck((SyncList_1_t3105763620 *)__this);
		((  void (*) (SyncList_1_t3105763620 *, int32_t, int32_t, uint32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((SyncList_1_t3105763620 *)__this, (int32_t)5, (int32_t)L_11, (uint32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
	}

IL_0043:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.Networking.SyncList`1<System.UInt32>::GetEnumerator()
extern "C"  Il2CppObject* SyncList_1_GetEnumerator_m1643433315_gshared (SyncList_1_t3105763620 * __this, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		List_1_t1518803153 * L_0 = (List_1_t1518803153 *)__this->get_m_Objects_0();
		NullCheck((List_1_t1518803153 *)L_0);
		Enumerator_t1053532827  L_1 = ((  Enumerator_t1053532827  (*) (List_1_t1518803153 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((List_1_t1518803153 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		Enumerator_t1053532827  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 20), &L_2);
		V_0 = (Il2CppObject*)L_3;
		goto IL_0017;
	}

IL_0017:
	{
		Il2CppObject* L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator UnityEngine.Networking.SyncList`1<System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SyncList_1_System_Collections_IEnumerable_GetEnumerator_m2819505454_gshared (SyncList_1_t3105763620 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((SyncList_1_t3105763620 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (SyncList_1_t3105763620 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((SyncList_1_t3105763620 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		V_0 = (Il2CppObject *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C"  void IndexedSet_1__ctor_m2689707074_gshared (IndexedSet_1_t549597370 * __this, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0));
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		__this->set_m_List_0(L_0);
		Dictionary_2_t1663937576 * L_1 = (Dictionary_2_t1663937576 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		__this->set_m_Dictionary_1(L_1);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C"  void IndexedSet_1_Add_m4044765907_gshared (IndexedSet_1_t549597370 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_List_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t2058570427 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Dictionary_2_t1663937576 * L_2 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_3 = ___item0;
		List_1_t2058570427 * L_4 = (List_1_t2058570427 *)__this->get_m_List_0();
		NullCheck((List_1_t2058570427 *)L_4);
		int32_t L_5 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2058570427 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t1663937576 *)L_2);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t1663937576 *)L_2, (Il2CppObject *)L_3, (int32_t)((int32_t)((int32_t)L_5-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::AddUnique(T)
extern "C"  bool IndexedSet_1_AddUnique_m3246859944_gshared (IndexedSet_1_t549597370 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1663937576 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t1663937576 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0045;
	}

IL_0019:
	{
		List_1_t2058570427 * L_3 = (List_1_t2058570427 *)__this->get_m_List_0();
		Il2CppObject * L_4 = ___item0;
		NullCheck((List_1_t2058570427 *)L_3);
		((  void (*) (List_1_t2058570427 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((List_1_t2058570427 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Dictionary_2_t1663937576 * L_5 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_6 = ___item0;
		List_1_t2058570427 * L_7 = (List_1_t2058570427 *)__this->get_m_List_0();
		NullCheck((List_1_t2058570427 *)L_7);
		int32_t L_8 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2058570427 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((Dictionary_2_t1663937576 *)L_5);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t1663937576 *)L_5, (Il2CppObject *)L_6, (int32_t)((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (bool)1;
		goto IL_0045;
	}

IL_0045:
	{
		bool L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C"  bool IndexedSet_1_Remove_m2685638878_gshared (IndexedSet_1_t549597370 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = (int32_t)(-1);
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1663937576 *, Il2CppObject *, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t1663937576 *)L_0, (Il2CppObject *)L_1, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_002b;
	}

IL_001d:
	{
		int32_t L_3 = V_0;
		NullCheck((IndexedSet_1_t549597370 *)__this);
		((  void (*) (IndexedSet_1_t549597370 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((IndexedSet_1_t549597370 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_1 = (bool)1;
		goto IL_002b;
	}

IL_002b:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t IndexedSet_1_GetEnumerator_m3646001838_MetadataUsageId;
extern "C"  Il2CppObject* IndexedSet_1_GetEnumerator_m3646001838_gshared (IndexedSet_1_t549597370 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IndexedSet_1_GetEnumerator_m3646001838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m3582353431_gshared (IndexedSet_1_t549597370 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		NullCheck((IndexedSet_1_t549597370 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (IndexedSet_1_t549597370 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((IndexedSet_1_t549597370 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		V_0 = (Il2CppObject *)L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C"  void IndexedSet_1_Clear_m2776064367_gshared (IndexedSet_1_t549597370 * __this, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_List_0();
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((List_1_t2058570427 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		Dictionary_2_t1663937576 * L_1 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		NullCheck((Dictionary_2_t1663937576 *)L_1);
		((  void (*) (Dictionary_2_t1663937576 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t1663937576 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C"  bool IndexedSet_1_Contains_m4188067325_gshared (IndexedSet_1_t549597370 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t1663937576 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t1663937576 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		V_0 = (bool)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void IndexedSet_1_CopyTo_m91125111_gshared (IndexedSet_1_t549597370 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_List_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((List_1_t2058570427 *)L_0, (ObjectU5BU5D_t3614634134*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return;
	}
}
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C"  int32_t IndexedSet_1_get_Count_m2839545138_gshared (IndexedSet_1_t549597370 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_List_0();
		NullCheck((List_1_t2058570427 *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2058570427 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C"  bool IndexedSet_1_get_IsReadOnly_m1571858531_gshared (IndexedSet_1_t549597370 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		goto IL_0008;
	}

IL_0008:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C"  int32_t IndexedSet_1_IndexOf_m783474971_gshared (IndexedSet_1_t549597370 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = (int32_t)(-1);
		Dictionary_2_t1663937576 * L_0 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t1663937576 *)L_0);
		((  bool (*) (Dictionary_2_t1663937576 *, Il2CppObject *, int32_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((Dictionary_2_t1663937576 *)L_0, (Il2CppObject *)L_1, (int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		int32_t L_2 = V_0;
		V_1 = (int32_t)L_2;
		goto IL_0019;
	}

IL_0019:
	{
		int32_t L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3687436746;
extern const uint32_t IndexedSet_1_Insert_m676465416_MetadataUsageId;
extern "C"  void IndexedSet_1_Insert_m676465416_gshared (IndexedSet_1_t549597370 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IndexedSet_1_Insert_m676465416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral3687436746, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void IndexedSet_1_RemoveAt_m2714142196_gshared (IndexedSet_1_t549597370 * __this, int32_t ___index0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t2058570427 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t2058570427 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (Il2CppObject *)L_2;
		Dictionary_2_t1663937576 * L_3 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_4 = V_0;
		NullCheck((Dictionary_2_t1663937576 *)L_3);
		((  bool (*) (Dictionary_2_t1663937576 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t1663937576 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		int32_t L_5 = ___index0;
		List_1_t2058570427 * L_6 = (List_1_t2058570427 *)__this->get_m_List_0();
		NullCheck((List_1_t2058570427 *)L_6);
		int32_t L_7 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2058570427 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)((int32_t)L_7-(int32_t)1))))))
		{
			goto IL_003f;
		}
	}
	{
		List_1_t2058570427 * L_8 = (List_1_t2058570427 *)__this->get_m_List_0();
		int32_t L_9 = ___index0;
		NullCheck((List_1_t2058570427 *)L_8);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t2058570427 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		goto IL_0082;
	}

IL_003f:
	{
		List_1_t2058570427 * L_10 = (List_1_t2058570427 *)__this->get_m_List_0();
		NullCheck((List_1_t2058570427 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2058570427 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_1 = (int32_t)((int32_t)((int32_t)L_11-(int32_t)1));
		List_1_t2058570427 * L_12 = (List_1_t2058570427 *)__this->get_m_List_0();
		int32_t L_13 = V_1;
		NullCheck((List_1_t2058570427 *)L_12);
		Il2CppObject * L_14 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t2058570427 *)L_12, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_2 = (Il2CppObject *)L_14;
		List_1_t2058570427 * L_15 = (List_1_t2058570427 *)__this->get_m_List_0();
		int32_t L_16 = ___index0;
		Il2CppObject * L_17 = V_2;
		NullCheck((List_1_t2058570427 *)L_15);
		((  void (*) (List_1_t2058570427 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2058570427 *)L_15, (int32_t)L_16, (Il2CppObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Dictionary_2_t1663937576 * L_18 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_19 = V_2;
		int32_t L_20 = ___index0;
		NullCheck((Dictionary_2_t1663937576 *)L_18);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t1663937576 *)L_18, (Il2CppObject *)L_19, (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		List_1_t2058570427 * L_21 = (List_1_t2058570427 *)__this->get_m_List_0();
		int32_t L_22 = V_1;
		NullCheck((List_1_t2058570427 *)L_21);
		((  void (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((List_1_t2058570427 *)L_21, (int32_t)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
	}

IL_0082:
	{
		return;
	}
}
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * IndexedSet_1_get_Item_m2560856298_gshared (IndexedSet_1_t549597370 * __this, int32_t ___index0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t2058570427 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t2058570427 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (Il2CppObject *)L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Il2CppObject * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void IndexedSet_1_set_Item_m3923255859_gshared (IndexedSet_1_t549597370 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t2058570427 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t2058570427 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_0 = (Il2CppObject *)L_2;
		Dictionary_2_t1663937576 * L_3 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_4 = V_0;
		NullCheck((Dictionary_2_t1663937576 *)L_3);
		((  bool (*) (Dictionary_2_t1663937576 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((Dictionary_2_t1663937576 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		List_1_t2058570427 * L_5 = (List_1_t2058570427 *)__this->get_m_List_0();
		int32_t L_6 = ___index0;
		Il2CppObject * L_7 = ___value1;
		NullCheck((List_1_t2058570427 *)L_5);
		((  void (*) (List_1_t2058570427 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((List_1_t2058570427 *)L_5, (int32_t)L_6, (Il2CppObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		Dictionary_2_t1663937576 * L_8 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_9 = V_0;
		int32_t L_10 = ___index0;
		NullCheck((Dictionary_2_t1663937576 *)L_8);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Dictionary_2_t1663937576 *)L_8, (Il2CppObject *)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C"  void IndexedSet_1_RemoveAll_m2736534958_gshared (IndexedSet_1_t549597370 * __this, Predicate_1_t1132419410 * ___match0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = (int32_t)0;
		goto IL_0034;
	}

IL_0008:
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_List_0();
		int32_t L_1 = V_0;
		NullCheck((List_1_t2058570427 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t2058570427 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_2;
		Predicate_1_t1132419410 * L_3 = ___match0;
		Il2CppObject * L_4 = V_1;
		NullCheck((Predicate_1_t1132419410 *)L_3);
		bool L_5 = ((  bool (*) (Predicate_1_t1132419410 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((Predicate_1_t1132419410 *)L_3, (Il2CppObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		if (!L_5)
		{
			goto IL_002f;
		}
	}
	{
		Il2CppObject * L_6 = V_1;
		NullCheck((IndexedSet_1_t549597370 *)__this);
		((  bool (*) (IndexedSet_1_t549597370 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((IndexedSet_1_t549597370 *)__this, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		goto IL_0033;
	}

IL_002f:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0033:
	{
	}

IL_0034:
	{
		int32_t L_8 = V_0;
		List_1_t2058570427 * L_9 = (List_1_t2058570427 *)__this->get_m_List_0();
		NullCheck((List_1_t2058570427 *)L_9);
		int32_t L_10 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2058570427 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C"  void IndexedSet_1_Sort_m2938181397_gshared (IndexedSet_1_t549597370 * __this, Comparison_1_t3951188146 * ___sortLayoutFunction0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		List_1_t2058570427 * L_0 = (List_1_t2058570427 *)__this->get_m_List_0();
		Comparison_1_t3951188146 * L_1 = ___sortLayoutFunction0;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, Comparison_1_t3951188146 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21)->methodPointer)((List_1_t2058570427 *)L_0, (Comparison_1_t3951188146 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 21));
		V_0 = (int32_t)0;
		goto IL_0034;
	}

IL_0014:
	{
		List_1_t2058570427 * L_2 = (List_1_t2058570427 *)__this->get_m_List_0();
		int32_t L_3 = V_0;
		NullCheck((List_1_t2058570427 *)L_2);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (List_1_t2058570427 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((List_1_t2058570427 *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		V_1 = (Il2CppObject *)L_4;
		Dictionary_2_t1663937576 * L_5 = (Dictionary_2_t1663937576 *)__this->get_m_Dictionary_1();
		Il2CppObject * L_6 = V_1;
		int32_t L_7 = V_0;
		NullCheck((Dictionary_2_t1663937576 *)L_5);
		((  void (*) (Dictionary_2_t1663937576 *, Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((Dictionary_2_t1663937576 *)L_5, (Il2CppObject *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_9 = V_0;
		List_1_t2058570427 * L_10 = (List_1_t2058570427 *)__this->get_m_List_0();
		NullCheck((List_1_t2058570427 *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((List_1_t2058570427 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m1750247524_gshared (U3CStartU3Ec__Iterator0_t2989619467 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m2339115502_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2339115502_gshared (U3CStartU3Ec__Iterator0_t2989619467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m2339115502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t2989619467 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t2989619467 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_t2989619467 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00d5;
		}
	}
	{
		goto IL_010f;
	}

IL_0021:
	{
		ColorTween_t3438117476 * L_2 = (ColorTween_t3438117476 *)__this->get_address_of_tweenInfo_0();
		bool L_3 = ColorTween_ValidTarget_m1255176467((ColorTween_t3438117476 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_010f;
	}

IL_003d:
	{
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		goto IL_00d6;
	}

IL_004d:
	{
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t3438117476 * L_5 = (ColorTween_t3438117476 *)__this->get_address_of_tweenInfo_0();
		bool L_6 = ColorTween_get_ignoreTimeScale_m641454126((ColorTween_t3438117476 *)L_5, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_t2989619467 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_t2989619467 *)(__this));
			goto IL_0075;
		}
	}
	{
		float L_7 = Time_get_unscaledDeltaTime_m4281640537(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t2989619467 *)(G_B6_1));
		goto IL_007a;
	}

IL_0075:
	{
		float L_8 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t2989619467 *)(G_B7_1));
	}

IL_007a:
	{
		NullCheck(G_B8_2);
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)((float)G_B8_1+(float)G_B8_0)));
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t3438117476 * L_10 = (ColorTween_t3438117476 *)__this->get_address_of_tweenInfo_0();
		float L_11 = ColorTween_get_duration_m1819967449((ColorTween_t3438117476 *)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, (float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		__this->set_U3CpercentageU3E__1_2(L_12);
		ColorTween_t3438117476 * L_13 = (ColorTween_t3438117476 *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		ColorTween_TweenValue_m3279916815((ColorTween_t3438117476 *)L_13, (float)L_14, /*hidden argument*/NULL);
		__this->set_U24current_3(NULL);
		bool L_15 = (bool)__this->get_U24disposing_4();
		if (L_15)
		{
			goto IL_00d0;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00d0:
	{
		goto IL_0111;
	}

IL_00d5:
	{
	}

IL_00d6:
	{
		float L_16 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t3438117476 * L_17 = (ColorTween_t3438117476 *)__this->get_address_of_tweenInfo_0();
		float L_18 = ColorTween_get_duration_m1819967449((ColorTween_t3438117476 *)L_17, /*hidden argument*/NULL);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_004d;
		}
	}
	{
		ColorTween_t3438117476 * L_19 = (ColorTween_t3438117476 *)__this->get_address_of_tweenInfo_0();
		ColorTween_TweenValue_m3279916815((ColorTween_t3438117476 *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_010f:
	{
		return (bool)0;
	}

IL_0111:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1702093362_gshared (U3CStartU3Ec__Iterator0_t2989619467 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		V_0 = (Il2CppObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4267712042_gshared (U3CStartU3Ec__Iterator0_t2989619467 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		V_0 = (Il2CppObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3903217005_gshared (U3CStartU3Ec__Iterator0_t2989619467 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m2580847683_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m2580847683_gshared (U3CStartU3Ec__Iterator0_t2989619467 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m2580847683_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m951808111_gshared (U3CStartU3Ec__Iterator0_t2537691210 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::MoveNext()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m42377021_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m42377021_gshared (U3CStartU3Ec__Iterator0_t2537691210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m42377021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t2537691210 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t2537691210 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_t2537691210 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00d5;
		}
	}
	{
		goto IL_010f;
	}

IL_0021:
	{
		FloatTween_t2986189219 * L_2 = (FloatTween_t2986189219 *)__this->get_address_of_tweenInfo_0();
		bool L_3 = FloatTween_ValidTarget_m2349734028((FloatTween_t2986189219 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_010f;
	}

IL_003d:
	{
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		goto IL_00d6;
	}

IL_004d:
	{
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t2986189219 * L_5 = (FloatTween_t2986189219 *)__this->get_address_of_tweenInfo_0();
		bool L_6 = FloatTween_get_ignoreTimeScale_m4161298485((FloatTween_t2986189219 *)L_5, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_t2537691210 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_t2537691210 *)(__this));
			goto IL_0075;
		}
	}
	{
		float L_7 = Time_get_unscaledDeltaTime_m4281640537(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t2537691210 *)(G_B6_1));
		goto IL_007a;
	}

IL_0075:
	{
		float L_8 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t2537691210 *)(G_B7_1));
	}

IL_007a:
	{
		NullCheck(G_B8_2);
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)((float)G_B8_1+(float)G_B8_0)));
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t2986189219 * L_10 = (FloatTween_t2986189219 *)__this->get_address_of_tweenInfo_0();
		float L_11 = FloatTween_get_duration_m1507521972((FloatTween_t2986189219 *)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, (float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		__this->set_U3CpercentageU3E__1_2(L_12);
		FloatTween_t2986189219 * L_13 = (FloatTween_t2986189219 *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		FloatTween_TweenValue_m3881535116((FloatTween_t2986189219 *)L_13, (float)L_14, /*hidden argument*/NULL);
		__this->set_U24current_3(NULL);
		bool L_15 = (bool)__this->get_U24disposing_4();
		if (L_15)
		{
			goto IL_00d0;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00d0:
	{
		goto IL_0111;
	}

IL_00d5:
	{
	}

IL_00d6:
	{
		float L_16 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_t2986189219 * L_17 = (FloatTween_t2986189219 *)__this->get_address_of_tweenInfo_0();
		float L_18 = FloatTween_get_duration_m1507521972((FloatTween_t2986189219 *)L_17, /*hidden argument*/NULL);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_004d;
		}
	}
	{
		FloatTween_t2986189219 * L_19 = (FloatTween_t2986189219 *)__this->get_address_of_tweenInfo_0();
		FloatTween_TweenValue_m3881535116((FloatTween_t2986189219 *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_010f:
	{
		return (bool)0;
	}

IL_0111:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1821360549_gshared (U3CStartU3Ec__Iterator0_t2537691210 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		V_0 = (Il2CppObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m635744877_gshared (U3CStartU3Ec__Iterator0_t2537691210 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_3();
		V_0 = (Il2CppObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m1161010130_gshared (U3CStartU3Ec__Iterator0_t2537691210 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m1787863864_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m1787863864_gshared (U3CStartU3Ec__Iterator0_t2537691210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m1787863864_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C"  void TweenRunner_1__ctor_m3259272810_gshared (TweenRunner_1_t3177091249 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C"  Il2CppObject * TweenRunner_1_Start_m1160751894_gshared (Il2CppObject * __this /* static, unused */, ColorTween_t3438117476  ___tweenInfo0, const MethodInfo* method)
{
	U3CStartU3Ec__Iterator0_t2989619467 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CStartU3Ec__Iterator0_t2989619467 * L_0 = (U3CStartU3Ec__Iterator0_t2989619467 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t2989619467 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t2989619467 *)L_0;
		U3CStartU3Ec__Iterator0_t2989619467 * L_1 = V_0;
		ColorTween_t3438117476  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t2989619467 * L_3 = V_0;
		V_1 = (Il2CppObject *)L_3;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_4 = V_1;
		return L_4;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C"  void TweenRunner_1_Init_m1193845233_gshared (TweenRunner_1_t3177091249 * __this, MonoBehaviour_t1158329972 * ___coroutineContainer0, const MethodInfo* method)
{
	{
		MonoBehaviour_t1158329972 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2779811765;
extern const uint32_t TweenRunner_1_StartTween_m577248035_MetadataUsageId;
extern "C"  void TweenRunner_1_StartTween_m577248035_gshared (TweenRunner_1_t3177091249 * __this, ColorTween_t3438117476  ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m577248035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t1158329972 * L_0 = (MonoBehaviour_t1158329972 *)__this->get_m_CoroutineContainer_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2779811765, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0022:
	{
		NullCheck((TweenRunner_1_t3177091249 *)__this);
		((  void (*) (TweenRunner_1_t3177091249 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((TweenRunner_1_t3177091249 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		MonoBehaviour_t1158329972 * L_2 = (MonoBehaviour_t1158329972 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t3819376471 *)L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_2, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_3);
		bool L_4 = GameObject_get_activeInHierarchy_m4242915935((GameObject_t1756533147 *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		ColorTween_TweenValue_m3279916815((ColorTween_t3438117476 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0055:
	{
		ColorTween_t3438117476  L_5 = ___info0;
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, ColorTween_t3438117476 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (ColorTween_t3438117476 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_m_Tween_1(L_6);
		MonoBehaviour_t1158329972 * L_7 = (MonoBehaviour_t1158329972 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t1158329972 *)L_7);
		MonoBehaviour_StartCoroutine_m2470621050((MonoBehaviour_t1158329972 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StopTween()
extern "C"  void TweenRunner_1_StopTween_m3552027891_gshared (TweenRunner_1_t3177091249 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		MonoBehaviour_t1158329972 * L_1 = (MonoBehaviour_t1158329972 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t1158329972 *)L_1);
		MonoBehaviour_StopCoroutine_m1170478282((MonoBehaviour_t1158329972 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		__this->set_m_Tween_1((Il2CppObject *)NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
extern "C"  void TweenRunner_1__ctor_m468841327_gshared (TweenRunner_1_t2725162992 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Start(T)
extern "C"  Il2CppObject * TweenRunner_1_Start_m791129861_gshared (Il2CppObject * __this /* static, unused */, FloatTween_t2986189219  ___tweenInfo0, const MethodInfo* method)
{
	U3CStartU3Ec__Iterator0_t2537691210 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CStartU3Ec__Iterator0_t2537691210 * L_0 = (U3CStartU3Ec__Iterator0_t2537691210 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t2537691210 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t2537691210 *)L_0;
		U3CStartU3Ec__Iterator0_t2537691210 * L_1 = V_0;
		FloatTween_t2986189219  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t2537691210 * L_3 = V_0;
		V_1 = (Il2CppObject *)L_3;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_4 = V_1;
		return L_4;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Init(UnityEngine.MonoBehaviour)
extern "C"  void TweenRunner_1_Init_m3983200950_gshared (TweenRunner_1_t2725162992 * __this, MonoBehaviour_t1158329972 * ___coroutineContainer0, const MethodInfo* method)
{
	{
		MonoBehaviour_t1158329972 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StartTween(T)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2779811765;
extern const uint32_t TweenRunner_1_StartTween_m3792842064_MetadataUsageId;
extern "C"  void TweenRunner_1_StartTween_m3792842064_gshared (TweenRunner_1_t2725162992 * __this, FloatTween_t2986189219  ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m3792842064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t1158329972 * L_0 = (MonoBehaviour_t1158329972 *)__this->get_m_CoroutineContainer_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, (Object_t1021602117 *)L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2779811765, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0022:
	{
		NullCheck((TweenRunner_1_t2725162992 *)__this);
		((  void (*) (TweenRunner_1_t2725162992 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((TweenRunner_1_t2725162992 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		MonoBehaviour_t1158329972 * L_2 = (MonoBehaviour_t1158329972 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t3819376471 *)L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835((Component_t3819376471 *)L_2, /*hidden argument*/NULL);
		NullCheck((GameObject_t1756533147 *)L_3);
		bool L_4 = GameObject_get_activeInHierarchy_m4242915935((GameObject_t1756533147 *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		FloatTween_TweenValue_m3881535116((FloatTween_t2986189219 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0055:
	{
		FloatTween_t2986189219  L_5 = ___info0;
		Il2CppObject * L_6 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, FloatTween_t2986189219 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (FloatTween_t2986189219 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		__this->set_m_Tween_1(L_6);
		MonoBehaviour_t1158329972 * L_7 = (MonoBehaviour_t1158329972 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_8 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t1158329972 *)L_7);
		MonoBehaviour_StartCoroutine_m2470621050((MonoBehaviour_t1158329972 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StopTween()
extern "C"  void TweenRunner_1_StopTween_m2135918118_gshared (TweenRunner_1_t2725162992 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		MonoBehaviour_t1158329972 * L_1 = (MonoBehaviour_t1158329972 *)__this->get_m_CoroutineContainer_0();
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t1158329972 *)L_1);
		MonoBehaviour_StopCoroutine_m1170478282((MonoBehaviour_t1158329972 *)L_1, (Il2CppObject *)L_2, /*hidden argument*/NULL);
		__this->set_m_Tween_1((Il2CppObject *)NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
extern "C"  List_1_t1440998580 * ListPool_1_Get_m3809147792_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t1440998580 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t4282372027 * L_0 = ((ListPool_1_t924852264_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t4282372027 *)L_0);
		List_1_t1440998580 * L_1 = ((  List_1_t1440998580 * (*) (ObjectPool_1_t4282372027 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t4282372027 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t1440998580 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t1440998580 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3716853512_gshared (Il2CppObject * __this /* static, unused */, List_1_t1440998580 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t4282372027 * L_0 = ((ListPool_1_t924852264_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t1440998580 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t4282372027 *)L_0);
		((  void (*) (ObjectPool_1_t4282372027 *, List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t4282372027 *)L_0, (List_1_t1440998580 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
extern "C"  void ListPool_1__cctor_m408291388_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t2807584331 * L_1 = (UnityAction_1_t2807584331 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t2807584331 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t4282372027 * L_2 = (ObjectPool_1_t4282372027 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t4282372027 *, UnityAction_1_t2807584331 *, UnityAction_1_t2807584331 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t2807584331 *)NULL, (UnityAction_1_t2807584331 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t924852264_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m2151100132_gshared (Il2CppObject * __this /* static, unused */, List_1_t1440998580 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1440998580 * L_0 = ___l0;
		NullCheck((List_1_t1440998580 *)L_0);
		((  void (*) (List_1_t1440998580 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1440998580 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Object>::Get()
extern "C"  List_1_t2058570427 * ListPool_1_Get_m529219189_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t2058570427 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t604976578 * L_0 = ((ListPool_1_t1542424111_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t604976578 *)L_0);
		List_1_t2058570427 * L_1 = ((  List_1_t2058570427 * (*) (ObjectPool_1_t604976578 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t604976578 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t2058570427 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t2058570427 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m1464559125_gshared (Il2CppObject * __this /* static, unused */, List_1_t2058570427 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t604976578 * L_0 = ((ListPool_1_t1542424111_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t2058570427 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t604976578 *)L_0);
		((  void (*) (ObjectPool_1_t604976578 *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t604976578 *)L_0, (List_1_t2058570427 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::.cctor()
extern "C"  void ListPool_1__cctor_m1613652121_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t3425156178 * L_1 = (UnityAction_1_t3425156178 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t3425156178 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t604976578 * L_2 = (ObjectPool_1_t604976578 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t604976578 *, UnityAction_1_t3425156178 *, UnityAction_1_t3425156178 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t3425156178 *)NULL, (UnityAction_1_t3425156178 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t1542424111_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m441310157_gshared (Il2CppObject * __this /* static, unused */, List_1_t2058570427 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2058570427 * L_0 = ___l0;
		NullCheck((List_1_t2058570427 *)L_0);
		((  void (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t2058570427 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Get()
extern "C"  List_1_t243638650 * ListPool_1_Get_m3357896252_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t243638650 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3085012097 * L_0 = ((ListPool_1_t4022459630_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t3085012097 *)L_0);
		List_1_t243638650 * L_1 = ((  List_1_t243638650 * (*) (ObjectPool_1_t3085012097 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t3085012097 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t243638650 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t243638650 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m3047738410_gshared (Il2CppObject * __this /* static, unused */, List_1_t243638650 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3085012097 * L_0 = ((ListPool_1_t4022459630_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t243638650 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t3085012097 *)L_0);
		((  void (*) (ObjectPool_1_t3085012097 *, List_1_t243638650 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t3085012097 *)L_0, (List_1_t243638650 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::.cctor()
extern "C"  void ListPool_1__cctor_m1262585838_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t1610224401 * L_1 = (UnityAction_1_t1610224401 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t1610224401 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t3085012097 * L_2 = (ObjectPool_1_t3085012097 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t3085012097 *, UnityAction_1_t1610224401 *, UnityAction_1_t1610224401 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t1610224401 *)NULL, (UnityAction_1_t1610224401 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t4022459630_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m334430706_gshared (Il2CppObject * __this /* static, unused */, List_1_t243638650 * ___l0, const MethodInfo* method)
{
	{
		List_1_t243638650 * L_0 = ___l0;
		NullCheck((List_1_t243638650 *)L_0);
		((  void (*) (List_1_t243638650 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t243638650 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Get()
extern "C"  List_1_t573379950 * ListPool_1_Get_m4215629480_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t573379950 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3414753397 * L_0 = ((ListPool_1_t57233634_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t3414753397 *)L_0);
		List_1_t573379950 * L_1 = ((  List_1_t573379950 * (*) (ObjectPool_1_t3414753397 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t3414753397 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t573379950 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t573379950 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m782571048_gshared (Il2CppObject * __this /* static, unused */, List_1_t573379950 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t3414753397 * L_0 = ((ListPool_1_t57233634_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t573379950 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t3414753397 *)L_0);
		((  void (*) (ObjectPool_1_t3414753397 *, List_1_t573379950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t3414753397 *)L_0, (List_1_t573379950 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::.cctor()
extern "C"  void ListPool_1__cctor_m4150135476_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t1939965701 * L_1 = (UnityAction_1_t1939965701 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t1939965701 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t3414753397 * L_2 = (ObjectPool_1_t3414753397 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t3414753397 *, UnityAction_1_t1939965701 *, UnityAction_1_t1939965701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t1939965701 *)NULL, (UnityAction_1_t1939965701 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t57233634_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m4179519904_gshared (Il2CppObject * __this /* static, unused */, List_1_t573379950 * ___l0, const MethodInfo* method)
{
	{
		List_1_t573379950 * L_0 = ___l0;
		NullCheck((List_1_t573379950 *)L_0);
		((  void (*) (List_1_t573379950 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t573379950 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
extern "C"  List_1_t1612828711 * ListPool_1_Get_m3002130343_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t1612828711 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t159234862 * L_0 = ((ListPool_1_t1096682395_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t159234862 *)L_0);
		List_1_t1612828711 * L_1 = ((  List_1_t1612828711 * (*) (ObjectPool_1_t159234862 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t159234862 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t1612828711 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t1612828711 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m2208096831_gshared (Il2CppObject * __this /* static, unused */, List_1_t1612828711 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t159234862 * L_0 = ((ListPool_1_t1096682395_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t1612828711 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t159234862 *)L_0);
		((  void (*) (ObjectPool_1_t159234862 *, List_1_t1612828711 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t159234862 *)L_0, (List_1_t1612828711 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
extern "C"  void ListPool_1__cctor_m709904475_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t2979414462 * L_1 = (UnityAction_1_t2979414462 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t2979414462 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t159234862 * L_2 = (ObjectPool_1_t159234862 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t159234862 *, UnityAction_1_t2979414462 *, UnityAction_1_t2979414462 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t2979414462 *)NULL, (UnityAction_1_t2979414462 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t1096682395_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m1243609651_gshared (Il2CppObject * __this /* static, unused */, List_1_t1612828711 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1612828711 * L_0 = ___l0;
		NullCheck((List_1_t1612828711 *)L_0);
		((  void (*) (List_1_t1612828711 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1612828711 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Get()
extern "C"  List_1_t1612828712 * ListPool_1_Get_m2998644518_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	List_1_t1612828712 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t159234863 * L_0 = ((ListPool_1_t1096682396_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t159234863 *)L_0);
		List_1_t1612828712 * L_1 = ((  List_1_t1612828712 * (*) (ObjectPool_1_t159234863 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)((ObjectPool_1_t159234863 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t1612828712 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t1612828712 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m4118150756_gshared (Il2CppObject * __this /* static, unused */, List_1_t1612828712 * ___toRelease0, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectPool_1_t159234863 * L_0 = ((ListPool_1_t1096682396_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_s_ListPool_0();
		List_1_t1612828712 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t159234863 *)L_0);
		((  void (*) (ObjectPool_1_t159234863 *, List_1_t1612828712 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t159234863 *)L_0, (List_1_t1612828712 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::.cctor()
extern "C"  void ListPool_1__cctor_m3678794464_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		UnityAction_1_t2979414463 * L_1 = (UnityAction_1_t2979414463 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (UnityAction_1_t2979414463 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->methodPointer)(L_1, (Il2CppObject *)NULL, (IntPtr_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectPool_1_t159234863 * L_2 = (ObjectPool_1_t159234863 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		((  void (*) (ObjectPool_1_t159234863 *, UnityAction_1_t2979414463 *, UnityAction_1_t2979414463 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->methodPointer)(L_2, (UnityAction_1_t2979414463 *)NULL, (UnityAction_1_t2979414463 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((ListPool_1_t1096682396_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_s_ListPool_0(L_2);
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::<s_ListPool>m__0(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_U3Cs_ListPoolU3Em__0_m3030633432_gshared (Il2CppObject * __this /* static, unused */, List_1_t1612828712 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1612828712 * L_0 = ___l0;
		NullCheck((List_1_t1612828712 *)L_0);
		((  void (*) (List_1_t1612828712 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->methodPointer)((List_1_t1612828712 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
