﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.Match.JoinMatchResponse
struct JoinMatchResponse_t1256242636;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID348058649.h"
#include "UnityEngine_UnityEngine_Networking_Types_NodeID3569487935.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void UnityEngine.Networking.Match.JoinMatchResponse::.ctor()
extern "C"  void JoinMatchResponse__ctor_m698246798 (JoinMatchResponse_t1256242636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.JoinMatchResponse::get_address()
extern "C"  String_t* JoinMatchResponse_get_address_m3988877926 (JoinMatchResponse_t1256242636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_address(System.String)
extern "C"  void JoinMatchResponse_set_address_m3474299283 (JoinMatchResponse_t1256242636 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.Match.JoinMatchResponse::get_port()
extern "C"  int32_t JoinMatchResponse_get_port_m1404363122 (JoinMatchResponse_t1256242636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_port(System.Int32)
extern "C"  void JoinMatchResponse_set_port_m3546525871 (JoinMatchResponse_t1256242636 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Networking.Match.JoinMatchResponse::get_domain()
extern "C"  int32_t JoinMatchResponse_get_domain_m661470421 (JoinMatchResponse_t1256242636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.JoinMatchResponse::get_networkId()
extern "C"  uint64_t JoinMatchResponse_get_networkId_m4244528406 (JoinMatchResponse_t1256242636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C"  void JoinMatchResponse_set_networkId_m3325540973 (JoinMatchResponse_t1256242636 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.JoinMatchResponse::get_accessTokenString()
extern "C"  String_t* JoinMatchResponse_get_accessTokenString_m2934178756 (JoinMatchResponse_t1256242636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_accessTokenString(System.String)
extern "C"  void JoinMatchResponse_set_accessTokenString_m4137246123 (JoinMatchResponse_t1256242636 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.JoinMatchResponse::get_nodeId()
extern "C"  uint16_t JoinMatchResponse_get_nodeId_m1696129542 (JoinMatchResponse_t1256242636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern "C"  void JoinMatchResponse_set_nodeId_m2516067441 (JoinMatchResponse_t1256242636 * __this, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.Match.JoinMatchResponse::get_usingRelay()
extern "C"  bool JoinMatchResponse_get_usingRelay_m3406749068 (JoinMatchResponse_t1256242636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_usingRelay(System.Boolean)
extern "C"  void JoinMatchResponse_set_usingRelay_m1296486861 (JoinMatchResponse_t1256242636 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.Match.JoinMatchResponse::ToString()
extern "C"  String_t* JoinMatchResponse_ToString_m429856533 (JoinMatchResponse_t1256242636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::Parse(System.Object)
extern "C"  void JoinMatchResponse_Parse_m1164611341 (JoinMatchResponse_t1256242636 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
