﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Singleton
struct Singleton_t1770047133;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// showData
struct  showData_t3631251573  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text showData::text
	Text_t356221433 * ___text_2;
	// UnityEngine.GameObject showData::refObj
	GameObject_t1756533147 * ___refObj_3;
	// Singleton showData::singleton
	Singleton_t1770047133 * ___singleton_4;
	// System.Double showData::average
	double ___average_5;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(showData_t3631251573, ___text_2)); }
	inline Text_t356221433 * get_text_2() const { return ___text_2; }
	inline Text_t356221433 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(Text_t356221433 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_refObj_3() { return static_cast<int32_t>(offsetof(showData_t3631251573, ___refObj_3)); }
	inline GameObject_t1756533147 * get_refObj_3() const { return ___refObj_3; }
	inline GameObject_t1756533147 ** get_address_of_refObj_3() { return &___refObj_3; }
	inline void set_refObj_3(GameObject_t1756533147 * value)
	{
		___refObj_3 = value;
		Il2CppCodeGenWriteBarrier(&___refObj_3, value);
	}

	inline static int32_t get_offset_of_singleton_4() { return static_cast<int32_t>(offsetof(showData_t3631251573, ___singleton_4)); }
	inline Singleton_t1770047133 * get_singleton_4() const { return ___singleton_4; }
	inline Singleton_t1770047133 ** get_address_of_singleton_4() { return &___singleton_4; }
	inline void set_singleton_4(Singleton_t1770047133 * value)
	{
		___singleton_4 = value;
		Il2CppCodeGenWriteBarrier(&___singleton_4, value);
	}

	inline static int32_t get_offset_of_average_5() { return static_cast<int32_t>(offsetof(showData_t3631251573, ___average_5)); }
	inline double get_average_5() const { return ___average_5; }
	inline double* get_address_of_average_5() { return &___average_5; }
	inline void set_average_5(double value)
	{
		___average_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
