﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EHuman
struct EHuman_t715499684;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void EHuman::.ctor()
extern "C"  void EHuman__ctor_m516293241 (EHuman_t715499684 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EHuman::Init(System.String)
extern "C"  void EHuman_Init_m186255137 (EHuman_t715499684 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EHuman::Move(System.Int32,System.Int32)
extern "C"  void EHuman_Move_m3808495210 (EHuman_t715499684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EHuman::Draw(System.Int32,System.Int32)
extern "C"  void EHuman_Draw_m2815985313 (EHuman_t715499684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EHuman::Erase(System.Int32,System.Int32)
extern "C"  void EHuman_Erase_m2034365109 (EHuman_t715499684 * __this, int32_t ___l0, int32_t ___r1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
