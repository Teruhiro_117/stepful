﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct ReadOnlyCollection_1_t1702880709;
// System.Collections.Generic.IList`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct IList_1_t2058035618;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.Networking.NetworkLobbyManager/PendingPlayer[]
struct PendingPlayerU5BU5D_t220373972;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>
struct IEnumerator_1_t3287586140;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Networking_UnityEngine_Networking_Netw1517095017.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2418201235_gshared (ReadOnlyCollection_1_t1702880709 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2418201235(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2418201235_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1815078905_gshared (ReadOnlyCollection_1_t1702880709 * __this, PendingPlayer_t1517095017  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1815078905(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, PendingPlayer_t1517095017 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1815078905_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4154439037_gshared (ReadOnlyCollection_1_t1702880709 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4154439037(__this, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m4154439037_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3194792896_gshared (ReadOnlyCollection_1_t1702880709 * __this, int32_t ___index0, PendingPlayer_t1517095017  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3194792896(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, int32_t, PendingPlayer_t1517095017 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3194792896_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2232247906_gshared (ReadOnlyCollection_1_t1702880709 * __this, PendingPlayer_t1517095017  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2232247906(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1702880709 *, PendingPlayer_t1517095017 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2232247906_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3461169868_gshared (ReadOnlyCollection_1_t1702880709 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3461169868(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3461169868_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  PendingPlayer_t1517095017  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2232074096_gshared (ReadOnlyCollection_1_t1702880709 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2232074096(__this, ___index0, method) ((  PendingPlayer_t1517095017  (*) (ReadOnlyCollection_1_t1702880709 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2232074096_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3775302873_gshared (ReadOnlyCollection_1_t1702880709 * __this, int32_t ___index0, PendingPlayer_t1517095017  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3775302873(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, int32_t, PendingPlayer_t1517095017 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3775302873_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3259020349_gshared (ReadOnlyCollection_1_t1702880709 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3259020349(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1702880709 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3259020349_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2409468498_gshared (ReadOnlyCollection_1_t1702880709 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2409468498(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2409468498_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m404307885_gshared (ReadOnlyCollection_1_t1702880709 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m404307885(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1702880709 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m404307885_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2713136910_gshared (ReadOnlyCollection_1_t1702880709 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2713136910(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1702880709 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2713136910_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3860649704_gshared (ReadOnlyCollection_1_t1702880709 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m3860649704(__this, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m3860649704_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m39884492_gshared (ReadOnlyCollection_1_t1702880709 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m39884492(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1702880709 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m39884492_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m766385496_gshared (ReadOnlyCollection_1_t1702880709 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m766385496(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1702880709 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m766385496_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3207186969_gshared (ReadOnlyCollection_1_t1702880709 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3207186969(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3207186969_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3329558497_gshared (ReadOnlyCollection_1_t1702880709 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3329558497(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3329558497_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3141149647_gshared (ReadOnlyCollection_1_t1702880709 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3141149647(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3141149647_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m128498786_gshared (ReadOnlyCollection_1_t1702880709 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m128498786(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1702880709 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m128498786_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2670711452_gshared (ReadOnlyCollection_1_t1702880709 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2670711452(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1702880709 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2670711452_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3497662629_gshared (ReadOnlyCollection_1_t1702880709 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3497662629(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1702880709 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3497662629_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1187125094_gshared (ReadOnlyCollection_1_t1702880709 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1187125094(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1702880709 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1187125094_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1285742985_gshared (ReadOnlyCollection_1_t1702880709 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m1285742985(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1702880709 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m1285742985_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2408179796_gshared (ReadOnlyCollection_1_t1702880709 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2408179796(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2408179796_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2668425955_gshared (ReadOnlyCollection_1_t1702880709 * __this, PendingPlayer_t1517095017  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2668425955(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1702880709 *, PendingPlayer_t1517095017 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2668425955_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3996538333_gshared (ReadOnlyCollection_1_t1702880709 * __this, PendingPlayerU5BU5D_t220373972* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3996538333(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1702880709 *, PendingPlayerU5BU5D_t220373972*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3996538333_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m3041409034_gshared (ReadOnlyCollection_1_t1702880709 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m3041409034(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1702880709 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m3041409034_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3522581765_gshared (ReadOnlyCollection_1_t1702880709 * __this, PendingPlayer_t1517095017  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3522581765(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1702880709 *, PendingPlayer_t1517095017 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3522581765_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m308460862_gshared (ReadOnlyCollection_1_t1702880709 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m308460862(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1702880709 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m308460862_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkLobbyManager/PendingPlayer>::get_Item(System.Int32)
extern "C"  PendingPlayer_t1517095017  ReadOnlyCollection_1_get_Item_m1646187924_gshared (ReadOnlyCollection_1_t1702880709 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m1646187924(__this, ___index0, method) ((  PendingPlayer_t1517095017  (*) (ReadOnlyCollection_1_t1702880709 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m1646187924_gshared)(__this, ___index0, method)
