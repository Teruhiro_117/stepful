﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Everyplay/UploadDidProgressDelegate
struct UploadDidProgressDelegate_t2069570344;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void Everyplay/UploadDidProgressDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void UploadDidProgressDelegate__ctor_m1182991975 (UploadDidProgressDelegate_t2069570344 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/UploadDidProgressDelegate::Invoke(System.Int32,System.Single)
extern "C"  void UploadDidProgressDelegate_Invoke_m511099547 (UploadDidProgressDelegate_t2069570344 * __this, int32_t ___videoId0, float ___progress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Everyplay/UploadDidProgressDelegate::BeginInvoke(System.Int32,System.Single,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UploadDidProgressDelegate_BeginInvoke_m2925747644 (UploadDidProgressDelegate_t2069570344 * __this, int32_t ___videoId0, float ___progress1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Everyplay/UploadDidProgressDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void UploadDidProgressDelegate_EndInvoke_m4023123501 (UploadDidProgressDelegate_t2069570344 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
